<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to Cart Page
 * @author Teamtweaks
 *
 */
class Order_model extends My_Model {

    public function add_order($dataArr = '') {
        $this->db->insert(PRODUCT, $dataArr);
    }

    public function edit_order($dataArr = '', $condition = '') {
        $this->db->where($condition);
        $this->db->update(PRODUCT, $dataArr);
    }

    public function view_order($condition = '') {
        return $this->db->get_where(PRODUCT, $condition);
    }

    public function view_order_details($status) {
        $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.id as PrdID,u.id as uid');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->where('p.status = "' . $status . '"');
        $this->db->group_by("p.dealCodeNumber");
        $this->db->order_by("p.created", "desc");
        $PrdList = $this->db->get();

        //echo '<pre>'; print_r($PrdList->result()); die;
        return $PrdList;
    }

    /*     * ********************************************* Payment Success Cart******************************************************* */

    public function PaymentSuccess($userid = '', $randomId = '', $transId = '', $payerMail = '') {


        $paymtdata = array(
            'randomNo' => $randomId,
            'fc_session_user_id' => $userid,
        );
        $this->session->set_userdata($paymtdata);

        $conditionCheck = array('user_id' => $userid, 'dealCodeNumber' => $randomId, 'status' => 'Paid');
        $statusCheck = $this->order_model->get_all_details(PAYMENT, $conditionCheck);

        if ($statusCheck->num_rows() == 0) {

            $CoupRes = $this->order_model->get_all_details(SHOPPING_CART, array('user_id' => $userid, 'couponID >' => 0));

            $couponID = $CoupRes->row()->couponID;
            $couponAmont = $CoupRes->row()->discountAmount;
            $couponType = $CoupRes->row()->coupontype;

            // Update Coupon
            if ($couponID != 0) {
                if ($couponType == 'Gift') {
                    $SelGift = $this->order_model->get_all_details(GIFTCARDS, array('id' => $couponID));
                    $GiftCountValue = $SelGift->row()->used_amount + $couponAmont;
                    $condition = array('id' => $couponID);
                    $dataArr = array('used_amount' => $GiftCountValue);
                    $this->order_model->update_details(GIFTCARDS, $dataArr, $condition);
                    if ($SelGift->row()->price_value <= $GiftCountValue) {

                        $condition1 = array('id' => $couponID);
                        $dataArr1 = array('card_status' => 'redeemed');
                        $this->order_model->update_details(GIFTCARDS, $dataArr1, $condition1);
                    }
                } else {
                    $SelCoup = $this->order_model->get_all_details(COUPONCARDS, array('id' => $couponID));
                    $CountValue = $SelCoup->row()->purchase_count + 1;
                    $condition = array('id' => $couponID);
                    $dataArr = array('purchase_count' => $CountValue);
                    $this->order_model->update_details(COUPONCARDS, $dataArr, $condition);
                }
            }

            //Update Payment Table	
            $condition1 = array('user_id' => $userid, 'dealCodeNumber' => $randomId);
            if ($payerMail != '') {
                $dataArr1 = array('status' => 'Paid', 'shipping_status' => 'Processed', 'paypal_transaction_id' => $transId, 'payer_email' => $payerMail, 'payment_type' => 'Paypal');
            } else {

                $dataArr1 = array('status' => 'Paid', 'shipping_status' => 'Processed', 'paypal_transaction_id' => $transId, 'payment_type' => 'Credit Cart');
            }

            $this->order_model->update_details(PAYMENT, $dataArr1, $condition1);


            //Update Quantity
            $SelQty = $this->order_model->get_all_details(PAYMENT, array('user_id' => $userid, 'dealCodeNumber' => $randomId));

            foreach ($SelQty->result() as $updPrdRow) {

                $SelPrd = $this->order_model->get_all_details(PRODUCT, array('id' => $updPrdRow->product_id));
                $PrdCount = $SelPrd->row()->purchasedCount + $updPrdRow->quantity;
                $productCount = $SelPrd->row()->quantity - $updPrdRow->quantity;
                $condition2 = array('id' => $updPrdRow->product_id);
                $dataArr2 = array('purchasedCount' => $PrdCount, 'quantity' => $productCount);
                $this->order_model->update_details(PRODUCT, $dataArr2, $condition2);
            }


            //Send Mail to User

            $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
            $this->db->from(PAYMENT . ' as p');
            $this->db->join(USERS . ' as u', 'p.user_id = u.id');
            $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
            $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
            $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
            $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
            $PrdList = $this->db->get();

            $this->db->select('p.sell_id,p.couponCode,u.email');
            $this->db->from(PAYMENT . ' as p');
            $this->db->join(USERS . ' as u', 'p.sell_id = u.id');
            $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
            $this->db->group_by("p.sell_id");
            $SellList = $this->db->get();

            $this->SendMailUSers($PrdList, $SellList);

            //Empty Cart Info
            $condition3 = array('user_id' => $userid);
            $this->order_model->commonDelete(SHOPPING_CART, $condition3);
        }

        $paymtdata = array('randomNo' => '');
        $this->session->set_userdata($paymtdata);

        echo 'Success';
    }

    /*     * ******************************************** Payment Gift Success *************************************************** */

    public function PaymentGiftSuccess($userid = '', $transId = '', $payerMail = '') {


        $paymtdata = array(
            'fc_session_user_id' => $userid,
        );

        $this->session->set_userdata($paymtdata);
        $GiftTemp = $this->order_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));

        if ($GiftTemp->num_rows() > 0) {

            foreach ($GiftTemp->result() as $GiftRows) {


                $dataArr = array();
                foreach ($GiftRows as $key => $val) {
                    if (!(in_array($key, 'id'))) {
                        $dataArr[$key] = trim(addslashes($val));
                    }
                }
                $condition = '';
                $this->order_model->simple_insert(GIFTCARDS, $dataArr);
            }


            $condition1 = array('user_id' => $userid, 'payment_status' => 'Pending');
            if ($payerMail != '') {
                $dataArr1 = array('payment_status' => 'Paid', 'paypal_transaction_id' => $transId, 'payer_email' => $payerMail, 'payment_type' => 'Paypal');
            } else {

                $dataArr1 = array('payment_status' => 'Paid', 'paypal_transaction_id' => $transId, 'payment_type' => 'Credit Cart');
            }

            $this->order_model->update_details(GIFTCARDS, $dataArr1, $condition1);

            //Send Mail to User
            $GiftTempVal = $this->order_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
            $this->SendMailUSersGift($GiftTempVal);

            //Empty Gift cart Temp Info
            $condition3 = array('user_id' => $userid);
            $this->order_model->commonDelete(GIFTCARDS_TEMP, $condition3);
        }

        echo 'Success';
    }

    /*     * ******************************************** Payment Subscribe Success *************************************************** */

    public function PaymentSubscribeSuccess($userid = '', $transId = '') {


        $paymtdata = array(
            'fc_session_user_id' => $userid,
        );

        $this->session->set_userdata($paymtdata);

        $FancyboxTemp = $this->order_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));


        foreach ($FancyboxTemp->result() as $FancyboxRow) {


            $dataArr = array();
            foreach ($FancyboxRow as $key => $val) {
                if ($key != 'id') {
                    $dataArr[$key] = trim(addslashes($val));
                }
            }
            $condition = '';
            $this->order_model->simple_insert(FANCYYBOX_USES, $dataArr);
        }


        $condition1 = array('user_id' => $userid);
        $dataArr1 = array('status' => 'Paid', 'trans_id' => $transId, 'payment_type' => 'Credit Cart');

        $this->order_model->update_details(FANCYYBOX_USES, $dataArr1, $condition1);

        //Update Quantity
        foreach ($FancyboxTemp->result() as $updPrdRow) {

            $SelPrd = $this->order_model->get_all_details(FANCYYBOX, array('id' => $updPrdRow->fancybox_id));
            $PrdCount = $SelPrd->row()->purchased + $updPrdRow->quantity;
            $condition2 = array('id' => $updPrdRow->fancybox_id);
            $dataArr2 = array('purchased' => $PrdCount);
            $this->order_model->update_details(FANCYYBOX, $dataArr2, $condition2);
        }


        //Send Mail to User

        $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city');
        $this->db->from(FANCYYBOX_USES . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->where('p.user_id = "' . $userid . '" and p.status="Paid"');
        $SubcribTempVal = $this->db->get();

        $this->SendMailUSersSubscribe($SubcribTempVal);

        //Empty Gift cart Temp Info
        $condition3 = array('user_id' => $userid);
        $this->order_model->commonDelete(FANCYYBOX_TEMP, $condition3);

        echo 'Success';
    }

    /*     * ******************************************** Send Mail to CSR for failed order********************************************** */

    public function Prepare_failed_order_data() {

        $quick_user_name = $this->session->userdata('quick_user_name');
        if ($quick_user_name != '') {
            $condition = array('user_name' => $quick_user_name);
            $userDetails = $this->user_model->get_all_details(USERS, $condition);
            if ($userDetails->num_rows() == 1) {
                $this->send_failed_order_mail($userDetails);
            }
        }
    }

    public function send_failed_order_mail() {
        $email = 'hello@cityfurnish.com';
        $header .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";

        $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/><body>
                        <p> Failed Order. Please check failed orders on Admin panel and contact customer</p>';
        $message .= '</body>
			</html>';
        $sender_email = $this->data['siteContactMail'];
        $sender_name = 'Admin';


        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $email,
            'subject_message' => '**Cityfurnish- Failed Order Alert**',
            'body_messages' => $message,
            'mail_id' => 'Checkout Mail'
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);
    }

    function get_line_with_replace($array)
    {
        $ci =& get_instance();  
        $ci->load->helper('language');
        $ci->lang->load($file,$lang);
        $str = $ci->lang->line($index);
        if(!empty($convert_arr)){
            $new_str = '';
            $cnt = 0;
            foreach ($convert_arr as $_key => $_data){
                if($cnt==0){
                    $new_str = str_replace($_key, $_data, $str);
                }else{
                    $new_str = str_replace($_key, $_data, $new_str);
                }
                $cnt++;
            }
            return $new_str;
        }else{
            return $str;  
        }
        
        //return str_replace($search, $swap, $str);
    }


    /*     * ******************************************** Send Mail to User********************************************** */

    public function SendMailUSers($PrdList, $SellList) {

       // echo '<pre>';print_r($PrdList->result()); die;

        $shipAddRess = $this->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));

        $newsid = '19';
        $template_values = $this->get_newsletter_template($newsid);
        //var_dump($template_values['news_subject']);exit;
        $adminnewstemplateArr = array(
            'logo' => $this->data['logo'],
            'meta_title' => $this->config->item('meta_title'),
            'ship_fullname' => stripslashes($shipAddRess->row()->full_name),
            'ship_address1' => stripslashes($shipAddRess->row()->address1),
            'ship_address2' => stripslashes($shipAddRess->row()->address2),
            'ship_city' => stripslashes($shipAddRess->row()->city),
            'ship_country' => stripslashes($shipAddRess->row()->country),
            'ship_state' => stripslashes($shipAddRess->row()->state),
            'ship_postalcode' => stripslashes($shipAddRess->row()->postal_code),
            'ship_phone' => stripslashes($shipAddRess->row()->phone),
            'bill_fullname' => stripslashes($shipAddRess->row()->full_name),
            'bill_address1' => stripslashes($shipAddRess->row()->address1),
            'bill_address2' => stripslashes($shipAddRess->row()->address2),
            'bill_city' => stripslashes($shipAddRess->row()->city),
            'bill_country' => stripslashes($shipAddRess->row()->country),
            'bill_state' => stripslashes($shipAddRess->row()->state),
            'bill_postalcode' => stripslashes($shipAddRess->row()->postal_code),
            'bill_phone' => stripslashes($shipAddRess->row()->phone),
            'invoice_number' => $PrdList->row()->dealCodeNumber,
            'invoice_date' => date("F j, Y g:i a", strtotime($PrdList->row()->created))
        );
        extract($adminnewstemplateArr);
        $subject = $template_values['news_subject'];

if($PrdList->result()[0]->user_id == 12320 || $PrdList->result()[0]->user_id == 16598 )
{
   // var_dump($PrdList->result()[0]->user_id);exit;
     $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width"/></head>
<style type="text/css">
@media screen and (max-width: 580px) {
.tab-container {
    max-width: 100%;
}
.txt-pad-15 {
    padding: 0 15px 51px 15px !important;
}
.foo-txt {
    padding: 0px 15px 18px 15px !important;
}
.foo-add {
    padding: 15px 15px 0px 15px !important;
}
.foo-add-left {
    width: 100% !important;
}
.foo-add-right {
    width: 100% !important;
}
.pad-bottom-15 {
    padding-bottom: 15px !important;
}
.pad-20 {
    padding: 25px 20px 20px !important;
}
}
</style>
 <body style="margin:0px;">
  <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="width: 850px;background-color: #fff;margin: 0 auto;table-layout: fixed;background:#dbdbdb;font-family:Arial, Helvetica, sans-serif;font-size:15px;">
    <tbody>
    <tr>
      <td style="padding:20px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td style="text-align: left;width: 100%;padding-bottom:25px;background:#90c5bc;padding:20px;"><a href="https://cityfurnish.com/"><img src="http://180.211.99.165/design/cityfurnish/email/images/logo.png" alt="logo"></a></td>
        </tr>
        </tbody>
        </table>

         <table width="100%" border="0" cellpadding="0" cellspacing="0">
     
          <tr>
            <td style="text-align: left;width: 100%;background:#f5f5f5;padding:25px 30px 30px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="text-align: left;width: 100%;padding-bottom:10px;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-weight:bold;font-size:15px;color:#656565;">Hi '.strtoupper($ship_fullname).'</p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-size:15px;color:#656565;">Thank you for your order.</p></td>
              </tr>
              </table>              
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                <tr>
                  <td style="text-align: left;padding:15px 20px;width:50%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Id</strong><span style="font-weight:400;"> : #'.$invoice_number.'</span> </p></td>
                  <td style="text-align: left;padding:15px 20px;border-left:1px solid #ddd;width:50%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Date </strong><span style="font-weight:400;"> : '.$invoice_date.'</span> </p></td>
                </tr>
              </table>              
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                <tr>
                  <td style="text-align: left;padding:15px 20px;width:100%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Shipping Details</strong> </p></td>
                </tr>
                <tr>
                  <td style="text-align: left;padding:15px 20px;border-top:1px solid #ddd;width:100%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Full Name</strong> <span style="font-weight:400;"> : '.$ship_fullname.'</span> </p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"><strong style="font-weight:600;">Address</strong> <span style="font-weight:400;"> : '.$ship_address1.'</span> </p>

                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">City</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.$ship_city.'</span> <strong style="font-weight:600;;">State</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.$ship_state.'</span> <strong style="font-weight:600;">Zip Code</strong> <span style="font-weight:400;display:inline-block;"> : '.$ship_postalcode.'</span> </p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Phone Number</strong> <span style="font-weight:400;"> : '.$ship_phone.'</span> </p></td>
                </tr>
              </table>

 <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;color:#656565;border-color:#ddd;">
    <tbody>
                <tr>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:100px;"> Product Images </td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;f\ont-weight:600;border-color:#ddd;width:120px;"> Product Name </td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:35px;">QTY</td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:120px;">Security Deposite</td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:110px;">Rental</td>
                  <td align="center" style="color:#656565;padding:20px 5px;font-size:12px;font-weight:600;width:120px;"> Sub Total </td>
                </tr></tbody></table><table>';


     //include('./newsletter/registeration' . $newsid . '.php');
     // $file_data  = file_get_contents('./newsletter/registeration' . $newsid . '.php');
    //  print_r($message1);exit;
      //get_line_with_replace($file_data);

        $message = stripslashes(substr($message, 0, strrpos($message, '</table>')));
      //  $message = stripslashes(substr($message, 0, strrpos($message, '</tbody>')));

       // var_dump($message);exit;

       $message1 .= $message;
        $disTotal = 0;
        $grantTotal = 0;
        
        foreach ($PrdList->result() as $cartRow) {
            $InvImg = @explode(',', $cartRow->image);
            $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->price;
            $unitDeposit = $cartRow->product_shipping_cost;
            $uTot = ($unitPrice + $unitDeposit) * $cartRow->quantity;

            if ($cartRow->attr_name != '' || $cartRow->attr_type != '') {
                $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
            } else {
                $atr = '';
            }
            $message1 .= '<tr>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid; font-size:11px;border-color:#ddd;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" />
            </td>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;line-height:20px;border-color:#ddd;">' . stripslashes($cartRow->product_name) .'<br/>'. $atr .' 
            </td>

            <td align="center" style="padding:20px 0px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . strtoupper($cartRow->quantity) . '
            </td>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitDeposit, 2, '.', '') . '
            </td>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '
            </td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . $uTot . '</td>
        </tr>';
            $grantTotal = $grantTotal + $uTot;
            $grandDeposit = $grandDeposit + ($unitDeposit * $cartRow->quantity);
        }
        $private_total = $grantTotal - $PrdList->row()->discountAmount;
        $private_total = $private_total + $PrdList->row()->tax;

        $total_deposite  += $unitDeposit;
        $total_rental +=  $unitPrice;
        $total_total += $uTot;

        $message1 .= '<tr>
                  <td align="right" colspan="3" style="padding:20px 20px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:12px;font-weight:600;border-color:#ddd;"> TOTAL </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . $total_deposite . ' </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;"> ' . $this->data['currencySymbol'] . $total_rental . ' </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . $total_total . '</td>
            </tr>
        </table>';


       // var_dump($message1);exit;
        if ($PrdList->row()->note != '') {
            $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
                <td width="87" ><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
               
            </tr>
			<tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($PrdList->row()->note) . '</span></td>
            </tr></table>';
        }

        if ($PrdList->row()->order_gift == 1) {
            $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
            </tr></table>';
        }

        $message1 .= '<table border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;color:#656565;border-color:#ddd;">
            <tr>
                <td align="left" style="color:#656565;padding:14px 20px 15px 20px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;padding-left:20px;"> Sub Total </td>

                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</span></td>
            </tr>';
        if ($PrdList->row()->couponCode != '') {
            $message1 .= '<tr>
                <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount <br />
                      <span style="font-size:9px !important;font-weight:400;">(Coupon Code : SUMMER6M)</span></p></td>
                  <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . '-'.number_format($PrdList->row()->discountAmount, '2', '.', '') . '</td>
            </tr>';
        }
        $message1 .= '<tr>
                  <td align="left" style="color:#656565;padding:14px 15px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-size:14px;font-weight:600;border-color:#ddd;padding-left:20px;">GRAND TOTAL</td>
                  <td align="center" style="color:#60a196;padding:15px 20px;border-top:1px solid #ddd;font-size:15px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</td>
                </tr>
            </tr>
            </table></td></tr>
          <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
            <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
              <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
                Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
          </tr>
        </table></td>
    </tr>
  </table>
</body>
</html>';

    }else {

        $message1 = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width"/></head>
        <title>'.$template_values['news_subject'].'</title>
        <body>';
        include('./newsletter/registeration'.$newsid.'.php');

            $message = stripslashes(substr($message,0,strrpos($message,'</tbody>')));
            $message = stripslashes(substr($message,0,strrpos($message,'</tbody>')));
            $message1 .= $message;      
$disTotal =0; $grantTotal = 0;
foreach ($PrdList->result() as $cartRow) { $InvImg = @explode(',',$cartRow->image); 
$unitPrice = ($cartRow->price*(0.01*$cartRow->product_tax_cost))+$cartRow->price; 
$unitDeposit =  $cartRow->product_shipping_cost;
$uTot = ($unitPrice + $unitDeposit)*$cartRow->quantity;
if($cartRow->attr_name != '' || $cartRow->attr_type != ''){ $atr = '<br>'.$cartRow->attr_type.' / '.$cartRow->attr_name; }else{ $atr = '';}
$message1.='<tr>
            <td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;"><img src="'.base_url().PRODUCTPATH.$InvImg[0].'" alt="'.stripslashes($cartRow->product_name).'" width="70" /></span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.stripslashes($cartRow->product_name).$atr.'</span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.strtoupper($cartRow->quantity).'</span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($unitDeposit,2,'.','').'</span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($unitPrice,2,'.','').'</span></td>
            <td style="text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($uTot,2,'.','').'</span></td>
        </tr>';
    $grantTotal = $grantTotal + $uTot;
    $grandDeposit = $grandDeposit + ($unitDeposit*$cartRow->quantity);
}
    $private_total = $grantTotal - $PrdList->row()->discountAmount;
    $private_total = $private_total + $PrdList->row()->tax;
                 
$message1.='</table></td> </tr><tr><td colspan="3"><table border="0" cellspacing="0" cellpadding="0" style=" margin:10px 0px; width:99.5%;"><tr>
            <td width="460" valign="top" >';
            if($PrdList->row()->note !=''){
$message1.='<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
                <td width="87" ><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
               
            </tr>
            <tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">'.stripslashes($PrdList->row()->note).'</span></td>
            </tr></table>';
            }
            
            if($PrdList->row()->order_gift == 1){
$message1.='<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
            </tr></table>';
            }
            
$message1.='</td>
            <td width="174" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cecece;">
            <tr bgcolor="#f3f3f3">
                <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Sub Total</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">'.$this->data['currencySymbol'].number_format($grantTotal,'2','.','').'</span></td>
            </tr>';
            if($PrdList->row()->couponCode != ''){
            $message1.='    <tr>
                <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Coupon Code Used</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">'.$PrdList->row()->couponCode.'</span></td>
            </tr>'; 
            }           
        $message1.='    <tr>
                <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Discount Amount</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">'.$this->data['currencySymbol'].number_format($PrdList->row()->discountAmount,'2','.','').'</span></td>
            </tr>
        <tr bgcolor="#f3f3f3">
            <td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Deposit</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">'.$this->data['currencySymbol'].number_format($grandDeposit,2,'.','').'</span></td>
              </tr>
              <!--<tr>
            <td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Shipping Tax</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">'.$this->data['currencySymbol'].number_format($PrdList->row()->tax ,2,'.','').'</span></td>
              </tr>-->
              <tr bgcolor="#f3f3f3">
                <td width="87" style="border-right:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">Grand Total (INR) </span></td>
                <td width="31"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">'.$this->data['currencySymbol'].number_format($private_total,'2','.','').'</span></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
        </tr>
    </table>
        </div>
        
        <!--end of left--> 
        <div style="width:50%; float:left;">
                <div style="float:left; width:100%;font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; width:100%; color:#000000; line-height:38px; "><span>'.stripslashes($PrdList->row()->full_name).'</span>, thank you for your purchase.</div>
                <div style="float:left; width:100%;font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; width:100%; color:#000000; line-height:38px; "> Our team will call you to schedule the delivery.</div>
               <ul style="width:100%; margin:10px 0px 0px 0px; padding:0; list-style:none; float:left; font-size:12px; font-weight:normal; line-height:19px; font-family:Arial, Helvetica, sans-serif; color:#000;">
                    <li>Price mentioned are inclusive of applicable Govt. taxes.</li>                    
                    <li>If you have any concerns please contact us.</li>
                    <li>Email: <span>'.stripslashes($this->data['siteContactMail']).' </span></li>
                    <li>Phone: +91 8010845000</li>
               </ul>
            </div>
            
            <div style="width:27.4%; margin-right:5px; float:right;">
            
           
            </div>
        
        <div style="clear:both"></div>
        
    </div>
    </div></body></html>';  
    }
        //echo $message;
        //echo '<br>'.$PrdList->row()->email;


        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->config->item('site_contact_mail');
            $sender_name = $this->config->item('email_title');
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }
        
       // var_dump($PrdList->row()->email);exit;
        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $PrdList->row()->email,
            'cc_mail_id' => 'hello@cityfurnish.com',
            'subject_message' => $subject,
            'body_messages' => $message1
        );
        $email_send_to_common = $this->common_email_send($email_values);
        //var_dump($email_send_to_common);exit;



        //echo $this->email->print_debugger(); die; 

        /*         * ********************************************seller Product Confirmation Mail Sent *********************************************** */

        foreach ($SellList->result() as $sellRow) {

            //echo '<pre>';print_r($sellRow->email);
            $message2 = '';
            $subject2 = $template_values['news_subject'];
            $message2 .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width"/></head>
<style type="text/css">
@media screen and (max-width: 580px) {
.tab-container {
    max-width: 100%;
}
.txt-pad-15 {
    padding: 0 15px 51px 15px !important;
}
.foo-txt {
    padding: 0px 15px 18px 15px !important;
}
.foo-add {
    padding: 15px 15px 0px 15px !important;
}
.foo-add-left {
    width: 100% !important;
}
.foo-add-right {
    width: 100% !important;
}
.pad-bottom-15 {
    padding-bottom: 15px !important;
}
.pad-20 {
    padding: 25px 20px 20px !important;
}
}
</style>
 <body style="margin:0px;">

';
            $message2 .= $message;

            $disTotal = 0;
            $grantTotal = 0;
            foreach ($PrdList->result() as $cartRow) {
                if ($cartRow->sell_id == $sellRow->sell_id) {

                    $InvImg = @explode(',', $cartRow->image);
                    $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->product_shipping_cost + $cartRow->price;
                    $uTot = $unitPrice * $cartRow->quantity;
                    if ($cartRow->attr_name != '' || $cartRow->attr_type != '') {
                        $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
                    } else {
                        $atr = '';
                    }

                   
                    $message2 .= '<tr>
            <td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" /></span></td>
			<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . stripslashes($cartRow->product_name) . $atr . '</span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . strtoupper($cartRow->quantity) . '</span></td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitDeposit, 2, '.', '') . '
            </td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '</span></td>
            <td style="text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($uTot, 2, '.', '') . '</span></td>
        </tr>';
                    $grantTotal = $grantTotal + $uTot;
                     $total_deposite1  +=  $unitDeposit;
                     $total_rental1 += $unitPrice;
                     $total_total1 += $uTot;
                }
            }

            $message2 .= '
             <tr>
                  <td align="right" colspan="3" style="padding:20px 20px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:12px;font-weight:600;border-color:#ddd;"> TOTAL </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;"> ' . $this->data['currencySymbol'] . number_format($total_deposite1, 2, '.', '') .' </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($total_rental1, 2, '.', '') .'</td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($total_total1, 2, '.', '') .'</td>
                </tr></table>

                 <table border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;color:#656565;border-color:#ddd;"><tr>';
            if ($PrdList->row()->note != '') {
                $message2 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
                <td width="87" ><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
               
            </tr>
			<tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($PrdList->row()->note) . '</span></td>
            </tr></table>';
            }

            if ($PrdList->row()->order_gift == 1) {
                $message2 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
            </tr></table>';
            }

            $message2 .= '
                <td align="left" style="color:#656565;padding:14px 20px 15px 20px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;padding-left:20px;"> Sub Total </td>
                  <td align="center" style="color:#656565;padding:15px 20px;font-size:11px;font-weight:600;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</td>
              </tr>';
            if ($PrdList->row()->couponCode != '') {
                $message2 .= '	<tr>
                    <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount <br />
                          <span style="font-size:9px !important;font-weight:400;">(Coupon Code :'. $PrdList->row()->couponCode .')</span></p></td>
                    <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . '-'.number_format($PrdList->row()->discountAmount, '2', '.', '') . '</td>
            </tr>';
            }
            $message2 .= '<tr>
                <td align="left" style="color:#656565;padding:14px 15px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-size:14px;font-weight:600;border-color:#ddd;padding-left:20px;">GRAND TOTAL</td>
                <td align="center" style="color:#60a196;padding:15px 20px;border-top:1px solid #ddd;font-size:15px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</td>
                </tr>
                </table>
            </td>
         </tr>
          <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
            <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
              <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
                Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
          </tr>
        </table></td>
    </tr>
  </table>
</body>
</html>';

            $email_values1 = array('mail_type' => 'html',
                'from_mail_id' => $sender_email,
                'mail_name' => $sender_name,
                'to_mail_id' => $sellRow->email,
                'cc_mail_id' => 'invoices@3c0x36x2q5.referralcandy.com',
                'subject_message' => $subject,
                'body_messages' => $message2
            );


            $email_send_to_common = $this->product_model->common_email_send($email_values1);
        }


        return;
    }

    /*     * ******************************************** Send Mail to Gift********************************************** */

    public function SendMailUSersGift($GiftRowsVals) {

        //echo '<pre>';print_r($GiftRowsVals);	
        foreach ($GiftRowsVals->result() as $GiftVals) {



            $newsid = '15';
            $template_values = $this->order_model->get_newsletter_template_details($newsid);
            $usrDetails = $this->order_model->get_all_details(USERS, array('id' => $GiftVals->user_id));
            $adminnewstemplateArr = array(
                'email_title' => $this->config->item('email_title'),
                'logo' => $this->data['logo'],
                'meta_title' => $this->config->item('meta_title'),
                'full_name' => stripslashes($usrDetails->row()->full_name),
                'email' => stripslashes($usrDetails->row()->email),
                'code' => stripslashes($GiftVals->code),
                'price_value' => stripslashes($GiftVals->price_value),
                'expiry_date' => stripslashes($GiftVals->expiry_date),
                'description' => stripslashes($GiftVals->description),
                'rname' => stripslashes($GiftVals->recipient_name)
            );
            extract($adminnewstemplateArr);
            $subject = $template_values['news_subject'];


            $message = '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>Gift Card</title>
			</head>
			<body>';
            include('./newsletter/registeration' . $newsid . '.php');
            $message .= '</body>
			</html>';

            if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                $sender_email = $this->config->item('site_contact_mail');
                $sender_name = $this->config->item('email_title');
            } else {
                $sender_name = $template_values['sender_name'];
                $sender_email = $template_values['sender_email'];
            }

            $email_values = array('mail_type' => 'html',
                'from_mail_id' => $sender_email,
                'mail_name' => $sender_name,
                'to_mail_id' => $GiftVals->recipient_mail,
                'cc_mail_id' => $this->config->item('site_contact_mail'),
                'subject_message' => $subject,
                'body_messages' => $message
            );
            $email_send_to_common = $this->product_model->common_email_send($email_values);
        }
        //echo $this->email->print_debugger(); die; 
        return;
    }

    /*     * ******************************************** Send Mail to Subscribe********************************************** */

    public function SendMailUSersSubscribe($PrdList) {

        $subject = 'From: ' . $this->config->item('email_title') . ' Subscription';

        $message = '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>Gift Card</title>
			</head>
			<body marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
			<table width="640" border="0" cellspacing="0" cellpadding="0" bgcolor="#7da2c1">
			<tr>
			<td style="padding:40px;">
			<table width="610" border="0" cellpadding="0" cellspacing="0" style="border:#1d4567 1px solid; font-family:Arial, Helvetica, sans-serif;">
				<tr>
				<td>
				<a href="' . base_url() . '"><img src="' . base_url() . 'images/logo/' . $this->data['logo'] . '" alt="' . $this->config->item('meta_title') . '" style="margin:15px 5px 0; padding:0px; border:none;"></a>
				</td>
				</tr>
				<tr>
				<td valign="top" style="background-color:#FFFFFF;">
				<table border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
					<td colspan="2">
					<h3 style="padding:10px 15px; margin:0px; color:#0d487a;">Subscription for ' . ucfirst($this->config->item('email_title')) . '</h3>
					</td>
					</tr>
				</table>';


        $message .= '<table width="611" border="0" cellpadding="0" cellspacing="0"><tr>
                		    <th width="37%" align="left">Product Title</th>
		                    <th width="30%">Quantity</th>
        		            <th width="33%">Amount</th>
                			</tr>';
        $grantTotal = 0;
        foreach ($PrdList->result() as $cartRow) {

            $message .= '
                <tr style="font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; color:#292881; padding: 0px 4px 0px 5px;">
                  <td width="38%">' . stripslashes($cartRow->name) . '</td>
                  <td width="23%" align="center">' . strtoupper($cartRow->quantity) . '</td>
                  <td width="28%" align="center">' . $this->data['currencySymbol'] . $cartRow->indtotal . '</td>
                </tr>
                ';
            $grantTotal = $grantTotal + $cartRow->indtotal;
        }
        $private_total = $grantTotal;
        $private_total = $private_total + ($private_total * $cartRow->tax * 0.01) + $PrdList->row()->shippingcost;
        $message .= '
                <tr>
                  <td>&nbsp;</td>
                </tr>
                ';

        $message .= '
                <tr>
                  <td width="30%">&nbsp;</td>
                  <td width="30%" style="font-size:14px; font-weight:bold; color:#000000;"  > Subscription Date</td>
                  <td width="40%" align="left" style="font-size:12px; font-weight:bold; color:#000000;">' . date("F j, Y, g:i a", strtotime($PrdList->row()->created)) . '</td>
                </tr>';

        $shipAddRess = $this->order_model->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));


        $message .= '<tr>
                  <td width="30%">&nbsp;</td>
                  <td width="30%" style="font-size:14px; font-weight:bold; color:#000000;" > Tax</td>
                  <td width="40%" align="left" style="font-size:12px; font-weight:bold; color:#000000;">$ ' . $PrdList->row()->tax . ' </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td width="20%" style="font-size:14px; font-weight:bold; color:#000000;" > Total </td>
                  <td width="28%" align="left" style="font-size:18px; font-weight:bold; color:#000000;">$ ' . number_format($private_total + $tax, 2, '.', ' ') . '</td>
                </tr>
				</table>
				
<div style="display:inline-block; float:left; width:100%; font-size:12px;">
	<div style="display:inline-block; float:left; width:50%;">

	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr style="border:1px solid #7DA2C1;">
                      <td style=" font:bold 14px/34px Arial, Helvetica, sans-serif;	color:#000;	background:#7DA2C1; border-bottom:1px solid #b6b3b3;">Billing Details</td>
                    </tr>
                  </table>
               
               
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-left:15px; font-family:Verdana, Arial, Helvetica, sans-serif;">
                    <tr>
                      <td width="100" style="color:#000000;"><strong>Full name </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->full_name) . '</td>
                    </tr>
                   
                    <tr>
                      <td style="color:#000000;"><strong>Address</strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->address) . '</td>
                    </tr>
                   
                    <tr>
                      <td style="color:#000000;"><strong>Country</strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->country) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>State</strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->state) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>City </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->city) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>postal code </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->postal_code) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>Phone </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->phone_no) . '</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td><b>Support Team</b></td>
                    </tr>
                    <tr>
                      <td style="font-size:16px; font-weight:bold; color:#935435;"><strong> ' . $this->config->item('email_title') . ' Team</strong></td>
                    </tr>
                  </table>
</div>
<div style="display:inline-block; float:left; width:50%;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr style="border:1px solid #b6b3b3;">
                      <td style=" font:bold 14px/34px Arial, Helvetica, sans-serif;	color:#000;	background:#7DA2C1; border-bottom:1px solid #b6b3b3;">Shipping Details</td>
                    </tr>
                  </table>
               
               
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-left:15px; font-family:Verdana, Arial, Helvetica, sans-serif;">
                    <tr>
                      <td width="120" style="color:#000000;"><strong>Full name </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->full_name) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>Address</strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->address1) . '</td>
                    </tr>
                   
                    <tr>
                      <td style="color:#000000;"><strong>Country</strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->country) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>State/province </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->state) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>City </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->city) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>Zip/postal code </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->postal_code) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>Phone </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->phone) . '</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
</div>
</div>
         </div>				
				
					<table border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">	
							<tr>
								<td width="50%" valign="top" style="font-size:12px; padding:10px 15px;">
									
								</td>
								<td width="50%" valign="top" style="font-size:12px; padding:10px 15px;">
									<p>
										
									</p>
									<p>
										
									</p>
								</td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
			</body>
			</html>';
        echo $message;




        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $this->config->item('site_contact_mail'),
            'mail_name' => $this->config->item('email_title'),
            'to_mail_id' => $PrdList->row()->email,
            'cc_mail_id' => $this->config->item('site_contact_mail'),
            'subject_message' => $subject,
            'body_messages' => $message
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);



        return;
    }

    /*     * ******************************************** View Orders ********************************************** */

    public function view_orders($userid, $randomId) {

        $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
        $PrdList = $this->db->get();

        $shipAddRess = $this->order_model->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));


        $newsid = '19';
        $template_values = $this->get_newsletter_template_details($newsid);
        $adminnewstemplateArr = array(
            'logo' => $this->data['logo'],
            'meta_title' => $this->config->item('meta_title'),
            'ship_fullname' => stripslashes($shipAddRess->row()->full_name),
            'ship_address1' => stripslashes($shipAddRess->row()->address1),
            'ship_address2' => stripslashes($shipAddRess->row()->address2),
            'ship_city' => stripslashes($shipAddRess->row()->city),
            'ship_country' => stripslashes($shipAddRess->row()->country),
            'ship_state' => stripslashes($shipAddRess->row()->state),
            'ship_postalcode' => stripslashes($shipAddRess->row()->postal_code),
            'ship_phone' => stripslashes($shipAddRess->row()->phone),
            'bill_fullname' => stripslashes($PrdList->row()->full_name),
            'bill_address1' => stripslashes($PrdList->row()->address),
            'bill_address2' => stripslashes($PrdList->row()->address2),
            'bill_city' => stripslashes($PrdList->row()->city),
            'bill_country' => stripslashes($PrdList->row()->country),
            'bill_state' => stripslashes($PrdList->row()->state),
            'bill_postalcode' => stripslashes($PrdList->row()->postal_code),
            'bill_phone' => stripslashes($PrdList->row()->phone_no),
            'invoice_number' => $PrdList->row()->dealCodeNumber,
            'invoice_date' => date("F j, Y g:i a", strtotime($PrdList->row()->created))
        );
        extract($adminnewstemplateArr);
        $subject = $template_values['news_subject'];
        $message1 .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width"/></head>
<title>' . $template_values['news_subject'] . '</title>
<body>

';
        include('./newsletter/registeration' . $newsid . '.php');

        $message = stripslashes(substr($message, 0, strrpos($message, '</tbody>')));
        $message = stripslashes(substr($message, 0, strrpos($message, '</tbody>')));
        $message1 .= $message;


        $disTotal = 0;
        $grantTotal = 0;
        foreach ($PrdList->result() as $cartRow) {
            $InvImg = @explode(',', $cartRow->image);
            $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->product_shipping_cost + $cartRow->price;
            $uTot = $unitPrice * $cartRow->quantity;
            if ($cartRow->attr_name != '' || $cartRow->attr_type != '') {
                $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
            } else {
                $atr = '';
            }
            $message1 .= '<tr>
            <td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" /></span></td>
			<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . stripslashes($cartRow->product_name) . $atr . '</span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . strtoupper($cartRow->quantity) . '</span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '</span></td>
            <td style="text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($uTot, 2, '.', '') . '</span></td>
        </tr>';
            $grantTotal = $grantTotal + $uTot;
        }
        $private_total = $grantTotal - $PrdList->row()->discountAmount;
        $private_total = $private_total + $PrdList->row()->tax + $PrdList->row()->shippingcost;

        $message1 .= '</table></td> </tr><tr><td colspan="3"><table border="0" cellspacing="0" cellpadding="0" style=" margin:10px 0px; width:99.5%;"><tr>
			<td width="460" valign="top" >';
        if ($PrdList->row()->note != '') {
            $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
                <td width="87" ><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
               
            </tr>
			<tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($PrdList->row()->note) . '</span></td>
            </tr></table>';
        }

        if ($PrdList->row()->order_gift == 1) {
            $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
            </tr></table>';
        }

        $message1 .= '</td>
            <td width="174" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cecece;">
            <tr bgcolor="#f3f3f3">
                <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Sub Total</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</span></td>
            </tr>
			<tr>
                <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Discount Amount</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->discountAmount, '2', '.', '') . '</span></td>
            </tr>
		<tr bgcolor="#f3f3f3">
            <td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Shipping Cost</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->shippingcost, 2, '.', '') . '</span></td>
              </tr>
			  <!--<tr>
            <td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Shipping Tax</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->tax, 2, '.', '') . '</span></td>
              </tr>-->
			  <tr bgcolor="#f3f3f3">
                <td width="87" style="border-right:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">Grand Total</span></td>
                <td width="31"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</span></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
        </tr>
    </table>
        </div>
        
        <!--end of left--> 
		<div style="width:50%; float:left;">
            	<div style="float:left; width:100%;font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; width:100%; color:#000000; line-height:38px; "><span>' . stripslashes($PrdList->row()->full_name) . '</span>, thank you for your order.</div>
               <ul style="width:100%; margin:10px 0px 0px 0px; padding:0; list-style:none; float:left; font-size:12px; font-weight:normal; line-height:19px; font-family:Arial, Helvetica, sans-serif; color:#000;">
                    <li>Price mentioned are inclusive of applicable Govt. taxes.</li>
                    <li>If you have any concerns please contact us.</li>
                    <li>Email: <span>' . stripslashes($this->data['siteContactMail']) . ' </span></li>
                    <li>Phone: +91 8010845000</li>
               </ul>
        	</div>
            
            <div style="width:27.4%; margin-right:5px; float:right;">
            
           
            </div>
        
        <div style="clear:both"></div>
        
    </div>
    </div></body></html>';
        return $message1;
    }

    public function view_orders_new($userid, $randomId) {

        $this->db->select('p.*,u.email,u.full_name,u.address,u.address2,u.phone_no,u.postal_code,u.state,u.country,u.city,u.s_tin_no,u.s_vat_no,u.s_cst_no,pd.product_name,pd.id as PrdID,pd.image,pd.seller_product_id,pd.sku,pAr.attr_name as attr_type,sp.attr_name');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.sell_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');

        $PrdList = $this->db->get();


        $shipAddRess = $this->order_model->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));
        if($PrdList->row()->address2 !=''){
    $new_address = $PrdList->row()->address.'<br/><br/><strong style="font-weight:600;">Address2:</strong> '.$PrdList->row()->address2;
}else {  $new_address = $PrdList->row()->address; }

if($shipAddRess->row()->address2 != ''){
    $new_ship_address = $shipAddRess->row()->address1.'<br/><br/><strong style="font-weight:600;">Address2:</strong> '.$shipAddRess->row()->address2;
}else {  $new_ship_address = $shipAddRess->row()->address1; }

        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width"/></head>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Order Confirmation</title>
  <style type="text/css">
@media screen and (max-width: 580px) {
.tab-container {
    max-width: 100%;
}
.txt-pad-15 {
    padding: 0 15px 51px 15px !important;
}
.foo-txt {
    padding: 0px 15px 18px 15px !important;
}
.foo-add {
    padding: 15px 15px 0px 15px !important;
}
.foo-add-left {
    width: 100% !important;
}
.foo-add-right {
    width: 100% !important;
}
.pad-bottom-15 {
    padding-bottom: 15px !important;
}
.pad-20 {
    padding: 25px 20px 20px !important;
}
}
</style>
  </head>
  <body style="margin:0px;">
   <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="width: 850px;background-color: #fff;margin: 0 auto;table-layout: fixed;background:#dbdbdb;font-family:Arial, Helvetica, sans-serif;font-size:15px;">
    <tr>
      <td style="padding:20px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td style="text-align: left;width: 100%;padding-bottom:25px;background:#90c5bc;padding:20px;"><a href="https://cityfurnish.com/"><img src="http://180.211.99.165/design/cityfurnish/email/images/logo.png" alt="logo"></a></td>
        </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td style="text-align: left;width: 100%;background:#f5f5f5;padding:25px 30px 30px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="text-align: left;width: 100%;padding-bottom:10px;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-weight:bold;font-size:15px;color:#656565;">Hi '.strtoupper(stripslashes($shipAddRess->row()->full_name)).'</p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-size:15px;color:#656565">Thank you for your order.</p></td>
              </tr>
              </table>


            <!-- order and date table -->
              
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                <tr>
                  <td style="text-align: left;padding:15px 20px;width:50%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Id</strong><span style="font-weight:400;"> : #'.$PrdList->row()->dealCodeNumber.'</span> </p></td>
                  <td style="text-align: left;padding:15px 20px;border-left:1px solid #ddd;width:50%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Date </strong><span style="font-weight:400;"> : '.date("j F, Y g:i a",strtotime($PrdList->row()->created)).'</span> </p></td>
                </tr>
              </table>


                <!-- address table -->
              
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                <tr>
                  <td style="text-align: left;padding:15px 20px;width:100%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Shipping Details</strong> </p></td>
                </tr>
                <tr>
                  <td style="text-align: left;padding:15px 20px;border-top:1px solid #ddd;width:100%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Full Name</strong> <span style="font-weight:400;"> : '.stripslashes($shipAddRess->row()->full_name).'</span> </p>
                   <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Address</strong> <span style="font-weight:400;"> : '.stripslashes($new_ship_address).'</span> </p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> 
                    <strong style="font-weight:600;">City</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.stripslashes($shipAddRess->row()->city).'</span> <strong style="font-weight:600;">State</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.stripslashes($shipAddRess->row()->state).'</span> <strong style="font-weight:600;">Zip Code</strong> <span style="font-weight:400;display:inline-block;"> : '.stripslashes($shipAddRess->row()->postal_code).'</span> </p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Phone Number</strong> <span style="font-weight:400;"> : '.stripslashes($shipAddRess->row()->phone).'</span> </p></td>
                </tr>
              </table>




              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                <tr>
                  <td style="text-align: left;padding:15px 20px;width:100%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">From</strong> </p></td>
                </tr>
                <tr>
                  <td style="text-align: left;padding:15px 20px;border-top:1px solid #ddd;width:100%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Full Name</strong> <span style="font-weight:400;"> : '.stripslashes($PrdList->row()->full_name).'</span> </p>
                   <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Address</strong> <span style="font-weight:400;"> : '.stripslashes($new_address).'</span> </p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> 
                    <strong style="font-weight:600;">City</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.stripslashes($PrdList->row()->city).'</span> <strong style="font-weight:600;">State</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.stripslashes($PrdList->row()->state).'</span> <strong style="font-weight:600;">Zip Code</strong> <span style="font-weight:400;display:inline-block;"> : '.stripslashes($PrdList->row()->postal_code).'</span> </p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Phone Number</strong> <span style="font-weight:400;"> : '.stripslashes($PrdList->row()->phone_no).'</span> </p></td>
                </tr>
              </table>
              

                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;color:#656565;border-color:#ddd;">
                <tr>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:100px"> Product Images </td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:120px"> Product Name </td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:35px;">QTY</td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:120px">Security Deposite</td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:110px;">Rental</td>
                  <td align="center" style="color:#656565;padding:20px 5px;font-size:12px;font-weight:600;width:120px;"> Sub Total </td>
                 </tr>';       
            
$disTotal =0; $grantTotal = 0;
foreach ($PrdList->result() as $cartRow) { $InvImg = @explode(',',$cartRow->image);
$unitPrice = ($cartRow->price*(0.01*$cartRow->product_tax_cost))+$cartRow->price;
$unitDeposit =  $cartRow->product_shipping_cost;
$grandDeposit = $grandDeposit + ($unitDeposit*$cartRow->quantity);
$uTot = ($unitPrice + $unitDeposit)*$cartRow->quantity;
if($cartRow->attr_name != '' || $cartRow->attr_type != ''){ $atr = '<br>'.$cartRow->attr_type.' / '.$cartRow->attr_name; }else{ $atr = '';}

$message.='<tr>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid; font-size:11px;border-color:#ddd;"><img src="'.base_url().PRODUCTPATH.$InvImg[0].'" alt="'.stripslashes($cartRow->product_name).'" alt="product" width="60" /></td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;line-height:20px;border-color:#ddd;"> '.stripslashes($cartRow->product_name).$atr.'</td>
            <td align="center" style="padding:20px 0px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;"> '.strtoupper($cartRow->quantity).'</td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">'.$this->data['currencySymbol'].number_format($unitDeposit,2,'.','').'</td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">'.$this->data['currencySymbol'].number_format($unitPrice,2,'.','').'</td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;font-size:11px;border-color:#ddd;"> '.$this->data['currencySymbol'].number_format($uTot,2,'.','').' </td>
        </tr>';
    $grantTotal = $grantTotal + $uTot;
    $new_deposite +=  $unitDeposit;
    $new_rental +=  $unitPrice;
    $new_total += $uTot;

}
    $private_total = $grantTotal - $PrdList->row()->discountAmount;
    $private_total = $private_total + $PrdList->row()->tax;



                 
$message.='<tr>
                  <td align="right" colspan="3" style="padding:20px 20px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:12px;font-weight:600;border-color:#ddd;"> TOTAL </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">  '.$this->data['currencySymbol'].number_format($new_deposite,2,'.','').' </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">'.$this->data['currencySymbol'].number_format($new_rental,2,'.','').' </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">'.$this->data['currencySymbol'].   number_format($new_total,2,'.','').'</td>
                </tr>
              </table>
               <table border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;color:#656565;border-color:#ddd;">
                <tr>
                  <td align="left" style="color:#656565;padding:14px 20px 15px 20px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;padding-left:20px;"> Sub Total </td>
                  <td align="center" style="color:#656565;padding:15px 20px;font-size:11px;font-weight:600;">'.$this->data['currencySymbol'].   number_format($grantTotal,2,'.','').'</td>
                </tr>';

                if($PrdList->row()->couponCode != ''){

               $message.='<tr>
                  <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount <br />
                      <span style="font-size:9px !important;font-weight:400;">(Coupon Code : '.$PrdList->row()->couponCode.')</span></p></td>
                  <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">'.$this->data['currencySymbol'].' - '.number_format($PrdList->row()->discountAmount,'2','.','').'</td>
                </tr>';
            }
                $message.='<tr>
                  <td align="left" style="color:#656565;padding:14px 15px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-size:14px;font-weight:600;border-color:#ddd;padding-left:20px;">GRAND TOTAL</td>
                  <td align="center" style="color:#60a196;padding:15px 20px;border-top:1px solid #ddd;font-size:15px;font-weight:600;border-color:#ddd;">'.$this->data['currencySymbol'].number_format($private_total,'2','.','').'</td>
                </tr>
              </table>
                </td>
          </tr>
          <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
            <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
              <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
                Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
          </tr>
        </table></td>
    </tr>
  </table>
</body>
</html>';
              


// <div style="width:1012px; margin:0 auto;font-size:12px;font-family: Arial, Helvetica, sans-serif;">
// <center><h4 style="margin:0 auto;">ORDER CONFIRMATION</h4></center>
// <div style="width:100%; border:1px solid #000; float:left; margin:0 auto;">
//     <div style="float:left; width:30%;"><a href="' . base_url() . '" target="_blank" id="logo"><img src="' . base_url() . 'images/logo/cityfurnish-transparent.png" alt="' . $this->data['WebsiteTitle'] . '" title="' . $this->data['WebsiteTitle'] . '" style=" height:43px; margin: 10px 10px 10px 10px;"></a></div>
// 	<div style="float:left; width:70%;">
// 		<div style="width:380px; float:right;">
// 		<table>
//         	<tr>
//         		<td>Rented From : </td>
//         		<td>' . stripslashes($PrdList->row()->full_name) . '</td>
//         	</tr>
//         	<tr>
//         		<td>GST No. :</td>
//         		<td>' . stripslashes($PrdList->row()->s_tin_no) . '/' . stripslashes($PrdList->row()->s_vat_no) . '/' . stripslashes($PrdList->row()->s_cst_no) . '</td>
//         	</tr>
        	
//     	</table>
// 		</div>
// 	</div>
// </div>			
// <!--END OF LOGO-->
    
//  <!--start of deal-->
//     <div style="width:972px;background:#FFFFFF;float:left; padding:20px; border:1px solid #454B56; ">
// 	<div style="float:left; width:100%; margin-bottom:20px; margin-right:7px;">
// 		<div style="float:left; width:49%; border:1px solid #cccccc;">
// 		<span style=" border-bottom:1px solid #cccccc; background:#f3f3f3; width:95.8%; float:left; padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#000305;">If Undelivered Return to :</span>
//         <table style="width:100%; float:left;">
//         	<tr><td>Full Name</td><td>:</td><td>' . stripslashes($PrdList->row()->full_name) . '</td></tr>
//             <tr><td style="vertical-align: baseline;width: 100px;">Address</td><td style="vertical-align: baseline;">:</td><td>' . stripslashes($PrdList->row()->address) . ',' . stripslashes($PrdList->row()->address2) . ',' . stripslashes($PrdList->row()->city) . ',' . stripslashes($PrdList->row()->state) . ',' . stripslashes($PrdList->row()->postal_code) . '</td></tr>
// 			<!--<tr><td>Address 2</td><td>:</td><td>' . stripslashes($PrdList->row()->address2) . '</td></tr>
// 			<tr><td>City</td><td>:</td><td>' . stripslashes($PrdList->row()->city) . '</td></tr>
// 			<tr><td>Country</td><td>:</td><td>' . stripslashes($PrdList->row()->country) . '</td></tr>
// 			<tr><td>State</td><td>:</td><td>' . stripslashes($PrdList->row()->state) . '</td></tr>
// 			<tr><td>Zipcode</td><td>:</td><td>' . stripslashes($PrdList->row()->postal_code) . '</td></tr>
// 			<tr><td>Phone Number</td><td>:</td><td>' . stripslashes($PrdList->row()->phone_no) . '</td></tr>-->
//     	</table>
//     	</div>
//     	<div style="float:right; width:40%;">
// 		<table style="width:100%; border:1px solid #cecece; float:left;">
// 		  	<tr bgcolor="#f3f3f3">
//             	<td width="100"  style="border-right:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Order Id</span></td>
// 	            <td  width="100"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">#' . $PrdList->row()->dealCodeNumber . '</span></td>
// 	        </tr>
// 	        <tr bgcolor="#f3f3f3">
//                 <td width="100"  style="border-right:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Order Date</span></td>
//                 <td  width="100"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . date("F j, Y g:i a", strtotime($PrdList->row()->created)) . '</span></td>
//             </tr>	 
//         </table>
//         </div>
//     </div>
		
//     <div style="float:left; width:100%;">
	
//     <div style="width:49%; float:left; border:1px solid #cccccc; margin-right:10px;">
//     	<span style=" border-bottom:1px solid #cccccc; background:#f3f3f3; width:95.8%; float:left; padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#000305;">Shipping Address</span>
//     		<div style="float:left; padding:10px; width:96%;  font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#030002; line-height:28px;">
//             	<table width="100%" border="0" cellpadding="0" cellspacing="0">
//                 	<tr><td>Full Name</td><td>:</td><td>' . stripslashes($shipAddRess->row()->full_name) . '</td></tr>
//                     <tr><td style="vertical-align: baseline;width: 100px;">Address</td><td style="vertical-align: baseline;">:</td><td>' . stripslashes($shipAddRess->row()->address1) . ', ' . stripslashes($shipAddRess->row()->address2) . ', ' . stripslashes($shipAddRess->row()->city) . ', ' . stripslashes($shipAddRess->row()->state) . ', ' . stripslashes($shipAddRess->row()->postal_code) . ', ' . stripslashes($shipAddRess->row()->country) . '</td></tr>
// 					<!--<tr><td>Address 2</td><td>:</td><td>' . stripslashes($shipAddRess->row()->address2) . '</td></tr>
// 					<tr><td>City</td><td>:</td><td>' . stripslashes($shipAddRess->row()->city) . '</td></tr>
// 					<tr><td>Country</td><td>:</td><td>' . stripslashes($shipAddRess->row()->country) . '</td></tr>
// 					<tr><td>State</td><td>:</td><td>' . stripslashes($shipAddRess->row()->state) . '</td></tr>
// 					<tr><td>Zipcode</td><td>:</td><td>' . stripslashes($shipAddRess->row()->postal_code) . '</td></tr> -->
// 					<tr><td style="vertical-align: baseline;">Phone Number</td><td>:</td><td>' . stripslashes($shipAddRess->row()->phone) . '</td></tr>
//             	</table>
//             </div>
//      </div>
    
//     <div style="width:49%; float:left; border:1px solid #cccccc;">
//     	<span style=" border-bottom:1px solid #cccccc; background:#f3f3f3; width:95.7%; float:left; padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#000305;">From :</span>
//     		<div style="float:left; padding:10px; width:96%;  font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#030002; line-height:28px;">
//             	<table width="100%" border="0" cellpadding="0" cellspacing="0">
//                 	<tr><td>Full Name</td><td>:</td><td>' . stripslashes($PrdList->row()->full_name) . '</td></tr>
//                     <tr><td style="vertical-align: baseline;width: 100px;">Address</td><td style="vertical-align: baseline;">:</td><td>' . stripslashes($PrdList->row()->address) . ',' . stripslashes($PrdList->row()->address2) . ',' . stripslashes($PrdList->row()->city) . ',' . stripslashes($PrdList->row()->state) . ',' . stripslashes($PrdList->row()->postal_code) . ',' . stripslashes($PrdList->row()->country) . '</td></tr>
// 					<!--<tr><td>Address 2</td><td>:</td><td>' . stripslashes($PrdList->row()->address2) . '</td></tr>
// 					<tr><td>City</td><td>:</td><td>' . stripslashes($PrdList->row()->city) . '</td></tr>
// 					<tr><td>Country</td><td>:</td><td>' . stripslashes($PrdList->row()->country) . '</td></tr>
// 					<tr><td>State</td><td>:</td><td>' . stripslashes($PrdList->row()->state) . '</td></tr>
// 					<tr><td>Zipcode</td><td>:</td><td>' . stripslashes($PrdList->row()->postal_code) . '</td></tr>
// 					<tr><td>Phone Number</td><td>:</td><td>' . stripslashes($PrdList->row()->phone_no) . '</td></tr>-->
//             	</table>
//             </div>
//     </div>
// </div> 
	   
// <div style="float:left; width:100%; margin-right:3%; margin-top:10px; font-size:12px; font-weight:normal; line-height:28px;  font-family:Arial, Helvetica, sans-serif; color:#000; overflow:hidden;">   
// 	<table width="100%" border="0" cellpadding="0" cellspacing="0">
//     <tr>
//     	<td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cecece; width:99.5%;">
//         <tr bgcolor="#f3f3f3">
//         	<td width="16%" style="border-right:1px solid #cecece; text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Product Image</span></td>
//         	<td width="9%" style="border-right:1px solid #cecece; text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Product ID</span></td>
//         	<td width="9%" style="border-right:1px solid #cecece; text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">SKU Code</span></td>
//             <td width="27%" style="border-right:1px solid #cecece;text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Product Name</span></td>
//             <td width="7%" style="border-right:1px solid #cecece;text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Qty</span></td>
//             <td width="10%" style="border-right:1px solid #cecece;text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Unit Deposit</span></td>
//             <td width="10%" style="border-right:1px solid #cecece;text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Unit Rental</span></td>
//             <td width="12%" style="text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Sub Total</span></td>
//          </tr>';

//         $disTotal = 0;
//         $grantTotal = 0;
//         foreach ($PrdList->result() as $cartRow) {
//             $InvImg = @explode(',', $cartRow->image);
//             $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->price;
//             $unitDeposit = $cartRow->product_shipping_cost;
//             $grandDeposit = $grandDeposit + ($unitDeposit * $cartRow->quantity);
//             $uTot = ($unitPrice + $unitDeposit) * $cartRow->quantity;
//             if ($cartRow->attr_name != '' || $cartRow->attr_type != '') {
//                 $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
//             } else {
//                 $atr = '';
//             }

//             $message .= '<tr>
//             <td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" /></span></td>
//             <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . stripslashes($cartRow->seller_product_id) . '</span></td>
//             <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . stripslashes($cartRow->sku) . '</span></td>
// 			<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . stripslashes($cartRow->product_name) . $atr . '</span></td>
//             <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . strtoupper($cartRow->quantity) . '</span></td>
//             <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($unitDeposit, 2, '.', '') . '</span></td>
//             <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '</span></td>
//             <td style="text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($uTot, 2, '.', '') . '</span></td>
//         </tr>';
//             $grantTotal = $grantTotal + $uTot;
//         }
//         $private_total = $grantTotal - $PrdList->row()->discountAmount;
//         $private_total = $private_total + $PrdList->row()->tax;

//         $message .= '</table></td> </tr><tr><td colspan="3"><table border="0" cellspacing="0" cellpadding="0" style=" margin:10px 0px; width:99.5%;"><tr>
// 			<td width="460" valign="top" >';
//         if ($PrdList->row()->note != '') {
//             $message .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
//                 <td width="87" ><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
               
//             </tr>
// 			<tr>
//                 <td width="87"  style="border:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($PrdList->row()->note) . '</span></td>
//             </tr></table>';
//         }


//         if ($PrdList->row()->order_gift == 1) {
//             $message .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
//                 <td width="87"  style="border:1px solid #cecece;"><span style="font-size:14px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
//             </tr></table>';
//         }

//         $message .= '</td>
//             <td width="174" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cecece;">
//             <tr bgcolor="#f3f3f3">
//                 <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Sub Total</span></td>
//                 <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</span></td>
//             </tr>';
//         if ($PrdList->row()->couponCode != '') {
//             $message .= '	<tr>
//                 <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Coupon Code Used</span></td>
//                 <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $PrdList->row()->couponCode . '</span></td>
//             </tr>';
//         }
//         $message .= '<tr>
//                 <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Discount Amount</span></td>
//                 <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->discountAmount, '2', '.', '') . '</span></td>
//             </tr>
// 		<tr bgcolor="#f3f3f3">
//             <td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Deposit</span></td>
//                 <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($grandDeposit, 2, '.', '') . '</span></td>
//               </tr>
// 			  <!--<tr>
//             	<td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Shipping Tax</span></td>
//                 <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->tax, 2, '.', '') . '</span></td>
//               </tr>-->
// 			  <tr bgcolor="#f3f3f3">
//                 <td width="87" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">Grand Total</span></td>
//                 <td width="31" style="border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</span></td>
//               </tr>
//               <tr>
//                 <td width="87" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">Amount Paid</span></td>
//                 <td width="31" style="border-bottom:1px solid #cecece;"><span style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;font-weight:800">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</span></td>
//               </tr>
//             </table></td>
//             </tr>
//         </table></td>
//         </tr>
//     </table>
//         </div>
        
//         <!--end of left--> 
//         <div style="width:100%; margin-right:5px; float:left; border-bottom:1px dashed #575757;">
// 	        <p>
// 		        <p> Have a question? Our customer service is here to help you -  +91- 80108 45 000</p>
//                         <p> Price mentioned are inclusive of applicable Govt. taxes</p>
//                         <br /></br>
// 	        </p>
//         </div>
//         <div style="clear:both"></div>
//     </div>
//     </div>
// <div style="float:left; width:100%; margin-right:3%; margin-top:10px; font-size:12px; font-weight:normal; line-height:28px;  font-family:Arial, Helvetica, sans-serif; color:#000; overflow:hidden;">
// </div>
// </div></body></html>';

        return $message;
    }

    public function getCurrentSuccessOrders($userid, $dealCode) {

        $this->db->select('p.*,u.email,u.full_name,u.address,u.address2,u.phone_no,u.postal_code,u.state,u.country,u.city,u.s_tin_no,u.s_vat_no,u.s_cst_no,pd.product_name,pd.id as PrdID,pd.image,pd.seller_product_id,pd.sku,pAr.attr_name as attr_type,sp.attr_name,sh.address1 as ship_address1,sh.address2 as ship_address2, sh.city as ship_city, sh.state as ship_state, sh.postal_code as ship_postal_code, sh.country as ship_country, sh.phone as ship_phone, sh.full_name as ship_full_name');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
        $this->db->join(SHIPPING_ADDRESS . ' as sh', 'sh.id=p.shippingid', 'left');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $dealCode . '" and p.status="Paid"');

        $currentOrderDetails = $this->db->get();
        //echo "query: " . $this->db->last_query();
        return $currentOrderDetails->result_array();
    }

}

?>