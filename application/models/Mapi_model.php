<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * This model contains all db functions related to product management
 * @author Teamtweaks
 *
 */
class Mapi_model extends My_Model {

    public function get_user_details($condition){
        $this->db->select('user.id,user.email,user.full_name,user.user_type,user.phone_no');
        // $this->db->from(PARTNER_USERS.' as sb');
        $this->db->from(USERS.' as user');
        $this->db->where($condition);
        $Data = $this->db->get();
        return $Data;
    }

    public function check_details_exists($data){
        $this->db->select('*');
        $this->db->from(USERS);
        $this->db->or_where('email',$data['email']);
        $this->db->or_where('phone_no',$data['mobile_number']);
        // $this->db->where('user_type != ','');
        $data = $this->db->get();
        return $data;
    }

    public function get_leads($count = NULL,$length = 10,$start = NULL,$condition,$status = 'all',$searchValue = null,$is_bd_login = null){

        $this->db->DISTINCT();
        if($count != NULL){ 
          $this->db->select( 'count(lead.id) as count' , FALSE);
        }else{
            $this->db->select('lead.*');
            $this->db->limit($length, $start);
        } 
        $this->db->from(BD_LEADS.' as lead'); 
        $this->db->join(PARTNER_USERS.' as partner','lead.bd_user_id = partner.user_id');
        $this->db->order_by('lead.created','desc');
        // $this->db->join(USERS.' as user','partner.user_id = user.id');
        if($status != 'all'){
            if($status == 'notconverted'){
                $status = 'not converted';   
            }
            $this->db->where('lead.lead_status',$status);   
        }
        
        $this->db->where($condition);
        if($is_bd_login != null){
            $this->db->or_where('partner.bdexecuter_id',$is_bd_login);
            if($status != 'all'){
                $this->db->where('lead.lead_status',$status);   
            }
            // $this->db->where($condition);
        }
        // $this->db->where($condition);
        if($searchValue != NULL && $searchValue != ''){
            $sortQry = "(lead.first_name LIKE '%$searchValue%' or lead.email LIKE '%$searchValue%' or lead.phone_no LIKE '%$searchValue%'  or  lead.city LIKE '%$searchValue%')";
            $this->db->where( $sortQry );
        }
        // print_r("expression");exit;  
        $query = $this->db->get();
        // print_r($this->db->last_query());exit;
        $result=$query->result();
        return $result;

    }


    public function check_lead_exists($table,$email,$phone){
        $this->db->select('*');
        $this->db->from($table);
        if($email != '' ||$email != NULL ){
            $this->db->or_where('email',$email);
        }
        $this->db->or_where('phone_no',$phone);
        $data = $this->db->get();
        return $data;
    }

    public function get_my_profile($user_id){
        $this->db->select('user.full_name,user.email,user.phone_no,partner.profile_pic,partner.poi,partner.employee_id,partner.visiting_card,partner.bank_details,partner.cancelled_cheque,user.user_type');
        $this->db->from(USERS.' as user');
        $this->db->join(PARTNER_USERS.' as partner','partner.user_id = user.id');
        $this->db->where('user.id',$user_id);
        $data = $this->db->get();
        return $data;
    }

    public function get_my_earning($status,$user_id){
        $this->db->select('COALESCE(sum(cm.commission_rate), 0 ) as total_earning ');
        $this->db->from(BD_LEAD_COMMISSION.' as cm');
        $this->db->join(BD_LEADS.' as lead','cm.lead_id = lead.id');
        $this->db->where('lead.bd_user_id',$user_id);
        $this->db->where('cm.status',$status);
        $this->db->where('lead.lead_status','delivered');
        $data = $this->db->get();
        return $data->row()->total_earning;
    }

    public function get_top_ranking(){
        $this->db->select('sum(cm.commission_rate) as total_earning,count(lead.id) as orders,user.full_name,user.email,partner.profile_pic,user.id');
        $this->db->from(BD_LEAD_COMMISSION.' cm');
        $this->db->join(BD_LEADS.' as lead','lead.id = cm.lead_id');
        $this->db->join(USERS.' as user','user.id = lead.bd_user_id');
        $this->db->join(PARTNER_USERS.' as partner','user.id = partner.user_id');
        $this->db->order_by('orders','desc');
        $this->db->where('lead.lead_status','delivered');
        $this->db->order_by('total_earning','desc');
        $this->db->group_by('user.id');
        $this->db->limit(20);
        $data = $this->db->get();
        return $data;
    }

    //get bd executer user
    public function get_all_bdexecuter_users($bd_user_id,$offset = 0,$limit = 10){
        $this->db->select('COALESCE(count(lead.bd_user_id), 0 ) as total_lead, user.id as user_id,partner.profile_pic,user.full_name,user.email,user.phone_no,lead.lead_status');
        $this->db->from(PARTNER_USERS.' as partner');
        $this->db->join(USERS.' as user','partner.user_id = user.id','left');
        $this->db->join(BD_LEADS.' as lead','user.id = lead.bd_user_id','left');
        $this->db->where('partner.bdexecuter_id',$bd_user_id);
        // $this->db->where('lead.lead_status','delivered');
        $this->db->group_by('partner.user_id');
        $this->db->limit($limit, $offset);
        $data = $this->db->get();
        return $data;
    }

    public function get_delivered_lead_count($user_id){
        $this->db->select('COALESCE(count(bd_user_id), 0 ) as total_lead');
        $this->db->from(BD_LEADS);
        $this->db->where('bd_user_id',$user_id);
        $this->db->where('lead_status','delivered');
        $data = $this->db->get();
        return $data->row();

    }

    //get porudct listing 
    public function searchProductListing($whereCond,$limit,$offset){
        $sel = 'select p.id,p.product_name,p.image,p.price from ' . PRODUCT . ' p
                LEFT JOIN ' . USERS . ' u on u.id=p.user_id ' . $whereCond . ' limit '.$offset.', '.$limit.'';
        return $this->ExecuteQuery($sel);

    }

    public function get_cat_name($id) {
        $this->db->select('cat_name,seourl');
        $this->db->from('fc_category');
        $this->db->where('id', $id);
        $data = $this->db->get();
        return $data;
    }

    public function getCategoryValues($selVal, $whereCond) {
        $sel = 'select ' . $selVal . ' from ' . CATEGORY . ' c LEFT JOIN ' . CATEGORY . ' sbc ON c.id = sbc.rootID ' . $whereCond . ' ';
        return $this->ExecuteQuery($sel);
    }

    public function getParentCategories($rootID) {
        $sel = 'select * from ' . CATEGORY . ' where rootID = ' . $rootID . ' ';
        return $this->ExecuteQuery($sel);
    }

    public function view_subproduct_details_join($prdId = '') {
        $select_qry = "select a.attr_name,a.attr_price from " . SUBPRODUCT . " a join " . PRODUCT_ATTRIBUTE . " b on a.attr_id = b.id where a.product_id = '" . $prdId . "'";
         $attList = $this->ExecuteQuery($select_qry);
         return $attList->result_array();
    }

      //get bd executive revenure
    public function get_my_revenue($user_id,$filter_date = NULL){
        // print_r($user_id);exit;
        $this->db->select('SUM(payment.price) as rental_amount');
        $this->db->from(BD_LEADS.' as lead');
        $this->db->join(PAYMENT.' as payment','payment.dealCodeNumber = lead.order_id');
        $this->db->where('lead.bd_user_id',$user_id);

        $this->db->where('lead.lead_status','delivered');
        if($filter_date != NULL){
            $this->db->where("month(payment.created)",$filter_date['month']);
            $this->db->where("year(payment.created)",$filter_date['year']);
            
        }
        $data = $this->db->get();
        return $data;
    }

    public function get_bdexecuter_users($bduser_id){
        $this->db->select('GROUP_CONCAT(user_id SEPARATOR  ",") as user_id');
        $this->db->from(PARTNER_USERS);
        $this->db->where('bdexecuter_id',$bduser_id);
        $data = $this->db->get();
        return $data;
    }

    public function get_broker_revenue($user_ids,$filter_date = NULL){
        $userIds = explode(',', $user_ids->user_id);
        $this->db->select('SUM(payment.price) as rental_amount');
        $this->db->from(BD_LEADS.' as lead');
        $this->db->join(PAYMENT.' as payment','payment.dealCodeNumber = lead.order_id');
        $this->db->where_in('lead.bd_user_id',$userIds);
        $this->db->where('lead.lead_status','delivered');       
        if($filter_date != NULL){
            $this->db->where("month(payment.created)",$filter_date['month']);
            $this->db->where("year(payment.created)",$filter_date['year']);
            
        }
        $data = $this->db->get();
        return $data;
    }

    public function get_my_order_count($bd_user_id,$filter_date = NULL){
        $this->db->select('payment.id as total_order');
        $this->db->from(BD_LEADS.' as lead');
        $this->db->join(PAYMENT.' as payment','payment.dealCodeNumber = lead.order_id');
        $this->db->where('lead.bd_user_id',$bd_user_id);
        $this->db->group_by('payment.dealCodeNumber');
        $this->db->where('lead.lead_status','delivered');
        if($filter_date != NULL){
            $this->db->where("month(payment.created)",$filter_date['month']);
            $this->db->where("year(payment.created)",$filter_date['year']);
            
        }
        $data = $this->db->get();
        return $data;

    }


    public function get_broker_order_count($user_ids,$filter_date = NULL){
        $userIds = explode(',', $user_ids->user_id);
        $this->db->select('payment.id as total_order');
        $this->db->from(BD_LEADS.' as lead');
        $this->db->join(PAYMENT.' as payment','payment.dealCodeNumber = lead.order_id');
        $this->db->where_in('lead.bd_user_id',$userIds);
        $this->db->group_by('payment.dealCodeNumber');
        $this->db->where('lead.lead_status','delivered');       
        if($filter_date != NULL){
            $this->db->where("month(payment.created)",$filter_date['month']);
            $this->db->where("year(payment.created)",$filter_date['year']);
            
        }
        $data = $this->db->get();
        return $data;
    }

    public function get_todays_order($bd_user_id,$date = NULL){
        if($date == NULL){
            $date = date('Y-m-d'); 
        }else{
            $date =  date('Y-m-d',strtotime($date));
        }
        $this->db->select('user_payment.full_name as customer_name,user.full_name as broker_name,user_payment.phone_no as customer_phone, user.phone_no as broker_phone, user_payment.email as cutomer_email , user.email as broker_email,payment.created');
        $this->db->from(PARTNER_USERS.' as p_user');
        $this->db->join(BD_LEADS.' as lead','p_user.user_id = lead.bd_user_id');
        $this->db->join(PAYMENT.' as payment','lead.order_id = payment.dealCodeNumber');
        $this->db->join(USERS.' as user_payment','payment.user_id = user_payment.id');
        $this->db->join(USERS.' as user','lead.bd_user_id = user.id');
        $this->db->where('bdexecuter_id',$bd_user_id);
        $this->db->where('lead.order_id != ','');
        $this->db->where('lead.lead_status','converted');
        $this->db->where("DATE_FORMAT(payment.created ,'%Y-%m-%d') = '$date'");
        $this->db->group_by('payment.dealCodeNumber');
        $this->db->order_by('payment.created','desc');
        $data = $this->db->get();
        return $data;
    }


}
