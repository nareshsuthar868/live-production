<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to Cart Page
 * @author Teamtweaks
 *
 */
class Order_model extends My_Model {

    public function add_order($dataArr = '') {
        $this->db->insert(PRODUCT, $dataArr);
    }

    public function edit_order($dataArr = '', $condition = '') {
        $this->db->where($condition);
        $this->db->update(PRODUCT, $dataArr);
    }

    public function view_order($condition = '') {
        return $this->db->get_where(PRODUCT, $condition);
    }

    // public function view_order_details($status) {
    //     $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.id as PrdID,u.id as uid');
    //     $this->db->from(PAYMENT . ' as p');
    //     $this->db->join(USERS . ' as u', 'p.user_id = u.id');
    //     $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
    //     $this->db->where('p.status = "' . $status . '"');
    //     $this->db->group_by("p.dealCodeNumber");
    //     $this->db->order_by("p.created", "desc");
    //     $this->db->limit(100,0);
    //     $PrdList = $this->db->get();

    //     //echo '<pre>'; print_r($PrdList->result()); die;
    //     return $PrdList;
    // }
    
    public function view_order_details($status,$count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL,$offline_order = 0) {

        $column_order = array('p.id','u.email',null,'p.created',null,null,null,'p.status'); //set column field database for datatable orderable
        $this->db->DISTINCT();
        if($count != NULL){ 
          $this->db->select( 'count(p.id) as count' , FALSE);
        }else{
        $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.id as PrdID,u.id as uid,or.advance_rental');
            $this->db->limit($length, $start);
            $this->db->group_by("p.dealCodeNumber");
            $this->db->order_by('p.created','desc' );
        }    
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->join(OFFLINE_ORDERS.' as or','p.dealCodeNumber = or.dealCodeNumber','left');
        if($status != '' || $status != null){
            $this->db->where('p.status = "' . $status . '"');
            $this->db->where('p.is_offline_placed','0');
        }
        if($offline_order == 1 || $offline_order == '1'){
            $this->db->where('p.is_offline_placed','1');
        }
        if($searchValue != NULL && $searchValue != ''){
            $sortQry = "(u.email LIKE '%$searchValue%' or p.id LIKE '%$searchValue%' or p.dealCodeNumber LIKE '%$searchValue%' or p.couponCode LIKE '%$searchValue%' or p.total LIKE '%$searchValue%' or p.shippingcity LIKE '%$searchValue%')";
            $this->db->where( $sortQry );
        }
        $query = $this->db->get();
        $result=$query->result();
        return $result;
    }

    
    // public function view_bulk_orders() {
    //   $this->db->select('id,name,email,phone,city,message');
    //   $this->db->from('fc_bulk_order');
    //   $data = $this->db->get();
    //   return $data;
    // }
    
    public function view_leads_request() {
       $this->db->select('*');
       $this->db->from(LEAD_REQUEST);
       $data = $this->db->get();
       return $data;
    }
    public function delete_bulk_order($id){
        $this->db->where('id',$id);
     return   $this->db->delete('fc_bulk_order');
    }

    /*     * ********************************************* Payment Success Cart******************************************************* */

     public function PaymentSuccess($userid = '', $randomId = '', $transId = '', $payerMail = '',$card_token = '',$mendate_id = '') {


        $paymtdata = array(
            'randomNo' => $randomId,
            'fc_session_user_id' => $userid,
        );
        $this->session->set_userdata($paymtdata);

        $conditionCheck = array('user_id' => $userid, 'dealCodeNumber' => $randomId, 'status' => 'Paid');
        $statusCheck = $this->order_model->get_all_details(PAYMENT, $conditionCheck);

        if ($statusCheck->num_rows() == 0) {

            $CoupRes = $this->order_model->get_all_details(SHOPPING_CART, array('user_id' => $userid, 'couponID >' => 0));

            $couponID = $CoupRes->row()->couponID;
            $couponAmont = $CoupRes->row()->discountAmount;
            $couponType = $CoupRes->row()->coupontype;

            // Update Coupon
            if ($couponID != 0) {
                if ($couponType == 'Gift') {
                    $SelGift = $this->order_model->get_all_details(GIFTCARDS, array('id' => $couponID));
                    $GiftCountValue = $SelGift->row()->used_amount + $couponAmont;
                    $condition = array('id' => $couponID);
                    $dataArr = array('used_amount' => $GiftCountValue);
                    $this->order_model->update_details(GIFTCARDS, $dataArr, $condition);
                    if ($SelGift->row()->price_value <= $GiftCountValue) {

                        $condition1 = array('id' => $couponID);
                        $dataArr1 = array('card_status' => 'redeemed');
                        $this->order_model->update_details(GIFTCARDS, $dataArr1, $condition1);
                    }
                } else {
                    $SelCoup = $this->order_model->get_all_details(COUPONCARDS, array('id' => $couponID));
                    // $CountValue = $SelCoup->row()->purchase_count + 1;
                    // $condition = array('id' => $couponID);
                    // $dataArr = array('purchase_count' => $CountValue);
                    // $this->order_model->update_details(COUPONCARDS, $dataArr, $condition);
                    if(isset($SelCoup->row()->code)){
                        $CountValue = $SelCoup->row()->purchase_count + 1;
                        $condition = array('id' => $couponID);
                        $dataArr = array('purchase_count' => $CountValue);
                        $this->order_model->update_details(COUPONCARDS, $dataArr, $condition);
                    }else{
                        $conditionCheck = array('user_id' => $userid, 'dealCodeNumber' => $randomId);
                        $GetDiscount = $this->order_model->get_all_details(PAYMENT, $conditionCheck);
                        $SelReferral = $this->order_model->get_all_details(REFERRAL_CODE, array('id' => $couponID));
                        $CountValue = $SelReferral->row()->count - 1;
                        $array_old = array('user_id' => $userid, 'amount' =>  $GetDiscount->row()->discountAmount);
                        if(!empty(unserialize($SelReferral->row()->used_by))){
                            $new_array123 = unserialize($SelReferral->row()->used_by);
                            $arraaaaa   =  array_push($new_array123, $array_old);
                        }else{
                            $new_array123[0] = $array_old;
                        }
                        $condition = array('id' => $couponID);
                        $dataArr = array('count' => $CountValue,'used_by' => serialize($new_array123));
                        $this->send_referral_success_mail($SelReferral->row()->id,$userid,$randomId);
                    }
                }
            }

            //Update Payment Table	
            $condition1 = array('user_id' => $userid, 'dealCodeNumber' => $randomId);
            if ($payerMail != '') {
                $dataArr1 = array('status' => 'Paid', 'shipping_status' => 'Processed', 'paypal_transaction_id' => $transId, 'payer_email' => $payerMail, 'payment_type' => 'Paypal','card_token' => $card_token ,'mandate_id' => $mendate_id,'zoho_status' => 'New Order','zoho_sub_status' => 'KYC In Progress' );
            } else {

                $dataArr1 = array('status' => 'Paid', 'shipping_status' => 'Processed', 'paypal_transaction_id' => $transId, 'payment_type' => 'Credit Cart','card_token' => $card_token ,'mandate_id' => $mendate_id,'zoho_status' => 'New Order','zoho_sub_status' => 'KYC In Progress' );
            }

            $this->order_model->update_details(PAYMENT, $dataArr1, $condition1);

            //Update Quantity
            $SelQty = $this->order_model->get_all_details(PAYMENT, array('user_id' => $userid, 'dealCodeNumber' => $randomId));

            foreach ($SelQty->result() as $updPrdRow) {

                $SelPrd = $this->order_model->get_all_details(PRODUCT, array('id' => $updPrdRow->product_id));
                $PrdCount = $SelPrd->row()->purchasedCount + $updPrdRow->quantity;

                /* Code added by Manish - 4/9/2019 - Start */
                $cityQuantity = $this->order_model->get_all_details('fc_city_product_quantity', array('city_id' => $_SESSION['prcity'], 'product_id' => $updPrdRow->product_id));
                $quantity = $cityQuantity->row()->quantity - $updPrdRow->quantity;
                $quantity = ($quantity > 0) ? $quantity : '0';

                $this->db->where('city_id = '.$_SESSION['prcity'].' and product_id = '.$updPrdRow->product_id);
                $this->db->update('fc_city_product_quantity',array('quantity' => $quantity));
                /* Code added by Manish - 4/9/2019 - End */

                //$productCount = $SelPrd->row()->quantity - $updPrdRow->quantity;
                $condition2 = array('id' => $updPrdRow->product_id);
                $dataArr2 = array('purchasedCount' => $PrdCount); //'quantity' => $productCount,
                $this->order_model->update_details(PRODUCT, $dataArr2, $condition2);
            }


            //Send Mail to User

            $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
            $this->db->from(PAYMENT . ' as p');
            $this->db->join(USERS . ' as u', 'p.user_id = u.id');
            $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
            $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
            $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
            $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
            $PrdList = $this->db->get();

            // var_dump($PrdList->result());exit;

            $this->db->select('p.sell_id,p.couponCode,u.email');
            $this->db->from(PAYMENT . ' as p');
            $this->db->join(USERS . ' as u', 'p.sell_id = u.id');
            $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
            $this->db->group_by("p.sell_id");
            $SellList = $this->db->get();
            
            $this->set_commision($PrdList->row());
            $this->SendMailUSers($PrdList, $SellList);
            

            //Empty Cart Info
            $condition3 = array('user_id' => $userid);
            $this->order_model->commonDelete(SHOPPING_CART, $condition3);
        }

        $paymtdata = array('randomNo' => '');
        $this->session->set_userdata($paymtdata);

        echo 'Success';
    }
    
    public function set_commision($paymentsData){
        $this->db->select('*');
        $this->db->from(BD_LEADS);
        if($paymentsData->email != ''){
            $this->db->or_where('email',$paymentsData->email);
        }
        $this->db->or_where('phone_no',$paymentsData->phone_no);
        $this->db->where('lead_status','pending');
        // $this->db->order_by('created','asc');
        $lead_matching = $this->db->get();
        // $lead_matching = $this->order_model->get_all_details(BD_LEADS,array('email' => $paymentsData->email,'phone_no' => $paymentsData->phone_no,'lead_status' => 'pending'));
        if($lead_matching->num_rows() > 0){
            $user_details = $this->order_model->get_all_details(USERS,array('id' => $paymentsData->user_id));
            // if(strtotime($user_details->row()->created) > strtotime($lead_matching->row()->created)){
                $commission_details = $this->order_model->get_all_details(COMISSIONS,array('lower(tenure)' => strtolower($paymentsData->attr_name)));
                if($commission_details->num_rows() > 0){

                    $total_discount = $paymentsData->discountAmount + $paymentsData->discount_on_si;
                    $my_earning = ((($paymentsData->total - $paymentsData->shippingcost ) + $total_discount )  * $commission_details->row()->comission ) / 100;
                    $this->order_model->update_details(BD_LEADS, array('order_id' => $paymentsData->dealCodeNumber,'lead_status' => 'converted'), array('id' => $lead_matching->row()->id));
                    $insert_data['lead_id'] = $lead_matching->row()->id;
                    $insert_data['commission_percentage'] = $commission_details->row()->comission;
                    $insert_data['commission_rate'] = $my_earning;
                    $insert_data['status'] = 'pending';
                    $this->send_notification($lead_matching->row()->bd_user_id,$user_details->row(),$paymentsData->dealCodeNumber,$paymentsData->created);
                    $this->order_model->simple_insert(BD_LEAD_COMMISSION, $insert_data);
                }
            // }
        }
    }
    
    public function send_notification($user_id,$user_details,$order_id,$order_date){
        $bd_user_id = $this->order_model->get_all_details(PARTNER_USERS,array('user_id' => $user_id));
        if($bd_user_id->num_rows() > 0){
            $bdexecuter_details = $this->order_model->get_all_details(PARTNER_USERS,array('user_id' => $bd_user_id->row()->bdexecuter_id,'device_token !=' => '' ));
            if($bdexecuter_details->num_rows() > 0){
                $lead_owner_name = $this->order_model->get_all_details(USERS,array('id' => $user_id));
                $message = 'Order #'.$order_id.' placed by lead '.ucfirst($user_details->full_name).' : '.$user_details->phone_no.' on '.date( "d-m-Y", strtotime($order_date)).' submitted by partner '.ucfirst($lead_owner_name->row()->full_name);
                // $message = 'Mr. '.ucfirst($user_details->full_name).' placed order '.$order_id.'('.$user_details->phone_no.')';
                $this->order_model->send_app_notification($bdexecuter_details->row()->device_token,$message);
                $this->order_model->simple_insert(BD_NOTIFICATION, array('user_id' => $bdexecuter_details->row()->user_id,'message' =>$message ));
            }
        }
    }
    
    
    
    public function send_referral_success_mail($coupon_id,$user_id,$order_id) {
        $user_details = $this->order_model->get_all_details(USERS,array('id' => $user_id));
        $this->db->select('user.email,user.user_name,referral.referral_code,referral.referral_amount');
        $this->db->from(REFERRAL_CODE . ' as referral');
        $this->db->join(USERS . ' as user', 'user.id = referral.user_id');
        $this->db->where('referral.id = "' . $coupon_id . '"');
        // $this->db->group_by("p.sell_id");
        $SellList = $this->db->get();
        // print_r($user_details->row()->user_name);exit;

        $email = $SellList->row()->email;
        $header .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";

        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>Welcome Cityfurnish</title>
                  <style type="text/css">
                  @media screen and (max-width: 580px) {
                    .tab-container{max-width: 100%;}
                    .txt-pad-15 {padding: 0 15px 51px 15px !important;}
                    .foo-txt {padding: 0px 15px 18px 15px !important;}
                    .foo-add {padding: 20px !important;}
                    .tab-padd-zero{padding:0px !important;}
                    .tab-padd-right{padding-right:25px !important;}
                    .pad-20{padding:25px 20px 20px !important;}
                    .social-padd-left{padding:15px 20px 0px 0px; !important;}
                    .offerimg{width:100% !important;}
                    .mobilefont{font-size:16px !important;line-height:18px !important;}
                  }
                 </style>
                </head>
                <body style="margin:0px;">
                <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff;margin: 0 auto;font-family:Arial, Helvetica, sans-serif;font-size:14px;border-collapse:collapse;width:600px;
                border-width:1px; border-style:solid; border-color:#e7e7e7;table-layout: fixed;display: block;border-right-width: 0px;">

                    <tr>
                     <td style="width:100%">
                      <table style="width:100%;table-layout:fixed" cellpadding="0" cellspacing="0px">
                      <tr>
                      <td style="text-align: left;padding:15px 0px 15px 20px;padding-left:20px;">
                        <a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/logo-2.png" alt="logo" style="width:150px;" /></a>
                      </td>
                      <td style="text-align: right;padding:15px 20px 15px 0px;text-align:right;width:50%;border-right: 1px solid #e7e7e7" class="social-padd-left">
                        <a href="https://www.facebook.com/cityFurnishRental" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/facebook.png" alt="facebook" width="18px" /></a>
                        <a href="https://twitter.com/CityFurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/twitter.png" alt="twitter" width="18px"/></a>
                        <a href="https://plus.google.com/+cityfurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/google-plus.png" alt="google" height="18px"/></a>
                        <a href="https://in.pinterest.com/cityfurnish/" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/pintrest.png" alt="pintrest" width="18px" /></a>
                        <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/linkedin.png" alt="linkedin" width="18px" /></a>
                        </td>
                      </tr>
                      </table>
                     </td>
                    </tr>
                    <tr style="width:100%;">
                    <td style="width:100%;border-right: 1px solid #e7e7e7">
                      <img src="https://cityfurnish.com/images/voucher-banner.jpg" style="display: block;width: 100%;">
                    </td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="padding-top: 50px;font-size: 40px;color: #002e40;text-align: center;border-right: 1px solid #e7e7e7">
                      <h1 style="color: #002e40;font-size: 40px;line-height:36px;font-weight:bold;margin:0px;">Congratulation!</h1>
                     </td>
                    </tr>
                  

                    <tr style="width:100%;">
                      <td style="text-align: center;padding-top: 15px;padding-bottom: 25px;font-size: 24px;border-right: 1px solid #e7e7e7">
                        <span style="display: inline-block;vertical-align: middle;font-size: 18px;">
                            Hello, <strong>'. $SellList->row()->user_name .'</strong><br>
                            Your referral code <strong style="display: inline-block;vertical-align: middle;color: #f9aa2b;">'.$SellList->row()->referral_code.'</strong> is used by <strong>'. $user_details->row()->user_name .'</strong> and got Rs <strong>'.$SellList->row()->referral_amount.'</strong> discount. Please contact our customer care to avail referral benefit.
                        </span>
                      </td>
                    </tr>
                  
                   
                    <tr  style="width:100%;">
                        <td style="padding:30px 50px 26px;text-align:center;line-height:24px;width:100%;border-right: 1px solid #e7e7e7">
                            <p style="margin:0px;line-height:22px;font-family:Arial, Helvetica, sans-serif;color:#b2b2b2">Sent by <a href="https://cityfurnish.com/" style="color:#38373d;text-decoration:none;">Cityfurnish</a>, 6B Tower 3, Bellevue Tower, Central Park 2, Sohna Road,
                Sector 48, Gurgaon, Haryana - 122018<br /><a href="mailto:hello@cityfurnish.com" style="color:#38373d; margin-top:8px;display:inline-block;text-decoration:none;">hello@cityfurnish.com</a></p>
                        </td>
                    </tr>
                </table>
                </body>
                </html>';

        $sender_email = $this->data['siteContactMail'];
        $sender_name = 'Cityfurnish';

        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $email,
            'subject_message' => '**Cityfurnish- Referral Success**',
            'body_messages' => $message,
            'mail_id' => 'Checkout Mail'
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);
        $this->Send_Referral_Copy($user_details,$SellList,$order_id);
    }
    
    
    function Send_Referral_Copy($user_details,$SellList,$order_id){
        $header .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";

        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <title>Welcome Cityfurnish</title>
                  <style type="text/css">
                  @media screen and (max-width: 580px) {
                    .tab-container{max-width: 100%;}
                    .txt-pad-15 {padding: 0 15px 51px 15px !important;}
                    .foo-txt {padding: 0px 15px 18px 15px !important;}
                    .foo-add {padding: 20px !important;}
                    .tab-padd-zero{padding:0px !important;}
                    .tab-padd-right{padding-right:25px !important;}
                    .pad-20{padding:25px 20px 20px !important;}
                    .social-padd-left{padding:15px 20px 0px 0px; !important;}
                    .offerimg{width:100% !important;}
                    .mobilefont{font-size:16px !important;line-height:18px !important;}
                  }
                 </style>
                </head>
                <body style="margin:0px;">
                <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff;margin: 0 auto;font-family:Arial, Helvetica, sans-serif;font-size:14px;border-collapse:collapse;width:600px;
                border-width:1px; border-style:solid; border-color:#e7e7e7;table-layout: fixed;display: block;border-right-width: 0px;">

                    <tr>
                     <td style="width:100%">
                      <table style="width:100%;table-layout:fixed" cellpadding="0" cellspacing="0px">
                      <tr>
                      <td style="text-align: left;padding:15px 0px 15px 20px;padding-left:20px;">
                        <a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/logo-2.png" alt="logo" style="width:150px;" /></a>
                      </td>
                      <td style="text-align: right;padding:15px 20px 15px 0px;text-align:right;width:50%;border-right: 1px solid #e7e7e7" class="social-padd-left">
                        <a href="https://www.facebook.com/cityFurnishRental" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/facebook.png" alt="facebook" width="18px" /></a>
                        <a href="https://twitter.com/CityFurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/twitter.png" alt="twitter" width="18px"/></a>
                        <a href="https://plus.google.com/+cityfurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/google-plus.png" alt="google" height="18px"/></a>
                        <a href="https://in.pinterest.com/cityfurnish/" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/pintrest.png" alt="pintrest" width="18px" /></a>
                        <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/linkedin.png" alt="linkedin" width="18px" /></a>
                        </td>
                      </tr>
                      </table>
                     </td>
                    </tr>
                    <tr style="width:100%;">
                    <td style="width:100%;border-right: 1px solid #e7e7e7">
                      <img src="https://cityfurnish.com/images/voucher-banner.jpg" style="display: block;width: 100%;">
                    </td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="padding-top: 50px;font-size: 40px;color: #002e40;text-align: center;border-right: 1px solid #e7e7e7">
                      <h1 style="color: #002e40;font-size: 40px;line-height:36px;font-weight:bold;margin:0px;">Referral Success!</h1>
                     </td>
                    </tr>
                  

                    <tr style="width:100%;">
                      <td style="padding-left: 15px;padding-top: 15px;padding-bottom: 25px;font-size: 24px;border-right: 1px solid #e7e7e7">
                        <span style="display: inline-block;font-size: 18px;">
                          Order id : #<strong style="display: inline-block;vertical-align: middle;color: #f9aa2b;">'.$order_id.'</strong><br>
                          Referral Code :<strong style="display: inline-block;vertical-align: middle;color: #f9aa2b;">'.$SellList->row()->referral_code.'</strong><br>
                          Referrer User Name : '.$SellList->row()->user_name.'<br>
                          Referrer User Email : '.$SellList->row()->email.'<br>
                        </span>
                      
                      </td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="padding-left: 15px;padding-top: 15px;padding-bottom: 25px;font-size: 24px;border-right: 1px solid #e7e7e7">
                        <span style="display: inline-block;font-size: 18px;">
                          Referred User Name : '.$user_details->row()->user_name.'<br>
                          Referred User Email : '.$user_details->row()->email.'<br>
                        </span>
                      </td>
                    </tr>
                  
                   
                    <tr  style="width:100%;">
                        <td style="padding:30px 50px 26px;text-align:center;line-height:24px;width:100%;border-right: 1px solid #e7e7e7">
                            <p style="margin:0px;line-height:22px;font-family:Arial, Helvetica, sans-serif;color:#b2b2b2">Sent by <a href="https://cityfurnish.com/" style="color:#38373d;text-decoration:none;">Cityfurnish</a>, 6B Tower 3, Bellevue Tower, Central Park 2, Sohna Road,
                Sector 48, Gurgaon, Haryana - 122018<br /><a href="mailto:hello@cityfurnish.com" style="color:#38373d; margin-top:8px;display:inline-block;text-decoration:none;">hello@cityfurnish.com</a></p>
                        </td>
                    </tr>
                </table>
                </body>
                </html>';

        $sender_email = $this->data['siteContactMail'];
        $sender_name = 'Cityfurnish';
        $to_email = array('hello@cityfurnish.com','accounts@cityfurnish.com');

        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $to_email,
            'subject_message' => '**Cityfurnish- Referral Success**',
            'body_messages' => $message,
            'mail_id' => 'Checkout Mail'
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);

    }


    /*     * ******************************************** Payment Gift Success *************************************************** */

    public function PaymentGiftSuccess($userid = '', $transId = '', $payerMail = '') {


        $paymtdata = array(
            'fc_session_user_id' => $userid,
        );

        $this->session->set_userdata($paymtdata);
        $GiftTemp = $this->order_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));

        if ($GiftTemp->num_rows() > 0) {

            foreach ($GiftTemp->result() as $GiftRows) {


                $dataArr = array();
                foreach ($GiftRows as $key => $val) {
                    if (!(in_array($key, 'id'))) {
                        $dataArr[$key] = trim(addslashes($val));
                    }
                }
                $condition = '';
                $this->order_model->simple_insert(GIFTCARDS, $dataArr);
            }


            $condition1 = array('user_id' => $userid, 'payment_status' => 'Pending');
            if ($payerMail != '') {
                $dataArr1 = array('payment_status' => 'Paid', 'paypal_transaction_id' => $transId, 'payer_email' => $payerMail, 'payment_type' => 'Paypal');
            } else {

                $dataArr1 = array('payment_status' => 'Paid', 'paypal_transaction_id' => $transId, 'payment_type' => 'Credit Cart');
            }

            $this->order_model->update_details(GIFTCARDS, $dataArr1, $condition1);

            //Send Mail to User
            $GiftTempVal = $this->order_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
            $this->SendMailUSersGift($GiftTempVal);

            //Empty Gift cart Temp Info
            $condition3 = array('user_id' => $userid);
            $this->order_model->commonDelete(GIFTCARDS_TEMP, $condition3);
        }

        echo 'Success';
    }

    /*     * ******************************************** Payment Subscribe Success *************************************************** */

    public function PaymentSubscribeSuccess($userid = '', $transId = '') {


        $paymtdata = array(
            'fc_session_user_id' => $userid,
        );

        $this->session->set_userdata($paymtdata);

        $FancyboxTemp = $this->order_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));


        foreach ($FancyboxTemp->result() as $FancyboxRow) {


            $dataArr = array();
            foreach ($FancyboxRow as $key => $val) {
                if ($key != 'id') {
                    $dataArr[$key] = trim(addslashes($val));
                }
            }
            $condition = '';
            $this->order_model->simple_insert(FANCYYBOX_USES, $dataArr);
        }


        $condition1 = array('user_id' => $userid);
        $dataArr1 = array('status' => 'Paid', 'trans_id' => $transId, 'payment_type' => 'Credit Cart');

        $this->order_model->update_details(FANCYYBOX_USES, $dataArr1, $condition1);

        //Update Quantity
        foreach ($FancyboxTemp->result() as $updPrdRow) {

            $SelPrd = $this->order_model->get_all_details(FANCYYBOX, array('id' => $updPrdRow->fancybox_id));
            $PrdCount = $SelPrd->row()->purchased + $updPrdRow->quantity;
            $condition2 = array('id' => $updPrdRow->fancybox_id);
            $dataArr2 = array('purchased' => $PrdCount);
            $this->order_model->update_details(FANCYYBOX, $dataArr2, $condition2);
        }


        //Send Mail to User

        $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city');
        $this->db->from(FANCYYBOX_USES . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->where('p.user_id = "' . $userid . '" and p.status="Paid"');
        $SubcribTempVal = $this->db->get();

        $this->SendMailUSersSubscribe($SubcribTempVal);

        //Empty Gift cart Temp Info
        $condition3 = array('user_id' => $userid);
        $this->order_model->commonDelete(FANCYYBOX_TEMP, $condition3);

        echo 'Success';
    }

    /*     * ******************************************** Send Mail to CSR for failed order********************************************** */

    public function Prepare_failed_order_data() {

        $quick_user_name = $this->session->userdata('quick_user_name');
        if ($quick_user_name != '') {
            $condition = array('user_name' => $quick_user_name);
            $userDetails = $this->user_model->get_all_details(USERS, $condition);
            if ($userDetails->num_rows() == 1) {
                $this->send_failed_order_mail($userDetails);
            }
        }
    }

    public function send_failed_order_mail() {
        $email = 'hello@cityfurnish.com';
        $header .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";

        $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			
			<meta name="viewport" content="width=device-width"/><body>
                        <p> Failed Order. Please check failed orders on Admin panel and contact customer</p>';
        $message .= '</body>
			</html>';
        $sender_email = $this->data['siteContactMail'];
        $sender_name = 'Admin';


        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $email,
            'subject_message' => '**Cityfurnish- Failed Order Alert**',
            'body_messages' => $message,
            'mail_id' => 'Checkout Mail'
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);
    }

    function get_line_with_replace($array)
    {
        $ci =& get_instance();  
        $ci->load->helper('language');
        $ci->lang->load($file,$lang);
        $str = $ci->lang->line($index);
        if(!empty($convert_arr)){
            $new_str = '';
            $cnt = 0;
            foreach ($convert_arr as $_key => $_data){
                if($cnt==0){
                    $new_str = str_replace($_key, $_data, $str);
                }else{
                    $new_str = str_replace($_key, $_data, $new_str);
                }
                $cnt++;
            }
            return $new_str;
        }else{
            return $str;  
        }
        
        //return str_replace($search, $swap, $str);
    }


    /*     * ******************************************** Send Mail to User********************************************** */

    public function SendMailUSers($PrdList, $SellList) {

       // echo '<pre>';print_r($PrdList->result()); die;

        $shipAddRess = $this->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));

        $newsid = '19';
        $template_values = $this->get_newsletter_template($newsid);
        //var_dump($template_values['news_subject']);exit;
        $adminnewstemplateArr = array(
            'logo' => $this->data['logo'],
            'meta_title' => $this->config->item('meta_title'),
            'ship_fullname' => stripslashes($shipAddRess->row()->full_name),
            'ship_address1' => stripslashes($shipAddRess->row()->address1),
            'ship_address2' => stripslashes($shipAddRess->row()->address2),
            'ship_city' => stripslashes($shipAddRess->row()->city),
            'ship_country' => stripslashes($shipAddRess->row()->country),
            'ship_state' => stripslashes($shipAddRess->row()->state),
            'ship_postalcode' => stripslashes($shipAddRess->row()->postal_code),
            'ship_phone' => stripslashes($shipAddRess->row()->phone),
            'bill_fullname' => stripslashes($shipAddRess->row()->full_name),
            'bill_address1' => stripslashes($shipAddRess->row()->address1),
            'bill_address2' => stripslashes($shipAddRess->row()->address2),
            'bill_city' => stripslashes($shipAddRess->row()->city),
            'bill_country' => stripslashes($shipAddRess->row()->country),
            'bill_state' => stripslashes($shipAddRess->row()->state),
            'bill_postalcode' => stripslashes($shipAddRess->row()->postal_code),
            'bill_phone' => stripslashes($shipAddRess->row()->phone),
            'invoice_number' => $PrdList->row()->dealCodeNumber,
            'invoice_date' => date("F j, Y g:i a", strtotime($PrdList->row()->created))
        );
        extract($adminnewstemplateArr);
        $subject = $template_values['news_subject'];

     $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  
  <title>Order Confirmation</title>
  <style type="text/css">
@media screen and (max-width: 580px) {
.tab-container {
  max-width: 100%;
}
.txt-pad-15 {
  padding: 0 15px 51px 15px !important;
}
.foo-txt {
  padding: 0px 15px 18px 15px !important;
}
.foo-add {
  padding: 15px 15px 0px 15px !important;
}
.foo-add-left {
  width: 100% !important;
}
.foo-add-right {
  width: 100% !important;
}
.pad-bottom-15 {
  padding-bottom: 15px !important;
}
.pad-20 {
  padding: 25px 20px 20px !important;
}
}
</style>
  </head>
  <body style="margin:0px;">
    <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="width: 850px;background-color: #fff;margin: 0 auto;table-layout: fixed;background:#dbdbdb;font-family:Arial, Helvetica, sans-serif;font-size:15px;"> <tbody>
    <tr>
      <td style="padding:20px;">

      <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td style="text-align: left;width: 100%;padding-bottom:25px;background:#90c5bc;padding:20px;"><a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/email/logo.png" alt="logo"></a></td>
        </tr>
     </table>

         <table width="100%" border="0" cellpadding="0" cellspacing="0">
     
          <tr>
            <td style="text-align: left;width: 100%;background:#f5f5f5;padding:25px 30px 30px;">

            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="text-align: left;width: 100%;padding-bottom:10px;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-weight:bold;font-size:15px;color:#656565;">Hi '.strtoupper($ship_fullname).'</p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-size:15px;color:#656565;">Thank you for your order.</p></td>
              </tr>
              </table> 
              
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                <tr>
                  <td style="text-align: left;padding:15px 20px;width:50%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Id</strong><span style="font-weight:400;"> : #'.$invoice_number.'</span> </p></td>
                  <td style="text-align: left;padding:15px 20px;border-left:1px solid #ddd;width:50%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Date </strong><span style="font-weight:400;"> : '.$invoice_date.'</span> </p></td>
                </tr>
              </table>
       

              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                <tr>
                  <td style="text-align: left;padding:15px 20px;width:100%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Shipping Details</strong> </p></td>
                </tr>
                <tr>
                  <td style="text-align: left;padding:15px 20px;border-top:1px solid #ddd;width:100%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Full Name</strong> <span style="font-weight:400;"> : '.$ship_fullname.'</span> </p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"><strong style="font-weight:600;">Address</strong> <span style="font-weight:400;"> : '.$ship_address1.'</span> </p>

                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">City</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.$ship_city.'</span> <strong style="font-weight:600;;">State</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.$ship_state.'</span> <strong style="font-weight:600;">Zip Code</strong> <span style="font-weight:400;display:inline-block;"> : '.$ship_postalcode.'</span> </p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Phone Number</strong> <span style="font-weight:400;"> : '.$ship_phone.'</span> </p></td>
                </tr>
              </table>

 <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;color:#656565;border-color:#ddd;">
   
                <tr>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:100px;"> Product Images </td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;f\ont-weight:600;border-color:#ddd;width:120px;"> Product Name </td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:35px;">QTY</td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:120px;">Security Deposit</td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:110px;">Monthly Rental</td>
                  <td align="center" style="color:#656565;padding:20px 5px;font-size:12px;font-weight:600;width:120px;"> Sub Total </td>
                </tr>';


     //include('./newsletter/registeration' . $newsid . '.php');
     // $file_data  = file_get_contents('./newsletter/registeration' . $newsid . '.php');
    //  print_r($message1);exit;
      //get_line_with_replace($file_data);

    //$message = stripslashes(substr($message, 0, strrpos($message, '</tbody>')));
    //$message = stripslashes(substr($message, 0, strrpos($message, '</table>')));


       $message1 .= $message;
        $disTotal = 0;
        $grantTotal = 0;
        
        foreach ($PrdList->result() as $cartRow) {
            $InvImg = @explode(',', $cartRow->image);
            $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->price * $cartRow->quantity;
            $unitDeposit = $cartRow->product_shipping_cost * $cartRow->quantity;

            $uTot = $unitPrice + $unitDeposit;


            if ($cartRow->attr_name != '' || $cartRow->attr_type != '') {
                $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
            } else {
                $atr = '';
            }
            $message1 .= '<tr>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid; font-size:11px;border-color:#ddd;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" />
            </td>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;line-height:20px;border-color:#ddd;">' . stripslashes($cartRow->product_name) .'<br/>'. $atr .' 
            </td>

            <td align="center" style="padding:20px 0px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . strtoupper($cartRow->quantity) . '
            </td>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitDeposit, 2, '.', '') . '
            </td>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '
            </td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] .number_format($uTot, 2, '.', '')  . '</td>
        </tr>';
            $grantTotal = $grantTotal + $uTot;
            $grandDeposit = $grandDeposit + $unitDeposit;
            $total_deposite  += $unitDeposit;
            $total_rental +=  $unitPrice;
            $total_total += $uTot;
        }
        $private_total = $grantTotal - $PrdList->row()->discountAmount;
        $private_total = $private_total + $PrdList->row()->tax;
        if($PrdList->row()->discount_on_si != ''){
            $private_total = $private_total - $PrdList->row()->discount_on_si;
        }

    

        $message1 .= '<tr>
                  <td align="right" colspan="3" style="padding:20px 20px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:12px;font-weight:600;border-color:#ddd;"> TOTAL </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] .  number_format($total_deposite, 2, '.', '') . ' </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;"> ' . $this->data['currencySymbol'] . number_format($total_rental, 2, '.', '') . ' </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($total_total, 2, '.', '') . '</td>
            </tr>
        </table>';

        if ($PrdList->row()->note != '') {
            $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
                <td width="87" ><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
               
            </tr>
			<tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($PrdList->row()->note) . '</span></td>
            </tr></table>';
        }

        if ($PrdList->row()->order_gift == 1) {
            $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
            </tr></table>';
        }

        $message1 .= '<table border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;color:#656565;border-color:#ddd;">
            <tr>
                <td align="left" style="color:#656565;padding:14px 20px 15px 20px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;padding-left:20px;"> Sub Total </td>

                <td><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</span></td>
            </tr>';
        if ($PrdList->row()->couponCode != '') {
            $message1 .= '<tr>
                <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount <br />
                      <span style="font-size:9px !important;font-weight:400;">(Coupon Code : '. $PrdList->row()->couponCode .')</span></p></td>
                  <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . ' '.number_format($PrdList->row()->discountAmount, '2', '.', '') . '</td>
            </tr>';
        }
        
        if ($PrdList->row()->discount_on_si != '') {
            $message1 .= '<tr>
                <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount For SI<br />
                      </p></td>
                  <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . ' '.number_format($PrdList->row()->discount_on_si, '2', '.', '') . '</td>
            </tr>';
        }
        
        $message1 .= '<tr>
                  <td align="left" style="color:#656565;padding:14px 15px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-size:14px;font-weight:600;border-color:#ddd;padding-left:20px;">GRAND TOTAL</td>
                  <td align="center" style="color:#60a196;padding:15px 20px;border-top:1px solid #ddd;font-size:15px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</td>
                </tr>
            </tr>
        </table>
           </td>
         </tr>
          <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
            <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
              <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
                Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
          </tr>
  </table>
  </td>
  </tr>
  </table>
</body>
</html>';

        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->config->item('site_contact_mail');
            $sender_name = $this->config->item('email_title');
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }
        
        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $PrdList->row()->email,
            'cc_mail_id' => 'hello@cityfurnish.com',
            'subject_message' => $subject,
            'body_messages' => $message1
        );
        $email_send_to_common = $this->common_email_send($email_values);
        
        
        
        //send voucher mail on new order
        
    if($PrdList->row()->is_recurring){
        $Secret_Data = $this->user_model->get_all_details('fc_voucher_settings',array('id !=' => ''));
        $Reward  = base64_encode ($Secret_Data->row()->reward);
        $post_url = $Secret_Data->row()->post_url;
        $return_url = $Secret_Data->row()->return_url;
        $partner_code = $Secret_Data->row()->partner_code;
        $user_name = str_replace(' ', '', $shipAddRess->row()->full_name);
        $timeStamp = base64_encode(time());

    if($Secret_Data->row()->status){
          $url = file_get_contents('http://tinyurl.com/api-create.php?url='.'http://kbba.klippd.in/?clientID='.$Secret_Data->row()->client_id.'&key='.$Secret_Data->row()->key_id.'&userID='.$shipAddRess->row()->user_id.'&rewards='.$Reward.'&username='.$user_name.'&EmailAddress='.$PrdList->row()->email.'&postURL='.$post_url.'&returnURL='.$return_url.'&PartnerCode='.$partner_code.'&timeStamp='.$timeStamp.'');

    
    $voucher ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <title>Welcome Cityfurnish</title>
                  <style type="text/css">
                  @media screen and (max-width: 580px) {
                    .tab-container{max-width: 100%;}
                    .txt-pad-15 {padding: 0 15px 51px 15px !important;}
                    .foo-txt {padding: 0px 15px 18px 15px !important;}
                    .foo-add {padding: 20px !important;}
                    .tab-padd-zero{padding:0px !important;}
                    .tab-padd-right{padding-right:25px !important;}
                    .pad-20{padding:25px 20px 20px !important;}
                    .social-padd-left{padding:15px 20px 0px 0px; !important;}
                    .offerimg{width:100% !important;}
                    .mobilefont{font-size:16px !important;line-height:18px !important;}
                  }
                 </style>
                </head>
                <body style="margin:0px;">
                <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff;margin: 0 auto;font-family:Arial, Helvetica, sans-serif;font-size:14px;border-collapse:collapse;width:600px;
                border-width:1px; border-style:solid; border-color:#e7e7e7;table-layout: fixed;display: block;border-right-width: 0px;">

                    <tr>
                     <td style="width:100%">
                      <table style="width:100%;table-layout:fixed" cellpadding="0" cellspacing="0px">
                      <tr>
                      <td style="text-align: left;padding:15px 0px 15px 20px;padding-left:20px;">
                        <a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/logo-2.png" alt="logo" style="width:150px;" /></a>
                      </td>
                      <td style="text-align: right;padding:15px 20px 15px 0px;text-align:right;width:50%;border-right: 1px solid #e7e7e7" class="social-padd-left">
                        <a href="https://www.facebook.com/cityFurnishRental" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/facebook.png" alt="facebook" width="18px" /></a>
                        <a href="https://twitter.com/CityFurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/twitter.png" alt="twitter" width="18px"/></a>
                        <a href="https://plus.google.com/+cityfurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/google-plus.png" alt="google" height="18px"/></a>
                        <a href="https://in.pinterest.com/cityfurnish/" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/pintrest.png" alt="pintrest" width="18px" /></a>
                        <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/linkedin.png" alt="linkedin" width="18px" /></a>
                        </td>
                      </tr>
                      </table>
                     </td>
                    </tr>
                    <tr style="width:100%;">
                    <td style="width:100%;border-right: 1px solid #e7e7e7">
                      <img src="https://cityfurnish.com/images/voucher-banner.jpg" style="display: block;width: 100%;">
                    </td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="padding-top: 50px;font-size: 40px;color: #002e40;text-align: center;border-right: 1px solid #e7e7e7">
                      <h1 style="color: #002e40;font-size: 40px;line-height:36px;font-weight:bold;margin:0px;">Congratulations!</h1>
                             <span style="font-size: 18px;">You Got Gift Vouchers Worth</span>
                     </td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="text-align: center;padding-top: 5px;border-right: 1px solid #e7e7e7">
                        <span style="display: inline-block;vertical-align: middle;margin-right: 10px;">
                          <img src="https://cityfurnish.com/images/rupee-icn.png">
                        </span>
                        <span style="display: inline-block;vertical-align: middle;color: #f9aa2b;font-size: 45px;"><strong>'.$Secret_Data->row()->reward.'</strong></span></td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="text-align: center;padding-top: 15px;padding-bottom: 25px;font-size: 24px;border-right: 1px solid #e7e7e7">
                          <span style="display: inline-block;vertical-align: middle;font-size: 18px;">
                            From Cityfurnish for your order #'.$invoice_number.'
                          </span>
                      </td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="text-align: center;font-size: 24px;color: #141414 !important;border-right: 1px solid #e7e7e7">
                          <a href="'.$url.'" target="_blank" style="background: #a5c8c2;width: 198px;text-decoration: none !important;font-size: 13px;display: inline-block;padding-top: 10px;padding-bottom: 10px;color: #141414 !important;">REDEEM</a>
                      </td>
                    </tr>
                    <tr style="width:100%;">
                        <td style="padding: 20px;">
                          <h5>Terms and Conditions:</h5>
                          <span style="font-size: 10px;">1.You can opt for multiple voucher up-to-cumulative total amount specified above.</span><br>
                          <span style="font-size: 10px;">2.Voucher link will be active for 60 days from send date .You can redeem desired vouchers by visting link multiple times using link within this period.</span><br>
                          <span style="font-size: 10px;">3.Once your have selected vouchers for redemption, you will receive selected voucher on email with details instruction for usage of each voucher. </span><br>
                          <span style="font-size: 10px;">4.Please contact us on  8010845000 or hello@cityfurnish.com in case your need any assistance.</span>
                        </td>
                    </tr>
                    <tr  style="width:100%;">
                        <td style="padding:30px 50px 26px;text-align:center;line-height:24px;width:100%;border-right: 1px solid #e7e7e7">
                            <p style="margin:0px;line-height:22px;font-family:Arial, Helvetica, sans-serif;color:#b2b2b2">Sent by <a href="https://cityfurnish.com/" style="color:#38373d;text-decoration:none;">Cityfurnish</a>, 540, Block D, JMD Megapolis, Sohna Road, Sector 48, Gurgaon, Haryana - 122018<br /><a href="mailto:hello@cityfurnish.com" style="color:#38373d; margin-top:8px;display:inline-block;text-decoration:none;">hello@cityfurnish.com</a></p>
                        </td>
                    </tr>
                </table>
                </body>
                </html>';

            $email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Cityfurnish',
                         'to_mail_id'=> $PrdList->row()->email,
                         'cc_mail_id'=> 'hello@cityfurnish.com',
                         'subject_message'=>'Cityfurnish - Gift Vouchers',
                         'body_messages'=>$voucher
            );
            $email_send_to_common = $this->product_model->common_email_send($email_values);
        }
    }


        //echo $this->email->print_debugger(); die; 

        /*         * ********************************************seller Product Confirmation Mail Sent *********************************************** */

        foreach ($SellList->result() as $sellRow) {

             $message2 = '';
//             $subject2 = $template_values['news_subject'];
//             $message2 .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
// <html xmlns="http://www.w3.org/1999/xhtml"><head>
// <meta name="viewport" content="width=device-width"/></head>
// <style type="text/css">
// @media screen and (max-width: 580px) {
// .tab-container {
//     max-width: 100%;
// }
// .txt-pad-15 {
//     padding: 0 15px 51px 15px !important;
// }
// .foo-txt {
//     padding: 0px 15px 18px 15px !important;
// }
// .foo-add {
//     padding: 15px 15px 0px 15px !important;
// }
// .foo-add-left {
//     width: 100% !important;
// }
// .foo-add-right {
//     width: 100% !important;
// }
// .pad-bottom-15 {
//     padding-bottom: 15px !important;
// }
// .pad-20 {
//     padding: 25px 20px 20px !important;
// }
// }
// </style>
//  <body style="margin:0px;">

// ';
            $message2 .= $message;

            $disTotal = 0;
            $grantTotal = 0;
            foreach ($PrdList->result() as $cartRow) {
                if ($cartRow->sell_id == $sellRow->sell_id) {

                    // $InvImg = @explode(',', $cartRow->image);
                    // $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->product_shipping_cost + $cartRow->price;
                    // $uTot = $unitPrice * $cartRow->quantity;
            $InvImg = @explode(',', $cartRow->image);
            $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->price * $cartRow->quantity;
            $unitDeposit = $cartRow->product_shipping_cost * $cartRow->quantity;

            $uTot = $unitPrice + $unitDeposit;

                    if ($cartRow->attr_name != '' || $cartRow->attr_type != '') {
                        $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
                    } else {
                        $atr = '';
                    }

                   
                    $message2 .= '<tr>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid; font-size:11px;border-color:#ddd;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" />
            </td>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;line-height:20px;border-color:#ddd;">' . stripslashes($cartRow->product_name) .'<br/>'. $atr .' 
            </td>

            <td align="center" style="padding:20px 0px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . strtoupper($cartRow->quantity) . '
            </td>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitDeposit, 2, '.', '') . '
            </td>

            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '
            </td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] .number_format($uTot, 2, '.', '')  . '</td>
        </tr>';
                    $grantTotal = $grantTotal + $uTot;
                     $total_deposite1  +=  $unitDeposit;
                     $total_rental1 += $unitPrice;
                     $total_total1 += $uTot;
                }
            }

            $message2 .= '
             <tr>
                  <td align="right" colspan="3" style="padding:20px 20px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:12px;font-weight:600;border-color:#ddd;"> TOTAL </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;"> ' . $this->data['currencySymbol'] . number_format($total_deposite1, 2, '.', '')  .' </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($total_rental1, 2, '.', '') .'</td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($total_total1, 2, '.', '') .'</td>
                </tr></table>';
            if ($PrdList->row()->note != '') {
                $message2 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
                <td width="87" ><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
               
            </tr>
            <tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($PrdList->row()->note) . '</span></td>
            </tr></table>';
            }

            if ($PrdList->row()->order_gift == 1) {
                $message2 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
            </tr></table>';
            }

                 
            $message2 .= '<table border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;color:#656565;border-color:#ddd;"><tr>
                <td align="left" style="color:#656565;padding:14px 20px 15px 20px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;padding-left:20px;"> Sub Total </td>
                  <td align="center" style="color:#656565;padding:15px 20px;font-size:11px;font-weight:600;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</td>
              </tr>';
            if ($PrdList->row()->couponCode != '') {
                $message2 .= '	<tr>
                    <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount <br />
                          <span style="font-size:9px !important;font-weight:400;">(Coupon Code :'. $PrdList->row()->couponCode .')</span></p></td>
                    <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . '-'.number_format($PrdList->row()->discountAmount, '2', '.', '') . '</td>
            </tr>';
            }
            $message2 .= '<tr>
                <td align="left" style="color:#656565;padding:14px 15px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-size:14px;font-weight:600;border-color:#ddd;padding-left:20px;">GRAND TOTAL</td>
                <td align="center" style="color:#60a196;padding:15px 20px;border-top:1px solid #ddd;font-size:15px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</td>
                </tr>
                </table>
            </td>
         </tr>
          <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
            <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
              <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
                Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
          </tr>
        </table></td>
    </tr>
  </table>
</body>
</html>';


            $email_values1 = array('mail_type' => 'html',
                'from_mail_id' => $sender_email,
                'mail_name' => $sender_name,
                'to_mail_id' => $sellRow->email,
                // 'cc_mail_id' => 'invoices@3c0x36x2q5.referralcandy.com',
                'subject_message' => $subject,
                'body_messages' => $message2
            );


            $email_send_to_common = $this->product_model->common_email_send($email_values1);
        }


        return;
    }

    /*     * ******************************************** Send Mail to Gift********************************************** */

    public function SendMailUSersGift($GiftRowsVals) {

        //echo '<pre>';print_r($GiftRowsVals);	
        foreach ($GiftRowsVals->result() as $GiftVals) {



            $newsid = '15';
            $template_values = $this->order_model->get_newsletter_template_details($newsid);
            $usrDetails = $this->order_model->get_all_details(USERS, array('id' => $GiftVals->user_id));
            $adminnewstemplateArr = array(
                'email_title' => $this->config->item('email_title'),
                'logo' => $this->data['logo'],
                'meta_title' => $this->config->item('meta_title'),
                'full_name' => stripslashes($usrDetails->row()->full_name),
                'email' => stripslashes($usrDetails->row()->email),
                'code' => stripslashes($GiftVals->code),
                'price_value' => stripslashes($GiftVals->price_value),
                'expiry_date' => stripslashes($GiftVals->expiry_date),
                'description' => stripslashes($GiftVals->description),
                'rname' => stripslashes($GiftVals->recipient_name)
            );
            extract($adminnewstemplateArr);
            $subject = $template_values['news_subject'];


            $message = '<!DOCTYPE HTML>
			<html>
			<head>
			
			<meta name="viewport" content="width=device-width"/>
			<title>Gift Card</title>
			</head>
			<body>';
            include('./newsletter/registeration' . $newsid . '.php');
            $message .= '</body>
			</html>';

            if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                $sender_email = $this->config->item('site_contact_mail');
                $sender_name = $this->config->item('email_title');
            } else {
                $sender_name = $template_values['sender_name'];
                $sender_email = $template_values['sender_email'];
            }

            $email_values = array('mail_type' => 'html',
                'from_mail_id' => $sender_email,
                'mail_name' => $sender_name,
                'to_mail_id' => $GiftVals->recipient_mail,
                'cc_mail_id' => $this->config->item('site_contact_mail'),
                'subject_message' => $subject,
                'body_messages' => $message
            );
            $email_send_to_common = $this->product_model->common_email_send($email_values);
        }
        //echo $this->email->print_debugger(); die; 
        return;
    }

    /*     * ******************************************** Send Mail to Subscribe********************************************** */

    public function SendMailUSersSubscribe($PrdList) {

        $subject = 'From: ' . $this->config->item('email_title') . ' Subscription';

        $message = '<!DOCTYPE HTML>
			<html>
			<head>
			
			<meta name="viewport" content="width=device-width"/>
			<title>Gift Card</title>
			</head>
			<body marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
			<table width="640" border="0" cellspacing="0" cellpadding="0" bgcolor="#7da2c1">
			<tr>
			<td style="padding:40px;">
			<table width="610" border="0" cellpadding="0" cellspacing="0" style="border:#1d4567 1px solid; font-family:Arial, Helvetica, sans-serif;">
				<tr>
				<td>
				<a href="' . base_url() . '"><img src="' . base_url() . 'images/logo/' . $this->data['logo'] . '" alt="' . $this->config->item('meta_title') . '" style="margin:15px 5px 0; padding:0px; border:none;"></a>
				</td>
				</tr>
				<tr>
				<td valign="top" style="background-color:#FFFFFF;">
				<table border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
					<td colspan="2">
					<h3 style="padding:10px 15px; margin:0px; color:#0d487a;">Subscription for ' . ucfirst($this->config->item('email_title')) . '</h3>
					</td>
					</tr>
				</table>';


        $message .= '<table width="611" border="0" cellpadding="0" cellspacing="0"><tr>
                		    <th width="37%" align="left">Product Title</th>
		                    <th width="30%">Quantity</th>
        		            <th width="33%">Amount</th>
                			</tr>';
        $grantTotal = 0;
        foreach ($PrdList->result() as $cartRow) {

            $message .= '
                <tr style="font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; color:#292881; padding: 0px 4px 0px 5px;">
                  <td width="38%">' . stripslashes($cartRow->name) . '</td>
                  <td width="23%" align="center">' . strtoupper($cartRow->quantity) . '</td>
                  <td width="28%" align="center">' . $this->data['currencySymbol'] . $cartRow->indtotal . '</td>
                </tr>
                ';
            $grantTotal = $grantTotal + $cartRow->indtotal;
        }
        $private_total = $grantTotal;
        $private_total = $private_total + ($private_total * $cartRow->tax * 0.01) + $PrdList->row()->shippingcost;
        $message .= '
                <tr>
                  <td>&nbsp;</td>
                </tr>
                ';

        $message .= '
                <tr>
                  <td width="30%">&nbsp;</td>
                  <td width="30%" style="font-size:14px; font-weight:bold; color:#000000;"  > Subscription Date</td>
                  <td width="40%" align="left" style="font-size:12px; font-weight:bold; color:#000000;">' . date("F j, Y, g:i a", strtotime($PrdList->row()->created)) . '</td>
                </tr>';

        $shipAddRess = $this->order_model->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));


        $message .= '<tr>
                  <td width="30%">&nbsp;</td>
                  <td width="30%" style="font-size:14px; font-weight:bold; color:#000000;" > Tax</td>
                  <td width="40%" align="left" style="font-size:12px; font-weight:bold; color:#000000;">$ ' . $PrdList->row()->tax . ' </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td width="20%" style="font-size:14px; font-weight:bold; color:#000000;" > Total </td>
                  <td width="28%" align="left" style="font-size:18px; font-weight:bold; color:#000000;">$ ' . number_format($private_total + $tax, 2, '.', ' ') . '</td>
                </tr>
				</table>
				
<div style="display:inline-block; float:left; width:100%; font-size:12px;">
	<div style="display:inline-block; float:left; width:50%;">

	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr style="border:1px solid #7DA2C1;">
                      <td style=" font:bold 14px/34px Arial, Helvetica, sans-serif;	color:#000;	background:#7DA2C1; border-bottom:1px solid #b6b3b3;">Billing Details</td>
                    </tr>
                  </table>
               
               
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-left:15px; font-family:Verdana, Arial, Helvetica, sans-serif;">
                    <tr>
                      <td width="100" style="color:#000000;"><strong>Full name </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->full_name) . '</td>
                    </tr>
                   
                    <tr>
                      <td style="color:#000000;"><strong>Address</strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->address) . '</td>
                    </tr>
                   
                    <tr>
                      <td style="color:#000000;"><strong>Country</strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->country) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>State</strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->state) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>City </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->city) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>postal code </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->postal_code) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>Phone </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($PrdList->row()->phone_no) . '</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td><b>Support Team</b></td>
                    </tr>
                    <tr>
                      <td style="font-size:16px; font-weight:bold; color:#935435;"><strong> ' . $this->config->item('email_title') . ' Team</strong></td>
                    </tr>
                  </table>
</div>
<div style="display:inline-block; float:left; width:50%;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr style="border:1px solid #b6b3b3;">
                      <td style=" font:bold 14px/34px Arial, Helvetica, sans-serif;	color:#000;	background:#7DA2C1; border-bottom:1px solid #b6b3b3;">Shipping Details</td>
                    </tr>
                  </table>
               
               
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-left:15px; font-family:Verdana, Arial, Helvetica, sans-serif;">
                    <tr>
                      <td width="120" style="color:#000000;"><strong>Full name </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->full_name) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>Address</strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->address1) . '</td>
                    </tr>
                   
                    <tr>
                      <td style="color:#000000;"><strong>Country</strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->country) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>State/province </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->state) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>City </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->city) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>Zip/postal code </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->postal_code) . '</td>
                    </tr>
                    <tr>
                      <td style="color:#000000;"><strong>Phone </strong></td>
                      <td>:</td>
                      <td style="color:#000000; font-weight:bold;">' . stripslashes($shipAddRess->row()->phone) . '</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
</div>
</div>
         </div>				
				
					<table border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">	
							<tr>
								<td width="50%" valign="top" style="font-size:12px; padding:10px 15px;">
									
								</td>
								<td width="50%" valign="top" style="font-size:12px; padding:10px 15px;">
									<p>
										
									</p>
									<p>
										
									</p>
								</td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
			</body>
			</html>';
        echo $message;




        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $this->config->item('site_contact_mail'),
            'mail_name' => $this->config->item('email_title'),
            'to_mail_id' => $PrdList->row()->email,
            'cc_mail_id' => $this->config->item('site_contact_mail'),
            'subject_message' => $subject,
            'body_messages' => $message
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);



        return;
    }

    /*     * ******************************************** View Orders ********************************************** */

    public function view_orders($userid, $randomId) {

        $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
        $PrdList = $this->db->get();

        $shipAddRess = $this->order_model->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));


        $newsid = '19';
        $template_values = $this->get_newsletter_template_details($newsid);
        $adminnewstemplateArr = array(
            'logo' => $this->data['logo'],
            'meta_title' => $this->config->item('meta_title'),
            'ship_fullname' => stripslashes($shipAddRess->row()->full_name),
            'ship_address1' => stripslashes($shipAddRess->row()->address1),
            'ship_address2' => stripslashes($shipAddRess->row()->address2),
            'ship_city' => stripslashes($shipAddRess->row()->city),
            'ship_country' => stripslashes($shipAddRess->row()->country),
            'ship_state' => stripslashes($shipAddRess->row()->state),
            'ship_postalcode' => stripslashes($shipAddRess->row()->postal_code),
            'ship_phone' => stripslashes($shipAddRess->row()->phone),
            'bill_fullname' => stripslashes($PrdList->row()->full_name),
            'bill_address1' => stripslashes($PrdList->row()->address),
            'bill_address2' => stripslashes($PrdList->row()->address2),
            'bill_city' => stripslashes($PrdList->row()->city),
            'bill_country' => stripslashes($PrdList->row()->country),
            'bill_state' => stripslashes($PrdList->row()->state),
            'bill_postalcode' => stripslashes($PrdList->row()->postal_code),
            'bill_phone' => stripslashes($PrdList->row()->phone_no),
            'invoice_number' => $PrdList->row()->dealCodeNumber,
            'invoice_date' => date("F j, Y g:i a", strtotime($PrdList->row()->created))
        );
        extract($adminnewstemplateArr);
        $subject = $template_values['news_subject'];
        $message1 .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta name="viewport" content="width=device-width"/></head>
<title>' . $template_values['news_subject'] . '</title>
<body>

';
        include('./newsletter/registeration' . $newsid . '.php');

        $message = stripslashes(substr($message, 0, strrpos($message, '</tbody>')));
        $message = stripslashes(substr($message, 0, strrpos($message, '</tbody>')));
        $message1 .= $message;


        $disTotal = 0;
        $grantTotal = 0;
        foreach ($PrdList->result() as $cartRow) {
            $InvImg = @explode(',', $cartRow->image);
            $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->product_shipping_cost + $cartRow->price;
            $uTot = $unitPrice * $cartRow->quantity;
            if ($cartRow->attr_name != '' || $cartRow->attr_type != '') {
                $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
            } else {
                $atr = '';
            }
            $message1 .= '<tr>
            <td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" /></span></td>
			<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . stripslashes($cartRow->product_name) . $atr . '</span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . strtoupper($cartRow->quantity) . '</span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '</span></td>
            <td style="text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($uTot, 2, '.', '') . '</span></td>
        </tr>';
            $grantTotal = $grantTotal + $uTot;
        }
        $private_total = $grantTotal - $PrdList->row()->discountAmount;
        $private_total = $private_total + $PrdList->row()->tax + $PrdList->row()->shippingcost;

        $message1 .= '</table></td> </tr><tr><td colspan="3"><table border="0" cellspacing="0" cellpadding="0" style=" margin:10px 0px; width:99.5%;"><tr>
			<td width="460" valign="top" >';
        if ($PrdList->row()->note != '') {
            $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
                <td width="87" ><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
               
            </tr>
			<tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($PrdList->row()->note) . '</span></td>
            </tr></table>';
        }

        if ($PrdList->row()->order_gift == 1) {
            $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
            </tr></table>';
        }

        $message1 .= '</td>
            <td width="174" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cecece;">
            <tr bgcolor="#f3f3f3">
                <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Sub Total</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</span></td>
            </tr>
			<tr>
                <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Discount Amount</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->discountAmount, '2', '.', '') . '</span></td>
            </tr>
		<tr bgcolor="#f3f3f3">
            <td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Shipping Cost</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->shippingcost, 2, '.', '') . '</span></td>
              </tr>
			  <!--<tr>
            <td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Shipping Tax</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->tax, 2, '.', '') . '</span></td>
              </tr>-->
			  <tr bgcolor="#f3f3f3">
                <td width="87" style="border-right:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">Grand Total</span></td>
                <td width="31"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</span></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
        </tr>
    </table>
        </div>
        
        <!--end of left--> 
		<div style="width:50%; float:left;">
            	<div style="float:left; width:100%;font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; width:100%; color:#000000; line-height:38px; "><span>' . stripslashes($PrdList->row()->full_name) . '</span>, thank you for your order.</div>
               <ul style="width:100%; margin:10px 0px 0px 0px; padding:0; list-style:none; float:left; font-size:12px; font-weight:normal; line-height:19px; font-family:Arial, Helvetica, sans-serif; color:#000;">
                    <li>Price mentioned are inclusive of applicable Govt. taxes.</li>
                    <li>If you have any concerns please contact us.</li>
                    <li>Email: <span>' . stripslashes($this->data['siteContactMail']) . ' </span></li>
                    <li>Phone: +91 8010845000</li>
               </ul>
        	</div>
            
            <div style="width:27.4%; margin-right:5px; float:right;">
            
           
            </div>
        
        <div style="clear:both"></div>
        
    </div>
    </div></body></html>';
        return $message1;
    }

    public function view_orders_new($userid, $randomId) {

        $this->db->select('p.*,u.email,u.full_name,u.address,u.address2,u.phone_no,u.postal_code,u.state,u.country,u.city,u.s_tin_no,u.s_vat_no,u.s_cst_no,pd.product_name,pd.id as PrdID,pd.image,pd.seller_product_id,pd.sku,pAr.attr_name as attr_type,sp.attr_name');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.sell_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');

        $PrdList = $this->db->get();


        $shipAddRess = $this->order_model->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));
        if($PrdList->row()->address2 !=''){
    $new_address = $PrdList->row()->address.'<br/><br/><strong style="font-weight:600;">Address2:</strong> '.$PrdList->row()->address2;
}else {  $new_address = $PrdList->row()->address; }

if($shipAddRess->row()->address2 != ''){
    $new_ship_address = $shipAddRess->row()->address1.'<br/><br/><strong style="font-weight:600;">Address2:</strong> '.$shipAddRess->row()->address2;
}else {  $new_ship_address = $shipAddRess->row()->address1; }

        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta name="viewport" content="width=device-width"/></head>
 <head>
  
  <title>Order Confirmation</title>
  <style type="text/css">
@media screen and (max-width: 580px) {
.tab-container {
    max-width: 100%;
}
.txt-pad-15 {
    padding: 0 15px 51px 15px !important;
}
.foo-txt {
    padding: 0px 15px 18px 15px !important;
}
.foo-add {
    padding: 15px 15px 0px 15px !important;
}
.foo-add-left {
    width: 100% !important;
}
.foo-add-right {
    width: 100% !important;
}
.pad-bottom-15 {
    padding-bottom: 15px !important;
}
.pad-20 {
    padding: 25px 20px 20px !important;
}
}
</style>
  </head>
  <body style="margin:0px;">
   <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="width: 850px;background-color: #fff;margin: 0 auto;table-layout: fixed;background:#dbdbdb;font-family:Arial, Helvetica, sans-serif;font-size:15px;">
    <tr>
      <td style="padding:20px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td style="text-align: left;width: 100%;padding-bottom:25px;background:#90c5bc;padding:20px;"><a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/email/logo.png" alt="logo"></a></td>
        </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td style="text-align: left;width: 100%;background:#f5f5f5;padding:25px 30px 30px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="text-align: left;width: 100%;padding-bottom:10px;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-weight:bold;font-size:15px;color:#656565;">Hi '.strtoupper(stripslashes($shipAddRess->row()->full_name)).'</p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-size:15px;color:#656565">Thank you for your order.</p></td>
              </tr>
              </table>


            <!-- order and date table -->
              
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                <tr>
                  <td style="text-align: left;padding:15px 20px;width:50%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Id</strong><span style="font-weight:400;"> : #'.$PrdList->row()->dealCodeNumber.'</span> </p></td>
                  <td style="text-align: left;padding:15px 20px;border-left:1px solid #ddd;width:50%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Date </strong><span style="font-weight:400;"> : '.date("j F, Y g:i a",strtotime($PrdList->row()->created)).'</span> </p></td>
                </tr>
              </table>


                <!-- address table -->
              
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                <tr>
                  <td style="text-align: left;padding:15px 20px;width:100%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Shipping Details</strong> </p></td>
                </tr>
                <tr>
                  <td style="text-align: left;padding:15px 20px;border-top:1px solid #ddd;width:100%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Full Name</strong> <span style="font-weight:400;"> : '.stripslashes($shipAddRess->row()->full_name).'</span> </p>
                   <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Address</strong> <span style="font-weight:400;"> : '.stripslashes($new_ship_address).'</span> </p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> 
                    <strong style="font-weight:600;">City</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.stripslashes($shipAddRess->row()->city).'</span> <strong style="font-weight:600;">State</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.stripslashes($shipAddRess->row()->state).'</span> <strong style="font-weight:600;">Zip Code</strong> <span style="font-weight:400;display:inline-block;"> : '.stripslashes($shipAddRess->row()->postal_code).'</span> </p>
                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Phone Number</strong> <span style="font-weight:400;"> : '.stripslashes($shipAddRess->row()->phone).'</span> </p></td>
                </tr>
              </table>

                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;color:#656565;border-color:#ddd;">
                <tr>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:100px"> Product Images </td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:120px"> Product Name </td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:35px;">QTY</td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:120px">Security Deposit</td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:110px;">Monthly Rental</td>
                  <td align="center" style="color:#656565;padding:20px 5px;font-size:12px;font-weight:600;width:120px;"> Sub Total </td>
                 </tr>';       
            
$disTotal =0; $grantTotal = 0;
$count_t = 1;
$coupon_code = '';
$discount_amount = 0;
foreach ($PrdList->result() as $cartRow) { $InvImg = @explode(',',$cartRow->image);
$unitPrice = ($cartRow->price*(0.01*$cartRow->product_tax_cost))+$cartRow->price * $cartRow->quantity;
$unitDeposit =  $cartRow->product_shipping_cost * $cartRow->quantity;
$grandDeposit = $grandDeposit + $unitDeposit;
if($cartRow->couponCode != '' && $count_t == 1){
    $coupon_code = $cartRow->couponCode;
    $discount_amount = $cartRow->discountAmount;
    $count_t++;
}
$uTot = $unitPrice + $unitDeposit;
if($cartRow->attr_name != '' || $cartRow->attr_type != ''){ $atr = '<br>'.$cartRow->attr_type.' / '.$cartRow->attr_name; }else{ $atr = '';}

$message.='<tr>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid; font-size:11px;border-color:#ddd;"><img src="'.base_url().PRODUCTPATH.$InvImg[0].'" alt="'.stripslashes($cartRow->product_name).'" alt="product" width="60" /></td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;line-height:20px;border-color:#ddd;"> '.stripslashes($cartRow->product_name).$atr.'</td>
            <td align="center" style="padding:20px 0px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;"> '.strtoupper($cartRow->quantity).'</td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">'.$this->data['currencySymbol'].number_format($unitDeposit,2,'.','').'</td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">'.$this->data['currencySymbol'].number_format($unitPrice,2,'.','').'</td>
            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;font-size:11px;border-color:#ddd;"> '.$this->data['currencySymbol'].number_format($uTot,2,'.','').' </td>
        </tr>';
    $grantTotal = $grantTotal + $uTot;
    $new_deposite +=  $unitDeposit;
    $new_rental +=  $unitPrice;
    $new_total += $uTot;

}
    $private_total = $grantTotal - $PrdList->row()->discountAmount;
    $private_total = $private_total + $PrdList->row()->tax;
     if($PrdList->row()->discount_on_si != ''){
        $private_total = $private_total - $PrdList->row()->discount_on_si;
    }



                 
$message.='<tr>
                  <td align="right" colspan="3" style="padding:20px 20px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:12px;font-weight:600;border-color:#ddd;"> TOTAL </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">  '.$this->data['currencySymbol'].number_format($new_deposite,2,'.','').' </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">'.$this->data['currencySymbol'].number_format($new_rental,2,'.','').' </td>
                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">'.$this->data['currencySymbol'].   number_format($new_total,2,'.','').'</td>
                </tr>
              </table>
               <table border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;color:#656565;border-color:#ddd;">
                <tr>
                  <td align="left" style="color:#656565;padding:14px 20px 15px 20px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;padding-left:20px;"> Sub Total </td>
                  <td align="center" style="color:#656565;padding:15px 20px;font-size:11px;font-weight:600;">'.$this->data['currencySymbol'].   number_format($grantTotal,2,'.','').'</td>
                </tr>';

                if($coupon_code != ''){

               $message.='<tr>
                  <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount <br />
                      <span style="font-size:9px !important;font-weight:400;">(Coupon Code : '.$coupon_code.')</span></p></td>
                  <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">'.$this->data['currencySymbol'].' - '.number_format($discount_amount,'2','.','').'</td>
                </tr>';
            }
            
            if($PrdList->row()->discount_on_si != ''){

               $message.='<tr>
                  <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount For SI <br />
                     </p></td>
                  <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">'.$this->data['currencySymbol'].' - '.number_format($PrdList->row()->discount_on_si,'2','.','').'</td>
                </tr>';
            }
                $message.='<tr>
                  <td align="left" style="color:#656565;padding:14px 15px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-size:14px;font-weight:600;border-color:#ddd;padding-left:20px;">GRAND TOTAL</td>
                  <td align="center" style="color:#60a196;padding:15px 20px;border-top:1px solid #ddd;font-size:15px;font-weight:600;border-color:#ddd;">'.$this->data['currencySymbol'].number_format($private_total,'2','.','').'</td>
                </tr>
              </table>
                </td>
          </tr>
          <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
            <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
              <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
                Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
          </tr>
        </table></td>
    </tr>
  </table>
</body>
</html>';
              


// <div style="width:1012px; margin:0 auto;font-size:12px;font-family: Arial, Helvetica, sans-serif;">
// <center><h4 style="margin:0 auto;">ORDER CONFIRMATION</h4></center>
// <div style="width:100%; border:1px solid #000; float:left; margin:0 auto;">
//     <div style="float:left; width:30%;"><a href="' . base_url() . '" target="_blank" id="logo"><img src="' . base_url() . 'images/logo/cityfurnish-transparent.png" alt="' . $this->data['WebsiteTitle'] . '" title="' . $this->data['WebsiteTitle'] . '" style=" height:43px; margin: 10px 10px 10px 10px;"></a></div>
// 	<div style="float:left; width:70%;">
// 		<div style="width:380px; float:right;">
// 		<table>
//         	<tr>
//         		<td>Rented From : </td>
//         		<td>' . stripslashes($PrdList->row()->full_name) . '</td>
//         	</tr>
//         	<tr>
//         		<td>GST No. :</td>
//         		<td>' . stripslashes($PrdList->row()->s_tin_no) . '/' . stripslashes($PrdList->row()->s_vat_no) . '/' . stripslashes($PrdList->row()->s_cst_no) . '</td>
//         	</tr>
        	
//     	</table>
// 		</div>
// 	</div>
// </div>			
// <!--END OF LOGO-->
    
//  <!--start of deal-->
//     <div style="width:972px;background:#FFFFFF;float:left; padding:20px; border:1px solid #454B56; ">
// 	<div style="float:left; width:100%; margin-bottom:20px; margin-right:7px;">
// 		<div style="float:left; width:49%; border:1px solid #cccccc;">
// 		<span style=" border-bottom:1px solid #cccccc; background:#f3f3f3; width:95.8%; float:left; padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#000305;">If Undelivered Return to :</span>
//         <table style="width:100%; float:left;">
//         	<tr><td>Full Name</td><td>:</td><td>' . stripslashes($PrdList->row()->full_name) . '</td></tr>
//             <tr><td style="vertical-align: baseline;width: 100px;">Address</td><td style="vertical-align: baseline;">:</td><td>' . stripslashes($PrdList->row()->address) . ',' . stripslashes($PrdList->row()->address2) . ',' . stripslashes($PrdList->row()->city) . ',' . stripslashes($PrdList->row()->state) . ',' . stripslashes($PrdList->row()->postal_code) . '</td></tr>
// 			<!--<tr><td>Address 2</td><td>:</td><td>' . stripslashes($PrdList->row()->address2) . '</td></tr>
// 			<tr><td>City</td><td>:</td><td>' . stripslashes($PrdList->row()->city) . '</td></tr>
// 			<tr><td>Country</td><td>:</td><td>' . stripslashes($PrdList->row()->country) . '</td></tr>
// 			<tr><td>State</td><td>:</td><td>' . stripslashes($PrdList->row()->state) . '</td></tr>
// 			<tr><td>Zipcode</td><td>:</td><td>' . stripslashes($PrdList->row()->postal_code) . '</td></tr>
// 			<tr><td>Phone Number</td><td>:</td><td>' . stripslashes($PrdList->row()->phone_no) . '</td></tr>-->
//     	</table>
//     	</div>
//     	<div style="float:right; width:40%;">
// 		<table style="width:100%; border:1px solid #cecece; float:left;">
// 		  	<tr bgcolor="#f3f3f3">
//             	<td width="100"  style="border-right:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Order Id</span></td>
// 	            <td  width="100"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">#' . $PrdList->row()->dealCodeNumber . '</span></td>
// 	        </tr>
// 	        <tr bgcolor="#f3f3f3">
//                 <td width="100"  style="border-right:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Order Date</span></td>
//                 <td  width="100"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . date("F j, Y g:i a", strtotime($PrdList->row()->created)) . '</span></td>
//             </tr>	 
//         </table>
//         </div>
//     </div>
		
//     <div style="float:left; width:100%;">
	
//     <div style="width:49%; float:left; border:1px solid #cccccc; margin-right:10px;">
//     	<span style=" border-bottom:1px solid #cccccc; background:#f3f3f3; width:95.8%; float:left; padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#000305;">Shipping Address</span>
//     		<div style="float:left; padding:10px; width:96%;  font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#030002; line-height:28px;">
//             	<table width="100%" border="0" cellpadding="0" cellspacing="0">
//                 	<tr><td>Full Name</td><td>:</td><td>' . stripslashes($shipAddRess->row()->full_name) . '</td></tr>
//                     <tr><td style="vertical-align: baseline;width: 100px;">Address</td><td style="vertical-align: baseline;">:</td><td>' . stripslashes($shipAddRess->row()->address1) . ', ' . stripslashes($shipAddRess->row()->address2) . ', ' . stripslashes($shipAddRess->row()->city) . ', ' . stripslashes($shipAddRess->row()->state) . ', ' . stripslashes($shipAddRess->row()->postal_code) . ', ' . stripslashes($shipAddRess->row()->country) . '</td></tr>
// 					<!--<tr><td>Address 2</td><td>:</td><td>' . stripslashes($shipAddRess->row()->address2) . '</td></tr>
// 					<tr><td>City</td><td>:</td><td>' . stripslashes($shipAddRess->row()->city) . '</td></tr>
// 					<tr><td>Country</td><td>:</td><td>' . stripslashes($shipAddRess->row()->country) . '</td></tr>
// 					<tr><td>State</td><td>:</td><td>' . stripslashes($shipAddRess->row()->state) . '</td></tr>
// 					<tr><td>Zipcode</td><td>:</td><td>' . stripslashes($shipAddRess->row()->postal_code) . '</td></tr> -->
// 					<tr><td style="vertical-align: baseline;">Phone Number</td><td>:</td><td>' . stripslashes($shipAddRess->row()->phone) . '</td></tr>
//             	</table>
//             </div>
//      </div>
    
//     <div style="width:49%; float:left; border:1px solid #cccccc;">
//     	<span style=" border-bottom:1px solid #cccccc; background:#f3f3f3; width:95.7%; float:left; padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#000305;">From :</span>
//     		<div style="float:left; padding:10px; width:96%;  font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#030002; line-height:28px;">
//             	<table width="100%" border="0" cellpadding="0" cellspacing="0">
//                 	<tr><td>Full Name</td><td>:</td><td>' . stripslashes($PrdList->row()->full_name) . '</td></tr>
//                     <tr><td style="vertical-align: baseline;width: 100px;">Address</td><td style="vertical-align: baseline;">:</td><td>' . stripslashes($PrdList->row()->address) . ',' . stripslashes($PrdList->row()->address2) . ',' . stripslashes($PrdList->row()->city) . ',' . stripslashes($PrdList->row()->state) . ',' . stripslashes($PrdList->row()->postal_code) . ',' . stripslashes($PrdList->row()->country) . '</td></tr>
// 					<!--<tr><td>Address 2</td><td>:</td><td>' . stripslashes($PrdList->row()->address2) . '</td></tr>
// 					<tr><td>City</td><td>:</td><td>' . stripslashes($PrdList->row()->city) . '</td></tr>
// 					<tr><td>Country</td><td>:</td><td>' . stripslashes($PrdList->row()->country) . '</td></tr>
// 					<tr><td>State</td><td>:</td><td>' . stripslashes($PrdList->row()->state) . '</td></tr>
// 					<tr><td>Zipcode</td><td>:</td><td>' . stripslashes($PrdList->row()->postal_code) . '</td></tr>
// 					<tr><td>Phone Number</td><td>:</td><td>' . stripslashes($PrdList->row()->phone_no) . '</td></tr>-->
//             	</table>
//             </div>
//     </div>
// </div> 
	   
// <div style="float:left; width:100%; margin-right:3%; margin-top:10px; font-size:12px; font-weight:normal; line-height:28px;  font-family:Arial, Helvetica, sans-serif; color:#000; overflow:hidden;">   
// 	<table width="100%" border="0" cellpadding="0" cellspacing="0">
//     <tr>
//     	<td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cecece; width:99.5%;">
//         <tr bgcolor="#f3f3f3">
//         	<td width="16%" style="border-right:1px solid #cecece; text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Product Image</span></td>
//         	<td width="9%" style="border-right:1px solid #cecece; text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Product ID</span></td>
//         	<td width="9%" style="border-right:1px solid #cecece; text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">SKU Code</span></td>
//             <td width="27%" style="border-right:1px solid #cecece;text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Product Name</span></td>
//             <td width="7%" style="border-right:1px solid #cecece;text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Qty</span></td>
//             <td width="10%" style="border-right:1px solid #cecece;text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Unit Deposit</span></td>
//             <td width="10%" style="border-right:1px solid #cecece;text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Unit Rental</span></td>
//             <td width="12%" style="text-align:center;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center;">Sub Total</span></td>
//          </tr>';

//         $disTotal = 0;
//         $grantTotal = 0;
//         foreach ($PrdList->result() as $cartRow) {
//             $InvImg = @explode(',', $cartRow->image);
//             $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->price;
//             $unitDeposit = $cartRow->product_shipping_cost;
//             $grandDeposit = $grandDeposit + ($unitDeposit * $cartRow->quantity);
//             $uTot = ($unitPrice + $unitDeposit) * $cartRow->quantity;
//             if ($cartRow->attr_name != '' || $cartRow->attr_type != '') {
//                 $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
//             } else {
//                 $atr = '';
//             }

//             $message .= '<tr>
//             <td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" /></span></td>
//             <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . stripslashes($cartRow->seller_product_id) . '</span></td>
//             <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . stripslashes($cartRow->sku) . '</span></td>
// 			<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . stripslashes($cartRow->product_name) . $atr . '</span></td>
//             <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . strtoupper($cartRow->quantity) . '</span></td>
//             <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($unitDeposit, 2, '.', '') . '</span></td>
//             <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '</span></td>
//             <td style="text-align:center;border-top:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($uTot, 2, '.', '') . '</span></td>
//         </tr>';
//             $grantTotal = $grantTotal + $uTot;
//         }
//         $private_total = $grantTotal - $PrdList->row()->discountAmount;
//         $private_total = $private_total + $PrdList->row()->tax;

//         $message .= '</table></td> </tr><tr><td colspan="3"><table border="0" cellspacing="0" cellpadding="0" style=" margin:10px 0px; width:99.5%;"><tr>
// 			<td width="460" valign="top" >';
//         if ($PrdList->row()->note != '') {
//             $message .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
//                 <td width="87" ><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
               
//             </tr>
// 			<tr>
//                 <td width="87"  style="border:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($PrdList->row()->note) . '</span></td>
//             </tr></table>';
//         }


//         if ($PrdList->row()->order_gift == 1) {
//             $message .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
//                 <td width="87"  style="border:1px solid #cecece;"><span style="font-size:14px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
//             </tr></table>';
//         }

//         $message .= '</td>
//             <td width="174" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cecece;">
//             <tr bgcolor="#f3f3f3">
//                 <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Sub Total</span></td>
//                 <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</span></td>
//             </tr>';
//         if ($PrdList->row()->couponCode != '') {
//             $message .= '	<tr>
//                 <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Coupon Code Used</span></td>
//                 <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $PrdList->row()->couponCode . '</span></td>
//             </tr>';
//         }
//         $message .= '<tr>
//                 <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Discount Amount</span></td>
//                 <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->discountAmount, '2', '.', '') . '</span></td>
//             </tr>
// 		<tr bgcolor="#f3f3f3">
//             <td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Deposit</span></td>
//                 <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($grandDeposit, 2, '.', '') . '</span></td>
//               </tr>
// 			  <!--<tr>
//             	<td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Shipping Tax</span></td>
//                 <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->tax, 2, '.', '') . '</span></td>
//               </tr>-->
// 			  <tr bgcolor="#f3f3f3">
//                 <td width="87" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">Grand Total</span></td>
//                 <td width="31" style="border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</span></td>
//               </tr>
//               <tr>
//                 <td width="87" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">Amount Paid</span></td>
//                 <td width="31" style="border-bottom:1px solid #cecece;"><span style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;font-weight:800">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</span></td>
//               </tr>
//             </table></td>
//             </tr>
//         </table></td>
//         </tr>
//     </table>
//         </div>
        
//         <!--end of left--> 
//         <div style="width:100%; margin-right:5px; float:left; border-bottom:1px dashed #575757;">
// 	        <p>
// 		        <p> Have a question? Our customer service is here to help you -  +91- 80108 45 000</p>
//                         <p> Price mentioned are inclusive of applicable Govt. taxes</p>
//                         <br /></br>
// 	        </p>
//         </div>
//         <div style="clear:both"></div>
//     </div>
//     </div>
// <div style="float:left; width:100%; margin-right:3%; margin-top:10px; font-size:12px; font-weight:normal; line-height:28px;  font-family:Arial, Helvetica, sans-serif; color:#000; overflow:hidden;">
// </div>
// </div></body></html>';

        return $message;
    }

    public function getCurrentSuccessOrders($userid, $dealCode) {

        $this->db->select('p.*,u.email,u.full_name,u.address,u.address2,u.phone_no,u.s_tin_no,u.s_vat_no,u.s_cst_no,pd.product_name,pd.id as PrdID,pd.description as product_description,pd.subproducts,pd.image,pd.seller_product_id,pd.sku,pAr.attr_name as attr_type,sp.attr_name,sh.address1 as ship_address1,sh.address2 as ship_address2, sh.city as ship_city, sh.state as ship_state, sh.postal_code as ship_postal_code, sh.country as ship_country, sh.phone as ship_phone, sh.full_name as ship_full_name, pd.package_discount, pd.subproduct_quantity');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
        $this->db->join(SHIPPING_ADDRESS . ' as sh', 'sh.id=p.shippingid', 'left');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $dealCode . '" and p.status="Paid"');

        $currentOrderDetails = $this->db->get();
        //echo "query: " . $this->db->last_query();
        return $currentOrderDetails->result_array();
    }
    
     public function insert_new_bulk_order($data){
       $this->db->insert('fc_bulk_order',$data);
       return $this->db->insert_id();
    }
    public function get_bulk_mail(){
        $this->db->select('bulk_order_mail');
        $this->db->from('fc_admin_settings');
        $data = $this->db->get();
        return $data->row()->bulk_order_mail;
    }
    public function send_bulk_data_email($id){
        $this->db->select('*');
        $this->db->from('fc_bulk_order');
        $this->db->where('id',$id);
        $bulk_data = $this->db->get();
        $bulk_to_email = $this->get_bulk_mail();

        $message ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml"><head>
                <meta name="viewport" content="width=device-width"/></head>
                    <head>
                        
                        <title>Bulk Order Information</title>
                        <style type="text/css">
                        @media screen and (max-width: 580px) {
                        .tab-container {
                            max-width: 100%;
                        }
                        .txt-pad-15 {
                            padding: 0 15px 51px 15px !important;
                        }
                        .foo-txt {
                            padding: 0px 15px 18px 15px !important;
                        }
                        .foo-add {
                            padding: 15px 15px 0px 15px !important;
                        }
                        .foo-add-left {
                            width: 100% !important;
                        }
                        .foo-add-right {
                            width: 100% !important;
                        }
                        .pad-bottom-15 {
                            padding-bottom: 15px !important;
                        }
                        .pad-20 {
                            padding: 25px 20px 20px !important;
                        }
                        }
                        </style>
                        </head>
                    <body style="margin:0px;">
                        <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="width: 850px;background-color: #fff;margin: 0 auto;table-layout: fixed;background:#dbdbdb;font-family:Arial, Helvetica, sans-serif;font-size:15px;">
                            <tr>
                                <td style="padding:20px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left;width: 100%;padding-bottom:25px;background:#90c5bc;padding:20px;"><a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/email/logo.png" alt="logo"></a></td>
                            </tr>
                        </table>
                        <!-- address table -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                            <tr>
                                <td style="text-align: left;padding:15px 20px;width:100%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Bulk Order Detail</strong> </p></td>
                            </tr>
                            <tr>
                                <td style="text-align: left;padding:15px 20px;border-top:1px solid #ddd;width:100%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Full Name</strong> <span style="font-weight:400;"> : '.$bulk_data->row()->name.'</span> </p>
                                   <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Email</strong> <span style="font-weight:400;"> :  '.$bulk_data->row()->email.'</span> </p>
                                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> 
                                    <strong style="font-weight:600;">Phone</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> :  '.$bulk_data->row()->phone.'</span>   <strong style="font-weight:600;">City</strong> <span style="font-weight:400;display:inline-block;"> :  '.$bulk_data->row()->city.'</span> </p>
                                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Message</strong> <span style="font-weight:400;"> :  '.$bulk_data->row()->message.'</span> </p></td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                            <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
                                <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
                              <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
                                Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
                            </tr>
                        </table>
                    </body>
                </html>';

        $email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Bulk Order',
                         'to_mail_id'=>$bulk_to_email,
                         'subject_message'=>'Bulk Order Request',
                         'body_messages'=>$message
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);
       return  $email_send_to_common;
       
    }
    
    public function Check_zoho_exists($data){
        $this->db->select('*');
        $this->db->from('fc_zoho_crm');
        $this->db->where('order_id',$data['order_id']);
        $this->db->where('request_type',$data['request_type']);
        $this->db->where('zoho_case_id != ""');
        $this->db->where('status !="Closed"');
        $response = $this->db->get();;
        return $response;
    }

    public function Update_Zoho($zoho_id){
        $array =  array('status' => 'Closed' );
        $this->db->where('zoho_case_id',$zoho_id);
        return  $this->db->update('fc_zoho_crm',$array);

    }

    public function Insert_New_Zoho($data){
       return  $this->db->insert('fc_zoho_crm',$data);
    }
    public function GetSiREcords(){
            $this->db->select('dealCodeNumber order_id,payer_email email, mandate_id mandate_id,card_token card_token'); 
            $this->db->from(PAYMENT);
            $this->db->where('status','paid');
            $this->db->where('is_recurring',1);
            $this->db->where('payer_email !=','');
            $this->db->group_by('dealCodeNumber');
            $this->db->order_by('created','desc');
            $response = $this->db->get();
            return $response;

    }

    public function get_customer_payment(){
         $this->db->select('order_id order_id,ccp_email email, mandate_id mandate_id,card_token card_token,ccp_id'); 
            $this->db->from(CP_CUSTOMER_PAYMENT);
             $this->db->where('is_recurring',1);
             $this->db->where('ccp_email !=','');
            $this->db->order_by('ccp_created_on','desc');
            $response = $this->db->get();
            return $response;

    }

    public function customer_payment(){
         $this->db->select('*'); 
            $this->db->from(CP_CUSTOMER_PAYMENT);
            $this->db->where('is_recurring !=',1);
            $this->db->order_by('ccp_id','desc');
            $response = $this->db->get();
            return $response;
    }
    
    public function Get_subproducts($where){
        $this->db->select('*');
        $this->db->from(PRODUCT);
        $this->db->where_in('id',$where);
        $response = $this->db->get();
        $data = $response->result();
        return $data;

    }
    public function UpdateAdminSetting($data,$condition){
        $this->db->where($condition);
        return $this->db->update(ADMIN_SETTINGS,$data);

    }
    public function GetPriceOnTenure($attr_name,$product_id){
        $this->db->select('*');
        $this->db->from(SUBPRODUCT);
        $this->db->where('attr_name',$attr_name);
        $this->db->where('product_id',$product_id);
        $response = $this->db->get();
        return $response->row();
    }
    
        //view payout details 
    public function view_order_payout_details($status,$count = 0,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL) {
        $column_order = array('p.id','u.email','u.mobile_no','p.created','p.dealCodeNumber','p.total','p.shippingcost'); //set column field database for datatable orderable
        $this->db->DISTINCT();
        if($count != NULL){ 
            $this->db->select( 'count(p.id) as count' , FALSE);
        }else{
        $this->db->select('p.*,DATE_FORMAT(p.created," %Y-%m-%d %h:%i:%s %p") as created,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.id as PrdID,u.id as uid');
            $this->db->limit($length, $start);
            $this->db->group_by("p.dealCodeNumber");
            $this->db->order_by($column_order[$orderIndex],$orderType );
        }    
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        // $this->db->join(PAYOUT . ' as payout', 'payout.order_id = p.id','left');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->where('p.status ="' . $status . '"');
        if($searchValue != NULL && $searchValue != ''){
            $sortQry = "(u.email LIKE '%$searchValue%' or p.id LIKE '%$searchValue%' or p.dealCodeNumber LIKE '%$searchValue%' or p.couponCode LIKE '%$searchValue%' or p.total LIKE '%$searchValue%' or p.shippingcity LIKE '%$searchValue%')";
            $this->db->where( $sortQry );
        }
        $query = $this->db->get();
        $result=$query->result();
        // print_r($result);exit;
        return $result;
    }
    
    
    
     //view customer payment details 
    public function view_customer_payment_details($status,$count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL) {

        $column_order = array('ccp_email,mandate_id'); //set column field database for datatable orderable
        $this->db->DISTINCT();
        // print_r($count);exit;
        if($count != NULL){ 
            $this->db->select( 'count(ccp_id) as count' , FALSE);
        }else{
            // DATE_FORMAT(payout.payout_date," %Y-%m-%d %h:%i:%s %p") as payout_date
        $this->db->select('*');
            $this->db->limit($length, $start);
            // print_r($column_order);
            $this->db->order_by($column_order[$orderIndex],$orderType );
        }    
        $this->db->from(CP_CUSTOMER_PAYMENT);
        $this->db->where('is_recurring !=',1);
        if($searchValue != NULL && $searchValue != ''){
            $sortQry = "(ccp_email LIKE '%$searchValue%' or mandate_id LIKE '%$searchValue%')";
            $this->db->where( $sortQry );
        }
        $query = $this->db->get();
        $result=$query->result();
        return $result;
    }


    public function get_user_order_details($order_id){
        $this->db->DISTINCT();
        $this->db->select('payout.payout_status,payout.payout_amount,p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.id as PrdID,u.id as uid'); 
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->join(PAYOUT . ' as payout', 'payout.order_id = p.id','left');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->where('p.id = "' . $order_id . '"');
        $query = $this->db->get();
        $result=$query->row();
        return $result;
    }

    public function update_shipped_payout($res,$post){
            $payout_insert = array(
                'order_id' => $post['order_id'],
                'user_id' => $post['user_id'],
                'payout_amount' => $post['payout_amount'],
                // 'deduct_amount' => $post['deduct_amount'],
                'payout_status' => 'Link Send',
                'cashgramId' => $post['cashgramId'],
                'cashgram_link' => $res->data->cashgramLink,
                'customer_remark' => $post['customer_remark'],
                'link_expired_date' => $post['expire_time']
            );
            return $this->db->insert(PAYOUT,$payout_insert);
    }

    public function get_all_payout_details(){
        $this->db->select('*');
        $this->db->from(PAYOUT);
        $this->db->where('payout_status !=','Expired');
        $this->db->where('payout_status !=','Paid');
        return $this->db->get();
    }

    public function Get_shipping_amount_with_payout_listing($order_id){
        $this->db->select('payout.*,p.shippingcost');
        $this->db->from(PAYOUT. ' as payout');
        $this->db->join(PAYMENT. ' as p','p.id = payout.order_id','left');
        $this->db->where('payout.order_id',$order_id);
        return $this->db->get();
    }

    public function get_user_cashgram_details($cashgramId){
        $this->db->select('payout.user_id,payout.link_expired_date,payout.payout_status,payout.payout_amount,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city'); 
        $this->db->from(PAYOUT . ' as payout');
        $this->db->join(USERS . ' as u', 'payout.user_id = u.id');
        $this->db->where('payout.cashgramId = "' . $cashgramId . '"');
        $query = $this->db->get();
        $result=$query->row();
        return $result;
    }
        public function get_productname($product_ids){
        $this->db->select('product_name');
        $this->db->from(PRODUCT);
        $this->db->where_in('id',$product_ids);
        $data = $this->db->get();
        return $data->result_array();
    }

    public function get_faild_order($user_id){
        $this->db->select('dealCodeNumber,total');
        $this->db->from(PAYMENT);
        $this->db->where('user_id',$user_id);
        $this->db->order_by('created','desc');
        $this->db->limit(1);
        $data = $this->db->get();
        return $data->row();
    }
    
    //view payout details 
    public function get_listing_bulk_orders($status,$count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL) {
        $column_order = array('id','name','email','phone','city','message'); //set column field database for datatable orderable
        $this->db->DISTINCT();

        if($count != NULL){ 
            $this->db->select( 'count(id) as count' , FALSE);
        }else{
            // DATE_FORMAT(payout.payout_date," %Y-%m-%d %h:%i:%s %p") as payout_date
        $this->db->select('id,name,email,phone,city,message');
        $this->db->limit($length, $start);
        $this->db->order_by($column_order[$orderIndex],$orderType );
        }    
        $this->db->from(BULK_ORDER);
        if($searchValue != NULL && $searchValue != ''){
            $sortQry = "(name LIKE '%$searchValue%' or email LIKE '%$searchValue%' or phone LIKE '%$searchValue%' or city LIKE '%$searchValue%')";
            $this->db->where( $sortQry );
        }
        $query = $this->db->get();
        $result=$query->result();
        return $result;
    }
    
    
    //view furniture order details 
    public function get_listing_office_furniture_orders($count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL) {
        $column_order = array('id','user_name','email','phone','city','remark','order_date'); //set column field database for datatable orderable
        $this->db->DISTINCT();

        if($count != NULL){ 
            $this->db->select( 'count(id) as count' , FALSE);
        }else{
            // DATE_FORMAT(payout.payout_date," %Y-%m-%d %h:%i:%s %p") as payout_date
        $this->db->select('id,user_name,email,phone,city,remark,order_date');
        $this->db->limit($length, $start);
       $this->db->order_by($column_order[$orderIndex],$orderType );
        }    
        $this->db->from(OFFICE_FURNITURE_ORDERS);
        if($searchValue != NULL && $searchValue != ''){
            $sortQry = "(user_name LIKE '%$searchValue%' or email LIKE '%$searchValue%' or phone LIKE '%$searchValue%' or city LIKE '%$searchValue%')";
            $this->db->where( $sortQry );
        }
        $query = $this->db->get();
        $result=$query->result();
        return $result;
    }

    public function new_failed_order_email($PrdList, $SellList) {
      $adminSettings = $this->getAdminSettings();
      $mailIds = 'hello@cityfurnish.com';
      if(trim($adminSettings->row()->failed_emails) != '') {
        $mailIds = $adminSettings->row()->failed_emails;
      }
      $shipAddRess = $this->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));
      $newsid = '19';
      $template_values = $this->get_newsletter_template($newsid);
      $adminnewstemplateArr = array(
          'logo' => $this->data['logo'],
          'meta_title' => $this->config->item('meta_title'),
          'ship_fullname' => stripslashes($shipAddRess->row()->full_name),
          'ship_address1' => stripslashes($shipAddRess->row()->address1),
          'ship_address2' => stripslashes($shipAddRess->row()->address2),
          'ship_city' => stripslashes($shipAddRess->row()->city),
          'ship_country' => stripslashes($shipAddRess->row()->country),
          'ship_state' => stripslashes($shipAddRess->row()->state),
          'ship_postalcode' => stripslashes($shipAddRess->row()->postal_code),
          'ship_phone' => stripslashes($shipAddRess->row()->phone),
          'bill_fullname' => stripslashes($shipAddRess->row()->full_name),
          'bill_address1' => stripslashes($shipAddRess->row()->address1),
          'bill_address2' => stripslashes($shipAddRess->row()->address2),
          'bill_city' => stripslashes($shipAddRess->row()->city),
          'bill_country' => stripslashes($shipAddRess->row()->country),
          'bill_state' => stripslashes($shipAddRess->row()->state),
          'bill_postalcode' => stripslashes($shipAddRess->row()->postal_code),
          'bill_phone' => stripslashes($shipAddRess->row()->phone),
          'invoice_number' => $PrdList->row()->dealCodeNumber,
          'invoice_date' => date("F j, Y g:i a", strtotime($PrdList->row()->created))
      );
      extract($adminnewstemplateArr);
      $subject = $template_values['news_subject'];

     $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
       <html xmlns="http://www.w3.org/1999/xhtml">
       <head>
       
       <title>Order Confirmation</title>
       <style type="text/css">
       @media screen and (max-width: 580px) {
       .tab-container {
       max-width: 100%;
       }
       .txt-pad-15 {
       padding: 0 15px 51px 15px !important;
       }
       .foo-txt {
       padding: 0px 15px 18px 15px !important;
       }
       .foo-add {
       padding: 15px 15px 0px 15px !important;
       }
       .foo-add-left {
       width: 100% !important;
       }
       .foo-add-right {
       width: 100% !important;
       }
       .pad-bottom-15 {
       padding-bottom: 15px !important;
       }
       .pad-20 {
       padding: 25px 20px 20px !important;
       }
       }
       </style>
       </head>
       <body style="margin:0px;">
         <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="width: 850px;background-color: #fff;margin: 0 auto;table-layout: fixed;background:#dbdbdb;font-family:Arial, Helvetica, sans-serif;font-size:15px;"> <tbody>
         <tr>
           <td style="padding:20px;">

           <table width="100%" border="0" cellpadding="0" cellspacing="0">
               <tr>
               <td style="text-align: left;width: 100%;padding-bottom:25px;background:#90c5bc;padding:20px;"><a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/email/logo.png" alt="logo"></a></td>
             </tr>
           </table>

               <table width="100%" border="0" cellpadding="0" cellspacing="0">
           
               <tr>
                 <td style="text-align: left;width: 100%;background:#f5f5f5;padding:25px 30px 30px;">

                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                     <tr>
                     <td style="text-align: left;width: 100%;padding-bottom:10px;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-weight:bold;font-size:15px;color:#656565;">Hi Admin</p>
                         <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-size:15px;color:#FF0000;">Failed Order. Please check failed orders on Admin panel and contact customer.</p></td>
                   </tr>
                   </table> 
                   
                   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                     <tr>
                       <td style="text-align: left;padding:15px 20px;width:50%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Id</strong><span style="font-weight:400;"> : #'.$invoice_number.'</span> </p></td>
                       <td style="text-align: left;padding:15px 20px;border-left:1px solid #ddd;width:50%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Date </strong><span style="font-weight:400;"> : '.$invoice_date.'</span> </p></td>
                     </tr>
                   </table>
             

                   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                     <tr>
                       <td style="text-align: left;padding:15px 20px;width:100%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Shipping Details</strong> </p></td>
                     </tr>
                     <tr>
                       <td style="text-align: left;padding:15px 20px;border-top:1px solid #ddd;width:100%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Full Name</strong> <span style="font-weight:400;"> : '.$ship_fullname.'</span> </p>
                         <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"><strong style="font-weight:600;">Address</strong> <span style="font-weight:400;"> : '.$ship_address1.'</span> </p>

                         <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">City</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.$ship_city.'</span> <strong style="font-weight:600;;">State</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.$ship_state.'</span> <strong style="font-weight:600;">Zip Code</strong> <span style="font-weight:400;display:inline-block;"> : '.$ship_postalcode.'</span> </p>
                         <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Phone Number</strong> <span style="font-weight:400;"> : '.$ship_phone.'</span> </p></td>
                     </tr>
                   </table>

       <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;color:#656565;border-color:#ddd;">
                     <tr>
                       <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:100px;"> Product Images </td>
                       <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;f\ont-weight:600;border-color:#ddd;width:120px;"> Product Name </td>
                       <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:35px;">QTY</td>
                       <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:120px;">Security Deposit</td>
                       <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:110px;">Monthly Rental</td>
                       <td align="center" style="color:#656565;padding:20px 5px;font-size:12px;font-weight:600;width:120px;"> Sub Total </td>
                     </tr>';

             $message1 .= $message;
             $disTotal = 0;
             $grantTotal = 0;
              foreach ($PrdList->result() as $cartRow) {
                  $InvImg = @explode(',', $cartRow->image);
                  $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->price * $cartRow->quantity;
                  $unitDeposit = $cartRow->product_shipping_cost * $cartRow->quantity ;
                  $uTot = $unitPrice + $unitDeposit;
                  if ($cartRow->attr_name != '' || $cartRow->attr_type != '') {
                      $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
                  } else {
                      $atr = '';
                  }
                  $message1 .= '<tr>
                  <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid; font-size:11px;border-color:#ddd;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" />
                  </td>
                  <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;line-height:20px;border-color:#ddd;">' . stripslashes($cartRow->product_name) .'<br/>'. $atr .' 
                  </td>
                  <td align="center" style="padding:20px 0px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . strtoupper($cartRow->quantity) . '
                  </td>
                  <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitDeposit, 2, '.', '') . '
                  </td>
                  <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '
                  </td>
                  <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] .number_format($uTot, 2, '.', '')  . '</td>
                  </tr>';
                      $grantTotal = $grantTotal + $uTot;
                      $grandDeposit = $grandDeposit + $unitDeposit;
                      $total_deposite  += $unitDeposit;
                      $total_rental +=  $unitPrice;
                      $total_total += $uTot;
              }

             $private_total = $grantTotal - $PrdList->row()->discountAmount;
             $private_total = $private_total + $PrdList->row()->tax;
             if($PrdList->row()->discount_on_si != ''){
                 $private_total = $private_total - $PrdList->row()->discount_on_si;
             }
             $message1 .= '<tr>
                       <td align="right" colspan="3" style="padding:20px 20px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:12px;font-weight:600;border-color:#ddd;"> TOTAL </td>
                       <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] .  number_format($total_deposite, 2, '.', '') . ' </td>
                       <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;"> ' . $this->data['currencySymbol'] . number_format($total_rental, 2, '.', '') . ' </td>
                       <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($total_total, 2, '.', '') . '</td>
                 </tr>
             </table>';
             if ($PrdList->row()->note != '') {
                 $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
                     <td width="87" ><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
                 </tr>
           <tr>
                     <td width="87"  style="border:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($PrdList->row()->note) . '</span></td>
                 </tr></table>';
             }
             if ($PrdList->row()->order_gift == 1) {
                 $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
                     <td width="87"  style="border:1px solid #cecece;"><span style="font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
                 </tr></table>';
             }
             $message1 .= '<table border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;color:#656565;border-color:#ddd;">
                 <tr>
                     <td align="left" style="color:#656565;padding:14px 20px 15px 20px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;padding-left:20px;"> Sub Total </td>

                     <td><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</span></td>
                 </tr>';
             if ($PrdList->row()->couponCode != '') {
                 $message1 .= '<tr>
                     <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount <br />
                           <span style="font-size:9px !important;font-weight:400;">(Coupon Code : '. $PrdList->row()->couponCode .')</span></p></td>
                       <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . '-'.number_format($PrdList->row()->discountAmount, '2', '.', '') . '</td>
                 </tr>';
             }

             if ($PrdList->row()->discount_on_si != '') {
                 $message1 .= '<tr>
                     <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount For SI<br />
                           </p></td>
                       <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . '-'.number_format($PrdList->row()->discount_on_si, '2', '.', '') . '</td>
                 </tr>';
             }
             

             $message1 .= '<tr>
                       <td align="left" style="color:#656565;padding:14px 15px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-size:14px;font-weight:600;border-color:#ddd;padding-left:20px;">GRAND TOTAL</td>
                       <td align="center" style="color:#60a196;padding:15px 20px;border-top:1px solid #ddd;font-size:15px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</td>
                     </tr>
                 </tr>
             </table>
                 </td>
               </tr>
               <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
                 <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
                   <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
                     Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
               </tr>
       </table>
       </td>
       </tr>
       </table>
       </body>
       </html>';
       
       $sender_email = $this->data['siteContactMail'];
       $sender_name = 'Admin';

       $email_values = array('mail_type' => 'html',
           'from_mail_id' => $sender_email,
           'mail_name' => $sender_name,
           'to_mail_id' => $mailIds,
           'subject_message' => '**Cityfurnish- Failed Order Alert - '.$ship_fullname.'**',
           'body_messages' => $message1,
           'mail_id' => 'Checkout Mail'
       );
       $sent = $this->failed_order_email_send($email_values);
       if(!$sent) {
           return false;
       } else {
           return true;
       }
       
    }
    
    public function SnedOfflineOrderEmail($userid,$randomId){
         //Send Mail to Use
        $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
        $PrdList = $this->db->get();

        $this->db->select('p.sell_id,p.couponCode,u.email');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.sell_id = u.id');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
        $this->db->group_by("p.sell_id");
        $SellList = $this->db->get();
        $this->SendMailUSers($PrdList, $SellList);
    }

    public function getOfflineOrderOwnerDetails($dealCodeNumber){
        $this->db->select('user.full_name,user.email,offline.advance_rental');
        $this->db->from(USERS.' as user');
        $this->db->join(OFFLINE_ORDERS.' as offline','user.id = offline.user_id');
        $this->db->where('offline.dealCodeNumber',$dealCodeNumber);
        $data = $this->db->get();
        return $data;
    }

}

?>