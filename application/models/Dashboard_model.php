<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard_model extends My_Model
{
	public function __construct() 
	{
		parent::__construct();
	}
	
	function getCountDetails($tableName='',$fieldName='',$whereCondition=array())
	{
		$this->db->select($fieldName);
		$this->db->from($tableName);
		$this->db->where($whereCondition);
		
		//$this->db->where(JOB.".dateAdded >= DATE_SUB(NOW(),INTERVAL 30 DAY)", NULL, FALSE);
		$countQuery = $this->db->get();
		return $countQuery->num_rows();
	}
	
	function getRecentDetails($tableName='',$fieldName='',$userOrderBy='',$userLimit='',$whereCondition=array())
	{
		$this->db->select('*');
		$this->db->from($tableName);
		$this->db->where($whereCondition);
		$this->db->order_by($fieldName, $userOrderBy);
		$this->db->limit($userLimit);
		$countQuery = $this->db->get();
		return $countQuery->result_array();
	}
	
	function getTodayUsersCount($tableName='',$fieldName='',$whereCondition=array())
	{
		$this->db->select($fieldName);
		$this->db->from($tableName);
		$this->db->where($whereCondition);
		
		$this->db->where("created >= DATE_SUB(NOW(),INTERVAL 24 HOUR)", NULL, FALSE);
		//$this->db->like("created",date('Y-m-d', strtotime('-24 hours')));
		$countQuery = $this->db->get();
		return $countQuery->num_rows();
	}
	
	function getThisMonthCount($tableName='',$fieldName='',$whereCondition=array())
	{
		$this->db->select($fieldName);
		$this->db->from($tableName);
		$this->db->where($whereCondition);
		
		$this->db->where("created >= DATE_SUB(NOW(),INTERVAL 30 DAY)", NULL, FALSE);
		$countQuery = $this->db->get();
		return $countQuery->num_rows();
	}
	
	function getLastYearCount($tableName='',$fieldName='',$whereCondition=array())
	{
		$this->db->select($fieldName);
		$this->db->from($tableName);
		$this->db->where($whereCondition);
		//date("Y");
		$this->db->like('created', date("Y"));
		$countQuery = $this->db->get();
		return $countQuery->num_rows();
		
	}
	
	function getDashboardOrderDetails()
	{
		$this->db->from(PAYMENT);
		$this->db->order_by('id','desc');
		$this->db->limit(3);
		$this->db->group_by('dealCodeNumber');
		$orderQueryDashboard = $this->db->get();
		//print_r($this->db->last_query());exit;
		return $orderQueryDashboard->result_array();
		//$this->db->where($whereCondition);
	}
	
	function get_all_order_status_records($status,$oldStatus=null)
	{
		$this->db->select('*');
		$this->db->from(ORDER_STATUS_TAT);
		$this->db->where('status_from',$status);
		$data = $this->db->get()->row();
		$duration = $data->duration;

		$this->db->select('order_track.id,order_track.new_status,order_track.order_id,order_track.updated_date as invoice_date,(select last_date.updated_date from fc_track_order_notification as last_date where last_date.order_id = order_track.order_id AND last_date.new_status = "'.$oldStatus.'" ORDER BY order_track.id desc limit 1) as updated_date');
		$this->db->from(TRACK_ORDER_NOTIFICATION.' as order_track');
		$this->db->where('order_track.new_status',$status);
		$this->db->group_by('order_track.order_id');
		$this->db->order_by('id','desc');

		$data = $this->db->get();
		$responseData = $data->result_array();
		$returnArray['OutTan'] = array();
		$returnArray['inTan'] = array();

		foreach ($responseData as $key => $value) {
			$ger_all_orders = $this->check_latestAvailable($value['order_id']);
			if($value['new_status'] != $ger_all_orders->new_status){
				// print_r("expression");exit;
				unset($responseData[$key]);
			}
		}

		foreach ($responseData as $key => $value) {

			if($value['updated_date'] == null || $value['updated_date'] == ''){
				$seconds = strtotime(date('Y-m-d h:i')) - strtotime($value['invoice_date']);
			}else{
				$seconds = strtotime($value['updated_date']) - strtotime($value['invoice_date']);
			}
			$hours = $seconds / 60 /  60;
			$responseData['date_diff'] = $hours;
			if($hours < $duration){
				$returnArray['inTan'][$key] = $value;
			}else{
				$returnArray['OutTan'][$key] = $value;
			}
		}
		return $returnArray;
	}


	public function check_latestAvailable($order_id){
		$this->db->select('*');
		$this->db->from(TRACK_ORDER_NOTIFICATION);
		$this->db->where('order_id',$order_id);
		$this->db->order_by('id','desc');
		$this->db->limit('1');
		$data = $this->db->get();
		return $data->row();
	}

	public function get_orderDetails($orderids,$currentStatus,$oldStatus){
		$this->db->select('order_track.owner_name,order_track.order_id,DATE_FORMAT((select last_date.updated_date from fc_track_order_notification as last_date where last_date.order_id = order_track.order_id AND last_date.new_status = "'.$oldStatus.'" ORDER BY order_track.id desc limit 1), "%M %d %Y %h:%I %p") as updated_date, DATE_FORMAT(updated_date, "%M %d %Y %h:%I %p") as invoice_date');
		$this->db->from(TRACK_ORDER_NOTIFICATION. ' as order_track');
		$this->db->where_in('order_track.id',$orderids);
		$data = $this->db->get();
		return $data->result_array();

	}

	public function get_order_reports($filter= null,$limit = 10,$offset= 0,$orderIndex = 2, $orderType = 'DESC', $searchValue = NULL,$count = NULL){
		$column_order = array('t1.order_id','t1.new_status','t1.sub_status','t1.owner_name',null,null,null);
		if($count != NULL  && $count != ''){
            	$Query = "SELECT t1.* FROM fc_track_order_notification as t1 where t1.id IN (SELECT max(id) from fc_track_order_notification  GROUP BY order_id ) AND date_format(updated_date,'%Y-%m-%d') >= '".$filter['start_date']."' AND date_format(updated_date,'%Y-%m-%d') <= '".$filter['end_date']."' ";
		}else{
    		if($searchValue != NULL && $searchValue != ''){
    		 	$Query = "SELECT t1.* FROM fc_track_order_notification as t1 where t1.id IN (SELECT max(id) from fc_track_order_notification  GROUP BY order_id ) AND date_format(updated_date,'%Y-%m-%d') >= '".$filter['start_date']."' AND date_format(updated_date,'%Y-%m-%d') <= '".$filter['end_date']."' AND order_id LIKE '%$searchValue%' or new_status LIKE '%$searchValue%' or sub_status  LIKE '%$searchValue%' or owner_name  LIKE '%$searchValue%' GROUP BY order_id ORDER BY ".$column_order[$orderIndex].' '.$orderType."  limit ".$offset.",".$limit." ";
            }
            else{
            	$Query = "SELECT t1.* FROM fc_track_order_notification as t1 where t1.id IN (SELECT max(id) from fc_track_order_notification  GROUP BY order_id ) AND date_format(updated_date,'%Y-%m-%d') >= '".$filter['start_date']."' AND date_format(updated_date,'%Y-%m-%d') <= '".$filter['end_date']."' ORDER BY ".$column_order[$orderIndex].' '.$orderType."  limit ".$offset.",".$limit."";
            }
		}
        $qu = $this->db->query($Query);
		return $qu->result_array();
	}

	public function getorderhistoryDetails($order_id){
		$this->db->select('*');
		$this->db->from(TRACK_ORDER_NOTIFICATION);	
		$this->db->where('order_id',$order_id);
		$data = $this->db->get();
		return $data;

	}

}