<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to seller requests
 * @author Teamtweaks
 *
 */
class Seller_model extends My_Model
{
	public function __construct() 
	{
		parent::__construct();
	}
	
	/**
    * 
    * Getting Sellers details
    * @param String $condition
    */
    public function get_sellers_details($condition=''){
   		$Query = " select * from ".USERS." ".$condition;
   		return $this->ExecuteQuery($Query);
    }
   
    public function get_all_sellers($count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL)
    {
        $column_order = array('full_name','user_name','email'); //set column field database for datatable orderable
        $this->db->DISTINCT();
        if($count != NULL){ 
            $this->db->select( 'count(id) as count' , FALSE);
        }else{
            $this->db->select('*');
            $this->db->limit($length, $start);
        }    
        $this->db->from(USERS);
        $this->db->where('group','Seller');
        $this->db->order_by('created','desc' );
        if($searchValue != NULL && $searchValue != ''){
            $sortQry = "(email LIKE '%$searchValue%' or full_name LIKE '%$searchValue%' or user_name LIKE '%$searchValue%'  or  id LIKE '%$searchValue%')";
            $this->db->where( $sortQry );
        }
        $query = $this->db->get();
        $result=$query->result();
        return $result;
    }
	
}