<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to couponcards management
 * @author Teamtweaks
 *
 */
class Couponcards_model extends My_Model
{


public function view_category_list($CatRow,$val){
	$SubcatView ='';
	$SubcatView .= '<span class="cat'.$val.'"><input name="category_id[]" class="checkbox" type="checkbox" value="'.$CatRow->id.'" tabindex="7"><strong>'.$CatRow->cat_name.' &nbsp;</strong></span>';
	return $SubcatView;					
	}
	
	public function view_edit_category_list($CatRow,$val,$ArrVal){
	$SubcatView ='';
	if(in_array($CatRow->id,$ArrVal)){ $Cond = 'checked="checked"';}else{ $Cond = '';}
	
	$SubcatView .= '<span class="cat'.$val.'"><input name="category_id[]" class="checkbox" type="checkbox" value="'.$CatRow->id.'" '.$Cond.' tabindex="7"><strong>'.$CatRow->cat_name.' &nbsp;</strong></span>';
	return $SubcatView;					
	}
	
	
	public function view_category_details(){
	
		$select_qry = "select * from ".CATEGORY." where rootID=0";
		$categoryList = $this->ExecuteQuery($select_qry);
		$catView='';$Admpriv = 0;$SubPrivi = '';

		foreach ($categoryList->result() as $CatRow){
			
			$catView .= $this->view_category_list($CatRow,'1');		
			
			$sel_qry = "select * from ".CATEGORY." where rootID='".$CatRow->id."'  ";	
			$SubList = $this->ExecuteQuery($sel_qry);	
				
			foreach ($SubList->result() as $SubCatRow){
					
				$catView .= $this->view_category_list($SubCatRow,'2');	
					
				$sel_qry1 = "select * from ".CATEGORY." where rootID='".$SubCatRow->id."'  ";	
				$SubList1 = $this->ExecuteQuery($sel_qry1);	
					
				foreach ($SubList1->result() as $SubCatRow1){
					$catView .= $this->view_category_list($SubCatRow1,'3');	
					
					$sel_qry2 = "select * from ".CATEGORY." where rootID='".$SubCatRow1->id."'  ";	
					$SubList2 = $this->ExecuteQuery($sel_qry2);	
		
					foreach ($SubList2->result() as $SubCatRow2){
						$catView .= $this->view_category_list($SubCatRow2,'4');	

					}			
				}
			}
		}
					
		return $catView;
	}
	
	public function view_edit_category_details($ArrVal){
	
		$select_qry = "select * from ".CATEGORY." where rootID=0";
		$categoryList = $this->ExecuteQuery($select_qry);
		$catView='';$Admpriv = 0;$SubPrivi = '';

		foreach ($categoryList->result() as $CatRow){
			
			$catView .= $this->view_edit_category_list($CatRow,'1',$ArrVal);		
			
			$sel_qry = "select * from ".CATEGORY." where rootID='".$CatRow->id."'  ";	
			$SubList = $this->ExecuteQuery($sel_qry);	
				
			foreach ($SubList->result() as $SubCatRow){
					
				$catView .= $this->view_edit_category_list($SubCatRow,'2',$ArrVal);	
					
				$sel_qry1 = "select * from ".CATEGORY." where rootID='".$SubCatRow->id."'  ";	
				$SubList1 = $this->ExecuteQuery($sel_qry1);	
					
				foreach ($SubList1->result() as $SubCatRow1){
					$catView .= $this->view_edit_category_list($SubCatRow1,'3',$ArrVal);	
					
					$sel_qry2 = "select * from ".CATEGORY." where rootID='".$SubCatRow1->id."'  ";	
					$SubList2 = $this->ExecuteQuery($sel_qry2);	
		
					foreach ($SubList2->result() as $SubCatRow2){
						$catView .= $this->view_edit_category_list($SubCatRow2,'4',$ArrVal);	

					}			
				}
			}
		}
					
		return $catView;
	}

	public function view_product_details($ArrVal = ''){
	

		$select_qry = "select * from ".PRODUCT." where status='Publish'";
		$productList = $this->ExecuteQuery($select_qry);
		$catView = '';
		
		if($ArrVal!=''){
			foreach ($productList->result() as $PrdRow){
				if(in_array($PrdRow->id, $ArrVal)==1){ $condT = ' checked="checked"';}else{ $condT = ''; }
				$catView .= '<span class="prd"><input name="product_id[]" class="checkbox" type="checkbox" value="'.$PrdRow->id.'" '.$condT.' tabindex="7"><strong>'.$PrdRow->product_name.' &nbsp;</strong></span>';
			}
		}else{
			foreach ($productList->result() as $PrdRow){
				$catView .= '<span class="prd"><input name="product_id[]" class="checkbox" type="checkbox" value="'.$PrdRow->id.'" tabindex="7"><strong>'.$PrdRow->product_name.' &nbsp;</strong></span>';
			}
		}			
		return $catView;
	}

    public function view_seller_details($ArrVal = ''){
	

		$select_qry = 'select * from '.USERS.' where `status`="Active" and `group`="Seller"';
		$productList = $this->ExecuteQuery($select_qry);
		$catView = '';
		
		if($ArrVal!=''){
			foreach ($productList->result() as $PrdRow){
				if(in_array($PrdRow->id, $ArrVal)==1){ $condT = ' checked="checked"';}else{ $condT = ''; }
				$catView .= '<span class="prd"><input name="user_id[]" class="checkbox" type="checkbox" value="'.$PrdRow->id.'" '.$condT.' tabindex="7"><strong>'.$PrdRow->user_name.' &nbsp;</strong></span>';
			}
		}else{
			foreach ($productList->result() as $PrdRow){
				$catView .= '<span class="prd"><input name="user_id[]" class="checkbox" type="checkbox" value="'.$PrdRow->id.'" tabindex="7"><strong>'.$PrdRow->user_name.' &nbsp;</strong></span>';
			}
		}			
		return $catView;
	}
// 	public function get_used_card_details($user_id){
// 		$new_id = explode(',', $user_id);
// 		$this->db->select('id,email,user_name');
// 		$this->db->from(USERS);
// 		$this->db->where_in('id',$new_id);
// 		$data = $this->db->get();
// 		return $data->result();
// 	}

    public function get_used_card_details($referral_code){
		$this->db->select('used_by');
		$this->db->from(REFERRAL_CODE);
		$this->db->where('referral_code',$referral_code);
		$result = $this->db->get();
		$used_by = unserialize($result->row()->used_by);
		$new_array = [];
		foreach ($used_by as $key => $value) {
			$this->db->select('id,email,user_name');
			$this->db->from(USERS);
			$this->db->where('id',$value['user_id']);
			$data  = $this->db->get();
			$new_array[$key]['id'] = $data->row()->id;
			$new_array[$key]['user_name'] = $data->row()->user_name;
			$new_array[$key]['email'] = $data->row()->email;
			$new_array[$key]['referral_amount'] = $value['amount'];
		}
		return $new_array;
	}

    public function getReferralCodeDetails($count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL){
		$column_order = array('referral.id','referral.referral_code','user.email',null,'referral.count','referral.used_by','referral.created_date','referral.created_date',null); //set column field database for datatable orderable
     	$this->db->DISTINCT();
	    if($count != NULL){ 
	      $this->db->select( 'count(referral.id) as count' , FALSE);
	    }else{
			$this->db->select('referral.*,user.email');
	    }    

		$this->db->from(REFERRAL_CODE.' as referral');
		$this->db->join(USERS.' as user','user.id = referral.user_id','left');
       	$this->db->order_by($column_order[$orderIndex],$orderType );
    	$this->db->limit($length, $start);
        if($searchValue != NULL && $searchValue != ''){
		    $sortQry = "(user.email LIKE '%$searchValue%' or referral.referral_code LIKE '%$searchValue%' or referral.count LIKE '%$searchValue%')";
		    $this->db->where( $sortQry );
	    }
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
		//get all coupons
	public function get_couponcards_details($count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL) {

        $column_order = array(null,'code','price_type','coupon_type','price_value','remaning','purchase_count','datefrom','dateto'); //set column field database for datatable orderable
        $this->db->DISTINCT();
        if($count != NULL){ 
          	$this->db->select( 'count(id) as count' , FALSE);
        }else{
		    $this->db->select('*,(quantity - purchase_count) AS remaning');
		    $this->db->limit($length, $start);
		    $this->db->order_by($column_order[$orderIndex],$orderType );
        }    
        $this->db->from(COUPONCARDS);
        if($searchValue != NULL && $searchValue != ''){
            $sortQry = "(code LIKE '%$searchValue%' or coupon_type LIKE '%$searchValue%')";
            $this->db->where( $sortQry );
        }
        $query = $this->db->get();
        $result=$query->result();
        return $result;
    }
}