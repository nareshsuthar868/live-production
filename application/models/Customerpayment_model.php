<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This model contains all db functions related to product management
 * @author Teamtweaks
 *
 */
class Customerpayment_model extends My_Model
{

	public function add_customer_payment($dataArr=''){
		$this->db->insert(CUSTOMER_PAYMENT,$dataArr);
	}
	public function add_cp_customer_payment($dataArr=''){
		$this->db->insert(CP_CUSTOMER_PAYMENT,$dataArr);
	}
}