<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This model contains all common db related functions
 * @author Teamtweaks
 *
 */
class My_Model extends CI_Model {

	/**
	 *
	 * This function connect the database and load the functions from CI_Model
	 */
	public function __construct()
	{
		parent::__construct();
		//		$this->load->database();
	}

	/**
	 *
	 * This function returns the table contents based on data
	 * @param String $table	->	Table name
	 * @param Array $condition	->	Conditions
	 * @param Array $sortArr	->	Sorting details
	 *
	 * return Array
	 */
	public function get_all_details($table='',$condition='',$sortArr=''){
		if ($sortArr != '' && is_array($sortArr)){
			foreach ($sortArr as $sortRow){
				if (is_array($sortRow)){
					$this->db->order_by($sortRow['field'],$sortRow['type']);
				}
			}
		}
		return $this->db->get_where($table,$condition);
	}

	/**
	 *
	 * This function update the table contents based on params
	 * @param String $table		->	Table name
	 * @param Array $data		->	New data
	 * @param Array $condition	->	Conditions
	 */
	public function update_details($table='',$data='',$condition=''){
		$this->db->where($condition);
		return $this->db->update($table,$data);
	}

	/**
	 *
	 * Simple function for inserting data into a table
	 * @param String $table
	 * @param Array $data
	 */
	public function simple_insert($table='',$data=''){
		//echo "<pre>";print_r($data);die;
		return $this->db->insert($table,$data);
	}

	/**
	 *
	 * This function do all insert and edit operations
	 * @param String $table		->	Table name
	 * @param String $mode		->	insert, update
	 * @param Array $excludeArr
	 * @param Array $dataArr
	 * @param Array $condition
	 */
	public function commonInsertUpdate($table='',$mode='',$excludeArr='',$dataArr='',$condition=''){
		$inputArr = array();
		foreach ($this->input->post() as $key => $val){
			if (!in_array($key, $excludeArr)){
				$inputArr[$key] = $val;
			}
		}
		$finalArr = array_merge($inputArr,$dataArr);
		if ($mode == 'insert'){
			return $this->db->insert($table,$finalArr);
		}else if ($mode == 'update'){
			$this->db->where($condition);
			return $this->db->update($table,$finalArr);
		}


	}

	/**
	 *
	 * For getting last insert id
	 */
	public function get_last_insert_id(){
		return $this->db->insert_id();
	}

	/**
	 *
	 * This function do the delete operation
	 * @param String $table
	 * @param Array $condition
	 */
	public function commonDelete($table='',$condition=''){
		$this->db->delete($table,$condition);
	}

	/**
	 *
	 * This function return the admin settings details
	 */
	public function getAdminSettings(){
		$this->db->select('*');
		$this->db->where(ADMIN.'.id','1');
		$this->db->from(ADMIN_SETTINGS);
		$this->db->join(ADMIN,ADMIN.'.id = '.ADMIN_SETTINGS.'.id');

		$result = $this->db->get();
		unset($result->row()->admin_password);
		return $result;
	}

	/**
	 *
	 * This function change the status of records and delete the records
	 * @param String $table
	 * @param String $column
	 */
	public function activeInactiveCommon($table='', $column=''){
		$data =  $_POST['checkbox_id'];
		for ($i=0;$i<count($data);$i++){
			if($data[$i] == 'on'){
				unset($data[$i]);
			}
		}
		$mode  = $this->input->post('statusMode');
		$AdmEmail  = strtolower($this->input->post('SubAdminEmail'));
		/*$getAdminSettingsDetails = $this->getAdminSettings();
		 $config = '<?php ';
		 foreach($getAdminSettingsDetails ->row() as $key => $val){
			$value = addslashes($val);
			$config .= "\n\$config['$key'] = '$value'; ";
			}
			$file = 'fc_admin_action_settings.php';
			file_put_contents($file, $config);
			vinu@teamtweaks.com
			*/


		$json_admin_action_value = file_get_contents('fc_admin_action_settings.php');
		if($json_admin_action_value !=''){
			$json_admin_action_result = unserialize($json_admin_action_value);
		}
			
		foreach ($json_admin_action_result as $valds) {
			$json_admin_action_result_Arr[] = $valds;
		}

		if(sizeof($json_admin_action_result)>29){
			unset($json_admin_action_result_Arr[1]);
		}

		$json_admin_action_result_Arr[] = array($AdmEmail,$mode,$table,$data,date('Y-m-d H:i:s'),$_SERVER['REMOTE_ADDR']);

			
		$file = 'fc_admin_action_settings.php';
		file_put_contents($file, serialize($json_admin_action_result_Arr));
			

		$this->db->where_in($column,$data);
		if (strtolower($mode) == 'delete'){
			$this->db->delete($table);
		}else {
			$statusArr = array('status' => $mode);
			$this->db->update($table,$statusArr);
		}
	}

	/**
	 *
	 * Common function for selecting records from table
	 * @param String $tableName
	 * @param Array $paraArr
	 */
	public function selectRecordsFromTable($tableName,$paraArr){
		extract($paraArr);
		$this->db->select($selectValues);
		$this->db->from($tableName);

		if(!empty($whereCondition))
		{
			$this->db->where($whereCondition);
		}

		if(!empty($sortArray))
		{
			foreach($sortArray as $key=>$val)
			{
				$this->db->order_by($key,$val);
			}
		}

		if($perpage !='')
		{
			$this->db->limit($perpage,$start);
		}

		if(!empty($likeQuery))
		{
			$this->db->like($likeQuery);
		}
		$query = $this->db->get();

		return $result = $query->result_array();

	}

	/**
	 *
	 * Common function for executing mysql query
	 * @param String $Query	->	Mysql Query
	 */
	public function ExecuteQuery($Query){
		return $this->db->query($Query);
	}

	/**
	 *
	 * Category -> product count function
	 * @param String $res	->product category colum values
	 * @param String $id	->Category id
	 */
	public function productPerCategory($res,$id){

		$option_exp="";
			
		echo '<pre>'; $res->num_rows;
		print_r($res);  die;

		for($i=0;$i<=count($res->num_rows);$i++){
			$option_exp .= $res[$i]['category_id'].",";
		}

		$option_exploded = explode(',',$option_exp);
		$valid_option =array_filter($option_exploded);
		$occurences = array_count_values($valid_option);
			
		if($occurences[$id] == ''){
			return '0';
		}else{
			return $occurences[$id];
		}

	}

        public function items_in_cart($userid = 0){
            $this->db->select('a.*,b.product_name,b.seourl,b.image,b.is_addon,b.id as prdid,b.price as orgprice,c.attr_name as attr_type,d.attr_name');
            $this->db->from(SHOPPING_CART . ' as a');
            $this->db->join(PRODUCT . ' as b', 'b.id = a.product_id');
            $this->db->join(SUBPRODUCT . ' as d', 'd.pid = a.attribute_values', 'left');
            $this->db->join(PRODUCT_ATTRIBUTE . ' as c', 'c.id = d.attr_id', 'left');
            $this->db->where('a.user_id = ' . $userid);
            $cartVal = $this->db->get();
            return $cartVal->result();
        }

        public function mini_cart_view($userid = '',$mini_cart_lg=array()){

		extract($mini_cart_lg);
			
		$minCartVal = ''; $GiftMiniValue = ''; $CartMiniValue = ''; $SubscribMiniValue = '';  $minCartValLast = ''; $giftMiniAmt = 0; $cartMiniAmt = 0; $SubcribMiniAmt = 0; $cartMiniQty = 0;

		$giftMiniSet = $this->minicart_model->get_all_details(GIFTCARDS_SETTINGS,array( 'id' => '1'));
		$giftMiniRes = $this->minicart_model->get_all_details(GIFTCARDS_TEMP,array( 'user_id' => $userid));
		$shipMiniVal = $this->minicart_model->get_all_details(SHIPPING_ADDRESS,array( 'user_id' => $userid));
		$SubcribeMiniRes = $this->minicart_model->get_all_details(FANCYYBOX_TEMP,array( 'user_id' => $userid));


		$this->db->select('a.*,b.product_name,b.seourl,b.image,b.id as prdid,b.price as orgprice');
		$this->db->from(SHOPPING_CART.' as a');
		$this->db->join(PRODUCT.' as b' , 'b.id = a.product_id');
		$this->db->where('a.user_id = '.$userid);
		$cartMiniVal = $this->db->get();

			
		if($cartMiniVal -> num_rows() > 0 ){
			$s=0;
			foreach ($cartMiniVal->result() as $CartRow){
					
				$newImg = @explode(',',$CartRow->image);
				$cartMiniAmt = $cartMiniAmt + $CartRow->indtotal;

		$CartMiniValue.= '<li class="relative"  id="cartMindivId_'.$s.'">
			<div style="display:none"></div>
			<div class="clearfix lh_small">
				<a href="things/'.$CartRow->prdid.'/'.$CartRow->seourl.'" class="f_left m_right_10 d_block"><img src="images/site/blank.gif" style="background-image:url('.PRODUCTPATH.$newImg[0].');width: 60px;height: 60px;background-size:cover;" alt="'.$CartRow->product_name.'"></a>
					<a href="things/'.$CartRow->prdid.'/'.$CartRow->seourl.'" class="fs_medium second_font color_dark sc_hover d_block m_bottom_4">'.$CartRow->product_name.'</a>
				    	<p class="fs_medium"><span>'.$CartRow->quantity.' X </span> <span>'.$this->data['currencySymbol'].$CartRow->price.'</span></p>
			</div>
			<hr class="divider_white m_top_15 m_bottom_0">
			
		</li>';
		
				$cartMiniQty = $cartMiniQty + $CartRow->quantity;
				$s++;
			}
		}


		if($SubcribeMiniRes -> num_rows() > 0 ){
			$s=0;
			foreach ($SubcribeMiniRes->result() as $SubCribRow){
					
				$SubscribMiniValue.= '<div id="SubcribtMinidivId_'.$s.'"><table><tbody><tr>
        	<th class="info"><a href="fancybox/'.$SubCribRow->fancybox_id.'/'.$SubCribRow->seourl.'"><img src="images/site/blank.gif" style="background-image:url('.FANCYBOXPATH.$SubCribRow->image.')" alt="'.$SubCribRow->name.'"><strong>'.$SubCribRow->name.'</strong></a></th>
            <td class="qty">1</td>
            <td class="price">'.$this->data['currencySymbol'].number_format($SubCribRow->price,2,'.','').'</td>
		</tr></tbody></table></div>';
				$SubcribMiniAmt = $SubcribMiniAmt + $SubCribRow->price;
				$s++;
			}


		}

		if($giftMiniRes -> num_rows() > 0 ){
			$k=0;
			foreach ($giftMiniRes->result() as $giftRow){
					
				$GiftMiniValue.= '<div id="GiftMindivId_'.$k.'"><table><tbody><tr>
        	<th class="info"><a href="gift-cards"><img src="images/site/blank.gif" style="background-image:url('.GIFTPATH.$giftMiniSet->row()->image.')" alt="'.$giftMiniSet->row()->title.'"><strong>'.$giftMiniSet->row()->title.'</strong><br>'.$giftRow->recipient_name.'</a></th>
            <td class="qty">1</td>
            <td class="price">'.$this->data['currencySymbol'].number_format($giftRow->price_value,2,'.','').'</td>
		</tr></tbody></table></div>';
				$giftMiniAmt = $giftMiniAmt + $giftRow->price_value;
				$k++;
			}


		}

		$countMiniVal = $giftMiniRes -> num_rows() + $cartMiniQty + $SubcribeMiniRes-> num_rows() ;

		if($countMiniVal == 0){
			$cartMiniDisp= '<li class="relative open_mini_shopping_cart" id="MiniCartViewDisp" style="margin-top: -5px;">
											<button class="scheme_color active_lbrown tr_all tooltip_container" data-open-dropdown="#shopping_cart">
												<i class="fa fa-shopping-cart fa-lg"></i>
												<span class="tooltip top fs_small color_white hidden animated" data-show="fadeInDown" data-hide="fadeOutUp">Your Cart</span>
											<sup id="Shop_MiniId_count">'.$countMiniVal.' '.$lg_items.'</sup>
											</button>
							</li>';
		}else{

			$minCartVal.= '<li class="relative open_mini_shopping_cart" id="MiniCartViewDisp"  style="margin-top: -5px;">
											<button class="scheme_color active_lbrown tr_all tooltip_container" data-open-dropdown="#shopping_cart">
												<i class="fa fa-shopping-cart fa-lg"></i>
												<span class="tooltip top fs_small color_white hidden animated" data-show="fadeInDown" data-hide="fadeOutUp">Your Cart</span>
											<sup id="Shop_MiniId_count">'.$countMiniVal.' '.$lg_items.'</sup>
											</button>
											<div id="shopping_cart" data-show="fadeInUp" data-hide="fadeOutDown" class="bg_grey_light dropdown animated">
											<div class="sc_header">Recently added item(s)</div>
											<hr class="divider_white">
											<ul class="shopping_cart_list m_bottom_4" style="overflow-y: auto;
    max-height: 200px;
    overflow-x: hidden;">';
										


			$totalMiniCartAmt = $giftMiniAmt + $cartMiniAmt + $SubcribMiniAmt;



			$cartMiniDisp1 = $minCartVal.$CartMiniValue.$SubscribMiniValue.$GiftMiniValue.$minCartValLast;

			$minCartValLast.=	'</ul>
								<ul class="fs_medium second_font color_dark m_bottom_15 summary">
												<li><span class="d_inline_b total_title">Total</span><span class="fw_light">'.$this->data['currencySymbol'].'<span id="MCartGAmt">'.number_format($totalMiniCartAmt,2,'.','').'</span></span></li>
								</ul>
								<a href="cart/" role="button" class="t_align_c tt_uppercase w_full second_font d_block fs_medium button_type_2 lbrown tr_all">'.$lg_proceed.'</a>
								</div></li>';
			$cartMiniDisp = $minCartVal.$CartMiniValue.$minCartValLast;
		}

		return $cartMiniDisp;
	}

	/**
	 *
	 * Retrieve records using where_in
	 * @param String $table
	 * @param Array $fieldsArr
	 * @param String $searchName
	 * @param Array $searchArr
	 * @param Array $joinArr
	 * @param Array $sortArr
	 * @param Integer $limit
	 *
	 * @return Array
	 */
	public function get_fields_from_many($table='',$fieldsArr='',$searchName='',$searchArr='',$joinArr='',$sortArr='',$limit='',$condition=''){
		if ($searchArr != '' && count($searchArr)>0 && $searchName != ''){
			$this->db->where_in($searchName, $searchArr);
		}
		if ($condition != '' && count($condition)>0){
			$this->db->where($condition);
		}
		$this->db->select($fieldsArr);
		$this->db->from($table);
		if ($joinArr != '' && is_array($joinArr)){
			foreach ($joinArr as $joinRow){
				if (is_array($joinRow)){
					$this->db->join($joinRow['table'],$joinRow['on'],$joinRow['type']);
				}
			}
		}
		if ($sortArr != '' && is_array($sortArr)){
			foreach ($sortArr as $sortRow){
				if (is_array($sortRow)){
					$this->db->order_by($sortRow['field'], $sortRow['type']);
				}
			}
		}
		if ($limit!=''){
			$this->db->limit($limit);
		}
		return $this->db->get();
	}

	public function get_total_records($table='',$condition=''){
		$Query = 'SELECT COUNT(*) as total FROM '.$table.' '.$condition;
		return $this->ExecuteQuery($Query);
	}

	public function common_email_send($eamil_vaues = array())
	{
	   //  var_dump($eamil_vaues);exit;

		/*		echo  'From : '.$eamil_vaues['from_mail_id'].' <'.$eamil_vaues['mail_name'].'><br/>'.
		 'To   : '.$eamil_vaues['to_mail_id'].'<br/>'.
		 'Subject : '.$eamil_vaues['subject_message'].'<br/>'.
		 'Message : '.trim(stripslashes($eamil_vaues['body_messages']));die;*/
			//var_dump($eamil_vaues);exit;
		//Prevent mail for pleasureriver
		$server_ip = $this->input->ip_address();
		$mail_id = '';
		if ($demoserverChk){
			if (isset($eamil_vaues['mail_id'])){
				$mail_id = $eamil_vaues['mail_id'];
			}
		}else {
			$mail_id = 'set';
		}

		if ($mail_id != ''){
			if (is_file('./fc_smtp_settings.php'))
			{
				include('fc_smtp_settings.php');
			}


			// Set SMTP Configuration

			if($config['smtp_user'] != '' && $config['smtp_pass'] != ''){
				$emailConfig = array(
					'protocol' => 'smtp',
					'smtp_host' => $config['smtp_host'],
					'smtp_port' => $config['smtp_port'],
					'smtp_user' => $config['smtp_user'],
					'smtp_pass' => $config['smtp_pass'],
					 'auth' => true,
				);
			}

			// Set your email information
			$from = array('email' => $eamil_vaues['from_mail_id'],'name' => $eamil_vaues['mail_name']);
			$to = $eamil_vaues['to_mail_id'];
			$subject = $eamil_vaues['subject_message'];
			$message = stripslashes($eamil_vaues['body_messages']);
			// Load CodeIgniter Email library

			if($config['smtp_user'] != '' && $config['smtp_pass'] != ''){

				$this->load->library('email', $emailConfig);

			}else {
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					
				// Additional headers
				$headers .= 'To: '.$eamil_vaues['to_mail_id']. "\r\n";
				$headers .= 'From: '.$eamil_vaues['mail_name'].' <'.$eamil_vaues['from_mail_id'].'>' . "\r\n";
				if($eamil_vaues['cc_mail_id'] != '')
				{
					$headers .= 'Cc: '.$eamil_vaues['cc_mail_id']. "\r\n";
				}
					
				// Mail it
				mail($eamil_vaues['to_mail_id'], trim(stripslashes($eamil_vaues['subject_message'])), trim(stripslashes($eamil_vaues['body_messages'])), $headers);
				return 1;
			}

			// Sometimes you have to set the new line character for better result

			$this->email->set_newline("\r\n");
			// Set email preferences
			$this->email->set_mailtype($eamil_vaues['mail_type']);
			$this->email->from($from['email'],$from['name']);
			$this->email->to($to);
		
			if($eamil_vaues['cc_mail_id'] != '')
			{
				$this->email->cc($eamil_vaues['cc_mail_id']);
			}
			$this->email->subject($subject);

			if(isset($eamil_vaues['dynamicforgpasslink']) && !empty($eamil_vaues['dynamicforgpasslink'])){
                            $message = str_replace('dynamicforgpasslink', $eamil_vaues['dynamicforgpasslink'], $message);
                        }

			$this->email->message($message);
			
			if(isset($eamil_vaues['attachment1'])){
			  	$this->email->attach(CSV_FILE_PATH.$eamil_vaues['attachment1']);
			}
			if(isset($eamil_vaues['attachment2'])){
			 	$this->email->attach(CSV_FILE_PATH.$eamil_vaues['attachment2']); 	
			}
			
			// Ready to send email and check whether the email was successfully sent

			if (!$this->email->send(FALSE)) {
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					
				// Additional headers
				$headers .= 'To: '.$eamil_vaues['to_mail_id']. "\r\n";
				$headers .= 'From: '.$eamil_vaues['mail_name'].' <'.$eamil_vaues['from_mail_id'].'>' . "\r\n";
				if($eamil_vaues['cc_mail_id'] != '')
				{
					$headers .= 'Cc: '.$eamil_vaues['cc_mail_id']. "\r\n";
				}

				// Mail it
				mail($eamil_vaues['to_mail_id'], trim(stripslashes($eamil_vaues['subject_message'])), trim(stripslashes($eamil_vaues['body_messages'])), $headers);
				return 1;

			}
			else {
				// Show success notification or other things here
				//echo 'Success to send email';

					


				return 1;
			}
		}else {
			return 1;
		}
	}
	//get newsletter template
	public function get_newsletter_template_details($apiId='')
	{
		$twitterQuery = "select * from ".NEWSLETTER." where id=".$apiId. " AND status='Active'";
		$twitterQueryDetails  = mysqli_query($twitterQuery);
		return $twitterFetchDetails = mysqli_fetch_assoc($twitterQueryDetails);
	}

        public function get_newsletter_template($apiId='')
	{
		$this->db->select('*');
		$this->db->from('fc_newsletter');
		$this->db->where('id',$apiId);
		$data = $this->db->get();
		return $data->row_array();
	}

	/**
	 *
	 * Merge two arrays and sort the result array using array_multisort
	 * @param Array $ar1
	 * @param Array $ar2
	 * @param String $field	=> Field name for sort
	 * @param String $type	=> Sort type asc or desc
	 */
	public function get_sorted_array($ar1=array(),$ar2=array(),$field='id',$type='asc'){
		$products_list_arr = array();
		if (count($ar1)>0 && $ar1->num_rows()>0){
			foreach ($ar1->result() as $ar1_row){
				$products_list_arr['product'][] = $ar1_row;
				$products_list_arr[$field][] = $ar1_row->$field;
			}
		}

		if (count($ar2)>0 && $ar2->num_rows()>0){
			foreach ($ar2->result() as $ar2_row){
				$products_list_arr['product'][] = $ar2_row;
				$products_list_arr[$field][] = $ar2_row->$field;
			}
		}

		if ($type == 'asc'){
			$sort = SORT_ASC;
		}else {
			$sort = SORT_DESC;
		}

		array_multisort($products_list_arr[$field],$sort,
		$products_list_arr['product']
		);

		return $products_list_arr['product'];
	}

	/**
	 *
	 * This function save the admin details in a file
	 */
	public function saveAdminSettings($common_prefix=''){
		if ($this->input->post('https_enabled')=='yes'){
			if (substr(base_url(), 0,7)=='http://'){
				$b_url = 'https://'.substr(base_url(), strpos(base_url(), 'http://')+7);
			}else {
				$b_url = base_url();
			}
			$https_enabled = 'yes';
		}else {
			if ($this->input->post('https_enabled')=='no'){
				if (substr(base_url(), 0,8)=='https://'){
					$b_url = 'http://'.substr(base_url(), strpos(base_url(), 'https://')+8);
				}else {
					$b_url = base_url();
				}
				$https_enabled = 'no';
			}else {
				if (HTTTPS_ENABLED == 'yes'){
					if (substr(base_url(), 0,7)=='http://'){
						$b_url = 'https://'.substr(base_url(), strpos(base_url(), 'http://')+7);
					}else {
						$b_url = base_url();
					}
					$https_enabled = 'yes';
				}else {
					if (substr(base_url(), 0,8)=='https://'){
						$b_url = 'http://'.substr(base_url(), strpos(base_url(), 'https://')+8);
					}else {
						$b_url = base_url();
					}
					$https_enabled = 'no';
				}
			}
		}
		$getAdminSettingsDetails = $this->getAdminSettings();
		$config = '<?php ';
		foreach($getAdminSettingsDetails->row() as $key => $val){
			$value = addslashes($val);
			if($key != 'meta_keyword')
			{ $config .= "\n\$config['$key'] = '$value'; "; }
		}
		$config .= "\n\$config['common_prefix'] = '".$common_prefix."'; ";
		$config .= "\n\$config['https_enabled'] = '".$https_enabled."'; ";
		$config .= "\n\$config['base_url'] = '".$b_url."'; ";
		$config .= ' ?>';
		$file = 'commonsettings/fc_admin_settings.php';
		file_put_contents($file, $config);
	}
	
	public function get_home_meta()
	{
		$this->db->select('meta_keyword');
		$this->db->from('fc_admin_settings');
		$data = $this->db->get();
		return $data->row();
	}

	/*
	* Get All Cities - Manish
	*/
	public function get_all_cities($status = 0) {

		$this->db->select('id, list_value, product_count, status, list_value_seourl');
		$this->db->from('fc_list_values');
		$this->db->where('list_id', '1');
		if($status == '1') {
			$this->db->where('status', '1');
		}
		$this->db->order_by('list_value','asc');
		$query = $this->db->get();

		return $query;
	}

	public function get_current_city_details($val='') {
	    $cityId = ($val == '') ? $_SESSION['prcity'] : $val;
		$this->db->select('list_value, list_value_seourl, product_count');
		$this->db->from('fc_list_values');
		$this->db->where('list_id', '1');
		$this->db->where('id', $cityId);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_city_array()
	{
		$cities = $this->get_all_cities(1);
        $cityResult = $cities->result_array();
        $split_city = '';
        foreach($cityResult as $key => $city) {

            if(strpos($city['list_value'], '/') !== false) {
                $split_citys = explode("/", $city['list_value']);
                $city_list_array = array();
                foreach($split_citys as $key1 => $split_city) {
                    $city_list_array[$key1] = $city;
                    $city_list_array[$key1]['list_value'] = $split_city;
                }
                unset($cityResult[$key]);
            }

        }

        $city_merge = array_merge($cityResult, $city_list_array);

        usort($city_merge, function($a, $b) {
            return $a['list_value'] - $b['list_value'];
		});

		return $city_merge;
	}
	/*
	* Get All Cities - Manish - End
	*/


    public function failed_order_email_send($eamil_vaues = array())
	{
		//Prevent mail for pleasureriver
		$server_ip = $this->input->ip_address();
		$mail_id = '';
		if ($demoserverChk){
			if (isset($eamil_vaues['mail_id'])){
				$mail_id = $eamil_vaues['mail_id'];
			}
		}else {
			$mail_id = 'set';
		}

		if ($mail_id != ''){
			if (is_file('./fc_smtp_settings.php'))
			{
				include('fc_smtp_settings.php');
			}

			// Set SMTP Configuration
			if($config['smtp_user'] != '' && $config['smtp_pass'] != ''){
				$emailConfig = array(
					'protocol' => 'smtp',
					'smtp_host' => $config['smtp_host'],
					'smtp_port' => $config['smtp_port'],
					'smtp_user' => $config['smtp_user'],
					'smtp_pass' => $config['smtp_pass'],
					'auth' => true,
				);
			}

			// Set your email information
			$from = array('email' => $eamil_vaues['from_mail_id'],'name' => $eamil_vaues['mail_name']);
			$to = $eamil_vaues['to_mail_id'];
			$subject = $eamil_vaues['subject_message'];
			$message = stripslashes($eamil_vaues['body_messages']);
			// Load CodeIgniter Email library

			if( $config['smtp_user'] != '' && $config['smtp_pass'] != ''){
				$this->load->library('email', $emailConfig);
			} else {
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

				// Additional headers
				$headers .= 'To: '.$eamil_vaues['to_mail_id']. "\r\n";
				$headers .= 'From: '.$eamil_vaues['mail_name'].' <'.$eamil_vaues['from_mail_id'].'>' . "\r\n";
				if($eamil_vaues['cc_mail_id'] != '')
				{
					$headers .= 'Cc: '.$eamil_vaues['cc_mail_id']. "\r\n";
				}

				// Mail it
				$mails = mail($eamil_vaues['to_mail_id'], trim(stripslashes($eamil_vaues['subject_message'])), trim(stripslashes($eamil_vaues['body_messages'])), $headers, $eamil_vaues['attachment']);
				if(!$mails) {
				    return false;
				} else {
				    return true;
				}
			}

			$this->email->initialize($emailConfig);
			$this->email->set_newline("\r\n");
			// Set email preferences
			$this->email->set_mailtype($eamil_vaues['mail_type']);
			$this->email->from($from['email'],$from['name']);

			$this->email->to($to);
			if($eamil_vaues['cc_mail_id'] != '')
			{
				$this->email->cc($eamil_vaues['cc_mail_id']);
			}
			$this->email->subject($subject);
			if(isset($eamil_vaues['dynamicforgpasslink']) && !empty($eamil_vaues['dynamicforgpasslink'])){
				$message = str_replace('dynamicforgpasslink', $eamil_vaues['dynamicforgpasslink'], $message);
			}
			$this->email->message($message);

			if (!$this->email->send(FALSE)) {
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

				// Additional headers
				$headers .= 'To: '.$eamil_vaues['to_mail_id']. "\r\n";
				$headers .= 'From: '.$eamil_vaues['mail_name'].' <'.$eamil_vaues['from_mail_id'].'>' . "\r\n";
				if($eamil_vaues['cc_mail_id'] != '')
				{
					$headers .= 'Cc: '.$eamil_vaues['cc_mail_id']. "\r\n";
				}

				// Mail it
				$mails = mail($eamil_vaues['to_mail_id'], trim(stripslashes($eamil_vaues['subject_message'])), trim(stripslashes($eamil_vaues['body_messages'])), $headers);
				if(!$mails) {
				    return false;
				} else {
				    return true;
				}
			}
			else {
				return false;
			}
		}else {
			return false;
		}

	}
	
	public function get_id_from_slug($slug, $table_name) {
		$this->db->select('id');
		$this->db->from($table_name);
		$this->db->where($slug[0], $slug[1]);
		$data = $this->db->get();
		return $data->row();
	}
	
    public function send_app_notification($device_id,$message){
		$url = 'https://fcm.googleapis.com/fcm/send';
	    /*api_key available in:
	    Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/   
	    $api_key = SMS_API_KAY;
	    $device_id = $device_id; 
	                
	    $fields = array (
	        'registration_ids' => array (
	                $device_id
	        ),
	        'data' => array (
	                "message" => $message
	        )
	    );

	    //header includes Content type and api key
	    $headers = array(
	        'Content-Type:application/json',
	        'Authorization:key='.$api_key
	    );
	                
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	    $result = curl_exec($ch);
	}
	
}