<?php

/*
 * English language
 */

/*---------------------------------
 * Common Messages
 * -------------------------------
 */
$lang['text_server_error'] = 'Server error';
$lang['something_wrong'] = "Something went wrong! Please try again later !!!";
$lang['text_registration_success'] = 'You have successfully registered and verification link sended on given email address.';
$lang['text_invalid_params'] = 'Missing Parameters';
$lang['text_duplicate_email'] = 'The email address you have entered is already registered';
$lang['text_email_available'] = 'Email is available';
$lang['text_user_not_found'] = 'User does not exists.';
$lang['text_user_updated'] = 'User updated successfully';
$lang['text_invalid_creds'] = 'Invalid credentials';
$lang['text_unauthorized'] = 'Unauthorized access';
$lang['invalid_user'] = "Invalid user.";
$lang['address_lookup'] = 'Address with postal code';
$lang['country_list'] = 'Country listing.'; 
$lang['delete_success'] = 'Delete successfully';
$lang['success'] = 'Success';


$lang['profile_pending'] = 'Registration request submitted successfully. You will be able to login once request is approved';
$lang['login_success'] = 'Login success'; 
$lang['user_exists'] = 'User already exists';
$lang['sing_up_success'] = 'New signup success';
$lang['email_phone_exists'] = 'Email or Phone number already exists';
$lang['registration_success'] = 'Registration request submitted succesfully. You will be able to login once request is approved';
$lang['otp_success'] = 'OTP sent successfully';
$lang['Invalid_otp'] = 'Invalid OTP entered';
$lang['reset_password_link_success'] = 'Password reset link sent successfully';
$lang['email_exists'] = 'Email already exists';
$lang['mobile_exists'] = 'Mobile number already exists';
$lang['lead_submit_success'] = 'Lead Submitted Successfully';
$lang['lead_listing'] = 'Lead listing';
$lang['my_profile'] = 'Profile Details';
$lang['commissions'] = 'Commission Listing';

$lang['ranking_board'] = 'Top ranking list';
$lang['cms_detail'] = 'CMS page detail';

$lang['invalid_access'] = 'Only BD user can access his users';
$lang['under_bd_users_list'] = 'BD assign users list';

$lang['city_listing'] = 'Listing of cities';

$lang['old_password_not_match'] = 'Old password is not matching with current password';
$lang['password_change_success'] = 'Password changed successfully';

$lang['invalid_invite_code'] = 'Invalid invite code';
$lang['invalid_code_valid'] = 'Valid invite code';

$lang['profile_update_success'] = 'Profile Updated Successfully';

$lang['offer_listing'] = 'Offer listing';
$lang['no_offer_found'] = 'No Offer Found';