                <article class="content cards-page">
                    <div class="title-block">
                        <h3 class="title"> <?php echo $ticket[0]->t_title; ?> </h3>
                        <p class="title-description"> <?php echo $ticket[0]->t_desc; ?> </p>
                    </div>
                    <section class="section">
                        <div class="row sameheight-container">
                            <div class="col-xl-12">
                                <div class="card sameheight-item">
                                    <div class="card-block">
                                        <!-- Nav tabs -->
                                        <div class="card-title-block">
                                            <h3 class="title"> Ticket Information </h3>
                                        </div>
                                        <ul class="nav nav-tabs nav-tabs-bordered">
                                            <li class="nav-item"> <a href="#home" class="nav-link active" data-target="#home" data-toggle="tab" aria-controls="home" role="tab">Ticket</a> </li>
                                            <li class="nav-item"> <a href="#profile" class="nav-link" data-target="#profile" aria-controls="profile" data-toggle="tab" role="tab">Status</a> </li>
                                            <li class="nav-item"> <a href="" class="nav-link" data-target="#messages" aria-controls="messages" data-toggle="tab" role="tab">Ticket Department</a> </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content tabs-bordered">
                                            <div class="tab-pane fade in active" id="home">
                                                <p><b>Email:</b>&nbsp;&nbsp;<?php echo $ticket[0]->t_email; ?></p>
                                                <p><b>Contact:</b>&nbsp;&nbsp;<?php echo $ticket[0]->t_contact; ?></p>
                                                <p><b>Description:</b>&nbsp;&nbsp;<?php echo $ticket[0]->t_description; ?></p>
                                                <p><b>Created At:</b>&nbsp;&nbsp;<?php echo date('d F, Y',strtotime($ticket[0]->adddateTime)); ?></p>
                                            </div>
                                            <div class="tab-pane fade" id="profile">
                                                <p>											<select class="form-control" onchange="changestatus(this.value,<?php echo $ticket[0]->ID; ?>)">
												<option value="NEW" <?php if($ticket[0]->t_status=='NEW'){ echo "selected"; } ?>>NEW</option>
												<option value="In Progress" <?php if($ticket[0]->t_status=='In Progress'){ echo "selected"; } ?>>In Progress</option>
												<option value="Completed" <?php if($ticket[0]->t_status=='Completed'){ echo "selected"; } ?>>Completed</option>
											</select></p>
                                            </div>
                                            <div class="tab-pane fade" id="messages">
                                                <p><b>Email:</b>&nbsp;&nbsp;<?php echo $ticket[0]->t_emailid; ?></p>
                                                <p><b>Email Title:</b>&nbsp;&nbsp;<?php echo $ticket[0]->t_emailtitle; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-block -->
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>
                    </section>
                </article>