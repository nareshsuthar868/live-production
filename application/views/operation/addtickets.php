                <article class="content item-editor-page">
                    <div class="title-block">
                        <h3 class="title"> Add new Ticket Topic<span class="sparkline bar" data-type="bar"></span> </h3>
                    </div>
                    <form name="item" action="" method="post">
                        <div class="card card-block">
                            <div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
				Title:
			</label>
                                <div class="col-sm-10"> <input type="text" class="form-control boxed" placeholder="" name="title" required> </div>
                            </div>
                            <div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
				Description:
			</label>
                                <div class="col-sm-10">
                                    <textarea rows="3" class="form-control" name="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
				Email Title:
			</label>
                                <div class="col-sm-10"> <input type="text" class="form-control boxed" placeholder="" name="t_emailtitle" required> </div>
                            </div>
                            <div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
				Email Id:
			</label>
                                <div class="col-sm-10"> <input type="text" class="form-control boxed" placeholder="" name="t_emailid" required> </div>
                            </div>                           
							<div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
				CC Email Id:
			</label>
                                <div class="col-sm-10"> <input type="text" class="form-control boxed" placeholder="" name="t_emailidccc" > </div>
                            </div>							
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2"> <button type="submit" class="btn btn-primary">
					Submit
				</button> </div>
                            </div>
                        </div>
					<input type="hidden" name="submit" value="submit" />	
                    </form>
                </article>