<?php
// print_r($user_details->row());exit;
$this->load->view('bdadmin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>View User</h6>
						<div id="widget_tab">
			              <ul>
			                <li><a href="#tab1" class="active_tab">User Details</a></li>
			                <li><a href="#tab2">Documents</a></li>
			                <li><a href="#tab3">Bank Details</a></li>
			              </ul>
			            </div>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label');
						echo form_open('admin',$attributes) 
					?>
						<div id="tab1">
	 						<ul>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title">User Image</label>
										<div class="form_input">
											<?php if ( $user_details->row()->profile_pic == ''){?>
											<img src="<?php echo CDN_URL;?>images/users/user-thumb1.png" width="100px"/>
											<?php }else {?>
											<img onclick="window.open('<?php echo CDN_URL;?>images/partner_app/profile_pic/<?php echo $user_details->row()->profile_pic;?>', '_blank');" src="<?php echo CDN_URL;?>images/partner_app/profile_pic/<?php echo $user_details->row()->profile_pic;?>" width="100px"/>
											<?php }?>
										</div>
									</div>
								</li>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title">Full Name</label>
										<div class="form_input">
											<?php echo $user_details->row()->full_name;?>
										</div>
									</div>
								</li>
		 						<li>
									<div class="form_grid_12">
										<label class="field_title">Email Address</label>
										<div class="form_input">
											<?php echo $user_details->row()->email;?>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title">Mobile Number</label>
										<div class="form_input">
											<?php echo $user_details->row()->phone_no;?>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title">Created On</label>
										<div class="form_input">
											<?php echo $user_details->row()->created;?>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title">Modified On</label>
										<div class="form_input">
											<?php echo $user_details->row()->modified;?>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title">Verified User Details ?</label>
										<div class="form_input">
											<input  type="checkbox" name="verified_user" id="verified_user" onchange="approveDocuments(<?php echo $user_details->row()->id; ?>,this,'user_details_verified', '1');" <?php if($user_details->row()->user_details_verified || $user_details->row()->user_details_verified == '1') { echo "checked"; } ?> >
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<div class="form_input">
											<a href="bdadmin/bdusers/display_bd_users" class="tipLeft" title="Go to users list"><span class="badge_style b_done">Back</span></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
							<div id="tab2">
								<ul>
									<li>
										<div class="form_grid_12">
											<label class="field_title">POI</label>
											<div class="form_input">
												<?php 
												$poi_images = explode(',', $user_details->row()->poi);
												foreach ($poi_images as $key => $value) {
												?>
											 	<img onclick="window.open('<?php echo CDN_URL;?>images/partner_app/poi/<?php echo $value;?>', '_blank');" src="<?php echo CDN_URL;?>images/partner_app/poi/<?php echo $value;?>" width="100px"/>
												<?php } ?>
											</div>
										</div>
									</li>

								
									<li>
										<div class="form_grid_12">
											<label class="field_title">Employee ID</label>
											<div class="form_input">
												<?php echo $user_details->row()->employee_id;?>
											</div>
										</div>
									</li>
									<li>
										<div class="form_grid_12">
											<label class="field_title">Verified Documents ?</label>
											<div class="form_input">
												<input type="checkbox" name="verified_doc" id="verified_doc" onchange="approveDocuments(<?php echo $user_details->row()->id; ?>,this,'document_verified', '1');" <?php if($user_details->row()->document_verified || $user_details->row()->document_verified == '1'){ echo "checked"; } ?> >
											</div>
										</div>
									</li>
									<li>
										<div class="form_grid_12">
											<div class="form_input">
												<a href="bdadmin/bdusers/display_bd_users" class="tipLeft" title="Go to users list"><span class="badge_style b_done">Back</span></a>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div id="tab3">
								<?php 
									$bankDetails = json_decode($user_details->row()->bank_details);
								 ?>
								<ul>
									<li>
										<div class="form_grid_12">
											<label class="field_title">Bank Name</label>
											<div class="form_input">
												<?php if(isset($bankDetails->bank_name)) { echo strtoupper($bankDetails->bank_name); }else{
													 echo '--';
												} ?>
											</div>
										</div>
									</li>
									<li>
										<div class="form_grid_12">
											<label class="field_title">Account Holder Name</label>
											<div class="form_input">
												<?php if(isset($bankDetails->account_holder_name)) { echo strtoupper($bankDetails->account_holder_name); } else { echo '--';} ?>
											</div>
										</div>
									</li>
									<li>
										<div class="form_grid_12">
											<label class="field_title">Account Number</label>
											<div class="form_input">
												<?php if(isset($bankDetails->account_number)) { echo strtoupper($bankDetails->account_number); }else { echo '--';} ?>
											</div>
										</div>
									</li>	
									<li>
										<div class="form_grid_12">
											<label class="field_title">IFSC Code</label>
											<div class="form_input">
												<?php if(isset($bankDetails->ifsc_code)) { echo strtoupper($bankDetails->ifsc_code); } else{
													 echo "--";
												} ?>
											</div>
										</div>
									</li>
									<li>
										<div class="form_grid_12">
											<label class="field_title">Verified Bank Details ?</label>
											<div class="form_input">
												<input type="checkbox" name="verified_bank" id="verified_bank" onchange="approveDocuments(<?php echo $user_details->row()->id; ?>,this,'bank_verified', '1');" <?php if($user_details->row()->bank_verified || $user_details->row()->bank_verified == '1'){ echo "checked"; } ?>>
											</div>
										</div>
									</li>
									<li>
										<div class="form_grid_12">
											<div class="form_input">
												<a href="bdadmin/bdusers/display_bd_users" class="tipLeft" title="Go to users list"><span class="badge_style b_done">Back</span></a>
											</div>
										</div>
									</li>
									<li>
										<div class="form_grid_12"`	>
											<div>
												<?php if($user_details->row()->user_status  == 'Pending'){ ?>
												<a href="JavaScript:void(0);" onclick="Approvepartneruser('<?php echo $user_details->row()->id;?>', '1')" class="tipLeft" title="Approve User"><span class="badge_style b_done">Approve User</span></a>
											<?php }?>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>