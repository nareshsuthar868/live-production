<?php
$this->load->view('bdadmin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Edit User</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('bdadmin/users/insertEditUser',$attributes) 
					?>
	 						<ul>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Full Name </label>
										<div class="form_input">
											<input name="full_name" style=" width:295px" id="full_name" value="<?php echo $user_details->row()->full_name;?>" type="text" tabindex="1" class="required tipTop" title="Please enter the user fullname"/>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="email">Email Address </label>
										<div class="form_input">
											<?php echo $user_details->row()->email;?>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="thumbnail">Profile Image<span class="req">*</span></label>
										<div class="form_input">
											<!-- <input name="thumbnail" id="thumbnail" type="file" tabindex="7" class="large tipTop" title="Please select user image"/> -->
										</div>
										<div class="form_input"><img src="<?php echo CDN_URL;?>images/partner_app/profile_pic/<?php echo $user_details->row()->profile_pic;?>" width="100px"/>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="group">Mobile Number <span class="req">*</span></label>
										<div class="form_input">
											<div class="user_seller">
													<input name="phone_no" style=" width:295px" id="phone_no" value="<?php echo $user_details->row()->phone_no;?>" type="text" tabindex="1" class="required tipTop" title="Please enter the user phone number"/>
											</div>
										</div>
									</div>
								</li>
								<li>
									 <!-- <div class="form_grid_12">
										<label class="field_title" for="admin_name">User Type<span class="req">*</span></label>
										<div class="form_input">
											<div class="active_inactive">
												<?php echo $user_details->row()->user_type; ?>
												<input type="hidden" name="user_id" value="<?php echo $user_details->row()->user_id;?>" />
											</div>
										</div>
									</div>									 -->
									<div class="form_grid_12">
									<label class="field_title" for="admin_name">User Type<span class="req">*</span></label>
										<div class="form_input">
											<div class="active_inactive">
												<select name="user_type" id="user_type" class="required tipTop">
													<option value="Channel Partner"> Channel Partner</option>
													<option value="BD Executive">BD Executive</option>
													<option value="Campus Ambassador">Campus Ambassador</option>
												</select>
												<input type="hidden" name="user_id" value="<?php echo $user_details->row()->user_id;?>" />
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="bd_user">User BD Executive<span class="req">*</span></label>
										<div class="form_input">
											<div class="active_inactive">
												<select name="bd_user" id="bd_user" class="required tipTop">
													<option value="0" disabled selected > Please Select User</option>
													<?php
													if($user_bd_executive->num_rows() > 0) {
														foreach($user_bd_executive->result_array() as $val) { ?>
															<option value="<?php echo $val['id']; ?>" <?php if($user_details->row()->bdexecuter_id == $val['id']) { echo 'selected'; } ?> ><?php echo $val['user_name'].' ('.$val['email'].')'; ?></option>
														<?php }
													}
													?>
												</select>
											</div>
										</div>
									</div>									
								</li>
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Update</span></button>
									</div>
								</div>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>