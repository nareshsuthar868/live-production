<?php
$this->load->view('bdadmin/templates/header.php');
extract($bdprivileges);
?>

<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('bdadmin/users/change_user_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>

					</div>
					<div class="widget_content">
						<table class="display sort-table" id="userListing">
                            <thead>
                                <tr>
	                            	<!-- <th class="center">
										<input name="checkbox_id[]" type="checkbox" value="on" class="selectall">
									</th> -->
	                                <th>Full Name</th>
	                                <th>Email</th>
									<th>RM Name</th>
									<th>RM Email</th>
	                                <th>Thumbnail</th>
	                                <th>User Type</th>
									<th>Submission date</th>
									<th>Approval date</th>
	                                <th>Last Login Date</th>
									<th>Status</th>
	                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	<!-- <th class="center">
										<input name="checkbox_id[]"  type="checkbox" value="on" class="selectall12">
									</th> -->
	                                <th>Full Name</th>
	                                <th>Email</th>
									<th>RM Name</th>
									<th>RM Email</th>
	                                <th>Thumbnail</th>
	                                <th>User Type</th>
									<th>Submission date</th>
									<th>Approval date</th>
	                                <th>Last Login Date</th>
									<th>Status</th>
	                                <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<script type="text/javascript">
$(function(){
	// add multiple select / deselect functionality
	$(".selectall").click(function () {
		  $('.case').attr('checked', this.checked);
		  $('.selectall12').attr('checked', this.checked);
	});
	$(".selectall12").click(function () {
		  $('.case').attr('checked', this.checked);
		   $('.selectall').attr('checked', this.checked);
	});
});

function change_all(){
	if($(".case").length == $(".case:checked").length) {
		$(".selectall").attr("checked", "checked");
		$(".selectall12").attr("checked", "checked");
	} else {
		$(".selectall").removeAttr("checked");
		$(".selectall12").removeAttr("checked", "checked");
	}
}
</script>
<script type="text/javascript">
$(function (){

    $('#userListing').DataTable({
        processing:  true,
        serverSide: true,
        ajax: '<?php echo base_url(); ?>bdadmin/users/Get_user_data',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        order: [5,'desc'],
        columns: [
            { "data": "full_name" },
			{ "data": "email" },
			{ "data": "executer_name" },
			{ "data": "executer_email" },
            { "data": "profile_pic" },
            { "data": "user_type" },
			// { "data": "created"},
			{ "data": "modified"},
			null,
			{ "data": "last_login_date"},
			{ "data": "user_status"},
			{ "data": "Action"},
  ],
        columnDefs: [
        	// {
			// 	orderable: false, targets: [0],
            //     "render": function ( data, type, full, meta ) {
			// 		console.log(full);
			// 		var link  = '<input name="verified_bank" id="verified_bank" type="hidden" value="'+ full.bank_verified +'"><input name="verified_doc" id="verified_doc" type="hidden" value="'+ full.document_verified +'"><input name="verified_user" id="verified_user" type="hidden" value="'+ full.user_details_verified +'">';
			// 		link  = link + '<input name="checkbox_id[]" type="checkbox" value="'+ full.id +'" class="case" onclick="change_all()">';
            //         return link;
            //     }
            // },
            // {"targets": [0], "className": "center tr_select"},
          	{
				orderable: false, targets: [4],
                "render": function ( data, type, full, meta ) {
                    var base_url =  '<?= BASE_URL?>';
                    var image_url = base_url + "images/partner_app/profile_pic/";
                    var alt_image = image_url + "default-user.png";
     				if(data == '' || data == null){
                        var url = alt_image;
                    }
                    else{
                        var url = image_url + data;
                    }
                    return '<div class="widget_thumb" style="margin-left: 15px;"><img width="40px" height="40px" src=\'' + url + '\'></div>'
                }
			},
			{
				orderable: true, targets: [7],
                "render": function ( data, type, full, meta ) {
					if(full.user_status == "Pending")
					{return 0;}
					else
					{return full.created_date;}
                }
            },
            {
				orderable: true, targets: [9],
                "render": function ( data, type, full, meta ) {
                	var mode = (data == 'Pending')?'0':'1';
                    if(data == 'Pending'){
                        var button = '<a title="Click to approve" class="tip_top" href="javascript:void(0);" onclick="Approvepartneruser('+  full.id +',1)"><span class="badge_style b_done">'+ data +'</span></a>';
                    }
                    else{
                        var button = '<lable style="color:green;"><b>Approved</b></label>'
                    }
                    return button;
                }
            },
            {
				orderable: false, targets: [10],
                "render": function ( data, type, full, meta ) {
                   var button = '<span><a class="action-icons c-suspend" href="bdadmin/users/view_user/'+ full.id +'" title="View">View</a></span>';
					   button += '<span><a class="action-icons c-edit" href="bdadmin/users/edit_user_form/'+ full.id + '" title="Edit">Edit</a></span>';
					   if(full.user_status == "Approved")
					   {
					   button += '<span><a class="action-icons c-approve" href="javascript:void(0);" title="pending"  onclick="Pendingpartneruser('+  full.id +',0)">Pending</a></span>';
					   }
                    return button;
                }
            },
   
        {
            orderable: false, targets: [0,3,4,5],
        }],
        language: {
            searchPlaceholder: "Search By Email"
        },
        fnDrawCallback: function (oSettings) {
        }
    });
});
</script>

<style type="text/css">
.dataTables_wrapper .dataTables_paginate .paginate_button {
	margin: 0px 0px 0px -2px !important;
	padding: 0px !important;
}
</style>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>