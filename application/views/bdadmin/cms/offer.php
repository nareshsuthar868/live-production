<?php
$this->load->view('bdadmin/templates/header.php');
extract($bdprivileges);
?>

<div id="content">
		<div class="grid_container">
            <?php 
                $attributes = array('id' => 'cms_lisitingID');
            ?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>

					</div>
					<div class="widget_content">
						<table class="display sort-table" id="offer_listing">
                            <thead>
                                <tr>
	                                <th>Offer Name</th>
	                                <th>Coupon Code</th>
                                    <th>Price</th>
                                    <th>Created On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                                <th>Offer Name</th>
	                                <th>Coupon Code</th>
                                    <th>Price</th>
                                    <th>Created On</th>
                                    <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>

<?php 
$this->load->view('bdadmin/templates/footer.php');
?>