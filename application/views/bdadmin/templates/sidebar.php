<?php 
$currentUrl = $this->uri->segment(2,0); $currentPage = $this->uri->segment(3,0);
if($currentUrl==''){$currentUrl = 'dashboard';} if($currentPage=='') { $currentPage = 'dashboard'; }
?>

<div id="left_bar">

	<div id="sidebar">
		<div id="secondary_nav">
			<ul id="sidenav" class="accordion_mnu collapsible">

				<li><a href="<?php echo base_url();?>bdadmin/dashboard/admin_dashboard" <?php if($currentUrl=='dashboard'){ echo 'class="active"';} ?>><span class="nav_icon computer_imac"></span> Dashboard</a></li>	




				<?php  extract($bdprivileges); if ($allPrev == '1'){ 	?>

				<li><a href="#" <?php if($currentUrl=='subadmin'){ echo 'class="active"';} ?>><span class="nav_icon user"></span> Subadmin<span class="up_down_arrow">&nbsp;</span></a>
					<ul class="acitem" <?php if($currentUrl=='subadmin'){ echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
						<li><a href="bdadmin/subadmin/display_sub_admin" <?php if($currentPage=='display_sub_admin'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>Subadmin List</a></li>
						<li><a href="bdadmin/subadmin/add_sub_admin_form" <?php if($currentPage=='add_sub_admin_form'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>Add New Subadmin</a></li>
					</ul>
				</li>	
				<?php }  if ((isset($user) && is_array($user)) && in_array('0', $user) || $allPrev == '1'){  ?>
				<li><a href="#" <?php if($currentUrl=='users'){ echo 'class="active"';} ?>><span class="nav_icon users"></span> Users<span class="up_down_arrow">&nbsp;</span></a>
				<ul class="acitem" <?php if($currentUrl=='users'){ echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
					<li><a href="bdadmin/users/display_user_list" <?php if($currentPage=='display_user_list'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>Users List</a></li>
				</ul>
				</li>


				<?php }  if ((isset($bdexecuters) && is_array($bdexecuters)) && in_array('0', $bdexecuters) || $allPrev == '1'){  ?>
				<li>
					<a href="#" <?php if($currentUrl=='bdusers'){ echo 'class="active"';} ?>><span class="nav_icon users"></span> RM executers<span class="up_down_arrow">&nbsp;</span></a>
					<ul class="acitem" <?php if($currentUrl=='bdusers'){ echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
						<li><a href="bdadmin/bdusers/display_bd_users" <?php if($currentPage=='display_bd_users'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>RM List</a></li>
						<li><a href="bdadmin/bdusers/add_bd_users" <?php if($currentPage=='add_bd_users'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>Add BD Executer</a></li>
					</ul>
				</li>

				<?php } if ((isset($settings) && is_array($settings)) && in_array('0', $settings) || $allPrev == '1'){  ?>

				<li><a href="#" <?php if($currentUrl=='setting'){ echo 'class="active"';} ?>><span class="nav_icon users"></span>Settings<span class="up_down_arrow">&nbsp;</span></a>
				<ul class="acitem" <?php if($currentUrl=='setting'){ echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
					<li><a href="bdadmin/setting/show_comission_rates" <?php if($currentPage=='show_comission_rates'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>Comissions Rates</a></li>
					<li><a href="bdadmin/setting/cms_pages" <?php if($currentPage=='cms_pages'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>CMS Pages</a></li>

					<li><a href="bdadmin/setting/offers_page" <?php if($currentPage=='offers_page'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>Offers</a></li>

					<li><a href="bdadmin/setting/add_new_offers" <?php if($currentPage=='add_new_offers'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>Add Offer</a></li>
				</ul>
				</li>
				<?php } if ((isset($leads) && is_array($leads)) && in_array('0', $leads) || $allPrev == '1'){  ?>
				<li><a href="bdadmin/lead_request/display_lead_request_orders"  <?php if($currentPage=='display_lead_request_orders'){ echo 'class="active"';} ?>><span class="nav_icon mail">&nbsp;</span>Lead Request</a>            
				</li>

				<?php }  if ((isset($bdexecuters) && is_array($bdexecuters)) && in_array('0', $bdexecuters) || $allPrev == '1'){  ?>
				<li>
					<a href="#" <?php if($currentUrl=='commissions'){ echo 'class="active"';} ?>><span class="nav_icon users"></span>
						Commission<span class="up_down_arrow">&nbsp;</span></a>
					<ul class="acitem" <?php if($currentUrl=='commissions'){ echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
						<li><a href="bdadmin/commission/show_commissions" <?php if($currentPage=='show_commissions'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>Commission listing</a></li>
					</ul>
				</li>

				<?php } 
			if ((isset($bdexecuters) && is_array($bdexecuters)) && in_array('0', $bdexecuters) || $allPrev == '1'){  ?>
				<li>
					<a href="#" <?php if($currentUrl=='bd_insentive'){ echo 'class="active"';} ?>><span class="nav_icon image_1"></span>
						BD Revenue<span class="up_down_arrow">&nbsp;</span></a>
					<ul class="acitem" <?php if($currentUrl=='bd_insentive'){ echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
						<li><a href="bdadmin/commission/bd_insentive" <?php if($currentPage=='bd_insentive'){ echo 'class="active"';} ?>><span class="list-icon">&nbsp;</span>BD Insentive</a></li>
					</ul>
				</li>

				<?php } ?>

			</ul>
		</div>
	</div>
</div>
<div class="new_scroll">
    <ul id="results" /> 
</div>
            
