<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width"/>
<base href="<?php echo base_url(); ?>">
<link href="css/reset.css" rel="stylesheet" type="text/css">
<link href="css/typography.css" rel="stylesheet" type="text/css">
<link href="css/styles.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css">
<link href="css/gradient.css" rel="stylesheet" type="text/css">
<link href="css/developer.css" rel="stylesheet" type="text/css">




<script src="<?php echo base_url(); ?>js/jquery-2.1.1.min.js" ></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>js/modernizr.min.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.js"></script>

<script src="assets/datatables/media/js/jquery.dataTables.min.js"></script> 
<script src="assets/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/datatables/jquery.dataTables.min.css">



<script src="js/bdadmin.js"></script>
<title>Create Password</title> 
<script type="text/javascript">

function hideErrDiv(arg) {
    document.getElementById(arg).style.display = 'none';
}
</script>

</head>
<body id="theme-default" class="full_block">
<div id="login_page">
	<div class="login_container">
		<div class="login_header blue_lgel">
			<ul class="login_branding">
				<li>
				<div class="logo_small">
					<img src="images/logo/<?php echo $logo;?>" width="100px" alt="<?php echo $siteTitle;?>" title="<?php echo $siteTitle;?>">
				</div>
				<span></span>
				</li>
				<li class="right go_to"><a href="<?php echo base_url();?>" title="Go to Main Site" class="home">Go To Main Site</a></li>
			</ul>
		</div>
		<?php if (validation_errors() != ''){?>
		<div id="validationErr">
			<script>setTimeout("hideErrDiv('validationErr')", 3000);</script>
			<p><?php echo validation_errors();?></p>
		</div>
		<?php }?>
		<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
		<?php 
		$attributes = array('id' => 'create_password_form');
		echo form_open('bdadmin/adminlogin/change_password_form',$attributes); ?>
			<div class="login_form">
				<h3 class="blue_d">Create Your Password</h3>
				<ul>
					<li class="login_pass tipBot" title="Please enter password">
					<input name="admin_password" id="admin_password" value="" type="password" placeholder="Enter Password" >
					</li>
					<li class="login_pass tipTop" title="Please enter confirm password">
					<input name="admin_confirm_password" id="admin_confirm_password"  type="password" value="" placeholder="Enter Confirm Password" >
					</li>
				</ul>
			</div>
			<input type="hidden" name="verification_token" value="<?php echo $verification_token; ?>">
			<input type="submit" class="login_btn blue_lgel"  id="button-submit-merchant" value=" Create Password">

		</form>
        
	</div>
</div>
</body>
</html>