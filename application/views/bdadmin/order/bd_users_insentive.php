<?php
$this->load->view('bdadmin/templates/header.php');
extract($bdprivileges);
?>
 <style type="text/css">

.modal {
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    opacity: 0;
    visibility: hidden;
    transform: scale(1.1);
    transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;
}
.modal-content {
    position: absolute;
    top: 30%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: white;
    padding: 1rem 1.5rem;
    /*width: rem;*/
    border-radius: 0.5rem;
}
.close-button {
    float: right;
    width: 1.5rem;
    line-height: 1.5rem;
    text-align: center;
    cursor: pointer;
    border-radius: 0.25rem;
    background-color: lightgray;
}
.close-button:hover {
    background-color: darkgray;
}
.show-modal {
    opacity: 1;
    visibility: visible;
    transform: scale(1.0);
    transition: visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s;
}
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    /*width: 100%;*/
}

td, th {
    border: 1px solid #dddddd;
    text-align: center;
    padding: 8px;
}
tr:nth-child(even) {
    background-color: #dddddd;
}
@media only screen and (max-width: 1366px) {
    .refralmodal{
        overflow: auto;
    }
    .refralmodal .modal-content{
        transform: translate(-50%, 0);
        top: 20px;
    } 
    .modal-open{
        overflow: hidden;
    }
}

.dataTables_wrapper .dataTables_paginate .paginate_button {
    margin: 0px 0px 0px -2px !important;
    padding: 0px !important;
}
</style>

<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('bdadmin/users/change_user_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>

					</div>
					<div class="widget_content">
						<table class="display sort-table" id="BduserListing">
                            <thead>
                                <tr>
	                            	<!-- <th class="center">
										<input name="checkbox_id[]" type="checkbox" value="on" class="selectall">
									</th> -->
	                                <th>Full Name</th>
	                                <th>Email</th>
	                                <th>Thumbnail</th>
	                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	<!-- <th class="center">
										<input name="checkbox_id[]"  type="checkbox" value="on" class="selectall12">
									</th> -->
	                                <th>Full Name</th>
	                                <th>Email</th>
	                                <th>Thumbnail</th>
	                                <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>

<button type="button" class="trigger" style="display: none;">View</button>
    <div class="modal refralmodal" id="model_css123">
        <div class="modal-content">
            <span class="close-button">&times;</span>  
                <div class="show_all_payouts col-md-12">
                    <!-- Total Leads: <label id="total_leads">0</label> -->
                   <table class="col-md-4" >
                       <thead> 
                           <tr>
                               <th>MY Revene</th>
                               <th>Broker Revenue</th>
                               <th>Total Revenue</th>
                            
                           </tr>
                       </thead>
                       <tbody>
                           <tr id="revenue">
                           </tr>
                       </tbody>
                   	</table>
                   	<br><br>
                    <table>
                       <thead>
                           <tr>
                               <th>MY Orders</th>
                               <th>Broker Orders</th>
                               <th>Total Orders</th>
                            
                           </tr>
                       </thead>
                       <tbody>
                           <tr id="orders">
                       
                           </tr>
                       </tbody>
                   	</table>
                </div> 
        </div>
    </div>

<script type="text/javascript">
$(function(){
	// add multiple select / deselect functionality
	$(".selectall").click(function () {
		  $('.case').attr('checked', this.checked);
		  $('.selectall12').attr('checked', this.checked);
	});
	$(".selectall12").click(function () {
		  $('.case').attr('checked', this.checked);
		   $('.selectall').attr('checked', this.checked);
	});
});

function change_all(){
	if($(".case").length == $(".case:checked").length) {
		$(".selectall").attr("checked", "checked");
		$(".selectall12").attr("checked", "checked");
	} else {
		$(".selectall").removeAttr("checked");
		$(".selectall12").removeAttr("checked", "checked");
	}
}
</script>
<script type="text/javascript">
$(function (){

    $('#BduserListing').DataTable({
        processing:  true,
        serverSide: true,
        ajax: '<?php echo base_url(); ?>bdadmin/commission/get_bd_users',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        // order: [,'desc'],
        columns: [
            { "data": "full_name" },
            { "data": "email" },
            { "data": "profile_pic" },
			{ "data": "Action"},
        ],
        columnDefs: [
          	{
				orderable: false, targets: [2],
                "render": function ( data, type, full, meta ) {
                    var base_url =  '<?= BASE_URL?>';
                    var image_url = base_url + "images/partner_app/profile_pic/";
                    var alt_image = image_url + "user-thumb1.png";
                    if(data == ''){
                        var url = alt_image;
                    }
                    else{
                        var url = image_url + data;
                    }
                    return '<div class="widget_thumb" style="margin-left: 65px;"><img width="40px" height="40px" src=\'' + url + '\'></div>'
                }
			},   
            {
				orderable: false, targets: [3],
                "render": function ( data, type, full, meta ) {
                   var button = '<span><a class="action-icons c-suspend" onclick="ShowbdRevenue('+ full.user_id +')"  title="View Revenue">View Revenue</a></span>';			   
                    return button;
                }
            },
   
        {
            orderable: false, targets: [0,3],
        }],

        language: {
            searchPlaceholder: "Search By Email"
        },
        fnDrawCallback: function (oSettings) {
        }
    });
});
</script>

<style type="text/css">
.dataTables_wrapper .dataTables_paginate .paginate_button {
	margin: 0px 0px 0px -2px !important;
	padding: 0px !important;
}
</style>

<script type="text/javascript">
  
    var modal = document.querySelector(".modal");
    var trigger = document.querySelector(".trigger");
    var closeButton = document.querySelector(".close-button");

    function toggleModal() {
        modal.classList.toggle("show-modal"); 
        document.getElementsByTagName('body')[0].style = 'overflow: auto';
    }

    function windowOnClick(event) {
        if (event.target === modal) {
            toggleModal();
        }
    }
    trigger.addEventListener("click", toggleModal);
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);
    
</script>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>