<?php
$this->load->view('site/templates/header_inner');
?>

			<!--main content-->
			<div class="page_section_offset">
				<div class="container">
					<div class="row m_bottom_50">
							<h1 class="color_dark second_font fw_light m_bottom_20 tt_uppercase">Why Rent?</h1>
							<hr class="divider_bg m_bottom_25">
							<p class="fw_light m_bottom_14 p_top_4">The notion of renting furniture and furnishing has gained warm welcome from people and its popularity is spreading like a wild fire. Renting furniture is gaining tremendous popularity as it is the most economical way to possess furniture and furnishings without causing any strain on your budget. It does not engender any of the inconvenience posed by traditional furniture buying mechanism.</p>
							<h4 class="color_dark second_font fw_light m_bottom_20 tt_uppercase">Who should seek for furniture rental?</h4>
								<ul class="vr_list_type_2 m_bottom_15">
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Are you a landlord and want to furnish your apartment without spending large capital?</li>
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Are you a tenant and dont want to spend capital in furnishing rented apartment?</li>
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Are you a bachelor staying with flatmates and want to furnish apartments without having long term commitment?</li>									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Are you relocating to new city and looking for new furnishing?</li>
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Are you tired of carrying bulky furniture to a new city every time you are travelling?</li>
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Are you short of time and juggling between office and setting up your new place?</li>
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Are you upset with the vendor who sold you furniture items with poor quality wood?</li>
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Did your old wooden furniture succumb to termites?</li>
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Are you tired of your old furniture and plan to revamp your surroundings?</li>
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Do you think that your neighbor has an up to date and more stylish furniture than yours?</li>								</ul>							
							<p class="fw_light m_bottom_14 p_top_4">Buyers predominantly suffer from one these problems.  If the answer to any of these questions is a YES for you, then Cityfurnish.com is the answer to your problems. We ensure that our clients do not undergo any of these hardships. We desire to make their renting experience pleasurable. </p>
					</div>
					<div class="row">
						<h1 class="color_dark second_font fw_light m_bottom_20 tt_uppercase">Why Cityfurnish?</h1>
						<hr class="divider_bg m_bottom_25">
						<section class="col-lg-12 col-md-12 col-sm-12 m_bottom_50 m_xs_bottom_30">
							<div class="clearfix m_bottom_15 m_bottom_25">
								<div class="t_xs_align_c f_left m_right_20 m_xs_bottom_15 f_xs_none"><img src="images/furniture-Range.jpg" alt=""></div>
								<h4 class="fw_light second_font color_dark m_bottom_27 tt_uppercase">An array of products and services</h4>
								<p class="fw_light m_bottom_14 p_top_4">Cityfurnish provides an assemblage of premium lifestyle products for our clients to choose from. Cityfurnish provides end to end solutions by covering delivery; installation, pick up and dropof the products.</p>
							</div>
							<div class="clearfix m_bottom_15 m_bottom_25">
								<div class="t_xs_align_c f_right m_left_20 m_xs_bottom_15 f_xs_none"><img src="images/Best_Price_Guarante.jpg" alt=""></div>
								<h4 class="fw_light second_font color_dark m_bottom_27 tt_uppercase">Competitive Rates</h4>
								<p class="fw_light m_bottom_14 p_top_4">Cityfurnish offers premium lifestyle products at very affordable rates. It brings to your platter plethora options that an individual can avail.</p>
							</div>
							<div class="clearfix m_bottom_15 m_bottom_25">
								<div class="t_xs_align_c f_left m_right_20 m_xs_bottom_15 f_xs_none"><img src="images/Customer_Delight.jpg" alt=""></div>
								<h4 class="fw_light second_font color_dark m_bottom_27 tt_uppercase">Adjunct Services</h4>
								<p class="fw_light m_bottom_14 p_top_4">Cityfurnish is one of its kind rental services available in the market. We differ from our competitors on various parameters and aspects. Renting products from Cityfurnish allows you to own premium lifestyle products for a predetermined amount of period and saves you from the headache of disposing it off later. We give high priority to customer delight and have designed user friendly interface to aid ease of ordering.</p>
							</div>
							<div class="clearfix m_bottom_15 m_bottom_25">
								<div class="t_xs_align_c f_right m_left_20 m_xs_bottom_15 f_xs_none"><img src="images/Supreme Quality Furniture.jpg" alt=""></div>
								<h4 class="fw_light second_font color_dark m_bottom_27 tt_uppercase">Supreme Quality & Safety</h4>
								<p class="fw_light m_bottom_14 p_top_4">All our furniture items are carved out of high quality material, chemically treated to shield furniture from termite. The edges of the furniture are such that they would not hurt if the kids or elders accidentally bang into the furniture. The electronic appliances displayed on our website are tested, certified and inspected for their safety standards.<p>			
							</div>
							<div class="clearfix m_bottom_15 m_bottom_25">
							<div class="row">
							<section class="col-lg-4 col-md-4 col-sm-4">
							<div class="t_xs_align_c f_left m_right_20 m_xs_bottom_15 f_xs_none"><img src="images/flexible-payment-options.jpg" alt=""></div>
							</section>
							<section class="col-lg-8 col-md-8 col-sm-8">
								<h4 class="fw_light second_font color_dark m_bottom_27 tt_uppercase">Flexible Payment Options</h4>
								<p class="fw_light m_bottom_14 p_top_4">Cityfurnish has transparent payment alternative. Our clients can reap benefits of:</p>
								<ul class="vr_list_type_2">
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Payment using bank transfer, debit/credit cards and checks</li>
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Monthly installment plans with flexible rental periods</li>
									<li class="fw_light m_bottom_14"><i class="fa fa-star fs_ex_small"></i>Lump sum payment and avail cash back and discounts</li>
								</ul>
							</section>
							</div>
							</div>
						</section>
					</div>
				</div>
			</div>
			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>

		<!--back to top-->
		<!-- <button class="back_to_top animated button_type_6 grey state_2 d_block black_hover f_left vc_child tr_all"><i class="fa fa-angle-up d_inline_m"></i></button> -->
			<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>	
<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>