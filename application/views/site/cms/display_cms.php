<?php
$this->load->view('site/templates/header_inner');
?>
<!--main content-->
			<div class="page_section_offset">
				<section class="innerbanner">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12"> 
                            <h1><?php echo $pageDetails->row()->page_name;?></h1>
                            <ul class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li class="active"><?php echo $pageDetails->row()->page_name;?></li>
                            </ul>
                          </div>	
                        </div>
                    </div>     	
                </section>
			 <div class="cmsdivrow">
                	<div class="container">
            	<?php 
            	if ($pageDetails->num_rows()>0){
            		echo $pageDetails->row()->description;
            	}
            	?>
            </div>
        </div>
    </div>


			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>

		<!--back to top-->
		<!-- <button class="back_to_top animated button_type_6 grey state_2 d_block black_hover f_left vc_child tr_all"><i class="fa fa-angle-up d_inline_m"></i></button> -->
		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>
		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>