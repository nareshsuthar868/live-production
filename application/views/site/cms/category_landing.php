<?php
 $this->load->view('site/templates/header_inner');
?>
<div class="page_section_offset"> 
  <section class="browsecatagoryrow">
    <div class="container">
      <h2>Complete Furnishing Solution</h2>
      <p>Furnish your home and office with us.</p>
      <div class="row">
      <div class="cat_col">
        <?php for ($i=0; $i < count($ListCateogry['cat']) ; $i++) { 
          $cat_name = explode(",", $ListCateogry['cat'][$i]->cat_name); 
          $cat_class = $ListCateogry['cat'][$i]->seourl;
          if($i < 3){ ?>
       	<div class="cat_white">
          <a href="<?php echo base_url().$cat_slug.'/'.$ListCateogry['cat'][$i]->seourl; ?>" class="rental-packages"
            style="background-image:url(<?php echo base_url() ?>images/category/cat_landing/<?php echo $ListCateogry['cat'][$i]->seourl.".jpg"; ?>);">
        	<div class="catagoryinner">
            	<div class="graybottom"><strong><?php  echo $ListCateogry['cat'][$i]->cat_name; ?></strong></div>
            </div>
            <span class="<?php echo $cat_name[0]; ?>"><?php echo $ListCateogry['cat'][$i]->product_count; ?></span>
          </a>
        </div>
        <?php } else { ?>
          
            <div class="cat_half">
           <a href="<?php echo base_url().$cat_slug.'/'.$ListCateogry['cat'][$i]->seourl; ?>">
              <div class="catagoryinner">
                     <img src="<?php echo base_url(); ?>images/category/cat_landing/<?php echo $ListCateogry['cat'][$i]->category_image;  ?>" alt="catagory_fitness"/>
                    <div class="graybottom"> <strong><?php  echo $ListCateogry['cat'][$i]->cat_name; ?></strong></div>
                </div>
                <span class="<?php echo $cat_name[0]; ?>"><?php echo $ListCateogry['cat'][$i]->product_count; ?></span>
            </a>
        </div>

         <?php  } } ?>
       </div>
      </div>
    </div>
  </section>
</div>
<?php
$this->load->view('site/templates/footer');
?>
</div>
</body>
</html>