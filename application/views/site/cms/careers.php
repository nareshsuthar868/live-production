<?php
$this->load->view('site/templates/header_inner');
?>

	<!--main content-->
			<!-- New style section -->
<link rel="stylesheet" type="text/css" media="all" href="plugins/royalslider/royalslider.min.css">
<link rel="stylesheet" type="text/css" media="all" href="plugins/royalslider/skins/default/rs-default.min.css">

 <div class="page_section_offset graylightbg">
	<section class="innerbanner careerstop">
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-12">
                          <h1>We are Hiring</h1>
                          <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">We are Hiring</li>
                          </ul>
                        </div>
                        <div class="col-lg-12">
                            <article class="careertopwhite">
                                <h6>We are looking for talented individuals to join our growing team.</h6>
                                <p>At Cityfurnish, we're building an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. We offer you an exciting and challenging environment in which you can grow and thrive. We are offering you roles in which you will be fundamentally impacting the success of the company. At Cityfurnish, we are looking for people who are concerned about the customer at all times. We value creativity, hard work, initiative and radical ideas. If this sounds exciting to you, then come have a chat with us.</p>
                            </article>
                        </div>
                      </div>
                    </div>
          </section>
	<!--main content-->
	
		
            <section class="wearehiring">
               	<div class="container">
                	<div class="row">
					 	<div class="col-sm-12 col-md-12 col-lg-12">
                         	<!--<a href="javascript:void(0)" class="tabmenu">We are Hiring <i class="material-icons">keyboard_arrow_down</i> </a>-->
                            <ul class="nav nav-tabs col-md-offset-1">
                              	<li class="active">
                               		<a href="#Finance" data-toggle="tab" aria-expanded="true">
                                    	<strong>Accounts Executive <small>(Gurgaon)</small></strong>
                                	</a>
                               	</li>
                               	<li>
                                  	<a href="#Customer" data-toggle="tab" aria-expanded="false">
                                     	<strong>Customer Care Executive <small>(Gurgaon)</small></strong>
                                  	</a>
                              	</li>
                            	<li>
                                	<a href="#Business" data-toggle="tab" aria-expanded="false">
                                    	<strong>Business Development Executive <small>(Bangalore)</small></strong>
                                  	</a>
                             	</li>
                              	<!--<li>
                                  	<a href="#Operations" data-toggle="tab" aria-expanded="false">
                                     	<strong>DGM - Operations <small>(Gurgaon)</small></strong>
                                  	</a>
                              	</li>
                               	<li>
                                  	<a href="#BusinessG" data-toggle="tab" aria-expanded="false">
                                    	<strong>Intern - Operations <small>(Gurgaon)</small></strong>
                                  	</a>
                              	</li>-->
                               	
                            </ul>
          					<div class="tab-content">
             				  <!-- tab content Finance -->
                              	<div class="tab-pane fade active in" id="Finance">
                                	<p><strong>Accounts Executive - <span class="fw_light">Gurgaon</span> </strong></p>
                                	<p><strong>Number Of Openings</strong><br /> 01</p>
                                	<hr class="divider">
                                	<p><strong>Job Overview</strong></p>
                                	<p>We are looking for a dynamic, high-performing Accounts Executive with proven experience in fields of accounting and finance. The accounts executive will be primarily responsible for preparing invoices and maintaining the accounting database. Ideal candidate should have excellent analytical skills and strong attention to detail.</p>
                                		<hr class="divider">
                                			<p><strong>Responsibilities </strong></p>
                                 		<ul>
                                    		<li>Managing the status of accounts and balances and identifying inconsistencies<li>
                                    		<li>Generating Invoices for orders through accounting software</li>
                                    		<li>Preparing monthly revenue reports and identifying any leakage in revenue generation</li>
	                                    	<li>Coordinating with other departments in order to resolve any discrepancies in relation to invoicing</li>
                                    		<li>Generating monthly payment receivable reports</li>
		                                    <li>Handling day to day accounting operations and maintaining accounting database</li>
		                                    <li>Conducting research on invoicing related issues in order to ensure continuous improvement in process</li>
		                                    <li>Handling customer queries and complaints in relation to payments and invoicing</li>
                                		</ul>
                                		<hr class="divider">
                                			<p><strong>Work Experience </strong> 0-2 Years</p>
                                			<p><strong>Educational Qualification </strong> Graduate</p>
                              	</div>
                              	<!-- tab content Customer Care -->
                              	<div class="tab-pane fade" id="Customer">
                                	<p><strong>Customer Care Executive - <span class="fw_light">Gurgaon</span> </strong></p>
                                	<p><strong>Number Of Openings</strong><br /> 01</p>
                                	<hr class="divider">
                               		<p><strong>Jobs Description</strong></p>
                                	<p>We are looking for enthusiastic people who can join our fast growing team. The primary responsibilities of a Customer Service Executive would be,</p>
                                	<ul>
	                                	<li>Answer calls professionally to provide information about products and services, take/ cancel orders, or obtain details of complaints.<li>
	                                    <li>Keep records of customer interactions and transactions, recording details of inquiries, complaints, and comments, as well as actions taken</li>
	                                    <li>Follow up to ensure that appropriate actions were taken on customers' requests</li>
	                                    <li>Refer unresolved customer grievances or special requests to designated departments for further investigation</li>
                               		</ul>
                                	<hr class="divider">
                                	<p><strong>Desired Candidate </strong></p>
                                 	<ul>
	                                    <li>Graduate in any stream <li>
	                                    <li>Strong written and oral communication skills - Hindi nd English</li>
	                                    <li>Ability to listen and active problem solving skills</li>
	                                    <li>Ability to multi-task</li>
	                                    <li>Professional, responsible, and dependable</li>
	                                </ul>
                              	</div>
                              	<!-- tab content Business -->
                              	<div class="tab-pane fade" id="Business">
                                	<p><strong>Business Development Executive - <span class="fw_light">Bangalore</span> </strong></p>
                                	<p><strong>Number Of Openings</strong><br /> 03</p>
                                	<hr class="divider">
                                	<p><strong>Job Overview</strong></p>
                                	<p>We are looking for a business development executive with strong interpersonal and communication skills. The sales executive will be primarily responsible for handling key B2C negotiations and identifying new sales leads along with pitching our products and services</p>
                                	<hr class="divider">
                                			<p><strong>Responsibilities </strong></p>
                                	<ul>
	                                	<li>Developing a sales pipeline and accomplishing a lead generation plan<li>
	                                    <li>Developing and executing a sales strategy for the market that ensures attainment of company sales goals and profitability</li>
	                                    <li>Developing business and contributing to increasing revenue and market share of the company</li>
	                                    <li>Assisting in expansion of business in B2B sales vertical</li>
	                                    <li>Preparing action plans for effective search of sales leads and prospects</li>
	                                    <li>Initiating and coordinating development of action plans for penetration into new markets</li>
	                                    <li>Acting as a point of contact between the company and its existing and potential markets</li>
	                                    <li>Enrolling channel partners from the different markets</li>
	                                    <li>Building and maintaining healthy network with the enrolled channels</li>
	                                    <li>Responsible for overall channel management and generating sales through channels</li>
	                                   
                                	</ul>
	                                	<hr class="divider">
	                                	<p><strong>Desired Candidate </strong></p>
	                                <ul>
	                                    <li>Excellent written and oral communication skills</li>
	                                    <li>Self-driven, ability to work with little supervision and track multiple processes</li>
	                                    <li>Strong analytical and selling skills</li>
	                                    <li>Experience in B2C sales will be preferred</li>
	                                    <li>Professional, responsible, and dependable</li>
	                                    <li>Knowledge of MS-office</li>
	                                </ul>
	                                <hr class="divider">
                                			<p><strong>Work Experience </strong> 1-3 Years</p>
                                			<p><strong>Educational Qualification </strong> Graduate</p>
                              	</div>
                              	<!-- tab content Operations -->
                              <!--	<div class="tab-pane fade" id="Operations">
                                	<p><strong>DGM - Operations - <span class="fw_light">Gurgaon</span> </strong></p>
	                                <p><strong>Number Of Openings</strong><br /> 01</p>
    	                            <hr class="divider">
        	                        <p><strong>Job Overview</strong></p>
            	                    <p>We are looking for a high performing, excellence driven Deputy General Manager- Operations with proven experience in the field of operations. The ideal candidate will be primarily responsible for overseeing day-to-day operations, which can include negotiating contracts with vendors, leading meetings and implementing initiatives handed down by the general manager.</p>
                                	<hr class="divider">
                                	<p><strong>Responsibilities </strong></p>
                                	<ul>
                                		<li>Recruiting, selecting, training, assigning, coaching and disciplining employees<li>
	                                    <li>Planning and reviewing compensation actions and enforcing policies and procedures</li>
                                    	<li>Analyzing and implementing policies and procedures and overseeing short- and long-term business plans</li>
  	                                  	<li>Preparing and completing action plans</li>
  	                                  	<li>Managing productivity, quality and meeting customer-service standards<li>
	                                    <li>Managing staff levels, wages and hours</li>
                                    	<li>Analyzing process workflow, employee and space requirements and equipment layout</li>
  	                                  	<li>Managing relationships with key operations vendors</li>
  	                                  	<li>Communicating customer issues with operations team and devising ways of improving the customer experience including resolving problems and complaints</li>
  	                                  	<li>Working closely with the General Manager and management team to set and/or implement policies, procedures and systems and follow through with implementation</li>
                                	</ul>
                                	<hr class="divider">
                                	<p><strong>Desired Candidate </strong></p>
                                 	<ul>
                                    	<li>Excellent communication skills<li>
                                    	<li>Languages - Hindi, English</li>
                                    	<li>Analytical ability and quick decision making capability</li>
                                    	<li>Leadership skills and process improvement and a problem solver</li>
                                    	<li>Should be well versed in Microsoft office (word and excel)</li>
                                    	<li>Must be process driven and should be adaptable to change and help in driving things</li>
                                	</ul>
                                	<hr class="divider">
                                			<p><strong>Work Experience </strong> 2-4 Years</p>
                                			<p><strong>Educational Qualification </strong> Graduate, MBA</p>
                              	</div>-->
                              	<!-- tab content Business G -->
                              	<!--<div class="tab-pane fade" id="BusinessG">
                                	<p><strong>Intern - Operations - <span class="fw_light">Gurgaon</span> </strong></p>
                                	<p><strong>Number Of Openings</strong><br /> 02</p>
                                	<hr class="divider">
                                	<p><strong>Responsibilities</strong></p>
                                	<ul>
	                                	<li>Tracking end to end order fulfillment including delivery, installation, product demo, documents and customer's feedback collection<li>
	                                    <li>Acting as a liaison between the delivery team, customers, and vendors</li>
	                                    <li>Engaging in problem-solving and process improvement process</li>
	                                    <li>Developing strategies to ensure judicial usage of resources and timely delivery</li>
	                                    <li>Coordinating with the ground team regarding delivery, pickup and repair requests</li>
	                                    <li>Forecasting requirements, preparing an annual budget, scheduling expenditures and analyzing variances</li>
	                                    <li>Managing stock control and overseeing inventory requirements</li>
                                	</ul>
                                	
                              	</div>-->
                              	
            				</div>
                        </div>
                    </div>
            	</div>
                </section>
                <section class="resumenrowbg">
                	<div class="container">
                    	<p>Want to be a part of CityFurnish team? Please send your resume at: 
                        	 <a href="mailto:hr@cityfurnish.com">hr@cityfurnish.com</a>
                        </p>
                    </div>
                </section>
            
            



	
					<!-- 
					 -->
		<!--footer-->
	<?php
$this->load->view('site/templates/footer');
?>

		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>