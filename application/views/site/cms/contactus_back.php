<?php
$this->load->view('site/templates/header_inner');
?>
			<!--main content-->
			<div class="page_section_offset m_bottom_50">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 m_bottom_30 m_xs_bottom_10">
						<!--	<div class="iframe_map_container m_bottom_38 m_xs_bottom_30">
								<img src="images/contactus.jpg" width=100% height="480" alt="Contact cityfurnish" title="Contact cityfurnish">		</div> -->
							<div class="row">
								<main class="col-lg-6 col-md-6 col-sm-6 m_xs_bottom_30">
									<h5 class="color_dark tt_uppercase second_font fw_light m_bottom_13">Have Queries?</h5>
									<hr class="divider_bg m_bottom_25">
									<ul class="second_font m_bottom_25" >
										<li>Read the most frequently asked questions <a href="pages/faq">here</a></li>
									</ul>
									<h5 class="color_dark tt_uppercase second_font fw_light m_bottom_13">Still can not find an answer?</h5>
									<hr class="divider_bg m_bottom_25">
									<ul class="second_font m_bottom_25">
										<li>Feel free to contact us with service questions, business proposals or media inquiries.</li>
									</ul>
									<ul class="second_font vr_list_type_2 m_bottom_33 m_xs_bottom_30">
										<li><i class="fa fa-phone color_dark fs_large"></i>8010845000 [ 10:00 AM - 8:00 PM ] </li>
										<li class="w_break" data-icon=""><i class="fa fa-envelope color_dark"></i><a href="mailto:#" class="sc_hover d_inline_b">hello@cityfurnish.com</a></li>
									</ul>
									<h5 class="color_dark tt_uppercase second_font fw_light m_bottom_13">Our Offices</h5>
									<hr class="divider_bg m_bottom_25">
									<p class="second_font m_bottom_15">Magneto Home Private Limited</p>
<h6 class="m_bottom_15 color_dark">Corporate Office:</h6>

										<p class="second_font m_bottom_15">6B Tower 3, Bellevue Tower, Central Park 2,</p>
										<p class="second_font m_bottom_15">Sohna Road, Sector 48, Gurgaon, Haryana - 122018</p>

<h6 class="m_bottom_15 color_dark">Gurgaon Office:</h6>
										<p class="second_font m_bottom_15">
#530, Block D, JMD Megapolis, Sohna Road,</p>
<p class="second_font m_bottom_15">Sector 48, Gurgaon, Haryana , 122018</p>
<h6 class="m_bottom_15 color_dark">Delhi Office:</h6>
										<p class="second_font m_bottom_15">Unit no. 8, ground floor, civil line, 10, Sham Nath Marg,</p>
										<p class="second_font m_bottom_15">Prema Kunj, Civil Lines, New Delhi - 110054</p>


<h6 class="m_bottom_15 color_dark">Bangalore Office:</h6>
										<p class="second_font m_bottom_15">2nd Floor, D, 310, 6th Cross Rd, 1st Block,</p>
										<p class="second_font m_bottom_15">Koramangala, Bengaluru, Karnataka - 560035</p>

								</main>


							</div>
						</div>
					</div>
				</div>
			</div>
		<!--footer-->
	<?php
$this->load->view('site/templates/footer');
?></div>

		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>