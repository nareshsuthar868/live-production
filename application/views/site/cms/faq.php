<?php
$this->load->view('site/templates/header_inner');
?>
<div class="page_section_offset graylightbg">
  <section class="innerbanner">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1>Frequently Asked Questions</h1>
          <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Frequently Asked Questions</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
    <section class="faqrow">
  	<div class="container">
    <div class="row">
      <section class="col-lg-12 col-md-12 col-sm-12">
        <div class="row">
          <aside class="col-sm-3 col-md-3 col-lg-4 padding-right-5">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#delivery" data-toggle="tab" aria-expanded="true">
                <figure><img src="<?php echo CDN_URL; ?>images/delivery_sm.svg" alt="place" class="icon"/>
                	    <img src="<?php echo CDN_URL; ?>images/delivery_sm_active.svg" alt="place" class="iconac"/></figure>
                		<span>Delivery</span>
               </a></li>
              <li><a href="#Service" data-toggle="tab" aria-expanded="false">
              <figure><img src="<?php echo CDN_URL; ?>images/services_sm.svg" alt="Service"  class="icon"/>
		              <img src="<?php echo CDN_URL; ?>images/services_sm_active.svg" alt="Service" class="iconac"/></figure>
                      <span>Service</span>
              </a></li>
              <li><a href="#Returns" data-toggle="tab" aria-expanded="false">
              <figure><img src="<?php echo CDN_URL; ?>images/return-cancel_sm.svg" alt="Returns and cancellation" class="icon"/>
              	      <img src="<?php echo CDN_URL; ?>images/return-cancel_sm_active.svg" alt="Returns and cancellation" class="iconac"/></figure>
                      <span>Returns and cancellation</span>
              </a></li>
              <li><a href="#Payment" data-toggle="tab" aria-expanded="false">
              	<figure><img src="<?php echo CDN_URL; ?>images/payment_sm.svg" alt="Payment and billing" class="icon"/>
                <img src="<?php echo CDN_URL; ?>images/payment_sm_active.svg" alt="Payment and billing" class="iconac"/></figure>
                <span>Payment and billing</span>
              </a></li>
            <!--  <li><a href="#Howitworks" data-toggle="tab" aria-expanded="false">
              	<figure><img src="<?php echo CDN_URL; ?>images/howitwork_sm.svg" alt="How it works" class="icon"/>
                <img src="<?php echo CDN_URL; ?>images/howitwork_sm_active.svg" alt="How it works" class="iconac"/></figure>
                <span>How it works</span>
              </a></li>
              <li><a href="#Offers" data-toggle="tab" aria-expanded="false">
              	<figure><img src="<?php echo CDN_URL; ?>images/offers_sm.svg" alt="How it works" class="icon"/>
                <img src="<?php echo CDN_URL; ?>images/offers_sm_active.svg" alt="How it works" class="iconac"/></figure>
                       <span>Offers</span>
              </a></li>
              <li><a href="#Referral" data-toggle="tab" aria-expanded="false">
              	<figure><img src="<?php echo CDN_URL; ?>images/refral-program_sm.svg" alt="How it works" class="icon"/>
                <img src="<?php echo CDN_URL; ?>images/refral-program_sm_active.svg" alt="How it works" class="iconac"/></figure>
                <span>Referral Program</span>
              </a></li>-->
            </ul>
          </aside>
          <aside class="col-sm-9 col-md-9 col-lg-8 padding-left-5">
            <div class="tab-content">
              <!-- delivery tab content -->
              <div class="tab-pane fade active in" id="delivery">
                <div class="panel-group" id="accordion">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">What is the minimum tenure for renting?<i class="more-less hidden-xs expand-up"></i> <i class="more-less visible-xs expand-down"></i> </a> </div>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                      <div class="panel-body">The minimum tenure for renting varies from product to product. If your required period is not covered on our product page, we will be happy to discuss your requirement and fulfill the same.</div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> Is there a contract? What are the terms? <i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                      <div class="panel-body">Yes, you are required to sign a contract at the time of delivery. The contract will include the basic terms of renting furniture in simple words. You can view sample of the same<a href="pages/rental-agreement"><u>here</u></a></div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">How can I terminate the contract?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                      <div class="panel-body">The contract can be terminated by providing us 2 weeks prior notice and paying applicable additional charges. Following charges will be applicable to early termination. Termination before minimum tenure of 3 months: Full 3 month's rental due with rates as per 3 months tenure. For fitness products, minimum tenure being 4 weeks, for any cancellation before 4 weeks of rental; the rental amount  for 4 weeks will be payable. If terminated after 3 months; one month extra rent will be charged. For example:  if the contract tenure selected at order placement was 12 months and the subscription is terminated within 4 months, rental of 5 months will be charged.</div>
                    </div>
                  </div>                 
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">What is the delivery timeline?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                      <div class="panel-body">We typically deliver the products within 72 hours of KYC process completion. However, delivery time might vary from product to product and the same is mentioned on the product page. If you need any product before mentioned delivery time, we will be happy to discuss the possibility. Please ensure submitting all your required KYC documents after placing the order, failing to do so might delay the delivery process.</div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">Who will deliver and setup the products?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="collapse9" class="panel-collapse collapse">
                      <div class="panel-body">Our team will be visiting your place at your convenient time to deliver and install products at the delivery address.  Extra charges would be levied for delivery and installation for more than 3 floors, without lift. The same has to be paid by the customer in cash at the time of delivery.</div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">What if my society does not allow delivery vehicles inside?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="collapse10" class="panel-collapse collapse">
                      <div class="panel-body">Please note that you should ensure the entry of delivery vehicle inside the premises. Most of the times, it is not allowed to park the delivery vehicles on the road. Please also ensure that you have completed the required documents and payment work mandatory with the landlord, we have observed that sometimes customers do not have permission to shift into the new house.</div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">What if my building does not have a lift?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="collapse11" class="panel-collapse collapse">
                      <div class="panel-body">In case you do not have a lift or permission to use the lift at your premises, extra labor charges will be applicable to carry the products via stairs; amount will depend on the order size and floor level.</div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">What if I am not at home at the time of delivery?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="collapse12" class="panel-collapse collapse">
                      <div class="panel-body">After you place the order, our representative will give you a call to book a delivery slot. In case you are not at home, you have to appoint a representative (flatmate/friend/relative) to collect the order and sign the contract on your behalf. Lessee or his/her representative has to be present at the agreed date and time. Otherwise extra shipping costs, INR 900 will be charged. Also, Lessee representative should carry an original/soft copy of Lessee ID proof at the time of accepting the delivery</div>
                    </div>
                  </div>
                </div>
              </div>
              
              <!-- services tab content -->
              <div class="tab-pane fade" id="Service"> 
              	<div class="panel-group" id="accordion2">   
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion2" href="#refcollapse2"> What sort of documentation is involved? <i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="refcollapse2" class="panel-collapse collapse">
                      <div class="panel-body">You are required to provide following documents after placing the order.
                        <li><strong>ID Proof -</strong> Company ID Card/Student ID Card</li>
                       <li><strong>Permanent Address Proof -</strong> Passport/Aadhaar/Voter ID Card/Driving License</li>
                        <li><strong> Delivery Address Proof -</strong> In case delivery address is different from permanent address,please provide us rental agreement and contact details of your landlord.</li>
                        <li><strong>Bank Statement -</strong> Last 3 Months</li>
                      </div>
                    </div>
                  </div> 
                   <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion2" href="#refcollapse3">Where do you guys operate?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="refcollapse3" class="panel-collapse collapse">
                      <div class="panel-body">We are currently operating in Delhi, Gurgaon, Noida, Pune, Mumbai and Bangalore. Soon we will launch our services in other major cities as well.</div>
                    </div>
                  </div> 
                   <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion2" href="#refcollapse4">Can we add or delete products from your packages? Will the prices be adjusted?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="refcollapse4" class="panel-collapse collapse">
                      <div class="panel-body">You can make your own package by choosing the individual products. Still if you need any assistance, we are here to help.
                      </div>
                    </div>
                  </div> 
                   <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion2" href="#refcollapse5">Can the contract be transferred?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="refcollapse5" class="panel-collapse collapse">
                      <div class="panel-body">Yes, the contracts can be transferred. In that case a new contract will be created for remaining tenure and you will have to bear the additional cost incurred in relocating the furniture (If any).</div>
                    </div>
                  </div>
                   <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion2" href="#refcollapse6">How can I place a bulk order?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="refcollapse6" class="panel-collapse collapse">
                      <div class="panel-body">For any bulk order or special requirements, call our customer care or drop us a mail. A dedicated account manager will call you and take care of your requirements.</div>
                    </div>
                  </div>     
                </div>
              </div>
              
              <!-- Returns and cancellation tab content -->
              <div class="tab-pane fade" id="Returns">
              	<div class="panel-group" id="accordion3">
                 <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion3" href="#accrding_col1">What is the return process?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="accrding_col1" class="panel-collapse collapse">
                      <div class="panel-body">Our team will call you one week before expiry of contract and to arrange pick up as per your convenience. Your deposit refund will be processed within 7 days of pick up after adjusting for any damage/issue.</div>
                    </div>
                  </div>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion3" href="#howcollapse6">When and how do I get my refundable deposit back?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="howcollapse6" class="panel-collapse collapse">
                      <div class="panel-body">Once the products are picked up from your place, they undergo a  quality check by the QC team at the warehouse. If found damaged, then repair charges will be deducted from your refundable deposit. Also, if there is any amount due towards early termination charges or rental, the same will be adjusted from your deposit, balance if any you will have to pay for the same. The refund will get processed within 7 working days after pickup and after that it will take 7-10 more days to get reflected in your account.</div>
                    </div>
                  </div>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion3" href="#howcollapse7">What is the return policy in case the product is damaged?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="howcollapse7" class="panel-collapse collapse">
                      <div class="panel-body">If product is found damaged at the time of delivery and setup, we will will arrange the replacement of the same.</div>
                    </div>
                  </div>
                </div>
              </div>
              
              <!-- Payment and billing tab content -->
              <div class="tab-pane fade" id="Payment"> 
              	<div class="panel-group" id="accordion4">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion4" href="#paycollapse5">How much do I need to pay and when?<i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="paycollapse5" class="panel-collapse collapse">
                      <div class="panel-body">You need to pay first month's rental and security deposit in advance. For remaining monthly rental, we will collect post dated cheques at the time of delivery of product. You can also use NACH facility for monthly rental payment. Monthly rent to be paid within 5 days after receipt of invoice. Late payment charges at the rate of 10% of the amount due per month (and a minimum of Rs 300) will be payable after a period of 5 days. Cityfurnish, reserves the right to cancel the subscription and pickup of products if amount due is not paid within 2 weeks of due date.</div>
                    </div>
                  </div>

                   <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion4" href="#refcollapse11"> What is the mode of payment? <i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="refcollapse11" class="panel-collapse collapse">
                      <div class="panel-body">At the time of placing the order, you can pay first rental and security deposit by Debit /Credit / Net Banking/ Citrus Wallet. For remaining duration, our team will collect post-dated cheques from you at the time of delivery. You can also use NACH facility or online payment mode for monthly rental payment.</div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion4" href="#refcollapse12"> How to register for auto debit of monthly rental? <i class="more-less expand-down"></i></a> </div>
                    </div>
                    <div id="refcollapse12" class="panel-collapse collapse">
                      <div class="panel-body">You can register for auto debit of monthly payment by providing standing instructions on credit card or ENACH on bank account while placing order or later at any time. Amount will be deducted monthly for tenure of your subscription. You can cancel auto debit anytime by simply contacting our team. Standing instructions and ENACH will get cancelled automatically on tenure expiry or order cancellation.</div>
                    </div>
                  </div>

                </div>
              </div>
              
              <!-- How it works tab content -->
              <div class="tab-pane fade" id="Howitworks"> 
              	<div class="panel-group" id="accordion5">                  
                </div>
              </div>
              
              <!-- Offers tab content -->
              <div class="tab-pane fade" id="Offers"> 
              	<div class="panel-group" id="accordion6">
                
                 
                </div>
              </div>
              
              <!-- Referral tab content -->
              <div class="tab-pane fade" id="Referral"> 
              	<div class="panel-group" id="accordion7">
                 
                </div>
              </div>
            </div>
          </aside>
      </div>
  </section>
</div>
</div>
</section>
</div>

<section class="section_offset clientreviewsrow text-center">
    <div class="container">
        <div class="col-xs-12">
            <h2>Client Reviews</h2>
        </div>
        <div class="clientslider">
            <?php 
            foreach ($clreviewList as $review){ 
                $reImage = '';
                if($review->image != ''){
                    $reImage = explode(',', $review->image)[0];
                }
                ?>
                <div>
                    <div class="testimonialreviewcol">  
                        <article class="frame_container">
                            <figure> 
                            <div class="usrimg">
                                <span>     
                                    <img src="<?php echo CDN_URL; ?>images/product/<?php echo $reImage; ?>" alt="" class="tr_all scale_image">
                                </span>
                                </div>
                                <figcaption>
                                    <div class="post_excerpt m_bottom_15 t_align_c relative">
                                        <p><?php echo substr($review->excerpt,0,200 ); if(strlen($review->excerpt) > 200){ echo '....';}?></p>
                                        <?php $custArray = explode(',',$review->product_name);
                                        if(isset($custArray[0])){
                                            echo "<b>".$custArray[0]."</b>";
                                        }
                                        if(isset($custArray[1])){
                                            echo "<span class='nw_scheme_gray'>, ".$custArray[1]."</span>";
                                        } ?>
                                    </div>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="text-center margin-top-30"> <a href="<?php echo base_url(); ?>reviews-testimonials/all" class="btn-check">View more Client Review</a> </div>
    </div>
</section>
<?php  ?>

			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>

		
		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>