<?php
$this->load->view('site/templates/header_inner');
?>
			<!--main content-->
			<div class="page_section_offset">
  <section class="innerbanner">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1>Referral Terms & Conditions</h1>
          <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Referral Terms & Conditions</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section class="rentaldocrow">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse23">1. Who can refer? <i class="more-less hidden-xs expand-up"></i> <i class="more-less visible-xs expand-down"></i> </a> </div>
              </div>
              <div id="collapse23" class="panel-collapse collapse in">
                <div class="panel-body">
                  <p>Only Cityfurnish customers can use referral program to refer their friends and family</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">2. How can I refer? <i class="more-less expand-down"></i> </a> </div>
              </div>
              <div id="collapse1" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>You can share your referral code on any social platform such as Facebook, Twitter etc from our Referral Page or you can even mail your link to your friends and family</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">3. How can I use referral code?<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>If you have a referral code, please enter it in the "Coupon Code" box on checkout page before the payment on the website. Applicable one time discount amount is applied immediately.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">4. How can I claim the referral benefit? <i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>The referrer gets a mail notification once their referral code is used by any of their friends. Contact our customer care via email or phone on receipt of notification to get applicable discount. Amount will be adjusted against remaining rental. No cashbacks are permitted against referral benifit.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">5. Is there a limit on benefit? <i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>You can refer as many friends as you want. You get benefit on every successful conversion</p>
                  <p>Referred customer can not club referral benefit with any other offer</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">6. Can I use my own referral code?<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>You can not use your own referral code</p>
                  <p>Cityfurnish reserves the right to revoke referral benefits availed by individuals who share a common address with the referrer</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">7. Other Terms & Conditions<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse7" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Referral program is not appliable on fitness equipments and office furniture</p>
                  <p>Referrer should place an order of min 1000 Rs monthly rental to avail benefit of referral program</p>
                  <p>Cityfurnish reserves the right to revoke referral benefits if they were earned against our terms or close the referral program anytime without any prior intimation</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>


		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/jquery.easytabs.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>