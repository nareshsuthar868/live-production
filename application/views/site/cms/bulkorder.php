<?php
$this->load->view('site/templates/header_inner');
?>
<style>
 .textboxcol{position: relative;}   
.textboxcol label.error{position: absolute;bottom:5px;left:0px;width: 100%;}

</style>

<div class="page_section_offset"> 
  <section class="bulkorderbg">
  <!--	<div class="bulktopbanner">
                <div class="bulkimgoverlay">
                	<h1>Get Your Hospitality, Office and Restaurant Furniture<br /> <span>Style, durability and sturdiness in your furniture</span></h1>
                </div>
             </div>-->
    <?php
    //if ($page_heading != '') { ?>
        <section class="innerbanner" style="margin-bottom:20px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Office, Hospitality and Home Furniture</h1>
                        <p class="margin-top-20 hidden-xs">If your home or business requirement calls for a bulk order, Cityfurnish offers turnkey services that cater to all needs: from setting up a new home, to furniture for your office or business venture.</p>
                    </div>
                </div>
            </div>
        </section>
    <?php //} ?>            
    <div class="container">
    	<div class="row">
        	<div class="col-xs-12">
            	<div class="maksurerow">
                <!--	<h2>We will make sure to provide you the best of the lot.</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>		-->
                <div class="hospitalitycol">
                	<i class="bulkficon"><img src="images/bulk-furniture-1.svg" alt="Icon" /></i>	
                    <h6>Hospitality</h6>
                    <p>Furniture for Hotels and Restaurants</p>
                </div>
                <div class="hospitalitycol">
                	<i class="bulkficon"><img src="images/bulk-furniture-2.svg" alt="Icon" /></i>	
                    <h6>Office</h6>
                    <p>Office Furniture and Other Equipments</p>
                </div>
                <div class="hospitalitycol">
                	<i class="bulkficon"><img src="images/bulk-furniture-3.svg" alt="Icon" /></i>	
                    <h6>Residential</h6>
                    <p>Furniture & Appliances for Managed Homes</p>
                </div>    
                
                </div>
            </div>
        </div>
    </div>
  </section>
  <section class="projectcomprow">
  <div class="container">
    	<div class="row">
        	<h3>Previous completed projects</h3>
            <div class="col-xs-6 col-sm-6 col-md-6 margin-bottom-20">
            	<img src="images/bulk-prev-project-1.jpg" alt="Project" />
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 padding-left-5 padding-right-5">
            	
                	<div class="bulkhalfwcol"><img src="images/bulk-prev-project-2.jpg" alt="Project" /></div>
                    <div class="bulkhalfwcol"><img src="images/bulk-prev-project-3.jpg" alt="Project" /></div>
                    <div class="bulkhalfwcol"><img src="images/bulk-prev-project-4.jpg" alt="Project" /></div>
                    <div class="bulkhalfwcol"><img src="images/bulk-prev-project-5.jpg" alt="Project" /></div>
              
            </div>
        </div>
    </div>
  </section>
  <section class="bulkorderformrow">
  	<div class="container">
    	<div class="row">
        	<div class="col-sm-9 col-md-7">
            	<h3>Get in Touch</h3>
                <p>Mention your requirements in brief and we will get back to you promptly</p>
                <div class="bulkfrmwhite">
                	<form id="bulk_order_form" name="bulk_order_form">
                    	<div class="bulkinput">
                           <div class="textboxcol"> 
                            <label>Name</label>
                        	<input type="text" id="User_Name" name="User_Name" class="form-control" />
                           </div>
                           <div class="textboxcol">
                            <label>Email Address</label>
                        	<input type="email" id="Email"  name="Email" class="form-control" />
                         </div>
                         <div class="textboxcol">
                            <label>Phone</label>
                        	<input type="text" id="Phone"  name="Phone" class="form-control" />
                        </div>
                        <div class="textboxcol">
                            <label>City</label>
                        	<input type="text" id="City"  name="City" class="form-control" />
                        </div>
                        </div>
                        <div class="bulkinput">
                         
                            <label>Message</label>
							<textarea  id="Message"  name="Message" class="form-control"></textarea>
                            <input type="submit" onclick="BulkOrder()" id="bulk_order_button" class="btn-check" value="Submit" />
                            <!-- <button type="button" onclick="BulkOrder()" class="btn-check">Submit</button> -->
                        </div>
                    </form>
                </div>
               <h3 id="response_msg" style="display: none;">hello</h3>
            </div>
        </div>
    </div>
  </section>
  <section class="quotionsrow">
  	<div class="container">
    	<div class="row">
        	<div class="col-xs-6 col-sm-6 col-md-6">
            	<div class="borderboxcol">
                  <div class="thumbpic">	
                    <img src="images/help-quotion-pic.jpg" alt="Quotation" />
                  </div>
                    <h6>Help With Quotation</h6>
                    <p>You will get a elaborated quotation covering all aspects of the offerings</p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="borderboxcol">
               <div class="thumbpic">
                <img src="images/fast-delivery-pic.jpg"  alt="Fast Delivery"/>
                <strong>Call : 8010845000</strong>
               </div>  
            	<h6>Custom Made Products</h6>
                <p>We provide custom made furniture as per the demand of your project</p>
              </div>  
            </div>
        </div>
    </div>
  </section>
</div>
<?php
$this->load->view('site/templates/footer');
?>
</div>
 </body>
</html>