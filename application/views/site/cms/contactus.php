<?php
$this->load->view('site/templates/header_inner');
?>

<div class="page_section_offset"> 
  <!-- New  html -->
  <section class="innerbanner">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1>Contact Us</h1>
          <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"> Contact Us</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section class="gotqueryrow contacequery">
    <section class="container">
      <div class="row">
        <div class="centerquerycontent"> 
          <!--<aside class="col-sm-5 col-md-5 col-lg-5">
            <div class="largeiconborder"> <img src="images/got-query-icn.svg" alt="Got Any Queries"> </div>
          </aside>-->
         	<div class="col-sm-6 col-md-6 col-lg-6">
            	<h2>Have Queries?</h2>
            	<h3>Read the most frequently asked questions here</h3>
            	<div class="leftbordercol">
              		<div class="panel-group" id="accordion">
                		<div class="panel panel-default">
                  			<div class="panel-heading">
                    			<div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> What is the minimum tenure for renting? <i class="more-less hidden-xs expand-up"></i> <i class="more-less visible-xs expand-down"></i> </a> 
                    			</div>
                  			</div>
                  			<div id="collapse1" class="panel-collapse collapse in">
                    			<div class="panel-body">The minimum tenure for renting varies from product to product. If your required period is not covered on our product page, we will be happy to discuss your requirement and fulfill the same.</div>
                  			</div>
                		</div>
                		<div class="panel panel-default">
                  			<div class="panel-heading">
                    			<div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Is there a contract? What are the terms? <i class="more-less expand-down"></i></a> </div>
                  			</div>
                  			<div id="collapse2" class="panel-collapse collapse">
                    			<div class="panel-body">Yes, you are required to sign a contract at the time of delivery. The contract will include the basic terms of renting furniture in simple words. You can view sample of the same <a href="pages/rental-agreement">here</a>.</div>
                  			</div>
                		</div>
              		</div>
              		<a href="<?php echo base_url()?>pages/faq" class="btn-check">Check faq</a> </div>
          		</div>
          		<div class="col-sm-6 col-md-6 col-lg-6">
            		<h2>Still can not find an answer?</h2> 
            		<h3>Feel free to contact us with service questions,<br />
              		 business proposals or media inquiries.</h3>
            		<div class="findcontact">
              			<div class="findcontactbg">
                			<div class="croundshpaicn"><span><i class="material-icons">account_circle</i></span></div>
                			<span><strong>Raise a Service Request</strong><br />
                			Log request against your order from <a href="https://cityfurnish.com/usersettings">My Account</a> section</span> 
                		</div>
              			<div class="findcontactbg">
                			<div class="croundshpaicn"><span><i class="material-icons">phone</i></span></div>
                			<span><strong>Phone</strong> <br />
                			+(91) 8010845000 ( 09:00AM - 09:00PM )</span> 
                		</div>
              			<div class="findcontactbg">
                			<div class="croundshpaicn"><span><i class="material-icons">mail_outline</i></span></div>
                			<span><strong>Email</strong><br />
                			<a href="mailto:hello@cityfurnish.com">hello@cityfurnish.com</a> </span>
                		</div>
              		</div>
          		</div>
        	</div>
    	</div>
    </section>
  	</section>
  	<section class="ourofficerow">
    	<div class="container">
     		<div class="row">
        		<div class="cols-sm-12 col-md-12 col-lg-12">
          			<h2>OUR OFFICES</h2>
          			<h3>Cityfurnish India Private Limited</h3>
        		</div>
        		<!--<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 offic-col-center">
          			<article class="officecol">
            			<div class="officetitle">Corporate Office</div>
            			<p>6B Tower 3, Bellevue Tower, Central Park 2, Sohna Road, Sector 48, Gurgaon, Haryana - 122018</p>
          			</article>
        		</div>-->
        		<div class="col-xs-12 col-sm-4 offic-col-center">
          			<article class="officecol">
            			<div class="officetitle">Gurgaon Office</div>
            			<p>525-527, Block D, JMD Megapolis, Sohna Road, Sector 48, 
              			Gurgaon, Haryana , 122018</p>
          			</article>
        		</div>
        		<!--<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 offic-col-center">-->
          <!--			<article class="officecol">-->
          <!--  			<div class="officetitle">Delhi Office</div>-->
          <!--  			<p>Sham Nath Marg, Prema Kunj, Civil Lines, New Delhi - 110054</p>-->
          <!--			</article>-->
        		<!--</div>-->
        		<div class="col-xs-12 col-sm-4 offic-col-center">
          			<article class="officecol">
            			<div class="officetitle">Noida Office</div>
            			<p>#72, Shahadra Sector-141,<br />
              			Noida, UP</p>
          			</article>
        		</div>
        		<div class="col-xs-12 col-sm-4 offic-col-center">
          			<article class="officecol">
            			<div class="officetitle">Bangalore Office</div>
            			<p>2nd Floor, D, 310, 6th Cross Rd, 1st Block , Koramangala
              			Bengaluru, Karnataka - 560035</p>
          			</article>
        		</div>
        		<div class="col-xs-12 col-sm-4 offic-col-center">
          			<article class="officecol">
            			<div class="officetitle">Pune Office</div>
            			<p>03A, 4th Floor, City Vista, Kharadi, Pune, <br />
              			Maharashtra - 411014</p>
          			</article>
        		</div>
        		<div class="col-xs-12 col-sm-4 offic-col-center">
          			<article class="officecol">
            			<div class="officetitle">Mumbai Office</div>
            			<p>Yadav Nagar, Chandivali, Powai Mumbai, Maharashtra - 400072</p>
          			</article>
        		</div>
      		</div>
    	</div>
  	</section>
</div>

			<!--main content-->
		<!-- 	<div class="page_section_offset m_bottom_50">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 m_bottom_30 m_xs_bottom_10"> -->
						<!--	<div class="iframe_map_container m_bottom_38 m_xs_bottom_30">
								<img src="images/contactus.jpg" width=100% height="480" alt="Contact cityfurnish" title="Contact cityfurnish">		</div> -->
							<!-- <div class="row">
								<main class="col-lg-6 col-md-6 col-sm-6 m_xs_bottom_30">
									<h5 class="color_dark tt_uppercase second_font fw_light m_bottom_13">Have Queries?</h5>
									<hr class="divider_bg m_bottom_25">
									<ul class="second_font m_bottom_25" >
										<li>Read the most frequently asked questions <a href="pages/faq">here</a></li> -->
									<!-- </ul>
									<h5 class="color_dark tt_uppercase second_font fw_light m_bottom_13">Still can not find an answer?</h5>
									<hr class="divider_bg m_bottom_25">
									<ul class="second_font m_bottom_25">
										<li>Feel free to contact us with service questions, business proposals or media inquiries.</li>
									</ul>
									<ul class="second_font vr_list_type_2 m_bottom_33 m_xs_bottom_30">
										<li><i class="fa fa-phone color_dark fs_large"></i>8010845000[ 10:00 AM - 8:00 PM ] </li>
										<li class="w_break" data-icon=""><i class="fa fa-envelope color_dark"></i><a href="mailto:#" class="sc_hover d_inline_b">hello@cityfurnish.com</a></li>
									</ul>
									<h5 class="color_dark tt_uppercase second_font fw_light m_bottom_13">Our Offices</h5>
									<hr class="divider_bg m_bottom_25">
									<p class="second_font m_bottom_15">Magneto Home Private Limited</p>
									<h6 class="m_bottom_15 color_dark">Corporate Office:</h6>
									<p class="second_font m_bottom_15">6B Tower 3, Bellevue Tower, Central Park 2,</p>
									<p class="second_font m_bottom_15">Sohna Road, Sector 48, Gurgaon, Haryana - 122018</p>
									<h6 class="m_bottom_15 color_dark">Gurgaon Office:</h6>
									<p class="second_font m_bottom_15">#540, Block D, JMD Megapolis, Sohna Road,</p>
									<p class="second_font m_bottom_15">Sector 48, Gurgaon, Haryana , 122018</p>
									<h6 class="m_bottom_15 color_dark">Delhi Office:</h6>
									<p class="second_font m_bottom_15">Unit no. 8, ground floor, 10 Sham Nath Marg,</p>
									<p class="second_font m_bottom_15">Prema Kunj, Civil Lines, New Delhi - 110054</p>
									<h6 class="m_bottom_15 color_dark">Noida Office:</h6>
									<p class="second_font m_bottom_15">#72, Shahadra Sector-141,</p>
									<p class="second_font m_bottom_15">Noida, UP</p>
									<h6 class="m_bottom_15 color_dark">Bangalore Office:</h6>
									<p class="second_font m_bottom_15">2nd Floor, D, 310, 6th Cross Rd, 1st Block Koramangala, Koramangala</p>
									<p class="second_font m_bottom_15">Bengaluru, Karnataka - 560035</p>
									<h6 class="m_bottom_15 color_dark">Pune Office:</h6>
									<p class="second_font m_bottom_15">305, Cosmos, Magarpatta City, Hadapsar</p>
									<p class="second_font m_bottom_15">Pune, Maharashtra - 411028</p>
									<h6 class="m_bottom_15 color_dark">Mumbai Office:</h6>
									<p class="second_font m_bottom_15">Yadav Nagar, Chandivali, Powai</p>
									<p class="second_font m_bottom_15">Mumbai, Maharashtra - 400072</p> -->										


<!--<h6 class="m_bottom_15 color_dark">Bangalore Office:</h6>
										<p class="second_font m_bottom_15">2nd Floor, D, 310, 6th Cross Rd, 1st Block,</p>
										<p class="second_font m_bottom_15">Koramangala, Bengaluru, Karnataka - 560035</p>-->

								<!-- </main> -->


						<!-- 	</div>
						</div> -->
						<!--<div class="col-lg-6 col-md-6 col-sm-6 m_bottom_30 m_xs_bottom_10">						
							<div class="row">
							<p>&nbsp;&nbsp;</p><p>&nbsp;&nbsp;</p>
							<h5 class="color_dark tt_uppercase second_font fw_light m_bottom_13">Complains?</h5>
							<hr class="divider_bg m_bottom_25">
							<form name="item" action="" method="post">
								<div class="card card-block">
								<?php if($this->session->userdata['msgasas']){ ?>
									<div class="form-group row" id="removealkala"> <label class="col-sm-2 form-control-label text-xs-right">
						
					</label>
										<div class="col-sm-10" style="color:green;"> <?php echo $this->session->userdata['msgasas']; ?> </div>
									</div>	
<p>&nbsp;&nbsp;</p>									
								<?php $this->session->unset_userdata('msgasas'); ?>							
								<?php } ?>							
								<?php if($msg!=''){ ?>
									<div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
						
					</label>
										<div class="col-sm-10" style="color:red;"> <?php echo $msg; ?> </div>
									</div>	
<p>&nbsp;&nbsp;</p>									
								<?php } ?>
									<div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
						Your Email:
					</label>
										<div class="col-sm-10"> <input style="width: 50%;" type="text" class="form-control boxed" placeholder="" name="uemail" value="<?php echo $uemail; ?>" required maxlength="100"> </div>
									</div>	
<p>&nbsp;&nbsp;</p>									
									<div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
						Your Contact No:
					</label>
										<div class="col-sm-10"> <input style="width: 50%;" type="text" class="form-control boxed" placeholder="" value="<?php echo $ucontact; ?>" name="ucontact" required> </div>
									</div>
									<p>&nbsp;&nbsp;</p>									
								<?php if(!empty($topics)){ ?>
									<div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
						Query:
					</label>
										<div class="col-sm-10"> 
										<select name="uquery" class="form-control">
											<option value="">Select</option>
									<?php foreach($topics as $row){ ?>
											<option value="<?php echo $row->ID; ?>" <?php if($uquery==$row->ID){ echo "selected"; } ?>><?php echo $row->t_title; ?></option>
									<?php } ?>
										</select>
										</div>
									</div>
								<?php } ?>
									<p>&nbsp;&nbsp;</p>
									<div class="form-group row"> <label  class="col-sm-2 form-control-label text-xs-right">
						Description:
					</label>
										<div class="col-sm-10">
											<textarea rows="3" class="form-control" style="width: 50%;" name="udescription"><?php echo $udescription; ?></textarea>
										</div>
									</div>
									<p>&nbsp;&nbsp;</p>
									<div class="form-group row">
										<div class="col-sm-10 col-sm-offset-2"> <button type="submit" value="Submit" name="submit" class="m_xs_float_r add_to_cart button_type_2 d_inline_b f_sm_none m_sm_bottom_3 t_align_c lbrown state_2 tr_all second_font fs_medium tt_uppercase f_left">Submit</button></div>
									</div>
								</div>
							<input type="hidden" name="uquery" id="valalaquery" value="<?php echo $uquery; ?>" />	-->
						<!-- 	</form>							
							</div>
						</div>
					</div>
				</div>
			</div> -->
		<!--footer-->
	<?php
$this->load->view('site/templates/footer');
?></div>

<!-- WhatsHelp.io widget -->
<!-- <script type="text/javascript">
	    (function () {
	        var options = {
	            whatsapp: "9871309994", // WhatsApp number
	            company_logo_url: "//static.whatshelp.io/img/flag.png", // URL of company logo (png, jpg, gif)
	            greeting_message: "Hello, how may we help you? Just send us a message now to get assistance.", // Text of greeting message
	            call_to_action: "Message us", // Call to action
	            position: "left", // Position may be 'right' or 'left'
	        };
	        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
	        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
	        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
	        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
	    })();
	</script> -->								
<!-- /WhatsHelp.io widget -->

		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
		<style>
			input[type="radio"]{ display: block !important; }
			fs_ex_large{ color: red; }
		</style>
		<script>
			$(document).ready(function(){
				setTimeout(function(){ $('#removealkala').fadeOut(); }, 2000);
			});
		</script>
	</body>
</html>