<?php
$this->load->view('site/templates/header_inner');
?>
					<div class="bg_grey_light">
						<div class="container" style="padding-top:20px;padding-bottom: 30px;margin-bottom: 30px;">
							  <h1 class="second_font color_dark m_bottom_10 tt_uppercase t_align_c p_top_25">EXCLUSIVE OFFER</h1>
								<div class="row sh_container p_bottom_20">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 same_height t_align_c">
										<p>For Bangalore</p>
									</div>								
								</div>
						</div>
					</div>
				<div class="container">
					<section class="m_bottom_48 m_xs_bottom_30">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4 m_bottom_30 m_xs_bottom_10">						
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 m_bottom_30 m_xs_bottom_10">						
							<div class="m_xs_bottom_30">
								<div class="pricing_table type_2 t_align_c fw_light relative">
									<header class="color_white border_lbrown bg_lbrown tt_uppercase second_font fs_large">
										<b>Festive Offer</b>
									</header>
									<ul class="pt_list">
										<li class="pt_price bg_grey_light_2">
											<dl class="scheme_color">
												<dt class="m_bottom_7 second_font"><b>100% OFF</b></dt>
												<dd>on last month's rent</dd>
											</dl>
										</li>
										<li class="relative bg_white">Coupon Code: <b>CITYBLR</b></li>
										<li class="relative bg_white">100% OFF on last month's rent of furniture and home appliances</li>
										<li class="relative bg_white">Applicable on min 6 months tenures</li>
										<li class="relative bg_white">Max Discount Limit: Rs 3000</li>
										<li class="relative bg_white"></li>									
									</ul>
								</div>
							</div>
						</div>				
						<div class="col-lg-4 col-md-4 col-sm-4 m_bottom_30 m_xs_bottom_10">						

						</div>
						</div>

					</section>
					<h3 class="m_bottom_13">T&C</h3>
					<hr class="divider m_bottom_25 animated hidden" data-animation="fadeInLeft" data-animation-delay="100">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 m_bottom_30 m_xs_bottom_10">
						<ul>
						<li class="relative m_bottom_10"><i class="fa fa-star scheme_color" style="margin-right:10px;"></i>Only one offer can be availed per order.</li>
						<li class="relative m_bottom_10"><i class="fa fa-star scheme_color" style="margin-right:10px;"></i> These offers can not be clubbed with referral program benefits.</li>

						<li class="relative m_bottom_10"><i class="fa fa-star scheme_color" style="margin-right:10px;"></i> For more detail about availing this offer, get in touch with our customer care team.</li>	
						</ul>
						</div>
					</div>
					</div>
			</div>

		<!--footer-->
	<?php
$this->load->view('site/templates/footer');
?></div>

		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>