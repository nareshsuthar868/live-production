<?php
//$this->load->view('site/templates/header_new');
$this->load->view('site/templates/header_inner');
?>
<div class="page_section_offset mobileheight"> 
    <!-- New  html -->
    <section class="thankyoupagerow">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <img src="<?php echo base_url(); ?>images/cancel-order-icn.svg" />
                    <h1>Oops! Your transaction failed</h1>
                    
                    <p>An email has been sent to - <b><i><?php echo $userEmail; ?></i></b> with the failure details.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- New  html --> 
</div>

<?php
$this->load->view('site/templates/footer');
?>
