<?php
$this->load->view('site/templates/header_new');

?>
<div class="page_section_offset">
    <div class="container">
        <div class="row m_bottom_50">
            <aside class="col-lg-4 col-md-4 col-sm-4 p_top_4">
            </aside>
            <section class="col-lg-4 col-md-4 col-sm-4">
                <h2 class="fw_light second_font color_dark m_bottom_27 tt_uppercase t_align_c">Payment Failed</h2>
            </section>
            <div style="clear:both;"></div>
            <section class="col-lg-12">
            <div class="payment_success" align="center">
                <h3 style="color:red">Sorry!, your transaction is failed. Please try again</h3><br>
                
	        <br>
                <p>An email has been sent to - <b><i><?php echo $userEmail; ?></i></b> with the failure details.</p><br> 
            </div> 
            </section>               
        </div>
    </div>
</div>
<?php
$this->load->view('site/templates/footer');
?>
