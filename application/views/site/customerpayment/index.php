<?php
$this->load->view('site/templates/header_inner');
//Should be unique for every transaction            
$merchantTxnId = rand(1,1000000);                                    
?>  
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>   
<!--<script id="context" type="text/javascript" src="https://sboxcheckout-static.citruspay.com/kiwi/app-js/icp.js"></script>-->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!--<link rel="stylesheet" href="resources/demos/style.css">-->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
    $( function() { 
      $("#start_date").datepicker({ minDate: 0, dateFormat: 'dd-mm-yy'  });
      $("#end_date").datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
      $(".nach_field").hide();
       
    } );
  </script>

<script id="context" type="text/javascript" src="https://checkout-static.citruspay.com/kiwi/app-js/icp.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/core.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/sha1.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/enc-base64.min.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
          $(document).off('click', '#btnSubmit').on('click', '#btnSubmit', function(e) {
          
            var fname = $.trim($('#user_first_name').val());
                var lname = $.trim($('#user_last_name').val());
                var uemail = $.trim($('#user_email').val());
                var uamount = $.trim($('#user_amount').val());
                var uinvoice = $.trim($('#user_invoice_number').val());
                var unotes = $.trim($('#user_notes').val());
                var tenure = $.trim($('#tenure_payment').val());
                var startDate = $.trim($('#date').val());
                var amountregEx = /^\d+(\.\d{1,2})?$/;
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

              
                
              $.ajax(
              {
                method: "POST", 
                data: { firstName: fname, 
                        lastName: lname, 
                        userEmail: uemail, 
                        invoiceNumber: uinvoice, 
                        notes: unotes, 
                        amount: uamount 
                }, 
                url: "customerpayment/setOneTimePaymentDetails", 
                success: function(result){
                  var secret_key = "<?php echo CITRUS_SECRET_KEY; ?>";
                  var data= "<?php echo CITRUS_VANITY_URL; ?>"+uamount+"<?php echo $merchantTxnId;?>"+"<?php echo CITRUS_CURRENCY_TYPE; ?>";
                  var encrypted = CryptoJS.HmacSHA1(data, secret_key);
                  var securitySignature = CryptoJS.enc.Hex.stringify(encrypted);
                  //var securitySignature = encrypted; 
                  var dataObj = {
                      orderAmount: uamount,
                      currency: "<?php echo CITRUS_CURRENCY_TYPE; ?>",
                      email: uemail,
                      merchantTxnId: "<?php echo $merchantTxnId; ?>",
                      returnUrl: "<?php echo CITRUS_RETURN_URL; ?>",
                      vanityUrl: "<?php echo CITRUS_VANITY_URL; ?>",
                      secSignature: securitySignature,
                      firstName: fname,
                      lastName: lname,                  
                      mode: "dropOut",
              };
            configObj = {
                eventHandler: function(cbObj) {
                    
                if (cbObj.event === 'icpLaunched') {
                  console.log('Overlay is launched');                  
                } else if (cbObj.event === 'icpClosed') {
                  console.log(JSON.stringify(cbObj.message));
                  console.log('Overlay is closed');
                }
             }
           };
           citrusICP.launchIcp(dataObj, configObj);
        }});
      
      });
    });
</script> 
<div class="page_section_offset">
  <section class="innerbanner">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          
                            
          <h1>Pay Now</h1>
                                               
          <ul class="breadcrumb">
            <li><a href="<?php echo  base_url();?>">Home</a></li>
             <li><a href="#">Help</a></li>
            <li class="active">Customer Payment</li>                                                                                   
           
          </ul>            
        </div>
      </div>
    </div>
  </section>
  <div class="container">
    <div class="row">
      <section class="col-lg-5 col-md-4 col-sm-6 col-sm-offset-3 col-md-offset-4"> 
        <!--<h2 class="fw_light second_font color_dark m_bottom_27 tt_uppercase t_align_c">Pay Now</h2>-->
        <form method="post" action="customerpayment/CustomerPayment" class="frm clearfix" id="customer_payment_form" >
        <div class="paymentradio">
          <div class="radiocol">
            <input type="radio" name="payment_option" value="one_time" id="one-time" checked  onchange="change_payment_type('one_time')">
            <label for="one-time">One Time Payment</label>
          </div>
          <div class="radiocol">
            <input type="radio" name="payment_option" value="rcpayment" id="rcpayment"  onchange="change_payment_type('rcpayment')">
            <label for="rcpayment">SI on Credit Card</label>
          </div>
          <!--<div class="radiocol">-->
          <!--  <input type="radio" name="payment_option" value="enach" id="enach"  onchange="change_payment_type('enach')">-->
          <!--  <label for="enach">ENACH</label>-->
          <!--</div>-->
        </div>
          <div class="frmpayment">
            <div class="m_bottom_15">
              <label>First Name</label>
              <input type="text"  name="user_first_name" id="user_first_name" class="form-control" autofocus="autofocus" value="<?php echo set_value('user_first_name'); ?>">
            </div>
            <div class="m_bottom_15">
              <label>Last Name (Optional)</label>
              <input  type="text"  name="user_last_name" id="user_last_name" class="form-control" value="<?php echo set_value('user_last_name'); ?>">
            </div>
            <div class="m_bottom_15">
              <label>Email (associated with your account at cityfurnish.com)</label>
              <input required type="email" name="user_email" id="user_email" class="form-control" value="<?php if(!empty($userEmail)){ echo $userEmail; }else{ echo set_value('user_email');} ?>" <?php if(!empty($userID)){ echo "readonly"; } ?>>
            </div>
            <div class="m_bottom_15">
              <label>Amount (Rs.)</label>
              <input type="text" name="user_amount" id="user_amount" class="form-control" value="<?php echo set_value('user_amount'); ?>">
            </div>
            <div class="m_bottom_15">
              <label>Invoice Number (Optional)</label>
              <input type="text" name="user_invoice_number" id="user_invoice_number" class="form-control" value="<?php echo set_value('user_invoice_number'); ?>">
            </div>
             <div class="m_bottom_15 recurring-field" style="display: none;">
              <label>Order ID</label>
              <input type="text"  class="form-control" autofocus="autofocus" name="order_id" id="order_id">
            </div>
            <div class="m_bottom_15">
              <label>Notes (Optional)</label>
              <input type="text" name="user_notes" id="user_notes" class="form-control" value="<?php echo set_value('user_notes'); ?>">
              <?php if(validation_errors() != ''){?>
                  <br>
                  <div id="validationErr" class="alert_box warning m_bottom_10 relative fw_light"> 
                    <!--<script>setTimeout("hideErrDiv('validationErr')", 3000);</script>--> 
                    <span class="d_inline_m second_font fs_medium color_red d_md_block">
                        <?php //echo validation_errors();?></span> 
                  </div>
              <?php }?>
            </div>

           <!--  <div class="m_bottom_15 recurring-field" style="display: none;">
                <select name="tenure_payment" id="tenure_payment" class="form-control">
                  <option value="" selected="true"> -- Select Tenure --</option>
                  <option value="1 Month">1 Month</option>
                  <option value="2 Months">2 Months</option>
                  <option value="3 Months">3 Months</option>
                  <option value="4 Months">4 Months</option>
                  <option value="5 Months">5 Months</option>
                  <option value="6 Months">6 Months</option>
                  <option value="7 Months">7 Months</option>
                  <option value="8 Months">8 Months</option>
                  <option value="9 Months">9 Months</option>
                  <option value="10 Months">10 Months</option>
                  <option value="11 Months">11 Months</option>
                  <option value="12 Months">12 Months</option>
              </select>        
            </div> -->
            <div class="m_bottom_15 nach_field">
              <label>Account Number</label>
              <input type="text"  class="form-control" autofocus="autofocus" name="account_number" id="account_number">
            </div>
            <div class="m_bottom_15 nach_field">
              <label>Mobile Number</label>
              <input type="text"  class="form-control" autofocus="autofocus" name="mobile_number" id="mobile_number">
            </div>

            <div class="m_bottom_15 nach_field">
              <label>Debit Start Date</label>
              <input type="text" name="start_date" class="form-control" id="start_date" readonly placeholder="mm/dd/yyyy">
           </div>
            <div class="m_bottom_15 nach_field">
              <label>Debit End Date</label>
              <input type="text" name="end_date" class="form-control" id="end_date" readonly placeholder="mm/dd/yyyy">            
            </div>
            <div class="m_bottom_15 nach_field">
              <label>Amount Type</label>
              <input type="text" name="amountType" id="amountType" class="form-control"  placeholder="INR">            
            </div>
            <div class="m_bottom_15 nach_field">
                <select name="recurring_frequency" id="recurring_frequency" class="form-control">
                  <option value="" selected="true"> -- Select Recurring Frequency --</option>
                  <option value="DAIL">Daily</option>
                  <option value="Week">Weekly</option>
                  <option value="MNTH">Monthly</option>
                  <option value="QURT">Quarterly</option>
                  <option value="MIAN">Semi annually</option>
                  <option value="YEAR">Yearly</option>
                  <option value="BIMN">Bi- monthly</option>
                  <option value="ADHO">As and when presented</option>
                </select>              
            </div>
              <div class="m_bottom_15 nach_field">
                <label>Card Number</label>
                <input type="text" name="card_number" id="card_number" class="form-control"  placeholder="1234 5678 8909">           
              </div>
            <div class="m_bottom_15 nach_field">
              <label>Expire Month</label>
              <select name="expire_month" id="expire_month" class="form-control">
                  <option value="" selected="true"> -- Select Card Expire Month--</option>
                  <?php for ($i=1; $i <= 12 ; $i++) { 
                    echo "<option value=".$i.">$i</option>";
                  } ?>
                </select> 
              </div>
            <div class="m_bottom_15 nach_field">
                  <label>Expire Year</label>
                  <select class="form-control" name="expiry_year" id="expiry_year">
                      <option value="" selected="true">-- Expire Year --</option>
                       <?php for ($i= date("Y"); $i <= 2040 ; $i++) { 
                        echo '<option value="'.$i.'">'.$i.'</option>';
                      } ?>
                  </select>
            </div>
            <div class="m_bottom_15 nach_field">
              <label>CVV</label>
              <input type="text" name="cvv" id="cvv" class="form-control"  placeholder="123">          
            </div>
            <div class="row  recurring-field" style="display: none;">
             <font color=red>You will receive gift voucher worth Rs <?php echo $voucher_data->row()->reward; ?> on successful registration.</font>
            </div><br>
            <div> 
               <button type="submit" class="btn-std  btn-check" name="camper">PROCEED</button>
            </div>

             <br>
          <?php if(isset($_SESSION['Error_message'])){
            echo '<div class="alert alert-danger">';
            echo $_SESSION['Error_message'];
            echo '</div>';
          } ?>

          </div>
        </form>
      </section>
    </div>
  </div>
</div>
<?php
$this->load->view('site/templates/footer');
?>