<?php
$this->load->view('site/templates/header_new');

?>
<div class="page_section_offset">
    <div class="container">
        <div class="row m_bottom_50">
            <aside class="col-lg-4 col-md-4 col-sm-4 p_top_4">
            </aside>
            <section class="col-lg-4 col-md-4 col-sm-4">
                <h2 class="fw_light second_font color_dark m_bottom_27 tt_uppercase t_align_c">Payment Success</h2>
            </section>
            <div style="clear:both;"></div>
            <section class="col-lg-12">
            <div class="payment_success" align="center">
                <h3 style="color:green">Thank you! Your transaction is successful.</h3><br>
                <p>You can note down the following details for future reference.</p><br>
                <table>
                	<tr>
	                	<td align="right">Transaction Reference Number:</td><td><?php echo $txnRefNumber; ?></td>
	                </tr>
	                <tr>
	                	<td align="right">TPSL Transaction ID:</td><td><?php echo $tpslTxnID; ?></td>
	                </tr>
	                <tr>
	                	<td align="right">Amount Paid:</span><td><?php echo $amountPaid; ?>&nbsp;<?php echo TECHPROCESS_CURRENCY_TYPE; ?></td>
	                </tr>
	        </table>
	        <br>
                <p>Transaction details have been emailed to - <b><i><?php echo $userEmail; ?></i></b></p><br> 
            </div> 
            </section>               
        </div>
    </div>
</div>
<?php
$this->load->view('site/templates/footer');
?>
