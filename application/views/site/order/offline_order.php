<?php 
$this->load->view('site/templates/header_inner'); ?>
<!--main content-->

<?php 
// echo "<p/**/mation);exit;
  if($Confirmation == 'Success'){ ?> 
    <div class="page_section_offset mobileheight"> 
        <!-- New  html -->
        <section class="thankyoupagerow">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                  <img src="<?php echo CDN_URL; ?>images/thankyou-thumb.svg" />
                  <h1>Thank you for your order</h1>
                  <p>You will receive an email confirmation shortly at your registered email ID.</p>
              </div>
            </div>
          </div>
        </section> 
    <script>
    $(window).on("load", function () {
        // alert("dfdfdfdf");
        CreateOfflineOrderZohoInvoices("<?php echo  $_SESSION['offline_user_id']?>","<?php echo  $_SESSION['offline_dealcode']?>");
        var win_width = $(window).width();
        //alert(win_width);
        if (win_width < 767) {
            $('.bg_white').addClass('fullheight');
        }
    });
    </script> 

<?php 

 $this->output->set_header('refresh:5;url='.base_url()); 
 }elseif($Confirmation =='Failure'){ ?> 
    <div class="page_section_offset mobileheight"> 
    <!-- New  html -->
    <section class="thankyoupagerow">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <img src="<?php echo CDN_URL; ?>images/cancel-order-icn.svg" />
                    <h1>Oops! Your transaction failed</h1>
                    <p>Please try again or call us on 8010845000, so that we can assist you..</p>
                </div>
            </div>
        </div>
    </section>
    <!-- New  html --> 
</div>
    <?php 
    $this->output->set_header('refresh:5;url='.base_url()); 
}elseif($Confirmation =='Message'){ ?>
    <div class="page_section_offset mobileheight"> 
    <section class="thankyoupagerow">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php if($this->session->flashdata('payment_flash')){
                        $message = $this->session->flashdata('payment_flash');?>
                        <img src="<?php echo CDN_URL; ?>images/<?php echo $message['img']; ?>"/>
                        <h1><?php echo $message['message']; ?></h1>

                    <?php } ?>
                    <!-- <p>Please try again or call us on 8010845000, so that we can assist you..</p> -->
                </div>
            </div>
        </div>
    </section>
    <!-- New  html --> 
</div>


<?php
$this->output->set_header('refresh:5;url='.base_url()); } ?>

<?php if($Confirmation == 'Success'){ ?>
<script>
    $(window).on("load", function () {
        var win_width = $(window).width();
        if (win_width < 767) {
            $('.bg_white').addClass('fullheight');
        }
    });
</script>
<?php } ?>

<!--footer-->
<?php $this->load->view('site/templates/footer'); ?>
<!--footer-->


</body>
</html>