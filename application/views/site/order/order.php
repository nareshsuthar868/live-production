<?php $this->load->view('site/templates/header_inner'); ?>
<!--main content-->

<?php
if(isset($_SESSION['OrderUserCart']) && !empty($_SESSION['OrderUserCart'])) {
    $_SESSION['OrderUserCart'] = '';
    unset($_SESSION['OrderUserCart']);
}
?>
<?php if($Confirmation == 'Success'){
    ?>
      <!-- Event snippet for Order Placement conversion page --> 
      <script> gtag('event', 'conversion', { 'send_to': 'AW-867888766/_s9cCJSEuYkBEP7c650D', 'transaction_id': '<?php echo  $_SESSION['OrderID']?>' }); </script>
 <script>
  fbq('track', 'Purchase');
</script>
    <div class="page_section_offset mobileheight"> 
  <!-- New  html -->
  <section class="thankyoupagerow">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
            <img src="<?php echo base_url(); ?>images/thankyou-thumb.svg" />
            <h1>Thank you for your order</h1>
            <p>You will receive an email confirmation shortly at your registered email ID.</p>
        </div>
      </div>
    </div>
  </section>  
</div>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '202224353551343');
  fbq('track', 'PageView');
  fbq('track', 'Purchase', {value: '0.00', currency:'INR'});
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=202224353551343&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<?php if(isset($_SESSION['OrderAmount']) && $_SESSION['OrderAmount']!="") { ?>
        <!-- Offer Conversion: City Furnish - CPV -->
        <iframe src="https://clickonik.go2cloud.org/aff_l?offer_id=901&adv_sub=<?php echo $_SESSION['OrderID']; ?>&amount=<?php echo $_SESSION['OrderAmount']; ?>" scrolling="no" frameborder="0" width="1" height="1"></iframe>
        <!-- // End Offer Conversion -->

        <script type='text/javascript' src="https://track.in.omgpm.com/1122614/transaction.asp?APPID=<?php echo $_SESSION['OrderID']; ?>&MID=1122614&PID=32325&status=<?php echo $_SESSION['OrderAmount']; ?>">
        </script>
        <noscript>
            <img src="https://track.in.omgpm.com/apptag.asp?APPID=<?php echo $_SESSION['OrderID']; ?>&MID=1122614&PID=32325&status=<?php echo $_SESSION['OrderAmount']; ?>" border="0" height="1" width="1">
        </noscript>
        <?php
    }    
$this->output->set_header('refresh:5;url='.base_url().'purchases');
 }elseif($Confirmation =='Failure'){ ?> 
    <div class="page_section_offset mobileheight"> 
    <!-- New  html -->
    <section class="thankyoupagerow">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <img src="<?php echo base_url(); ?>images/cancel-order-icn.svg" />
                    <h1>Oops! Your transaction failed</h1>
                    <p>Please try again or call us on 8010845000, so that we can assist you..</p>
                </div>
            </div>
        </div>
    </section>
    <!-- New  html --> 
</div>
    <?php $this->output->set_header('refresh:5;url='.base_url().'cart'); 
}
if($this->uri->segment(3) == 'subscribe'){
    $this->output->set_header('refresh:5;url='.base_url().'fancyybox/manage'); 
 }elseif($this->uri->segment(3) == 'gift'){
    $this->output->set_header('refresh:5;url='.base_url().'settings/giftcards'); 
 }elseif($this->uri->segment(3) == 'cart'){
    $this->output->set_header('refresh:5;url='.base_url().'documentation'); 
 }  ?>

<!--footer-->
<?php $this->load->view('site/templates/footer'); ?>
<!--footer-->
<script>
    $(window).on("load", function () {
        CreateZohoCases("<?php echo  $_SESSION['user_id']?>","<?php echo  $_SESSION['dealCodeNumber']?>");
        var win_width = $(window).width();
        //alert(win_width);
        if (win_width < 767) {
            $('.bg_white').addClass('fullheight');
        }
    });
</script>

<?php if($Confirmation != 'Success'){ ?>
<script>
    $(window).on("load", function () {
        $.ajax({
            url :  "<?php echo base_url()?>site/order/getOrderDetails",
            type: 'POST',
            dataType: 'json',
            data: null,
            success: function(x){
                // console.log("x")
            }
      });
    });
</script>
<?php } ?>
</body>
</html>