<?php $this->load->view('site/templates/header_inner'); ?>
<div class="page_section_offset">
  <section class="container">
      <script type="text/javascript">
      var error_message = "<?php if(isset($_SESSION['order_error'])){ echo $_SESSION['order_error']; unset($_SESSION['order_error']); }?>";

      if(error_message){ sweetAlert('',error_message, 'warning');}
      
       var error_message = "<?php if(isset($_SESSION['order_error'])){ echo $_SESSION['order_error']; unset($_SESSION['order_error']); }?>";
      if(error_message){ sweetAlert('',error_message, 'warning');}

      var verification_error = "<?php if(isset($_SESSION['verification_error'])){ echo $_SESSION['verification_error']; unset($_SESSION['verification_error']); }?>";

      if(verification_error){ 

          sweetAlert({
                    title: "Verify!",
                    text: verification_error,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: "Verify Now",
                    animation: "slide-from-bottom",
                    showConfirmButton: true,      
                },function(isConfirm){   
                    if (isConfirm) {
                      window.location.href  = baseURL + 'usersettings';

                    }
                  }); 
      }


      var email_verification_error = "<?php if(isset($_SESSION['email_verification_error'])){ echo $_SESSION['email_verification_error']; unset($_SESSION['email_verification_error']); }?>";

      if(email_verification_error){ 

          sweetAlert({
                    title: "Verify!",
                    text: email_verification_error,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: "Verify Now",
                    animation: "slide-from-bottom",
                    showConfirmButton: true,      
                },function(isConfirm){   
                    if (isConfirm) {
                        resendConfirmation('<?php echo $_SESSION['session_user_email']; ?>');
                    }
          }); 
      }
    </script>
    <div class="row">
        <?php echo $cartViewResults; ?>
    </div>
  </section>
</div>
<!--footer-->
<script type="text/javascript">

 $(document).ready(function(){
  $('#newaddress').on('click', function () {
  $('#cartSubmit').trigger("reset");})
});

$(function(){
  $('#newaddress').prop('checked', true);
})
</script>
<?php $this->load->view('site/templates/footer'); ?>



<?php //}?>
<script src="js/step-panel-jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#sectional').formalize({
    timing: 300,
    nextCallBack: function(){
      if (validateEmpty($('#sectional .open'))){
        scrollToNewSection($('#sectional .open'));
        return true;
      };
      return false;
    },
    prevCallBack: function(){
      return scrollToNewSection($('#sectional .open').prev())
    }
  });
  $('#global').formalize({
    navType: 'global',
    prevNav: '#global-nav-prev',
    nextNav: '#global-nav-next',
    timing: 300,
    nextCallBack: function(){
      return validateEmpty($('#global .open'));
    }
  });

  $('#btn-global').on('click', function(){
    $('#btn-sectional').removeClass('disabled');
    $(this).addClass('disabled');
    $('#sectional').hide();
    $('#global').show();
  });

  $('#btn-sectional').on('click', function(){
    $('#btn-global').removeClass('disabled');
    $(this).addClass('disabled');
    $('#global').hide();
    $('#sectional').show();
  });

  $('input').on('keyup change', function(){
    $(this).closest($('.valid')).removeClass('valid');
  });
  
  $('#newaddress').click(function(){
      $('.addressfrm').show();
      $('.saveaddresscol').hide();
  });
  $('#saveaddress').click(function(){
      $('.addressfrm').hide();
      $('.saveaddresscol').show();
  });

  function validateEmpty(section){
    var errors = 0;
    section.find($('.required-field')).each(function(){
      var $this = $(this),
        input = $this.find($('input'));
      if (input.val() === ""){
        errors++;
        $this.addClass('field-error');
        $this.append('\<div class="form-error-msg">This field is required!\</div>');
      }
    });
    if (errors > 0){
      section.removeClass('valid');
      return false;
    }
    section.find($('.field-error')).each(function(){
      $(this).removeClass('field-error');
    });
    section.find($('.form-error-msg')).each(function(){
      $(this).remove();
    });
    section.addClass('valid');
    return true;
  }
          function scrollToNewSection(section){
    var top = section.offset().top;
                        $("html, body").animate({
                                scrollTop: top
                        }, '100');
    return true;
  } 
});

    function CaputrPayment(){
          $.ajax({
                  url :  "<?php echo base_url()?>site/order/capturePayment",
                  type: 'POST',
                  dataType: 'json',
                  data: null,
                  success: function(x){
                    // console.log("x")
                  }
              });
    }
    
</script>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script> $("#shippingAddForm").validate(); </script> 