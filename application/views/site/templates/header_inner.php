<?php
// get user email
$useremail = '';
if (isset($_SESSION['fc_session_user_id']) && $_SESSION['fc_session_user_id'] != '') {
    $useremail = trim(strtolower($_SESSION['session_user_email']));
}
// Product Detail page criteo tag
if (isset($productDetails) && $this->uri->segment(1) == 'things') {
    $productid = $productDetails->row()->id;
    $ecommercePD[0]['name']    =   $productDetails->row()->product_name;
    $ecommercePD[0]['id']      =   (int) $productid;
    $ecommercePD[0]['price']   =   $productDetails->row()->price;
    $ecommercePD = json_encode($ecommercePD);
}

// Listing page criteo tag
if (isset($productList) && !empty($productList)) {
    $ecommerceProducts = array();
    $count = 0;
    foreach ($productList->result() as $productListVal) {
        $productArry[] = $productListVal->id;
        $ecommerceProducts[$count] = array(
         'id'       => (int) $productListVal->id,
         'name'     => $productListVal->product_name,
         'price'    => $productListVal->price,
        );
        $count++;
    }
    $productArry = json_encode($productArry);
    $ecommerceProducts = json_encode($ecommerceProducts);
}
if (isset($itemsIncart) && !empty($itemsIncart) && isset($cartViewResults)) {
    $ProductBasketProducts = array();
    $ecommerceCart = array();
    foreach ($itemsIncart as $k => $v) {
        if (count($ProductBasketProducts) < 3) {
            $ProductBasketProducts[$k]['id'] = (int) $v->product_id;
            $ProductBasketProducts[$k]['price'] = $v->price;
            $ProductBasketProducts[$k]['quantity'] = (int) $v->quantity;
        }
        $ecommerceCart[$k] = array(
         'id'       => (int) $v->product_id,
         'name'     => $v->product_name,
         'price'    => $v->price,
         'quantity' => (int) $v->quantity
        );
    }
    $ProductBasketProducts = json_encode($ProductBasketProducts);
    $ecommerceCart = json_encode($ecommerceCart);
}

if (isset($_SESSION['OrderUserCart']) && $_SESSION['OrderUserCart'] != '' && isset($Confirmation) && $Confirmation == 'Success') {
    $orderId = $_SESSION['OrderID'];
    $cartdata = $_SESSION['OrderUserCart'];
    $ordertotal = $_SESSION['OrderAmount'];
    $ecommerceOrder = array();
    $ProductTransactionProducts = array();
    foreach ($cartdata as $k => $v) {
        $ProductTransactionProducts[$k]['id'] = (int) $v['product_id'];
        $ProductTransactionProducts[$k]['price'] = $v['price'];
        $ProductTransactionProducts[$k]['quantity'] = (int) $v['quantity'];
        
        $ecommerceOrder[$k]['id'] = (int) $v['product_id'];
        $ecommerceOrder[$k]['name'] = $v['product_name'];
        $ecommerceOrder[$k]['price'] = $v['price'];
        $ecommerceOrder[$k]['quantity'] = (int) $v['quantity'];
    }
    $ProductTransactionProducts = json_encode($ProductTransactionProducts);
    $ecommerceOrder = json_encode($ecommerceOrder);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <?php echo ($header_code_snippet != '') ? $header_code_snippet : ''; ?>
        <script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
        <script type="text/javascript">
            var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
            var dataLayer = dataLayer || [];
            dataLayer.push(
                {'email': '<?php echo $useremail; ?>'}
                );
        </script>
        <?php if (isset($productList) && !empty($productList)) { ?>
            <script type="text/javascript">
                dataLayer.push(
                    {event: "setAccount", account: 61755},
                    {event: "setSiteType", type: deviceType},
                    {event: "setEmail", email: '<?php echo $useremail; ?>'},
                    {event: "viewList", item: <?php echo $productArry; ?>},
                    {'PageType': 'ListingPage', 'email': '<?php echo $useremail; ?>', 'ProductIDList': <?php echo $productArry; ?>}
                );
                dataLayer.push(
                    {event: "productImpressions", ecommerce: {currencyCode: "INR", impressions:<?php echo $ecommerceProducts; ?>}}
                );
            </script>            
        <?php } ?>

        <?php if (isset($productDetails) && $this->uri->segment(1) == 'things') { ?>
            <script type="text/javascript">
                dataLayer.push(
                    {event: "setAccount", account: 61755},
                    {event: "setSiteType", type: deviceType},
                    {event: "setEmail", email: '<?php echo $useremail; ?>'},
                    {event: "viewList", item: <?php echo $productid; ?>},
                    {'PageType': 'ProductPage','email': '<?php echo $useremail; ?>','ProductID': <?php echo $productid; ?>}
                    );
                dataLayer.push(
                    {event: "EEproductClick", ecommerce: {click: {products: <?php echo $ecommercePD; ?>}}}
                );
            </script>            
        <?php } ?>

        <?php if (isset($itemsIncart) && !empty($itemsIncart) && isset($cartViewResults)) {
            ?>
            <script type="text/javascript">
                dataLayer.push(
                    {event: "setAccount", account: 61755},
                    {event: "setSiteType", type: deviceType},
                    {event: "setEmail", email: '<?php echo $useremail; ?>'},
                    {event: "viewBasket", item: <?php echo $ProductBasketProducts; ?>},
                    {'PageType': 'BasketPage','email': '<?php echo $useremail; ?>','ProductBasketProducts': <?php echo $ProductBasketProducts; ?>});
                dataLayer.push(
                    {event: "EEaddToCart", ecommerce: {add: {products: <?php echo $ecommerceCart; ?>}}}
                );
            </script>

        <?php } ?>

        <?php if (isset($_SESSION['OrderUserCart']) && $_SESSION['OrderUserCart'] != '' && isset($Confirmation) && $Confirmation == 'Success') { ?>
            <script type="text/javascript">
                dataLayer.push(
                    {event: "setAccount", account: 61755},
                    {event: "setSiteType", type: deviceType},
                    {event: "setEmail", email: '<?php echo $useremail; ?>'},
                    {event: "trackTransaction", id: '<?php echo $orderId; ?>', item: <?php echo $ProductTransactionProducts; ?>},
                    {'PageType': 'TransactionPage','email': '<?php echo $useremail; ?>','ProductTransactionProducts': <?php echo $ProductTransactionProducts; ?>,'TransactionID': '<?php echo $orderId; ?>'});
                    
                dataLayer.push(
                    {
                     event: "EEtransaction", 
                      ecommerce: {
                       purchase:{
                        actionField : {
                         id :'<?php echo $orderId; ?>',  
                         affiliation : 'Online Store', 
                         revenue : <?php echo $ordertotal; ?>
                        },
                        products : <?php echo $ecommerceOrder; ?>
                       }
                      }
                    }
                );    
                    
            </script>
        <?php } ?>
        
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-PF4G2HJ');</script>
        <!-- End Google Tag Manager -->

        <?php
        if ($this->config->item('google_verification')) {
            echo stripslashes($this->config->item('google_verification'));
        }
        if ($meta_title != '' and $meta_title != $title) {
            ?>
            <title><?php echo $meta_title; ?></title>
        <?php } elseif ($heading != '') { ?>
            <title><?php echo $heading; ?></title>
        <?php } else { ?>
            <title><?php echo $title; ?></title>
<?php } ?>
        <meta name="Title" content="<?php echo $meta_title; ?>" />
        

        <!--add responsive layout support-->
        <!-- <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <!--meta info-->
        <meta name="author" content="">
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <meta property="og:image" content="<?php echo base_url(); ?>images/product/<?php echo $this->data['meta_image']; ?>" />
        <base href="<?php echo base_url(); ?>" />
        <!--include favicon-->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>images/logo/<?php echo $fevicon; ?>">

        <?php
        if (is_file('google-login-mats/index.php')) {
            require_once 'google-login-mats/index.php';
        }
        ?>

        <!--<link href='css/Roboto_slab.css' rel='stylesheet' type='text/css'>-->
        <!--stylesheet include-->
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet" defer/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" defer/>
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>css/bootstrap.min.css" defer/>
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>css/style.css" defer/>
        <script src="<?php echo base_url()?>assets/bootstrap-sweetalert/dist/sweetalert.js" async defer></script>
        <link href="<?php echo base_url()?>assets/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css" defer/>
        <script src="<?php echo base_url() ?>assets/bootstrap-sweetalert/dist/swalExtend.js" async defer></script>
        <link href="<?php echo base_url() ?>assets/bootstrap-sweetalert/dist/swalExtend.css" rel="stylesheet" type="text/css"defer/>
    
        <script src="<?php echo base_url(); ?>js/jquery-2.1.1.min.js" ></script>
        <script src="<?php echo base_url(); ?>js/modernizr.min.js" ></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.js" ></script>
        
          <!-- Lazy load images  -->
        <script src="<?php echo base_url()?>assets/js/jquery.unveil.js"></script>
        <script>
            $(function() {
                $('.listcategoryrowmobile').css({
                    height: 'auto',
                    visibility: 'visible'
                });
                $("img").unveil(400, function() {
                  $(this).load(function() {
                    this.style.opacity = 1;
                  });
                });
            });
        </script>
    <a href="https://plus.google.com/+Cityfurnish" rel="publisher"></a>
    <a href="https://plus.google.com/+Cityfurnish?rel=author"></a>
    
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1232711836899459');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1232711836899459&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


</head>
<body class="hi">
    <!-- Google Tag Manager (noscript)-->
    <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PF4G2HJ"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) --> 

    <!--layout-->
    <div class="wide_layout db_centered bg_white">
        <!--[if (lt IE 9) | IE 9]>
                                        <div class="bg_red" style="padding:5px 0 12px;">
                                        <div class="container" style="width:1170px;"><div class="row wrapper"><div class="clearfix color_white" style="padding:9px 0 0;float:left;width:80%;"><i class="fa fa-exclamation-triangle f_left m_right_10" style="font-size:25px;"></i><b>Attention! This page may not display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer browsing experience.</b></div><div class="t_align_r" style="float:left;width:20%;"><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode" class="button_type_1 d_block f_right lbrown tr_all second_font fs_medium" target="_blank" style="margin-top:6px;">Update Now!</a></div></div></div></div>
                                <![endif]-->
          <header class="stickyinner">
            <div class="header_top_part">
                <div class="container1">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 t_xs_align_c">
                            <p> <span><img src="<?php echo CDN_URL; ?>images/top-percent-icn.svg" alt="percenticn"></span> <span>Flat 50% Off for first 3 months. Hurry! Limited Period Offer. <a class="sc_hover d_inline_b tt_lowercase" style="color:white;" href="<?php echo base_url(); ?>pages/offers"> <u> Know more.. </u></a></span> </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header_bottom_part w_inherit">
                <div class="container1">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="headertop">
                                <div class="mobilemenu">
                                    <a href="javascript:void(0)" class="togglemenu"> <i class="material-icons">menu</i> </a>
                                </div>
                                <div class="logo clearfix t_sm_align_c">
                                    <a href="<?php echo base_url(); ?>" class="d_inline_b hide_on_mobile">
                                        <img src="<?php echo CDN_URL; ?>images/logo-white.png" alt="Logo Here" class="logounstick">
                                        <img src="<?php echo CDN_URL; ?>images/logo-stick.png" alt="Logo Here" class="logostick">
                                    </a>
                                    <!-- mobile logo -->
                                    <a href="<?php echo base_url(); ?>" class="d_inline_b show_on_mobile">
                                        <img src="<?php echo CDN_URL; ?>images/mobile-logo-white.png" alt="Logo Here" class="innerlogostick">
                                        <img src="<?php echo CDN_URL; ?>images/mobile-logo-stick.png" alt="Logo Here" class="innerlogounstick">
                                    </a>
                                </div>
                                <div class="mobileoverlay"></div>
                                
                                
                                <div class="header-right-part">
                                <nav role="navigation" class="navigation">
                                    <div class="menuheader  visible-xs">
                                        <div class="mobilemenu">
                                            <a href="javascript:void(0)" class="togglemenu"> <i class="material-icons">menu</i> </a>
                                        </div>
                                        <div class="logo"> <a href="<?php echo base_url(); ?>" class="d_inline_b show_on_mobile"> <img src="<?php echo CDN_URL; ?>images/mobile-logo-white.png" alt="Logo Here"> </a> </div>
                                        <div class="header-right-top" >
                                            <ul class="hr_list shop_list f_right second_font fs_medium f_sm_none d_sm_inline_b t_sm_align_l scheme_color_font">
                                                <li><a  href="mailto:hello@cityfurnish.com"> <i class="material-icons">mail_outline</i></a> </li>
                                                <li><a  href="tel:8010845000"><i class="material-icons">phone</i></a>
                                            </ul>
                                        </div>
                                    </div>
                                    <ul class="main_menu hr_list">
                                        <li class="mobile-search-bar">
                                            <input type="text" id="new_click" name="" placeholder="Search">
                                        </li>
                                        <li class="dropdown city-dropdown visible-xs">
                                            <div class="btn-group"><i class="citytoggle"><img src="<?php echo CDN_URL; ?>images/location-sm.svg" alt="Cabinet"></i><a class="dropdown-toggle active" data-toggle="dropdown" href="javascript:void(0);">
                                                    <?php
                                                    if ($_SESSION['prcity'] && $_SESSION['prcity'] == '45') {
                                                        echo "<span>Delhi</span>";
                                                    } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '46') {
                                                        echo "<span>Bangalore</span>";
                                                    } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '47') {
                                                        echo "<span>Pune</span>";
                                                    } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '48') {
                                                        echo "<span>Mumbai</span>";
                                                    } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '49') {
                                                        echo "<span>Gurgaon</span>";
                                                    } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '50') {
                                                        echo "<span>Ghaziabad/Noida</span>";
                                                    } else {
                                                        echo "<span>City</span>";
                                                    }
                                                    ?>
                                                    <span class="caret"> <i class="material-icons adown">keyboard_arrow_down</i> <i class="material-icons aup">keyboard_arrow_up</i> </span> </a>

                                                <ul class="dropdown-menu">
                                                    <?php foreach($cityList->result() as $val) { ?>
                                                        <?php
                                                            if($val->list_value_seourl == 'delhincr') {
                                                                $imgname = 'delhi';
                                                            } else if($val->list_value_seourl == 'bangalore') {
                                                                $imgname = 'banglore';
                                                            } else {
                                                                $imgname = $val->list_value_seourl;
                                                            }
                                                            // hyderabad
                                                       if($val->list_value_seourl != 'hyderabad') { ?>
                                                        <li><a href="javascript:void(0)" onclick="setcity('<?php echo $val->id; ?>')"><i><img src="<?php echo CDN_URL; ?>images/<?php echo $imgname.'.svg'; ?>" alt="<?php echo $val->list_value; ?>"></i><?php echo $val->list_value; ?></a></li>
                                                    <?php } } ?>
                                                </ul>
                                            </div>
                                        </li>
                                        <?php
                                        foreach ($categoriesTree as $row) {
                                            if ($row->cat_name != '' && $row->cat_name != 'Our Picks') {
                                                $active = '';
                                                if (strpos(current_url(), "/" . $row->seourl) !== false) {
                                                    $active = 'active';
                                                }
                                                if ($row->image != '') {
                                                    $catImage = CDN_URL . 'images/category/' . $row->image;
                                                } else {
                                                    $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
                                                }
                                                if (isset($row->sub_categories)) {
                                                    if ($row->cat_name != 'Addon') {
                                                        ?>
                                                        <li class="hidden-xs"> <a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>" class="tt_uppercase tr_delay <?php echo $active; ?>"><?php echo $row->cat_name; ?></a> </li>
                                                        <li class="dropdown  visible-xs"> <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>"><i><img src="<?php echo $catImage; ?>" alt=""></i><?php echo $row->cat_name; ?> <span class="caret"> <i class="material-icons adown">keyboard_arrow_down</i> <i class="material-icons aup">keyboard_arrow_up</i> </span> </a>
                                                            <ul class="dropdown-menu">
                                                                <?php
                                                                foreach ($row->sub_categories as $subCat) {
                                                                    $subcatImage = '';
                                                                    if ($subCat->image != '') {
                                                                        $catImage = CDN_URL . 'images/category/' . $subCat->image;
                                                                        $subcatImage = "<i><img src='" . $catImage . "' alt=''></i>";
                                                                    }
                                                                    ?>
                                                                    <li><a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $subCat->seourl; ?>"><?php echo $subcatImage . $subCat->cat_name; ?></a></li>
                                                        <?php } ?>
                                                            </ul>
                                                        </li>
                                                    <?php } ?>
                                                <?php } else if ($row->cat_name != 'Cred' && $row->cat_name != 'Credr2') { 
                                                    ?>
                                                    <li><a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>" class="tt_uppercase tr_delay <?php echo $active; ?>"><i><img src="<?php echo $catImage; ?>" alt=""></i> <?php echo $row->cat_name; ?> </a> </li>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
<!-- <li> <a class="tt_uppercase"  target="_blank" href="http://vior.in"><i><img src="<?php echo CDN_URL; ?>images/clearance-sale-sm.svg" alt=""></i>Clearance Sale</a> </li> -->
                                          <li class="dropdown dividebefore">
                                                <a href="pages/bulkorder" class="tt_uppercase tr_delay bulkorder <?php if ($this->uri->segment(2) == 'bulkorder') {echo 'active';}?>"> <i><img src="<?php echo base_url() ?>images/logo/bulkorder.svg" alt=""></i>Bulk Order</a>
                                            </li>
                                        <li class="dropdown showdesktop hidden-xs">
                                            <a href="javascript:void(0)">Help <span class="material-icons">keyboard_arrow_down</span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="pages/how-it-works">How It Works?</a></li>
                                                <li><a href="customerpayment" target="_blank">Customer Payment</a></li>
                                                <li><a href="pages/faq">FAQs</a></li>
                                                <li><a href="reviews-testimonials/all">Customer Reviews</a></li>
                                                <li><a href="pages/careers">We are hiring</a></li>
                                                <li><a href="blog/" target="_blank">Blog</a></li>
                                                <li><a href="pages/contact-us">Contact us</a></li>

                                            </ul>
                                        </li>
                                        <li class="dropdown visible-xs"> <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"><i><img src="<?php echo CDN_URL; ?>images/help-sm.svg" alt=""></i>Help <span class="caret"> <i class="material-icons adown">keyboard_arrow_down</i> <i class="material-icons aup">keyboard_arrow_up</i> </span> </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url(); ?>pages/contact-us">Contact us</a></li>
                                                <li><a href="<?php echo base_url(); ?>pages/how-it-works">How It Works?</a>  </li>
                                                <li><a href="<?php echo base_url(); ?>pages/faq">FAQs</a></li>
                                                <li><a href="<?php echo base_url(); ?>reviews-testimonials/all">Customer Reviews</a></li>
                                                <li><a href="customerpayment" target="_blank">Customer Payment</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown visible-xs">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"><i><img src="<?php echo CDN_URL; ?>images/information-sm.svg" alt=""></i>Information
                                                <span class="caret">
                                                    <i class="material-icons adown">keyboard_arrow_down</i>
                                                    <i class="material-icons aup">keyboard_arrow_up</i>
                                                </span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="blog/" target="_blank">Blog</a></li>
                                                <!--<li><a target="_blank" href="http://vior.in">Clearance Sale</a></li>-->
                                                <li><a href="<?php echo base_url(); ?>pages/offers">Offers</a></li>
                                                <li><a href="<?php echo base_url(); ?>pages/careers">We are hiring</a></li>
                                                <li><a href="<?php echo base_url(); ?>pages/friends-and-partners">Friends & Partners</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown visible-xs">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"><i><img src="<?php echo CDN_URL; ?>images/policies-sm.svg" alt=""></i>Policies
                                                <span class="caret">
                                                    <i class="material-icons adown">keyboard_arrow_down</i>
                                                    <i class="material-icons aup">keyboard_arrow_up</i>
                                                </span>
                                            </a>


                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url(); ?>pages/terms-of-use">Terms of use</a></li>
                                                <li><a href="<?php echo base_url(); ?>pages/privacy-policy">Privacy policy</a></li>
                                                <li><a href="<?php echo base_url(); ?>pages/refer-a-friend">Referral Terms of use</a></li>
                                                <li><a href="<?php echo base_url(); ?>pages/rentalagreement">Sample Rental Agreement</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                                <ul class="hr_list headeropt">
                                    <li class="header-search">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><i class="material-icons" id="new_click">search</i></a>
                                    </li>

                                    <li>
                                        <!-- new dropdown design -->
                                        <div class="btn-group"> <i class="material-icons">room</i>
                                            <a class="dropdown-toggle white_span" data-toggle="dropdown" href="javascript:void(0)">
                                                <?php
                                                if ($_SESSION['prcity'] && $_SESSION['prcity'] == '45') {
                                                    echo "<span>Delhi</span>";
                                                } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '46') {
                                                    echo "<span>Bangalore</span>";
                                                } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '47') {
                                                    echo "<span>Pune</span>";
                                                } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '48') {
                                                    echo "<span>Mumbai</span>";
                                                } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '49') {
                                                    echo "<span>Gurgaon</span>";
                                                } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '50') {
                                                    echo "<span>Ghaziabad/Noida</span>";
                                                } else {
                                                    echo "<span>Select city</span>";
                                                }
                                                ?>
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <?php foreach($cityList->result() as $val) { ?>
                                                    <?php
                                                        if($val->list_value_seourl == 'delhincr') {
                                                            $imgname = 'delhi';
                                                        } else if($val->list_value_seourl == 'bangalore') {
                                                            $imgname = 'banglore';
                                                        } else {
                                                            $imgname = $val->list_value_seourl;
                                                        }
                                                    if($val->list_value_seourl != 'hyderabad') { ?>
                                                    <li><a href="javascript:void(0)" onclick="setcity('<?php echo $val->id; ?>')"><i class="cityicon"><img src="<?php echo CDN_URL; ?>images/<?php echo $imgname.'.svg'; ?>" alt="<?php echo $val->list_value; ?>"></i><?php echo $val->list_value; ?></a></li>
                                                <?php } } ?>                                                
                                            </ul>
                                        </div>
                                    </li>
                                    <?php if ($loginCheck != '') {?>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">person</i></a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                        
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="usersettings">
                                                        <i class="material-icons">settings</i>My account</a>
                                                </li>
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="purchases">
                                                        <i class="material-icons">build</i>Log Request</a>
                                                </li>
                                                <li><a href="logout"><i class="material-icons">power_settings_new</i>Logout</a></li>
                                            </ul>
                                        </li>
                                    <?php } else {?>
                                        <li>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal3"><i class="material-icons">person</i></a>
                                        </li>
                                <?php }?>
                                <?php if ($loginCheck != '') {?>
                                        <li>
                                            <a class="carticn" href="javascript:void(0)">
                                                <i class="material-icons nw_theme_color">shopping_cart</i>
                                                <span id="new_count"><?php echo count($itemsIncart); ?></span>                                            </a>
                                        </li>

                                <?php }?>
                                </ul>
                            </div>


                                <!-- cart sidebar mene -->
                                <aside class="cartsidebar">
                                    <a href="javascript:void(0)" class="cartcross"><i class="material-icons">close</i></a>
                                    <div class="cartsys">
                                        <div class="titlecart">Your Shopping Cart</div>
                                        <ul id="add_new_product">
                                            <?php
                                           if (!empty($itemsIncart)) {
    $subTotal = 0;
    foreach ($itemsIncart as $item) {
        $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $item->couponID . "' LIMIT 1");
        $rr = $qu->result();

        $cartDiscountAmt = $cartDiscountAmt + ($item->discountAmount * $item->quantity);

        if ($rr[0]->maximumamount != 0) {
            if ($cartDiscountAmt > $rr[0]->maximumamount) {
                $cartAmt = $cartAmt + $cartDiscountAmt - $rr[0]->maximumamount;
                $cartDiscountAmt = $rr[0]->maximumamount;
            }
        }
        // $advance_rental = $advance_rental + ($item->price  * $item->quantity)-($item->discountAmount * $item->quantity);
        $product_shipping_cost = $product_shipping_cost + ($item->product_shipping_cost * $item->quantity);

        // $subTotal += ($item->product_shipping_cost * $item->quantity) + ($item->price * $item->quantity) - $item->discountAmount;
        $cartAmt = $cartAmt + (($item->price - $item->discountAmount + ($item->price * 0.01 * $item->product_tax_cost)) * $item->quantity);

        $rental_amount =  $rental_amount  + ((($item->product_tax_cost * 0.01 * $item->price ) + $item->price) * $item->quantity);

        $cartTAmt = ($cartAmt * 0.01 * 0);
        $grantAmt = $cartAmt + $product_shipping_cost + $cartTAmt;

        $cartprodImg = @explode(',', $item->image);?>
                                                    <li id="header-cart-row-<?php echo $item->id; ?>">
                                                        <div class="cart-thumb">
                                                            <img src="<?php echo CDN_URL; ?>images/product/<?php echo $cartprodImg[0]; ?>" alt="<?php echo $item->product_name; ?>" />
                                                        </div>
                                                        <div class="cart-desc">
                                                            <strong><?php echo $item->product_name; ?> </strong><br>
                                                                <?php if ($item->attr_name != '') {?>
                                                                    <small><?php echo $item->attr_type . ' / ' . $item->attr_name; ?></small>
                                                                <?php }?>
                                                            <span id="new_quantity1_<?php echo $item->product_id; ?>"><?php echo $item->quantity; ?></span><span><?php echo ' x ' . $this->data['currencySymbol'] . $item->price; ?></span><br>
                                                            <a href="cart"><i class="material-icons">mode_edit</i></a>
                                                    <a href="javascript:void(0)" onclick="javascript:delete_cart(<?php echo $item->id; ?>,<?php
echo $item->id ?>)"><i class="material-icons">delete</i></a>
                                                        </div>
                                                    </li>
                                                    <?php }  
                                                    // print_r($itemsIncart);
            if($itemsIncart[0]->discount_on_si != ''){
                $cartAmt  = $cartAmt - $itemsIncart[0]->discount_on_si;
                 $grantAmt = $cartAmt + $product_shipping_cost + $cartTAmt;  
            }
        ?>
                                                <li>
                                                    <div class="pull-left">ADVANCE RENTAL</div>
                                                    <div class="pull-right"><strong id="rental_amt"><i id="new_pricesymbl" class="fa fa-inr" aria-hidden="true"></i><?php echo  number_format($rental_amount, 2, '.', ''); ?></strong></div>
                                                    <br>
                                                    <div class="pull-left">REFUNDABLE DEPOSIT</div>
                                                    <div class="pull-right"><strong id="deposit_amt"><i id="new_pricesymbl" class="fa fa-inr" aria-hidden="true"></i><?php echo  number_format($product_shipping_cost, 2, '.', ''); ?></strong></div>
                                                    <br>
                                                
                                                    <div class="pull-left">DISCOUNT</div>
                                                    <div class="pull-right">
                                                        <strong id="disAmtVal_header"><i id="new_pricesymbl" class="fa fa-inr" aria-hidden="true"></i><?php echo  number_format($cartDiscountAmt, 2, '.', ''); ?></strong>
                                                    </div>
                                                    <br>
                                               
                                                    <div class="pull-left">SUBTOTAL</div>
                                                    <div class="pull-right"><strong id="CartGAmt"><i id="new_pricesymbl" class="fa fa-inr" aria-hidden="true"></i><?php echo  number_format($grantAmt, 2, '.', ''); ?></strong></div>

                                                </li>
<?php } else { ?>
                                                <div class="emptycartbar">
                                                    <img src="<?php echo CDN_URL; ?>images/empty-cart-icn.svg" alt="Empaty cart">
                                                    <h5>Your Cart is Empty</h5>
                                                    <p>Looks like you haven't chosen <br> any product yet</p>
                                                </div>
                                            <?php } ?>

                                        </ul>
                                        <div class="btncartdiv">
                                            <?php if (!empty($itemsIncart)) { ?>

                                                <a href="<?php echo base_url(); ?>cart" class="btn-check pull-left btngray"><i class="material-icons">shopping_cart</i><span>View Cart</span></a>
                                                <a href="<?php echo base_url(); ?>cart" class="btn-check pull-right"><span>Check Out</span> <i class="material-icons checkrot">reply</i></a>
<?php } ?>
                                        </div>
                                    </div>
                                </aside>
                                <!-- cart sidebar mene -->
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </header>

        <!--Page Js--> 
        <script src="js/site/header_new_searchbox.js"></script> 
        <script>
            function setcity(v) {
                $.ajax({
                    url: '<?php echo base_url(); ?>site/ajaxhandler/setcity',
                    data: 'v=' + v,
                    dataType: 'json',
                    type: 'POST',
                    success: function (r) {
                        if (r.ok == false) {
                            alert("Exactly what are you trying to do??");
                        }
                        if (r.ok == true) {
                            window.location.reload();
                        }
                    }
                });
            }
        </script>
        <script>

            var clicked = false;//Global Variable
            function ClickLogin()
            {
                clicked = true;
            }

            function onSignIn(googleUser) {
                if (clicked) {
                    var profile = googleUser.getBasicProfile();
                    var datasend = {};
                    datasend['email'] = profile.getEmail();
                    datasend['gender'] = "";
                    datasend['first_name'] = profile.getName();
                    datasend['last_name'] = "";
                    datasend['facebook_id'] = profile.getId();
                    datasend['name'] = profile.getName();
                    datasend['login_type'] = "google";
                    datasend['str'] = '';

                    if (profile.getEmail()) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>site/user/sociallogin",
                            type: 'POST',
                            dataType: 'json',
                            data: datasend,
                            success: function (x) {
                                location.reload();
                            }
                        });
                    }
                }

            }
            ;


        </script>
        <meta name="google-signin-client_id" content="1065795218106-s2m2k3s28ch432hn8gp669pjjn7esr7d.apps.googleusercontent.com">
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        

<?php include "popup_city.php"; ?>