<!--footer-->
<!-- Start of Zopim Live Chat Script -->
<!-- <script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3ux3p7fHKrotWQVCLJMIvvXxyTcFh33i";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> -->
<!-- End of Zopim Live Chat Script -->
<!--Start of Zoho Chat Script-->

<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
   <script src="<?php echo base_url()?>assets/js/check_login.js"></script>
<script type="text/javascript">
var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"5b8b964959404d9d889f511a19f2cc07f377ebba877a5b6e14e7b09993d5cc63cc06322062ad0d2d62c263e72e075dea", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
</script>
<!--End of Zoho Chat Script-->

 <!-- Izooto code -->
<!-- <script> window._izq = window._izq || []; window._izq.push(["init"]);</script>
<script src="//cdn.izooto.com/scripts/9ec169198ed9589be7ec3b9d9096e8a67edf2980.js"> </script> -->
 <!-- Izooto code -->

<footer role="contentinfo">
    <div class="container hidden-xs">
        <div class="row">
            <div class="m_bottom_30 footerlogosec">
                <figure class="m_bottom_15">
                    <img src="<?php echo base_url(); ?>images/logo-stick.png" alt="footer logo" />
                </figure>
                <div class="m_bottom_15">
                    <div class="color_light">
                      <p>Cityfurnish is revolutionizing the furniture industry by providing quality furniture and home appliances on easy monthly rental. With the immense focus on product quality and customer service, we strive to become most preferred name in furniture industry by customer's choice.</p>
                    </div>
                </div>
            </div>
            <div class="footerlink m_bottom_30">
                <h5 class="tt_uppercase  m_bottom_15 nw_scheme_color">Help</h5>
                <ul class="second_font vr_list_type_1 with_links">
                    <li><a href="<?php echo base_url();?>pages/contact-us">Contact us</a></li>
                    <li><a href="<?php echo base_url();?>pages/how-it-works">How It Works?</a>  </li>
                    <li><a href="<?php echo base_url();?>pages/faq">FAQs</a></li>
                    <li><a href="<?php echo base_url();?>reviews-testimonials/all">Customer Reviews</a></li>
                    <li><a href="customerpayment" target="_blank">Customer Payment</a></li>
                </ul>
            </div>
            <div class="footerlink m_bottom_30">
                <h5 class="tt_uppercase  m_bottom_15 nw_scheme_color">Information</h5>
                <ul class="second_font vr_list_type_1 with_links">
                    <li><a href="<?php echo base_url();?>blog/" target="_blank">Blog</a></li>
                    <li><a target="_blank" href="http://vior.in" rel="nofollow" >Clearance Sale</a></li>
                    <li><a href="<?php echo base_url();?>pages/offers">Offers</a></li>
                    <li><a href="<?php echo base_url();?>pages/careers">We are hiring</a></li>
                    <li><a href="<?php echo base_url();?>pages/friends-and-partners">Friends & Partners</a></li>
                    
                </ul>
            </div>
            <div class="footerlink last m_bottom_30">
                <h5 class="tt_uppercase  m_bottom_15 nw_scheme_color">POLICIES</h5>
                <ul class="second_font vr_list_type_1 with_links">
                    <li><a href="<?php echo base_url();?>pages/terms-of-use">Terms of use</a></li>
                    <li><a href="<?php echo base_url();?>pages/privacy-policy">Privacy policy</a></li>
                    <li><a href="<?php echo base_url();?>pages/refer-a-friend">Referral Terms of use</a></li>
                    <li><a href="<?php echo base_url();?>pages/rentalagreement">Sample Rental Agreement</a></li>
                </ul>
            </div>
            <div class="new_scroll">
                <ul id="results" /> 
            </div>
            <div class="quickcontactcol m_bottom_30">
                <div class="footerquickcontact">
                    <h5 class="m_bottom_15 nw_scheme_color">QUICK CONTACT</h5>
                    <ul class="contactquick">
                        <li>
                            <a href="mailto:hello@cityfurnish.com">
                                <i class="material-icons">mail_outline</i>
                                <span>hello@cityfurnish.com</span>
                            </a>
                        </li>
                        <li>
                            <a href="tel:8010845000">
                                <i class="material-icons">phone</i>
                                <span>8010845000</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="socialicon">
                        <li>
                            <a href="https://www.facebook.com/cityFurnishRental" rel="nofollow" target="_blank">
                                <!--<i class="fa fa-facebook fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/CityFurnish" rel="nofollow" target="_blank" class="twitter">
                                <!--<i class="fa fa-twitter fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/+cityfurnish" rel="nofollow" target="_blank" class="google">
                                <!--<i class="fa fa-google-plus fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://in.pinterest.com/cityfurnish/" rel="nofollow" target="_blank" class="pintrest">
                                <!--<i class="fa fa-pinterest-p fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" rel="nofollow" target="_blank" class="linkedin">
                                <!--<i class="fa fa-linkedin fs_large"></i>-->
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-12 col-sm-12 col-md-12">
                <hr class="divider_grey m_bottom_25">
            </div>
            <article class="col-lg-12 col-md-12 col-sm-12 text-center">
                <h6 class="nw_fontw_dark tt_uppercase m_bottom_15">Quality Furniture and Branded Appliances on Rent</h6>
                <article class="qulitysec">
                   <?php  if($meta_keyword !=''){ ?>
                   <p><?php echo $meta_keyword;  ?></p>
                   <?php } else { ?>

                    <p>Cityfurnish offers a wide range of stylish, elegant and modern
                        <b>furniture on rent online</b>. Our comprehensive range of
                        <a href="shopby/furniture-rental">home furniture</a> includes bedroom furniture, living room furniture, dining room furniture and study room furniture. On Cityfurnish.com you can browse through range of products, choose the one you like and place the order online.
                        Our well trained team will deliver and install your chosen products within 72 hours, is it not really great?</p>
                    <p>Good piece of furniture is not just a matter of convenience but it reflects your taste and status as well. All our products are made keeping these simple facts in mind. Our furniture designs are finalised after putting a lot of thought
                        process behind them. We tend to provide the furniture which is not only looks elegant but also provides comfort and value for money. Our all furniture products are made in our own manufacturing facility using highest quality wood
                        and hardware.</p>
                    <p> As an
                        <b>online furniture rental</b> company, we understand the importance of good service as well and that's why we strive for excellence in our delivery and post delivery services. We keep our process simple to cut down overheads and
                        keep our customers informed during every stage of the process.</p>
                    <p> Though initially we started as a
                        <b>furniture rental company</b> but later we realised that our customers are also asking for
                        <a href="shopby/rent-electronics">home appliances on rent</a>. So, now our packages include not only furniture but also appliances to make your house a home. Just choose the right package and leave the rest to us. So, what are you waiting for, give it a try and
                        we promise that after using our services, you would like to recommend us to your friends as well.</p>
                        <?php } ?>
                    <hr class="divider_grey margin-top-25 m_bottom_30">
                    <div class="row">
                        <div class="col-sm-6 col-lg-6 col-xs-12  m_bottom_25 packageslink">
                            <h6 class="nw_fontw_dark tt_uppercase m_bottom_15">Furniture Rental Packages </h6>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url()?>shopby/living-room-furniture">Living Room</a>
                                </li>
                                <li>
                                     <a href="<?php echo base_url()?>shopby/bedroom-furniture">BedRoom</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>shopby/study-room-furniture-rental">Study Room</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>shopby/storage">Storage</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>shopby/rental-packages">Full Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>shopby/rent-electronics">Home Appliances</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>shopby/office-furniture-rental">Office Furniture</a>
                                </li>
                                 <li>
                                    <a href="<?php echo base_url()?>shopby/fitness-equipments-on-rent">Fitness Equipments</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-lg-6 col-xs-12  m_bottom_25 packageslink">
                            <h6 class="nw_fontw_dark tt_uppercase m_bottom_15">MOST POPULAR SEARCH TERMS</h6>
                            <ul>
                                <a href="<?php echo base_url();?>shopby/furniture-rental">Furniture on rent in Noida</a>,
                                <a href="<?php echo base_url();?>shopby/furniture-rental">Furniture on rent in Bangalore</a>,
                                <a href="<?php echo base_url();?>shopby/furniture-rental">Furniture on rent in Delhi</a>,
                                <a href="<?php echo base_url();?>shopby/furniture-rental">Furniture on rent in Gurgaon</a>,
                                <a href="<?php echo base_url();?>shopby/furniture-rental/bedroom-furniture">Bed on rent</a>,
                                <a href="<?php echo base_url();?>things/3799/TV-32-inches-LED">TV on rent</a>
                                <a href="<?php echo base_url();?>shopby/appliances-rental">Home appliances on rent in Bangalore</a>,
                                <a href="<?php echo base_url();?>shopby/appliances-rental">Rent home appliances in Bangalore</a>
                            </ul>
                        </div>
                    </div>
                    <hr class="divider_grey  m_bottom_30">
                    <div class="row">
                        <div class="col-lg-12 citylinks">
                            <h6 class="nw_fontw_dark tt_uppercase m_bottom_15">Cities in which you can Rent Furniture</h6>
                            <ul>
                                <li><a href="javascript:void(0)">Bengaluru</a></li>
                                <li><a href="javascript:void(0)">Pune</a></li>
                                <li><a href="javascript:void(0)">Gurugram</a></li>
                                <li><a href="javascript:void(0)">Mumbai</a></li>
                                <li><a href="javascript:void(0)">Delhi</a></li>
                                <li><a href="javascript:void(0)">Noida</a></li>
                                 <li><a href="javascript:void(0)">Ghaziabad</a></li>
                                <li><a href="javascript:void(0)">Faridabad</a></li>
                                <li><a href="javascript:void(0)">Navi Mumbai</a></li>
                                <li><a href="javascript:void(0)">Thane</a></li>
                            </ul>
                        </div>
                    </div>
                </article>
            </article>
        </div>
    </div>
    <?php
    $this->load->view('site/templates/script_files_new');
    ?>
</footer>
<section class="copyright" id="mobile_footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6 hidden-xs">
                <p>&copy; Copyright <?php echo date('Y'); ?>
                    <strong class="nw_theme_color">Cityfurnish</strong>. All Rights Reserved.</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 headertopcontact visible-xs">
                <a href="mailto:hello@cityfurnish.com">
                    <i class="material-icons">mail_outline</i>
                    <span>hello@cityfurnish.com</span>
                </a>
                <a href="tel:8010845000">
                    <i class="material-icons">phone</i>
                    <span>8010845000</span>
                </a>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 cityright hidden-xs">
                <p>Soon coming to: Hyderabad and Chennai</p>
            </div>
        </div>
    </div>
</section>

<button class="back_to_top scrollToTop">
    <i class="material-icons d_inline_m">keyboard_arrow_up</i>
</button>
<!-- serarch popup model -->
<script type="text/javascript">
$('.searchicn').click(function() {
  $('#search_value').val('');
  $('#finalResult').empty();

});

</script>

<section class="modal fade-scale searchmodel" id="myModal" tabindex="-1" role="dialog" style="padding-right:0px;">
    <section class="modal-dialog" role="document">
        <section class="modal-content">
            <div class="modal-header">
                <div class="container relative">
                    <a href="javascript:void(0)"  class="btncross" data-toggle="modal" data-target="#myModal">
                        <span aria-hidden="true"  class="material-icons">close</span>
                    </a>
                        <div class="col-xs-10 col-lg-10 col-md-10 col-sm-10">
                            <i class="material-icons searchicnab nw_theme_color">search</i>
                            <input type="text" placeholder="Search" id="search_value" class="form-control" 
                             onkeyup="search_product()"/ >                            
                        </div>

                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row sproductrow new_search_data">
                    </div>
                    <div id="no_found" style="display:none;"> 
                        <figure>
                            <strong><h4 id="new_text"></h4></strong>
                        </figure>
                    </div>
                    <div class="row catagoryrow">
                        <div class="col-sm-12 col-md-12 col-lg-12 m_top_10">
                            <h6>Categories</h6>
                            <hr class="divider_grey margin-top-20 m_bottom_50">
                        </div>

                        <div class="new_cat">      
                        </div>      
                    </div>
                    <div id="cat_no" style="display:none;"> 
                        <figure>
                            <strong><h4 id="not_found_cat"></h4></strong>
                        </figure>
                    </div>
                </div>
            </div>
        </section>
    </section>
</section>

<!-- login popup -->
<div id="myModal3" class="common-popup">
    <div class="common-content">
        <div class="common-content-box">
            <aside class="popup-left">
                <span  id="sign_up_span" style="display: none;">Sign up with <a href="javascript:void(0)">Linkedin</a> <br/> or <a href="javascript:void(0)"  onclick= "fb_login()">facebook</a> for faster <br/> verification and delivery.</span>
            </aside>
            <aside class="popup-right">
               <!--  <a href="javascript:void(0)" class="common-popup-close" data-toggle="modal" data-target="#myModal3"><i aria-hidden="true" class="material-icons">clear</i></a> -->
                 <a href="javascript:void(0)" class="btncross" data-toggle="modal" data-target="#myModal3">
                        <i aria-hidden="true" class="material-icons">close</i>
                 </a>
                <div class="login-block popup-block">
                    <ul class="nav nav-tab sing_up_in">
                        <li class="active"><a href="#tab2" data-toggle="tab"  class="tabsignin">Sign In</a></li>
                        <li><a href="#tab1" data-toggle="tab"  class="tabsignup">Sign Up</a></li>
                       
                    </ul>
                    <div class="tab-content">
                        <div id="tab1" class="tab-pane signup-section">
                            <div class="">
                                <h6>Recommended</h6>
                                <p class="visible-xs signupcontent">Sign up with Linkedin  or facebook for faster  verification and delivery.</p>
                                <div class="popup-link">
                                    <a href="javascript:void(0)" class="linkdin-link social-link" data-onload="true" 
                                    data-onsuccess="onLinkedInLoad" id="linkdin">Linkedin</a>
                                    <a onclick="fb_login()" class="facebook-link social-link">Facebook</a>
                                </div>
                                <label class="line line2"><span>or</span></label>
                                <div class="form-input">
                                    <form name="sign-up" id="sign-up">
                                        <div class="input-box">
                                            <label>What's your name?</label>
                                            <input type="text" class="fullname" id="user_name" placeholder="Your Name" name="full_name">
                                        </div>
                                        <div class="input-box">
                                            <label>Enter Email</label>
                                            <input type="email" class="email" id="signup_email" placeholder="Your Email" name="signup_email">
                                             <span id="error_msg" style="color: red;" style="display: none; height: 13px;width: 320px; "></span>
                                        </div>
                                        <div class="input-box">
                                            <label>Create a Password</label>
                                            <input type="password" class="password" id="signup_password" placeholder="***********" name="signup_password">
                                        </div>
                                        <input type='hidden' name='api_id' id="api_id"  value='<?php echo $social_login_session_array['social_login_unique_id'];?>' />
                                        <input type='hidden' name='thumbnail' id='thumbnail' value='<?php echo $social_login_session_array['social_image_name'];?>' />
                                        <input type='hidden' name='loginUserType' id='loginUserType' value='<?php if($social_login_session_array['loginUserType'] != '') echo $social_login_session_array['loginUserType']; else echo "normal";?>' />
                                        <span>By signing up, you agree to Cityfurnish <a href="<?php echo base_url()?>pages/terms-of-use">Terms and Conditions</a> & <a href="<?php echo base_url()?>pages/privacy-policy">Privacy Policy</a></span>
                                        <div class="input-box signup-input">
                                            <button   class="form_button"  id="signupsubmit" name="signupsubmit"  onclick="return check_validation()">Sign Up</button>
                                            <div class="ordivider"><span>or</span></div>
                                            <a href="javascript:void(0);" 
                                            onclick= "ClickLogin()" class="g-signin2 form_button" 
                                           data-onsuccess="onSignIn">Google</a>
                                        </div>
                                         
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane signin-section fade in active">
                            <div>
                                <h6></h6>
                                <div class="popup-link popsignin">
                                     <a href="javascript:void(0)" class="linkdin-link social-link" data-onload="true" 
                                    data-onsuccess="onLinkedInLoad" id="linkdinlog">Linkedin</a>
                                     <a onclick= "fb_login()" id="fb_login" class="facebook-link social-link">Facebook</a>                                  
                                    <a href="javascript:void(0);" 
                                        onclick= "ClickLogin()"
                                         data-onload="true" class="g-signin2" data-onsuccess="onSignIn">Google</a>
                                </div>
                                <label class="line line2"><span>or</span></label>
                                <div class="form-input">
                                    <form method="post"  name="form-login" id="form-login">
                                        <div class="input-box">
                                            <label>Enter Email</label>
                                            <input type="email" id="login_email" placeholder="Your Email" name="email">
                                        </div>
                                        <div class="input-box">
                                            <label>Enter Password</label>
                                            <input type="password" id="login_password" placeholder="***********" name="password">
                                        </div>
                                        <input type="hidden" value="<?php echo base_url();?>" id="base_url">
                                        <a href="javascript:void" class="forgot-pass">Forgot Password?</a>
                                        <div class="input-box signup-input">
                                            <!-- <input type="submit" id="signinsubmit" name="signinsubmit" value="Sign In"> -->
                                            <button class="form_button" id="signinsubmit" name="signinsubmit"
                                                onclick="login();">Sign In</button>
                                            <div class="ordivider hidden-xs"><span>or</span></div>
                                                    <a href="javascript:void(0);" 
                                                    onclick= "ClickLogin()"
                                                    data-onload="true" class="g-signin2 hidden-xs" data-onsuccess="onSignIn">Google
                                                    </a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>        
                </div> 
                 <div class="success-block popup-block display_none" id="success_registraion">
                            <div class="inner-block text-center">
                                <i class="material-icons">&#xE876;</i>
                                <h4>Register Success</h4>
                                <p>Your account has been created. We have sent you account activation link on your registered email address.</p>
                            </div>
                        </div>  
                <div class="forgot-pass-block popup-block" >

                    <div id="overlay" style="display: none;">
                         <!-- <img src="images/loader.gif" alt="Loading" /> -->
                    </div>

                    <h3 id="h3_text"><a href="javascript:void" class="backtologin"><i class="material-icons">keyboard_backspace</i></a> Forgot Password</h3>
                    <div class="inner-block text-center" id="div_for">
                        <i class="material-icons">lock_outline</i>
                        <p>Please enter your registered email address. We will provide you password reset instruction.</p>
                        <div class="form-input">
                             <form  id="forgot-pass" class="frm clearfix"><input type='hidden' />
                                <div class="input-box">
                                    <label>Enter Email</label>
                                    <input type="email" id="forgotemail" placeholder="Jhondoe@cityfurnish.com" name="forgotemail">
                                </div>
                                <div class="input-box signup-input">
                                    <button id="forgotsubmit" onclick="forgot_password()" name="forgotsubmit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>                   

                    <div class="reset-block popup-block display_none" id="forgot_password">
                        <div class="inner-block text-center">
                            <i class="material-icons">&#xE897;</i>
                            <p>We have sent password reset instruction on your registered email address.</p>
                        </div>
                    </div> 
                    <div class="reset-block popup-block display_none" id="email_error">
                        <div class="inner-block text-center">
                            <i class="material-icons">&#xE897;</i>
                            <p class="alert alert-warning">Your email id not matched in our records</p>
                        </div>
                    </div>     
                </div>                
            </aside>
            
        </div>
    </div>
</div>  
<style type="text/css">
    #overlay{
      position:fixed;
      top:0px;
      right:0px;
      width:100%;
      height:100%;
      background-color:#666;
      background-image:url('images/loader.gif');
      background-repeat:no-repeat;
      background-position:center;
      z-index:10000000;
      opacity: 0.6;
      filter: alpha(opacity=40); /* For IE8 and earlier */
    }
</style>                
<script type="text/javascript">
        var pro_id = '';
        var page = '';
        var price = 0;
        if(jQuery('.product_item').length > 0 && jQuery('.layerslider').length == 0){
            page = 'category';
        }
        else if(jQuery('.product_preview').length > 0){
            var pro_id = jQuery('#product_id').val();
            page = 'product';
            price = jQuery('#SalePrice').text().trim();
        }
        else if(window.location.href.indexOf('/cart') != -1){
            var pro_id = new Array();
                        jQuery('.table-cart tr.first a img').each(function(){
                                var temp_id = jQuery(this).parent().attr('href').split('/')[1];
                                pro_id.push(temp_id);
                        });
            page = 'cart';
            price = jQuery('#CartGAmt').text().trim();
        }
        else if(window.location.pathname == '/'){
            page = 'home';
        }
        else{
            page = 'other';
        }
        var google_tag_params = {
                              dynx_itemid: pro_id,
                              dynx_pagetype: page,
                              dynx_totalvalue: parseFloat(price)
                              };
</script>

<script type="text/javascript" src="//platform.linkedin.com/in.js">
            api_key: 81tyo00okgh7jg
            authorize: true
            onLoad: onLinkedInLoad
            scope: r_basicprofile r_emailaddress
</script>

<script type="text/javascript">
jQuery(document).ready(function(){


    jQuery('#myModal').on('shown.bs.modal', function() {
        jQuery("#search_value").focus();
    })


    jQuery("#linkdinlog").click(function(){
        IN.UI.Authorize().place(); 
        IN.Event.on(IN, "auth", one_click);
    });
});
</script>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#linkdin").click(function(){
        IN.UI.Authorize().place(); 
        IN.Event.on(IN, "auth", one_click);
    });
});
</script>

<script type="text/javascript">
var clicked=false;//Global Variable
    function one_click(){
        clicked=true;
    }
    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        IN.Event.on(IN, "auth", getProfileData);          
        //return false;  
    }
    function logout(){
        IN.User.logout(onLogout);
    }
    function onLogout(){
        console.log('Logout successfully');
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
        IN.API.Profile("me").fields("first-name", "last-name", "email-address","picture-url").result(function (data) {
            if (clicked) {  
                var userdata = data.values[0];
                var datasend = {};             
                datasend['email'] = userdata.emailAddress;           
                datasend['first_name'] = userdata.firstName;         
                datasend['last_name'] = userdata.lastName;           
                datasend['facebook_id'] = userdata.key; 
                datasend['login_type'] = "linkdin";
                if(userdata){
                    $.ajax({
                        url: "<?php echo base_url()?>site/user/sociallogin",
                        type: 'POST',
                        dataType: 'json',
                        data:  datasend,
                        success: function(x){
                          //  window.location.href = x.url;
                          location.reload();
                        },
                    });
                }
            }
        });
    }
</script>
<script>
window.fbAsyncInit = function() {
    FB.init({
         appId      : '183441015137603',
        //appId : '324383858071764',
        //appId : '145747126093085',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.12'
    });
    FB.AppEvents.logPageView();   
};
(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
   
function fb_login(str){ 
    FB.login(function(response) {
        if (response.authResponse) {
            console.log('Welcome!  Fetching your information.... ');
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID
            FB.api('/me?fields=id,name,email,gender,first_name,last_name', function(response) {
                var datasend = {};
                datasend['id']  = response.id;             
                datasend['email'] = response.email;   
                datasend['gender'] = response.gender;         
                datasend['first_name'] = response.first_name;         
                datasend['last_name'] = response.last_name;           
                datasend['facebook_id'] = response.id;
                datasend['name'] = response.name;  
                datasend['login_type'] = "facebook";               
                datasend['str'] = str;              
                if(response.id){
                    $.ajax({
                        url :  "<?php echo base_url()?>site/user/sociallogin",
                        type: 'POST',
                        dataType: 'json',
                        data: datasend,
                        success: function(x){
                           // console.log(x);
                            //window.location.href = x.url;
                               location.reload();
                        }
                    });
                }
            });

        } else {
            //user hit cancel button
            console.log('User cancelled login or did not fully authorize.');

        }
    }, {
        scope: 'email, public_profile, user_friends'
    });
} 
 
</script>    
<script src="<?php echo base_url(); ?>js/easing.jquery.js"></script> 
<script src="<?php echo base_url(); ?>js/readmore.js"></script> 
<script src="<?php echo base_url(); ?>js/slick.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.sliderTabs.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap-slider.js"></script> 
<script src="<?php echo base_url(); ?>js/custom-function.js"></script>    