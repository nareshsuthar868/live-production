<?php if (!$this->session->userdata("prcity")) { ?>
<div id="my-Modal" class="modal fade citymodel" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Select City</h5>
            </div>
            <div class="modal-body">
                <div class="modalcontent">
                    <?php foreach($cityList->result() as $val) { ?>
                    <?php
                        if($val->list_value_seourl == 'delhincr') {
                            $imgname = 'delhi';
                        } else if($val->list_value_seourl == 'bangalore') {
                            $imgname = 'banglore';
                        } else {
                            $imgname = $val->list_value_seourl;
                        }
                    if($val->list_value_seourl != 'hyderabad') {
                     ?>
                    <div class="col-xs-3">
                        <a href="javascript:void(0)" onclick="setcity('<?php echo $val->id ?>')" data-toggle="modal" data-target="#my-Modal">
                            <div class="lgcityicn">
                                <img src="<?php echo CDN_URL; ?>images/lg-city-<?php echo $imgname; ?>.png" alt="<?php echo $val->list_value; ?>" />
                            </div>
                            <span><?php echo $val->list_value; ?></span>
                        </a>
                    </div>
                    <?php } } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>