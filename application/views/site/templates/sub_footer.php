				<div class="section_offset bg_grey_light_2 m_bottom_38" style="text-align:center;">
					<div class="container m_bottom_13 m_top_10">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 m_xs_bottom_30">
								<h2 class="scheme_color tt_uppercase m_bottom_20" style="text-align:center;">Quality Furniture and Branded Appliances on Rent</h2>
								<hr class="divider m_bottom_30">
								<p>Cityfurnish offers a wide range of stylish, elegant and modern <b>furniture on rent online</b>. Our comprehensive range of <a href="shopby/furniture-rental">home furniture</a> includes bedroom furniture, living room furniture, dining room furniture and study room furniture. On Cityfurnish.com you can browse through range of products, choose the one you like and place the order online. Our well trained team will deliver and install your chosen products within 72 hours, is it not really great?<br>

Good piece of furniture is not just a matter of convenience but it reflects your taste and status as well. All our products are made keeping these simple facts in mind. Our furniture designs are finalised after putting a lot of thought process behind them. We tend to provide the furniture which is not only looks elegant but also provides comfort and value for money. Our all furniture products are made in our own manufacturing facility using highest quality wood and hardware.<br>

As an <b>online furniture rental</b> company, we understand the importance of good service as well and that’s why we strive for excellence in our delivery and post delivery services. We keep our process simple to cut down overheads and keep our customers informed during every stage of the process.<br>

Though initially we started as a <b>furniture rental company</b> but later we realised that our customers are also asking for <a href="shopby/rent-electronics">home appliances on rent</a>. So, now our packages include not only furniture but also appliances to make your house a home. Just choose the right package and leave the rest to us. So, what are you waiting for, give it a try and we promise that after using our services, you would like to recommend us to your friends as well.</p>
							</div>
						</div>
					</div>
				</div>
