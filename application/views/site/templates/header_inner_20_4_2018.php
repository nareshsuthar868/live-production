<!doctype html>
<html lang="en">
    <head>

        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-PF4G2HJ');</script>
        <!-- End Google Tag Manager -->

        <?php if ($this->config->item('google_verification')) {
            echo stripslashes($this->config->item('google_verification'));
        }
        if ($meta_title != '' and $meta_title != $title) {
            ?>
            <title><?php echo $meta_title; ?></title>
        <?php } elseif ($heading != '') { ?>
            <title><?php echo $heading; ?></title>
<?php } else { ?>
            <title><?php echo $title; ?></title>
<?php } ?>
        <meta name="Title" content="<?php echo $meta_title; ?>" />
        <meta charset="utf-8">

        <!--add responsive layout support-->
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!--meta info-->
        <meta name="author" content="">
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <meta property="og:image" content="<?php echo base_url(); ?>images/product/<?php echo $this->data['meta_image']; ?>" />
        <base href="<?php echo base_url(); ?>" />
        <!--include favicon-->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>images/logo/<?php echo $fevicon; ?>">

        <?php
        if (is_file('google-login-mats/index.php')) {
            require_once 'google-login-mats/index.php';
        }
        ?>

        <!--<link href='css/Roboto_slab.css' rel='stylesheet' type='text/css'>-->
        <!--stylesheet include-->
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">




<!--<link rel="stylesheet" type="text/css" media="all" href="<?php //echo base_url();  ?>plugins/owl-carousel/assets/owl.carousel.min.css">-->
<!--<link rel="stylesheet" type="text/css" media="all" href="<?php //echo base_url();  ?>plugins/fancybox/jquery.fancybox.min.css">-->
<!--<link rel="stylesheet" type="text/css" media="all" href="<?php //echo base_url();  ?>plugins/jackbox/css/jackbox.min.css">-->
<!--<link rel="stylesheet" type="text/css" media="all" href="<?php //echo base_url();  ?>css/animate.min.css">-->
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>css/bootstrap.min.css">
        <!--<link rel="stylesheet" type="text/css" media="all" href="css/bootstrap_new.min.css">-->
        <!--<link rel="stylesheet" type="text/css" media="all" href="<?php //echo base_url();  ?>css/slick.css">-->
        <!--<link rel="stylesheet" type="text/css" media="all" href="<?php //echo base_url();  ?>css/sliderTabs.css">-->
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>css/style.css">


        <!-- Sweet Alert -->
        <script src="<?php echo base_url() ?>assets/bootstrap-sweetalert/dist/sweetalert.js"></script>
        <link href="<?php echo base_url() ?>assets/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

         <script src="<?php echo base_url() ?>assets/bootstrap-sweetalert/dist/swalExtend.js"></script>
        <link href="<?php echo base_url() ?>assets/bootstrap-sweetalert/dist/swalExtend.css" rel="stylesheet" type="text/css">






        <!--[if lte IE 10]>
                                <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
                                <link rel="stylesheet" type="text/css" media="screen" href="plugins/jackbox/css/jackbox-ie9.css">
                        <![endif]-->
        <!--head libs-->
        <!--[if lte IE 8]>
                                <style>
                                        #preloader{display:none !important;}
                                </style>
                        <![endif]-->
        <!-- toaster  -->
        <script src="<?php echo base_url() ?>assets/toastr-master/toastr.js"></script>
        <!-- Toastr Css -->
        <link href="<?php echo base_url() ?>assets/toastr-master/toastr.min.css" rel="stylesheet" type="text/css">

        <script src="<?php echo base_url(); ?>js/jquery-2.1.1.min.js"></script>
        <script src="<?php echo base_url(); ?>js/modernizr.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <a href="https://plus.google.com/+Cityfurnish" rel="publisher"></a>
    <a href="https://plus.google.com/+Cityfurnish?rel=author"></a>
    <!-- start Mixpanel<script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
                    0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
                    for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
                    mixpanel.init("54b221e752447d804f0b1a444eba1c8e");</script>end Mixpanel -->

    <!-- Facebook Pixel Code
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '202224353551343'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    //fbq('track', 'Purchase', {value: '0.00', currency:'INR'});
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=202224353551343&ev=PageView&noscript=1"
    /></noscript>
    End Facebook Pixel Code -->

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)}, i[r].l = 1 * new Date();a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-71251009-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
    <!-- Google Tag Manager (noscript)-->
    <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PF4G2HJ"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) --> 

    <!--layout-->
    <div class="wide_layout db_centered bg_white">
        <!--[if (lt IE 9) | IE 9]>
                                        <div class="bg_red" style="padding:5px 0 12px;">
                                        <div class="container" style="width:1170px;"><div class="row wrapper"><div class="clearfix color_white" style="padding:9px 0 0;float:left;width:80%;"><i class="fa fa-exclamation-triangle f_left m_right_10" style="font-size:25px;"></i><b>Attention! This page may not display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer browsing experience.</b></div><div class="t_align_r" style="float:left;width:20%;"><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode" class="button_type_1 d_block f_right lbrown tr_all second_font fs_medium" target="_blank" style="margin-top:6px;">Update Now!</a></div></div></div></div>
                                <![endif]-->
        <header class="stickyinner">
            <div class="header_top_part">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 t_xs_align_c"> 
                            <p> <span><img src="<?php echo CDN_URL; ?>images/top-percent-icn.svg" alt="percenticn"></span> <span>GET GIFT VOUCHERS WORTH RS 2500 - WITH EVERY ORDER <a class="sc_hover d_inline_b tt_lowercase" style="color:white;" href="<?php echo base_url(); ?>pages/offers"> <u> KNOW MORE.. </u></a></span> </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header_bottom_part w_inherit">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="headertop">
                                <div class="mobilemenu"> 
                                    <a href="javascript:void(0)" class="togglemenu"> <i class="material-icons">menu</i> </a> 
                                </div>
                                <div class="logo clearfix t_sm_align_c"> 
                                    <a href="<?php echo base_url(); ?>" class="d_inline_b hide_on_mobile"> 
                                        <img src="<?php echo CDN_URL; ?>images/logo-white.png" alt="Logo Here" class="logounstick"> 
                                        <img src="<?php echo CDN_URL; ?>images/logo-stick.png" alt="Logo Here" class="logostick"> 
                                    </a> 
                                    <!-- mobile logo -->
                                    <a href="<?php echo base_url(); ?>" class="d_inline_b show_on_mobile"> 
                                        <img src="<?php echo CDN_URL; ?>images/mobile-logo-white.png" alt="Logo Here" class="innerlogostick"> 
                                        <img src="<?php echo CDN_URL; ?>images/mobile-logo-stick.png" alt="Logo Here" class="innerlogounstick"> 
                                    </a> 
                                </div>
                                <div class="mobileoverlay"></div>
                                <ul class="hr_list headeropt">
                                                    <li>
                                                        <a href="javascript:void(0)" class="searchicn" data-toggle="modal" data-target="#myModal"><i class="material-icons" id="new_click">search</i></a>
                                                    </li>

                                    <li> 
                                        <!-- new dropdown design -->
                                        <div class="btn-group"> <i class="material-icons">room</i>
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
                                                <?php
                                                if ($_SESSION['prcity'] && $_SESSION['prcity'] == '45') {
                                                    echo "<span>Delhi NCR</span>";
                                                } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '46') {
                                                    echo "<span>Bangalore</span>";
                                                } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '47') {
                                                    echo "<span>Pune</span>";
                                                } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '48') {
                                                    echo "<span>Mumbai</span>";
                                                } else {
                                                    echo "<span>Select city</span>";
                                                }
                                                ?>
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)" onclick="setcity(45)"><i class="cityicon"><img src="<?php echo CDN_URL; ?>/images/delhi.svg" alt="delhi"></i>Delhi NCR</a></li>
                                                <li><a href="javascript:void(0)" onclick="setcity(46)"><i class="cityicon"><img src="<?php echo CDN_URL; ?>/images/banglore.svg" alt="Bangalore"></i>Bangalore</a></li>
                                                <li><a href="javascript:void(0)" onclick="setcity(47)"><i class="cityicon"><img src="<?php echo CDN_URL; ?>/images/pune.svg" alt="Pune"></i>Pune</a></li>
                                                <li><a href="javascript:void(0)" onclick="setcity(48)"><i class="cityicon"><img src="<?php echo CDN_URL; ?>/images/mumbai.svg" alt="Mumbai"></i>Mumbai</a></li>
                                            </ul>
                                        </div>
                                    </li>
<?php if ($loginCheck != '') { ?>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">person</i></a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                           <!--  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo 'user/' . $userDetails->row()->user_name; ?>">
                                              <i class="material-icons">favorite</i>Wishlist</a>
                                              </li> -->

                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="usersettings">
                                                        <i class="material-icons">settings</i>My account</a>
                                                </li>
                                                <li><a href="logout"><i class="material-icons">power_settings_new</i>Logout</a></li>
                                            </ul>
                                        </li>
                                    <?php } else { ?>
                                        <li> 
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal3"><i class="material-icons">person</i></a> 
                                        </li>
<?php } ?>
<?php if ($loginCheck != '') { ?>
                                        <li>
                                            <a class="carticn" href="javascript:void(0)"> 
                                                <i class="material-icons nw_theme_color">shopping_cart</i><span id="new_count"><?php echo count($itemsIncart); ?></span>


                                            </a> 
                                        </li>

<?php } ?>
                                </ul> 
                                <nav role="navigation" class="navigation"> 
                                               <!--<a href="javascript:void(0)" class="cross-menu"><i class="material-icons">close</i></a>-->
                                    <div class="menuheader  visible-xs">
                                        <div class="mobilemenu"> 
                                            <a href="javascript:void(0)" class="togglemenu"> <i class="material-icons">menu</i> </a> 
                                        </div>
                                        <div class="logo"> <a href="<?php echo base_url(); ?>" class="d_inline_b show_on_mobile"> <img src="<?php echo CDN_URL; ?>images/mobile-logo-white.png" alt="Logo Here"> </a> </div>
                                        <div class="header-right-top" >
                                            <ul class="hr_list shop_list f_right second_font fs_medium f_sm_none d_sm_inline_b t_sm_align_l scheme_color_font">
                                                <li><a  href="mailto:hello@cityfurnish.com"> <i class="material-icons">mail_outline</i></a> </li>
                                                <li><a  href="tel:8010845000"><i class="material-icons">phone</i></a> 
                                            </ul>
                                        </div> 
                                    </div>
                                    <ul class="main_menu hr_list">
                                        <li class="dropdown visible-xs">
                                            <div class="btn-group"><i class="citytoggle"><img src="<?php echo CDN_URL; ?>images/location-sm.svg" alt="Cabinet"></i><a class="dropdown-toggle active" data-toggle="dropdown" href="javascript:void(0);">
                                                    <?php
                                                    if ($_SESSION['prcity'] && $_SESSION['prcity'] == '45') {
                                                        echo "<span>Delhi NCR</span>";
                                                    } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '46') {
                                                        echo "<span>Bangalore</span>";
                                                    } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '47') {
                                                        echo "<span>Pune</span>";
                                                    } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '48') {
                                                        echo "<span>Mumbai</span>";
                                                    } else {
                                                        echo "<span>City</span>";
                                                    }
                                                    ?>
                                                    <span class="caret"> <i class="material-icons adown">keyboard_arrow_down</i> <i class="material-icons aup">keyboard_arrow_up</i> </span> </a>

                                                <ul class="dropdown-menu">
                                                    <li><a href="javascript:void(0)" onclick="setcity(45)"><i><img src="<?php echo CDN_URL; ?>images/delhi.svg" alt="Delhi"></i>Delhi NCR</a></li>
                                                    <li><a href="javascript:void(0)" onclick="setcity(46)"><i><img src="<?php echo CDN_URL; ?>images/banglore.svg" alt="Bangalore"></i>Bangalore</a></li>
                                                    <li><a href="javascript:void(0)" onclick="setcity(47)"><i><img src="<?php echo CDN_URL; ?>images/pune.svg" alt="Pune"></i>Pune</a></li>
                                                    <li><a href="javascript:void(0)" onclick="setcity(48)"><i><img src="<?php echo CDN_URL; ?>images/mumbai.svg" alt="Mumbai"></i>mumbai</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <?php
                                        foreach ($categoriesTree as $row) {
                                            if ($row->cat_name != '' && $row->cat_name != 'Our Picks' ) {
                                                $active = '';
                                                if (strpos(current_url(), "/" . $row->seourl) !== false) {
                                                    $active = 'active';
                                                }
                                                if ($row->image != '') {
                                                    $catImage = base_url() . 'images/category/' . $row->image;
                                                } else {
                                                    $catImage = base_url() . 'images/wishlist_img_1.jpg';
                                                }
                                                if (isset($row->sub_categories)) {
                                                    if ($row->cat_name != 'Addon') {
                                                        ?>
                                                        <li class="hidden-xs"> <a href="<?php echo base_url(); ?>shopby/<?php echo $row->seourl; ?>" class="tt_uppercase tr_delay <?php echo $active; ?>"><?php echo $row->cat_name; ?></a> </li>
                                                        <li class="dropdown  visible-xs"> <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo base_url(); ?>shopby/<?php echo $row->seourl; ?>"><i><img src="<?php echo $catImage; ?>" alt=""></i><?php echo $row->cat_name; ?> <span class="caret"> <i class="material-icons adown">keyboard_arrow_down</i> <i class="material-icons aup">keyboard_arrow_up</i> </span> </a>
                                                            <ul class="dropdown-menu">
                                                                <?php
                                                                foreach ($row->sub_categories as $subCat) {
                                                                    $subcatImage = '';
                                                                    if ($subCat->image != '') {
                                                                        $catImage = base_url() . 'images/category/' . $subCat->image;
                                                                        $subcatImage = "<i><img src='" . $catImage . "' alt=''></i>";
                                                                    }
                                                                    ?>
                                                                    <li><a href="<?php echo base_url(); ?>shopby/<?php echo $subCat->seourl; ?>"><?php echo $subcatImage . $subCat->cat_name; ?></a></li>
                                                        <?php } ?>
                                                            </ul>
                                                        </li>
                                                    <?php } ?>
                                                <?php } else {
                                                    ?>
                                                    <li><a href="<?php echo base_url(); ?>shopby/<?php echo $row->seourl; ?>" class="tt_uppercase tr_delay <?php echo $active; ?>"><i><img src="<?php echo $catImage; ?>" alt=""></i> <?php echo $row->cat_name; ?> </a> </li>
            <?php
        }
    }
}
?>
<!-- <li> <a class="tt_uppercase"  target="_blank" href="http://vior.in"><i><img src="<?php echo CDN_URL; ?>images/clearance-sale-sm.svg" alt=""></i>Clearance Sale</a> </li> -->
                                        <li class="dropdown showdesktop hidden-xs"> 
                                            <a href="javascript:void(0)">Help <span class="material-icons">keyboard_arrow_down</span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="pages/how-it-works">How It Works?</a></li>
                                                <li><a href="customerpayment" target="_blank">Customer Payment</a></li>
                                                <li><a href="pages/faq">FAQs</a></li>
                                                <li><a href="reviews-testimonials/all">Customer Reviews</a></li>
                                                <li><a href="pages/careers">We are hiring</a></li>
                                                <li><a href="blog/" target="_blank">Blog</a></li>
                                                <li><a href="pages/contact-us">Contact us</a></li>

                                            </ul>
                                        </li>
                                        <li class="dropdown visible-xs"> <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"><i><img src="<?php echo CDN_URL; ?>images/help-sm.svg" alt=""></i>Help <span class="caret"> <i class="material-icons adown">keyboard_arrow_down</i> <i class="material-icons aup">keyboard_arrow_up</i> </span> </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url();?>pages/contact-us">Contact us</a></li>
                    <li><a href="<?php echo base_url();?>pages/how-it-works">How It Works?</a>  </li>
                    <li><a href="<?php echo base_url();?>pages/faq">FAQs</a></li>
                    <li><a href="<?php echo base_url();?>reviews-testimonials/all">Customer Reviews</a></li>
                    <li><a href="customerpayment" target="_blank">Customer Payment</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown visible-xs"> 
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"><i><img src="<?php echo CDN_URL; ?>images/information-sm.svg" alt=""></i>Information 
                                                <span class="caret"> 
                                                    <i class="material-icons adown">keyboard_arrow_down</i> 
                                                    <i class="material-icons aup">keyboard_arrow_up</i> 
                                                </span> 
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="blog/" target="_blank">Blog</a></li>
                    <li><a target="_blank" href="http://vior.in">Clearance Sale</a></li>
                    <li><a href="<?php echo base_url();?>pages/offers">Offers</a></li>
                    <li><a href="<?php echo base_url();?>pages/careers">We are hiring</a></li>
                    <li><a href="<?php echo base_url();?>pages/friends-and-partners">Friends & Partners</a></li 
                                            </ul>
                                        </li>
                                        <li class="dropdown visible-xs"> 
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"><i><img src="<?php echo CDN_URL; ?>images/policies-sm.svg" alt=""></i>POLICIES 
                                                <span class="caret"> 
                                                    <i class="material-icons adown">keyboard_arrow_down</i> 
                                                    <i class="material-icons aup">keyboard_arrow_up</i> 
                                                </span> 
                                            </a>


                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url();?>pages/terms-of-use">Terms of use</a></li>
                    <li><a href="<?php echo base_url();?>pages/privacy-policy">Privacy policy</a></li>
                    <li><a href="<?php echo base_url();?>pages/refer-a-friend">Referral Terms of use</a></li>
                    <li><a href="<?php echo base_url();?>pages/rentalagreement">Sample Rental Agreement</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>                

                                <!-- cart sidebar mene -->
                                <aside class="cartsidebar">
                                    <a href="javascript:void(0)" class="cartcross"><i class="material-icons">close</i></a>
                                    <div class="cartsys">
                                        <div class="titlecart">Your Shopping Cart</div>

                                        <ul id="add_new_product">

                                            <?php
                                            if (!empty($itemsIncart)) {
                                                $subTotal = 0;
                                                foreach ($itemsIncart as $item) {
                                                   
                                                     $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $item->couponID . "' LIMIT 1");
                                                     $rr = $qu->result();

                                                      $cartDiscountAmt = $cartDiscountAmt + ($item->discountAmount * $item->quantity);

                                                    if($rr[0]->maximumamount != 0){
                                                        if($cartDiscountAmt > $rr[0]->maximumamount) {
                                                        $cartAmt = $cartAmt + $cartDiscountAmt - $rr[0]->maximumamount;
                                                            $cartDiscountAmt = $rr[0]->maximumamount;
                                                            }
                                                        }
                                                     
                                                  // $advance_rental = $advance_rental + ($item->price  * $item->quantity)-($item->discountAmount * $item->quantity);
                                                 $product_shipping_cost = $product_shipping_cost + ($item->product_shipping_cost * $item->quantity);

                                                    // $subTotal += ($item->product_shipping_cost * $item->quantity) + ($item->price * $item->quantity) - $item->discountAmount;


                                                     $cartAmt = $cartAmt + (($item->price - $item->discountAmount + ($item->price * 0.01 * $item->product_tax_cost)) * $item->quantity);

                                                    $cartTAmt = ($cartAmt * 0.01 * 0);
                                                    $grantAmt = $cartAmt + $product_shipping_cost + $cartTAmt;

                                                    $cartprodImg = @explode(',', $item->image);
                                                    ?>
                                                    <li id="header-cart-row-<?php echo $item->id; ?>">
                                                        <div class="cart-thumb"><img src="<?php echo CDN_URL; ?>images/product/<?php echo $cartprodImg[0]; ?>" alt="<?php echo $item->product_name; ?>" />
                                                        </div>
                                                        <div class="cart-desc">
                                                            <strong><?php echo $item->product_name; ?> </strong>
                                                            <?php if($item->attr_name !=''){ ?> 
                                                            <small><?php echo $item->attr_type . ' / ' . $item->attr_name; ?></small>
                                                            <?php } ?>
                                                            <span><?php echo $item->quantity . ' x ' . $this->data['currencySymbol'] . $item->price; ?></span>
                                                            <a href="cart"><i class="material-icons">mode_edit</i></a>
                                                            <a href="javascript:void(0)" onclick="javascript:delete_cart(<?php echo $item->id; ?>,<?php echo
                                            $item->id
                                            ?>)"><i class="material-icons">delete</i></a>
                                                        </div>  
                                                    </li>
    <?php   }   ?>
                                                <li>
                                                     <div class="pull-left">ADVANCE RENTAL</div>
                                                    <div class="pull-right"><strong id="rental_amt"><i id="new_pricesymbl" class="fa fa-inr" aria-hidden="true"></i><?php echo $cartAmt; ?></strong></div>
                                                <br>
                                                 <div class="pull-left">REFUNDABLE DEPOSIT</div>
                                                    <div class="pull-right"><strong id="deposit_amt"><i id="new_pricesymbl" class="fa fa-inr" aria-hidden="true"></i><?php echo $product_shipping_cost; ?></strong></div>
                                                <br>
                                                    <div class="pull-left">SUBTOTAL</div>
                                                    <div class="pull-right"><strong id="CartGAmt"><i id="new_pricesymbl" class="fa fa-inr" aria-hidden="true"></i><?php echo $grantAmt; ?></strong></div>
                                                   
                                                </li>
<?php } else { ?>
                                                <div class="emptycartbar">
                                                    <img src="<?php echo CDN_URL; ?>images/empty-cart-icn.svg" alt="Empaty cart">
                                                    <h5>Your Cart is Empty</h5>
                                                    <p>Looks like you haven't chosen <br> any product yet</p>
                                                </div>
                                        <?php } ?>

                                        </ul>
<div class="btncartdiv">
<?php if (!empty($itemsIncart)) { ?>
                                            
                                                <a href="<?php echo base_url(); ?>cart" class="btn-check pull-left btngray"><i class="material-icons">shopping_cart</i><span>View Cart</span></a>
                                                <a href="<?php echo base_url(); ?>cart" class="btn-check pull-right"><span>Check Out</span> <i class="material-icons checkrot">reply</i></a>
<?php } ?>
                                            </div>
                                    </div>
                                </aside>
                                <!-- cart sidebar mene -->
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </header>

        <!--Page Js--> 
        <script src="js/site/header_new_searchbox.js"></script> 
        <script>
                                             function setcity(v) {
                                                 $.ajax({
                                                     url: '<?php echo base_url(); ?>site/ajaxhandler/setcity',
                                                     data: 'v=' + v,
                                                     dataType: 'json',
                                                     type: 'POST',
                                                     success: function (r) {
                                                         if (r.ok == false) {
                                                             alert("Exactly what are you trying to do??");
                                                         }
                                                         if (r.ok == true) {
                                                             window.location.reload();
                                                         }
                                                     }
                                                 });
                                             }
        </script>
        <script>

            var clicked = false;//Global Variable
            function ClickLogin()
            {
                clicked = true;
            }

            function onSignIn(googleUser) {
                if (clicked) {
                    var profile = googleUser.getBasicProfile();
                    var datasend = {};
                    datasend['email'] = profile.getEmail();
                    datasend['gender'] = "";
                    datasend['first_name'] = profile.getName();
                    datasend['last_name'] = "";
                    datasend['facebook_id'] = profile.getId();
                    datasend['name'] = profile.getName();
                    datasend['login_type'] = "google";
                    datasend['str'] = '';

                    if (profile.getEmail()) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>site/user/sociallogin",
                            type: 'POST',
                            dataType: 'json',
                            data: datasend,
                            success: function (x) {
                               location.reload();
                            }
                        });
                    }
                }

            }
            ;


        </script>
        <meta name="google-signin-client_id" content="1065795218106-s2m2k3s28ch432hn8gp669pjjn7esr7d.apps.googleusercontent.com">
        <script src="https://apis.google.com/js/platform.js" async defer></script>


