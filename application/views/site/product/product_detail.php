<?php
$this->load->view('site/templates/header_new');
?>
<style>
#loadingmessage
{
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(http://www.advacto.com/images/progress.gif) 50% 50% no-repeat #ede9df;
	display:none;
	opacity: 0.5;
}
</style>
<div id="loadingmessage"></div>
			<!--main content-->
					<div class="bg_grey_light_2" style="padding:20px 0 20px 0;">
						<h1 class="t_align_c" style="font-size:14px;"><?php echo $productDetails->row()->product_name;?></h1>
			<!--breadcrumbs
			<div class="breadcrumbs bg_grey_light_2 fs_medium fw_light t_align_c">
				<div class="container">
				<?php echo $breadCumps; ?> 
				</div>
			</div>-->
						<div class="t_align_c m_top_10">
							<span class="d_inline_b">Delivered in:</span> <span class="d_inline_b"> <?php $shipping = $productDetails->row()->shipping;
						   switch($shipping)
						   {

							case '1': 
								echo stripslashes($this->lang->line('shipping_in_option_first')); 
							break;

							case '2':
								echo stripslashes($this->lang->line('shipping_in_option_second')); 
							break;

							case '3':
								echo stripslashes($this->lang->line('shipping_in_option_third')); 
							break;

							case '4':
								echo stripslashes($this->lang->line('shipping_in_option_fourth')); 
							break;


							case '5':
								echo stripslashes($this->lang->line('shipping_in_option_fifth')); 
							break;


							case '6':
								echo stripslashes($this->lang->line('shipping_in_option_sixth')); 
							break;
						   }
							?> (Free Delivery and Installation) 
							</span>
						</div>							  
					</div>
			<div class="page_section_offset" style="padding-top:15px;">
				<div class="container m_xs_top_0">
					<div class="row m_xs_top_0">
						<section class="col-lg-12 col-md-12 col-sm-12 m_xs_bottom_15 m_xs_top_0">
							<main class="clearfix m_bottom_40 m_xs_bottom_0 m_xs_top_0">
                            	<?php 
								if ($productDetails->num_rows()==1){
									$img = 'dummyProductImage.jpg';
									$imgArr = explode(',', $productDetails->row()->image);
									if (count($imgArr)>0){
										foreach ($imgArr as $imgRow){
											if ($imgRow != ''){
												$img = $pimg = $imgRow;
												break;
											}
										}
									}
								}
									
								$fancyClass = 'addToFavourite';
								$fancyText = LIKE_BUTTON;
								if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
									foreach ($likedProducts->result() as $likeProRow){
										if ($likeProRow->product_id == $productDetails->row()->seller_product_id){
											$fancyClass = 'addedToFavourite';$fancyText = LIKED_BUTTON;break;
										}
									}
								}
								?>	
								<div class="product_preview f_left f_xs_none wrapper m_xs_bottom_0">
									<div class="d_block relative r_image_container">
										<img  src="<?php echo base_url();?>images/product/<?php echo $img;?>" alt="<?php echo $productDetails->row()->product_name;?>" data-zoom-image="<?php echo base_url();?>images/product/<?php echo $img;?>">
									</div>
									<!--thumbnails-->
									<div class="product_thumbnails_wrap relative m_xs_bottom_0">
										<div class="owl-carousel" id="thumbnails" data-nav="thumbnails_product_" data-owl-carousel-options='{
											"responsive" : {
												"0" : {
													"items" : 3
												},
												"321" : {
													"items" : 4
												},
												"769" : {
													"items" : 2
												},
												"992" : {
													"items" : 3
												}
											},
											"stagePadding" : 40,
											"margin" : 10,
											"URLhashListener" : false
										}'>	
											<?php 
												$limitCount = 0;
												$imgArr = explode(',', $productDetails->row()->image);
												if (count($imgArr)>2){
													foreach ($imgArr as $imgRow){
														if ($limitCount>10)break;

														if ($imgRow != ''){
															$limitCount++;
if ($limitCount==1)continue;
											?>
											<a href="<?php echo base_url().PRODUCTPATH.$imgRow;?>" data-image="<?php echo base_url().PRODUCTPATH.$imgRow;?>" data-zoom-image="<?php echo base_url();?>images/product/<?php echo $imgRow;?>" class="d_block">
												<img src="<?php echo CDN_URL;?>images/product/<?php echo $imgRow;?>" alt="">
											</a>
												
											<?php 
														}
												}
											}
											?>
										</div>
						<?php 
						if (count($imgArr)>2){ ?>
										<button class="thumbnails_product_prev black_hover button_type_4 grey state_2 tr_all d_block vc_child hide_on_mobile"><i class="fa fa-angle-left d_inline_m"></i></button>
										<button class="thumbnails_product_next black_hover button_type_4 grey state_2 tr_all d_block vc_child hide_on_mobile"><i class="fa fa-angle-right d_inline_m"></i></button>
									
							<?php 		}
						?>
						</div>
						
							<?php if ($loginCheck != ''  && ($userDetails->row()->id == $productDetails->row()->user_id)){?>
							<ul class="hide_on_mobile">
								<li><a id="edit-details" class="button_type_2 d_block f_sm_none m_sm_bottom_3 t_align_c lbrown state_2 tr_all second_font fs_medium tt_uppercase f_left m_right_3 product_button" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit"><?php if($this->lang->line('product_edit_dtls') != '') { echo stripslashes($this->lang->line('product_edit_dtls')); } else echo "Edit Product"; ?></a></li>
								<li><a uid="<?php echo $productDetails->row()->user_id;?>" thing_id="<?php echo $productDetails->row()->seller_product_id;?>" ntid="7220865" class="remove_new_thing button_type_2 d_block f_sm_none m_sm_bottom_3 t_align_c lbrown state_2 tr_all second_font fs_medium tt_uppercase f_left m_right_3 product_button" href="things/<?php echo $productDetails->row()->seller_product_id;?>/delete"><?php if($this->lang->line('shipping_delete') != '') { echo stripslashes($this->lang->line('shipping_delete')); } else echo "Delete"; ?></a></li>
							</ul>
							<?php }?>  

                    
								</div>
								<div class="product_description f_left f_xs_none m_xs_bottom_0 m_xs_top_0" id="product_description">
									<table class="w_full">
										<tbody>
											<!-- vinit code start -->
											<?php
													$prodID = $productDetails->row()->id;
													$origPrice = $productDetails->row()->sale_price;
													$userId = $productDetails->row()->user_id;
													$catId = $productDetails->row()->category_id;

														$couponCode = '';
														$discVal = 0.00;
														$discPrice = '';
														$discDesc = '';
														$resultArr = $this->product_model->getDiscountedDetails($prodID,$origPrice,$userId,$catId);
														$couponCode = $resultArr['coupon_code'];
														$discPrice = $resultArr['disc_price'];
														$discVal = $resultArr['disc_percent'];
														$discDesc = $resultArr['disc_desc'];    
														
													
													?>
											<!-- vinit code end -->
												<input type="hidden" class="option number" name="product_id" id="product_id" value="<?php echo $productDetails->row()->id;?>">
												<input type="hidden" class="option number" name="cateory_id" id="cateory_id" value="<?php echo $productDetails->row()->category_id;?>">                
												<input type="hidden" class="option number" name="sell_id" id="sell_id" value="<?php echo $productDetails->row()->user_id;?>">
												<input type="hidden" class="option number" name="price" id="price" value="<?php echo $productDetails->row()->sale_price;?>">
												<input type="hidden" class="option number" name="product_shipping_cost" id="product_shipping_cost" value="<?php echo $productDetails->row()->shipping_cost;?>"> 
												<input type="hidden" class="option number" name="product_tax_cost" id="product_tax_cost" value="<?php echo $productDetails->row()->tax_cost;?>">
												<input type="hidden" class="option number" name="attribute_values" id="attribute_values" value="<?php echo $attrValsSetLoad; ?>">
																		<?php if($discPrice != ''){?>
																			<tr>
																				<td colspan="3" class="color_white" style="background-color:#F2583E;">
																					<!--<div class="m_top_2 m_bottom_6"><span class="fw_light d_inline_m m_right_5">Use coupon code <div style="color: #333333;font-size:12px;font-weight: bold;display:inline-block;">"<?php echo $couponCode;?>"</div> to get <?php echo number_format($discVal);?>% discount on all month's rent, or check other offers appliacble on this product <u><a href="pages/offers" class="color_white" >here</a></u></span></div>-->

<div class="m_top_2 m_bottom_6"><span class="fw_light d_inline_m m_right_5">Check all offers applicable on this product <u><a href="pages/offers" class="color_white" >here</a></u></span></div>
																				</td>
																			</tr>
																		<?php }?>										
											<tr>
												<td class="color_light" style="width:50%;">
													<p class="m_xs_top_0 m_xs_bottom_0 d_inline_b">Refundable Deposit:</p>
													<b class="d_block fs_sm_default d_inline_b"><?php echo $currencySymbol;?><?php echo $productDetails->row()->shipping_cost;?></b>
												</td>
												<td class="color_light" style="width:50%;">
													<p class="m_xs_top_0 m_xs_bottom_0 d_inline_b">Min Rental Tenure:</p>
													<b class="d_block fs_sm_default d_inline_b"><?php echo $productDetails->row()->min_tenure;?></b>
												</td>
											</tr>

										</tbody>
									</table>


		<table class="w_full">
			<tbody>
				<tr>
					<td class="color_light" style="width:50%;">
						<div class="product_options bt_none d_inline_b">
							<?php  
								$attrValsSetLoad = ''; //echo '<pre>'; print_r($PrdAttrVal->result_array()); 
								if($PrdAttrVal->num_rows() >0){ 
									$attrValsSetLoad = $PrdAttrVal->row()->pid; 
							?>
									<div class="d_inline_b">
										<p class="m_bottom_7 m_xs_top_0">Select Rental Duration</p>
										<div class="relative m_bottom_5">
											<select class="select-round select-shipping-addr select_title fs_medium fw_light color_light relative tr_all" id="attr_name_id" name="attr_name_id" onchange="ajaxCartAttributeChange(this.value,'<?php echo $productDetails->row()->id; ?>');">
												<?php foreach($PrdAttrVal->result_array() as $Prdattrvals ){ ?>
													<option value="<?php echo $Prdattrvals['pid']; ?>">
														<?php echo $Prdattrvals['attr_type'].':  '.$Prdattrvals['attr_name']; ?>
													</option>
												<?php } ?>
											</select>
											<div class="alert_box error relative m_bottom_10 fw_light" id="AttrErr" style="margin-top: 10px; display:none;">
											</div>								
										</div>
									</div>
							<?php 
								} 
							?>
						</div>	
						<div class="d_inline_b" style="margin-left:80px;">
						 <p class="m_bottom_7 m_xs_top_0"><?php echo $productDetails->row()->rental_freq;?> Rental:</p>
						 <b><div style="font-size: 19px;"><span class="scheme_color d_block m_bottom_15 m_xs_bottom_0 fs_sm_default" style="float:left;"><?php echo $currencySymbol;?>&nbsp;   </span><span id="SalePrice" class="scheme_color d_inline_b m_bottom_15 m_xs_bottom_0 fs_sm_default"> <?php echo $productDetails->row()->sale_price;?></span></div></b>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<input type="hidden" id="product_cart_count" name="product_cart_count" value="<?php echo $prod_cart_count;?>">
		<input type="hidden" id="parent_product_addon" name="parent_product_addon" value="<?php echo $parent_available;?>">
		<input type="hidden" id="is_prod_addons" name="is_prod_addons" value="<?php echo $is_addon_prod;?>">
		<input type="hidden" id="cart_product_duration" name="cart_product_duration" value="<?php echo preg_replace('/\s+/', '', strtolower($cart_product_duration));?>">

		

										<footer class="bg_grey_light_2">
											<div class="clearfix" style="padding:20px;">
												<div class="quantity clearfix t_align_c f_left f_md_none m_right_10 m_md_bottom_3 d_inline_b">
													<button class="f_left d_block minus black_hover tr_all bg_white">-</button>
													<input type="text" id="quantity" value="1" name="" data-mqty="<?php echo $productDetails->row()->quantity;?>" min="1" readonly="" class="f_left color_light orderQuantity">
													<button class="f_left d_block black_hover tr_all bg_white">+</button>
												</div>
										
												<button  style="height: 38px;margin-left:50px;" class="m_xs_float_r add_to_cart button_type_2 d_inline_b f_sm_none m_sm_bottom_3 t_align_c lbrown state_2 tr_all second_font fs_medium tt_uppercase f_left"
												<?php if ($loginCheck==''){echo 'require_login="true"';}?> name="addtocart" value="<?php if($this->lang->line('header_add_cart') != '') { echo stripslashes($this->lang->line('header_add_cart')); } else echo "Add to Cart"; ?>" onclick="ajax_add_cart('<?php echo $PrdAttrVal->num_rows(); ?>');"
												><i class="fa fa-shopping-cart d_inline_m m_right_9"></i>Add To Cart</button>
												
											<!--	<button style="line-height: 38px;" id="like_product" class="d_inline_b" item_img_url="images/product/<?php echo $img;?>" tid="<?php echo $productDetails->row()->seller_product_id;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?> style="<?php if($fancyClass == "addedToFavourite"){echo "color:darkred !important"; };?>" class="<?php echo $fancyClass?> button_type_8 grey state_2 tr_delay color_dark t_align_c vc_child f_right m_right_3 tooltip_container relative button "><i class="fa fa-heart fs_large d_inline_m f_right d_inline_b"></i>
												<span class="tooltip top fs_small color_white hidden animated" data-show="fadeInDown" data-hide="fadeOutUp">
												<?php if($fancyClass == "addedToFavourite"){echo "Remove from Wishlist"; }else echo "Add to Wishlist";?>
												</span></button>-->
											</div>
										</footer>

									<table class="w_full">
										<tbody>
											<tr>
												<td class="scheme_color">
													<p class="m_bottom_13">
														<?php  echo $productDetails->row()->description; ?><div class="clear"></div> 
													</p>
												</td>
											</tr>
										</tbody>
									</table>

								</div>
							</main>
						</section>

		<!--				<aside class="col-lg-3 col-md-3 col-sm-3">
									<div class="represent_wrap widget clearfix m_bottom_30">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-certificate d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium">
												<p class="color_dark second_font m_bottom_2 fs_large"><b>Mint Condition</b></p>
												<small class="fw_light">All our products delivered to you <br>Rented products are as good as new.</small>
											</div>
										</section>										
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-truck d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium">
												<p class="color_dark second_font m_bottom_2 fs_large"><b>Free Delivery & Setup</b></p>
												<small class="fw_light">We will deliver and Setup it for you without any additional charge.</small>
											</div>
										</section>
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-money d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium">
												<p class="color_dark second_font m_bottom_2 fs_large"><b>Money Back Guarantee</b></p>
												<small class="fw_light">100% moneyback guarantee if you <br>dont like our products.</small>
											</div>
										</section>
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-credit-card d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium">
												<p class="color_dark second_font m_bottom_2 fs_large"><b>Easy Payment Terms</b></p>
												<small class="fw_light">We accept all payment methods - Credit card, netbanking and COD.</small>
											</div>
										</section>
									</div>
						</aside> -->
					</div>
					

                   				<?php 
				if (count($relatedProductsArr)>0 || count($affiliaterelatedProductsArr)>0){
				?>
					<section>
						<h5 class="second_font color_dark tt_uppercase fw_light d_inline_m m_bottom_13"><b>Add-on Products</b></h5>
						<hr class="divider m_bottom_30 m_xs_bottom_0">
						<div class="row" id="addon-products">
                        <?php 
					$limitCount = 0;
					foreach ($relatedProductsArr as $relatedRow){
						if ($limitCount<10){
							$limitCount++;
						$img = 'dummyProductImage.jpg';
						$imgArr = explode(',', $relatedRow->image);
						if (count($imgArr)>0){
							foreach ($imgArr as $imgRow){
								if ($imgRow != ''){
									$img = $imgRow;
									break;
								}
							}
						}
					?>
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 m_bottom_30" id="<?php echo $relatedRow->product_order;?>">
								<figure class="relative r_image_container c_image_container qv_container">
									<div class="relative m_bottom_15">
									<a href="<?php echo base_url();?>things/<?php echo $relatedRow->id;?>/<?php echo url_title($relatedRow->product_name,'-');?>" class="second_font sc_hover">
										<div>
											<img class="tr_all" alt="<?php echo $relatedRow->product_name;?>" src="<?php echo base_url();?>images/product/<?php echo $img;?>">
											<!--<img class="c_image_2 tr_all" alt="<?php echo $relatedRow->product_name;?>" src="<?php echo base_url();?>images/product/<?php echo $img;?>">-->
										</div>
									</a>
									</div>
									<figcaption class="t_align_c">
										<ul>
											<li><a href="<?php echo base_url();?>things/<?php echo $relatedRow->id;?>/<?php echo url_title($relatedRow->product_name,'-');?>" class="second_font sc_hover"><?php echo $relatedRow->product_name;?></a></li>
										</ul>
									</figcaption>

								</figure>
							</div>
                            					<?php 
					}
				}?>
					
						</div>
					</section>
								<?php }?>
									<section class="m_bottom_17">
						<h5 class="second_font color_dark tt_uppercase fw_light d_inline_m m_bottom_13"><b>Our Promise</b></h5>
						<hr class="divider m_bottom_30 m_xs_bottom_0">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_30 m_xs_bottom_0">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-certificate d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium">
												<p class="color_dark second_font m_bottom_2 fs_large"><b>Mint Condition</b></p>
												<small class="fw_light">All our products delivered to you <br>are as good as new</small>
											</div>
										</section>								
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_30 m_xs_bottom_0">							
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-truck d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium">
												<p class="color_dark second_font m_bottom_2 fs_large"><b>Free Delivery & Setup</b></p>
												<small class="fw_light">We will deliver and Setup it for you <br>without any additional charge</small>
											</div>
										</section>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_30 m_xs_bottom_0">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-money d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium">
												<p class="color_dark second_font m_bottom_2 fs_large"><b>Money Back Guarantee</b></p>
												<small class="fw_light">100% moneyback guarantee if you <br>don't like our products</small>
											</div>
										</section>							
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_30 hide_on_mobile">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-credit-card d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium">
												<p class="color_dark second_font m_bottom_2 fs_large"><b>Easy Payment Terms</b></p>
												<small class="fw_light">We accept all payment methods - Credit <br>card, netbanking and cheque</small>
											</div>
										</section>						
							</div>							
						</div>
					</section>
	
<section class="m_bottom_17">
						<hr class="divider m_bottom_30 m_xs_bottom_0">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 m_bottom_30 m_xs_bottom_0">
							<?php  echo $productDetails->row()->excerpt; ?><div class="clear"></div>
							</div>
						</div>
						<hr class="divider m_bottom_30 m_xs_bottom_0">
						
							
				</div>
			</div>
<?php
$this->load->view('site/templates/footer');
?>
		</div>


		<!--libs include-->
		<script src="plugins/jquery.elevateZoom-3.0.8.min.js"></script>
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/jquery.easytabs.min.js"></script>
		<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>
		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
		<script>
			$( document ).ready(function() {				
				//$("#addon-products > div").tsort("",{attr:"id"});
				//$('#addon-products > div').toArray().sort( function(a,b) { a.id - b.id } );
				$("#addon-products > div").sort(function (a, b) {
					return parseInt(a.id) > parseInt(b.id);
				}).each(function () {
					var elem = $(this);
					elem.remove();
					$(elem).appendTo("#addon-products");
				});
			});
		</script>
	</body>
</html>