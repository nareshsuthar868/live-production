<?php $this->load->view('site/templates/header_inner'); ?>
<div class="page_section_offset">
    <?php
    //if ($page_heading != '') { ?>
        <section class="innerbanner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1><?php echo $page_heading; ?></h1>
                        <ul class="breadcrumb">
                            <?php echo trim($breadCumps); ?>
                            <a href="javascript:void(0)" class="hidden-xs">
                                <span class="left_arrow" style='font-size:22px; color: #fff;'>&#8594;</span>
                            </a>
                        </ul>
                        <p style="display: none" class="margin-top-20 hidden-xs seo_description"><?php echo strip_tags($page_description); ?></p>
                            <a href="javascript:void(0)" class="hidden-xs">
                                <span class="right_arrow" style='display: none;font-size:22px; color: #fff;'>&#8592;</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    <?php //} ?>
    <?php if($listSubCat[0]->subcat_seourl != '' && $listSubCat[0]->subcat_sub_cat_status != 'InActive'){   ?>
        <div class="listcategoryrow">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="owl-carousel owl-theme">
                        <?php foreach ($listSubCat as $subCategory){
                            if($subCategory->subcat_sub_cat_status == 'Active'){
                                if($subCategory->subcat_sub_cat_image != ''){
                                    $catImage = CDN_URL . 'images/category/' . $subCategory->subcat_sub_cat_image;
                                }else{
                                    $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
                                }
                                ?>    
                                <div class="item"><a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $subCategory->subcat_seourl; ?>"><img src="<?php echo $catImage; ?>" alt="<?php echo $subCategory->subcat_sub_cat_name; ?>"> <span><?php echo $subCategory->subcat_sub_cat_name; ?></span></a> </div>
                        <?php }
                        
                        } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
    
    <?php 
    //print_r($productList->result());exit;
    if ($productList->num_rows() > 0) { ?>	
     <script type="text/javascript">
        $(function(){
            $('#mobile_footer').css('display','none');
        });
    </script>
        <section class="productlistingrow">
            <section class="container">
                <div class="row" id="product_main_container">
                    <?php foreach ($ResponseProductList as $productListVal) {
                        $img = 'dummyProductImage.jpg';                        
                        $imgArr = explode(',', $productListVal->image);                        
                        if (count($imgArr) > 0) {
                            foreach ($imgArr as $imgRow) {
                                if ($imgRow != '') {
                                    $img = $pimg = $imgRow;
                                    break;
                                }
                            }
                        } ?>

                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 prolistcol">

                            <div class="productlistitem"> 
                            <a  href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>" class="productcatimg hidden-xs" target="_blank">
                               <img data-src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $img; ?>" alt="<?php echo $productListVal->product_name; ?>"  src="<?php echo base_url(); ?>/images/ajax-loader/ajax-loader_new.gif"/>
                                <?php if($productListVal->is_sold){ ?>
                                    <span class="soldout">Out of stock</span>
                                <?php }?>
                            </a>
                             <a  href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>" class="productcatimg visible-xs">
                                <img data-src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $img; ?>" alt="<?php echo $productListVal->product_name; ?>"  src="<?php echo base_url(); ?>/images/ajax-loader/ajax-loader_new.gif"/>
                             <?php if($productListVal->is_sold){ ?>
                                        <span class="soldout">Out of stock</span>
                                    <?php }?>
                            </a>
                            <input type="hidden" id="user_id" value="<?php if(isset($_SESSION['fc_session_user_id'])){ echo $_SESSION['fc_session_user_id']; } ?>">
                            <input type="hidden" id="base_url" value="<?php echo base_url() ?>" >

                            <div class="relative">
                              <div class="proshortdetail hidden-xs"> <span class="title_minheight"><a target="_blank" href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>"><?php echo $productListVal->product_name; ?></a></span>
                                <div class="listcontent">
                                 <p>
                                    <?php  echo strip_tags(substr($productListVal->description, 0, 60)); if(strlen(trim($productListVal->description)) > 60){ echo '....'; }?> 
                                 </p>
                                </div>
                              </div>
                              
                              <div class="priceitemgray">
                                 <a href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>" class="visible-xs"><?php echo $productListVal->product_name;?></a> 
                                <div class="overlaywhis">
                                       <button type="button" class="wishactive">
                                        <i class="material-icons" style="display:none;">favorite_border</i>
                                        <i class="material-icons">favorite</i>
                                      </button>
                                 </div>
                                <div class="prolistmobieprice">
                                            <span class="startprice">Starting from  <i class="fa fa-inr" aria-hidden="true"></i> <strong> <?php echo number_format($productListVal->sale_price); ?></strong></span>
                                            <span class="itemleftw"><?php if($productListVal->subproducts != ''){ ?> 
                                                        <?php 
                                                         $product_quantity = unserialize($productListVal->subproduct_quantity); 
                                                         if(!empty($product_quantity)){
                                                            echo  array_sum($product_quantity);
                                                            
                                                         }else{
                                                         echo count(explode(',', $productListVal->subproducts));
                                                         }
                                                         ?> item
                                                <?php }else{?> 
                                                       <?php echo 1; ?> item
                                                <?php } ?></span>
                                </div>
                              </div>                                 
                            </div>
                          </div>
                    </div>
                    <?php } ?>
                </div>
            </section>
        </section>
    <?php }  else {
        $txt = "'t";
        echo '<div class="productnot"><div class="container"><img src="images/cancel-order-icn.svg" /><h3>We don'.$txt.' provide '.$listSubCat[0]->cat_name.' products '.$selected_city.'</h3></div></div>';  }?>
    <?php  $c_status_count = 0;

    foreach ($listSubCat as $subCategory){         
            if($subCategory->subcat_sub_cat_status == 'Active') {
                $c_status_count++;
            }
    }

    if($c_status_count != 0) {

        if($listSubCat[0]->subcat_seourl != ''){ ?>
            <div class="listcategoryrowmobile">
              <div class="container">
                <div class="categorymobile">
                <?php foreach ($listSubCat as $subCategory){ 
                    if($subCategory->subcat_sub_cat_status == 'Active'){
                        if($subCategory->subcat_sub_cat_image != ''){
                            $catImage = CDN_URL . 'images/category/' . $subCategory->subcat_sub_cat_image;
                        }else{
                            $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
                        }
                        ?> 
                    <div>
                        <a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $subCategory->subcat_seourl; ?>">
                            <img src="<?php echo $catImage; ?>" alt="<?php echo $subCategory->subcat_sub_cat_name; ?>"> <span><?php echo $subCategory->subcat_sub_cat_name; ?></span>
                        </a>
                    </div>
                    <?php }
                } ?>

                </div>
              </div>
            </div>
        <?php } 
    }

    if($parentCat) { 
        $pc = $parentCat->result(); ?>
           <div class="listcategoryrowmobile">
              <div class="container">
                <div class="categorymobile">
                <?php foreach ($pc as $subCategory){
                    //print_r($subCategory);exit;
                    if($subCategory->status == 'Active') {
                        if($subCategory->image != '') {
                            $catImage = CDN_URL . 'images/category/' . $subCategory->image;
                        }else{
                            $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
                        }
                        ?> 
                    <div class="<?php if($this->uri->segment(2) == $subCategory->seourl){ echo 'cat-active'; }?>">
                        <a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $subCategory->seourl; ?>">
                            <img src="<?php echo $catImage; ?>" alt="<?php echo $subCategory->cat_name; ?>"> <span><?php echo $subCategory->cat_name; ?></span>
                        </a>
                    </div>
                    <?php }
                } ?>
                </div>
              </div>
            </div>
     <?php
    }  else {?>
    <script type="text/javascript">
        $(function(){
        $('#mobile_footer').css('display','block');
    });
    </script>
    <?php } ?>

</div>
<?php $this->load->view('site/templates/footer'); ?>
</div>
<style>
    .listcategoryrowmobile{
        height: 3px;
        overflow: hidden;
        visibility: hidden;
        transition: height 0.2s linear;
    }
</style>
<script src="plugins/owl-carousel/owl.carousel.min.js"></script> 
<script>
    $(document).ready(function () {
        $('.left_arrow').click(function() {
            $('.seo_description').show('slow');
            $('.right_arrow').show()
            $('.left_arrow').hide();
        });

        $('.right_arrow').click(function() {
            $('.seo_description').hide('slow');
            $('.right_arrow').hide()
            $('.left_arrow').show();
        });
        
        var totalItems = $('.item').length;
        if (totalItems <= 7) {
            var isNav = false;
        }
        else {
            var isNav = true;
        }

        if($('.owl-carousel').length > 0) {
            $('.owl-carousel').owlCarousel({
                loop: false,
                nav: isNav,
                navText: ["<i class='material-icons'>keyboard_arrow_left</i>", "<i class='material-icons'>keyboard_arrow_right</i>"],
                margin: 0,
                mouseDrag: false,
                pullDrag: false,
                touchDrag: true,
                autoplay: false,
                responsive: {
                    768: {
                        items: 5
                    },
                    1000: {
                        items: 6,
                    },
                    1200: {
                        items: 7
                    }
                }
            });
        }
    });
</script>
</html>
