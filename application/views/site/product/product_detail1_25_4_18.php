<?php $this->load->view('site/templates/header_inner'); ?>
  <script type="text/javascript">
        $(function(){
        $('#mobile_footer').css('display','none');
    });
    </script>
    
<div class="page_section_offset graylightbg"> 
    <div class="productdetailsliderbg">
        <div class="product-slider-full">
            <?php
            $productImgs = explode(',', $productDetails->row()->image);
            foreach ($productImgs as $productImage){ ?>
                <?php if(!file_exists('images/product/slider_view/'.$productImage)) {?>
              <div>  <img src="<?php echo CDN_URL; ?>images/product/<?php echo $productImage; ?>" alt="" /></div>

            <?php  } else{?>
             <div><img src="<?php echo CDN_URL; ?>images/product/slider_view/<?php echo $productImage; ?>" alt="" /></div>
            <?php } }?>
        </div>
         <?php if(count($productImgs) > 2 ){ ?> 
        <div class="prothumbsection">
            <div class="container"> 
                <div class="product-full-thumb">
                    <?php foreach ($productImgs as $productImage){ ?>
                    <?php if($productImage != ""){?>
                        <div>
                            <?php if(!file_exists('images/product/slider_view/'.$productImage)) {?>
                            <div class="pthumb"><img src="<?php echo CDN_URL; ?>images/product/<?php echo $productImage; ?>" alt="" /></div>
                            
                            <?php }else {?>
                             <div class="pthumb"><img src="<?php echo CDN_URL; ?>images/product/<?php echo $productImage; ?>" alt="" /></div>
                             <?php }?>
                        </div>
                    <?php }?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="sideabs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                        <div class="product_description">
                            <h1><?php echo $productDetails->row()->product_name; ?></h1>
                            <?php
                            $tenureArray = array();
                            $sliderData = array();
                            foreach (array_reverse($PrdAttrVal->result_array()) as $tenureData){
                                $tenureArray[(int)$tenureData['attr_name']]['price'] = $tenureData['attr_price'];
                                $tenureArray[(int)$tenureData['attr_name']]['pid'] = $tenureData['pid'];
                                $sliderData[] = (int)$tenureData['attr_name'];
                            } 
                            $cat_id = explode(',', $productDetails->row()->category_id);
                            sort($sliderData);
                            ?>

                            <?php if($productDetails->row()->is_addon !=1 ){?> 
                                 <div class="range-slider monthslider">
                                    <strong>Pick a preferred tenure <?php if($productDetails->row()->rental_freq == "Monthly"){
                                                 echo "(In Months)";
                                             }else{ echo "(In Weeks)";}?>
                                    </strong>
                                    <input type="text" id="tenureslider" class="tenure-range-slider" value="<?php if(in_array(288,$cat_id)){  echo min(array_keys($tenureArray)); } else {  echo max(array_keys($tenureArray)); } ?>"  />
                                 </div>
                            <?php }?>

                            <div class="product_options"> 
                                <?php if($productDetails->row()->is_addon !=1 ){?>
                                 <strong>
                                    <small>  
                                        <?php if($productDetails->row()->rental_freq == "Monthly"){
                                             echo "Monthly";
                                         }else{ echo "Total";}?> Rent</small>
                                </strong> 
                                <?php }?>
                                 <i class="fa fa-inr" aria-hidden="true"></i> 
                                 <strong id="SalePrice"><?php echo $productDetails->row()->sale_price; ?></strong> 
                             </div>

                            <div class="paydeposit"> 
                                <strong><small>Refundable Deposit</small></strong>
                                <i class="fa fa-inr" aria-hidden="true"></i>
                                <strong><?php echo $productDetails->row()->shipping_cost; ?></strong>
                            </div>

                         <div class="addcartsection">
                   <div class="quantity d_inline_b">
                          <button type="button" class="btn-number qtyminus" data-type="minus" data-field="quant[2]">
                              <i class="material-icons">remove_circle_outline</i>
                          </button>
                          <input type="text" name="quant[2]" id="quantity" data-mqty="<?php echo $productDetails->row()->quantity;?>" class="form-control input-number" value="1" min="1" max="<?php echo $productDetails->row()->quantity;?>">
                          <button type="button" class="btn-number qtyplus" data-type="plus" data-field="quant[2]">
                              <i class="material-icons">add_circle_outline</i>
                          </button>
                    </div>

                   <button  class="btn-check add_to_cart" name="addtocart" <?php if ($loginCheck==''){echo 'require_login="true"';}?> value="<?php if($this->lang->line('header_add_cart') != '') { echo stripslashes($this->lang->line('header_add_cart')); } else echo "Add to Cart"; ?>" onclick="ajax_add_cart('<?php echo $PrdAttrVal->num_rows(); ?>');">
                       <i class="material-icons">shopping_cart</i>Add To Cart
                   </button>
                 </div>

                            
                            <div class="addonbtncol">
                                <a class="btn-check addonbtn" data-toggle="modal" data-target="#myModaladdon" >Select Add Ons</a>
                            </div>
                            
                            <input type="hidden" name="product_id" id="product_id" value="<?php echo $productDetails->row()->id;?>">
                <input type="hidden" name="cateory_id" id="cateory_id" value="<?php echo $productDetails->row()->category_id;?>">                
                <input type="hidden" name="sell_id" id="sell_id" value="<?php echo $productDetails->row()->user_id;?>">
                            <input type="hidden" name="price" id="price" value="<?php echo $productDetails->row()->sale_price;?>">
                            <input type="hidden" name="product_shipping_cost" id="product_shipping_cost" value="<?php echo $productDetails->row()->shipping_cost;?>"> 
                            <input type="hidden" name="product_tax_cost" id="product_tax_cost" value="<?php echo $productDetails->row()->tax_cost;?>">
                            <input type="hidden" name="attribute_values" id="attribute_values" value="<?php echo $attrValsSetLoad; ?>">
                            <input type="hidden" name="attr_name_id" id="attr_name_id" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="prothumbsection">
            <div class="container"> 
                <!-- event section -->
                <div class="eventdesc margin-bottom-30">
                 <p> <?php echo $productDetails->row()->description; ?> </p>
                 <?php if($productDetails->row()->weight != '') { ?> 
                    <div class="sizecol"> 
                        <strong>Size: </strong> 
                        <span>
                            <i><img src="<?php echo CDN_URL;?>images/size-icn.svg" alt="size"></i><span><?php echo $productDetails->row()->weight; ?></span>
                        </span> 
                    </div>
                 <?php } ?>
                 <?php if($productDetails->row()->brand != '') { ?> 
                    <div class="sizecol brandright"> 
                        <strong>Brand: </strong>
                        <span><i class="material-icons">loyalty</i><span><?php echo $productDetails->row()->brand; ?></span></span> 
                    </div>                 
                 <?php } ?>
                 <div class="shipeddcolmar">
                 <?php 
                    $day_of_shipping = $productDetails->row()->shipping;
                    if( $day_of_shipping != ''){ ?>
                    <div class="shippedinleft"><i class="material-icons shippedicn">local_shipping</i>
                            <span>
                        Shipped In:
                        <small class="daysspac">
                        <?php
                        if($day_of_shipping == 1){ echo '1-3 days'; }
                        if($day_of_shipping == 2){ echo '4-7 days'; }
                        if($day_of_shipping == 3){ echo '2-3 weeks'; }
                        if($day_of_shipping == 4){ echo '3-4 weeks'; }
                        if($day_of_shipping == 5){ echo '4-6 weeks'; }
                        if($day_of_shipping == 6){ echo '6-8 weeks'; }
                    } ?>
                        </small>
                            </span>   
                    
                    </div>
                 
                 <div class="minrental text-right"> <span>Min Rental Tenure<br />
                   <small><?php echo $productDetails->row()->min_tenure;?></small></span><i class="material-icons">event</i>
                 </div>
                 </div>
                     <?php if(count($subProductsArray) > 0 ){ ?>
                        <div class="itemincludes m_top_5"><strong><?php echo count($subProductsArray);?> ITEMS INCLUDED</strong></div>
                  <?php } ?>
               </div>
            </div>   
        </div>
    </div>
    <?php if(count($subProductsArray) > 0 ){ //print_r($subProductsArray);exit;?>
                <div class="productpackagesrow">
                    <div class="container">
                      <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <h2>Package Includes</h2>
                        <?php  
                        for ( $i = 0; $i < count($subProductsArray); $i++ ) {
                        //foreach($subProductsArray as $subpro) {
                            $image = explode(',', $subProductsArray[$i]->image); ?>
                          <div class="otherproduct">
                                <aside class="productfig">
                                  <figure>
                                    <img src="<?php echo CDN_URL; ?>images/product/<?php echo $image[0];?>" alt="King Bed">
                                </figure>
                                </aside>
                                <aside class="productdetailpack">
                                   <h3><?php echo $subProductsArray[$i]->product_name ;?></h3>
                                   <p><?php echo $subProductsArray[$i]->excerpt; ?></p>
                                    <?php if($subProductsArray[$i]->weight != ''){ ?> 
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 sizecol"> 
                                            <strong>Size</strong> <span>
                                                <i>
                                                <img src="<?php echo CDN_URL; ?>images/size-icn.svg" alt="size"></i><span><?php echo $subProductsArray[$i]->weight; ?></span></span> 
                                        </div>
                                    <?php } ?>
                                    <?php if($subProductsArray[$i]->brand != ''){ ?> 
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 sizecol brandright"> 
                                            <strong>Brand</strong>
                                            <span><i class="material-icons">loyalty</i>
                                            <span><?php echo $subProductsArray[$i]->brand; ?></span>
                                            </span> 
                                        </div>
                                    <?php } ?>
                                </aside>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
              </div>
  <?php } ?>
</div>
<section class="cityfurnishbenefitrow hidden-xs">
    <div class="container">
        <div class="row">
            <h2 class="text-center">Cityfurnish Benefits</h2>
            <div class="col-sm-4 col-md-4 col-lg-4 margin-bottom-30">
                <div class="benefitcol"> <i class="roundshapeicn"><img src="<?php echo CDN_URL; ?>images/cleaning-service-icn.svg" alt="cleaning service" /></i> <strong>Cleaning Services</strong>
                    <p>Don't worry about cleaning furniture surfaces and upholstery! You can avail one free cleanup after completion of 6 month subscription.</p>
                </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 margin-bottom-30">
                <div class="benefitcol"> <i class="roundshapeicn"><img src="<?php echo CDN_URL; ?>images/damage-waiver-icn.svg" alt="cleaning service" /></i> <strong>Damage Waiver</strong>
                    <p>It is natural to have some wear and tear during the course of your subscription. We don't charge you for that.</p>
                </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 margin-bottom-30">
                <div class="benefitcol"> <i class="roundshapeicn"><img src="<?php echo CDN_URL; ?>images/free-location.svg" alt="cleaning service" /></i> <strong>Free relocation</strong>
                    <p>Relocating to a new city where we operate? We will help you relocate for free.</p>
                </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 margin-bottom-30">
                <div class="benefitcol"> <i class="roundshapeicn"><img src="<?php echo CDN_URL; ?>images/free-upgrade-icn.svg" alt="Free upgrade" /></i> <strong>FREE UPGRADE</strong>
                    <p>Renovate your home every year by upgrading to new designs. Delivery and installation is free.</p>
                </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 margin-bottom-30">
                <div class="benefitcol"> <i class="roundshapeicn"><img src="<?php echo CDN_URL; ?>images/rent-to-buy-option-benefits.svg" alt="Rent to by option" /></i> <strong>RENT TO BUY OPTION</strong>
                    <p>Feel like buying our products? Talk to us we will give you a deal you will not be able to refuse.</p>
                </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 margin-bottom-30">
                <div class="benefitcol"> <i class="roundshapeicn"><img src="<?php echo CDN_URL; ?>images/mint-condition-benefits.svg" alt="Mint condition" /></i> <strong>MINT CONDITION</strong>
                    <p>All our products delivered to you are as good as new and we don't reuse soft furnishing.</p>
                </div>
            </div>

        </div>
    </div>
</section>
<section class="section_offset clientreviewsrow text-center hidden-xs proreviewsec">
    <div class="container">
        <div class="col-xs-12">
            <h2>Client Reviews</h2>
        </div>
        <div class="clientslider">
            <?php 
            foreach ($clreviewList as $review){ 
                $reImage = '';
                if($review->image != ''){
                    $reImage = explode(',', $review->image)[0];
                }
                ?>
                <div>
                    <div class="testimonialreviewcol">  
                        <article class="frame_container">
                            <figure>
                                <div class="usrimg">
                                    <span>
                                        <img src="<?php echo CDN_URL; ?>images/product/<?php echo $reImage; ?>" alt="" class="tr_all scale_image">
                                    </span>
                                </div>
                                <figcaption>
                                    <div class="post_excerpt m_bottom_15 t_align_c relative">
                                        <p><?php echo substr($review->excerpt,0,200 ); if(strlen($review->excerpt) > 200){ echo '....';}?></p>
                                        <?php 
                                        $custArray = explode(',',$review->product_name);
                                        if(isset($custArray[0])){
                                            echo "<b>".$custArray[0]."</b>";
                                        }
                                        if(isset($custArray[1])){
                                            echo "<span class='nw_scheme_gray'>, ".$custArray[1]."</span>";
                                        }
                                        ?>
                                    </div>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="text-center margin-top-30"> <a href="<?php echo CDN_URL; ?>reviews-testimonials/all" class="btn-check">View more Client Review</a> </div>
    </div>
</section>
<script src="<?php echo base_url(); ?>js/ion.rangeSlider.min.js"></script>
<script>
    var $range = $(".tenure-range-slider");
    var tenureData = <?php echo json_encode($tenureArray); ?>;
    
    <?php ?>
    $range.ionRangeSlider({
        type: "single",
        values: <?php echo json_encode($sliderData); ?>,
        grid: true,
        onStart: updateInput,
        onChange: updateInput
    });

    function updateInput (data) {
        $('#price').val(tenureData[data.from_value]['price']);
        $('#SalePrice').html(tenureData[data.from_value]['price']);
        $('#attr_name_id').val(tenureData[data.from_value]['pid']);
    }
     function change_for_mobile()
    {
     var data = document.getElementById("mobile-tenureslider").value;
     var new_price  = data.split('|');
     if(new_price[1]){ var tenure_price =  new_price[1];}else { tenure_price = '--'; }
     $('#new_price_tenure').html('&#8377;'+tenure_price);
     $('#attr_name_id').val(new_price[0]);
      $('#price').val(new_price[1]);
    }
</script>
<?php
$this->load->view('site/templates/footer');
?>

<div class="categoryrow">
    <div class="cartbottom">
        <div class="product_options"> 
            <strong>
                <small>Rent per month</small>
            </strong> 
            <strong>&#8377; <?php echo $productDetails->row()->sale_price; ?>
            </strong> 
        </div>
        <div class="dropdownfooter">
            <i class="material-icons">event</i>
            <select name="mobile-tenureslider" id="mobile-tenureslider" onchange="return change_for_mobile()">
                <option value="0" selected="selected">Choose Tenure</option>
                <?php
                foreach ($PrdAttrVal->result_array() as $tenure){
                    echo "<option value='".$tenure['pid'].'|'.$tenure['attr_price']."'>".$tenure['attr_name']."</option>";
                }
                ?>
            </select>
        </div>
         <button  class="btn-check" name="addtocart" <?php if ($loginCheck==''){echo 'require_login="true"';}?> value="<?php if($this->lang->line('header_add_cart') != '') { echo stripslashes($this->lang->line('header_add_cart')); } else echo "Add to Cart"; ?>" onclick="ajax_add_cart('<?php echo $PrdAttrVal->num_rows(); ?>');">
                <i class="material-icons">shopping_cart</i>Add To Cart
        </button>        
    </div>
</div>
<!-- add on popup model -->
<?php if(count($addon_cats) > 0 ){ ?> 
<script type="text/javascript">
  $(function(){
   get_prodcut_count();  })
</script>  
<div class="addonmodel-popup" id="myModaladdon" tabindex="-1" role="dialog" style="padding-right:0px;">
  <div class="container">
    <div class="container-block col-lg-10">
      <div class="addon-title">
        <h2>SELECT ADD ONS FOR YOUR PACKAGE</h2>
        <i class="material-icons" data-toggle="modal" data-target="#myModaladdon">&#xE14C;</i> </div>
      <div class="addontab">
        <div class="pn-ProductNav">
        <ul class="nav nav-tab" id="addnavtab">
          <?php
         // $i = 0;
           foreach ($addon_cats as $addoncat) { ?>            
            <li id="li_<?php echo $addoncat->id;?>"> 
                <a href="#tab_<?php echo $addoncat->id;?>" data-toggle="tab" aria-expanded="false">
                  <span class="addtabicn">
                    <?php if($addoncat->image != ''){ ?> 
                      <img src="<?php echo CDN_URL; ?>images/category/<?php echo $addoncat->image;?>" 
                        alt="<?php echo $addoncat->cat_name; ?>" />
                    <?php } ?>                    
                </span><?php echo $addoncat->cat_name; ?></a>
             </li>
             <?php } ?>
        </ul>
        </div>
        <div class="tab-content">
        <?php foreach ($addon_cats as $addoncat) {   ?>
            <div id="tab_<?php echo $addoncat->id;?>" class="tab-pane fade">
              <div class="row">
                <?php
                    $rpc = 1; 
                    foreach($allAddonProducts->result() as $relatedProduct) {
                            $cat_array = explode(',', $relatedProduct->category_id);              
                            $image = explode(',', $relatedProduct->image); 
                            
                            if(in_array($addoncat->id, $cat_array) ){ 
                                ?> 
                  
                                <div class="col-sm-4">
                                  <div class="product-box">
                                    <div class="product-image"> 
                                        <a onclick="viewProduct(<?php echo $relatedProduct->id;?>)" href="javascript:void(0)" data-toggle="modal" data-target="#myModalview">
                                            <img src="<?php echo CDN_URL; ?>images/product/<?php echo $image[0]; ?>" alt="" />
                                        </a>
                                    </div>
                                    <div class="product-detail">
                                      <h3><?php echo $relatedProduct->product_name; ?></h3>
<!--                                        <a name="addtocart add_to_cart" class="addtocart" <?php if ($loginCheck==''){echo 'require_login="true"';}?> value="<?php if($this->lang->line('header_add_cart') != '') { echo stripslashes($this->lang->line('header_add_cart')); } else echo "Add to Cart"; ?>" onclick="ajax_get_tenure_data('<?php echo $rpc; ?>','no');">
                                            <i class="material-icons">shopping_cart</i>Add To Cart
                                        </a>-->
                                      <span class="pro-price"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $relatedProduct->price; ?></span>
                                      <div class="addonqty">
                                        <div class="quantity d_inline_b">

                                            <button type="button" id="decbutton_<?php echo $relatedProduct->id; ?>" class="btn-number qtyminus" data-type="minus"  onclick="remove_product('<?php echo $relatedProduct->id; ?>','<?php echo $rpc;?>',0)">
                                                <i class="material-icons">remove_circle_outline</i>
                                            </button>
                                          <input name="quant[3]" id="product_quantity_<?php echo $relatedProduct->id; ?>" class="form-control input-number" value="<?php if(isset($relatedProduct->shopping_count)){
                                            echo $relatedProduct->shopping_count; } else { echo '0'; } ?>" min="1" max="100" type="text"  disabled="disabled">
                                            <button type="button" id="puls_button_<?php echo $relatedProduct->id; ?>" class="btn-number qtyplus" data-type="plus"  <?php if ($loginCheck==''){echo 'require_login="true"';}?> value="<?php if($this->lang->line('header_add_cart') != '') { echo stripslashes($this->lang->line('header_add_cart')); } else echo "Add to Cart"; ?>" onclick="ajax_get_tenure_data('<?php echo $rpc; ?>','no');">
                                                <i class="material-icons">add_circle_outline</i>
                                            </button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <input type="hidden" name="product_id_<?php echo $rpc;?>" id="product_id_<?php echo $rpc;?>" value="<?php echo $relatedProduct->id;?>">
                                <input type="hidden" name="cateory_id_<?php echo $rpc;?>" id="cateory_id_<?php echo $rpc;?>" value="<?php echo $relatedProduct->category_id;?>">
                                <input type="hidden" name="sell_id_<?php echo $rpc;?>" id="sell_id_<?php echo $rpc;?>" value="<?php echo $relatedProduct->user_id;?>">
                                <input type="hidden" name="price_<?php echo $rpc;?>" id="price_<?php echo $rpc;?>" value="<?php echo $relatedProduct->sale_price;?>">
                                <input type="hidden" name="product_shipping_cost_<?php echo $rpc;?>" id="product_shipping_cost_<?php echo $rpc;?>" value="<?php echo $relatedProduct->shipping_cost;?>"> 
                                <input type="hidden" name="product_tax_cost_<?php echo $rpc;?>" id="product_tax_cost_<?php echo $rpc;?>" value="<?php echo $relatedProduct->tax_cost;?>">
                                <input type="hidden" name="attribute_values_<?php echo $rpc;?>" id="attribute_values_<?php echo $rpc;?>" value="">
                                <input type="hidden" name="quantity_<?php echo $rpc;?>" id="quantity_<?php echo $rpc;?>" value="<?php echo $relatedProduct->quantity;?>">

                        <?php  }
                         $rpc++;
                    }   ?>
              </div>
            </div>
            <?php } ?> 
            </div>           
      </div>
    </div>
  </div>
<script type="text/javascript">
    $(function(){
       $('#li_<?php echo $addon_cats[0]->id; ?>').addClass('active');
       $('#tab_<?php echo $addon_cats[0]->id;?>').addClass('active in');
    });
</script>

  <div class="addcart-section" id="all_content_addon">
    <div class="widget-section">
      <h3 class="addcart-title">Your Shopping Cart</h3>
      <ul id="new_product_added">
        <?php if(!empty($itemsIncart)){
        $subTotal = 0;
            $c = 0;
            foreach ($itemsIncart as $item){
                $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $item->couponID . "' LIMIT 1");
             $rr = $qu->result();
             $cartDiscountAmt = $cartDiscountAmt + ($item->discountAmount * $item->quantity);
              if ($rr[0]->maximumamount != 0) {
                  if ($cartDiscountAmt > $rr[0]->maximumamount) {
                      $cartAmt = $cartAmt + $cartDiscountAmt - $rr[0]->maximumamount;
                      $cartDiscountAmt = $rr[0]->maximumamount;
                  }
              }

            $product_shipping_cost = $product_shipping_cost + ($item->product_shipping_cost * $item->quantity);
               $cartAmt = $cartAmt + (($item->price - $item->discountAmount + ($item->price * 0.01 * $item->product_tax_cost)) * $item->quantity);

                $cartTAmt = ($cartAmt * 0.01 * 0);
               $grantAmt = $cartAmt + $product_shipping_cost + $cartTAmt;

                $cartprodImg = @explode(',',$item->image); ?>
                <?php if($item->is_addon == 0 ){ ?> 
                    <li id="all-product-detail-cart-<?php echo $c;?>">
                      <div class="cart-item">
                        <div class="cart-image"> 
                            <img src="<?php echo CDN_URL; ?>images/product/<?php echo $cartprodImg[0]; ?>" alt="<?php echo $item->product_name; ?>" /> 
                        </div>
                        <div class="cart-item-detail">
                          <h5><?php echo $item->product_name; ?></h5>
                          <?php if($item->attr_name !='' && $item->attr_type !='' ){ ?>
                          <label><?php echo $item->attr_type.'/'.$item->attr_name; ?></label>
                          <?php } ?> 
                          <span id="new_quantity">
                            <?php echo $item->quantity ?></span>
                            <span><?php echo ' x ' .$this->data['currencySymbol']. $item->price; ?>
                          </span> <br>
                          <a href="cart">
                            <i class="material-icons">&#xE254;</i>
                          </a> 
                          <a onclick="javascript:delete_cart(<?php echo $item->id; ?>,<?php echo $c; ?>)">
                            <i class="material-icons">&#xE872;</i>
                          </a> 
                        </div>
                      </div>
                    </li> 
                <?php $c++;} ?>       
            <?php } ?>
        <?php } else { ?>
        <div class="emptycartbar" id="new_msg_box">
                <img src="<?php echo CDN_URL; ?>images/empty-cart-icn.svg" alt="Empaty cart">
                <h5>Your Cart is Empty</h5>
                <p>Looks  like you haven't chosen <br> any product yet</p>
            </div>
            <script type="text/javascript">
                $(function(){
                $('#title').hide();
            })
            </script>
            <?php }?>
      </ul>
    </div>
    <div class="widget-section package-section">
      <h3 class="addcart-title" id="title">Add ons for your package</h3>
      <ul id="addon_new_product">
        <?php 
        $c1 = 0;
        foreach ($itemsIncart as $item){ 
            $cartprodImg = @explode(',',$item->image);
            if($item->is_addon == 1 ){ ?> 
            <li id="addon-product-detail-cart-<?php echo $item->id;?>">
              <div class="cart-item">
                <div class="cart-image"> <img src="<?php echo CDN_URL; ?>images/product/<?php echo $cartprodImg[0]; ?>" alt="<?php echo $item->product_name; ?>" /> </div>
                <div class="cart-item-detail">
                  <h5><?php echo $item->product_name; ?></h5>
                  <span id="new_quantity_<?php echo $item->product_id  ?>"><?php echo $item->quantity; ?></span>
                  <span><?php echo ' x ' .$this->data['currencySymbol']. $item->price; ?></span> <br>

                  <a href="cart"><i class="material-icons">&#xE254;</i></a> <a href="javascript:void(0)" onclick="javascript:delete_cart(<?php echo $item->id; ?>,<?php echo $item->id; ?>)"><i class="material-icons">&#xE872;</i></a> </div>
              </div>
            </li>
            <?php $c++;} 
        } ?>
      </ul>
      <div class="priceaddon">
    <div class="price-table" id="price_table">
    <div class="discol">
        <label>ADVANCE RENTAL</label>
        <span id="rental_amt">&#8377;<?php echo $cartAmt; ?></span>
    </div>
    <div class="discol">
    <label>REFUNDABLE DEPOSIT</label>
        <span id="deposit_amt">&#8377;<?php echo $product_shipping_cost; ?></span>
    </div>    
    <label>TOTAL RENT</label>
      <span id="DetailCartGAmt">&#8377;<?php echo ' '. $grantAmt; ?></span>
  </div>
    <a href="cart" class="cart-button"><i class="material-icons">&#xE254;</i> view cart</a>
     <a href="cart" class="cart-button color-button">Checkout <i class="material-icons">reply</i></a>
     </div>
</div>
    
    </div>
<!--   <div class="add-cart-popup"> <img src="<?php echo CDN_URL; ?>images/success-icon.svg" alt="" /> -->
 <!--    <label>Added Successfully</label>
  </div> -->
</div>
<?php } ?>
<div id="myModalview" class="modal fade viewdetailpop" role="dialog">
  <div class="common-content">
    <!-- Modal content-->
    <div class="detail-content-box">
      <div class="productpopthumb">
        <img id="p-image" src="" alt="" />
      </div>
      <div class="viewdeatilcontent" id="viewdeatilcontent">
            <h6 id="p-name"></h6>
            <strong id="p-price1"><i class="fa fa-inr" aria-hidden="true"></i></strong>            
            <span id="sub-product-count"></span>
            
            <button type="button" class="close" data-toggle="modal" data-target="#myModalview">
                <i class="material-icons">clear</i>
            </button>
      </div>
    </div>
  </div>
</div>
</div>
</body>
</html>