<?php $this->load->view('site/templates/header_inner'); ?>
<div class="page_section_offset">
    <?php
    //if ($page_heading != '') { ?>
        <section class="innerbanner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1><?php echo $page_heading; ?></h1>
                        <ul class="breadcrumb">
                            <?php echo trim($breadCumps); ?>
                        </ul>
                        <p class="margin-top-20 hidden-xs"><?php echo strip_tags($page_description); ?></p>
                    </div>
                </div>
            </div>
        </section>
    <?php //} ?>
    <?php if($listSubCat[0]->subcat_seourl != '' && $listSubCat[0]->subcat_sub_cat_status != 'InActive'){   ?>
        <div class="listcategoryrow">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="owl-carousel owl-theme">
                        <?php foreach ($listSubCat as $subCategory){
                            if($subCategory->subcat_sub_cat_status == 'Active'){
                                if($subCategory->subcat_sub_cat_image != ''){
                                    $catImage = CDN_URL . 'images/category/' . $subCategory->subcat_sub_cat_image;
                                }else{
                                    $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
                                }
                                ?>    
                                <div class="item"><a href="<?php echo base_url(); ?>shopby/<?php echo $subCategory->subcat_seourl; ?>"><img src="<?php echo $catImage; ?>" alt="<?php echo $subCategory->subcat_sub_cat_name; ?>"> <span><?php echo $subCategory->subcat_sub_cat_name; ?></span></a> </div>
                        <?php }
                        
                        } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
    
    <?php 
    //print_r($productList->result());exit;
    if ($productList->num_rows() > 0) { ?>	
     <script type="text/javascript">
        $(function(){
            $('#mobile_footer').css('display','none');
        });
    </script>
        <section class="productlistingrow">
            <section class="container">
                <div class="row" id="product_main_container">
                    <?php

                    foreach ($productList->result() as $productListVal) {
                        $img = 'dummyProductImage.jpg';
                        
                        $imgArr = explode(',', $productListVal->image);
                        
                        if (count($imgArr) > 0) {
                            foreach ($imgArr as $imgRow) {
                                if ($imgRow != '') {
                                    $img = $pimg = $imgRow;
                                    break;
                                }
                            }
                        } ?>

                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 prolistcol">
                            <div class="productlistitem">
                                <a  href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo url_title($productListVal->product_name, '-'); ?>" class="productcatimg">
                                    <img src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $img; ?>" alt="<?php echo $productListVal->product_name; ?>" />                               
                                </a>
                                <input type="hidden" id="user_id" value="<?php if(isset($_SESSION['fc_session_user_id'])){ echo $_SESSION['fc_session_user_id']; } ?>">
                                <input type="hidden" id="base_url" value="<?php echo base_url() ?>" >
                                <div class="relative">
                                    <div class="proshortdetail hidden-xs">
                                        <span class="title_minheight">
                                         <!--   <a href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo url_title($productListVal->product_name, '-'); ?>"><?php echo $productListVal->product_name; ?></a> -->
<a href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>"><?php echo $productListVal->product_name; ?></a>

                                        </span>
                                        <div class="listcontent">
                                            <p>
                                                <?php 
                                                echo strip_tags(substr($productListVal->description, 0, 100));                                              
                                                if(strlen(trim($productListVal->description)) > 100){
                                                    echo '....';
                                                }?> 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="priceitemgray">
                                        <a href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo url_title($productListVal->product_name, '-'); ?>" class="visible-xs"><?php echo $productListVal->product_name;?></a> 
<!--                                      <div class="overlaywhis">
                                        <button type="button" id="show_button_<?php echo $productListVal->id; ?>" onclick="Add_to_whishlist(<?php echo  $productListVal->id; ?>)">
                                            <?php foreach ($whislist as $like) {
                                                if($like->product_id == $productListVal->id){ ?>
                                                <script type="text/javascript">
                                                    $(function(){
                                                        $('#hide_like_<?php echo $productListVal->id; ?>').hide();
                                                        $('#show_icon_<?php echo $productListVal->id; ?>').show(); 
                                                        $('#show_button_<?php echo $productListVal->id; ?>').addClass('wishactive'); 
                                                    });
                                                </script>     
                                             <?php   }  } ?>
                                                    <i id="show_icon_<?php echo  $productListVal->id; ?>" class="material-icons" style="display: none;" >favorite</i>
                                                    <i id="hide_like_<?php echo $productListVal->id; ?>"  class="material-icons" >favorite_border</i>
                                                    <div id="new_text"></div>                       
                                        </button>
                                    </div>-->




                                        <span class="startprice">Starting from  <i class="fa fa-inr" aria-hidden="true"></i> 
                                        <strong><?php echo number_format($productListVal->sale_price); ?> </strong></span> 
                                        <?php if($productListVal->subproducts != ''){ ?> 
                                                <span class="itemleftw"><?php echo count(explode(',', $productListVal->subproducts))?> item</span>
                                        <?php }else{?> 
                                                <span class="itemleftw"><?php echo 1; ?> item</span>
                                        <?php } ?>
                                    </div>
                
                                
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </section>
        </section>
    <?php }  else {
        $txt = "'t";
        echo '<div class="productnot"><div class="container"><img src="images/cancel-order-icn.svg" /><h3>We don'.$txt.' provide '.$listSubCat[0]->cat_name.' products '.$selected_city.'</h3></div></div>';  }?>
    <?php  $c_status_count = 0;

    foreach ($listSubCat as $subCategory){         
            if($subCategory->subcat_sub_cat_status == 'Active') {
                $c_status_count++;
            }
    }

    if($c_status_count != 0) {

        if($listSubCat[0]->subcat_seourl != ''){ ?>
            <div class="listcategoryrowmobile">
              <div class="container">
                <div class="categorymobile">
                <?php foreach ($listSubCat as $subCategory){ 
                    if($subCategory->subcat_sub_cat_status == 'Active'){
                        if($subCategory->subcat_sub_cat_image != ''){
                            $catImage = CDN_URL . 'images/category/' . $subCategory->subcat_sub_cat_image;
                        }else{
                            $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
                        }
                        ?> 
                    <div>
                        <a href="<?php echo base_url(); ?>shopby/<?php echo $subCategory->subcat_seourl; ?>">
                            <img src="<?php echo $catImage; ?>" alt="<?php echo $subCategory->subcat_sub_cat_name; ?>"> <span><?php echo $subCategory->subcat_sub_cat_name; ?></span>
                        </a>
                    </div>
                    <?php }
                } ?>

                </div>
              </div>
            </div>
        <?php } 
    }

    if($parentCat) { 
        $pc = $parentCat->result(); ?>
           <div class="listcategoryrowmobile">
              <div class="container">
                <div class="categorymobile">
                <?php foreach ($pc as $subCategory){
                    //print_r($subCategory);exit;
                    if($subCategory->status == 'Active') {
                        if($subCategory->image != '') {
                            $catImage = CDN_URL . 'images/category/' . $subCategory->image;
                        }else{
                            $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
                        }
                        ?> 
                    <div class="<?php if($this->uri->segment(2) == $subCategory->seourl){ echo 'cat-active'; }?>">
                        <a href="<?php echo base_url(); ?>shopby/<?php echo $subCategory->seourl; ?>">
                            <img src="<?php echo $catImage; ?>" alt="<?php echo $subCategory->cat_name; ?>"> <span><?php echo $subCategory->cat_name; ?></span>
                        </a>
                    </div>
                    <?php }
                } ?>
                </div>
              </div>
            </div>
     <?php
    }  else {?>
    <script type="text/javascript">
        $(function(){
        $('#mobile_footer').css('display','block');
    });
    </script>
    <?php } ?>

</div>
<?php $this->load->view('site/templates/footer'); ?>
</div>
<script src="plugins/owl-carousel/owl.carousel.min.js"></script> 
<script>
    $(document).ready(function () {
        var totalItems = $('.item').length;
        if (totalItems <= 7) {
            var isNav = false;
        }
        else {
            var isNav = true;
        }

        $('.owl-carousel').owlCarousel({
            loop: false,
            nav: isNav,
            navText: ["<i class='material-icons'>keyboard_arrow_left</i>", "<i class='material-icons'>keyboard_arrow_right</i>"],
            margin: 0,
            mouseDrag: false,
            pullDrag: false,
            touchDrag: true,
            autoplay: false,
            responsive: {
                768: {
                    items: 5
                },
                1000: {
                    items: 6,
                },
                1200: {
                    items: 7
                }
            }
        });
    });
</script>
</body>
</html>
