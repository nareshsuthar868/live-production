<?php
$this->load->view('site/templates/new_header');
?>
<style type="text/css" media="screen">


#edit-details {
    color: #FF3333;
    font-size: 11px;
}
.option-area select.option {
    border: 1px solid #D1D3D9;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 1px 1px 1px #EEEEEE;
    height: 22px;
    margin: 5px 0 12px;
}
a.selectBox.option {
    margin: 5px 0 10px;
    padding: 3px 0;
}
a.selectBox.option .selectBox-label {
    font: inherit !important;
    padding-left: 10px;

}
form label.error{
	color:red;
}

</style>
<div class="lang-en wider no-subnav thing signed-out winOS">
<div class="page_section_offset"> 
  <!-- New  html -->
  <section class="innerbanner">
    <div class="container">
      <div class="row">
         <div class="page-header padding_all15 margin_all0">
                            <h2> <?php if($this->lang->line('product_edit') != '') { echo stripslashes($this->lang->line('product_edit')); } else echo "Edit Product"; ?></h2>
             			 
            </div>
      </div>
    </div>
  </section>
</div>


 <!-- Section_start -->
  <div id="container-wrapper">
	<div class="container ">
	<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
		<div class="wrapper-content">					
            <div class="profile-list">            
                
                
                <div class="box-content">
                    <form accept-charset="utf-8" method="post" action="site/product/sell_it/images" id="sellerProdEdit1" enctype="multipart/form-data">
                    <section class="left-section min_height" style="height: 924px;">	
                        <div class="person-lists bs-docs-example">
                            <ul class="tabs1" id="myTab">
                                    <li ><a  href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit"><?php if($this->lang->line('product_details') != '') { echo stripslashes($this->lang->line('product_details')); } else echo "Details"; ?></a></li>
                                    <li class=""><a  href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/categories"><?php if($this->lang->line('product_categories') != '') { echo stripslashes($this->lang->line('product_categories')); } else echo "Categories"; ?></a></li>
                                    <li><a  href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/list"><?php if($this->lang->line('display_lists') != '') { echo stripslashes($this->lang->line('display_lists')); } else echo "List"; ?></a></li>
                                    <li class="active"><a  href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/images"><?php if($this->lang->line('product_images') != '') { echo stripslashes($this->lang->line('product_images')); } else echo "Images"; ?></a></li>
                                    <li><a  href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/attribute"><?php if($this->lang->line('header_attr') != '') { echo stripslashes($this->lang->line('header_attr')); } else echo "Attribute"; ?></a></li>
                                    <li ><a href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/seo"><?php if($this->lang->line('product_seo') != '') { echo stripslashes($this->lang->line('product_seo')); } else echo "SEO"; ?></a></li>
                                </ul>
                                <input type="hidden" name="nextMode" id="nextMode" value="images"/>
                            <div class="tab-content border_right width_100 pull-left" id="myTabContent"> 
                                <div id="product_info" class="tab-pane active">
                                    <div class="form_fields">
                                        <label><?php if($this->lang->line('product_prod_image') != '') { echo stripslashes($this->lang->line('product_prod_image')); } else echo "Product Image"; ?></label>
                                        <div class="form_fieldsgroup validation-input">
                                            <input type="file" class="global-input multi" name="product_image[]">   
                                            <p style="color:#407A2A;"><?php if($this->lang->line('product_mult_imgs') != '') { echo stripslashes($this->lang->line('product_mult_imgs')); } else echo "You can upload multiple images"; ?></p>                                     </div>
                                    </div>
                                    <div class="widget_content table-responsive">
							<table class="table table-bordered" id="image_tbl">
							<thead>
							<tr>
								<th class="center">
									<?php if($this->lang->line('product_sno') != '') { echo stripslashes($this->lang->line('product_sno')); } else echo "Sno"; ?>
								</th>
								<th>
									<?php if($this->lang->line('product_img') != '') { echo stripslashes($this->lang->line('product_img')); } else echo "Image"; ?>
								</th>
								<th title="Change the order in which your images need to be displayed Eg : 0 is the main image">
									<?php if($this->lang->line('product_position') != '') { echo stripslashes($this->lang->line('product_position')); } else echo "Position"; ?> 
								</th>
								<th>
									  <?php if($this->lang->line('product_actioin') != '') { echo stripslashes($this->lang->line('product_actioin')); } else echo "Action"; ?>
								</th>
							</tr>
							</thead>
							<tbody>
							<?php 
							$imgArr = explode(',', $productDetails->row()->image);
							if (count($imgArr)>0){
								$i=0;$j=1;
								$this->session->set_userdata(array('product_image_'.$productDetails->row()->id => $productDetails->row()->image));
								foreach ($imgArr as $img){
									if ($img != ''){
							?>
							<tr id="img_<?php echo $i ?>">
								<td>
									<input type="hidden" name="imaged[]" value="<?php echo $img; ?>"/>
									<?php echo $j;?>
								</td>
								<td>
									<img src="<?php echo base_url();?>images/product/<?php echo $img; ?>"  height="80px" width="80px" />
								</td>
								<td class="center">
								<span>
									<input type="text" name="changeorder[]" value="<?php echo $i; ?>" size="3" />
								</span>
								</td>
								<td class="center">
									<ul class="action_list" style="background:none;border-top:none;"><li style="width:100%;"><a class="p_del tipTop" href="javascript:void(0)" onclick="editPictureProductsUser(<?php echo $i; ?>,<?php echo $productDetails->row()->id;?>);" title="Delete this image"><?php if($this->lang->line('product_remove') != '') { echo stripslashes($this->lang->line('product_remove')); } else echo "Remove"; ?></a></li></ul>
								</td>
							</tr>
							<?php 
							$j++;
									}
									$i++;
								}
							}
							?>
							</tbody>
							<tfoot>
							<tr>
								<th class="center">
									<?php if($this->lang->line('product_sno') != '') { echo stripslashes($this->lang->line('product_sno')); } else echo "Sno"; ?>
								</th>
								<th>
									<?php if($this->lang->line('product_img') != '') { echo stripslashes($this->lang->line('product_img')); } else echo "Image"; ?>
								</th>
								<th title="Change the order in which your images need to be displayed Eg : 0 is the main image">
									<?php if($this->lang->line('product_position') != '') { echo stripslashes($this->lang->line('product_position')); } else echo "Position"; ?>
								</th>
								<th>
									 <?php if($this->lang->line('product_actioin') != '') { echo stripslashes($this->lang->line('product_actioin')); } else echo "Action"; ?>
								</th>
							</tr>
							</tfoot>
							</table>
						</div>
                                    <input type="hidden" name="PID" value="<?php echo $productDetails->row()->seller_product_id;?>"/>
                                    <br/>
                                    <div class="form_fields">
                                            <label></label>
                                            <div class="form_fieldsgroup form_field_center">
                                            <input type="submit" id="editDetailsSub" value="<?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?>" class="button"/>
                                                                                  </div>
                                        </div>
                                    
                                                                        
                                </div>
                                
                                
                            </div>
                        </div>
                    </section>
                        
                    </form>
                </div>
            </div>
        		
    
</div>
		
		<!-- / wrapper-content -->

		

		
	<!--	<a id="scroll-to-top" href="#header" style="display: none;"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a>-->

	</div>
	<!-- / container -->
		<?php 
     //$this->load->view('site/templates/footer_menu');
     ?>
</div>
</div>
<script src="js/site/<?php echo SITE_COMMON_DEFINE ?>filesjquery_zoomer.js" type="text/javascript"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>selectbox.js"></script>
<script type="text/javascript" src="js/site/thing_page.js"></script>
<script type="text/javascript" src="js/jquery.MultiFile.js"></script>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>

<script>
	$("#sellerProdEdit1").validate();
</script>
<?php
$this->load->view('site/templates/footer');
?>