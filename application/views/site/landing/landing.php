<?php $this->load->view('site/templates/header_new');?>
<script type="text/javascript">/*<![CDATA[*/var error_message="<?php if(isset($_SESSION['user-error-msg'])){ echo $_SESSION['user-error-msg']; unset($_SESSION['user-error-msg']); }?>";if(error_message){sweetAlert("Error!",error_message,"error")}var success_message="<?php if(isset($_SESSION['user-success-msg'])){ echo $_SESSION['user-success-msg']; unset($_SESSION['user-success-msg']); }?>";if(success_message){sweetAlert("Success!",success_message,"success")}$(function(){var b=$(window);var a=$(".home_serach_container");b.on("click.Bst",function(c){if(a.has(c.target).length==0&&!a.is(c.target)){$(".home_serach_container").hide();$("#home_page_search").val("");$(".orverflow_search").attr("style","")}})});/*]]>*/</script>

<section class="hedertopbanner orverflow_search">
<figure> <img src="<?php echo CDN_URL; ?>images/banner-img.jpg" alt="Product-banner">
<figcaption class="captionheadertitle">
<h1><span>Renting sets you free! </span></h1>
<div class="home-search-wrapper">
<input type="search" id="home_page_search" placeholder="What are you looking for?" onkeyup="serach_on_home_page()">
<input type="hidden" name="catSlug" id="catSlug" value="<?php echo $cat_slug; ?>" />
</div>
<a href="<?php echo base_url() ?>shop" class="btn btn-default">Explore</a>
<div class="modal-body" id="home_search_div" style="display:none">
<div class="container home_serach_container">
<div class="row sproductrow search_data">
</div>
<div id="home_no_found" style="display:none">
<figure>
<strong><h4 id="home_new_text"></h4></strong>
</figure>
</div>
<div class="row catagoryrow">
<div class="col-sm-12 col-md-12 col-lg-12 m_top_10">
<h6>Categories</h6>
<hr class="divider_grey margin-top-20 m_bottom_50">
</div>
<div class="new_cat_data">
</div>
</div>
<div id="home_cat_no" style="display:none">
<figure>
<strong><h4 id="home_not_found_cat"></h4></strong>
</figure>
</div>
</div>
</div>
</figcaption>
</figure>
<div class="header_top_part visible-xs">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12">
<p> <span><img src="<?php echo CDN_URL; ?>images/top-percent-icn.svg" alt="percenticn"></span> <span>SUMMER OFFER - 2 MONTHS FREE! OFFER ENDING TONIGHT. <a style="color:white" href="<?php echo base_url(); ?>pages/offers"> <u> KNOW MORE..</u></a></span> </p>
<?php if(!empty($userDetails)){ if($userDetails->row()->is_verified == 'No'){ ?>
<p style="margin:5px 0 0"><span style="color:#141414;text-transform:uppercase;font-weight:700">You still not verify your email, Please Verify it</span> </p>
<?php } }?>
</div>
</div>
</div>
</div>
</section>
<section class="productcategoryrow" id="productdown">
<section class="container">
<section class="row">
<?php foreach ($categoriesTree as $row) { if ($row->cat_name == 'Packages' || $row->cat_name == 'Home Furniture' || $row->cat_name == 'Electronics' || $row->cat_name == 'Office Furniture' || $row->cat_name == 'Fitness') { if($row->image != ''){ $catImage = CDN_URL . 'images/category/' . $row->image;}else{ $catImage = CDN_URL . 'images/wishlist_img_1.jpg'; } ?>
<aside class="productcat">
<a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>">
<div class="productcat-wrapper">
<figure><img src="<?php echo $catImage; ?>" alt="<?php echo $row->cat_name; ?>"></figure>
<article><h3><?php echo $row->cat_name; ?></h3></article>
</div>
</a>
</aside>
<?php } } ?>
</section>
</section>
</section>
<section class="section_discount">
<div class="container">
<div class="row">
<div class="discount-wrapper">
<div class="discount-heading">
<h2>Hot Deals</h2>
</div>
<div class="discount-box-right">
<div class="discount-box-wrapper">
<div class="discount-box">
<span>Flat 50% Off*</span>
<p>Get flat 50% Off in first 3 months. Limited period offer.</p>
</div>
<a href="<?php echo base_url(); ?>pages/offers" class="btn-check">Check all offers</a>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section_how_it_works">
<div class="container">
<div class="row">
<div class="how-it-works-wrapper">
<div class="how-it-works-content col-lg-6 col-md-6 col-sm-12 col-xs-12">
<h2>How does it work? It's very simple.</h2>
<p>You can get your favorite products to your home in just 4 simple steps.</p>
<ul class="process-listing">
<li>
<span class="number-left">1</span>
<div class="process-content-right">
<h4>Place your order</h4>
<p>Select your favorite products, add them into the shopping cart and place the order by paying first month rent and security deposit</p>
</div>
</li>
<li>
<span class="number-left">2</span>
<div class="process-content-right">
<h4>KYC Process</h4>
<p>Our team will call you for verification. You just need to share few required details with us</p>
</div>
</li>
<li>
<span class="number-left">3</span>
<div class="process-content-right">
<h4>Delivery And Installation</h4>
<p>Our delivery team will reach at your doorstep within 72 hrs and install all the products without any additional cost</p>
</div>
</li>
<li>
<span class="number-left">4</span>
<div class="process-content-right">
<h4>Pay Monthly Rental</h4>
<p>Pay monthly rental through standing instructions or PDCs</p>
</div>
</li>
</ul>
<div>
<a href="<?php echo base_url()?><?php echo $cat_slug; ?>/furniture-rental" class="btn-check">Get Started</a>
</div>
</div>
<div class="how-it-works-image col-lg-6 col-md-6 col-sm-12 col-xs-12">
<figure>
<img data-src="<?php echo CDN_URL; ?>images/how-it-works3.jpg" alt="How it works" title="" src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif">
</figure>
</div>
</div>
</div>
</div>
</section>
<section class="section_our_promises">
<div class="container">
<div class="promises-wrapper">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<h2>Our Promise</h2>
</div>
<ul class="promises-listing">
<li>
<div class="promises-box">
<i class="icon">
<img src="<?php echo CDN_URL; ?>images/mint-condition1.svg" alt="Mint Conditon"/>
</i>
<h4>Mint Condition</h4>
</div>
</li>
<li>
<div class="promises-box">
<i class="icon">
<img src="<?php echo CDN_URL; ?>images/free-delivery-setup.svg" alt="Free Delivery Setup"/>
</i>
<h4>Free Delivery & Setup</h4>
</div>
</li>
<li>
<div class="promises-box">
<i class="icon">
<img src="<?php echo CDN_URL; ?>images/free-swap.svg" alt="Free Swap"/>
</i>
<h4>Free Swap</h4>
</div>
</li>
<li>
<div class="promises-box">
<i class="icon">
<img src="<?php echo CDN_URL; ?>images/free-location.svg" alt="Free Relocation"/>
</i>
<h4>Free Relocation</h4>
</div>
</li>
<li>
<div class="promises-box">
<i class="icon">
<img src="<?php echo CDN_URL; ?>images/free-maintenance.svg" alt="Free Maintenance"/>
</i>
<h4>Free Maintenance</h4>
</div>
</li>
<li>
<div class="promises-box">
<i class="icon">
<img src="<?php echo CDN_URL; ?>images/free-upgrade-icn.svg" alt="Free Upgrade"/>
</i>
<h4>Free Upgrade</h4>
</div>
</li>
</ul>
</div>
</div>
</section>
<?php if($caption_list->visible == 'yes') { ?>
<section class="section_offset nwcollectionbg hidden-xs">
<section class="overlyablackbg">
<div class="container">
<div class="row">
<h2><?php echo $caption_list->heading; ?></h2>
<aside class="whitecentercol">
<article class="bgwhitetrans">
<h4>Celebrate FIFA World Cup with us</h4>
<h3><?php echo $caption_list->caption;?> <strong><?php echo $caption_list->discount."%"; ?></strong> Off MONTHLY</h3><a href="<?php echo $caption_list->url; ?>" class="btn-default">Rent Now</a>
</<article>
</aside>
</div>
</div>
</section>
</section>
<?php }?>
<?php if(!empty($bestSelling)){ ?>
<section class="section_offset bestsellingrow hidden-xs">
<div class="container">
<div class="row">
<h2>Trending Products</h2>
<ul class="nav nav-tabs">
<?php $count=0; foreach ($bestSelling as $sellingCat){ if($sellingCat->cat_name != 'Addon' && $sellingCat->cat_name != 'Office Furniture' && $sellingCat->cat_name != 'All' && $sellingCat->cat_name != 'Fitness' && $sellingCat->cat_name != 'Cred'){ ?>
<li class="<?php echo ($count == 0) ? 'active' : ''; ?>"><a data-toggle="tab" href="#<?php echo $sellingCat->id; ?>"><?php echo $sellingCat->cat_name; ?></a></li>
<?php } ?>
<?php $count++;} ?>
</ul>
<div class="tab-content">
<?php $count=0; foreach ($bestSelling as $sellingCat){ ?>
<div id="<?php echo $sellingCat->id; ?>" class="tab-pane fade in <?php echo ($count == 0) ? 'active' : ''; ?>">
<?php if(!empty($sellingCat->categoryproducts)){ $count = 0; foreach ($sellingCat->categoryproducts as $productDetails){if($count >= 3)break;$count++;$proImage = '';if($productDetails->image != ''){$proImage = explode(',', $productDetails->image)[0]; } ?>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hidden-xs">
<a href="<?php echo base_url(); ?>things/<?php echo $productDetails->id; ?>/<?php echo $productDetails->seourl ;?>">
<figure class="relative wrapper scale_image_container m_bottom_30 r_image_container"><img data-src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $proImage; ?>"  alt="<?php echo $productDetails->product_name; ?>"  src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif">
<figcaption class="caption_type_1 tr_all">
<div class="caption_inner"> <strong><?php echo $productDetails->product_name; ?></strong> <strong>&#8377; <?php echo $productDetails->price; ?></strong>
</div>
</figcaption>
</figure>
</a>
</div>
<?php } ?>
<?php } ?>
</div>
<?php $count++;} ?>
</div>
</div>
</div>
</section>
<?php } ?>
<?php if(!empty($bestSelling)){ ?>
<section class="section_offset bestsellingrow bestsellingrow-mobile visible-xs">
<div class="container">
<div class="row">
<h2>Trending Products</h2>
<div class="tabcenterslider">
<?php foreach ($bestSelling as $sellingCat){ if($sellingCat->cat_name == 'Packages') { foreach ($sellingCat->categoryproducts as $cp) { $image = explode(',', $cp->image); ?>
<div>
<div>
<a href="<?php echo base_url()?>things/<?php echo $cp->id ;?>/<?php echo $cp->seourl ;?>">
<figure class="relative wrapper scale_image_container"><img src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif" data-src="<?php echo CDN_URL; ?>images/product/<?php echo $image[0]; ?>" alt="<?php echo $sellingCat->product_name?>">
<figcaption class="caption_type_1 tr_all">
<div class="caption_inner"> <strong><?php echo $cp->product_name?></strong> <strong>&#8377; <?php echo $cp->sale_price?></strong>
<p>
<?php echo $cp->meta_title?>
</p>
</div>
</figcaption>
</figure>
</a>
</div>
</div>
<?php }}} ?>
</div>
</div>
</div>
</section>
<?php } ?>
<section class="section_offset gotqueryrow text-center">
<section class="container">
<div class="row">
<div class="cols-sm-12 col-md-12 col-lg-12">
</div>
<div class="centerquerycontent">
<aside class="col-sm-4 col-md-4 col-lg-4">
<div class="faq-left">
<h2>So what's the catch?</h2>
<p>There isn't one. Seriously. No sneaky contracts and no tricky fine prints. Still not convinced? Read on.</p>
</div>
</aside>
<aside class="col-sm-8 col-md-8 col-lg-8 text-left faq-right">
<div class="panel-group" id="accordion">
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> Why should I rent when I can even buy these products? <i class="more-less hidden-xs expand-up"></i> <i class="more-less visible-xs expand-down"></i> </a> </div>
</div>
<div id="collapse1" class="panel-collapse collapse in">
<div class="panel-body">Primary reason that you should be subscribing with Cityfurnish is ease and affordability. Now your furniture does not own you any more, hassles of refurbishment, relocation and depreciation will become things of past.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> Isn't it costlier than buying? <i class="more-less expand-down"></i></a> </div>
</div>
<div id="collapse2" class="panel-collapse collapse">
<div class="panel-body">Actually it is not. Think about the capital you invest while buying furniture and appliances and the interest cost of that capital, You will find that monthly subscribing them on long term basis is more economical. And moreover you can also refurnish your home every year by switching to renting.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"> What is the minimum tenure of renting?<i class="more-less expand-down"></i></a> </div>
</div>
<div id="collapse3" class="panel-collapse collapse">
<div class="panel-body">The minimum tenure is 3 months and there is no limit of maximum tenure.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"> What is the delivery timeline and do I need to pay for delivery?<i class="more-less expand-down"></i></a> </div>
</div>
<div id="collapse4" class="panel-collapse collapse">
<div class="panel-body">Our team will deliver and install all products in maximum 72 hours and that too free of cost.</div>
</div>
</div>
</div><a href="<?php echo base_url() ?>pages/faq" class="btn-check">Check All faqs</a> </aside>
</div>
</div>
</section>
</section>
<?php if(!empty($clreviewList)){ ?>
<section class="section_offset clientreviewsrow text-center">
<div class="container">
<div class="col-xs-12">
<h2>Client Reviews</h2></div>
<div class="clientslider">
<?php foreach ($clreviewList as $review){ $reImage=''; if($review->image != ''){$reImage = explode(',', $review->image)[0]; }?>
<div>
<div class="testimonialreviewcol">
<article class="frame_container">
<figure>
<div class="usrimg"><span><img src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif" data-src="<?php echo CDN_URL; ?>images/product/<?php echo $reImage; ?>" alt="" class="tr_all scale_image"> </span></div>
<figcaption>
<div class="post_excerpt m_bottom_15 t_align_c relative">
<p>
<?php echo substr($review->excerpt,0,200 ); if(strlen($review->excerpt) > 200){ echo '....';}?>
</p>
<?php $custArray=explode(',',$review->product_name);if(isset($custArray[0])){ echo "<b>".$review->product_name."</b><br>"; }if(isset($custArray[1])){ echo "<span class='nw_scheme_gray'> ".$custArray[1]."</span>";} ?></div>
</figcaption>
</figure>
</article>
</div>
</div>
<?php } ?>
</div>
<div class="text-center margin-top-30"> <a href="<?php echo base_url(); ?>reviews-testimonials/all" class="btn-check">Check all Testimonials</a> </div>
</div>
</section>
<?php } ?>
<section class="section_offset medialogorow hidden-xs">
<div class="container">
<h2 class="nw_scheme_color">Media also loves us</h2>
<div class="row media_also_loves_us_silder">
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
<figure> <a href="http://yourstory.com/2016/02/cityfurnish/" rel="nofollow" target="_blank" class="d_block scale_image_container wrapper color_white"> <img data-src="<?php echo CDN_URL; ?>images/YourStory_Media-Logo.png" alt="Yourstory coverage for furniture rental" class="tr_all scale_image" src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif"> </a> </figure>
</div>
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
<figure> <a href="http://techstory.in/cityfurnish-acquires-funding-24122015/" rel="nofollow" target="_blank" class="d_block scale_image_container wrapper color_white"> <img  data-src="<?php echo CDN_URL; ?>images/techstory.png" alt="Techstory coverage" class="tr_all scale_image " src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif"> </a> </figure>
</div>
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
<figure> <a href="http://www.iamwire.com/2015/12/cityfurnish-set-revolutionise-demand-furniture-rental-industry-india/129073" target="_blank" rel="nofollow" class="d_block scale_image_container wrapper color_white"> <img data-src="<?php echo CDN_URL; ?>images/logo-iamwire.png" alt="Iamwire coverage" class="tr_all scale_image" src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif"> </a> </figure>
</div>
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
<figure> <a href="https://www.vccircle.com/exclusive-citrus-pays-gupta-backs-furniture-rental-venture-cityfurnish/" rel="nofollow" target="_blank" class="d_block scale_image_container wrapper color_white"> <img data-src="<?php echo CDN_URL; ?>images/vccircle-com-squarelogo.png" alt="VCcircle coverage" class="tr_all scale_image" src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif"> </a> </figure>
</div>
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
<figure> <a href="http://bizztor.com/cityfurnish-raises-seed-funding-from-citrus-pays-cofounder-jitendra-gupta/" rel="nofollow" target="_blank" class="d_block scale_image_container f_left wrapper color_white"> <img data-src="<?php echo CDN_URL; ?>images/Bizztor1.png" alt="Bizztor coverage" class="tr_all scale_image" src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif"> </a> </figure>
</div>
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
<figure> <a href="https://economictimes.indiatimes.com/wealth/earn/startup-cityfurnish-offers-furniture-consumer-durables-on-rent/articleshow/64329260.cms/" rel="nofollow" target="_blank" class="d_block scale_image_container f_left wrapper color_white"> <img data-src="<?php echo CDN_URL; ?>images/economics-wealth-logo.png" alt="economics-wealth" class="tr_all scale_image" src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif"> </a> </figure>
</div>
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
<figure> <a href="https://www.businesstoday.in/magazine/the-buzz/cityfurnish-easy-and-affordable-furniture-rental/story/277857.html/" rel="nofollow" target="_blank" class="d_block scale_image_container f_left wrapper color_white"> <img data-src="<?php echo CDN_URL; ?>images/business-today-logo.png" alt="business-today" class="tr_all scale_image" src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif"> </a> </figure>
</div>
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
<figure> <a href="https://www.thehindubusinessline.com/companies/furniture-rental-firm-cityfurnish-scouting-for-4-m/article9876274.ece/" rel="nofollow" target="_blank" class="d_block scale_image_container f_left wrapper color_white"> <img data-src="<?php echo CDN_URL; ?>images/thehindu-businessline-logo.png" alt="thehindu-businessline" class="tr_all scale_image" src="<?php echo base_url(); ?>images/ajax-loader/ajax-loader_new.gif"> </a> </figure>
</div>
</div>
</div>
</section>
<script type="text/javascript">
$( document ).ready(function() {
//   $("img").trigger("unveil");
    $("img").unveil(400, function() {
      $(this).load(function() {
        this.style.opacity = 1;
      });
    });
});
$('body').on('click', '.slick-arrow', function () {
    $("img").unveil(400, function() {
      $(this).load(function() {
        this.style.opacity = 1;
      });
    });
})
</script>
<?php $this->load->view('site/templates/footer'); ?>