<?php
$this->load->view('site/templates/header_new');
?>
<script>
$( document ).ready(function() {
		
		  logout();
		  signOut();
		setTimeout(function(){  }, 5000);
	});
</script>

<?php //}?>
			<!--main content-->
			<div class="page_section_offset">
				<div class="container">
					<div class="row m_bottom_50">
						<aside class="col-lg-4 col-md-4 col-sm-4 p_top_4">
						</aside>
						<section class="col-lg-4 col-md-4 col-sm-4">
							
							
							<div style="width:510px;float:left;margin-left:-66px;margin-bottom:25px;">
									<a href="javascript:void(0);" onclick="fb_login('<?php echo $next; ?>');" style="vertical-align:middle;">
															<img src="<?php echo BASE_URL; ?>images/facebookSignin.png">
														</a>
														
														<a href="javascript:void(0);" data-longtitle="true" class="g-signin2" onclick="ClickLogin()" data-onsuccess="onSignIn" style="float:left;border:1px solid #ccc;margin-right:3px;"></a>
														
														<div style="background:#0077b5;height:38px;width:161px;vertical-align:middle;float:right;line-height:5px; padding-left:8px;">
															<script type="in/Login"></script>
														</div>
														<div id="profileData" style="display: none;">
															<p><a href="javascript:void(0);" onclick="logout()">Logout</a></p>
														</div>
							</div>
							
							<h2 class="fw_light second_font color_dark m_bottom_27 tt_uppercase t_align_c">Sign in with email</h2>
							
							<form method="post" action="site/user/login_user" class="frm clearfix"><input type='hidden' >
								<ul class="m_bottom_14">
													<li class="m_bottom_15">
														<label for="username" class="second_font m_bottom_4 d_inline_b fs_medium">Email Address</label>
														<input type="text" id="username" name="email" placeholder="" autofocus="autofocus" class="w_full tr_all">
													</li>
													<li class="m_bottom_20">
														<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">Password</label>
														<input type="password" id="password" name="password" placeholder=""  class="w_full tr_all m_bottom_20">
														<input class="next_url" type="hidden" name="next" value="<?php echo $next;?>"/>
														<?php if (validation_errors() != ''){?>
															<div id="validationErr" class="alert_box warning m_bottom_10 relative fw_light">
																<script>setTimeout("hideErrDiv('validationErr')", 3000);</script>
																<span class="d_inline_m second_font fs_medium color_red d_md_block"><?php echo validation_errors();?></span>
															</div>
															<?php }?>
															<?php if($flash_data != '') { ?>
															<div class="errorContainer alert_box warning m_bottom_10 relative fw_light" id="<?php echo $flash_data_type;?>">
																<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
																<span class="d_inline_m second_font fs_medium color_red d_md_block"><?php echo $flash_data;?></span>
															</div>
														<?php } ?>
													</li>
													<li>
														<button class="t_align_c tt_uppercase w_full second_font d_block fs_medium button_type_2 lbrown tr_all">Log In</button>
													</li>
													
								</ul>
							</form>
							
							<div class="m_bottom_14 t_align_c" style="font-size:18px;text-align:left;">
												<a href="forgot-password" class="second_font sc_hover fs_small">Forgot Password?</a>
												<a href="/signup" class="second_font sc_hover fs_small  createNewUser" style="float:right;">Signup</a><br>
								</div>
								
							
						</section>
						<aside class="col-lg-2 col-md-2 col-sm-2 p_top_2">
						</aside>
					</div>
				</div>
			</div>
			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>

		<!--libs include-->
		<script src="plugins/jquery.appear.js"></script>
		<script src="plugins/afterresize.min.js"></script>
	 

		<!--theme initializer-->
		<script src="js/themeCore.js"></script>
		<script src="js/theme.js"></script>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '183441015137603',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();   
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
function fb_login(str){	
    FB.login(function(response) {
        if (response.authResponse) {
			console.log('Welcome!  Fetching your information.... ');
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID
            FB.api('/me?fields=id,name,email,gender,first_name,last_name', function(response) {
				var datasend = {};				
                datasend['email'] = response.email;	  
                datasend['gender'] = response.gender; 		  
                datasend['first_name'] = response.first_name; 		  
                datasend['last_name'] = response.last_name; 		  
                datasend['facebook_id'] = response.id;
                datasend['name'] = response.name;				
                datasend['str'] = str;				
				if(response.email){
					$.ajax({
						url: "https://cityfurnish.nachmundiye.com/site/user/sociallogin",
						type: 'POST',
						dataType: 'json',
						data: datasend,
						success: function(x){
							window.location.href = x.url;
						}
					});
				}
            });

        } else {
            //user hit cancel button
            console.log('User cancelled login or did not fully authorize.');

        }
    }, {
        scope: 'email, public_profile, user_friends'
    });
} 
 
</script>		
	</body>
</html>