<?php
$this->load->view('site/templates/header_inner');
?>	
<style>
{box-sizing:border-box}.inputtext{width:100%;padding:12px;border:1px solid #ccc;border-radius:4px;resize:vertical;background:#fff}.col-75 select{background-color:#fff;background-image:url(https://d1eohs8f9n2nha.cloudfront.net/images/down-select-arrow.png);background-position:97% center;background-repeat:no-repeat}label{padding:12px 12px 12px 0;display:inline-block}.btn{background-color:#4CAF50;color:#fff;padding:12px 20px;border:none;border-radius:4px;cursor:pointer;float:right;margin-top:14px}.col-25,.col-75{float:left;margin-top:6px}.btn:hover{background-color:#45a049}.container_new{border-radius:5px;background-color:#f2f2f2;padding:50px}.col-25{width:25%}.col-75{width:75%}.row:after{content:"";display:table;clear:both}.close{opacity:.7!important;font-size:32px;margin-right:7px}@media only screen and (max-width:580px){.col-25{width:100%;margin-top:0}label{padding:0}.col-75{width:100%;padding-left:0!important;margin-bottom:10px}.container_new{padding:20px 40px 30px}}
</style>
<!--main content-->
<div class="page_section_offset lightgryabg pageheight">
    <section class="innerbanner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Your Order</h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Your Order</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="profilecolumn">
                <?php $this->load->view('site/user/settings_sidebar');?>
                <main class="col-lg-9 col-md-9 col-sm-9">
                    <div class="yourorderhistory">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><b>Order #</b></th>
                                        <th><b>Rental</b></th>
                                        <th><b>Order Date</b></th>
                                        <th><b>Order Status</b></th>
                                        <th><b>Options</b></th>
                                        <th><b>Recurring Payment</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($purchasesList->result() as $row){ ?>
                                    <tr>
                                        <td data-cell-title="Product Name and Category">
                                            <div class="lh_small m_bottom_7"><br><?php echo $row->dealCodeNumber;?></div>
                                        </td>
                                        <td data-cell-title="Product Name and Category">
                                          <div class="lh_small m_bottom_7"><br><?php echo $row->total;?></div>
                                        </td>
                                        <td data-cell-title="Product Name and Category">
                                          <div class="lh_small m_bottom_7"><br><?php echo  date(" h:i a d/m/y", strtotime($row->created));?></div>
                                        </td>
                                        <td data-cell-title="Product Name and Category">
                                          <div class="lh_small m_bottom_7"><br><?php echo $row->zoho_sub_status ? $row->zoho_sub_status : '---'  ;?></div>
                                        </td>
                                        <td data-cell-title="Quantity">
                                            <div>
                                                <a style="color:green;" target="_blank" href="view-purchase/<?php echo $row->user_id;?>/<?php echo $row->dealCodeNumber;?>"><?php echo "View Order Details"; ?></a><br/>
                                            </div>
                                            <div id="request_service_<?php echo $row->dealCodeNumber; ?>">
                                                <a style="color:green;"  data-toggle="modal" data-target="#myModal1" onclick="zoho_submit('<?php  echo $row->user_id; ?>','<?php  echo $row->dealCodeNumber; ?>')"><?php echo "Request Service"; ?></a>
                                            </div>
                                        </td>
                                        <td data-cell-title="Product Name and Category">
                                            <div class="lh_small m_bottom_7"><br><?php 
                                                if($row->is_recurring){
                                                    echo "Yes";
                                                }else{ echo "No"; } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="divider_td">
                                        <td colspan="6">&nbsp;</td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="container_new">
                    <form id="case_form">
                        <div class="row">
                          <div class="col-25">
                            <label for="type">Service Request Type</label>
                          </div>
                          <div class="col-75">
                            <select name="type" class="inputtext" onchange="change_value()" id="type">
                              <option value="select"  selected="true" disabled="true">Select Request Type</option>
                              <option value="cancellation">Cancel Order</option>
                              <option value="full_extension">Extend Tenure</option>
                              <option value="request_pickup">Request Order Pickup</option>
                              <option value="repair">Repair</option>
                              <option value="replacement">Replacement</option>
                              <option value="upgrade">Upgrade</option>
                              <option value="installation">Installation</option>
                              <option value="relocation">Relocation</option>
                              <option value="buy">Buy</option>
                               <option value="change_bill_cycle">Change Bill Cycle</option>
                            </select>
                          </div>
                        </div>
                        <div class="row" id="pickup_type_div" style="display: none;">
                            <div class="col-25">
                                <label for="type">Request Order Pickup</label>
                            </div>
                            <div class="col-75">
                                <select name="pickup_type" class="inputtext" id="pickup_type">
                                    <option value="Partial" selected="true">Partial</option>
                                    <option value="Full">Full</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" id="pickup_reason_div" style="display: none;">
                            <div class="col-25">
                                <label for="type">Pickup Reason</label>
                            </div>
                            <div class="col-75">
                                <select name="pickup_reason" class="inputtext" id="pickup_reason">
                                    <option value="Products not needed anymore" selected="true">Products not needed anymore</option>
                                    <option value="Did not like products">Did not like products</option>
                                    <option value="Faced problem in service">Faced problem in service</option>
                                    <option value="Faced problems with products">Faced problems with products</option>
                                    <option value="Switching to other provider">Switching to other provider</option>
                                    <option value="Want to purchase things now">Want to purchase things now</option>
                                    <option value="Moving to other city">Moving to other city</option>
                                    <option value="Moving out of country">Moving out of country</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" id="Possible_Values"  style="display: none;">
                            <div class="col-25">
                                <label for="type">Cancellation Reason</label>
                            </div>
                            <div class="col-75">
                                <select name="Possible_Values" class="inputtext" id="possible_value">
                                  <!-- <option>Product Out Of Stock</option> -->
                                  <!-- <option>Customer Found Better/Cheaper Option</option> -->
                                  <!-- <option>Document Not Available</option> -->
                                  <!-- <option>Customer Not Available</option> -->
                                  <!-- <option>Invalid Product Select</option> -->
                                  <option>Wrong Items Selected</option>
                                  <option>Late Delivery</option>
                                  <option>Want To Buy Items</option>
                                  <option>Items Not Required Anymore</option>
                                  <!-- <option>Customer Want to Buy</option> -->
                                  <!-- <option>Customer No longer Needs Item</option> -->
                                  <!-- <option>Non Serviceable Area</option> -->
                                  <option>Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" id="tenure" style="display: none;">
                          <div class="col-25">
                            <label for="firstname">Extension Tenure(Months)</label>
                          </div>
                          <div class="col-75">
                            <select class="inputtext" id="get_tenure">
                              <?php  for ($i=1; $i <= 24; $i++) { ?>
                              <option><?php echo $i; ?></option> <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="row" id="date_picker" style="display: none;">
                          <div class="col-25">
                            <label for="firstname">Preferred Pickup Date</label>
                          </div>
                          <div class="col-75">
                            <input  class="form-control" type="text" placeholder="Select Date"  id="Chose_date" readonly="true" style="background-color: white;">
                          </div>
                        </div>
                        <div class="row" id="allign_bill_cycle" style="display: none;">
                            <div class="col-25">
                            </div> 
                          <div class="col-75">
                            <!-- <input type="text" class="inputtext" name="allign_bill_cycle_check" id="allign_bill_cycle_check"> -->
                            <div class="col-lg-10 col-sm-10 col-md-10 addressinput">
                            <div class="checkdiv">
                              <input id="allign_bill_cycle_check" name="allign_bill_cycle_check" type="checkbox">
                              <label for="allign_bill_cycle_check" style="padding: 1px; padding-left: 45px;">Align Bill Cycle to 1st day of Month</label>
                              </div>
                              </div>
                            </div>
                        </div>
                        <div class="row" id="requested_date_picker" style="display: none;">
                          <div class="col-25">
                            <label for="firstname">Preferred Date</label>
                          </div>
                          <div class="col-75">
                            <input  class="form-control" type="text" placeholder="Select Date"  id="requested_date" readonly="true"  style="background-color: white;">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-25">
                            <label for="subject">Description</label>
                          </div>
                          <div class="col-75">
                            <textarea id="description" class="inputtext" name="description" placeholder="Write something.." style="height:200px"></textarea>
                          </div>
                        </div>
                        <input type="hidden" id="user_id" name="user_id" value="">
                        <input type="hidden" id="deal_id" name="deal_id" value="">
                        <div class="row">
                            <button type="button" onclick="Create_zoho_cash()" id="zoho_submit_button" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('site/templates/footer'); ?>
</div>

<script type="text/javascript">
function change_value(){
  $('#Chose_date').val('');
  var Selected_value = $('#type').val();
  if(Selected_value == 'cancellation'){
    $('#Possible_Values').show();
  }
  else{
    $('#Possible_Values').hide();
  }
  if(Selected_value == 'request_pickup'){
    $('#date_picker').show();
    $('#pickup_type_div').show();
    $('#pickup_reason_div').show();
  }else{
    $('#date_picker').hide();
    $('#pickup_type_div').hide();
    $('#pickup_reason_div').hide();
  }
  if(Selected_value == 'full_extension'){
    $('#tenure').show();
  }else{
    $('#tenure').hide();
  }
    if(Selected_value == 'repair' || Selected_value == 'replacement' || Selected_value == 'installation'){
        $('#requested_date_picker').show();
    }else{
      $('#requested_date_picker').hide();
    }
    
    if(Selected_value == 'change_bill_cycle')
    {
        $('#allign_bill_cycle').show();
    }
    else{
        $('#allign_bill_cycle').hide();
    }
  
}

function zoho_submit(user_id,deal_id){
  $('#user_id').val(user_id);
  $('#deal_id').val(deal_id);
}

function Create_zoho_cash(){
  var Selected_value = $('#type').val();
  var Pickup_Request = '';
  var tenure = '';
  var pickup_request_date = '';
  var Pickup_Request_Type = '';
  var pickup_reason = '';
  var requested_date = '';
  if(Selected_value == null){
     alert('Please Select Type');
     return false;
  }
  else if(Selected_value == 'request_pickup'){
    Pickup_Request_Type = $('#pickup_type').val();
    pickup_request_date = $('#Chose_date').val();
    pickup_reason = $('#pickup_reason').val();
      if(pickup_request_date == ''){
        alert('Please Select Date');
        return false;
      }
  }
  else if(Selected_value == 'change_bill_cycle')
  {
    if (!$('#allign_bill_cycle_check').is(":checked"))
     {
        alert('Please Checkbox check');
        return false;
    }
  }
  
  if(Selected_value == 'full_extension'){
    tenure = $('#get_tenure').val();
  }
  if(Selected_value == 'repair' || Selected_value == 'replacement' || Selected_value == 'installation'){
    requested_date = $('#requested_date').val();
     if(requested_date == ''){
        alert('Please Select Date');
        return false;
      }
  }

  var user_id = $('#user_id').val();
  var deal_id = $('#deal_id').val();
  $('#zoho_submit_button').attr('disabled','disabled');
  $.ajax({
      url: baseURL + "site/order/zoho_submit",
      data: {
        'user_id':user_id,
        'deal_id':deal_id,
        'description':$('#description').val(),
        'Possible_Values':$('#possible_value').val(),
        'type':$('#type').val(),
        'Pickup_Request_Type':Pickup_Request_Type,
        'tenure':tenure,
        'pickup_request_date':pickup_request_date,
        'requested_date':requested_date,
        'pickup_reason':pickup_reason
      },                         
      type: 'post',
      beforeSend: function(){   
        $('#overlay').show();
        },
      success: function(result){
        $("#case_form").trigger('reset');
        $('#pickup_type_div').hide();
        $('#pickup_reason_div').hide();
        $('#Possible_Values').hide();
        $('#tenure').hide();
        $('#date_picker').hide();
        $('#requested_date_picker').hide();
        $('#overlay').hide();
        $('#allign_bill_cycle').hide();
        $('#zoho_submit_button').removeAttr('disabled','disabled');
        if(result.status){
          $('#myModal1').modal('hide');  
          sweetAlert({
              title: 'Success',
              text: result.msg,                         
              type: "success",
              showCancelButton: false,
              closeOnConfirm: true,
              animation: "slide-from-top",
              showConfirmButton: true,      
          });            
        }
        else{
          $('#myModal').modal('hide');  
          sweetAlert({
              title: 'warning',
              text: result.msg,                         
              type: "warning",
              showCancelButton: false,
              closeOnConfirm: true,
              animation: "slide-from-top",
              showConfirmButton: true,      
          });
        }
      }
  });
}



$(document).ready(function () {
  var date = new Date();
  date.setDate(date.getDate()+1);
   var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  $('#Chose_date').datepicker({
       format: "yyyy-mm-dd",
      startDate :  date,
      minDate: today,
      autoclose: true
  }); 

  $('#requested_date').datepicker({
      format: "yyyy-mm-dd",
      startDate : date,
      minDate: today,
      autoclose: true
  });  
});
</script>
 <script src="<?php echo base_url()?>assets/js/Date_Picker.js"></script>
 <link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" type="text/css">