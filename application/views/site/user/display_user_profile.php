<?php
$this->load->view('site/templates/header_inner');
?>
</script>
			<!--breadcrumbs-->
<div class="breadcrumbs bg_grey_light_2 fs_medium fw_light">
	<div class="page_section_offset lightgryabg pageheight">
    <section class="innerbanner">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h1>Wishlist</h1>
            <ul class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Wishlist</li>
            </ul>
          </div>
        </div>
      </div>
    </section>
	</div>
	<!--main content-->
	<div class="container">
		<div class="row">
			<div class="profilecolumn">
        <?php $this->load->view('site/user/settings_sidebar'); ?>
          <main class="col-lg-9 col-md-9 col-sm-9">
						<div class="whishlistcol">
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th><b>Product</b></th>
                      <th><b>Advance Rental</b></th>
                      <th><b>Refundable Deposit</b></th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $rpc = 1;
                    foreach ($productLikeDetails->result() as $value) {
                      ?>
                      <tr id="new_row_<?php echo $value->id; ?>">
                        <td>
                          <?php $img = explode(',', $value->image); ?>
                       <a href="<?php echo base_url(); ?>things/<?php echo $value->id; ?>/<?php echo url_title($value->product_name, '-'); ?>">
                          <div class="prdthumb"><img src="<?php echo base_url()."images/product/$img[0]" ?>" /></div>
                        </a>
                          <div class="prdname">
                            <strong><?php echo $value->product_name; ?></strong>
                            <small>Duration / 24 months</small>
                          </div>
                        
                        </td>
                        <td> 
                           <strong>&#8377;<?php echo $value->sale_price; ?></strong>      
                        </td>
                        <td>
                          <strong>&#8377;<?php echo $value->shipping_cost; ?></strong>
                        </td>
                        <td>
                          <a onclick="add_to_cart('<?php echo $rpc; ?>')"><i class="material-icons" id="new_<?php echo $rpc;?>">shopping_cart</i> Add To Cart</a>
                        </td>
                            <input type="hidden" name="product_id_<?php echo $rpc;?>" id="product_id_<?php echo $rpc;?>" value="<?php echo $value->id;?>">                                         
                            <input type="hidden" name="cateory_id_<?php echo $rpc;?>" id="cateory_id_<?php echo $rpc;?>" value="<?php echo $value->category_id?>">
                            <input type="hidden" name="sell_id_<?php echo $rpc;?>" id="sell_id_<?php echo $rpc;?>" value="<?php echo $value->user_id;?>">
                            <input type="hidden" name="product_shipping_cost_<?php echo $rpc;?>" id="product_shipping_cost_<?php echo $rpc;?>" value="<?php echo $value->shipping_cost;?>"> 
                            <input type="hidden" name="product_tax_cost_<?php echo $rpc;?>" id="product_tax_cost_<?php echo $rpc;?>" value="<?php echo $value->tax_cost;?>">
                            <input type="hidden" name="quantity_<?php echo $rpc;?>" id="quantity_<?php echo $rpc;?>" value="<?php echo $value->max_quantity;?>">
                            <input type="hidden" name="price_<?php echo $rpc;?>" id="price_<?php echo $rpc;?>" value="<?php echo $value->price;?>">
                        <td>
                          <a onclick="remove_wishlist(<?php echo  $value->id; ?>)"><i class="material-icons clsbtn">close</i></a>
                        </td>
                      </tr>
                       <tr class="divider_td">
                    <td colspan="6">&nbsp;</td>
                  </tr>
                      <?php $rpc++; }?>      
                  </tbody>
                </table>
                  <input type="hidden" id="base_url" value="<?php echo base_url();?>">
                  <input type="hidden" id="user_id" value="<?php if(isset($_SESSION['fc_session_user_id'])){ echo $_SESSION['fc_session_user_id']; } ?>">
              </div>	
            </div>
          </main>
        </div>
		  </div>
    </div>
	</div>
<!--footer-->
<?php
$this->load->view('site/templates/footer');
?>
</div>

		<!--back to top-->
		<button class="back_to_top animated button_type_6 grey state_2 d_block black_hover f_left vc_child tr_all"><i class="fa fa-angle-up d_inline_m"></i></button>

		<!--libs include-->
		<script src="plugins/jquery.appear.js"></script>
		<script src="plugins/afterresize.min.js"></script>
	 

		<!--theme initializer-->
		<script src="js/themeCore.js"></script>
		<script src="js/theme.js"></script>
	</body>
</html>