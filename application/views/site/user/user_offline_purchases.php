<?php
$this->load->view('site/templates/header_inner');
?>	
<style>
{box-sizing:border-box}.inputtext{width:100%;padding:12px;border:1px solid #ccc;border-radius:4px;resize:vertical;background:#fff}.col-75 select{background-color:#fff;background-image:url(https://d1eohs8f9n2nha.cloudfront.net/images/down-select-arrow.png);background-position:97% center;background-repeat:no-repeat}label{padding:12px 12px 12px 0;display:inline-block}.btn{background-color:#4CAF50;color:#fff;padding:12px 20px;border:none;border-radius:4px;cursor:pointer;float:right;margin-top:14px}.col-25,.col-75{float:left;margin-top:6px}.btn:hover{background-color:#45a049}.container_new{border-radius:5px;background-color:#f2f2f2;padding:50px}.col-25{width:25%}.col-75{width:75%}.row:after{content:"";display:table;clear:both}.close{opacity:.7!important;font-size:32px;margin-right:7px}@media only screen and (max-width:580px){.col-25{width:100%;margin-top:0}label{padding:0}.col-75{width:100%;padding-left:0!important;margin-bottom:10px}.container_new{padding:20px 40px 30px}}
</style>


    <!--main content-->
<div class="page_section_offset lightgryabg pageheight">
<section class="innerbanner">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <h1>Your Order</h1>
            <ul class="breadcrumb">
               <li><a href="#">Home</a></li>
               <li class="active">Your Order</li>
            </ul>
         </div>
      </div>
   </div>
</section>
<div class="container">
   <div class="row">
<!-- <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#normal_payment">Normal Orders</a></li>
    <li><a data-toggle="tab" href="#recurring_payment">Recurring Orders</a></li>
  </ul> -->
      <div class="profilecolumn">
        <?php $this->load->view('site/user/settings_sidebar');?>
        <main class="col-lg-9 col-md-9 col-sm-9">
          <div class="yourorderhistory tab-content">
            <div class="table-responsive tab-pane fade in active">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th><b>Order #</b></th>
                    <th><b>Amount</b></th>
                    <th><b>Order Date</b></th>
                    <th><b>Order Owner</b></th>
                    <th><b>Options</b></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($purchasesList->result() as $row){ ?>
                  <tr>
                    <td data-cell-title="Product Name and Category">
                      <div class="lh_small m_bottom_7"><br><?php echo $row->dealCodeNumber;?></div>
                    </td>
                    <td data-cell-title="Product Name and Category">
                      <div class="lh_small m_bottom_7"><br><?php echo $row->total;?></div>
                    </td>
                    <td data-cell-title="Product Name and Category">
                      <div class="lh_small m_bottom_7"><br><?php echo $row->created;?></div>
                    </td>
                    <td data-cell-title="Product Name and Category">
                      <div class="lh_small m_bottom_7"><br><?php echo $row->email;?></div>
                    </td>
                    <td data-cell-title="Quantity">
                      <div>
                        <a style="color:green;" target="_blank" href="view-purchase-offline/<?php echo $row->user_id;?>/<?php echo $row->dealCodeNumber;?>"><?php echo "View Order Details"; ?></a><br/>
                      </div>
                    </td>

                  </tr>
                  <tr class="divider_td">
                    <td colspan="6">&nbsp;</td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </main>
      </div>
   </div>
</div>
<?php $this->load->view('site/templates/footer'); ?>
	</div>
</body>
</html>

<style type="text/css">
  .yourorderhistory {
    width: 140% !important; 
  }
</style>


 <script src="<?php echo base_url()?>assets/js/Date_Picker.js"></script>
 <link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" type="text/css">