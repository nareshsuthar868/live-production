<?php
$this->load->view('site/templates/header_inner');
?>
		<!--main content-->
			<div class="page_section_offset lightgryabg pageheight">
            	<div class="container">
					<div class="row">
					 <div class="resetpasswordcol">
                    	<main class="col-sm-offset-1 col-md-offset-1 col-lg-10 col-md-10 col-sm-10">
							<h1>Reset password</h1>
                            <div class="profilerightedit">
                                <div class="col-sm-offset-2 col-md-offset-2 col-lg-8 col-md-8 col-sm-8">
                                        <form name="password_change" id="password_change">
                                            <div class="txtprofile">
                                                <label for="password">New Password</label>
                                                <input  type="password" name="chngpass" id="chngpass" class="form-control" placeholder="* * * * * *">
                                            </div>
                                            <div class="txtprofile">
                                                <label for="password">Confirm Password</label>
                                                <input  type="password" name="cchngpass" id="cchngpass" placeholder="* * * * * *"  class="form-control">
                                            </div>
                                            <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($id)){ echo $id[0]->id;} ?>">
                                             <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url()?>home">
                                            <div class="editsubmit">
                                            	<button  class="btn-submit" onclick="return check_value();"  id="save_password">Submit</button>
                                            </div>
                                            <!-- <button name="submit" onclick="return check_value();" class="btn btn-success">Submit</button> -->
                                       </form>
                                </div>
                                 
                            </div>
						</main>
                     </div>
   
                   </div>
				</div>
            </div>
			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>

		
	</body>
</html>