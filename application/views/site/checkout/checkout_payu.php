<?php $this->load->view('site/templates/header_inner'); ?>
<script type='text/javascript'>
    window.onload=function(){ 
        window.setTimeout($("#transactionForm").submit(), 1000);
    };
</script>
<div class="page_section_offset">
    <div class="container">
        <div class="row">
            <aside class="col-lg-2 col-md-2 col-sm-2 p_top_4">
            </aside>
            <section class="col-lg-8 col-md-8 col-sm-8">
                <div class="wrapper-content order">
                    <div id="content">
                        <div class="cart-list chept2">
                            <div class="cart-payment-wrap card-payment new-card-payment" id="otherPay" style="display:block;">
                                <form name="TransactionForm" id="transactionForm"  method="post"  action="<?php echo  $ABC['action_url']; ?>">
                                  <input type="hidden" name="key" value="<?php echo  $ABC['key']; ?>" />
                                  <input type="hidden" name="hash" value="<?php echo $ABC['hash']; ?>"/>
                                  <input type="hidden" name="txnid" value="<?php echo $ABC['txnid']; ?>" />
                                  <input type="hidden" name="amount" value="<?php echo $ABC['orderAmount']; ?>" />
                                  <input type="hidden" name="email" value="<?php echo $ABC['email']; ?>" />
                                  <input type="hidden" name="firstname" value="<?php echo $ABC['firstname']; ?>" />
                                  <input type="hidden" name="phone" value="<?php echo $ABC['phone']; ?>" />
                                  <input type="hidden" name="productinfo" value="<?php echo $ABC['productinfo']; ?>" />
                                  <!-- <input type="hidden" name="recurring" value="FIRST" /> -->
                             
                                  <!-- <input type="hidden" name="ccnum" value="5100018609086544" /> -->
                                  <!-- <input type="hidden" name="ccname" value="test" /> -->
                                  <!-- <input type="hidden" name="ccvv" value="123" /> -->
                                  <!-- <input type="hidden" name="ccexpmon" value="05" />   -->
                                  <!-- <input type="hidden" name="ccexpyr" value="2020" /> -->
                                  <?php if($payu_type == 'si'){ ?>
                                  <input type="hidden" name="store_card" value="1" />
                                  <input type="hidden" name="si" value="1" />
                                  <input type="hidden" name="pg" value="cc" />
                                  <input type="hidden" name="bankcode" value="VISA" />
                                  <input type="hidden" name="user_credentials" value="<?php echo $ABC['user_credentials']; ?>" />
                                <?php } ?>
                                  <input type="hidden" name="surl" value="<?php echo $ABC['returnUrl']; ?>" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
                                  <input type="hidden" name="furl" value="<?php echo $ABC['faile_url']; ?>" />


                                            <div id="complete-payment" class="paymsg">
                                                <div class="hotel-booking-left">
                                                        <!--    <img src="http://sandbox.citruspay.com/images/logo.png"/> -->
                                                    <div class="text-center">
                                                        <h3>Redirecting to our payment partner's site</h3>
                                                        <h5>Please Wait for a second!!.....</h5><h5>
                                                                <div class="loader-inner line-scale-pulse-out-rapid">
                                                                    <div></div>
                                                                    <div></div>
                                                                    <div></div>
                                                                    <div></div>
                                                                    <div></div>
                                                                </div>
                                                                <br>
                                                                </h5>
                                                    </div>
                                                </div>
                                            </div>

                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
</section>
</div>
</div>
</div>

    <?php $this->load->view('site/templates/footer'); ?>
    </body>
</html>