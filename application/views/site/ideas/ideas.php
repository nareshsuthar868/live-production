<?php
$this->load->view('site/templates/header_inner');
?>
<!--main content-->
<div class="page_section_offset"> 
  <!-- New  html -->
	<section class="innerbanner">
	    <div class="container">
	      <div class="row">
	        <div class="col-lg-12">
	          <h1>Customer Feedback and Testimonials</h1>
	          <ul class="breadcrumb">
	            <li><a href="#">Home</a></li>
	            <li class="active"> Customer review page</li>
	          </ul>
	        </div>
	      </div>
	    </div>
	  	</section>
	    <section class="testimonialrow">
			<div class="container">
				<div class="row">
					<div class="portfolio_isotope_container">
					<?php if($userproductList->num_rows()>0){?>							
						<?php  foreach ($userproductList->result() as $productListVal) { 
							$img = 'dummyProductImage.jpg';
							$imgArr = explode(',', $productListVal->image);
							if (count($imgArr)>0){
								foreach ($imgArr as $imgRow){
									if ($imgRow != ''){
										$img = $pimg = $imgRow;
										break;
									}
								}
							}
							?>
						<div class="portfolio_isotope_item">
								<div class="tastimonialcontent">
	              					<figure class="relative">
										<div class="testithumb">
											 <?php if(file_exists("images/product/".$img)){ ?>
												<img src="<?php echo CDN_URL ?>images/product/<?php echo $img; ?>" alt="<?php echo $productListVal->product_name;?>" >
											<?php
											}else{ ?>
                                               <img src="<?php echo CDN_URL ?>images/product/dummyProductImage.jpg" alt="<?php echo $productListVal->product_name;?>" >
											<?php }?>
										</div>
									</figure>
									<div class="testimonialreview">
										<p class="m_bottom_7">
											<i class="color_dark"><?php echo $productListVal->excerpt;?></i>
										</p>
										<?php 	$name_city= explode(",",$productListVal->product_name);
										//var_dump($name_city);	
										?>
										<p><strong><?php
										if(isset($name_city[0])){
										 	echo $name_city[0]; 
										 } 
										 ?></strong>
										<span><?php
										if(isset($name_city[1])){
										 	echo ', '.$name_city[1]; 
										 } ?>										 	
										 </span></p>
									</div>
								</div>
								<hr class="divider_light m_bottom_5">
							</div>
						<?php 
						} ?>
						<?php 
					} ?>
							
					</div>
						<div class="pagination" style="display:none">
							<?php echo $paginationDisplay; ?>
						</div>
							<!-- </main> -->
				</div>
			</div>
	</section>
			
		<!--footer-->
		<?php
		$this->load->view('site/templates/footer');
		?>
	</div>
<script>
$(window).resize(function(){
    masonry_layout();
});

function masonry_layout(){
	$('.portfolio_isotope_container').isotope({
		  itemSelector: '.portfolio_isotope_item',
		  percentPosition: true,
		  masonry: {
			// use outer width of grid-sizer for columnWidth
			//columnWidth: '.grid-item'
		  }
	});
}
	
$(window).load(function() { 	
	masonry_layout();
});
        
        </script> 
<script src="js/grid-isotope.pkgd.js"></script>
		<!--<!--back to top-->
		<!-- <button class="back_to_top animated button_type_6 grey state_2 d_block black_hover f_left vc_child tr_all"><i class="fa fa-angle-up d_inline_m"></i></button> -->

		<!--libs include-->
		<!--<script src="plugins/jquery.appear.js"></script>
		<script src="plugins/isotope.pkgd.min.js"></script>-->
	
		<!--theme initializer-->
		<!--<script src="js/themeCore.js"></script>
		<script src="js/theme.js"></script>-->

	</body>
</html>