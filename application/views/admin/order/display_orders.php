<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/check_login.js"></script>
<div id="admin_loader" style="display: none;"><div class="loader-inner"><img src="<?php echo base_url()?>images/admin_loader.gif"></div></div>

<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/order/change_order_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
					</div>
					<div class="widget_content">
						<table class="display sort-table" id="Paid_orders_list">
                            <thead>
                                <tr>
	        <!--                    	<th class="center">-->
									<!--	<input name="checkbox_id[]" type="checkbox" value="on" class="selectall">-->
									<!--</th>-->
	                                <th>Order Id</th>
	                                <th>User Email</th>
	                                <th>Phone no</th>
	                                <th>Order Date</th>
	                                <th>Transaction ID</th>
	                                <th>Total</th>
	                                <th>City</th>
	                                <th>Status</th>
	                                <th>Zoho Status/SubStatus</th>
	                                <th>Coupon Code</th>
	                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	        <!--                    	<th class="center">-->
									<!--	<input name="checkbox_id[]"  type="checkbox" value="on" class="selectall12">-->
									<!--</th>-->
	                                <th>Order Id</th>
	                                <th>User Email</th>
	                                <th>Phone no</th>
	                                <th>Order Date</th>
	                                <th>Transaction ID</th>
	                                <th>Total</th>
	                                <th>City</th>
	                                <th>Status</th>
	                                <th>Zoho Status/SubStatus</th>
	                                <th>Coupon Code</th>
	                                <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>

			
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>

<script type="text/javascript">
$(function(){
	// add multiple select / deselect functionality
	$(".selectall").click(function () {
		  $('.case').attr('checked', this.checked);
		  $('.selectall12').attr('checked', this.checked);
	});
	$(".selectall12").click(function () {
		  $('.case').attr('checked', this.checked);
		   $('.selectall').attr('checked', this.checked);
	});
});

function change_all(){
	if($(".case").length == $(".case:checked").length) {
		$(".selectall").attr("checked", "checked");
		$(".selectall12").attr("checked", "checked");
	} else {
		$(".selectall").removeAttr("checked");
		$(".selectall12").removeAttr("checked", "checked");
	}
}
</script>
<script type="text/javascript">
$(function (){
    $('#Paid_orders_list').DataTable({
        processing:  true,
        serverSide: true,
        pageLength: 25,
        lengthMenu: [ 10, 25, 50, 75, 100 ],
        ajax: '<?php echo base_url(); ?>admin/order/Get_Paid_order',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        order: [[ 4, "desc" ]],
        columns: [
            { "data": "id" },
            { "data": "email"},
            { "data": "phone_no"},
            { "data": "created" },
            { "data": "dealCodeNumber" },
            { "data": "total" },
            { "data": "shippingcity"},
            { "data": "status"},
              null,
            { "data": "couponCode"},
            null
        ],
        columnDefs: [
        // 	{
        //          orderable: false, targets: [0],
        //         "render": function ( data, type, full, meta ) {
        //             var link  = '<input name="checkbox_id[]" type="checkbox" value="'+ full.id +'" class="case" onclick="change_all()">';
        //             return link;
        //         }
        //     },
            // {"targets": [0], "className": "center tr_select"},
            {
                 orderable: false, targets: [3],
                "render": function ( data, type, full, meta ) {
                    var link  = '<input style="width: 70px;margin:5px;" type="text" value='+ data +'>\
                    			<a href="javascript:void(0);" onclick="update_phone(this,'+ full.uid +');">Update</a>';
                    return link;
                }
            },
            {
                 orderable: true, targets: [7],
                "render": function ( data, type, full, meta ) {
                    var link  = '<input style="width: 45px;margin:5px;" type="text" value="'+ data +'"/>\
                                 <a href="javascript:void(0);" onclick="update_status(this,'+ full.id +');">Update</a>';
                    return link;
                }
            },
            {
                 orderable: true, targets: [8],
                "render": function ( data, type, full, meta ) {
                   if(full.zoho_status){
                    return full.zoho_status+' / '+full.zoho_sub_status;
                   }else{
                    return 'NA';
                   }
                }
                
            },
            {
                 orderable: false, targets: [10],
                "render": function ( data, type, full, meta ) {
                	var user_id = full.user_id;

			        var dealCodeNumber = full.dealCodeNumber;
			      
                    var link  = '<a class="tipTop" title="Send Voucher Email" onclick="Send_Voucher('+ full.user_id +','+ full.dealCodeNumber +')"><span class="action-icons c-approve" style="cursor:pointer;"></span></a></a><a href="order-review/'+ full.dealCodeNumber +'" class="tipTop" title="View Comments"><span class="action-icons c-suspend" style="cursor:pointer;"></span></a>\
                    <a href="<?php echo base_url(); ?>admin/order/view_order/'+ user_id +'/'+dealCodeNumber +'" target="popup" onclick="window.open(\'http://localhost/cityfurnish_dev/admin/order/view_order/'+ user_id +'/'+dealCodeNumber +'\',\'popup\',\'width=1100,height=700\')"><span class="action-icons c-suspend tipTop" title="View Invoice" style="cursor:pointer;"></span></a>';


                    return link;
                }
            },
          	
   
        {
            orderable: false, targets: [5,6,7,9],
        }],
        language: {
            searchPlaceholder: "Search"
        },
        fnDrawCallback: function (oSettings) {
        }
    });
});
</script>
<style type="text/css">
	
.dataTables_wrapper .dataTables_paginate .paginate_button {
	margin: 0px 0px 0px -2px !important;
	padding: 0px !important;
}
</style>
<?php 
$this->load->view('admin/templates/footer.php');
?>