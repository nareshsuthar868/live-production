 <?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>

<div id="admin_loader" style="display: none;"><div class="loader-inner"><img src="<?php echo base_url()?>images/admin_loader.gif"></div></div>

<script type="text/javascript">

    function filterGlobal () {
        $('#get_referral_data').DataTable().search(
            $('#global_filter').val(),
        ).draw();
    }

$(function (){
    $("#customer_remark").cleditor();
     $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );

     $( ".selector" ).datepicker({ dateFormat: 'yy-mm-dd' });

 var ref=  $('#get_referral_data').DataTable({
        "processing":  true,
        "serverSide": true,
        ajax: '<?php echo base_url(); ?>admin/order/order_listing',
        "deferRender": true,
        "pagingType": "full_numbers",
        "responsive": true,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('offersDataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('offersDataTables'));
        },
        columns: [
        	{ "data": "id"},
            { "data": "email" },
            { "data": "phone_no"},
           	{ "data": "created"},
           	{ "data": "dealCodeNumber" },
            { "data": "shippingcost" },
            { "data": "total_payout" },
        ],
        columnDefs: [
            {
                 orderable: false, targets: [3],
                "render": function ( data, type, full, meta ) {
                  if(full.payout_date != null){
                    return '<label style="color:green;">'+full.payout_date+'</label>';
                  }else{
                    return data;
                  }
                }
            }, 
            {
                 orderable: false, targets: [6],
                "render": function ( data, type, full, meta ) {
                   if(data == null){
                    return 0;
                   }else{
                    return data;
                   }
                }
            }, 
             {
                 orderable: false, targets: [7],
                "render": function ( data, type, full, meta ) {
                    // if(full.payout_status == null){
            		  return '<button onclick="return ShowPayout(\''+ full.id +'\')" >Payout</button><br><br><button onclick="return ShowOldPayouts(\''+ full.id +'\')" >View Payouts</button>';
                }
            }, 

        {
            orderable: false, targets: [7],
        }],
        language: {
            searchPlaceholder: "Search"
        },
        fnDrawCallback: function (oSettings) {
        }
    });
});
</script>

<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
				
					</div>
					<div class="widget_content">
                        <table class="display sort-table" id="get_referral_data">
                            <thead>
                                <tr>
                                   <th>Order Id</th>
	                                <th>User Email</th>
	                                <th>Phone no</th>
	                                <th>Order/ Payout Date</th>
	                                <th>Transaction ID</th>
                                    <th>Reundable Deposite</th>
	                                <!-- <th>Deduct Amount</th> -->
                                    <th>Paid Amount</th>
                                    <!-- <th>Cashgram ID</th> -->
	                                <!-- <th>Payout Status</th> -->
	                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	 <th>Order Id</th>
	                                <th>User Email</th>
	                                <th>Phone no</th>
	                                <th>Order/ Payout Date</th>
	                                <th>Transaction ID</th>
	                                <th>Reundable Deposite</th>
                                    <!-- <th>Deduct Amount</th> -->
                                    <th>Paid Amount</th>
                                    <!-- <th>Cashgram ID</th> -->
	                                <!-- <th>Payout Status</th> -->
	                                <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>


					</div>
				</div>
			</div>		
		</div>
		<span class="clear"></span>
	</div>
</div>
<button type="button" class="trigger" style="display: none;">View</button>
    <div class="modal refralmodal" id="model_css">
        <div class="modal-content"  >
            <span class="close-button">&times;</span>
            <h1>Payout</h1><br>
            <form  class="form_container left_label" id="create_payout" name="create_payout">
                <ul class="payout_form_ul">
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="full_name">User Name<span class="req">*</span></label>
                            <div class="form_input">
                                <input name="full_name" id="full_name" type="text" tabindex="1" class="required large tipTop">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="email">User Email<span class="req">*</span></label>
                            <div class="form_input">
                                <input name="email" id="email" type="text" tabindex="1" class="required large tipTop">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="mobile">Mobile Number<span class="req">*</span></label>
                            <div class="form_input">
                                <input name="mobile_number" id="mobile_number" type="text" tabindex="1" class="required large tipTop" >
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="full_name">Refundable Amount</label>
                            <div class="form_input">
                                <input name="total_shipping_amount" id="total_shipping_amount" type="text" tabindex="1" class="required large tipTop" readonly>
                                <!-- <label id="total_shipping_amount">1100</label> -->
                            </div>
                        </div>
                    </li>
               <!--      <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="full_name">Deduction Amount <span class="req">*</span></label>
                            <div class="form_input">
                                <input onkeyup="change_shipping_cost(this.value)" name="deduct_amount" id="deduct_amount" type="text" tabindex="1" class="required large tipTop" >
                            </div>
                        </div>
                    </li> -->
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="full_name">Payout Amount</label>
                            <div class="form_input">
                                <input onkeyup="change_shipping_cost(this.value)" name="refund_amount" id="refund_amount" type="text" tabindex="1" class="required large tipTop" >
                                <!-- <label id="refund_amount">0</label> -->
                                <!-- <input name="full_name" id="full_name" type="text" tabindex="1" class="required large tipTop" value="500"> -->
                            </div>
                            <label style="color: red;" id="invlid_amont"></label>
                        </div>
                    </li>
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="full_name">Link Expire Date</label>
                            <div class="form_input">
                                <!-- <input type="date" id="link_expire_date_label" class="required selector" > -->
                                <label id="link_expire_date_label"></label>
                                <input name="order_id" id="order_id" type="hidden">
                                <input name="link_expire_date" id="link_expire_date" type="hidden">
                                <input name="user_id" id="user_id" type="hidden">
                                <input name="cashgramId" id="cashgramId" type="hidden">
                                <input name="payout_status" id="payout_status" type="hidden">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="full_name">Customer Remark</label>
                            <!--<div class="form_input">-->
                                <textarea  placeholder="Write something here..." name="customer_remark" id="customer_remark" type="text" tabindex="1" class="required large tipTop" ></textarea>
                                <!-- <input name="full_name" id="full_name" type="text" tabindex="1" class="required large tipTop" value="500"> -->
                            <!--</div>-->
                        </div>
                    </li> 
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="full_name">Notify Customer</label>
                            <div class="form_input">
                               <input type="radio" id="notify" name="notify" value="1" checked="">Yes
                               <input type="radio" id="notify" name="notify" value="0">No
                            </div>
                        </div>
                    </li>             
                    <li id="new_payout">
                        <div class="form_grid_12 cashgram_button"  style="text-align: right;">
                            <input type="button" onclick="Create_payout()" id="cashgram_button" class="btn-check" value="Create Payout" />
                            <input type="hidden" id="resend" name="resend">
                        </div>
                    </li>
                 <!--    <li id="resend_payout" style="display: none;">
                        <div class="form_grid_12 sama_cashgram_button">
                            <input type="button" onclick="ResendCashgramLink()" id="sama_cashgram_button" class="btn-check" value="Resend" />
                        </div>
                    </li> -->
                    <li>
                        <div class="form_grid_12" id="response"   style="display: none;">
                            Status Code :<label id="response_code"></label><br>
                            Message :<label id="response_message"></label><br>
                            Status :<label id="response_status"></label><br>                            
                        </div>
                    </li>
                </ul>

                <div class="show_all_payouts">
                   <table>
                       <thead>
                           <tr>
                               <th>Order ID</th>
                               <th>Payout Date</th>
                               <th>Shipping Amount</th>
                               <th>Payout Amount</th>
                               <th>Cashgram ID</th>
                               <th>Payout Status</th>
                               <th>Action</th>
                           </tr>
                       </thead>
                       <tbody id="payout_table_body">
                           
                       </tbody>
                   </table>
                </div>
            </form>
        </div>
    </div>


    <!-- <button type="button" class="trigger" style="display: none;">View</button> -->
 <!--    <div class="modal refralmodal" id="status_model">
        <div class="modal-content">
            <span class="close-button">&times;</span>
            <h1>Payout</h1><br>
        </div>
    </div> -->






 <style type="text/css">

    .modal {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        opacity: 0;
        visibility: hidden;
        transform: scale(1.1);
        transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;
    }
    .modal-content {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: white;
        padding: 1rem 1.5rem;
        width: 40rem;
        border-radius: 0.5rem;
    }
    .close-button {
        float: right;
        width: 1.5rem;
        line-height: 1.5rem;
        text-align: center;
        cursor: pointer;
        border-radius: 0.25rem;
        background-color: lightgray;
    }
    .close-button:hover {
        background-color: darkgray;
    }
    .show-modal {
        opacity: 1;
        visibility: visible;
        transform: scale(1.0);
        transition: visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s;
    }
    </style>
    <style>
		table {
		    font-family: arial, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		}

		td, th {
		    border: 1px solid #dddddd;
		    text-align: center;
		    padding: 8px;
		}

		tr:nth-child(even) {
		    background-color: #dddddd;
		}
		@media only screen and (max-width: 1366px) {
			.refralmodal{
				overflow: auto;
			}
			.refralmodal .modal-content{
				transform: translate(-50%, 0);
				top: 20px;
			} 
			.modal-open{
				overflow: hidden;
			}
		}

</style>

    <script type="text/javascript">
    	 var modal = document.querySelector(".modal");
    	var trigger = document.querySelector(".trigger");
    	var closeButton = document.querySelector(".close-button");

	    function toggleModal() {
	        modal.classList.toggle("show-modal"); 
	        document.getElementsByTagName('body')[0].style = 'overflow: auto';
	    }

	    function windowOnClick(event) {
	        if (event.target === modal) {
	            toggleModal();
	        }
	    }

	    trigger.addEventListener("click", toggleModal);
	    closeButton.addEventListener("click", toggleModal);
	    window.addEventListener("click", windowOnClick);
    </script>


<style type="text/css">
.dataTables_wrapper .dataTables_paginate .paginate_button {
	margin: 0px 0px 0px -2px !important;
	padding: 0px !important;
}
</style>


    <?php 
$this->load->view('admin/templates/footer.php');
?>
