<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
					</div>
					<div class="widget_content">
						<table class="display sort-table" id="OfficeOrderListing">
                            <thead>
                                <tr>
	                            	<th class="center">#</th>
									<th>User Name</th>
	                                <th>User Email</th>
	                                <th>User Phone</th>
	                                <th>City</th>
                                    <th>Remark</th>
	                                <th>Order Date</th>
	                                <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	<th class="center">#</th>
									<th>User Name</th>
	                                <th>User Email</th>
	                                <th>User Phone</th>
	                                <th>City</th>
                                    <th>Remark</th>
	                                <th>Order Date</th>
	                                <!-- <th>Action</th> -->
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
				</div>
			</div>
			
		</div>
		<span class="clear"></span>
	</div>
</div>

<script type="text/javascript">
$(function (){

   var bulkOrder =  $('#OfficeOrderListing').DataTable({
        processing:  true,
        serverSide: true,
        ajax: '<?php echo base_url(); ?>admin/order/get_office_funriture_listing',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        order: [],
        columns: [
        	{ "data": "id" },
        	{ "data": "user_name" },
            { "data": "email" },
            { "data": "phone" },
            { "data": "city"},
            { "data": "remark" },
            { "data": "order_date" },
            // { "data": "Action"},
        ],
        columnDefs: [
            {"targets": [0], "className": "center tr_select"},
            // {
            //     orderable: false, targets: [7],
            //     "render": function ( data, type, full, meta ) {
            //     	var buttons = '<span class="action-icons c-suspend"  original-title="Delete" onclick="ViewOfficeFurnitureOrderDetails('+full.id+')"></span>';
                
            //         return buttons;
            //     }
            // },
            {
                orderable: true, targets: [6],
                "render": function ( data, type, full, meta ) {
                    var date = new Date(data);
                    // console.log("here is data",data.getTime());
                    var month = date.getMonth() + 1;
                    var amOrPm = (date.getHours() < 12) ? "AM" : "PM";
                     return  date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear() + "&nbsp;&nbsp;" +(date.getHours() < 10 ? ("0"+date.getHours()) : date.getHours())+ ":"+(date.getMinutes() < 10 ? ("0"+date.getMinutes()) : date.getMinutes()) + ' '+amOrPm ; 
                }
            },
	        {
	            orderable: false, targets: [0],
	        }
        ],
        language: {
            searchPlaceholder: "Search By All"
        },
        fnDrawCallback: function (oSettings) {
        }
    });

      bulkOrder.on( 'order.dt search.dt', function () {
        bulkOrder.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

});


</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>