<?php
$this->load->view('admin/templates/header.php');
?>
<script type="text/javascript">

$(document).ready(function() {
  $("#update_setting").validate({
    rules: {
    	RentalAmount: {
                required:true
            }
    },
    messages: {
    		RentalAmount: {
                required : "<font color='red'>Value cannot be empty.</font>"
            }
    },
    errorElement: "span",
        errorPlacement: function(error, element) {
                error.appendTo(element.parent());
        }

  });
});
</script>

<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Order Setting Form</h6>
					</div>

					<div class="widget_content">

					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'update_setting','enctype'=>"multipart/form-data");
						echo form_open('admin/adminlogin/update_order_setting',$attributes) 
					?>
	 					<ul>
                            <!-- <input type="hidden" name="id" value="<?php echo $setting->id; ?>" /> -->
                             <li>
								<div class="form_grid_12">
									<label class="field_title" for="heading">Minimum Order Value<span class="req">*</span></label>
									<div>
										<input name="RentalAmount" id="RentalAmount" type="number" tabindex="2" class="required small tipTop" title="Please Enter the Value" 
										value="<?php echo $AdminSetting->row()->RentalAmount; ?>" />
									</div>
								</div>
							</li>
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="user_name">Status<span class="req">*</span></label>
									<div>
										<input type="radio" name="OrderRentalStatus" value="enable" <?php if($AdminSetting->row()->OrderRentalStatus == 1){ echo 'checked'; } ?>>Enable
										<input type="radio" name="OrderRentalStatus" value="disable" <?php if($AdminSetting->row()->OrderRentalStatus == 0){ echo 'checked'; } ?>>Disable
									</div>
								</div>
							</li> 
                        <li>
							<div class="form_grid_12">
								<div class="form_input">
										<button type="submit"  id="submit" class="btn_small btn_blue" tabindex="8"><span>Update</span></button>
								</div>
							</div>
						</li>
					</ul>
				</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>