<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
	<div class="grid_container">
		<?php if(isset($_SESSION['error'])) { echo '<h2><font color="red">'.$_SESSION['error'].'</h2></font>'; } ?>
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon blocks_images"></span>
					<h6><?php echo $heading?></h6>
					<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
						<div class="btn_30_light" style="height: 29px;">
							<a href="admin/product/exportProduct" class="tipTop" title="Click here to export product quantity"><span class="icon add_co"></span><span class="btn_link">Export</span></a>
						</div>
					</div>
				</div>
				<div class="widget_content">
					<table class="display sort-table" id="SellerListing">
						<thead>
							<tr>
								<th class="center">Sr No</th>
								<th>Product Name</th>
								<th>Image</th>
								<th>Bangalore Remain</th>
								<th>Delhi Remain</th>
								<th>Ghaziabad/Noida Remain</th>
								<th>Gurgaon Remain</th>
								<th>Mumbai Remain</th>
								<th>Pune Remain</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<th class="center">Sr No</th>
								<th>Product Name</th>
								<th>Image</th>
								<th>Bangalore Remain</th>
								<th>Delhi Remain</th>
								<th>Ghaziabad/Noida Remain</th>
								<th>Gurgaon Remain</th>
								<th>Mumbai Remain</th>
								<th>Pune Remain</th>
								<th>Status</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<input type="hidden" name="statusMode" id="statusMode"/>
		<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>

	</div>
	<span class="clear"></span>
</div>

<style>
#selling_product_tbl tr td{
	border-right:#ccc 1px solid;
}
</style>

<script type="text/javascript">
$(function () {

	var cities = '<?php echo json_encode($cityList->result()); ?>';
	cities = JSON.parse(cities);	

	$('#SellerListing').DataTable({
        "processing": true,
		"serverSide": false,
		"ajax": {
            "url": "<?php echo base_url(); ?>admin/product/get_all_product_listing",
            "type": "POST"
        },		
        "columns": [
            null,
        	{ "data": "product_name" },
			{ "data": "image" },
			{ "data": "city_quantity" },
			{ "data": "city_quantity" },
			{ "data": "city_quantity" },
			{ "data": "city_quantity" },
			{ "data": "city_quantity" },
			{ "data": "city_quantity" },
            { "data": "status" }
		],
		"order": [[9, "asc"]],
		"columnDefs": [
        	{
				orderable: false, targets: [0],
                "render": function ( data, type, full, meta ) {
                    var link  = meta.row + 1;
                    return link;
                }
            },
            {
                orderable: false, targets: [2],
                "render": function ( data, type, full, meta ) {
                    var base_url =  '<?= BASE_URL ?>';
                    var image_url = base_url + "images/product/";
                    var alt_image = image_url + 'dummyProductImage.jpg';
                    var res = data.split(",");
                    if(res.length > 0){
                    	if(res[0] != '') {
                    		var url = image_url + res[0];
                    	} else {
                    	  	var url = alt_image;
                    	}
                    }
                    return '<div class="widget_thumb" style="margin-left: 25%;"><img width="40px" height="40px"   src=\'' + url + '\'></div>';
				}
			},
			{
				orderable: false, targets: [3],
				"render": function ( data, type, full, meta ) {
				    var city = cities[0].id;
					if(data != '0' && data != '') {
						var data = (typeof(data[city]) != "undefined" && data[city] !== null) ? data[city] : '0';
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value='+ data +'><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city+'" data-proid="'+full.id+'">Update</a>';
					} else {
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value="0"><a class="udpate_qty" href="javascript:void(0);" id="'+city+'" style="margin:5px;display:block;"  data-proid="'+full.id+'">Update</a>';						
					}
					return link;
				}
			},
			{
				orderable: false, targets: [4],
				"render": function ( data, type, full, meta ) {
				    var city = cities[1].id;
					if(data != '0' && data != '') {
						var data = (typeof(data[city]) != "undefined" && data[city] !== null) ? data[city] : '0';
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value='+ data +'><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city+'" data-proid="'+full.id+'">Update</a>';
					} else {
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value="0"><a class="udpate_qty" href="javascript:void(0);" id="'+city+'" style="margin:5px;display:block;"  data-proid="'+full.id+'">Update</a>';						
					}
					return link;
				}
			},
			{
				orderable: false, targets: [5],
				"render": function ( data, type, full, meta ) {
				    var city = cities[2].id;
					if(data != '0' && data != '') {
						var data = (typeof(data[city]) != "undefined" && data[city] !== null) ? data[city] : '0';
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value='+ data +'><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city+'" data-proid="'+full.id+'">Update</a>';
					} else {
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value="0"><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city+'" data-proid="'+full.id+'">Update</a>';						
					}
					return link;
				}
			},
			{
				orderable: false, targets: [6],
				"render": function ( data, type, full, meta ) {
				    var city = cities[3].id;
					if(data != '0' && data != '') {
						var data = (typeof(data[city]) != "undefined" && data[city] !== null) ? data[city] : '0';
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value='+ data +'><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city+'" data-proid="'+full.id+'">Update</a>';
					} else {
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value="0"><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city+'" data-proid="'+full.id+'">Update</a>';						
					}
					return link;
				}
			},
			{
				orderable: false, targets: [7],
				"render": function ( data, type, full, meta ) {
				    var city = cities[4].id;
					if(data != '0' && data != '') {
						var data = (typeof(data[city]) != "undefined" && data[city] !== null) ? data[city] : '0';
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value='+ data +'><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city+'" data-proid="'+full.id+'">Update</a>';
					} else {
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value="0"><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city+'" data-proid="'+full.id+'">Update</a>';						
					}
					return link;
				}
			},
			{
				orderable: false, targets: [8],
				"render": function ( data, type, full, meta ) {
				    var city = cities[5].id;
					if(data != '0' && data != '') {
						var data = (typeof(data[city]) != "undefined" && data[city] !== null) ? data[city] : '0';
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value='+ data +'><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city+'" data-proid="'+full.id+'">Update</a>';
					} else {
						var link  = '<input style="width: 50px;" type="number" name="qty" id="qty" min="0" value="0"><a href="javascript:void(0);" class="udpate_qty" style="margin:5px;display:block;" data-proid="'+full.id+'" id="'+city+'">Update</a>';						
					}
					return link;
				}
			}
		]
	});

	$(document).on('click', '.udpate_qty', function(){
		var proId = $(this).data('proid');
		var cityid = $(this).attr('id');
		var qty = $(this).prev('#qty').val();
		
		$.post("<?php echo base_url(); ?>admin/product/update_city_qty", {"qty": qty, "cityid": cityid, "proId": proId}, function(result){

		})
	})

});
</script>

<?php
$this->load->view('admin/templates/footer.php');
?>