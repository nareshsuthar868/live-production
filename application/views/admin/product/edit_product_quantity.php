<?php
$this->load->view('admin/templates/header.php');
?>

<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon list"></span>
					<h6>Edit Product Quantity</h6>
				</div>
				<div class="widget_content">
					<?php
					$attributes = array('class' => 'form_container left_label', 'id' => 'addproduct_form', 'enctype' => 'multipart/form-data');
					echo form_open_multipart('admin/product/EditProductQuantity',$attributes);

					$total = (!empty($product_quantity)) ? array_sum($product_quantity) : '0';
					?>

						<div id="tab1" class="tab_con" data-tab="link_tab1">
							<ul>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="product_name">Product Name <span class="req">*</span></label>
										<div class="form_input">
											<b><?php echo $product_details->row()->product_name;?></b>
										</div>
									</div>
								</li>

								<li>
									<div class="form_grid_12">
										<label class="field_title" for="total_quantity">Total Quantity</label>
										<div class="form_input">
											<b><?php echo ($total > 0) ? $total : '0'; ?></b>
										</div>
									</div>
								</li>

								<?php if ($cityList->num_rows() > 0) {
									foreach ($cityList->result() as $row) { ?>
										<li>
											<div class="form_grid_12">
												<label class="field_title" for="quantity"><?php echo $row->list_value; ?> Quantity</label>
												<div class="form_input">
													<input type="number" min="0" name="quantity[<?php echo $row->id; ?>]" id="quantity" tabindex="6" value="<?php echo (isset($product_quantity[$row->id])) ? $product_quantity[$row->id] : '0'; ?>" class="number large tipTop" title="Please enter the product quantity" />
												</div>
											</div>
										</li>
									<?php }
								} ?>

								<li>
									<div class="form_grid_12">
										<label class="field_title" for="admin_name">Status <span class="req">*</span></label>
										<div class="form_input">
											<div class="publish_unpublish">
												<input type="checkbox" tabindex="12" name="status" <?php if ($product_details->row()->status == 'Publish'){ echo 'checked="checked"';}?> id="publish_unpublish_publish" class="publish_unpublish"/>
											</div>
										</div>
									</div>
								</li>

								<li>
									<div class="form_grid_12">
										<div class="form_input">
											<button type="submit" class="btn_small btn_blue" tabindex="9"><span>Update</span></button>
										</div>
									</div>
								</li>
							</ul>
						</div>

						<input type="hidden" name="productID"  id="productID" value="<?php echo $product_details->row()->id;?>"/>

					</form>
				</div>
			</div>
		</div>
	</div>
	<span class="clear"></span>
</div>

<script>
$(document).ready(function(){

	var i = 1;		
	$('#add').click(function() { 
		$('<div style="float: left; margin: 12px 10px 10px; width:85%;" class="field">'+
			'<div class="image_text" style="float: left;margin: 5px;margin-right:50px;">'+
				'<span>List Name:</span>&nbsp;'+
				'<select name="attribute_name[]" onchange="javascript:loadListValues(this)" style="width:200px;color:gray;width:206px;" class="chzn-select">'+
					'<option value="">--Select--</option>'+
					<?php foreach ($atrributeValue->result() as $attrRow){ 
						if (strtolower($attrRow->attribute_name) != 'price'){
					?>
					'<option value="<?php echo $attrRow->id; ?>"><?php echo $attrRow->attribute_name; ?></option>'+
					<?php }} ?>
					'</select>'+
			'</div>'+
			'<div class="attribute_box attrInput" style="float: left;margin: 5px;" >'+
					'<span>List Value :</span>&nbsp;'+
					'<select name="attribute_val[]" style="width:200px;color:gray;width:206px;" class="chzn-select">'+
					'<option value="">--Select--</option>'+
					'</select>'+
			'</div>'+
		'</div>').fadeIn('slow').appendTo('.inputs');
		i++;
	});
	
	$('#remove').click(function() {
		$('.field:last').remove();
	});
	
	$('#reset').click(function() {
		$('.field').remove();
		$('#add').show();
		i=0;
	});

});
$.validator.addMethod("smallerThan", function (value, element, param) {
    var $element = $(element)
        , $min;

    if (typeof(param) === "string") {
        $min = $(param);
    } else {
        $min = $("#" + $element.data("min"));
    }

    if (this.settings.onfocusout) {
        $min.off(".validate-smallerThan").on("blur.validate-smallerThan", function () {
            $element.valid();
        });
    }
    return parseFloat(value) <= parseFloat($min.val());
}, "Sale price must be smaller than price");
$.validator.addClassRules({
	smallerThan: {
    	smallerThan: true
    }
});
$('#addproduct_form').validate({
	ignore : '',
	errorPlacement: function(error, element) {
	    error.appendTo( element.parent());
	    $('.tab_con').hide();
	    $('#widget_tab ul li a').removeClass('active_tab');
	    var $link_tab = $('label.error:first').parents('.tab_con').show().data('tab');
	    $('.'+$link_tab).addClass('active_tab');
	}
});
</script>

<?php 
$this->load->view('admin/templates/footer.php');
?>