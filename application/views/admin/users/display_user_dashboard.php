<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<script type="text/javascript">
	// alert("dfdffdsfxcv");
$(function (){
    $('#userDashboardlsiting').DataTable({
        processing:  true,
        serverSide: true,
        ajax: '<?php echo base_url(); ?>admin/users/get_user_dashboard_listing',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        pageLength: 8,
        order: [],
        columns: [
            { "data": "full_name" },
            { "data": "user_name"},
            { "data": "email" },
            { "data": "thumbnail" }
        ],
        columnDefs: [
          	{
                 orderable: false, targets: [3],
                "render": function ( data, type, full, meta ) {
                    var base_url =  '<?= BASE_URL?>';
                    var image_url = base_url + "images/";
                    var alt_image = image_url + "user-thumb1.png";
                    if(data == ''){
                        var url = alt_image;
                    }
                    else{
                        var url = image_url + data;
                    }
                    return '<div class="widget_thumb" style="margin-left: 65px;"><img width="40px" height="40px" src=\'' + url + '\'></div>'
                }
            },
	        {
	            orderable: false, targets: [],
	        }
        ],
        language: {
            searchPlaceholder: "Search By Email"
        },
        fnDrawCallback: function (oSettings) {
        }
    });
});


</script>
<div id="content">
	<div class="grid_container">
		<div class="grid_6">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon graph"></span>
					<h6><?php echo $heading;?></h6>
				</div>
				<div class="widget_content">
					<div class="stat_block">
						<h4><?php echo ($active_users->count+$block_users->count);?> users registered in this site</h4>
						<table>
						<tbody>
						<tr>
							<td>
								Active Users
							</td>
							<td>
								<?php echo $active_users->count;?>
							</td>
						</tr>
						<tr>
							<td>
								Inactive Users
							</td>
							<td>
								<?php echo $block_users->count;?>
							</td>
						</tr>
						</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
		<div class="grid_6">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon image_1"></span>
					<h6>Recent Users</h6>
				</div>
				<div class="widget_content">
					<table class="display sort-table" id="userDashboardlsiting">
                        <thead>
                            <tr>
                            	<th>
							 		Full Name
								</th>
								<th>
								 	User Name
								</th>
								<th>
							 		Email
								</th>
								<th>
							 		Thumbnail
								</th>
                            </tr>
                        </thead>
                        <tbody>
                    	</tbody>
                      	<tfoot>
                        	<tr>
                            	<th>
							 		Full Name
								</th>
								<th>
								 	User Name
								</th>
								<th>
							 		Email
								</th>
								<th>
							 		Thumbnail
								</th>
                            </tr>
                        </tfoot>
                    </table>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>


<?php 
$this->load->view('admin/templates/footer.php');
?>

