<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
 <div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/users/change_user_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
						<?php if ($allPrev == '1' || in_array('2', $user)){?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Active','<?php echo $subAdminMail; ?>');" class="tipTop" title="Select any checkbox and click here to active records"><span class="icon accept_co"></span><span class="btn_link">Active</span></a>
							</div>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Inactive','<?php echo $subAdminMail; ?>');" class="tipTop" title="Select any checkbox and click here to inactive records"><span class="icon delete_co"></span><span class="btn_link">Inactive</span></a>
							</div>
						<?php 
						}
						if ($allPrev == '1' || in_array('3', $user)){
						?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Delete','<?php echo $subAdminMail; ?>');" class="tipTop" title="Select any checkbox and click here to delete records"><span class="icon cross_co"></span><span class="btn_link">Delete</span></a>
							</div>
						<?php }?>
						</div>
					</div>
					<div class="widget_content">
						<table class="display" id="userListTbl123">
                            <thead>
                                <tr>
	                            	<th class="center">
										<input name="checkbox_id[]" type="checkbox" value="on" class="selectall">
									</th>
	                                <th>Full Name</th>
	                                <th>User Name</th>
	                                <th>User ID</th>
	                                <th>Email</th>
	                                <th>Thumbnail</th>
	                                <th>Last Login Date</th>
	                                <th>Last Login IP</th>
	                                <th>Status</th>
	                                <th>Is Offline</th>
	                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	<th class="center">
										<input name="checkbox_id[]"  type="checkbox" value="on" class="selectall12">
									</th>
	                                <th>Full Name</th>
	                                <th>User Name</th>
	                                <th>User ID</th>
	                                <th>Email</th>
	                                <th>Thumbnail</th>
	                                <th>Last Login Date</th>
	                                <th>Last Login IP</th>
	                                <th>Status</th>
	                                <th>Is Offline</th>
	                                <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<script type="text/javascript">
$(function(){
	// add multiple select / deselect functionality
	$(".selectall").click(function () {
		  $('.case').attr('checked', this.checked);
		  $('.selectall12').attr('checked', this.checked);
	});
	$(".selectall12").click(function () {
		  $('.case').attr('checked', this.checked);
		   $('.selectall').attr('checked', this.checked);
	});
});

function change_all(){
	if($(".case").length == $(".case:checked").length) {
		$(".selectall").attr("checked", "checked");
		$(".selectall12").attr("checked", "checked");
	} else {
		$(".selectall").removeAttr("checked");
		$(".selectall12").removeAttr("checked", "checked");
	}
}
</script>
<script type="text/javascript">
$(function (){
    $('#userListTbl123').DataTable({
        processing:  true,
        serverSide: true,
        pageLength: 25,
        lengthMenu: [ 10, 25, 50, 75, 100 ],
        ajax: '<?php echo base_url(); ?>admin/users/Get_user_data',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        order: [ 10, "desc" ],
        columns: [
        		null,
            { "data": "full_name" },
            { "data": "user_name"},
            { "data": "id"},
            { "data": "email" },
            { "data": "thumbnail" },
            { "data": "last_login_date" },
            { "data": "last_login_ip"},
            { "data": "status"},
             { "data": "is_offline_user"},
             { "data": "Action"},
        ],
        columnDefs: [
        	{
                 orderable: false, targets: [0],
                "render": function ( data, type, full, meta ) {
                    var link  = '<input name="checkbox_id[]" type="checkbox" value="'+ full.id +'" class="case" onclick="change_all()">';
                    return link;
                }
            },
            {"targets": [0], "className": "center tr_select"},
          	{
                 orderable: false, targets: [5],
                "render": function ( data, type, full, meta ) {
                    var base_url =  '<?= BASE_URL?>';
                    var image_url = base_url + "images/";
                    var alt_image = image_url + "user-thumb1.png";
                    if(data == ''){
                        var url = alt_image;
                    }
                    else{
                        var url = image_url + data;
                    }
                    return '<div class="widget_thumb" style="margin-left: 65px;"><img width="40px" height="40px" src=\'' + url + '\'></div>'
                }
            },
            {
                 orderable: true, targets: [8],
                "render": function ( data, type, full, meta ) {
                	var mode = (data == 'Active')?'0':'1';
                    if(data == 'Active'){
                        var button = '<a title="Click to inactive" class="tip_top" href="javascript:confirm_status(\'admin/users/change_user_status/'+ mode +'/'+ full.id +'\');"><span class="badge_style b_done">'+ data +'</span></a>';
                    }
                    else{
                        var button = '<a title="Click to active" class="tip_top" href="javascript:confirm_status(\'admin/users/change_user_status/'+ mode +'/'+ full.id +'\')"><span class="badge_style">'+ data +'</span></a>';
                    }
                    return button;
                }
            },
            {
                orderable: true, targets: [9],
                "render": function ( data, type, full, meta ) {
                    if(data == 1){
                        var button = '<label>Yes</label>';
                    }
                    else{
                        var button = '<label>No</label>';
                    }
                    return button;
                }
            },
            {
                 orderable: false, targets: [10],
                "render": function ( data, type, full, meta ) {
                    var button = '<span><a class="action-icons c-edit" href="admin/users/edit_user_form/'+ full.id + '" title="Edit">Edit</a></span>';
                    button += '<span><a class="action-icons c-suspend" href="admin/users/view_user/'+ full.id +'" title="View">View</a></span>';
                    button += '<span><a class="action-icons c-delete" href="javascript:confirm_delete(\'admin/users/delete_user/'+  full.id +  '\')" title="Delete">Delete</a></span>'
                    return button;
                }
            },
            
   
        {
            orderable: false, targets: [0,5,6,7],
        }],
        language: {
            searchPlaceholder: "Search By Email"
        },
        fnDrawCallback: function (oSettings) {
        }
    });
});
</script>
<style type="text/css">
	
.dataTables_wrapper .dataTables_paginate .paginate_button {
	margin: 0px 0px 0px -2px !important;
	padding: 0px !important;
}
</style>
<?php 
$this->load->view('admin/templates/footer.php');
?>