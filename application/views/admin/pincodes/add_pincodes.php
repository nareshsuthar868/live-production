<?php
$this->load->view('admin/templates/header.php');
?>

<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add New Pin Code</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form');
						echo form_open('admin/pincodes/insertEditpincode',$attributes) 
					?>
	 						<ul>
                            <input type="hidden" name="pincode_id" value="" />
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="user_name">Pin code <span class="req">*</span></label>
									<div class="form_input">
										<input name="pincode" id="pincode" type="text" tabindex="2" class="required small tipTop" title="Please Enter the Pin Code" />
									</div>
								</div>
								</li>                                                               
                                
                                <li>
								<div class="form_grid_12">
                                    <label class="field_title">Select a City</label>
									<div class="form_input">
									<select data-placeholder="Select a Coupon Type" name="city" style=" width:300px" class="chzn-select-deselect" tabindex="13">
										<option value="">None</option>
									<?php foreach($listv as $row){ ?>                                      
										<option value="<?php echo $row->id; ?>"><?php echo $row->list_value; ?></option>
									<?php } ?>
									</select>
									</div>
								</div>
								</li>
                                </ul>
                                								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="8"><span>Submit</span></button>
									</div>
								</div>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>