<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>


<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon blocks_images"></span>
					<h6>Date Filter</h6>
				</div>
				<div class="widget_content">
				        <label>Date From</label> : <input type="text" name="start_date" id="start_date" class="form-control  input-daterange" />
						<label>Date To</label> : <input type="text" name="end_date" id="end_date" class="form-control  input-daterange" />
						<input type="button" name="search" id="search" value="Filter" class="btn btn-info" />
				</div>
			</div>
		</div>
	</div>
	<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
						<table  class="display" id="userListTbl123">
							<thead>
								<tr>
									<th class="center">Order ID</th>
									<th class="tip_top">Case Status</th>
									<th class="tip_top">Case Sub Status</th>
									<th>Case Owner</th>
									<th>TAT Status</th>
									<th>History</th>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
							<tfoot>
								<tr>
									<th class="center">Order ID</th>
									<th class="tip_top">Case Status</th>
									<th class="tip_top">Case Sub Status</th>
									<th>Case Owner</th>
									<th>TAT Status</th>
									<th>History</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
	</div>
	<span class="clear"></span>
</div>
</div>

<button type="button" class="trigger" style="display: none;">View</button>
    <div class="modal refralmodal" id="model_css" >
        <div class="modal-content">
		        <span class="close-button">&times;</span>
        	<div id="defaultListing">
		        <div><h3>Order History <span id="orderID"></span></h3></div>
		        <div class="table-wrap">
    				<table id="customers">
    					<thead>
    						<th>Status From</th>
    						<th>Status To</th>
    						<th>Case Owner</th>
    						<th>Status In Time</th>	
    						<th>Status Out Time</th>
    						<th>Total Diff</th>
    					</thead>
    					<tbody id="orderlistingdynamic">
    					</tbody>
    				</table>
				</div>
        	</div>
		</div>
    </div>

<style type="text/css">
    .modal {position: fixed; left: 0;top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); opacity: 0;visibility: hidden;transform: scale(1.1);transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;}
    .modal-content {position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; padding: 1rem 1.5rem;width: 42rem; border-radius: 0.5rem;}
    .close-button {float: right;width: 1.5rem;line-height: 1.5rem;text-align: center;cursor: pointer;border-radius: 0.25rem;background-color: lightgray; }
    .close-button:hover { background-color: darkgray; }
    .show-modal { opacity: 1; visibility: visible;transform: scale(1.0);transition: visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s;}
	.modal-content { width: 50rem !important; height:auto; overflow: scroll; max-height: 90%; }
	.w3-badge,.w3-tag{background-color:#000;color:#fff;display:inline-block;padding-left:8px;padding-right:8px;text-align:center;width: 18px;padding-top: 9px;height: 21px;}.w3-badge{border-radius:50%}
	.w3-red,.w3-hover-red:hover{color:#fff!important;background-color:#f44336!important}
	.w3-green,.w3-hover-red:hover{color:#fff!important;background-color: #4CAF50!important;}
	/*#customers {font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; border-collapse: collapse;width: 100%;}*/
	#customers td, #customers th {border: 1px solid #ddd;padding: 8px;}
	#customers tr:nth-child(even){background-color: #f2f2f2;}
	#customers tr:hover {background-color: #ddd;}
    #customers th {padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #4CAF50;color: white;position: -webkit-sticky;position: sticky;top: 0;}
	.table-wrap {max-width: 100%;max-height: 630px;overflow-y: scroll;}
</style>


<script type="text/javascript">
    $(function (){
		$('.input-daterange').datepicker({
		  todayBtn:'linked',
		  format: "Y-m-d",
		  autoclose: true
	 	});
		fetch_data('no');
		function fetch_data(is_date_search, start_date='', end_date=''){
		   	var oTable = '';
	    	oTable = $('#userListTbl123').DataTable({
		        processing:  true,
		        serverSide: true,
		        searching: false,
	          	"ajax": {
		            "url": "<?php echo base_url(); ?>admin/orderdashboard/getorderhistorylist",
		        	"data":{"is_date_search":is_date_search, "start_date":start_date, "end_date":end_date},
		            "type": "POST"
		        },
		        columns: [
				    { "data": "order_id"},
		           	{ "data": "new_status"},
		           	{ "data": "sub_status"},
		           	{ "data": "owner_name"},
		           	{ "data": "status"},
		           	null
		        ],
		        columnDefs: [
		        {
		                 orderable: false, targets: [0],
		                "render": function ( data, type, full, meta ) {
		                    var order_id  = '#'+data;
		                    return order_id;
		                }
		       	},
		       	{
		                 orderable: false, targets: [5],
		                "render": function ( data, type, full, meta ) {
	                      var button = '<a title="Click to active" class="action-icons c-edit" href="javascript:void(0)" onclick=showhistory('+full.order_id+')>History</a>';
		                    return button;
		                }
		       	},
		        {
		            orderable: false, targets: [0],
		        }],
		        language: {
		            searchPlaceholder: "Search"
		        },
		        fnDrawCallback: function (oSettings) {
		        },

	   		});	  
 	   	}

	 	$('#search').click(function(){
		  	var start_date = $('#start_date').val();
		  	var end_date = $('#end_date').val();
		  	if(start_date != '' && end_date !='')
		  	{
			   $('#userListTbl123').DataTable().destroy();
			   fetch_data('yes', start_date, end_date);
		  	}
		  	else{
			   alert("Both Date is Required");
		  	}
	 	}); 
	});
	function showhistory(order_id){
	   	$.ajax({
	        type: 'post',
	        url: baseURL + 'admin/orderdashboard/getorderhistory',
	        dataType: 'json',
	        data: {'order_id':order_id},
	        success: function(json) {
	        	$('#model_css').addClass('show-modal');
	        	$('#orderID').html('#'+order_id);
	        	DynamicHtml = '';
	        	for (var i = 0; i < json.length; i++) {
	        		DynamicHtml += '<tr>\
	        		<td>'+ json[i].status_from +'</td>\
	        		<td>'+ json[i].status_to +'</td>\
	        		<td>'+ json[i].case_owner +'</td>\
	        		<td>'+ json[i].status_in_time +'</td>\
	        		<td>'+ json[i].status_out_time +'</td>\
	        		<td>'+ json[i].hours_diff +'</td>\
	        		</tr>';
	        	}
	        	$('#orderlistingdynamic').html(DynamicHtml);
	        }
        });
	}
	// function updatetatvalue(id){
	// 	var value = $('#tat_duration_'+id).val();
	// 	// alert(value);
	// }
</script>


    <script type="text/javascript">
    	 var modal = document.querySelector(".modal");
    	var trigger = document.querySelector(".trigger");
    	var closeButton = document.querySelector(".close-button");

	    function toggleModal() {
	        modal.classList.toggle("show-modal"); 
	        document.getElementsByTagName('body')[0].style = 'overflow: auto';
	    }

	    function windowOnClick(event) {
	        if (event.target === modal) {
	            toggleModal();
	        }
	    }

	    trigger.addEventListener("click", toggleModal);
	    closeButton.addEventListener("click", toggleModal);
	    window.addEventListener("click", windowOnClick);
    </script>


<?php
$this->load->view('admin/templates/footer.php');
?>