<?php
$this->load->view('admin/templates/header.php');
?>
<!-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> -->
<div id="admin_loader" style="display: none;"><div class="loader-inner"><img src="<?php echo base_url()?>images/admin_loader.gif"></div></div>
<div id="content">
		<div class="grid_container">
		<!-- 	<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_content">
					
					</div>
				</div>
			</div> -->
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
						<table class="display" id="customers">
							<thead>
							<tr>
								<th class="tip_top">
									Order Status
								</th>
								<th class="tip_top">
									 Order SubStatus
								</th>
								<th class="tip_top">
									In TAT
								</th>
								<th class="tip_top">
									Out of TAT
								</th>
								<th class="tip_top">
									Total
								</th>

							</tr>
							</thead>
							<tbody>
								<tr>
									<td><b>New Order</b></td>	
									<td>KYC In Progress</td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("New Order",<?php echo json_encode($new_order['inTan']); ?>,"inTan","Document Complete");'><span class="w3-badge w3-green"><?php echo count($new_order['inTan']); ?></span></a></td>	
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("New Order",<?php echo json_encode($new_order['OutTan']); ?>,"OutTan","Document Complete");'><span class="w3-badge w3-red"><?php echo count($new_order['OutTan']); ?></span></a></td>
									<td>
										<a href="javascript:void(0);" title="Show Details" onclick='Showtatforall("New Order",<?php echo json_encode($new_order); ?>,"Document Complete");'>
											<span class="w3-badge w3-green"><?php echo count($new_order['OutTan']) + count($new_order['inTan']);  ?></span>
										</a>
									</td>
								</tr>
								<tr>
									<td><b>Document Complete</b></td>
									<td>KYC Completed</td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Document Complete",<?php echo json_encode($document_complete['inTan']); ?>,"inTan","Invoice Created");'><span class="w3-badge w3-green"><?php echo  count($document_complete['inTan']); ?></span></a></td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Document Complete",<?php echo json_encode($document_complete['OutTan']); ?>,"OutTan","Invoice Created");'><span class="w3-badge w3-red"><?php echo  count($document_complete['OutTan']); ?></span></a></td>
									<td>
										<a href="javascript:void(0);" title="Show Details" onclick='Showtatforall("Document Complete",<?php echo json_encode($document_complete); ?>,"Invoice Created");'>
											<span class="w3-badge w3-green"><?php echo count($document_complete['OutTan']) + count($document_complete['inTan']);  ?></span>
										</a>
									</td>
								</tr>

								<tr>
									<td><b>Invoice Created</b></td>
									<td>KYC Completed</td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Invoice Created",<?php echo json_encode($invoice_created['inTan']); ?>,"inTan","Delivery Scheduled");'><span class="w3-badge w3-green"><?php echo count($invoice_created['inTan']); ?></span></a></td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Invoice Created",<?php echo json_encode($invoice_created['OutTan']); ?>,"OutTan","Delivery Scheduled");'><span class="w3-badge w3-red"><?php echo count($invoice_created['OutTan']); ?></span></a></td>
									<td>
										<a href="javascript:void(0);" title="Show Details" onclick='Showtatforall("Invoice Created",<?php echo json_encode($invoice_created); ?>,"Delivery Scheduled");'>
											<span class="w3-badge w3-green"><?php echo count($invoice_created['OutTan']) + count($invoice_created['inTan']);  ?></span>
										</a>
									</td>
								</tr>
								<tr>
									<td><b>Delivery Scheduled</b></td>
									<td>Delivery Scheduled</td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Delivery Scheduled",<?php echo json_encode($delivery_scheduled['inTan']); ?>,"inTan","Delivery Done");'><span class="w3-badge w3-green"><?php echo count($delivery_scheduled['inTan']); ?></span></a></td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Delivery Scheduled",<?php echo json_encode($delivery_scheduled['OutTan']); ?>,"OutTan","Delivery Done");'><span class="w3-badge w3-red"><?php echo count($delivery_scheduled['OutTan']); ?></span></a></td>
									<td>
										<a href="javascript:void(0);" title="Show Details" onclick='Showtatforall("Delivery Scheduled",<?php echo json_encode($delivery_scheduled); ?>,"Delivery Done");'>
										<span class="w3-badge w3-green"><?php echo count($delivery_scheduled['OutTan']) + count($delivery_scheduled['inTan']);  ?></span>
										</a>
									</td>
								</tr>
								<tr>
									<td><b>Delivery Done</b></td>
									<td>Delivered</td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Delivery Done",<?php echo json_encode($delivery_done['inTan']); ?>,"inTan","Payment Doc Proof Pending");'><span class="w3-badge w3-green"><?php echo count($delivery_done['inTan']); ?></span></a></td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Delivery Done",<?php echo json_encode($delivery_done['OutTan']); ?>,"OutTan","Payment Doc Proof Pending");'><span class="w3-badge w3-red"><?php echo count($delivery_done['OutTan']); ?></span></a></td>
									<td>
										<a href="javascript:void(0);" title="Show Details" onclick='Showtatforall("Delivery Done",<?php echo json_encode($delivery_done); ?>,"Payment Doc Proof Pending");'>
											<span class="w3-badge w3-green"><?php echo count($delivery_done['OutTan']) + count($delivery_done['inTan']);  ?></span>
										</a>
									</td>
								</tr>
								<tr>
									<td><b>Payment Doc Proof Pending</b></td>
									<td>Delivered</td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Payment Doc Proof Pending",<?php echo json_encode($payment_doc_proof_pending['inTan']); ?>,"inTan","Payment Doc Validation Pending");'><span class="w3-badge w3-green"><?php echo count($payment_doc_proof_pending['inTan']); ?></span></a></td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Payment Doc Proof Pending",<?php echo json_encode($payment_doc_proof_pending['OutTan']); ?>,"OutTan","Payment Doc Validation Pending");'><span class="w3-badge w3-red"><?php echo count($payment_doc_proof_pending['OutTan']); ?></span></a></td>
									<td>
										<a href="javascript:void(0);" title="Show Details" onclick='Showtatforall("Payment Doc Proof Pending",<?php echo json_encode($payment_doc_proof_pending); ?>,"Payment Doc Validation Pending");'>
											<span class="w3-badge w3-green"><?php echo count($payment_doc_proof_pending['OutTan']) + count($payment_doc_proof_pending['inTan']);  ?></span>
										</a>
									</td>
								</tr>
								<tr>
									<td><b>Payment Doc Validation Pending</b></td>
									<td>Delivered</td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Payment Doc Validation Pending",<?php echo json_encode($payment_doc_validation_pending['inTan']); ?>,"inTan","Payment Doc Dispatch Pending");'><span class="w3-badge w3-green"><?php echo count($payment_doc_validation_pending['inTan']); ?></span></a></td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Payment Doc Validation Pending",<?php echo json_encode($payment_doc_validation_pending['OutTan']); ?>,"OutTan","Payment Doc Dispatch Pending");'><span class="w3-badge w3-red"><?php echo count($payment_doc_validation_pending['OutTan']); ?></span></a></td>
									<td>
										<a href="javascript:void(0);" title="Show Details" onclick='Showtatforall("Payment Doc Validation Pending",<?php echo json_encode($payment_doc_validation_pending); ?>,"Payment Doc Dispatch Pending");'>
											<span class="w3-badge w3-green"><?php echo count($payment_doc_validation_pending['OutTan']) + count($payment_doc_validation_pending['inTan']);  ?></span>
										</a>
									</td>
								</tr>
								<tr>
									<td><b>Payment Doc Dispatch Pending</b></td>
									<td>Delivered</td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Payment Doc Dispatch Pending",<?php echo json_encode($payment_doc_dispatch_pending['inTan']); ?>,"inTan","Closed");'><span class="w3-badge w3-green"><?php echo count($payment_doc_dispatch_pending['inTan']); ?></span></a></td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Payment Doc Dispatch Pending",<?php echo json_encode($payment_doc_dispatch_pending['OutTan']); ?>,"OutTan","Closed");'><span class="w3-badge w3-red"><?php echo count($payment_doc_dispatch_pending['OutTan']); ?></span></a></td>
									<td>
										<a href="javascript:void(0);" title="Show Details" onclick='Showtatforall("Payment Doc Dispatch Pending",<?php echo json_encode($payment_doc_dispatch_pending); ?>,"Closed");'>
											<span class="w3-badge w3-green"><?php echo count($payment_doc_dispatch_pending['OutTan']) + count($payment_doc_dispatch_pending['inTan']);  ?></span>
										</a>
									</td>
								</tr>
						<!-- 		<tr>
									<td><b>Closed</b></td>
									<td>Resolved / Delivered / Cancelled</td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Closed",<?php echo json_encode($Closed['inTan']); ?>,"inTan");'><span class="w3-badge w3-green"><?php echo count($Closed['inTan']); ?></span></a></td>
									<td><a href="javascript:void(0);" title="Show Details" onclick='OrderCountBody("Closed",<?php echo json_encode($Closed['OutTan']); ?>,"OutTan");'><span class="w3-badge w3-red"><?php echo count($Closed['OutTan']); ?></span></a></td>
									<td><span class="w3-badge w3-green"><?php echo count($Closed['OutTan']) + count($Closed['inTan']);  ?></span></td>
								</tr> -->
							</tbody>
							<tbody>
							
							</tbody>
							<tfoot>
							<tr>
								<th class="tip_top">
									Order Status
								</th>
								<th class="tip_top">
									 Order SubStatus
								</th>
								<th class="tip_top">
									In Tat
								</th>
								<th class="tip_top">
									Out of Tat
								</th>
								<th class="tip_top">
									Total
								</th>


							</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<button type="button" class="trigger" style="display: none;">View</button>
    <div class="modal refralmodal" id="model_css">
        <div class="modal-content">
		        <span class="close-button">&times;</span>
        	<div id="defaultListing">
		        <div><h3>Order Listing</h3></div>
		        <div class="table-wrap">
    				<table id="customers">
    					<thead>
    						<th>Order ID</th>
    						<th>Status In Time</th>
    						<th>Status Out Time</th>
    						<th>Total Hours Difference</th>	
    						<th>Case Owner</th>
    					</thead>
    					<tbody id="orderlistingdynamic">
    					</tbody>
    				</table>
				</div>
        	</div>
		</div>
    </div>

<style type="text/css">
    .modal {position: fixed; left: 0;top: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); opacity: 0;visibility: hidden;transform: scale(1.1);transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;}
    .modal-content {position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; padding: 1rem 1.5rem;width: 42rem; border-radius: 0.5rem;}
    .close-button {float: right;width: 1.5rem;line-height: 1.5rem;text-align: center;cursor: pointer;border-radius: 0.25rem;background-color: lightgray; }
    .close-button:hover { background-color: darkgray; }
    .show-modal { opacity: 1; visibility: visible;transform: scale(1.0);transition: visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s;}
	.modal-content { width: 48rem !important; height:auto; overflow: scroll; max-height: 90%; }
	.w3-badge,.w3-tag{background-color:#000;color:#fff;display:inline-block;padding-left:8px;padding-right:8px;text-align:center;width: 18px;padding-top: 9px;height: 21px;}.w3-badge{border-radius:50%}
	.w3-red,.w3-hover-red:hover{color:#fff!important;background-color:#f44336!important}
	.w3-green,.w3-hover-red:hover{color:#fff!important;background-color: #4CAF50!important;}
	/*#customers {font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; border-collapse: collapse;width: 100%;}*/
	#customers td, #customers th {border: 1px solid #ddd;padding: 8px;}
	#customers tr:nth-child(even){background-color: #f2f2f2;}
	#customers tr:hover {background-color: #ddd;}
	#customers th {padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #4CAF50;color: white;position: -webkit-sticky;position: sticky;top: 0;}
	.table-wrap {max-width: 100%;max-height: 630px;overflow-y: scroll;}
</style>
<script type="text/javascript">
	function OrderCountBody(status,dataArray = '',type="",oldStatus = null){	
        $('#model_css').addClass('show-modal');

       	$.ajax({
	        type: 'POST',
	        url: baseURL + 'admin/orderdashboard/get_orderDetails',
	        dataType: 'json',
	        data: {
	            'array': dataArray,
	            'type': type,
	            'oldStatus':oldStatus,
	            'status': status
            },
            beforeSend:function(){
                 $('#admin_loader').show();
            },
            success:function(response){
                $('#admin_loader').hide();
            	var DyanicHtml = '';
            	if(response.length > 0){
	            	for (var i = 0; i < response.length; i++) {
	            		DyanicHtml += '<tr><td>#'+ response[i].order_id +'</td><td>'+ response[i].invoice_date +'</td><td>'+ response[i].updated_date +'</td><td>'+response[i].interval +'</td><td>'+ response[i].owner_name+'</td></tr>';	
	            	}
            		$('#orderlistingdynamic').html(DyanicHtml);
            	}else{
            		$('#orderlistingdynamic').html('<tr><td colspan="6" class="no-data-available">No data!</td></tr>');
            	}
            	// console.log("here is your repsonse",DyanicHtml);
            }
        }); 
	}


	function Showtatforall(status,tatData,oldStatus = null){
		$('#model_css').addClass('show-modal');
       	$.ajax({
	        type: 'POST',
	        url: baseURL + 'admin/orderdashboard/get_bulkDetails',
	        dataType: 'json',
	        data: {
	            'array': tatData,
	            'oldStatus':oldStatus,
	            'status': status
            },
            beforeSend:function(){
                 $('#admin_loader').show();
            },
            success:function(response){
                $('#admin_loader').hide();
            	var DyanicHtml = '';
            	if(response.length > 0){
	            	for (var i = 0; i < response.length; i++) {
	            		DyanicHtml += '<tr><td>#'+ response[i].order_id +'</td><td>'+ response[i].invoice_date +'</td><td>'+ response[i].updated_date +'</td><td>'+response[i].interval +'</td><td>'+ response[i].owner_name+'</td></tr>';	
	            	}
            		$('#orderlistingdynamic').html(DyanicHtml);
            	}else{
            		$('#orderlistingdynamic').html('<tr><td colspan="6" class="no-data-available">No data!</td></tr>');
            	}
            	// console.log("here is your repsonse",DyanicHtml);
            }
        }); 

	}


</script>
     <script type="text/javascript">


	 	var modal = document.querySelector(".modal");
    	var trigger = document.querySelector(".trigger");
    	var closeButton = document.querySelector(".close-button");

	    function toggleModal() {
	        modal.classList.toggle("show-modal"); 
	        document.getElementsByTagName('body')[0].style = 'overflow: auto';
	    }
	    function windowOnClick(event) {
	        if (event.target === modal) {
	            toggleModal();
	        }
	    }
	    trigger.addEventListener("click", toggleModal);
	    closeButton.addEventListener("click", toggleModal);
	    window.addEventListener("click", windowOnClick);
    </script>



<?php 
$this->load->view('admin/templates/footer.php');
?>