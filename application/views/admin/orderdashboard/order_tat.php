<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
	<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
						<table  class="display" id="customers">
							<thead>
								<tr>
									<th class="tip_top">Status</th>
									<th class="center">Time Period</th>
									<th>Last Update On</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ($tatConfiguration->num_rows() > 0) {
									foreach ($tatConfiguration->result() as $row) {
								?>
								<tr>
									<td class="center tr_select">
										<?php echo $row->status_from; ?>
									</td>
									<td class="center tr_select">
										<div id="static_days_<?php echo $row->id; ?>">
											<?php echo $row->duration.' Hours'; ?>
										</div>
										<div style="display: none;" id="days_box_<?php echo $row->id; ?>">
											<input type="number" id="tat_duration_<?php echo $row->id; ?>" name="tat_duration_<?php echo $row->id; ?>"  value="<?php echo $row->duration;  ?>" style="width: 30px;">Hours
											<button onclick="updatetatvalue('<?php echo $row->id; ?>')">Update</button>
										</div>
									</td>
									<td class="center">
										<?php  if($row->updated_date != '0000-00-00 00:00:00') { echo date('Y-m-d h:i a',strtotime($row->updated_date)); } ?>
									</td>
									<td class="center">
										<?php if ($allPrev == '1' || in_array('2', $order_report)){?>
											<span><a class="action-icons c-edit" href="javascript:void(0)"" title="Edit" onclick="editturnaroundtime('<?php echo $row->id; ?>')">Edit</a></span>
										<?php } ?>
									</td>
								</tr>
								<?php
									}
								}
								?>
							</tbody>
							<tfoot>
								<tr>
									<th class="tip_top">Status</th>
									<th class="center">Time Period</th>
									<th>Last Update On</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
	</div>
	<span class="clear"></span>
</div>
</div>

<style type="text/css">
	.w3-badge,.w3-tag{background-color:#000;color:#fff;display:inline-block;padding-left:8px;padding-right:8px;text-align:center;width: 18px;padding-top: 9px;height: 21px;}.w3-badge{border-radius:50%}
	.w3-red,.w3-hover-red:hover{color:#fff!important;background-color:#f44336!important}
	.w3-green,.w3-hover-red:hover{color:#fff!important;background-color: #4CAF50!important;}
	/*#customers {font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; border-collapse: collapse;width: 100%;}*/
	#customers td, #customers th {border: 1px solid #ddd;padding: 8px;}
	#customers tr:nth-child(even){background-color: #f2f2f2;}
	#customers tr:hover {background-color: #ddd;}
	#customers th {padding-top: 12px;padding-bottom: 12px;background-color: #4CAF50;color: white;}
</style>


<script type="text/javascript">
	$('#bulkOrder_tbl').DataTable();
	function editturnaroundtime(id){
		$('#static_days_'+id).hide();
		$('#days_box_'+id).show();
		// alert(id);
	}
	function updatetatvalue(id){
		var value = $('#tat_duration_'+id).val();
		// alert(value);
	   	$.ajax({
	        type: 'post',
	        url: baseURL + 'admin/orderdashboard/updatetatvalue',
	        dataType: 'json',
	        data: {'id':id,'tat':value},
	        success: function(json) {
	           		$('#static_days_'+id).show();
					$('#days_box_'+id).hide();
					$('#static_days_'+id).html(value+' Hours');
	        },
        });
	}
</script>

<?php
$this->load->view('admin/templates/footer.php');
?>