<?php
$this->load->view('admin/templates/header.php');
?>
<script>
$(document).ready(function(){
	$('.nxtTab').click(function(){
		var cur = $(this).parent().parent().parent().parent().parent();
		cur.hide();
		cur.next().show();
		var tab = cur.parent().parent().prev();
		tab.find('a.active_tab').removeClass('active_tab').parent().next().find('a').addClass('active_tab');
	});
	$('.prvTab').click(function(){
		var cur = $(this).parent().parent().parent().parent().parent();
		cur.hide();
		cur.prev().show();
		var tab = cur.parent().parent().prev();
		tab.find('a.active_tab').removeClass('active_tab').parent().prev().find('a').addClass('active_tab');
	});
});
</script>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Edit Category</h6>
                        <div id="widget_tab">
							<ul>
								<li><a href="#tab1" class="active_tab">Content</a></li>
								<li><a href="#tab2">SEO</a></li>
								<li><a href="#tab3">City wise content</a></li>
							</ul>
            			</div>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'editcategory_form',  'enctype' => 'multipart/form-data');
						echo form_open_multipart('admin/category/EditCategory',$attributes) 
					?>
					<div id="tab1" class="tab_con" data-tab="link_tab1">
	 						<ul>
	 							
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="category_name">Category Name <span class="req">*</span></label>
									<div class="form_input">
										<input name="category_name" id="category_name" type="text" tabindex="2" class="required large tipTop" title="Please enter the categoryname" value="<?php echo $category_details->row()->cat_name;?>"/>
									</div>
								</div>
								</li>
                                
                                <li>
								<div class="form_grid_12">
									<label class="field_title" for="category_logo">Category Logo</label>
									<div class="form_input">
										<input name="category_logo" id="category_logo" type="file" tabindex="2" class="large tipTop" title="Please upload category image"/>
									</div>
								</div>
								</li>
                                
                                	<?php if($category_details->row()->image !=''){?>
                                
                                <li>
								<div class="form_grid_12">
									<label class="field_title" for="category_logo">&nbsp; </label>
									<div class="form_input">
                                    	<img src="<?php echo CATEGORY_PATH.$category_details->row()->image;?>" alt="<?php echo $category_details->row()->cat_name;?>" width="100" />

									</div>
								</div>
								</li>
                                
                                <?php }?>
                                
                                   <li>
								<div class="form_grid_12">
									<label class="field_title" for="category_name">Category Image</label>
									<div class="form_input">
										<input name="category_image" id="category_image" type="file" tabindex="2" class="large tipTop" title="Please upload category image"/>
									</div>
								</div>
								</li>

								<?php if($category_details->row()->category_image !=''){?>
                                <li>
								<div class="form_grid_12">
									<label class="field_title" for="category_logo">&nbsp; </label>
									<div class="form_input">
                                    	<img src="<?php echo CATEGORY_PATH.'cat_landing/'.$category_details->row()->category_image;?>" alt="<?php echo $category_details->row()->cat_name;?>" width="100" />

									</div>
								</div>
								</li>
                                <?php }?>
								
	 							<li>
								<div class="form_grid_12">
									<label class="field_title" for="admin_name">Status <span class="req">*</span></label>
									<div class="form_input">
										<div class="active_inactive">
											<input type="checkbox" tabindex="7" name="status" id="active_inactive_active" class="active_inactive" <?php if ($category_details->row()->status == 'Active'){echo 'checked="checked"';}?>  />
										</div>
									</div>
								</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="visible_category">Show / Hide From Category Landing</label>
										<div class="form_input">
											<input name="visible_category" id="visible_category" type="checkbox" tabindex="2" value="yes" title="check for show on category landing page" <?php if($category_details->row()->category_visible == 'yes'){ echo 'checked'; } ?> />
										</div>
									</div>
								</li>
								
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="category_name">Category Heading</label>
									<div class="form_input">
										<input name="category_heading" id="category_heading" type="text" tabindex="2" class="large tipTop" title="Please enter the category heading" value="<?php echo $category_details->row()->page_heading;?>"/>
									</div>
								</div>
								</li>
								<li>
				                  <div class="form_grid_12">
				                    <label class="field_title" for="meta_tag">Header Content</label>
				                    <div class="form_input">
				                      <textarea name="category_description" id="category_description"  tabindex="2" class="large tipTop mceEditor" title="Please enter the header content"><?php echo $category_details->row()->page_description;?></textarea>
				                    </div>
				                  </div>
				                </li>

								<li>
									<div class="form_grid_12">
										<label class="field_title" for="header_code_snippet">Header Code snippet</label>
										<div class="form_input">
											<textarea name="header_code_snippet" id="header_code_snippet" tabindex="1" class="large tipTop" title="Please enter the header code snippet"><?php echo $category_details->row()->header_code_snippet;?></textarea>
										</div>
									</div>
								</li>
				                
								<li>
								<div class="form_grid_12">
									<div class="form_input">
	                                    <input type="hidden" name="category_id" value="<?php echo $category_details->row()->id;?>"/>
    									<input type="button" class="btn_small btn_blue nxtTab" tabindex="9" value="Next"/>
									</div>
								</div>
								</li>
							</ul>
                       </div>
                    	<div id="tab2" class="tab_con" data-tab="link_tab2">
							<ul>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="meta_title">Meta Title</label>
									<div class="form_input">
									<input name="meta_title" id="meta_title" type="text" tabindex="1" class="large tipTop" title="Please enter the page meta title" value="<?php echo $category_details->row()->seo_title;?>"/>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="meta_tag">Footer Content</label>
									<div class="form_input">
									<textarea name="meta_keyword" id="meta_keyword"  tabindex="2" class="large tipTop mceEditor" title="Please enter the page footer content"><?php echo $category_details->row()->seo_keyword;?></textarea>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="meta_description">Meta Description</label>
									<div class="form_input">
									<textarea name="meta_description" id="meta_description" tabindex="3" class="large tipTop" title="Please enter the meta description"><?php echo $category_details->row()->seo_description;?></textarea>
									</div>
								</div>
								</li>
							</ul>
							<ul>
								<li><div class="form_grid_12">
									<div class="form_input">
										<input type="button" class="btn_small btn_blue prvTab" tabindex="9" value="Prev"/>
											<input type="button" class="btn_small btn_blue nxtTab" tabindex="9" value="Next"/>
										</div>
									</div>
								</li>
							</ul>

						</div>

						<div id="tab3" class="tab_con" data-tab="link_tab3">
							<ul>
								<li>
									<div class="form_grid_12 add_category">
										<div style="margin:12px;">
											<div class="btn_30_blue">
												<a href="javascript:void(0)" id="addAttr" class="tipTop" title="Add new category meta field">
													<span class="icon add_co"></span>
													<span class="btn_link">Add</span>
												</a>
											</div>
										</div>

										<?php
										if(!empty($city_wise_cat_details)) {
											$a='';
											foreach($city_wise_cat_details as $key => $val) {
												$a = 1 + $key; ?>
												<div class="row cat_data" id="cat_data_<?php echo $a; ?>" data-num="<?php echo $a; ?>" style="border:1px solid #000; padding:10px; margin:10px;">
													<ul>
														<li><div class="form_grid_12">
															<label class="field_title" for="city_name">City</label>
															<div class="form_input">
																<select name="city_name[]" id="city_name" class="large tipTop">
																	<option value="">Select City</option>
																	<?php foreach($cityList->result_array() as $city) { ?>
																		<option value="<?php echo $city['id']; ?>" <?php echo ($val['city_id'] == $city['id']) ? 'selected' : ''; ?>><?php echo $city['list_value']; ?></option>
																	<?php } ?>
																</select>
															</div>
														</div></li>
														<li><div class="form_grid_12">
															<label class="field_title" for="city_category_heading">Category Heading</label>
															<div class="form_input">
																<input name="city_category_heading[]" id="city_category_heading" type="text" tabindex="2" class="large tipTop" value="<?php echo $val['cat_heading']; ?>" title="Please enter the category heading"/>
															</div>
														</div></li>
														<li><div class="form_grid_12">
															<label class="field_title" for="city_category_description">Header Content</label>
															<div class="form_input">
																<textarea name="city_category_description[]" id="city_category_description" tabindex="3" class="city_category_description large tipTop mceEditor1" title="Please enter the header content"><?php echo $val['cat_desc']; ?></textarea>
															</div>
														</div></li>
														<li><div class="form_grid_12">
															<label class="field_title" for="city_header_code_snippet">Header Code snippet</label>
															<div class="form_input">
																<textarea name="city_header_code_snippet[]" id="city_header_code_snippet" tabindex="1" class="large tipTop" title="Please enter the header code snippet"><?php echo $val['cat_header_code_snippet']; ?></textarea>
															</div>
														</div></li>
														<li><div class="form_grid_12">
															<label class="field_title" for="city_meta_title">Meta Title</label>
															<div class="form_input">
																<input name="city_meta_title[]" id="city_meta_title" type="text" tabindex="1" class="large tipTop" title="Please enter the page meta title" value="<?php echo $val['cat_meta_title']; ?>" />
															</div>
														</div></li>
														<li><div class="form_grid_12">
															<label class="field_title" for="city_meta_keyword">Footer Content</label>
															<div class="form_input">
																<textarea name="city_meta_keyword[]" id="city_meta_keyword"  tabindex="2" class="large tipTop mceEditor1 city_meta_keyword" title="Please enter the page footer content"><?php echo $val['cat_meta_keyword']; ?></textarea>
															</div>
														</div></li>
														<li><div class="form_grid_12">
															<label class="field_title" for="city_meta_description">Meta Description</label>
															<div class="form_input">
																<textarea name="city_meta_description[]" id="city_meta_description" tabindex="3" class="large tipTop" title="Please enter the meta description"><?php echo $val['cat_meta_desc']; ?></textarea>
															</div
														></div></li>
														<li><div class="btn_30_blue">
															<a href="javascript:void(0)" id="removeAttr" class="tipTop" title="Remove this attribute"><span class="icon cross_co"></span><span class="btn_link">Remove</span></a>
														</div></li>
													</ul>
												</div>
											<?php }
										}
										?>
									</div>
								</li>

								<li>
									<div class="form_grid_12">
										<div class="form_input">
											<input type="button" class="btn_small btn_blue prvTab" tabindex="9" value="Prev"/>
											<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Update</span></button>
										</div>
									</div>
								</li>
							</ul>
						</div>

						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>

<?php
$content = '<li><div class="form_grid_12"><label class="field_title" for="city_name">City</label><div class="form_input"><select name="city_name[]" id="city_name" class="large tipTop"><option value="">Select City</option>';
	foreach($cityList->result_array() as $val) {
		$content .= '<option value="'.$val['id'].'">'.$val['list_value'].'</option>';
	}
$content .= '</select></div></div></li>';

$content .= '<li><div class="form_grid_12"><label class="field_title" for="city_category_heading">Category Heading</label><div class="form_input"><input name="city_category_heading[]" id="city_category_heading" type="text" tabindex="2" class="large tipTop" title="Please enter the category heading"/></div></div></li>';
$content .= '<li><div class="form_grid_12"><label class="field_title" for="city_category_description">Header Content</label><div class="form_input"><textarea name="city_category_description[]" id="city_category_description" tabindex="3" class="city_category_description large tipTop mceEditor" title="Please enter the header content"></textarea></div></div></li>';
$content .= '<li><div class="form_grid_12"><label class="field_title" for="city_header_code_snippet">Header Code snippet</label><div class="form_input"><textarea name="city_header_code_snippet[]" id="city_header_code_snippet" tabindex="1" class="large tipTop" title="Please enter the header code snippet"></textarea></div></div></li>';

$content .= '<li><div class="form_grid_12"><label class="field_title" for="city_meta_title">Meta Title</label><div class="form_input"><input name="city_meta_title[]" id="city_meta_title" type="text" tabindex="1" class="large tipTop" title="Please enter the page meta title"/></div></div></li>';
$content .= '<li><div class="form_grid_12"><label class="field_title" for="city_meta_keyword">Footer Content</label><div class="form_input"><textarea name="city_meta_keyword[]" id="city_meta_keyword"  tabindex="2" class="large tipTop mceEditor city_meta_keyword" title="Please enter the page footer content"></textarea></div></div></li>';
$content .= '<li><div class="form_grid_12"><label class="field_title" for="city_meta_description">Meta Description</label><div class="form_input"><textarea name="city_meta_description[]" id="city_meta_description" tabindex="3" class="large tipTop" title="Please enter the meta description"></textarea></div></div></li>';
$content .= '<li><div class="btn_30_blue"><a href="javascript:void(0)" id="removeAttr" class="tipTop" title="Remove this attribute"><span class="icon cross_co"></span><span class="btn_link">Remove</span></a></div></li>';
?>

<script type="text/javascript">
jQuery(document).ready(function($){

	$('#addAttr').click(function() {
		var count = '<?php echo count($cityList->result_array()); ?>';
		var div_len = ($('.add_category .cat_data').length > 0) ? $('.add_category .cat_data:last-child').data('num') : '0';
		if(count > div_len) {
			div_len = (parseInt(div_len) + 1);
			var content = '<div class="row cat_data" id="cat_data_'+div_len+'" data-num="'+div_len+'" style="border:1px solid #000; padding:10px; margin:10px;"><ul><?php echo $content; ?></ul></div>';
			$(content).fadeIn('slow').appendTo('.add_category');

			$("#cat_data_"+div_len+" .city_category_description").cleditor();
			$("#cat_data_"+div_len+" .city_meta_keyword").cleditor();
		}
	});

	$(document).on('click', '#removeAttr', function() {
		var id = $(this).parent('.btn_30_blue').parent().parent().parent().data('num');
		$('#cat_data_'+id).remove();
	});

	$(".city_category_description").cleditor();
	$(".city_meta_keyword").cleditor();

	var validator = $("#editcategory_form").submit(function() {
		// update underlying textarea before submit validation
		tinyMCE.triggerSave();
	}).validate({
		ignore: [],
		rules:{
			category_name: {
				required: true,
				minlength: 2
			}
		},
		messages: {
			category_name: {
				required: "Please enter category name",
				minlength: "category name must consist of at least 2 characters"
			}
		},
		errorPlacement: function(error, element) {
			error.appendTo( element.parent());
			$('.tab_con').hide();
			$('#widget_tab ul li a').removeClass('active_tab');
			var $link_tab = $('label.error:first').parents('.tab_con').show().data('tab');
			$('.'+$link_tab).addClass('active_tab');
		},
	});
	validator.focusInvalid = function() {
		// put focus on tinymce on submit validation
		if (this.settings.focusInvalid) {
			try {
				var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
				if (toFocus.is("textarea")) {
					tinyMCE.get(toFocus.attr("id")).focus();
				} else {
					toFocus.filter(":visible").focus();
				}
			} catch (e) {
				// ignore IE throwing errors when focusing hidden elements
			}
		}
	}

});
</script>

<?php 
$this->load->view('admin/templates/footer.php');
?>