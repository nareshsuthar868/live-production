<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/couponcards/change_couponcards_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
						<?php if ($allPrev == '1' || in_array('2', $couponcards)){?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Active','<?php echo $subAdminMail; ?>');" class="tipTop" title="Select any checkbox and click here to active records"><span class="icon accept_co"></span><span class="btn_link">Active</span></a>
							</div>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Inactive','<?php echo $subAdminMail; ?>');" class="tipTop" title="Select any checkbox and click here to inactive records"><span class="icon delete_co"></span><span class="btn_link">Inactive</span></a>
							</div>
						<?php 
						}
						if ($allPrev == '1' || in_array('3', $couponcards)){
						?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Delete','<?php echo $subAdminMail; ?>');" class="tipTop" title="Select any checkbox and click here to delete records"><span class="icon cross_co"></span><span class="btn_link">Delete</span></a>
							</div>
						<?php }?>
						</div>
					</div>
										<div class="widget_content">
						<table class="display display_tbl" id="OfficeOrderListing">
						<thead>
						<tr>
							<th class="center">
								<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">
							</th>
							<th class="tip_top" title="Click to sort">
								 Code
							</th>
							<th class="tip_top" title="Click to sort">
								 Type
							</th>
							<th class="tip_top" title="Click to sort">
								 Coupon Type
							</th>
							<th class="tip_top" title="Click to sort">
								 Value
							</th>
							<th class="tip_top" title="Click to sort">
								Remain
							</th>
							<th class="tip_top" title="Click to sort">
								Purchased
							</th>
							<th class="tip_top" title="Click to sort">
								Date From
							</th>
							<th class="tip_top" title="Click to sort">
								Date To
							</th>
							<th class="tip_top" title="Click to sort">
								Card Status
							</th>
							<th class="tip_top" title="Click to sort">
								Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
					    <tbody>
                    	</tbody>
						<tfoot>
						<tr>
							<th class="center">
								<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">
							</th>
							<th>
								 Code
							</th>
							<th>
								 Type
							</th>
							<th>
								 Coupon Type
							</th>
							<th>
								 Value
							</th>
							<th>
								Remain
							</th>
							<th>
								Purchased
							</th>
							<th>
								Date From
							</th>
							<th>
								Date To
							</th>
							<th>
								Card Status
							</th>
							<th>
								Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
		
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
            <input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<script type="text/javascript">
	$(function (){

   var bulkOrder =  $('#OfficeOrderListing').DataTable({
        processing:  true,
        serverSide: true,
        ajax: '<?php echo base_url(); ?>admin/couponcards/get_couponcards',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        order: [4,'desc'],
        columns: [
            null,
            { "data":"code"},
            { "data": "price_type"},
            { "data": "coupon_type"},
           	null,
            { "data": "remaning"},
            { "data": "purchase_count" },
            { "data": "datefrom" },
            { "data": "dateto" },
           	null,
            null,
            null,
        ],
        columnDefs: [
	        {
	           	orderable: true, targets: [0],
	            "render": function ( data, type, full, meta ) {
	                var link = '<input name="checkbox_id[]" type="checkbox" value='+full.id+'>';
	                return link;
	            }
	        },
	        {
	           	orderable: true, targets: [2],
	            "render": function ( data, type, full, meta ) {
	             	var price_type = '';
	             	if(data == '1'){
	             		price_type ='<span class="badge_style b_high">Flat</span>';
	             	}else if(data == '2'){
						price_type ='<span class="badge_style b_away">Percentage</span>';
	             	}else{
             			price_type ='<span class="badge_style b_away">Free</span>';
	             	}
	             	return price_type;
	            }
	        },
	        {
	        	orderable: true, targets: [4],
	            "render": function ( data, type, full, meta ) {
	             	var price_type = '';
	             	if(full.price_type == '1'){
	             		price_type =full.price_value;
	             	}else{
             			price_type =full.price_value+' %';
	             	}
	             	return price_type;
	            }
	        },

	       // {
	       // 	orderable: true, targets: [5],
	       //     "render": function ( data, type, full, meta ) {
	       //    		var reamin = full.quantity - full.purchase_count;
	       //      	return reamin;
	       //     }
	       // },
	        {
	        	orderable: true, targets: [9],
	            "render": function ( data, type, full, meta ) {
	            	var txt = '';
   					var var1 = new Date(full.dateto); 
            	  	var var2 = new Date();  
   					 if (var1 < var2) {  
   					 	txt = 'expired';
			        }else {  
			        	txt = full.card_status;
			        }  
	             	return txt;
	            }
	        },

	        {
	        	orderable: true, targets: [10],
	            "render": function ( data, type, full, meta ) {
	            	var links  = '';
            		'<?php if ($allPrev == '1' || in_array('2', $couponcards)){?>';
								var mode  = (full.status == 'Active')?'0':'1';
								// console.log(mode);
								if (mode == '0'){
									links += '<a title=Click to inactive class=tip_top href=javascript:confirm_status("admin/couponcards/change_couponcard_status/'+ mode +'/'+ full.id +'")><span class="badge_style b_done">'+ full.status +'</span></a>';
								}else{
									links += '<a title=Click to active class=tip_top href=javascript:confirm_status("admin/couponcards/change_couponcard_status/'+ mode +'/'+ full.id +'")><span class="badge_style">'+ full.status +'</span></a>';
								}
					'<?php }else{ ?>'
						links += '<span class="badge_style b_done">'+full.status +'</span>';
					'<?php }
					?>';
					return links;
	            }

	        },

	        {
	        	orderable: true, targets: [11],
	            "render": function ( data, type, full, meta ) {
	            	var links  = '';
            		'<?php if ($allPrev == '1' || in_array('2', $couponcards)){?>'
							links += '<span><a class="action-icons c-edit" href="admin/couponcards/edit_couponcard_form/'+ full.id +'" title="Edit">Edit</a></span>';
							'<?php }?>'
							'<?php if ($allPrev == '1' || in_array('3', $couponcards)){?>'	
							links += '<span><a class="action-icons c-delete" href="javascript:confirm_delete("admin/couponcards/delete_couponcard/'+ full.id +'")" title="Delete">Delete</a></span>';
							'<?php }?>'

					return links;
	            }

	        },

            {"targets": [0], "className": "center tr_select"},
	        {
	            orderable: false, targets: [0,9],
	        }],
        language: {
            searchPlaceholder: "Search By All"
        },
        fnDrawCallback: function (oSettings) {
        }
    });

});
</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>