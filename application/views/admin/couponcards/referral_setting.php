<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Referral Setting</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'update_voucher');
						echo form_open('admin/couponcards/update_referral_setting',$attributes) 
					?>
	 					<ul>
                             <li>
								<div class="form_grid_12">
									<label class="field_title" for="heading">Referral Amount<span class="req">*</span></label>
									<div>
										<input name="amount" type="text" tabindex="2" class="required small tipTop" title="Please Enter the Referral Amount" 
										value="<?php echo $referral_setting->row()->amount; ?>" />
									</div>
								</div>
							</li>
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="user_name">Total Quantity<span class="req">*</span></label>
									<div>
										<input name="quantity"  type="text" tabindex="2" class="required" title="Please Enter the Quantity" 
										value="<?php echo $referral_setting->row()->quantity; ?>" style="width:300px;" />
									</div>
								</div>
							</li>                                                               

							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Expire Days<span class="req">*</span></label>
									<div>
									  <input name="expire_days"  type="text" tabindex="2" class="required" title="Please Enter the Expire Days" 
                                        value="<?php echo $referral_setting->row()->expire_days; ?>" style="width:300px;" />
                                        <span class="req">(Days Only)</span>
									</div>
								</div>

							</li>
							
							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Social Media Message<span class="req">*</span></label>
									<div>
									
										<input type="text" name="social_message" value="<?php echo $referral_setting->row()->social_message; ?>" style="width: 70%;">
									 <span>(<strong>CODE_HERE,AMOUNT_HERE</strong>) are veriable</span>
									</div>
								</div>

							</li>
							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Referral Page Content<span class="req">*</span></label>
									<div>
										<textarea name="page_content" id="page_content" tabindex="3" style="width:370px;" class="required large tipTop mceEditor" title="Please enter the Content"><?php echo $referral_setting->row()->page_content; ?></textarea>
									</div>
								</div>								
							</li>
					
						
 
                        <li>
							<div class="form_grid_12">
								<div class="form_input">
										<button type="submit"  id="submit" class="btn_small btn_blue" tabindex="8"><span>Update</span></button>
								</div>
							</div>
						</li>
					</ul>
				</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>