<?php
$this->load->view('admin/templates/header.php');
?>
<script type="text/javascript">

$(document).ready(function() {
  $("#update_voucher").validate({
    rules: {
    	client_id: {
                required:true,
                minlength: 2,
                maxlength: 200
            },
    	  key_id: {
                required:true,
                minlength: 2,
                maxlength: 200
            },
            reward: {
                required: true,
                minlength: 2,
                maxlength: 10
            },
            post_url: {
                required: true,
                minlength: 2,
                maxlength: 150
            },
            return_url: {
                required: true,
                minlength: 2,
                maxlength: 150
            },
            partner_code: {
                required: true,
                minlength: 2,
                maxlength: 100
            },
             voucher_title: {
                required: true,
                minlength: 2,
                maxlength: 100
            },
             voucher_label: {
                required: true,
                minlength: 2,
                maxlength: 100
            },
             steps_desc: {
                required: true,
                minlength: 2,
                maxlength: 1000
            },
    },
    messages: {
    		client_id: {
                required : "<font color='red'>Client Id cannot be empty.</font>",
                minlength : "<font color='red'>Client Id has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Id length should be 100 characters maximum.</font>"
            },

    		key_id: {
                required : "<font color='red'>Key cannot be empty.</font>",
                minlength : "<font color='red'>Key has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Key length should be 40 characters maximum.</font>"
            },
            reward: {
                required: "<font color='red'>Reward Required.</font>",
                minlength : "<font color='red'>Reward has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Reward length should be 40 characters maximum.</font>"
            },
            post_url: {
                required: "<font color='red'>Url Required.</font>",
                minlength : "<font color='red'>Url has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Url length should be 150 characters maximum.</font>"
            },
            return_url: {
                required: "<font color='red'>Url Required.</font>",
                minlength : "<font color='red'>Url has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Url length should be 150 characters maximum.</font>"
            },
            partner_code: {
                required: "<font color='red'>Code Required.</font>",
                minlength : "<font color='red'>Code has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Code length should be 100 characters maximum.</font>"
            },
            voucher_title: {
                required: "<font color='red'>Title Required.</font>",
                minlength : "<font color='red'>Title has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Title length should be 100 characters maximum.</font>"
            },
            voucher_label: {
                required: "<font color='red'>Label Required.</font>",
                minlength : "<font color='red'>Label has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Label length should be 100 characters maximum.</font>"
            },
            steps_desc: {
                required: "<font color='red'>Steps Desc Required.</font>",
                minlength : "<font color='red'>Steps Desc has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Steps Desc length should be 1000 characters maximum.</font>"
            },
    },
    errorElement: "span",
        errorPlacement: function(error, element) {
                error.appendTo(element.parent());
        }

  });
});
</script>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Voucher Setting</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'update_voucher');
						echo form_open('admin/couponcards/update_voucher',$attributes) 
					?>
	 					<ul>
                             <li>
								<div class="form_grid_12">
									<label class="field_title" for="heading">Client ID<span class="req">*</span></label>
									<div>
										<input name="client_id" id="client_id" type="text" tabindex="2" class="required small tipTop" title="Please Enter the client id" 
										value="<?php echo $voucher_details->row()->client_id; ?>" />
									</div>
								</div>
							</li>
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="user_name">Key<span class="req">*</span></label>
									<div>
										<input name="key_id" id="key_id" type="text" tabindex="2" class="required" title="Please Enter the Caption" 
										value="<?php echo $voucher_details->row()->key_id; ?>" style="width:300px;" />
									</div>
								</div>
							</li>                                                               
                            <li>
								<div class="form_grid_12">
                                    <label class="field_title">Reward<span class="req">*</span></label>
									<div>
									<input name="reward" id="reward" type="number" tabindex="2" class="required small tipTop" title="Please Enter the Reward"
									value="<?php echo $voucher_details->row()->reward; ?>" />
									</div>
								</div>
							</li>
							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Post Url<span class="req">*</span></label>
									<div>
									<input name="post_url" id="post_url" type="text" tabindex="2" class="required small tipTop" title="Enter The Url"
									value="<?php echo $voucher_details->row()->post_url; ?>" />
									</div>
								</div>
							</li>
							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Retrun Url<span class="req">*</span></label>
									<div>
									<input name="return_url" id="return_url" type="text" tabindex="2" class="required small tipTop" title="Enter The Url"
									value="<?php echo $voucher_details->row()->return_url; ?>" />
									</div>
								</div>
							</li>
							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Partner Code<span class="req">*</span></label>
									<div>
									<input name="partner_code" id="partner_code" type="text" tabindex="2" class="required small tipTop" title="Enter The Url"
									value="<?php echo $voucher_details->row()->partner_code; ?>" />
									</div>
								</div>
							</li>

							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Voucher title<span class="req">*</span></label>
									<div>
										<input name="voucher_title" id="voucher_title" type="text" tabindex="2" class="required small tipTop" title="Enter The Url"
									value="<?php echo $voucher_details->row()->voucher_title; ?>" />	 

									</div>
								</div>
							</li>
							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Voucher label<span class="req">*</span></label>
									<div>
										<input name="voucher_label" id="voucher_label" type="text" tabindex="2" class="required small tipTop" title="Enter The Url"
									value="<?php echo $voucher_details->row()->voucher_label; ?>" />	 

									</div>
								</div>
							</li>

							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Step Desc<span class="req">*</span></label>
									<div>
									  <textarea name="steps_desc" class="required large tipTop mceEditor" id="steps_desc" style="width:370px;"><?php echo $voucher_details->row()->steps_desc; ?></textarea>

									</div>
								</div>
							</li>
					
							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Status<span class="req">*</span></label>
									<div>
								<input name="status" id="status"  value="1" type="radio" tabindex="2" <?php if($voucher_details->row()->status){ echo 'checked'; } ?> >Enable
								<input name="status" id="visible"  value="0" type="radio" tabindex="2"
								<?php if(!$voucher_details->row()->status){ echo 'checked'; } ?> >
								Disable
									</div>
								</div>
							</li>
 
                        <li>
							<div class="form_grid_12">
								<div class="form_input">
										<button type="submit"  id="submit" class="btn_small btn_blue" tabindex="8"><span>Update</span></button>
								</div>
							</div>
						</li>
					</ul>
				</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>