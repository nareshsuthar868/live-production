<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>


<script type="text/javascript">
$(function (){
    var t =  $('#get_referral_data').DataTable({
        processing:  true,
        serverSide: true,
        pageLength: 25,
        lengthMenu: [ 10, 25, 50, 75, 100 ],
        ajax: '<?php echo base_url(); ?>admin/couponcards/Get_referral_code_list',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        order: [],
        columns: [
        	{ "data": "id"},
            { "data": "referral_code" },
            { "data": "email"},
           	{ "data": null},
            { "data": "count" },
            { "data": "used_by" },
            { "data": "created_date" },
            { "data": "expire_date"},
            { "data": "referral_code"},
        ],
        columnDefs: [
        	{
                 orderable: false, targets: [3],
                "render": function ( data, type, full, meta ) {
                    
                    return 'Flate';
                }
            },
          
             {
                 orderable: false, targets: [8],
                "render": function ( data, type, full, meta ) {
                    
            		return '<button onclick="return show_used_count(\''+ data +'\')" >View</button>'
                     
                }
            }, 

        {
            orderable: false, targets: [8],
        }],
        language: {
            searchPlaceholder: "Search By Email"
        },
        fnDrawCallback: function (oSettings) {
        }
    });

    //  t.on( 'order.dt search.dt', function () {
    //     t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
    //         cell.innerHTML = i+1;
    //     } );
    // } ).draw();
});
</script>

<div id="content">
		<div class="grid_container">
			
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
				
					</div>
					<div class="widget_content">
						<table class="display sort-table" id="get_referral_data">
                            <thead>
                                <tr>
	                            	<th class="center">
										#
									</th>
	                                <th>Code</th>
	                                <th>User Email</th>
	                                <th>Coupon Type</th>
	                                <th>Remain</th>
	                                <th>Purchased</th>
	                                <th>Date From</th>
	                                <th>Expire In</th>
	                                <th>Used By</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	<th class="center">
										#
									</th>
	                                <th>Code</th>
	                                <th>User Email</th>
	                                <th>Coupon Type</th>
	                                <th>Remain</th>
	                                <th>Purchased</th>
	                                <th>Date From</th>
	                                <th>Expire In</th>
	                                <th>Used By</th>
	                            </tr>
	                        </tfoot>
                        </table>


					</div>
				</div>
			</div>
			<!-- <input type="hidden" name="statusMode" id="statusMode"/>
            <input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/> -->
		<!-- </form>	 -->
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<button type="button" class="trigger" style="display: none;">View</button>
    <div class="modal refralmodal" id="model_css">
        <div class="modal-content">
            <span class="close-button">&times;</span>
            <h1>List of Used Referral Code Users</h1><br>
	            <table >
	            	<thead>
				  <tr>
				    <th>User Id</th>
				    <th>User Name</th>
				    <th>Email</th>
				    <th>Discount Amount</th>
				  </tr>
				  </thead>
				  <tbody id="append_new_user">
				  	
				  </tbody>
				 
				</table>

        </div>
    </div>






 <style type="text/css">

    .modal {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        opacity: 0;
        visibility: hidden;
        transform: scale(1.1);
        transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;
    }
    .modal-content {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: white;
        padding: 1rem 1.5rem;
        width: 40rem;
        border-radius: 0.5rem;
    }
    .close-button {
        float: right;
        width: 1.5rem;
        line-height: 1.5rem;
        text-align: center;
        cursor: pointer;
        border-radius: 0.25rem;
        background-color: lightgray;
    }
    .close-button:hover {
        background-color: darkgray;
    }
    .show-modal {
        opacity: 1;
        visibility: visible;
        transform: scale(1.0);
        transition: visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s;
    }
    </style>
    <style>
		table {
		    font-family: arial, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		}

		td, th {
		    border: 1px solid #dddddd;
		    text-align: center;
		    padding: 8px;
		}

		tr:nth-child(even) {
		    background-color: #dddddd;
		}
		@media only screen and (max-width: 1366px) {
			.refralmodal{
				overflow: auto;
			}
			.refralmodal .modal-content{
				transform: translate(-50%, 0);
				top: 20px;
			} 
			.modal-open{
				overflow: hidden;
			}
		}

</style>

    <script type="text/javascript">
    	 var modal = document.querySelector(".modal");
    	var trigger = document.querySelector(".trigger");
    	var closeButton = document.querySelector(".close-button");

	    function toggleModal() {
	        modal.classList.toggle("show-modal"); 
	        document.getElementsByTagName('body')[0].style = 'overflow: auto';
	    }

	    function windowOnClick(event) {
	        if (event.target === modal) {
	            toggleModal();
	        }
	    }

	    trigger.addEventListener("click", toggleModal);
	    closeButton.addEventListener("click", toggleModal);
	    window.addEventListener("click", windowOnClick);
    </script>


<style type="text/css">
.dataTables_wrapper .dataTables_paginate .paginate_button {
	margin: 0px 0px 0px -2px !important;
	padding: 0px !important;
}
</style>


    <?php 
$this->load->view('admin/templates/footer.php');
?>
