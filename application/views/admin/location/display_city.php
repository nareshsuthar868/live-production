<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
	<div class="grid_container">
		<form action="admin/location/change_location_status_global" method="post" id="display_form">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
						<table class="display" id="city_tbl">
							<thead>
								<tr>
									<th class="tip_top" title="Click to sort">City Name</th>
									<th class="center">Total Products</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ($cityList->num_rows() > 0) {
									foreach ($cityList->result() as $row) {
								?>
								<tr>
									<td class="center tr_select">
										<?php echo $row->list_value; ?>
									</td>
									<td class="center tr_select">
										<?php echo $row->product_count; ?>
									</td>
									<td class="center">
										<?php echo ($row->status == '1') ? 'Active' : 'Inactive'; ?>
									</td>
									<td class="center">
										<?php if ($allPrev == '1') { ?>
											<span><a class="action-icons c-edit" href="admin/location/edit_city_form/<?php echo $row->id;?>" title="Edit">Edit</a></span>
											<span><a class="action-icons c-delete" href="javascript:confirm_delete('admin/location/delete_city/<?php echo $row->id;?>')" title="Delete">Delete</a></span>
										<?php } ?>
									</td>
								</tr>
								<?php
									}
								}
								?>
							</tbody>
							<tfoot>
								<tr>
									<th class="tip_top" title="Click to sort">City Name</th>
									<th class="center">Total Products</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
		</form>
	</div>
	<span class="clear"></span>
</div>
</div>

<?php
$this->load->view('admin/templates/footer.php');
?>