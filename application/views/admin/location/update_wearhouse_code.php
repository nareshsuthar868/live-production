<?php
$this->load->view('admin/templates/header.php');
?>
<script type="text/javascript">
	$('document').ready(function()
{
    $('textarea').each(function(){
            $(this).val($(this).val().trim());
        }
    );
});
</script>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Update WareHouse Code</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label');
						echo form_open('admin/location/InsertUpdateWarehouseCode',$attributes) ;
						// print_r($result);exit;
					?>
					<table  class="display" >
						<thead>
							<tr class="center">
								<th class="tip_top">#</th>
								<th class="tip_top">City Name</th>
								<th class="tip_top">Gst Number</th>
								<th class="tip_top">Reg Address</th>
								<th class="tip_top">SO City Code</th>
								<th class="tip_top">Warehouse Code</th>
							</tr>
						</thead>
						<tbody>
							<?php 
	 							foreach ($result->result() as $key => $value) { ?>
                            	<tr>
                            		<input type="hidden" value="<?php echo $value->id; ?>" name="<?php echo $value->id; ?>">
                            		<td  class="center  tr_select">
									<?php echo $key+1; ?>
									</td>
									<td  class="center  tr_select">
									<?php echo $value->city_name; ?>
									</td>
                                   <td  class="center  tr_select">       
                                       <input type="text" name="gst_<?php echo $value->city_name; ?>" value="<?php  echo $value->gst_num;  ?>" placeholder="GST CODE" style="width: 300px;">
                                   </td>
									  <td  class="center  tr_select">       
                                    	<textarea  name="gstAddress_<?php echo $value->city_name; ?>" placeholder="REG ADDRESS">
                                    		<?php  echo trim($value->reg_address);  ?>
                                    	</textarea>
                                   </td>
                                <td  class="center  tr_select">
								   <input type="text" name="so_<?php echo $value->city_name; ?>" value="<?php  echo $value->so_city_code;  ?>" placeholder="SO CITY CODE" style="width: 150px;">
                                  </td>
                                  <td  class="center  tr_select">
                                       <select  name="<?php echo $value->city_name; ?>">
                                       <option disabled="true">Select Code</option>
                                       	<option <?php if($value->wearhouse_code == 'GUR') { echo "selected"; }  ?> value="GUR" >GUR</option>
                                        <option <?php if($value->wearhouse_code == 'MUM') { echo "selected"; }  ?> value="MUM" >MUM</option>
                                        <option <?php if($value->wearhouse_code == 'NOI') { echo "selected"; }  ?> value="NOI" >NOI</option>
                                        <option <?php if($value->wearhouse_code == 'PUN') { echo "selected"; }  ?> value="PUN" >PUN</option>
                                        <option <?php if($value->wearhouse_code == 'BAN') { echo "selected"; }  ?> value="BAN" >BAN</option>
                                       </select>
                                  </td>
								</tr>
							<?php } ?>
						
						</tbody>
					</table>
	 						
							
								 <ul><li><div class="form_grid_12">
										<div class="form_input">
									<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Update</span></button>
								</div></div></li></ul>
							
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>