<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Edit Email Template</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'commentForm');
						echo form_open('admin/newsletter/insertEditOrderNotification',$attributes) 
					?>
	 						<ul>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="order_status">Order Status <span class="req">*</span></label>
										<div class="form_input">
	                                    <input name="order_status" style=" width:295px" id="order_status" value="<?php echo $user_details->row()->order_status;?>" type="text" tabindex="1" class="required tipTop" title="Please enter the order status"/>
										</div>
									</div>
								</li>

								<li>
									<div class="form_grid_12">
										<label class="field_title" for="order_sub_status">Order Sub Status <span class="req">*</span></label>
										<div class="form_input">
	                                    <input name="order_sub_status" style="width:295px" id="order_sub_status" type="text" tabindex="1" class="required tipTop" value="<?php echo $user_details->row()->order_sub_status;?>" title="Please enter the order sub status"/>
										</div>
									</div>
								</li>

                             	<li>
									<div class="form_grid_12">
										<label class="field_title" for="sender_email">Sender Email Address <span class="req">*</span></label>
										<div class="form_input">
	                                    <input name="sender_email" style=" width:295px" id="sender_email" type="text" tabindex="1" value="<?php echo $this->config->item('email');?>" class="required tipTop" title="Please enter the sender email address"/>
										</div>
									</div>
								</li>

								<li>
								<div class="form_grid_12">
									<label class="field_title" for="sms_content">SMS Description </label>
									<div class="form_input">

                                 <textarea name="sms_content" id="sms_content" style="height: 100px; width:900px" id="news_subject" >
                                 <?php echo trim($user_details->row()->sms_content);?>
                                 </textarea>
                                 <br>
									<b>{{ORDER_ID}}, {{CUSTOMER_NAME}}</b> Are Varibles.
									</div>
								</div>
								</li>


                                <li>
									<div class="form_grid_12">
										<label class="field_title" for="email_content">Email Description </label>
										<div class="form_input">
	                                    <textarea name="email_content" style=" width:295px;" class="tipTop mceEditor" title="Please enter the email templete description"><?php echo $user_details->row()->email_content;?></textarea>
                        	         	<br>
										<b>{{ORDER_ID}}, {{CUSTOMER_NAME}}</b> Are Varibles.   
										</div>
									</div>
								</li>

								<li>
									<div class="form_grid_12">
										<label class="field_title" for="sms_status">Sms Notification </label>
										<div class="form_input">
	                                 	<div class="publish_unpublish">
											<input type="checkbox" tabindex="11" name="sms_status" id="publish_unpublish_publish" class="publish_unpublish" <?php if ($user_details->row()->sms_status == '1'){ echo 'checked="checked"';}?>  />
										</div>
									</div>
								</li>

								<li>
									<div class="form_grid_12">
										<label class="field_title" for="email_status">Email Notification  </label>
										<div class="form_input">
												<div class="publish_unpublish">
											<input type="checkbox" tabindex="11" name="email_status" id="publish_unpublish_publish" class="publish_unpublish"  <?php if ($user_details->row()->email_status == '1'){ echo 'checked="checked"';}?> />
										</div>
	                                  
										</div>
									</div>
								</li>

								<input type="hidden" name="newsletter_id" value="<?php echo $user_details->row()->id;?>"/>
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Update</span></button>
									</div>
								</div>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>

<script type="text/javascript">
	$('document').ready(function(){
	    $('textarea').each(function(){
	            $(this).val($(this).val().trim());
	    });
	});
</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>