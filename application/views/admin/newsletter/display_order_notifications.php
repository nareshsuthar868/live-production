<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading;?></h6>
					</div>
					<div class="widget_content">
						<table class="display" id="newsletter_tbl">
						<thead>
						<tr>
							
							<th class="tip_top" title="Click to sort">
								Order Stauts
							</th>
							<th class="center" title="Click to sort">
                            	Order Sub Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($notification_listing->num_rows() > 0){
							foreach ($notification_listing->result() as $row){
						?>
						<tr>
							
							<td class="center  tr_select">
								<?php echo $row->order_status;?>
							</td>
							<td class="center tr_select ">
                                <?php echo $row->order_sub_status;?>
							</td>

							<td class="center">
							<?php if ($allPrev == '1' || in_array('2', $newsletter)){?>
								<span><a class="action-icons c-edit" href="admin/newsletter/edit_order_notification_form/<?php echo $row->id;?>" title="Edit">Edit</a></span>
							<?php }?>
								<span><a class="action-icons c-delete" href="javascript:confirm_delete('admin/newsletter/delete_ordernotification/<?php echo $row->id;?>')" title="Delete">Delete</a></span>
							</td>
						</tr>
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							
							<th>
								 Template Name
							</th>
							<th class="center">
								Email Subject
							</th>
							<th>
								 Action
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>