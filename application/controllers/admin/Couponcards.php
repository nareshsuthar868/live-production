<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to couponcards management 
 * @author Teamtweaks
 *
 */

class Couponcards extends MY_Controller { 

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('couponcards_model');
		if ($this->checkPrivileges('couponcards',$this->privStatus) == FALSE){
			redirect('admin');
		}
    }
    
    /**
     * 
     * This function loads the couponcards list page
     */
   	public function index(){	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/couponcards/display_couponcards');
		}
	}
	
	/**
	 * 
	 * This function loads the couponcards list page
	 */
	public function display_couponcards(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Coupon Codes List';
// 			$condition = array();
// 			$this->data['couponCardsList'] = $this->couponcards_model->get_all_details(COUPONCARDS,$condition);
			$this->load->view('admin/couponcards/display_couponcards',$this->data);
		}
	}
	   
	/**
	 * 
	 * This function loads the couponcards list page on ajax call
	*/
	public function get_couponcards(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->couponcards_model->get_couponcards_details(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->couponcards_model->get_couponcards_details('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}
	
	/**
	 * 
	 * This function loads the add new couponcard form
	 */
	public function add_couponcard_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Add New Coupon Code';
			$this->data['code'] = $this->get_rand_str('10');
			$this->data['CateogyView'] = $this->couponcards_model->view_category_details();
			$this->data['ProductView'] = $this->couponcards_model->view_product_details();
			$this->data['SellerView'] = $this->couponcards_model->view_seller_details();
			$this->load->view('admin/couponcards/add_couponcard',$this->data);
		}
	}
	/**
	 * 
	 * This function insert and edit a couponcard
	 */
	public function insertEditCouponcard(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$coupon_id = $this->input->post('coupon_id');
			$coupon_type = $this->input->post('coupon_type');
			
			$excludeArr = array("coupon_id","price_type","status","category_id","product_id","user_id");
			if($coupon_type!='shipping'){
			
				if ($this->input->post('price_type') != ''){
					$priceType = '1';
				}else {
					$priceType = '2';
				}
				if($this->input->post('category_id') != ''){
					$categoryid = @implode(',',$this->input->post('category_id'));
				}else{
					$categoryid = '';
				}
				if($this->input->post('product_id') != ''){
					$productid = @implode(',',$this->input->post('product_id'));
				}else{
					$productid = '';
				}
				if($this->input->post('user_id') != ''){
					$userid = @implode(',',$this->input->post('user_id'));
				}else{
					$userid = '';
				}
				$inputArr = array('status' => 'Active','category_id' => $categoryid, 'product_id' => $productid, 'user_id' => $userid, 'price_type' =>	$priceType);
			}else{
				$priceType = 3;
				$inputArr = array('status' => 'Active','price_type'	=> $priceType);
			}
			$datestring = "%Y-%m-%d";
			$time = time();
			if ($coupon_id == ''){
				$coupon_data = array('price_type'	=>	$priceType);
			}else {
				$coupon_data = array();
			}
			$dataArr = array_merge($inputArr,$coupon_data);
			$condition = array('id' => $coupon_id);
			if ($coupon_id == ''){
				$this->couponcards_model->commonInsertUpdate(COUPONCARDS,'insert',$excludeArr,$dataArr,$condition);
				$this->setErrorMessage('success','Coupon Code added successfully');
			}else {
				$this->couponcards_model->commonInsertUpdate(COUPONCARDS,'update',$excludeArr,$dataArr,$condition);
				$this->setErrorMessage('success','Coupon Code updated successfully');
			}
			redirect('admin/couponcards/display_couponcards');
		}
	}
	
	/**
	 * 
	 * This function loads the edit couponcard form
	 */
	public function edit_couponcard_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Coupon Code';
			$user_id = $this->uri->segment(4,0);
			$condition = array('id' => $user_id);
			$this->data['couponcard_details'] = $this->couponcards_model->get_all_details(COUPONCARDS,$condition);
			
			$newCatArr = @explode(',',$this->data['couponcard_details']->row()->category_id);
			$newPrdArr = @explode(',',$this->data['couponcard_details']->row()->product_id);			
			$newSelArr = @explode(',',$this->data['couponcard_details']->row()->user_id);			
			$this->data['CateogyView'] = $this->couponcards_model->view_edit_category_details($newCatArr);
			$this->data['ProductView'] = $this->couponcards_model->view_product_details($newPrdArr);
			$this->data['SellerView'] = $this->couponcards_model->view_seller_details($newSelArr);
			if ($this->data['couponcard_details']->num_rows() == 1){
				$this->load->view('admin/couponcards/edit_couponcard',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	
	/**
	 * 
	 * This function change the couponcard status
	 */
	public function change_couponcard_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$user_id = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Inactive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $user_id);
			$this->couponcards_model->update_details(COUPONCARDS,$newdata,$condition);
			$this->setErrorMessage('success','Coupon Code Status Changed Successfully');
			redirect('admin/couponcards/display_couponcards');
		}
	}
	
	/**
	 * 
	 * This function delete the couponcard from db
	 */
	public function delete_couponcard(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$coupon_id = $this->uri->segment(4,0);
			$condition = array('id' => $coupon_id);
			$this->couponcards_model->commonDelete(COUPONCARDS,$condition);
			$this->setErrorMessage('success','Coupon Code deleted successfully');
			redirect('admin/couponcards/display_couponcards');
		}
	}
	
	/**
	 * 
	 * This function change the couponcards status
	 */
	public function change_couponcards_status_global(){
		if(count($_POST['checkbox_id']) > 0 &&  $_POST['statusMode'] != ''){
			$this->couponcards_model->activeInactiveCommon(COUPONCARDS,'id');
			if (strtolower($_POST['statusMode']) == 'delete'){
				$this->setErrorMessage('success','Coupon Code deleted successfully');
			}else {
				$this->setErrorMessage('success','Coupon Code status changed successfully');
			}
			redirect('admin/couponcards/display_couponcards');
		}
	}
	
	public function edit_voucher_form(){
		// print_r("expression");exit;
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Voucher Settings';
			$user_id = $this->uri->segment(4,0);
			$condition = array('id !=' => '');
			$this->data['voucher_details'] = $this->couponcards_model->get_all_details(VOUCHER,$condition);
			// print_r($this->data['voucher_details']->row());exit;
			$this->load->view('admin/couponcards/edit_voucher_setting',$this->data);
			
		}
	}

	public function update_voucher(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$data = $this->input->post();
			$condition =  array('id !=' => '');
		   	$respose = $this->couponcards_model->update_details(VOUCHER,$data,$condition);
		    $this->setErrorMessage('success','Voucher Setting Updated Successfully');
		    redirect('admin/couponcards/edit_voucher_form');
		}

	}
	
	//Referral Code 
	public function edit_referral_setting(){
	   	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$data = $this->input->post();
			$condition = array('id !=' => '');
			$this->data['referral_setting'] = $this->couponcards_model->get_all_details(REFERRAL_SETTING,$condition);
			$this->load->view('admin/couponcards/referral_setting',$this->data);
		}
	}

	public function update_referral_setting(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$data = $this->input->post();
			$condition =  array('id !=' => '');
		   	$respose = $this->couponcards_model->update_details(REFERRAL_SETTING,$data,$condition);
		    $this->setErrorMessage('success','Referral Setting Updated Successfully');
		    redirect('admin/couponcards/edit_referral_setting');
		}
	}

	public function show_referral_code(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Referral Code list';
// 			$condition = array('id !=' => '');
// 			$this->data['referral_code'] = $this->couponcards_model->getReferralCodeDetails();
			$this->load->view('admin/couponcards/referral_list',$this->data);
		}
	}
	
	public function Get_referral_code_list(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;

	        $data = $this->couponcards_model->getReferralCodeDetails(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        // print_r($data);exit;
	        foreach ($data as $key => $value) {
	        	$data[$key]->created_date = date('d M Y h.i.s A', strtotime($value->created_date));
	        	$get_count = unserialize($value->used_by);
	        	if(!empty($get_count) > 0){
	        		$data[$key]->used_by = count($get_count);
 	        	}else{
 	        		$data[$key]->used_by = 0;
 	        	}

 	        	$now = time(); // or your date as well
				$your_date = strtotime($value->expire_date);
				$current_date = strtotime($now);
				$datediff = $your_date - $now;
				if($your_date > $now ){
					$data[$key]->expire_date =  round($datediff / (60 * 60 * 24)).' Days';
				}else{
			 		$data[$key]->expire_date =  "<font style=color:red;>Expired</font>";
				}
	        }
	        $TotalCount = $this->couponcards_model->getReferralCodeDetails('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}

	public function getUsedCodeUserDetails(){
		$response = $this->couponcards_model->get_used_card_details($this->input->post('used_by'));
		echo json_encode($response);
	}
	
	
	public function edit_si_discount_setting(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$data = $this->input->post();
			$condition = array('id ' => 1);
			$this->data['si_discount_setting'] = $this->couponcards_model->get_all_details(SI_DISCOUNT_SETTING,$condition);
			// print_r($this->data['si_discount_setting']->row());exit;
			$this->load->view('admin/couponcards/si_discount_setting',$this->data);
		}
		// print_r("expression");exit;
	}
	public function update_si_discount_setting(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
			$data = $this->input->post();
			$data['updated_date'] =  $date->format('Y/m/d H:i:s');
			$condition =  array('id ' => 1);
		   	$respose = $this->couponcards_model->update_details(SI_DISCOUNT_SETTING,$data,$condition);
		    $this->setErrorMessage('success','SI Discount Setting Updated Successfully');
		    redirect('admin/couponcards/edit_si_discount_setting');
		}
	}

}

/* End of file couponcards.php */
/* Location: ./application/controllers/admin/couponcards.php */