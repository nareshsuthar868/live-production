<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to Pincodes management 
 * @author Teamtweaks
 *
 */

class Pincodes extends MY_Controller { 

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('couponcards_model');
		
    }
    
    /**
     * 
     * This function loads the Pincodes list page
     */
   	public function index(){	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/couponcards/display_couponcards');
		}
	}
	
	/**
	 * 
	 * This function loads the Pincodes list page
	 */
	public function display_pincodes(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Coupon Codes List';
			$condition = array();
			$this->data['pincodesList'] = $this->db->query("SELECT ID,pincode,cityid FROM ".PINCODE." ORDER BY adddateTime");
			$this->load->view('admin/pincodes/display_pincodes',$this->data);
		}
	}
	
	/**
	 * 
	 * This function loads the add new couponcard form
	 */
	public function add_pincodes_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Add New Pin Code';
			$query = $this->db->query("SELECT id,list_value FROM ".LIST_VALUES." WHERE list_id='1'");			
			$this->data['listv'] = $query->result();
			$this->load->view('admin/pincodes/add_pincodes',$this->data);
		}
	}
	/**
	 * 
	 * This function insert and edit a couponcard
	 */
	public function insertEditpincode(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$pincode_id = $this->input->post('pincode_id');			
			if ($pincode_id == ''){
				$this->db->query("INSERT INTO ".PINCODE." SET pincode='".mysql_real_escape_string($this->input->post('pincode'))."',cityid='".mysql_real_escape_string($this->input->post('city'))."',adddateTime=now()");
				$this->setErrorMessage('success','Coupon Code added successfully');
			}else {
				$this->db->query("UPDATE ".PINCODE." SET pincode='".mysql_real_escape_string($this->input->post('pincode'))."',cityid='".mysql_real_escape_string($this->input->post('city'))."' WHERE ID='{$pincode_id}' LIMIT 1");
				$this->setErrorMessage('success','Coupon Code updated successfully');
			}
			redirect('admin/pincodes/display_pincodes');
		}
	}
	
	/**
	 * 
	 * This function loads the edit couponcard form
	 */
	public function edit_pincodes_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Pin Code';
			$pincode_id = $this->uri->segment(4,0);
			$condition = array('id' => $user_id);
			$query = $this->db->query("SELECT id,list_value FROM ".LIST_VALUES." WHERE list_id='1'");			
			$this->data['listv'] = $query->result();
			$this->data['pincode_details'] = $this->db->query("SELECT * FROM ".PINCODE." WHERE ID='{$pincode_id}'");
			if ($this->data['pincode_details']->num_rows() == 1){
				$this->load->view('admin/pincodes/edit_pincodes',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	public function delete_pincode(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$pincode_id = $this->uri->segment(4,0);			
			$this->db->query("DELETE FROM ".PINCODE." WHERE ID='{$pincode_id}' LIMIT 1");
			$this->setErrorMessage('success','Coupon Code deleted successfully');
			redirect('admin/pincodes/display_pincodes');
		}
	}	
	
}

/* End of file couponcards.php */
/* Location: ./application/controllers/admin/couponcards.php */