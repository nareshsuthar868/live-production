<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orderdashboard extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('dashboard_model');
		$this->load->model('giftcards_model');
    }
    
	public function order_reports(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['new_order'] = $this->dashboard_model->get_all_order_status_records('New Order','Document Complete');
			$this->data['document_complete'] = $this->dashboard_model->get_all_order_status_records('Document Complete','Sales Order Created');
			$this->data['sales_order_created'] = $this->dashboard_model->get_all_order_status_records('Sales Order Created','Invoice Created');
			$this->data['invoice_created'] = $this->dashboard_model->get_all_order_status_records('Invoice Created','Delivery Scheduled');
			$this->data['delivery_scheduled'] = $this->dashboard_model->get_all_order_status_records('Delivery Scheduled','Delivery Done');
			$this->data['delivery_done'] = $this->dashboard_model->get_all_order_status_records('Delivery Done','Payment Doc Proof Pending');
			$this->data['payment_doc_proof_pending'] = $this->dashboard_model->get_all_order_status_records('Payment Doc Proof Pending','Payment Doc Validation Pending');
			$this->data['payment_doc_validation_pending'] = $this->dashboard_model->get_all_order_status_records('Payment Doc Validation Pending','Payment Doc Dispatch Pending');
			$this->data['payment_doc_dispatch_pending'] = $this->dashboard_model->get_all_order_status_records('Payment Doc Dispatch Pending','Closed');
			$this->data['Closed'] = $this->dashboard_model->get_all_order_status_records('Closed','');
			// echo "<pre>";
			// print_r($this->data['new_order']);exit;

			$this->data['heading'] = 'Order Status Reports';
			$this->load->view('admin/orderdashboard/dashboard_report',$this->data);
		}
	}

	public function ordertatSettings(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['tatConfiguration'] = $this->dashboard_model->get_all_details(ORDER_STATUS_TAT,array());
			$this->data['heading'] = 'TAT For Order Status';
			$this->load->view('admin/orderdashboard/order_tat',$this->data);
		}
	}

	public function updatetatvalue(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$tat_id = $this->input->post('id');
			$duration = $this->input->post('tat');
			$tatDetails = $this->dashboard_model->get_all_details(ORDER_STATUS_TAT,array('id' => $tat_id));
			if($tatDetails->num_rows() > 0){
				$this->dashboard_model->update_details(ORDER_STATUS_TAT,array('duration' => $duration),array('id' => $tat_id));
				echo json_encode(array("message" => 'success', "code" => 200));
			}
		}
	}

	public function get_orderDetails(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$TatDetails = $this->input->post('array');
			$orderIds = array();
			if(!empty($TatDetails)){
				foreach ($TatDetails as $key => $value) {
					array_push($orderIds, $value['id']);
				}
			}
			if(!empty($orderIds)){
				$OrderDetails = $this->dashboard_model->get_orderDetails($orderIds,$this->input->post('status'),$this->input->post('oldStatus'));
				foreach ($OrderDetails as $key => $value) {
					if($value['updated_date'] == null || $value['updated_date'] == ''){
						$OrderDetails[$key]['updated_date']  = date('F d Y h:i A');
						$update_date = date_create(date('Y-m-d h:i'));
					}else{
						$update_date = date_create($value['updated_date']);
					}
					$invoice_date = date_create($value['invoice_date']);
					$InterVal =  date_diff($update_date,$invoice_date);
					$OrderDetails[$key]['interval'] = $InterVal->days.' Days '. $InterVal->h.' Hours '.$InterVal->i. ' Minutes';
				}
			}else{
				$OrderDetails = array();
			}

			header('Content-Type: application/json');
			echo json_encode($OrderDetails);exit;

		}
	}

	public function get_bulkDetails(){
		// print_r($this->input->post());exit;
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$array  = $this->input->post('array');
			if(array_key_exists('OutTan', $array) || array_key_exists('inTan', $array)){
				// print_r("expression");exit;
				$stats = $this->input->post('status');
				$oldStatus = $this->input->post('oldStatus');
				if(array_key_exists('OutTan', $array)){
					$Outtat = $this->get_details_forTat($array['OutTan'],$status,$oldStatus);
				}

				if(array_key_exists('inTan', $array)){
					$Intat = $this->get_details_forTat($array['inTan'],$status,$oldStatus);
				}


				if(!empty($Outtat) && !empty($Intat)){
					$response = array_merge($Intat,$Outtat);
				}else if(!empty($Outtat)){
					$response =$Outtat;
				}else{
					$response =$Intat;
				}
			}else{
				$response = array();
			}
			header('Content-Type: application/json');
			echo json_encode($response);exit;

		}
	}


	function get_details_forTat($array,$status,$oldStatus){
			$TatDetails = $array;
			$orderIds = array();
			if(!empty($TatDetails)){
				foreach ($TatDetails as $key => $value) {
					array_push($orderIds, $value['id']);
				}
			}
			if(!empty($orderIds)){
				$OrderDetails = $this->dashboard_model->get_orderDetails($orderIds,$status,$oldStatus);
				foreach ($OrderDetails as $key => $value) {
					if($value['updated_date'] == null || $value['updated_date'] == ''){
						$OrderDetails[$key]['updated_date']  = date('F d Y h:i A');
						$update_date = date_create(date('Y-m-d h:i'));
					}else{
						$update_date = date_create($value['updated_date']);
					}
					$invoice_date = date_create($value['invoice_date']);
					$InterVal =  date_diff($update_date,$invoice_date);
					$OrderDetails[$key]['interval'] = $InterVal->days.' Days '. $InterVal->h.' Hours '.$InterVal->i. ' Minutes';
				}
			}else{
				$OrderDetails = array();
			}

			return $OrderDetails;

	}
	
	public function getorderhistorylist(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_REQUEST['start']))? intval($_REQUEST['start']) : 0;
		    $length = (isset($_REQUEST['length']) && intval($_REQUEST['length']) > 0 )? $_REQUEST['length'] : 0;
			$orderIndex = (isset($_REQUEST['order'][0]['column']))? $_REQUEST['order'][0]['column'] : 0; 
	    	$orderType = (isset($_REQUEST['order'][0]['dir']))? $_REQUEST['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_REQUEST['search']['value']))? $_REQUEST['search']['value'] : NULL;

	    	if($_REQUEST['start_date'] !='' && $_REQUEST['end_date'] != ''){
			    $data['start_date'] = date('Y-m-d', strtotime($_REQUEST['start_date']));
				$data['end_date'] = date('Y-m-d',strtotime($_REQUEST['end_date']));
				$reportData = $this->dashboard_model->get_order_reports($data,$length,$start,$orderIndex,$orderType,$searchValue);
				$count = $this->dashboard_model->get_order_reports($data,$length,$start,$orderIndex,$orderType,$searchValue,'count');
			}else{
				$timestamp = time()-86400;
				$date = strtotime("-7 day", $timestamp);
				$data['start_date'] = date('Y-m-d', $date);
				$data['end_date'] = date('Y-m-d');
				$reportData = $this->dashboard_model->get_order_reports($data,$length,$start,$orderIndex,$orderType,$searchValue);
                $count = $this->dashboard_model->get_order_reports($data,$length,$start,$orderIndex,$orderType,$searchValue,'count');
			}
			foreach ($reportData as $key => $value) {
				$tat_settings = $this->dashboard_model->get_all_details(ORDER_STATUS_TAT,array('status_from' => $value['new_status']));
				$seconds = strtotime(date('Y-m-d h:i')) - strtotime($value['updated_date']);
				$hours = $seconds / 60 /  60;
				if($hours < $tat_settings->row()->duration){
					$reportData[$key]['status'] = 'InTat';
				}else{
					$reportData[$key]['status'] = 'OutTat';
				}
			}
	    	$jsonArray1 = array("draw" => intval($_REQUEST['draw']), "recordsTotal" => count($count), "recordsFiltered" => count($count));
	        $jsonArray1['data'] = $reportData;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}

	public function orderhistory(){
// 		// print_r($this->input->post());exit;
		if ($this->checkLogin('A') == ''){
// 			redirect('admin');
		}else {
// 			// print_r($_GET);exit;
// 			if(isset($_GET['date_from']) && isset($_GET['date_to'])){
// 				$reportData = $this->dashboard_model->get_order_reports($_GET);
// 			}else{
// 				$timestamp = time()-86400;
// 				$date = strtotime("-7 day", $timestamp);
// 				$data['date_from'] = date('Y-m-d', $date);
// 				$data['date_to'] = date('Y-m-d');
// 				$reportData = $this->dashboard_model->get_order_reports($data);
// 			}
// 			foreach ($reportData as $key => $value) {
// 				$tat_settings = $this->dashboard_model->get_all_details(ORDER_STATUS_TAT,array('status_from' => $value['new_status']));
// 				$seconds = strtotime(date('Y-m-d h:i')) - strtotime($value['updated_date']);
// 				$hours = $seconds / 60 /  60;
// 				if($hours < $tat_settings->row()->duration){
// 					$reportData[$key]['status'] = 'InTat';
// 				}else{
// 					$reportData[$key]['status'] = 'OutTat';
// 				}
// 			}
			$this->data['orderReports'] = $reportData;
// 			$this->data['heading'] = 'Order Reports';
			$this->load->view('admin/orderdashboard/orderReports',$this->data);
		}
	}

	public function checkForNewStatus($array,$val){
		if($val != null){
			foreach ($array as $key => $value) {
				if($value['new_status'] == $val){
					return $value;
				}
			}
		}else{
			return array();
		}
	}

	public function getorderhistory(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$OrderID = $this->input->post('order_id');
			$orderhistory = $this->dashboard_model->getorderhistoryDetails($OrderID);
			$Histroy = $orderhistory->result_array();

			$histroyArray = [];		
			foreach ($Histroy as $key => $value) {
				switch ($value['new_status']) {
					case 'New Order':
						$data = $this->checkForNewStatus($Histroy,'Document Complete');
					break;
					
					case 'Document Complete':
						$data = $this->checkForNewStatus($Histroy,'Invoice Created');
						break;
					
					case 'Invoice Created':
						$data = $this->checkForNewStatus($Histroy,'Product Out of Stock');
						break;
					
					case 'Product Out of Stock':
						$data = $this->checkForNewStatus($Histroy,'Delivery Scheduled');
						break;

					case 'Delivery Scheduled':
						$data = $this->checkForNewStatus($Histroy,'Delivery Done');
						break;

					case 'Delivery Done':
						$data = $this->checkForNewStatus($Histroy,'Payment Doc Proof Pending');
						break;
					case 'Payment Doc Proof Pending':
						$data = $this->checkForNewStatus($Histroy,'Payment Doc Validation Pending');
						break;
					case 'Payment Doc Validation Pending':
						$data = $this->checkForNewStatus($Histroy,'Payment Doc Dispatch Pending');
						break;
					case 'Payment Doc Dispatch Pending':
						$data = $this->checkForNewStatus($Histroy,'Closed');
						break;
					case 'Closed':
						$data = $this->checkForNewStatus($Histroy,null);
						break;
					default:
						break;
				}

				if($value['new_status'] == 'New Order'){
					$invoice_date = date_create($value['invoice_date']);
					$histroyArray[$key]['status_in_time'] = $value['invoice_date'];
				}else{
					$invoice_date = date_create($value['updated_date']);
					$histroyArray[$key]['status_in_time'] = $value['updated_date'];
				}
				if(!empty($data)){
					$update_date = date_create($data['updated_date']);
					$histroyArray[$key]['status_to']  = $data['new_status'];
					$histroyArray[$key]['status_out_time'] = $data['updated_date'];
				
				}else{
					$update_date = date_create(date('Y-m-d h:i'));
					$histroyArray[$key]['status_to']  = 'NA';
					$histroyArray[$key]['status_out_time'] = date('Y-m-d h:i');
				}
					$histroyArray[$key]['case_owner'] = $value['owner_name'];
					$histroyArray[$key]['status_from'] = $value['new_status'];
					$InterVal =  date_diff($update_date,$invoice_date);
					$histroyArray[$key]['hours_diff'] = $InterVal->days.' Days '. $InterVal->h.' Hours '.$InterVal->i. ' Minutes';
			}
			header('Content-Type: application/json');
			echo json_encode($histroyArray);exit;
			// print_r($histroyArray);exit;
		}
	}
	
	
}
