<?php  
// if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    require 'vendor/autoload.php';

/**
 * 
 * CMS related functions
 * @author Teamtweaks
 *
 */

class Crone extends MY_Controller {
	function __construct(){
        parent::__construct();
// 		 if (!$this->input->is_cli_request()) show_error('Direct access is not allowed');
		$this->load->model('product_model');
		$this->load->model('Cms_model');
		$this->load->model('order_model');
		error_reporting(0);
    }

    
    public function getClient()
	{
    	$client = new Google_Client();
	    $client->setClientId('881003059391-a0vl5m6cbidc51mgeegfbfqfcfpnr83c.apps.googleusercontent.com');
	    $client->setClientSecret('rrQG2uVHfgtzCQnOy4QdYG9q');
	    $client->setAccessType('offline');
	    $client->refreshToken('1/qO_cgTtpx5aE5Wqqj_1kgNIRFvUtpoh3HVkGLdyOE9qowax2zebve1vot0agjc50');
    	return $client;
	}


    //Get and store abonded sing up and signup with cart value in google sheet.
    public function GetTodayReports(){

		$client = $this->getClient();
		$service = new Google_Service_Sheets($client);
		$Abandoned_Cart_spreadsheetId =  '1Yg-zA7Vv1AqIGa2EwTDmxkCx_k5kjfnW80NfH0SY4Hc';
		$Abandoned_SignUp_spreadsheetId = '1-V6X9lGOENlvqsKRenhndC7j5YJ1QG2fua4Aq9f2uEY';
		$Abandoned_Cart_range = 'A2:U';
		$Abandoned_SignUp_range = 'A2:O';
    	$time = time();
    	$curr_date = date("Y-m-d", mktime(0,0,0,date("n", $time),date("j",$time)- 1 ,date("Y", $time)));
    	$for_mail_formate = date("d-m-y", mktime(0,0,0,date("n", $time),date("j",$time)- 1 ,date("Y", $time)));

 		$Admin_setting = $this->Cms_model->get_all_details(ADMIN_SETTINGS,array('id','1'));
    	$this->data['OnlyNewSignUpUsers'] = $this->Cms_model->Abandoned_SignUp_Users($curr_date);
    	$this->data['SignUpWithCart'] = $this->Cms_model->Abandoned_Cart($curr_date);

    	if(!empty($this->data['OnlyNewSignUpUsers'])){
			foreach ($this->data['OnlyNewSignUpUsers'] as $user ) {
				$datatocsv = [[$user->id, $user->full_name, $user->user_name, $user->email, $user->status, $user->created, $user->last_login_date, $user->address, $user->address2, $user->city, $user->district, $user->state, $user->country, $user->postal_code, $user->phone_no]];
				$response = $service->spreadsheets_values->get($Abandoned_SignUp_spreadsheetId, $Abandoned_SignUp_range);
				$options = array('valueInputOption' => 'RAW');
				$body   = new Google_Service_Sheets_ValueRange(['values' => $datatocsv]);
				$result = $service->spreadsheets_values->append($Abandoned_SignUp_spreadsheetId, 'A1:O', $body, $options);
			}
    	}
    	if(!empty($this->data['SignUpWithCart'])){
			foreach ( $this->data['SignUpWithCart'] as $new_key => $user ) {
				$mini_cart_items  = $this->minicart_model->items_in_cart($user->user_id);
				$new_total = [];
				$cartDiscountAmt = '';
				$product_shipping_cost = '';
				$cartAmt = '';
				$grantAmt = '';
				$product_name = '';	
				foreach ($mini_cart_items as $key => $product) {
					    $product_name .= $product->product_name.'|';
 		                $cartDiscountAmt = $cartDiscountAmt + ($product->discountAmount * $product->quantity);
					 	$product_shipping_cost = $product_shipping_cost + ($product->product_shipping_cost * $product->quantity);
		                $cartAmt = $cartAmt + (($product->price - $product->discountAmount + ($product->price * 0.01 * $product->product_tax_cost)) * $product->quantity);
		                $cartTAmt = ($cartAmt * 0.01 * 0);
		                $grantAmt = $cartAmt + $product_shipping_cost + $cartTAmt;
	            	}
            		if(isset($mini_cart_items[0]->attr_name)){
            		    $tenure = $mini_cart_items[0]->attr_name;
            		}else{
            		     $tenure = '';
            		}
					
		           	$product_name_temp = rtrim($product_name,'|');
					$datatocsv = [[ $user->user_id, $user->full_name, $user->user_name, $user->email, $user->status, $user->created, $user->last_login_date, $user->address, $user->address2, $user->city, $user->district, $user->state, $user->country, $user->postal_code, $user->phone_no,$product_name_temp , $tenure , $user->couponCode , $product_shipping_cost ,$cartDiscountAmt, $grantAmt]];
					$response = $service->spreadsheets_values->get($Abandoned_Cart_spreadsheetId, $Abandoned_Cart_range);
					$options = array('valueInputOption' => 'RAW');
					$body   = new Google_Service_Sheets_ValueRange(['values' => $datatocsv]);
					$result = $service->spreadsheets_values->append($Abandoned_Cart_spreadsheetId, 'A1:U', $body, $options);
			}
    	}
    
    	echo "success";
 
    }
    
    
    public function GetAbondedSignupAndCarts(){
    	$Admin_setting = $this->Cms_model->get_all_details(ADMIN_SETTINGS,array('id','1'));
    	$time = time();
    	$curr_date = date("Y-m-d", mktime(0,0,0,date("n", $time),date("j",$time) -1  ,date("Y", $time)));
    	$for_mail_formate = date("d-m-y", mktime(0,0,0,date("n", $time),date("j",$time) -1 ,date("Y", $time)));
    	
    	$time_two_hour_ago =  date('d_M_Y_h_i_A', $time);
    	$time_two_hour_before =  date('d_M_Y_h_i_A', strtotime('-2 hours', $time));
    	
    	$this->data['OnlyNewSignUpUsers'] = $this->Cms_model->Abandoned_SignUp_Users($curr_date);
    	$this->data['SignUpWithCart'] = $this->Cms_model->Abandoned_Cart($curr_date);
    	if(!empty($this->data['OnlyNewSignUpUsers'])){
    // 		$new_user_csvName = 'Abandoned_SignUp_'.$for_mail_formate.'.csv';
            $new_user_csvName = 'Abandoned_SignUp_'.$time_two_hour_ago.'_to_'.$time_two_hour_before.'.csv';
			$header = array('Id','Full Name','User Name','Email','Status','Created','Last Login Date','Address','Address2','City','District','State','Country','Postal Code','Phone_no');
			$fp = fopen(CSV_FILE_PATH.$new_user_csvName, 'w+');
			fwrite($fp, implode(',', $header)."\n");
			foreach ($this->data['OnlyNewSignUpUsers'] as $user ) {
				$datatocsv = array($user->id, $user->full_name, $user->user_name, $user->email, $user->status, $user->created, $user->last_login_date, $user->address, $user->address2, $user->city, $user->district, $user->state, $user->country, $user->postal_code, $user->phone_no);
				fwrite($fp, implode(',', $datatocsv)."\n");
			}
			fclose($fp);
    	}else{
    // 		$new_user_csvName = 'Abandoned_SignUp_'.$for_mail_formate.'.csv';
            $new_user_csvName = 'Abandoned_SignUp_'.$time_two_hour_before.'_to_'.$time_two_hour_ago.'.csv';
			$header = array('No Abandoned User Found');
			$fp = fopen(CSV_FILE_PATH.$new_user_csvName, 'w+');
			fwrite($fp, implode(',', $header)."\n");
			$datatocsv = array();
			fwrite($fp,'');
			fclose($fp);
    	}
    	if(!empty($this->data['SignUpWithCart'])){
        //   	$cart_csvName = 'Abandoned_Cart_'.$for_mail_formate.'.csv';
            $cart_csvName = 'Abandoned_Cart_'.$time_two_hour_before.'_to_'.$time_two_hour_ago.'.csv';
			$header = array('Id','Full Name','User Name','Email','Status','Created','Last Login Date','Address','Address2','City','District','State','Country','Postal Code','Phone Number','Product Name','Tenure','Quantity','Coupon Code','Price','Shipping Cost','Discount Amount','Total');
			$fp = fopen(CSV_FILE_PATH.$cart_csvName, 'w+');
			fwrite($fp, implode(',', $header)."\n");
			foreach ( $this->data['SignUpWithCart'] as $user ) {
				// $mini_cart_items  = $this->minicart_model->items_in_cart($user->user_id);
				$Product_details = $this->Cms_model->get_all_details(PRODUCT ,array( 'id' =>  $user->product_id));
				$Product_tenure = $this->Cms_model->get_all_details(SUBPRODUCT ,array( 'pid' =>  $user->attribute_values));
				$datatocsv = array($user->user_id, $user->full_name, $user->user_name, $user->email, $user->status, $user->created, $user->last_login_date, $user->address, $user->address2, $user->city, $user->district, $user->state, $user->country, $user->postal_code, $user->phone_no,$Product_details->row()->product_name,$Product_tenure->row()->attr_name,$user->quantity,$user->couponCode,$user->price,$user->product_shipping_cost,$user->discountAmount,$user->indtotal);
				fwrite($fp, implode(',', $datatocsv)."\n");

			}
			fclose($fp);
    	}
    	else{
    // 		$cart_csvName = 'Abandoned_Cart_'.$for_mail_formate.'.csv';
            $cart_csvName = 'Abandoned_Cart_'.$time_two_hour_before.'_to_'.$time_two_hour_ago.'.csv';
			$header = array('No Abandoned Cart Found');
			$fp = fopen(CSV_FILE_PATH.$cart_csvName, 'w+');
			fwrite($fp, implode(',', $header)."\n");
			$datatocsv = array();
			fwrite($fp,'');
			fclose($fp);
    	}


		$email_values = array('mail_type'=>'html',
                 'from_mail_id'=> $Admin_setting->row()->site_contact_mail,
                 'mail_name'=>'Daily Report',
                 'to_mail_id'=> $Admin_setting->row()->report_receipt_mail,
                //  'subject_message'=> 'Abandoned Reports '.$for_mail_formate.'',
                  'subject_message'=> 'Abandoned Reports From '.$time_two_hour_before.' To '.$time_two_hour_ago.'',
                 'body_messages'=> 'SignUP and Cart Abandoned reports for Date :' .$for_mail_formate.'',
                 'attachment1' => $new_user_csvName,
                 'attachment2' => $cart_csvName
		);
	 	$email_send_to_common = $this->Cms_model->common_email_send($email_values);

   		unlink(CSV_FILE_PATH.$new_user_csvName);
   		unlink(CSV_FILE_PATH.$cart_csvName);
    }
    
    public function getFailedOrders() {
		$date = date('Y-m-d');

		$this->db->select('*');
		$this->db->from('fc_payment');
		$this->db->where('is_failed_mail_sent = "0" and is_offline_placed = "0" and status = "Pending" and DATE(modified) = "'.$date.'" and modified <= DATE_SUB(now(), INTERVAL 5 MINUTE) and modified > DATE_SUB(now(), INTERVAL 10 MINUTE)');
		$this->db->group_by('dealCodeNumber');
		$orders = $this->db->get();

		foreach($orders->result() as $val) {
			$this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
			$this->db->from(PAYMENT . ' as p');
			$this->db->join(USERS . ' as u', 'p.user_id = u.id');
			$this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
			$this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
			$this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
			$this->db->where('p.dealCodeNumber="' . $val->dealCodeNumber . '"');
			$PrdList = $this->db->get();

			$this->db->select('p.sell_id,p.couponCode,u.email');
			$this->db->from(PAYMENT . ' as p');
			$this->db->join(USERS . ' as u', 'p.sell_id = u.id');
			$this->db->where('p.dealCodeNumber="' . $val->dealCodeNumber . '"');
			$this->db->group_by("p.sell_id");
			$SellList = $this->db->get();

			$isSend = $this->order_model->new_failed_order_email($PrdList, $SellList);
		    $this->db->where('dealCodeNumber', $val->dealCodeNumber);
			$this->db->update('fc_payment', array('is_failed_mail_sent' => '1'));
		}
	}
	
	
// 	function create_csv_string() {
//  		$Admin_setting = $this->Cms_model->get_all_details(ADMIN_SETTINGS,array('id','1'));
//     	$data = $this->Cms_model->get_last_seven_days_report();
// 	    $array = [];
// 	    if($data->num_rows() > 0){
// 	    	foreach ($data->result_array() as $key => $value) {
// 	    		$array[$value['product_id']][] = $value;
// 	    	}
// 	    	$city_array =  [];
// 	    	$new_array = [];
// 	    	foreach ($array as $key => $value) {
// 	    		foreach ($value as $k => $v) {
// 	    			if($key == $v['product_id']){
// 	    				$new_array[$key]['id'] = $v['product_id'];	
// 	    				$new_array[$key]['name'] = $v['product_name'];
// 	    				$new_array[$key]['city'][$v['city']] = $v['total_sold'];
// 	    				$city_array[] = $v['city'];
// 	    			}
// 	    		}    	
// 	    	}
// 	    	$start_date = date("d M",strtotime($data->result_array()[0]['created']));
// 	    	$end_date = date("d M");
// 	    	$year = date('Y');
// 	    	$city_array = array_unique($city_array);
// 	    	if(!empty($new_array)){
// 	    		$new_user_csvName = "Product ordered online  $start_date - $end_date $year.csv";
// 				$header = array('Product ID','Product Name');
// 				$new_header = array_merge($header,$city_array);
// 				array_push($new_header, 'Total');
// 				$fp = fopen(CSV_FILE_PATH.$new_user_csvName, 'w+');
// 				fwrite($fp, implode(',', $new_header)."\n");
// 				foreach ($new_array as $val ) {
// 					$product_count = 0;
// 					$datatocsv = array($val['id'],$val['name']);
// 					$city_vise_array = $this->abc($new_header,$val);
// 					foreach ($city_vise_array as $key => $value) {
// 						$datatocsv[$key] = $value;
// 						$product_count += $value;
// 					}
// 					$datatocsv[end(array_keys($new_header))] = $product_count;
// 					fwrite($fp, implode(',', $datatocsv)."\n");
// 				}
// 				fclose($fp);
// 	    	}else{
// 	    		$new_user_csvName = "Product ordered online  $start_date - $end_date $year.csv";
// 				$header = array('No Order Found');
// 				$fp = fopen(CSV_FILE_PATH.$new_user_csvName, 'w+');
// 				fwrite($fp, implode(',', $header)."\n");
// 				$datatocsv = array();
// 				fwrite($fp,'');
// 				fclose($fp);
// 	    	}

// 	    	$email_values = array('mail_type'=>'html',
// 	                 'from_mail_id'=> $Admin_setting->row()->site_contact_mail,
// 	                 'mail_name'=>'Daily Report',
// 	                 'to_mail_id'=> $Admin_setting->row()->weekly_report_email,
// 	                 'subject_message'=> "Products ordered online:  $start_date - $end_date $year",
// 	                 'body_messages'=> 'Weekly report of citywise products ordered from site.',
// 	                 'attachment1' => $new_user_csvName
// 			);
// 		 	$email_send_to_common = $this->Cms_model->common_email_send($email_values);
// 	    }

// 	}

    function create_csv_string() {
 		$Admin_setting = $this->Cms_model->get_all_details(ADMIN_SETTINGS,array('id','1'));
    	$data = $this->Cms_model->get_last_seven_days_report();
	    $array = [];
	    if($data->num_rows() > 0){
	    	foreach ($data->result_array() as $key => $value) {
	    		$array[$value['product_id']][] = $value;
	    	}
	    	$city_array =  [];
	    	$new_array = [];
	    	
	    	$product_id_array = [];

	    	foreach ($array as $key => $value) {

	    		foreach ($value as $k => $v) {
	    			if($key == $v['product_id'] && $v['subproducts'] == ''){
	    				$new_array[$key]['id'] = $v['product_id'];	
	    				$new_array[$key]['name'] = $v['product_name'];
	    				$new_array[$key]['city'][$v['city']] = $v['total_sold'] * $v['quantity'];
	    				$city_array[] = $v['city'];
	    			}else{
	    				$product_id = explode(',', $v['subproducts']);
	    				$product_data = $this->Cms_model->get_last_seven_days_report_on_products($product_id);
	    				foreach ($product_data->result_array() as $pkey => $pvalue) {
							if(array_key_exists($pid, $product_id_array)){
	    						$product_id_array[$pkey]['id'] = $pvalue['product_id'];	
			    				$product_id_array[$pkey]['name'] = $pvalue['product_name'];
			    				$product_id_array[$pkey]['city'][$v['city']] = ($v['total_sold'] * $v['quantity']) + 1;
			    				$city_array[] = $v['city'];
	    					}else{
	    						$product_id_array[$pkey]['id'] = $pvalue['product_id'];	
			    				$product_id_array[$pkey]['name'] = $pvalue['product_name'];
			    				$product_id_array[$pkey]['city'][$v['city']] = $v['total_sold'] * $v['quantity'];
			    				$city_array[] = $v['city'];
	    					} 
	    				}

	    			}
	    		}    	
	    	}

	    	$new_array = array_merge($new_array,$product_id_array);
	    	$start_date = date("d M",strtotime($data->result_array()[0]['created']));
	    	$end_date = date("d M");
	    	$year = date('Y');
	    	$city_array = array_unique($city_array);
	    	if(!empty($new_array)){
	   // 		$new_user_csvName = "Product ordered online  $start_date - $end_date $year.csv";
	    	    $new_user_csvName = "Product ordered online  $start_date $year.csv";
				$header = array('Product ID','Product Name');
				$new_header = array_merge($header,$city_array);
				array_push($new_header, 'Total');
				$fp = fopen(CSV_FILE_PATH.$new_user_csvName, 'w+');
				fwrite($fp, implode(',', $new_header)."\n");
				foreach ($new_array as $val ) {
					$product_count = 0;
					$datatocsv = array($val['id'],$val['name']);
					$city_vise_array = $this->abc($new_header,$val);
					foreach ($city_vise_array as $key => $value) {
						$datatocsv[$key] = $value;
						$product_count += $value;
					}
					$datatocsv[end(array_keys($new_header))] = $product_count;
					fwrite($fp, implode(',', $datatocsv)."\n");
				}
				fclose($fp);
	    	}else{
	   // 		$new_user_csvName = "Product ordered online  $start_date - $end_date $year.csv";
	    		$new_user_csvName = "Product ordered online  $start_date  $year.csv";
				$header = array('No Order Found');
				$fp = fopen(CSV_FILE_PATH.$new_user_csvName, 'w+');
				fwrite($fp, implode(',', $header)."\n");
				$datatocsv = array();
				fwrite($fp,'');
				fclose($fp);
	    	}

	    	$email_values = array('mail_type'=>'html',
	                 'from_mail_id'=> $Admin_setting->row()->site_contact_mail,
	                 'mail_name'=>'Daily Report',
	                 'to_mail_id'=> $Admin_setting->row()->weekly_report_email,
	                 'subject_message'=> "Products ordered online:  $start_date $year",
	                 'body_messages'=> 'Weekly report of citywise products ordered from site.',
	                 'attachment1' => $new_user_csvName
			);
		 	$email_send_to_common = $this->Cms_model->common_email_send($email_values);
	    }

	}

	function abc($new_header,$val){
		$array = [];
		foreach ($new_header as $key => $value) {
			foreach ($val['city'] as $k => $v) {
				if($k == $value ){
					 $array[$key] = $val['city'][$k]; 
				}
				else{
					if($key != 0 && $key != 1){
						if($array[$key] == '' || $array[$key] == ''){
						 $array[$key] = 0; 
						}
					}
				}
			}
		}
		return $array;
	}
	
	
	
	public function get_state(){
		switch ($this->input->post('city')) {
			case 'Bangalore':
				$state = 'Karnataka';
				break;
			case 'Delhi':
				$state = 'Delhi';
				break;
			case 'Ghaziabad':
				$state = 'Uttar Pradesh'; 
				break;
			case 'Gurgaon':
				$state = 'Haryana';
				break;
			case 'Mumbai':
				$state = 'Maharashtra';
				break;
			case 'Noida':
				$state = 'Uttar Pradesh';
				break;
			case 'Pune':
				$state = 'Maharashtra';
				break;
			default:
				$state = '';
				break;
		}

		$cityarray = array('message' => 'State Found', 'data'=> $state);
		echo json_encode($cityarray);
	}
	
	
	public function get_today_commission_for_brokers(){
	 	$type = "Request";
	  	$sub_type = "Broker Payment";
	  	$status = "Payment Pending";
	  	$caseOwner = '387430131'; //finance ID
	  	// $caseOwner = '82995370';
	  	$subject = 'Broker Payment Request';
		$all_broker_comissions = $this->Cms_model->get_broker_comissions();
		if($all_broker_comissions->num_rows() > 0){
			foreach ($all_broker_comissions->result_array() as $key => $value) {
				$lead_data = $this->Cms_model->get_all_lead_details($value['bd_user_id']);
				$leadIDS = [];
				if($lead_data->num_rows() > 0){
					$DynamicTable = '';
					$total_Amt = '';
					$DynamicTable .= 'Broker Name :'.$value['full_name'].' \n';
					$DynamicTable .= 'Broker Email :'.$value['user_email'].' \n';
					$DynamicTable .= 'Broker Phone :'.$value['broker_phone'].' \n\n';
					$DynamicTable .= 'Lead_Name | Lead_Email | Lead_Phone | Lead_Comission | OrderID';
					foreach ($lead_data->result_array() as $k => $val) {
						$total_Amt += $val['commission_rate'];
						$DynamicTable .= ' \n \n '.$val['first_name'].' '.$val['last_name'].' | '.$val['email'].' | '. $val['phone_no']. ' |  Rs '.$val['commission_rate'].' |  #'.$val['order_id'];
					    $leadIDS[] = array(
							'id' => $val['id'],
							'case_id' => '',
							'is_case_created' => '1'
						);
					    
					}
				
					$DynamicTable .= ' \n \n Total Comission: Rs '.$total_Amt.' \n \n Please access this url for lead details: '.base_url().'show_comissions_listing/'.$value['bd_user_id'].' \n \n ';
						$createCaseURL = 'https://www.zohoapis.com/crm/v2/Cases';
				// 		$headers = [
				// 		  	'Authorization: '.CRM_AUTH_TOKEN
				// 	  	];
				        
				        $acceess_token = $this->get_zohaccess_token();
					    $headers = [
                   		    'Content-Type:application/json',
                            'Authorization:'.$acceess_token
                        ];
			  			
			  			$Invoice =   '{
				  			"data" : [
							 	{
									"Priority":"Medium",
									"Type":"'.$type.'",
									"Sub_Type":"'.$sub_type.'",
									"Subject":"'.$subject.'",
									"Owner":"'.$caseOwner.'",
									"Related_To":"2486101000047835227",
									"LeadOwner":"'.$value['bd_user_id'].'",
									"Case_Origin":"Website",
									"Status":"'.$status.'",
									"Description": "'.$DynamicTable.'",
									"Broker_Name":"'.$value['full_name'].'"
						  		}
					  		]
				  		}';

						$curl = curl_init($createCaseURL);
			    		curl_setopt_array($curl, array(
			        		CURLOPT_POST => 1,
			        		CURLOPT_HTTPHEADER => $headers,
			        		CURLOPT_POSTFIELDS => $Invoice,
			        		CURLOPT_RETURNTRANSFER => true
						));
						$customer_payment = curl_exec($curl);
						$respo = json_decode($customer_payment);
					    if(array_key_exists("data",$respo)){
                     		$caseID = $respo->data[0]->details->id;
    						foreach($leadIDS as $key => $value)
    						{
    						  $leadIDS[$key]['case_id'] = $caseID;
    						}
    						$this->db->update_batch(BD_LEADS,$leadIDS, 'id');
                 	    }
				}
			}	
		}
	}

	public function get_commissions_details(){
	    $BrokerID = $this->uri->segment(2, 0);
	    $lead_data = $this->Cms_model->get_all_lead_details($BrokerID);
		$total_Amt = '';
		$DynamicTable = '';
		foreach ($lead_data->result_array() as $k => $val) {
			$total_Amt += $val['commission_rate'];
			$DynamicTable .=
				'<tr>
					<td>'.$val['first_name'].' '.$val['last_name'].'</td>
					<td>'.$val['email'].'</td>
					<td>'.$val['phone_no'].'</td>
					<td>Rs '.$val['commission_rate'].'</td>
					<td>#'.$val['order_id'].'</td>
				</tr>';
		}


		$htttml = '<style>
			#customers {
			  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
			  border-collapse: collapse;
			}

			#customers td, #customers th {
			  border: 1px solid #ddd;
			  padding: 8px;
			}

			#customers tr:nth-child(even){background-color: #f2f2f2;}

			#customers tr:hover {background-color: #ddd;}

			#customers th {
			  padding-top: 12px;
			  padding-bottom: 12px;
			  text-align: left;
			  background-color: #4CAF50;
			  color: white;
			}
			</style><table  id="customers" style="border:1px solid black"> 
			<tr>
			<th>Lead Name</th>
			<th>Lead Email</th>
			<th>Lead Phone</th>
			<th>Lead Comission</th>
			<th>Order ID</th>
			</tr><tbody>
				'.$DynamicTable.'
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td>Total Comission</td>
					<td>Rs '.$total_Amt.'</td>
				</tr>
			</tbody>
			</table>';

		echo $htttml;exit;

	}

	public function get_and_update_broker_payout(){
		$case_id = $this->input->post('case_id');
		$lead_owner = $this->input->post('lead_owner');
		$status = $this->input->post('status');
		$created_date = $this->input->post('created_date');
		if($status == 'Payment Done'){
		 	$lead_data = $this->Cms_model->get_all_lead_details($lead_owner,null,$case_id);
		 	if($lead_data->num_rows() > 0){
		 		foreach ($lead_data->result_array() as $key => $value) {
					$updateArray[] = array(
				        'id'=> $value['lead_cm_id'],
				        'status' => 'paid'
				    );
		 		}
		 		$this->db->update_batch(BD_LEAD_COMMISSION,$updateArray, 'id'); 
		 	}
		}
	}

	
	




}