<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require (BASE_URL.'libraries/REST_Controller.php');
/**
 * device_id,os_is,access_token  are presents in headers in all apis,Expcept login and Forgot password APIS 
 * Parameters are passed as json object with api name as key
 * All apis first validate access_token and also checks if user is blocked or not
 * All api authentication logic is in auth_model
 * All apis model function has same name as api name removing _method
 */
class Mapi extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('encrypt','form_validation','Mobile_Detect'));
        $this->load->helper(array('cookie','date','form','email'));
        $this->load->model(array('user_model','product_model'));
         $result = array('message' => 'Please reinstall our latest app from play store','status_code' => 200, 'user_data' => array() );
         $this->sendResponse($result,false);
    }

    //Validation Function
    /*
        @params params* : array of required parametrs  
    */
    function validateRequest($params=array(),$requestedData=array()){
        $_POST = $requestedData;
        foreach($params as $key=>$val){
            $this->form_validation->set_rules($key, $val, 'trim|required');                   
        }

        if ($this->form_validation->run() == FALSE){
            return $this->form_validation->error_array();
        }else{
            return FALSE;
        }
    }


    function SocailLogin(){
        // print_r($_POST);exit;
        $_POST = $this->input->post();
        $requiredParams = array("email"=>"email");

        $isValid = $this->validateRequest($requiredParams,$_POST);
         // print_r($isValid);exit;
        
        if ($isValid)
        {            
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;
        }



        $query = $this->db->query("SELECT * FROM ".USERS." WHERE email='".$this->input->post('email')."' LIMIT 1");
            if($query->num_rows() > 0){
                // print_r($_POST);exit;
            $result['ok'] = true;
            $row = $query->result();
            $user_data  = array(
                'id' => $row[0]->id,
                'email' => $row[0]->email,
                'full_name' => $row[0]->full_name,
                'status' => $row[0]->status,
                 'user_type' => $row[0]->user_type
                        );
            // print_r($user_data);
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $newdata = array(
               'last_login_date' => mdate($datestring,$time),
               'last_login_ip' => $this->input->ip_address()
            );
            $condition = array('id' => $row[0]->id);
            $this->user_model->update_details(USERS,$newdata,$condition);
            
            
            $result = array('message' => 'Success','status_code' => 200, 'user_data' => $user_data );
           

        }else{
            $this->db->query("INSERT INTO ".USERS." SET loginUserType='".$this->input->post('login_type')."',full_name='".$this->input->post('first_name')." ".$this->input->post('last_name')."',email='".$this->input->post('email')."',status='Active',password='".md5(rand(0,10000))."',user_name='".$this->input->post('first_name')."".rand(1,10000)."',last_login_date=now(),created=now(),modified=now(),last_logout_date=now(),birthday='".date('Y-m-d')."',user_type='broker'");
           
            $insert_id = $this->db->insert_id();
            $query = $this->db->query("SELECT * FROM ".USERS." WHERE id='".$insert_id."' LIMIT 1");
            $row = $query->result();
              $user_data  = array(
                'id' => $row[0]->id,
                'email' => $row[0]->email,
                'full_name' => $row[0]->full_name,
                'status' => $row[0]->status,
                'user_type' => $row[0]->user_type
                        );

            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $newdata = array(
               'last_login_date' => mdate($datestring,$time),
               'last_login_ip' => $this->input->ip_address()
            );
            $condition = array('id' => $row[0]->id);
            $this->user_model->update_details(USERS,$newdata,$condition);
            $result = array('message' => 'Success','status_code' => 200, 'user_data' => $user_data );
        }
        
        $this->sendResponse($result,false);

    }

    /**
     * Validate form inputs
     * This function validates form inputs data
     * @param array params   
    **/
    function validateFormData($params=array(),$requestedData=array()){
        $_POST = $requestedData;
        foreach($params as $key=>$val){
            $this->form_validation->set_rules($key, $key, $val);                   
        }

        if ($this->form_validation->run() == FALSE){
            return $this->form_validation->error_array();
        }else{
            return FALSE;
        }
    }

    /*
        General Functions statrs
    */
    private function encryptData($data){
        $encryptedData = $this->encryption->encrypt($data);
        
        return $encryptedData;
    }

    private function decryptData($data){
        
        $decryptedData = $this->encryption->decrypt($data);
        
        return json_decode($decryptedData,1);
    }
    /**
        * This function creates response send to the client
        * @param array|stdclass $data
        * @param bool $doEncrypt
    **/
    private function sendResponse($data,$doEncrypt=true){   
        $code = (!empty($data['code']) ? $data['code']:$data['status_code']);
        unset($data['code']);
        $response = $data;
        if($doEncrypt == true){
            $response = json_encode($response);
            $response = $this->encryptData($response);
        }
        
        // $this->response(array("response"=>$response),$code); 
        echo json_encode(array("response"=>$response),$code);       
    }


    public function login(){
        $_POST = $this->input->post();

        $requiredParams = array("email"=>"Email","password" => "Password");


        $isValid = $this->validateRequest($requiredParams,$_POST);
        
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;   
        }

        // print_r($_POST['email']);exit;

        $query = $this->db->query("SELECT * FROM ".USERS." WHERE email='".$this->input->post('email')."' LIMIT 1");
            if($query->num_rows() > 0){
                // print_r("expression");exit;
                $row_data = $query->row();
                $new_password =  md5($this->input->post('password'));
                if($new_password == $row_data->password){
                    $user_data  = array(
                        'id' => $row_data->id,
                        'email' => $row_data->email,
                        'full_name' => $row_data->full_name,
                        'status' => $row_data->status,
                         'user_type' => $row_data->user_type
                        );
                    $result = array('message' => 'Login Success','status_code' => 200, 'user_data' => $user_data );
                              
                }else{
                    $user_data  = array(
                        'email' => $_POST['email'],
                        'password' => $_POST['password']
                    );
                    $result = array('message' => 'Invalid Password','status_code' => 400, 'user_data' => $user_data );
                }
            }else{
                $user_data  = array(
                        'email' => $_POST['email'],
                        'password' => $_POST['password']
                    );
                $result = array('message' => 'Invalid Login Details','status_code' => 400, 'user_data' => $user_data );
               
            }
        
        $this->sendResponse($result,false);
    }


    public function signup(){
        $_POST = $this->input->post();

        $requiredParams = array("full_name"=>"Full Name","email" => "email","password"=>"Password");


        $isValid = $this->validateRequest($requiredParams,$_POST);
        
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;   
        }
         $query = $this->db->query("SELECT * FROM ".USERS." WHERE email='".$this->input->post('email')."' LIMIT 1");
            if($query->num_rows() > 0){
                $user_data  = array(
                        'email' => $_POST['email'],
                );
                $result = array('message' => 'User Exists','status_code' => 400, 'user_data' => $user_data );
                // print_r("user elready exists");exit;
            }else{

                $response =  $this->db->query("INSERT INTO ".USERS." SET loginUserType='',full_name='".$this->input->post('full_name')."',email='".$this->input->post('email')."',status='Active',password='".md5($this->input->post('password'))."',user_name='".$this->input->post('full_name')."',last_login_date=now(),created=now(),modified=now(),last_logout_date=now(),birthday='".date('Y-m-d')."',user_type='".$this->input->post('type')."'");
                $last_insert_id  = $this->db->insert_id();
                 $user_type = $this->db->query("SELECT * FROM ".USERS." WHERE id='".$last_insert_id."' LIMIT 1");
                  $row_data = $user_type->row();
                if($response){
                    $user_data  = array(
                        'id' => $last_insert_id,
                        'email' => $_POST['email'],
                        'full_name' => $_POST['full_name'],
                        'status' => 'Active',
                         'user_type' => $row_data->user_type
                        );
                    $result = array('message' => 'Registration Success','status_code' => 200, 'user_data' => $user_data );
                }else{
                    $user_data  = array(
                        'email' => $_POST['email'],
                    );
                    $result = array('message' => 'Something Went wrong','status_code' => 400, 'user_data' => $user_data );

                }
               // $response =  $this->user_model->insertUserQuick($_POST['full_name'],$_POST['email'],$_POST['password']);
                // print_r($response);exit;
            }


        
        $this->sendResponse($result,false);
    }



    public function forgot_password(){
        $_POST = $this->input->post();
        $requiredParams = array("email"=>"Email");
        $isValid = $this->validateRequest($requiredParams,$_POST);
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;   
        }else{
             $query = $this->db->query("SELECT * FROM ".USERS." WHERE email='".$this->input->post('email')."' LIMIT 1");
            if($query->num_rows() > 0){
                $token = mt_rand();
                $email = $this->input->post('email');
                $data['token'] = $token;
                $data['check_current_time'] =   date("Y-m-d h:i:s");
                $result =  $this->user_model->store_token_date($data,$email);
                $get_token = $this->user_model->get_token($email);
                    $link = base_url()."reset-password/".$get_token[0]->token; 
                    //$check = '<a href ='.$link.'>Click Here</a>';
                    $check = '<a href='.$link.' style="display:inline-block;text-decoration:none;font-weight:bold;margin-top:30px;">
                <img src="http://180.211.99.165/design/cityfurnish/email/images/reset-pass-btn.png" />
                </a>';
                $response = $this->send_user_password($check,$query);
                $result = array('message' => 'Password Reset link Send Successfully','status_code' => 200);

                
            }else{
                $user_data  = array(
                        'email' => $_POST['email'],
                    );
                $result = array('message' => 'No User Found','status_code' => 400, 'user_data' => $user_data );
                
            }

        }
        $this->sendResponse($result,false);

    }

    public function send_user_password($pwd='',$query){
        $newsid='5';
        $template_values=$this->user_model->get_newsletter_template_details($newsid);

        $adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);

        extract($adminnewstemplateArr);
        $subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
        $registeration =   file_get_contents('./newsletter/fogot_password.html');
        
        $message .=  str_replace('dynamicforgpasslink', $pwd,$registeration );
        if($template_values['sender_name']=='' && $template_values['sender_email']==''){
            $sender_email=$this->config->item('site_contact_mail');
            $sender_name=$this->config->item('email_title');
        }else{
            $sender_name=$template_values['sender_name'];
            $sender_email=$template_values['sender_email'];
        }
        
        $email_values = array('mail_type'=>'html',
                            'from_mail_id'=>$sender_email,
                            'mail_name'=>$sender_name,
                            'to_mail_id'=>$query->row()->email,
                            'subject_message'=>'Password Reset link',
                            'body_messages'=>$message,
                            'mail_id'=>'forgot',
                            'dynamicforgpasslink' => $pwd
                            );
                            $email_send_to_common = $this->product_model->common_email_send($email_values);
                            
    }



}

 ?>