<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * Generic Payment related functions
 * @author OSI
 *
 */

class Customerpayment extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('cookie','date','form','email','url'));
        $this->load->library(array('encrypt','form_validation','session','curl'));
       $this->load->model('product_model');
        $this->load->model('customerpayment_model');
        $this->load->model('order_model');
         $this->load->model('user_model');
        if($_SESSION['sMainCategories'] == ''){
                $sortArr1 = array('field'=>'cat_position','type'=>'asc');
                $sortArr = array($sortArr1);
                $_SESSION['sMainCategories'] = $this->product_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
        }
        $this->data['mainCategories'] = $_SESSION['sMainCategories'];

        if($_SESSION['sColorLists'] == ''){
                $_SESSION['sColorLists'] = $this->product_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
        }
        $this->data['mainColorLists'] = $_SESSION['sColorLists'];

        $this->data['loginCheck'] = $this->checkLogin('U');
    }
    /**
    *
    * Loading Generic Payment Page
    */

    public function index(){
      	if ($this->data['loginCheck'] != ''){
    		$userDetails = $this->user_model->get_users_details("WHERE id=".$this->data['loginCheck']);
    		$userRow = $userDetails->result_array();
    		$this->data['userEmail'] = $userRow[0]['email'];
            	$this->data['userID'] = $this->data['loginCheck'];
        }
			$this->data['heading'] = 'Pay Now';
			$this->data['meta_title'] = 'Cityfurnish Online Payment - Pay Your Rental';
			$this->data['meta_description'] = 'Now Pay Your Due Rental Online.';
			$this->data['voucher_data'] = $this->product_model->get_all_details(VOUCHER,array('voucher_title != ' => ''));
        $this->load->view('site/customerpayment/index.php',$this->data);
    }
    
   
    
     public function CustomerPayment(){
        $userDetails = $this->user_model->get_all_details(USERS, array('email' => $_POST['user_email']));
    	$userRow = $userDetails->result_array();
    	if( $_POST['order_id'] != ''  && ($_POST['payment_option'] == 'rcpayment' || $_POST['payment_option'] == 'enach')){
    		$order_data = $this->product_model->get_all_details(PAYMENT,array('dealCodeNumber' => $_POST['order_id'],'user_id' => $userRow[0]['id']));
    		if($order_data->num_rows() == 0){
		       	$order_errro = array('status' =>false ,'msg'=>'Invalid Order ID.','status_code' => 400);
            	header('Content-Type: application/json');
            	echo json_encode($order_errro);
            	exit;
    		}
    	}
  		if ($userDetails->num_rows() > 0) {
		   	$this->data['userEmail'] = $userRow[0]['email'];
		    	$payment_type = $_POST['payment_option'];
		    	$is_recurring = 'normal';
				switch ($payment_type) {
				    case "rcpayment":
				    		$is_recurring = 'si';
				    		$recurring_type = 'si_on_credit_card';
				        break;
				    case "one_time":
				        $recurring_type = 'not_opted';
				        break;
				    case "enach":
				         $recurring_type = 'not_opted';
				        //  $this->enach_payment($_POST);
				         exit;
				        break;
				    default:
				    // $is_recurring = 'si';
				        $recurring_type = 'not_opted';
				}
				$_SESSION['recurring_type'] = $recurring_type;
		   		$this->data['payu_type'] = $is_recurring;
		   	    $this->setOneTimePaymentDetails($_POST);

		      	$user_data = $this->product_model->get_all_details(USERS, array('email' => $this->input->post('user_email')));
		      	$this->data['PapiInfo'] = $this->Customerpayment_set_data($user_data,$_POST);
		      	if(!empty($user_data->result())){
		       		$this->load->view('site/checkout/checkout_payu.php', $this->data);
		       }else{
		       		$this->load->view('site/customerpayment/cpfailure.php',$this->data);
		       }  
	       
  		}else{
		   		$new_result = array('status' =>false ,'msg'=>'Please provide email id registered with us or login','status_code' => 400);
            	header('Content-Type: application/json');
            	echo json_encode($new_result);
		} 
    }
    
    
    
    public function Customerpayment_set_data($user_data,$post_data){
    	$condition = array();
        $data = $this->product_model->get_all_details(PAYMENT_GATEWAY,$condition);
        $gatewaySettings = $data->result();
    	$user_info = $user_data->row();
        foreach ($data->result() as $key => $val){
                    $gatewaySettings[$key]->settings = unserialize($val->settings);
        }
        if($gatewaySettings[3]->settings['mode'] == 'sandbox'){
            $PAYU_BASE_URL =  "https://test.payu.in/_payment";
        }else{
            $PAYU_BASE_URL =  "https://secure.payu.in/_payment";
        }
        // $loginUserId = $this->checkLogin('U');
        $lastFeatureInsertId = $this->session->userdata('randomNo');
        $MERCHANT_KEY = $gatewaySettings[3]->settings['merchant_key']; //Please change this value with live key for production
        $hash_string = '';
        $SALT = $gatewaySettings[3]->settings['salt']; //Please change this value with live salt for production
        $productinfo = 'AgileInfoways';
        $action = '';
        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

        // print_r($user_info->email);exit;
        $hash_key =  hash('sha512', $MERCHANT_KEY.'|'.$txnid.'|'.$post_data['user_amount'].'|'.$productinfo.'|'.$user_info->full_name.'|'.$user_info->email.'|||||||||||'.$SALT);
        $time = time() * 1000;
        $time = number_format($time, 0, '.', ''); 
        $this->data['ABC']['action_url'] = $PAYU_BASE_URL;
        $this->data['ABC']['key'] = $MERCHANT_KEY;
        $this->data['ABC']['currency'] = 'INR';
        $this->data['ABC']['txnid'] = $txnid;
        $this->data['ABC']['hash'] = $hash_key;
        $this->data['ABC']['email'] = $user_info->email;
        $this->data['ABC']['firstname'] = $user_info->full_name;
        $this->data['ABC']['phone'] = $user_info->phone_no;
        $this->data['ABC']['productinfo'] = $productinfo;
        $this->data['ABC']['user_credentials'] = $MERCHANT_KEY.':'.$post_data['user_email'].rand();
        // user_credentials
        // $this->data['ABC']['templateCode'] = $templateCode;
        // $this->data['ABC']['dpFlag'] = $dpFlag;
        $this->data['ABC']['orderAmount'] = $post_data['user_amount'];
        $this->data['ABC']['time'] = $time;
        $this->data['ABC']['returnUrl'] = base_url() . 'customerpayment/cpresponse';
        $this->data['ABC']['faile_url'] = base_url() . 'customerpayment/failure';
            // base_url().'payu/payu_success/success/'.$loginUserId .'/'. $lastFeatureInsertId;

            // base_url() . 'order/success/' . $loginUserId . '/' . $lastFeatureInsertId;

             return $this->data['ABC'];
    }
    
    
    
    
    
	public function email_check($str)
	{
	    	$this->load->model('user_model');
		$userDetails = $this->user_model->get_users_details("WHERE email='".$_POST['user_email']."'");
		$userRow = $userDetails->result_array();
		if(count($userRow)==0)
		{
	    		$this->form_validation->set_message('email_check', 'Account does not exists with the email - '.$_POST['user_email']);
	    		return FALSE;
	    	}
	    	else
	    	{
	    		return TRUE;
	    	}
	}
    public function check_user(){
        /*echo "<pre>post values:";
        print_r($this->input->post());
        echo "</pre>";*/
			   
        $this->form_validation->set_rules('user_first_name', 'First Name', 'trim|required');
	//$this->form_validation->set_rules('user_email', 'Email Address', 'trim|required|valid_email|callback_email_check');
	$this->form_validation->set_rules('user_email', 'Email Address', 'trim|required|valid_email');
        $this->form_validation->set_rules('user_amount', 'Amount', 'trim|required|numeric');
        if ($this->form_validation->run() === FALSE)
	{
            //$this->setErrorMessage('error','First Name, Email Address and Amount fields are required');
            if($this->data['loginCheck']!='')
            {
            	$this->load->model('user_model');
    		$userDetails = $this->user_model->get_users_details("WHERE id=".$this->data['loginCheck']);
    		$userRow = $userDetails->result_array();
    		$this->data['userEmail'] = $userRow[0]['email'];
            	$this->data['userID'] = $this->data['loginCheck'];
            }
            $this->load->view('site/customerpayment/index.php',$this->data);
        }
        else 
        {
        	$this->techProcessPayment();    		
        }		
    }
								
	public function setOneTimePaymentDetails($user_input)
	{
		session_start();
		if(!empty($_POST['user_first_name'])&&!empty($_POST['user_first_name']))
		{
			$_SESSION['userFullName'] = $_POST['user_first_name']. " ".$_POST['user_last_name'];
		}
		else if(!empty($_POST['user_first_name']))
		{
			$_SESSION['userFullName'] = $_POST['user_first_name'];
		}
		if(!empty($_POST['user_email']))
		{
			$_SESSION['userEmail'] = $_POST['user_email'];
		}
		if(!empty($_POST['user_invoice_number']))
		{
			$_SESSION['invoiceNumber'] = $_POST['user_invoice_number'];
		}
		else{
			$_SESSION['invoiceNumber'] = "";
		}
		if(!empty($_POST['order_id']))
		{
			$_SESSION['Order_id'] = $_POST['order_id'];
		}else{
		    $_SESSION['Order_id'] = "";
		}
		if(!empty($_POST['user_notes']))
		{
			$_SESSION['notes'] = $_POST['user_notes'];
		}
		if(!empty($_POST['user_amount']))
		{
			$_SESSION['pay_now_amount'] = $_POST['user_amount'];
		}	
		if(!empty($_POST['tenure_payment']))
		{
			$_SESSION['tenure'] = $_POST['tenure_payment'];
		}	
		return true;	
		
	} 
					
public function cpresponse()
	{

		session_start();
		$_SESSION['response'] = $_POST;
		$data = "";
		$flag = "true";
		if(isset($_POST['txnid'])) {
			$txnid = $_POST['txnid'];
			$data .= $txnid;
		}
		 if(isset($_POST['status'])) {
			$txnstatus = $_POST['status'];
			$data .= $txnstatus;
		 }
		 if(isset($_POST['amount'])) {
			$amount = $_POST['amount'];
			$data .= $amount;
		 }
	
		 if(isset($_POST['firstname'])) {
			$firstName = $_POST['firstname'];
			$data .= $firstName;
		 }
		 if(isset($_POST['lastname'])) {
			$lastName = $_POST['lastname'];
			$data .= $lastName;
		 }
		 if(isset($_POST['address1'])) {
			$pincode = $_POST['address1'];
			$data .= $pincode;
		 }
		 if(isset($_POST['hash'])) {
			$hash = $_POST['hash'];
		 }
		
		 // $respSignature = hash_hmac('sha1', $data, $secret_key);
		 if($hash == "") {
			$flag = "false";
		}
		if ($flag == "true" && $_POST['status']=='success') {	
			redirect('customerpayment/cpsuccess');

		} 
		else { 
			redirect('customerpayment/cpfailure');
		}
	
	}				 
				 
					 
public function cpsuccess()
	{
		session_start();
		if(!empty($_SESSION['response']))
		{
			if(isset($_SESSION['response']['payment_source']) &&  $_SESSION['response']['payment_source'] == 'sist'){
				$this->data['is_recurring'] = true;
			    $this->send_voucher_email($_SESSION['userEmail'],$_SESSION['Order_id']);
				$this->data['card_token'] = $_SESSION['response']['cardToken'];
			}else{
				$this->data['is_recurring'] = '0';
			}
		    
				
			$this->data['amountPaid'] = $_SESSION['response']['amount'];
		
			$this->data['citrusTxnID'] = $_SESSION['response']['txnid'];
		
			$this->data['txnRefNumber'] = $_SESSION['response']['mihpayid'];		 
		    	$this->data['userEmail'] = $_SESSION['userEmail'];
		    	$this->data['userFullName'] = $_SESSION['firstname'];
		    	$this->data['invoiceNumber'] = $_SESSION['invoiceNumber'];
		    	$this->data['notes'] = $_SESSION['notes'];
		    	//$this->data['response'] = $response['response'];
		    	/* start of - send email to the user */		   
		  
												 
		    	$message = $this->get_user_email_message($this->data);
		    	$email_values = array('mail_type'=>'html',
						'from_mail_id'=>$this->config->item('site_contact_mail'),
						'mail_name'=>$this->config->item('email_title'),
						'to_mail_id'=>$_SESSION['userEmail'],
						'subject_message'=>"Payment Confirmation",
						'body_messages'=>$message
						);
			$email_send_to_common = $this->product_model->common_email_send($email_values);
			/* end of - send email to the user */
			/* start of - send email to the finance team */
			$messageFinTeam = $this->get_finance_team_email_message($this->data);
		    	$finance_team_email_values = array('mail_type'=>'html',
						'from_mail_id'=>$this->config->item('site_contact_mail'),
						'mail_name'=>$this->config->item('email_title'),
						'to_mail_id'=>FINANCE_TEAM_EMAIL,
						'subject_message'=>"Received Payment from ".$_SESSION['userFullName']." (".$_SESSION['userEmail'].")",
						'body_messages'=>$messageFinTeam
						);
			$email_send_to_finance_team = $this->product_model->common_email_send($finance_team_email_values);
			/* end of - send email to the finance team */
			// insert data into table
			$insertArr = array("ccp_email"=>$_SESSION['userEmail'],
						"ccp_invoice_number"=>$_SESSION['invoiceNumber'],
						"ccp_notes"=>$_SESSION['notes'],
						"ccp_amount"=>$this->data['amountPaid'],
						"ccp_merchant_txn_ref"=>$this->data['txnRefNumber'],
						"ccp_citrus_txn_id"=>$this->data['citrusTxnID'],
						"ccp_response"=>json_encode($_SESSION['response']),
						"is_recurring"=>$this->data['is_recurring'],
						"card_token"=>$this->data['card_token'],
						"recurring_type" => $_SESSION['recurring_type'],
						"mandate_id" => $_SESSION['response']['mihpayid'],
						"order_id" => $_SESSION['Order_id']
					);
			$this->customerpayment_model->add_cp_customer_payment($insertArr);
			$last_insert_id = $this->db->insert_id();
			$this->createCaseInZohoCRM($_SESSION['userEmail'],$last_insert_id);
			unset($_SESSION['response']);
										  
		    	$this->load->view('site/customerpayment/cpsuccess.php',$this->data);
		}
		else
		{
			redirect('customerpayment');
		}
	 
	}
	
	
	
    
    public function cpfailure()
    {   
    	session_start();	 
	 
					 
    	if(!empty($_SESSION['response']))
    	{

		$this->data['amountPaid'] = $_SESSION['response']['amount'];
	
		$this->data['citrusTxnID'] = $_SESSION['response']['TxRefNo'];
	
		$this->data['txnRefNumber'] = $_SESSION['response']['transactionId'];
		$this->data['TxMsg'] = 	$_SESSION['response']['TxMsg'];			
		
	    	$this->data['userEmail'] = $_SESSION['userEmail'];
	    	$this->data['userFullName'] = $_SESSION['userFullName'];
	    	$this->data['invoiceNumber'] = $_SESSION['invoiceNumber'];
	    	$this->data['notes'] = $_SESSION['notes'];
	    	$this->data['userAmount'] = $_SESSION['userAmount'];
	    	
	    	/* start of - send email to the user */
	    	$message = $this->get_user_failed_email_message($this->data);
	    	$email_values = array('mail_type'=>'html',
					'from_mail_id'=>$this->config->item('site_contact_mail'),
					'mail_name'=>$this->config->item('email_title'),
					'to_mail_id'=>$_SESSION['userEmail'],
					'subject_message'=>"Payment Failed",
					'body_messages'=>$message
					);
		$email_send_to_common = $this->product_model->common_email_send($email_values);
		/* end of - send email to the user */
		/* start of - send email to the finance team */
		$messageFinTeam = $this->get_finance_team_failed_email_message($this->data);
	    	$finance_team_email_values = array('mail_type'=>'html',
					'from_mail_id'=>$this->config->item('site_contact_mail'),
					'mail_name'=>$this->config->item('email_title'),
					'to_mail_id'=>FINANCE_TEAM_EMAIL,
					'subject_message'=>"Failed Payment info - ".$_SESSION['userFullName']." (".$_SESSION['userEmail'].")",
					'body_messages'=>$messageFinTeam
					);
		$email_send_to_finance_team = $this->product_model->common_email_send($finance_team_email_values);
		/* end of - send email to the finance team */
	    	//$this->data['response'] = $response['response'];
	    	unset($_SESSION['response']);
						   
	    	$this->load->view('site/customerpayment/cpfailure.php',$this->data);
	 
    	}
    	else
    	{
    		redirect('customerpayment');
    	}
    }
    
    
    
    public function createCaseInZohoCRM($email,$last_insert_id)
    {
    	$userDetails = $this->user_model->get_all_details(USERS, array('email' => $email));
    	$userData = $userDetails->result_array(); 
    	$get_order_list = $this->user_model->get_all_details(SHIPPING_ADDRESS,array('user_id' => $userData->id ));
    	$shiping_data  = $get_order_list->result_array();
    	$si_data = $this->product_model->get_all_details(CP_CUSTOMER_PAYMENT,array('ccp_id' => $last_insert_id));		
		$checkContactsURL = 'https://www.zohoapis.com/crm/v2/Contacts/search?criteria=(Email:equals:'.$userData[0]['email'].')';
// 		$headers = [
// 			'Content-Type:application/json',
//     		'Authorization:'.CRM_AUTH_TOKEN
//     	];
	    $acceess_token = $this->get_zohaccess_token();
   		$headers = [
   		    'Content-Type:application/json',
            'Authorization:'.$acceess_token
        ];

		$curl = curl_init($checkContactsURL);
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => $headers
		));
		$contactsExists = curl_exec($curl);
		$contactsExists_array = json_decode($contactsExists);		
		$accountName = $userData[0]['full_name'];

		if(array_key_exists("data",$contactsExists_array)){
				$contactID = $contactsExists_array->data[0]->id;
				$accountName = $contactsExists_array->data[0]->Account_Name->name;
		}
		else
		{
			$checkAccountsURL = 'https://www.zohoapis.com/crm/v2/Accounts/search?criteria=(Email:equals:'.$order_data[0]['email'].')';
		    $curl = curl_init($checkAccountsURL);
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HTTPHEADER => $headers
			));
			$accountsExists = curl_exec($curl);
		    $accountsExists_array = json_decode($accountsExists);
			
			$accountID = "";
			$existingEmail="";
		
			if(array_key_exists("data",$accountsExists_array))
			{
				$existingEmail = $accountsExists_array->data[0]->Email;
		      	if($currentSuccessOrders[0]['email']!=$existingEmail)
		  		{
			           	$accountName = $currentSuccessOrders[0]['full_name'] . " - " . $currentSuccessOrders[0]['email'];
		           		$checkAccounts2URL = 'https://www.zohoapis.com/crm/v2/Accounts/search?criteria=(accountname:equals:'.$accountName.')';
		           	  	$curl = curl_init($checkAccountsURL);
						curl_setopt_array($curl, array(
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_HTTPHEADER => $headers
						));
						$accounts2Exists = curl_exec($curl);
			    		$accounts2Exists_array = json_decode($accounts2Exists);
			           
			          	if(array_key_exists("data",$accounts2Exists_array)){
							$accountID = $accounts2Exists_array->data[0]->id;
		           		}else{
		           			$accountID = "";
		           		}
		      	}              
			}
			
			if($accountID=="")
			{
			    // create an account name
		    	$createAccountURL = 'https://www.zohoapis.com/crm/v2/Accounts';	
		        $AccountJsonData =   '{
		            "data" : [
				       	{
			       			"Account_Name":"'.$accountName.'",
				       		"Account_Owner":"'.CASE_OWNER.'",
				       		"Email":"'.$userData[0]['email'].'",
				       		"Phone":"'.$shiping_data[0]['ship_phone'].'",
				       		"Billing_Street":"'.$shiping_data[0]['ship_address1'].'",
				       		"Billing_Street2":"'.$shiping_data[0]['ship_address2'].'",
				       		"Billing_City":"'.$shiping_data[0]['ship_city'].'",
				       		"Billing_State":'.$shiping_data[0]['ship_state'].',
				       		"Billing_Code":"'.$shiping_data[0]['ship_postal_code'].'",
				       		"Billing_Country":"'.$shiping_data[0]['ship_country'].'",
				       		"Shipping_Street":"'.$shiping_data[0]['ship_address1'].'",
				       		"Shipping_Street2":"'.$shiping_data[0]['ship_address2'].'",
				       		"Shipping_City":"'.$shiping_data[0]['ship_city'].'",
				       		"Shipping_State": "'.$shiping_data[0]['ship_state'].'",
			       			"Shipping_Code":"'.$shiping_data[0]['ship_postal_code'].'",
				       		"Shipping_Country":"'.$shiping_data[0]['ship_country'].'"
				        }
				    ]
				}';
			      
			    $curl = curl_init($createAccountURL);
					curl_setopt_array($curl, array(
						CURLOPT_POST => 1,
		            	CURLOPT_HTTPHEADER => $headers,
		            	CURLOPT_POSTFIELDS => $AccountJsonData,
		            	CURLOPT_RETURNTRANSFER => true
					));
				$createAccountResult = curl_exec($curl);
		    	$createAccountResultArray = json_decode($createAccountResult);
			      
			      if(array_key_exists("result",$createAccountResultArray))
			      {
			           $accountID = $createAccountResultArray->data[0]->details->id;
			      }
			      else
			      {
			          $accountID = "";
			      }
			}

			$fullNameArr = explode(" ",$userData[0]['full_name']);
			if(count($fullNameArr)==3)
			{
				$lastName = $fullNameArr[2];
				$firstName = $fullNameArr[0] . " " . $fullNameArr[1];
			}
			else if(count($fullNameArr)==2)
			{
				$lastName = $fullNameArr[1];
				$firstName = $fullNameArr[0];
			}
			else
			{
				$lastName = $purchaseList[0]['full_name'];
				$firstName = "";
			}

			$createContactURL = 'https://www.zohoapis.com/crm/v2/Contacts';	
	        $ContactJsonData =   '{
	            "data" : [
			       	{
			       		"First_Name":"Mr'.$firstName.'",
			       		"Last_Name":"'.$lastName.'",
			       		"Contact_Owner":"'.CASE_OWNER.'",
			       		"Account_Name":"'.$accountName.'",
			       		"Mobile":"'.$shiping_data[0]['ship_phone'].'",
			       		"Email":"'.$shiping_data[0]['email'].'",
			       		"Mailing_Street":"'.$shiping_data[0]['ship_address1'].'",
			       		"Mailing_Street2":"'.$shiping_data[0]['ship_address2'].'",
			       		"Mailing_City":"'.$shiping_data[0]['ship_city'].'",
			       		"Mailing_State":"'.$shiping_data[0]['ship_state'].'",
			       		"Mailing_Zip":"'.$shiping_data[0]['ship_postal_code'].'",
			       		"Mailing_Country":"'.$shiping_data[0]['ship_country'].'"
			        }
			    ]
			}';
			      
		    $curl = curl_init($createContactURL);
				curl_setopt_array($curl, array(
					CURLOPT_POST => 1,
	            	CURLOPT_HTTPHEADER => $headers,
	            	CURLOPT_POSTFIELDS => $ContactJsonData,
	            	CURLOPT_RETURNTRANSFER => true
				));
			$jsonContactRes = curl_exec($curl);
	    	$createContactResultArray = json_decode($jsonContactRes);
			
			if(array_key_exists("data",$createContactResultArray))
			{
			     $contactID = $createContactResultArray->data[0]->details->id;
			}
			else
			{
			    $contactID = "";
			}		
			
		}

		$Possible_Values_recurring = '';
		$si_new_data = $si_data->result_array();
		////check order is recurring or not
		if($si_new_data[0]['recurring_type'] == 'si_on_credit_card'){
			$Possible_Values_recurring  = 'SI On Credit Card';
		}else if($si_new_data[0]['recurring_type'] == 'enach'){
			$Possible_Values_recurring = 'Enach';
		}else if($si_new_data[0]['recurring_type'] == 'physical_nach'){
			$Possible_Values_recurring = 'Physical Nach';
		}else if($si_new_data[0]['recurring_type'] == 'pdc'){
			$Possible_Values_recurring = 'PDC';
		}else if($si_new_data[0]['recurring_type'] == 'not_opted'){
			$Possible_Values_recurring = 'Not Opted';
		}else{
			$Possible_Values_recurring = 'Not Opted';
		}


		if($si_new_data[0]['is_recurring']){
			$description = 'New Payment made by '.$si_new_data[0]['ccp_user_name'].' ' .$userData[0]['email'].' of '.$si_new_data[0]['ccp_amount'].' on '.$si_new_data[0]['ccp_created_on'].' and SI registration done on credit card';
			$subject = 'New SI Registration - ('.$userData[0]['email'].')';
		}else{
			$description = 'New Payment made by '.$si_new_data[0]['ccp_user_name'].' ' .$userData[0]['email'].' of '.$si_new_data[0]['ccp_amount'].' on '.$si_new_data[0]['ccp_created_on'].'';        	
        	$subject = 'New payment - ('.$userData[0]['email'].')';		
		}
		
		$products = "";
		$deposit = 0;
		$monthlyRent = 0;
        $couponcode = "NA";
		$i=1;

		// $deposit = $shiping_data[0]['shippingcost'];
		// $monthlyRent = ($shiping_data[0]['total']-$shiping_data[0]['shippingcost']);
		//$tenure = $purchaseList[0]['attr_name'];
		$fullName = $shiping_data[0]['full_name'];
		$address1 = $shiping_data[0]['address1'];
		$address2 = $shiping_data[0]['address2'];
		$city = $shiping_data[0]['city'];
		$country = $shiping_data[0]['country'];
		$state = $shiping_data[0]['state'];
		$zipCode = $shiping_data[0]['postal_code'];
		$phoneNumber = $shiping_data[0]['phone'];
        // $couponcode = $shiping_data[0]['couponCode'];


		//$description = "test desc";
		$city = $shiping_data[0]['city'];
		$relatedTo = $contactID;
		$caseOwner = CASE_OWNER;
		
		$createCaseURL = 'https://www.zohoapis.com/crm/v2/Cases';	
        $CreateCaseJsonData =   '{
        "data" : [
		       	{
	       		"Case_Origin":"Website",
	       		"Subject":"'.$subject.'",
	       		"Status":"Payment Done",
	       		"Owner":"649600348",
	       		"Recurring_Opted":"'.$Possible_Values_recurring.'",
	       		"Mandate_ID":"'.$si_new_data[0]['mandate_id'].'",
	       		"Order_ID":"'.$si_new_data[0]['order_id'].'",
	       		"Payment Ref No":"'.$si_new_data[0]['ccp_merchant_txn_ref'].'",
	       		"PG Transaction ID":"'.$si_new_data[0]['ccp_citrus_txn_id'].'",
	       		"Payment Amount":"'.$si_new_data[0]['ccp_amount'].'",
	       		"Invoice Number":"'.$si_new_data[0]['ccp_invoice_number'].'",
	       		"Payment Date":"'.$si_new_data[0]['ccp_invoice_number'].'",
	       		"Type":"Request",
	       		"Sub_Type":"Customer Payment",
	       		"Description":"'.$description.'",
	       		"Coupon_Code":"'.$couponcode.'",
	       		"Related_To":"'.$relatedTo.'",
	       		"City":"'.$shiping_data[0]['ship_city'].'",
	       		"Account_Name" :"'.$accountName.'",
	       		"LOB":"B2C",
	       		"Email":"'.$userData[0]['email'].'",
	       		"Phone":"'.$shiping_data[0]['ship_phone'].'"
		        }
		    ]
		}';
	    $curl = curl_init($createCaseURL);
			curl_setopt_array($curl, array(
				CURLOPT_POST => 1,
            	CURLOPT_HTTPHEADER => $headers,
            	CURLOPT_POSTFIELDS => $CreateCaseJsonData,
            	CURLOPT_RETURNTRANSFER => true
			));
		$jsonCaseRes = curl_exec($curl);
    	$createCaseResultArray = json_decode($jsonCaseRes);

		// echo "<pre>";
		if(!array_key_exists("data",$createCaseResultArray)){
			$failed_msg = 'For '.$fullName . ' Zoho Case is not Working<br>'.$products.'<br> Order ID:'.$si_new_data[0]['ccp_id'].'<br>Order Amount:'.$si_new_data[0]['ccp_amount'].'<br> email is '.$userData[0]['email'];
			$email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Zoho Error Mail',
                         'to_mail_id'=>'naresh.suthar@agileinfoways.com',
                         'subject_message'=>'Zoho Case Error',
                         'body_messages'=>$failed_msg
        				);
        	$email_send_to_common = $this->product_model->common_email_send($email_values);
		}
	
    }
    
    
      public function send_voucher_email($email,$order_id){
        $User_data = $this->product_model->get_all_details(USERS,array('email' => $email));
		$Secret_Data = $this->product_model->get_all_details('fc_voucher_settings',array('id !=' => ''));
	    $Reward  = base64_encode($Secret_Data->row()->reward);
	    $post_url = $Secret_Data->row()->post_url;
	    $return_url = $Secret_Data->row()->return_url;
	    $partner_code = $Secret_Data->row()->partner_code;
	    $user_name = str_replace(' ', '', $User_data->row()->user_name);
	    $timeStamp = base64_encode(time());

	    $url = file_get_contents('http://tinyurl.com/api-create.php?url='.'http://kbba.klippd.in/?clientID='.$Secret_Data->row()->client_id.'&key='.$Secret_Data->row()->key_id.'&userID='.$User_data->row()->id.'&rewards='.$Reward.'&username='.$user_name.'&EmailAddress='.$User_data->row()->email.'&postURL='.$post_url.'&returnURL='.$return_url.'&PartnerCode='.$partner_code.'&timeStamp='.$timeStamp.'');

	    
	    $voucher_url =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>Welcome Cityfurnish</title>
                  <style type="text/css">
                  @media screen and (max-width: 580px) {
                    .tab-container{max-width: 100%;}
                    .txt-pad-15 {padding: 0 15px 51px 15px !important;}
                    .foo-txt {padding: 0px 15px 18px 15px !important;}
                    .foo-add {padding: 20px !important;}
                    .tab-padd-zero{padding:0px !important;}
                    .tab-padd-right{padding-right:25px !important;}
                    .pad-20{padding:25px 20px 20px !important;}
                    .social-padd-left{padding:15px 20px 0px 0px; !important;}
                    .offerimg{width:100% !important;}
                    .mobilefont{font-size:16px !important;line-height:18px !important;}
                  }
                 </style>
                </head>
                <body style="margin:0px;">
                <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff;margin: 0 auto;font-family:Arial, Helvetica, sans-serif;font-size:14px;border-collapse:collapse;width:600px;
                border-width:1px; border-style:solid; border-color:#e7e7e7;table-layout: fixed;display: block;border-right-width: 0px;">

                    <tr>
                     <td style="width:100%">
                      <table style="width:100%;table-layout:fixed" cellpadding="0" cellspacing="0px">
                      <tr>
                      <td style="text-align: left;padding:15px 0px 15px 20px;padding-left:20px;">
                        <a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/logo-2.png" alt="logo" style="width:150px;" /></a>
                      </td>
                      <td style="text-align: right;padding:15px 20px 15px 0px;text-align:right;width:50%;border-right: 1px solid #e7e7e7" class="social-padd-left">
                        <a href="https://www.facebook.com/cityFurnishRental" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/facebook.png" alt="facebook" width="18px" /></a>
                        <a href="https://twitter.com/CityFurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/twitter.png" alt="twitter" width="18px"/></a>
                        <a href="https://plus.google.com/+cityfurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/google-plus.png" alt="google" height="18px"/></a>
                        <a href="https://in.pinterest.com/cityfurnish/" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/pintrest.png" alt="pintrest" width="18px" /></a>
                        <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/linkedin.png" alt="linkedin" width="18px" /></a>
                        </td>
                      </tr>
                      </table>
                     </td>
                    </tr>
                    <tr style="width:100%;">
                    <td style="width:100%;border-right: 1px solid #e7e7e7">
                      <img src="https://cityfurnish.com/images/voucher-banner.jpg" style="display: block;width: 100%;">
                    </td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="padding-top: 50px;font-size: 40px;color: #002e40;text-align: center;border-right: 1px solid #e7e7e7">
                      <h1 style="color: #002e40;font-size: 40px;line-height:36px;font-weight:bold;margin:0px;">Congratulations!</h1>
                           <span style="font-size: 18px;">You Got Gift Vouchers Worth</span>
                     </td>
                       
                    </tr>
                    <tr style="width:100%;">
                      <td style="text-align: center;padding-top: 5px;border-right: 1px solid #e7e7e7">
                        <span style="display: inline-block;vertical-align: middle;margin-right: 10px;">
                          <img src="https://cityfurnish.com/images/rupee-icn.png">
                        </span>
                        <span style="display: inline-block;vertical-align: middle;color: #f9aa2b;font-size: 45px;"><strong>'.$Secret_Data->row()->reward.'</strong></span></td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="text-align: center;padding-top: 15px;padding-bottom: 25px;font-size: 24px;border-right: 1px solid #e7e7e7">
                          <span style="display: inline-block;vertical-align: middle;font-size: 18px;">
                            From Cityfurnish for your #'.$order_id.'
                          </span>
                      </td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="text-align: center;font-size: 24px;color: #141414 !important;border-right: 1px solid #e7e7e7">
                          <a href="'.$url.'" target="_blank" style="background: #a5c8c2;width: 198px;text-decoration: none !important;font-size: 13px;display: inline-block;padding-top: 10px;padding-bottom: 10px;color: #141414 !important;">REDEEM</a>
                      </td>
                    </tr>
                    <tr style="width:100%;">
                    	<td style="padding: 20px;">
                    	  <h5>Terms and Conditions:</h5>
                    	  <span style="font-size: 10px;">1.You can opt for multiple voucher up-to-comulative total amount specified above.</span><br>
                    	  <span style="font-size: 10px;">2.Voucher link will be active for 60 days from send date .You can redeem desired vouchers by visting link multiple times using link within this period.</span><br>
                    	  <span style="font-size: 10px;">3.Once your have selected vouchers for redemption, you will receive selected voucher on email with details instruction for usage of each voucher. </span><br>
                    	  <span style="font-size: 10px;">4.Please contact us on  8010845000 or hello@cityfurnish.com in case your need any assistance.</span>
                    	</td>
                    </tr>
                    <tr  style="width:100%;">
                        <td style="padding:30px 50px 26px;text-align:center;line-height:24px;width:100%;border-right: 1px solid #e7e7e7">
                            <p style="margin:0px;line-height:22px;font-family:Arial, Helvetica, sans-serif;color:#b2b2b2">Sent by <a href="https://cityfurnish.com/" style="color:#38373d;text-decoration:none;">Cityfurnish</a> 525-527, Block D, JMD Megapolis, Sohna Road, Sector 48, Gurgaon, Haryana , 122018<br /><a href="mailto:hello@cityfurnish.com" style="color:#38373d; margin-top:8px;display:inline-block;text-decoration:none;">hello@cityfurnish.com</a></p>
                        </td>
                    </tr>
                </table>
                </body>
                </html>';

        $email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Cityfurnish',
                         'to_mail_id'=> $User_data->row()->email,
                         'subject_message'=>'Cityfurnish - Gift Vouchers',
                         'body_messages'=> $voucher_url
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);
	}

											  
												 
									 
	  
								
						  
											   
	   
												  
	   
										   
	  
											   
														  
										  
										
									   
   
										 
   
	  
   
										 
   
  
	  
		 
	  
											
		   
	 
    
    public function techProcessPayment()
    {    	
        session_start();
    	if($_POST && isset($_POST['submit']))
        {
		require_once './techprocess/TransactionRequestBean.php';
		require_once './techprocess/TransactionResponseBean.php';
		$val = $_POST;
                
    		$_SESSION['iv'] = TECHPROCESS_IV;
    		$_SESSION['key']   = TECHPROCESS_KEY;
    		//$this->session->iv = TECHPROCESS_IV;
    		//$this->session->key = TECHPROCESS_KEY;
    		
														   
														 
	  
    		if(isset($val['user_first_name']) && isset($val['user_last_name']))
    		{
    			$custName = $val['user_first_name'] . " " . $val['user_last_name'];
    		}
    		else if(isset($val['user_first_name']))
    		{
    			$custName = $val['user_first_name'];
    		}
    		
    		$merchantTxtID = rand(1,1000000);
    		$transDate = date('d-m-Y');
    		$tpslTransId = 'TXN00'.rand(1,10000);
    		$invoiceNumber = "";
    		if(isset($val['user_invoice_number'])&&!empty($val['user_invoice_number']))
    		{
    			$invoiceNumber = $val['user_invoice_number'];
    		}
    		
    		$name = str_replace(" ","~",$custName);
    		$userEmail = $val['user_email'];
    		$userAmount = $val['user_amount'];
    		$_SESSION['userEmail'] = $userEmail;
    		$_SESSION['userFullName'] = $custName;
    		$_SESSION['invoiceNumber'] = $invoiceNumber;
    		$_SESSION['notes'] = $val['user_notes'];
    		$_SESSION['userAmount'] = $userAmount;
    		
    		$itc = "NIC~".$name."~".$userEmail."~Payment~".$val['user_amount']."~Invoice~".$invoiceNumber;
    		$_SESSION['itc'] = $itc;
    		$requestDetail = TECHPROCESS_SCHEME_CODE."_".$userAmount."_0.0";
		//echo "request detail: ";
		//var_dump($requestDetail);exit;
    		$transactionRequestBean = new TransactionRequestBean();
    		//Setting all values here
		$transactionRequestBean->setMerchantCode(TECHPROCESS_MERCHANT_CODE);
		//$transactionRequestBean->setAccountNo($val['tpvAccntNo']);
		$transactionRequestBean->setITC($itc);
		//$transactionRequestBean->setMobileNumber($val['mobNo']);
		$transactionRequestBean->setCustomerName($custName);
		$transactionRequestBean->setRequestType(TECHPROCESS_REQUEST_TYPE);
		$transactionRequestBean->setMerchantTxnRefNumber($merchantTxtID);
		$transactionRequestBean->setAmount($userAmount);
		$transactionRequestBean->setCurrencyCode(TECHPROCESS_CURRENCY_TYPE);
		$transactionRequestBean->setReturnURL(TECHPROCESS_RETURN_URL);
		$transactionRequestBean->setS2SReturnURL(TECHPROCESS_S2S_RETURN_URL);
		$transactionRequestBean->setShoppingCartDetails($requestDetail);
		$transactionRequestBean->setTxnDate($transDate);
		//$transactionRequestBean->setBankCode($val['bankCode']);
		$transactionRequestBean->setTPSLTxnID($tpslTransId);
		//$transactionRequestBean->setCustId($val['custID']);
		//$transactionRequestBean->setCardId($val['cardID']);
		$transactionRequestBean->setKey(TECHPROCESS_KEY);
		$transactionRequestBean->setIv(TECHPROCESS_IV);
		$transactionRequestBean->setWebServiceLocator(TECHPROCESS_LOCATOR_URL);
		//$transactionRequestBean->setMMID($val['mmid']);
		//$transactionRequestBean->setOTP($val['otp']);
		//$transactionRequestBean->setCardName($val['cardName']);
		//$transactionRequestBean->setCardNo($val['cardNo']);
		//$transactionRequestBean->setCardCVV($val['cardCVV']);
		//$transactionRequestBean->setCardExpMM($val['cardExpMM']);
		//$transactionRequestBean->setCardExpYY($val['cardExpYY']);
		//$transactionRequestBean->setTimeOut($val['timeOut']);
		
		$responseDetails = $transactionRequestBean->getTransactionToken();
		$responseDetails = (array)$responseDetails;
		$response = $responseDetails[0];
		
		if(is_string($response) && preg_match('/^msg=/',$response))
		{
			$outputStr = str_replace('msg=', '', $response);
			$outputArr = explode('&', $outputStr);
			$str = $outputArr[0];
			
			$transactionResponseBean = new TransactionResponseBean();
			$transactionResponseBean->setResponsePayload($str);
			$transactionResponseBean->setKey(TECHPROCESS_KEY);
			$transactionResponseBean->setIv(TECHPROCESS_IV);
			
			$response = $transactionResponseBean->getResponsePayload();
			$_SESSION['response'] = $response;
			$responseArr = explode("|",$response);
			$i=0;
			foreach($responseArr as $value)
			{
				$valueArr = explode("=",$value);
				if($valueArr[0] == "txn_msg" && $valueArr[1] == "success")
				{
					$i++;
					$this->transactionDualVerification($response);
					//redirect('customerpayment/success');
				}
				else if($valueArr[0] == "txn_msg" && $valueArr[1] == "failure")
				{
					$i++;
					redirect('customerpayment/failure');
				}			
			}
			if($i==0) //failed
			{
				redirect('customerpayment/failure');
			}
    		}
    		else if(is_string($response) && preg_match('/^txn_status=/',$response))
    		{
    			$_SESSION['response'] = $response;
			$responseArr = explode("|",$response);
			$i=0;
			foreach($responseArr as $value)
			{
				$valueArr = explode("=",$value);
				if($valueArr[0] == "txn_msg" && $valueArr[1] == "success")
				{
					$i++;
					$this->transactionDualVerification($response);
					//redirect('customerpayment/success');
				}
				else if($valueArr[0] == "txn_msg" && $valueArr[1] == "failure")
				{
					$i++;
					redirect('customerpayment/failure');
				}			
			}
			if($i==0) //failed
			{
				redirect('customerpayment/failure');
			}
		}

    		//echo "<script>window.location = '".$response."'</script>";
    		redirect($response);
    		//ob_flush();
        }
        else if($_POST)
        {
	        /*echo "<pre>post from else if";
	    	print_r($_POST);
	    	echo "</pre>";*/
	    	//exit;
                /*echo "session key" . $this->session->key . "<br>";
                echo "post<pre>";
                print_r($_SESSION);
                exit;*/
		$response = $_POST;
		
		if(is_array($response))
		{
			$str = $response['msg'];
		}
		else if(is_string($response) && strstr($response, 'msg='))
		{
			$outputStr = str_replace('msg=', '', $response);
			$outputArr = explode('&', $outputStr);
			$str = $outputArr[0];
		}
		else 
		{
			$str = $response;
		}
		require_once './techprocess/TransactionResponseBean.php';
		$transactionResponseBean = new TransactionResponseBean();
		
		$transactionResponseBean->setResponsePayload($str);
		//$transactionResponseBean->setKey($this->session->key);
		//$transactionResponseBean->setIv($this->session->iv);
		$transactionResponseBean->setKey($_SESSION['key']);
		$transactionResponseBean->setIv($_SESSION['iv']);
		
		$response = $transactionResponseBean->getResponsePayload();
		/*echo "First Response<pre><br>";
		$_SESSION['first_response'] = $response;
		print_r($response);
		echo "</pre>";*/
		//exit;
		$_SESSION['response'] = $response;		
		//$this->data['response'] = $response;
		
		$responseArr = explode("|",$response);
		$i=0;
		foreach($responseArr as $value)
		{
			$valueArr = explode("=",$value);
			if($valueArr[0] == "txn_msg" && $valueArr[1] == "success")
			{
				$i++;
				$this->transactionDualVerification($response);				
				//redirect('customerpayment/success');
			}
			else if($valueArr[0] == "txn_msg" && $valueArr[1] == "failure")
			{
				$i++;	
				redirect('customerpayment/failure');
			}			
		}
		if($i==0) //failed
		{
			//$this->session->set_flashdata('response',$response);
			redirect('customerpayment/failure');
		}
	}
    }
    
    public function transactionDualVerification($res)
    {
    	require_once './techprocess/TransactionRequestBean.php';
    	$responseArr = explode("|",$res);
	
	foreach($responseArr as $value)
	{
		$valueArr = explode("=",$value);
		if($valueArr[0] == "txn_amt")
		{
			$amountPaid = $valueArr[1];
		}
		else if($valueArr[0] == "tpsl_txn_id")
		{
			$tpslTxnID = $valueArr[1];
		}
		else if($valueArr[0] == "clnt_txn_ref")
		{
			$txnRefNumber = $valueArr[1];
		}			
	}
    	$transactionRequestBean = new TransactionRequestBean();
	//Setting all values here
	$transactionRequestBean->setRequestType("S");
	$transactionRequestBean->setMerchantCode(TECHPROCESS_MERCHANT_CODE);
	$transactionRequestBean->setKey(TECHPROCESS_KEY);
	$transactionRequestBean->setIv(TECHPROCESS_IV);
	$transactionRequestBean->setMerchantTxnRefNumber($txnRefNumber);
	$transactionRequestBean->setTPSLTxnID($tpslTxnID);
	$transactionRequestBean->setAmount($amountPaid);
	$transactionRequestBean->setWebServiceLocator(TECHPROCESS_LOCATOR_URL);
	
	/*$transactionRequestBean->setITC($itc);	
	$transactionRequestBean->setCustomerName($custName);	
	$transactionRequestBean->setCurrencyCode(TECHPROCESS_CURRENCY_TYPE);
	$transactionRequestBean->setReturnURL(TECHPROCESS_RETURN_URL);
	$transactionRequestBean->setS2SReturnURL(TECHPROCESS_S2S_RETURN_URL);
	$transactionRequestBean->setShoppingCartDetails($requestDetail);
	$transactionRequestBean->setTxnDate($transDate);*/
	
	
	$responseDetails = $transactionRequestBean->getTransactionToken();
	$responseDetails = (array)$responseDetails;
	$response = $responseDetails[0];	
	
	$_SESSION['response'] = $response;
	$responseArr = explode("|",$response);
	$i=0;
	foreach($responseArr as $value)
	{
		$valueArr = explode("=",$value);
		if($valueArr[0] == "txn_msg" && ($valueArr[1] == "SUCCESS"||$valueArr[1] == "success"))
		{
			$i++;
			redirect('customerpayment/success');
		}
		else if($valueArr[0] == "txn_msg" && $valueArr[1] == "failure")
		{
			$i++;
			redirect('customerpayment/failure');
		}			
	}
	if($i==0) //failed
	{
		redirect('customerpayment/failure');
	}
    }
    
    public function success()
    {
    	session_start();
    	if(!empty($_SESSION['response']))
    	{
	    	//echo $_SESSION['first_response']."<br>";
	    	//echo $_SESSION['response'];
	    	$responseArr = explode("|",$_SESSION['response']);
		
		foreach($responseArr as $value)
	  
									
		{
			$valueArr = explode("=",$value);
			if($valueArr[0] == "txn_amt")
										
	  
														   
	  
								
			{
				$this->data['amountPaid'] = $valueArr[1];
			}
			else if($valueArr[0] == "tpsl_txn_id")
			{
				$this->data['tpslTxnID'] = $valueArr[1];
			}
			else if($valueArr[0] == "clnt_txn_ref")
			{
				$this->data['txnRefNumber'] = $valueArr[1];
			}			
		}
	
							   
	 
					 
										 
	  
	    	$this->data['userEmail'] = $_SESSION['userEmail'];
	    	$this->data['userFullName'] = $_SESSION['userFullName'];
	    	$this->data['invoiceNumber'] = $_SESSION['invoiceNumber'];
	    	$this->data['notes'] = $_SESSION['notes'];
	    	//$this->data['response'] = $response['response'];
	    	/* start of - send email to the user */
													
												   
													  
													
													   
										   
	  
											 
	    	$message = $this->get_user_email_message($this->data);
	    	$email_values = array('mail_type'=>'html',
					'from_mail_id'=>$this->config->item('site_contact_mail'),
					'mail_name'=>$this->config->item('email_title'),
					'to_mail_id'=>$_SESSION['userEmail'],
					'subject_message'=>"Payment Confirmation",
					'body_messages'=>$message
					);
		$email_send_to_common = $this->product_model->common_email_send($email_values);
		/* end of - send email to the user */
		/* start of - send email to the finance team */
		$messageFinTeam = $this->get_finance_team_email_message($this->data);
	    	$finance_team_email_values = array('mail_type'=>'html',
					'from_mail_id'=>$this->config->item('site_contact_mail'),
					'mail_name'=>$this->config->item('email_title'),
					'to_mail_id'=>FINANCE_TEAM_EMAIL,
					'subject_message'=>"Received Payment from ".$_SESSION['userFullName']." (".$_SESSION['userEmail'].")",
					'body_messages'=>$messageFinTeam
					);
		$email_send_to_finance_team = $this->product_model->common_email_send($finance_team_email_values);
		/* end of - send email to the finance team */
		// insert data into table
		$insertArr = array("cp_email"=>$_SESSION['userEmail'],
					"cp_invoice_number"=>$_SESSION['invoiceNumber'],
					"cp_notes"=>$_SESSION['notes'],
					"cp_amount"=>$this->data['amountPaid'],
					"cp_merchant_txn_ref"=>$this->data['txnRefNumber'],
					"cp_tpsl_txn_id"=>$this->data['tpslTxnID'],
					"cp_itc"=>$_SESSION['itc']
				);
		$this->customerpayment_model->add_customer_payment($insertArr);
		unset($_SESSION['response']);
									  
	    	$this->load->view('site/customerpayment/success.php',$this->data);
    	}
    	else
    	{
    		redirect('customerpayment');
    	}
	 
    }
    
    
    
    
     public function failure()
    {   
    	session_start();
	 

					 
    	if(!empty($_SESSION['response']))
    	{
	 
	    	//echo $_SESSION['response'];
								  
	 
	    	$responseArr = explode("|",$_SESSION['response']);
		
		foreach($responseArr as $value)
		{
			$valueArr = explode("=",$value);
			if($valueArr[0] == "txn_amt")
			{
				$this->data['amountPaid'] = $valueArr[1];
			}
			else if($valueArr[0] == "tpsl_txn_id")
			{
				$this->data['tpslTxnID'] = $valueArr[1];
			}
			else if($valueArr[0] == "clnt_txn_ref")
			{
				$this->data['txnRefNumber'] = $valueArr[1];
			}			
		}
	    	$this->data['userEmail'] = $_SESSION['userEmail'];
	    	$this->data['userFullName'] = $_SESSION['userFullName'];
	    	$this->data['invoiceNumber'] = $_SESSION['invoiceNumber'];
	    	$this->data['notes'] = $_SESSION['notes'];
	    	$this->data['userAmount'] = $_SESSION['userAmount'];
	    	//$this->data['response'] = $response['response'];
	    	/* start of - send email to the user */
	    	$message = $this->get_user_failed_email_message($this->data);
	    	$email_values = array('mail_type'=>'html',
					'from_mail_id'=>$this->config->item('site_contact_mail'),
					'mail_name'=>$this->config->item('email_title'),
					'to_mail_id'=>$_SESSION['userEmail'],
					'subject_message'=>"Payment Failed",
					'body_messages'=>$message
					);
		$email_send_to_common = $this->product_model->common_email_send($email_values);
		/* end of - send email to the user */
		/* start of - send email to the finance team */
		$messageFinTeam = $this->get_finance_team_failed_email_message($this->data);
	    	$finance_team_email_values = array('mail_type'=>'html',
					'from_mail_id'=>$this->config->item('site_contact_mail'),
					'mail_name'=>$this->config->item('email_title'),
					'to_mail_id'=>FINANCE_TEAM_EMAIL,
					'subject_message'=>"Failed Payment info - ".$_SESSION['userFullName']." (".$_SESSION['userEmail'].")",
					'body_messages'=>$messageFinTeam
					);
		$email_send_to_finance_team = $this->product_model->common_email_send($finance_team_email_values);
		/* end of - send email to the finance team */
	    	//$this->data['response'] = $response['response'];
	    	unset($_SESSION['response']);
																			   
				  
							   
	    	$this->load->view('site/customerpayment/failure.php',$this->data);
	 
    	}
    	else
    	{
    		redirect('customerpayment');
    	}
    }
    
    
 
	
    public function get_user_failed_email_message($data)
    {
	/*$message = '<div style="width: 1012px; background: #FFFFFF; margin: 0 auto;">
			Hi '.$data['userFullName'].',<br><br>This is to inform you that the transaction you have tried at '.base_url().' has FAILED. Please try again.<br><br>We apologize for the inconvenience.<br><br>Regards,<br>Finance Team<br>Cityfurnish<br></div>';
	return $message;*/
	$message = '<div style="width: 1012px; background: #FFFFFF; margin: 0 auto;">
			Hi '.$data['userFullName'].',<br><br>This is to inform you that the transaction you have tried at '.base_url().' has FAILED. Please try again.<br><br><i>Reason:</i>&nbsp;'.$data['TxMsg'].'<br><br>We apologize for the inconvenience.<br><br>Regards,<br>Finance Team<br>Cityfurnish<br></div>';
	return $message;
    }
    public function get_finance_team_failed_email_message($data)
    {
    	$message = '<div style="width: 1012px; background: #FFFFFF; margin: 0 auto;">
			Dear Team,<br><br>The following user tried to process the payment. But, it has failed.<br><br>
			<table>
				<tr>
					<td>Customer Name: </td>
					<td>'.$data['userFullName'].'</td>
				</tr>
				<tr>
					<td>Customer Email: </td>
					<td>'.$data['userEmail'].'</td>
				</tr>
				<tr>
					<td>Amount: </td>
					<td>'.$data['userAmount'].'</td>
				</tr>
				<tr>
					<td>Invoice Number: </td>
					<td>'.$data['invoiceNumber'].'</td>
				</tr>
				<tr>
					<td>Notes: </td>
					<td>'.$data['notes'].'</td>
				</tr>
				<tr>
					<td>Message from Citruspay: </td>
					<td>'.$data['TxMsg'].'</td>
				</tr>
			</table>
			<br><br>Regards,<br>Finance Team<br>Cityfurnish<br></div>';
    	return $message;
    }
    public function get_finance_team_email_message($data)
    {
	    $message = '<div style="width: 1012px; background: #FFFFFF; margin: 0 auto;">
				<div style="width: 99.8%; background: #f3f3f3;border: 1px solid #454b56; text-align: center; margin: 0 auto;">
					<div><a id="logo" target="_blank" href="'.base_url().'"><img title="'.$this->config->item('meta_title').'" alt="'.$this->config->item('meta_title').'" src="'.base_url().'images/logo/'.$this->data['logo'].'" /></a></div>
					</div>
					<div style="width: 970px; background: #FFFFFF; float: left; padding: 20px; border: 1px solid #454B56;">
					<div style="float: right; width: 35%; margin-bottom: 20px; margin-right: 7px;">
						<table style="border: 1px solid #cecece;" cellpadding="0" cellspacing="0" border="0" width="100%">
							<tbody>
								<tr bgcolor="#f3f3f3">
									<td style="border-right: 1px solid #cecece;" width="87"><span style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; width: 100%; font-weight: bold; color: #000000; line-height: 38px; float: left;">Transaction Ref#</span></td>
									<td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">#'.$data['txnRefNumber'].'</span></td>
								</tr>
								<tr bgcolor="#f3f3f3">
									<td style="border-right: 1px solid #cecece;" width="87"><span style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; width: 100%; font-weight: bold; color: #000000; line-height: 38px; float: left;">Transaction Date</span></td>
									<td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.date("F j, Y g:i a").'</span></td>
								</tr>
							</tbody>
						</table>
					</div>
		
					<div style="float: left; width: 100%; margin-right: 3%; margin-top: 10px; font-size: 14px; font-weight: normal; line-height: 28px; font-family: Arial, Helvetica, sans-serif; color: #000; overflow: hidden;">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tbody>
								<tr>
									<td colspan="3">
										<table style="border: 1px solid #cecece; width: 99.5%;" cellpadding="0" cellspacing="0" border="0" width="100%">
											<tbody>
											<tr bgcolor="#f3f3f3">
												<td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Customer Name</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['userFullName'].'</span></td></tr>
												<tr><td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Customer Email</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['userEmail'].'</span></td></tr>
											<tr bgcolor="#f3f3f3">
												<td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Invoice Number</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['invoiceNumber'].'</span></td></tr>
												
												<tr><td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Citruspay Transaction ID</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['citrusTxnID'].'</span></td></tr>
												<tr bgcolor="#f3f3f3"><td style="border-right: 1px solid #cecece; text-align: center;" width="40%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Amount Paid</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['amountPaid'].'&nbsp;'.CITRUS_CURRENCY_TYPE.'</span></td></tr>
												
												<tr><td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Notes</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['notes'].'</span></td></tr>
											
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
				</div>
			</div>';


		return $message;
    }
    
    public function get_user_email_message($data)
    {
    	$message = '<div style="width: 1012px; background: #FFFFFF; margin: 0 auto;">
			<div style="width: 99.8%; background: #f3f3f3;border: 1px solid #454b56; text-align: center; margin: 0 auto;">
				<div><a id="logo" target="_blank" href="'.base_url().'"><img title="'.$this->config->item('meta_title').'" alt="'.$this->config->item('meta_title').'" src="'.base_url().'images/logo/'.$this->data['logo'].'" /></a></div>
				</div>
				<div style="width: 970px; background: #FFFFFF; float: left; padding: 20px; border: 1px solid #454B56;">
				<div style="float: right; width: 35%; margin-bottom: 20px; margin-right: 7px;">
					<table style="border: 1px solid #cecece;" cellpadding="0" cellspacing="0" border="0" width="100%">
						<tbody>
							<tr bgcolor="#f3f3f3">
								<td style="border-right: 1px solid #cecece;" width="87"><span style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; width: 100%; font-weight: bold; color: #000000; line-height: 38px; float: left;">Transaction Ref#</span></td>
								<td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">#'.$data['txnRefNumber'].'</span></td>
							</tr>
							<tr bgcolor="#f3f3f3">
								<td style="border-right: 1px solid #cecece;" width="87"><span style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; width: 100%; font-weight: bold; color: #000000; line-height: 38px; float: left;">Transaction Date</span></td>
								<td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.date("F j, Y g:i a").'</span></td>
							</tr>
						</tbody>
					</table>
				</div>
	
				<div style="float: left; width: 100%; margin-right: 3%; margin-top: 10px; font-size: 14px; font-weight: normal; line-height: 28px; font-family: Arial, Helvetica, sans-serif; color: #000; overflow: hidden;">
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tbody>
							<tr>
								<td colspan="3">
									<table style="border: 1px solid #cecece; width: 99.5%;" cellpadding="0" cellspacing="0" border="0" width="100%">
										<tbody>
										<tr bgcolor="#f3f3f3">
											<td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Invoice Number(optional)</span></td>
											<td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Citruspay Transaction ID</span></td>
											<td style="border-right: 1px solid #cecece; text-align: center;" width="40%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Amount Paid</span></td>
										</tr>
										<tr>
											<td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$data['invoiceNumber'].'</span></td>
											<td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$data['citrusTxnID'].'</span></td>
											<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$data['amountPaid'].'&nbsp;'.TECHPROCESS_CURRENCY_TYPE.'</span></td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div style="width:50%; float:left;">
            				<div style="float:left; width:100%;font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; width:100%; color:#000000; line-height:38px; "><span>'.$data['userFullName'].'</span>, thank you for the payment.</div>
                
               				<ul style="width:100%; margin:10px 0px 0px 0px; padding:0; list-style:none; float:left; font-size:12px; font-weight:normal; line-height:19px; font-family:Arial, Helvetica, sans-serif; color:#000;">
			                    <li>If you have any concerns please contact us.</li>
			                    <li>Email: <span>'.stripslashes($this->data['siteContactMail']).' </span></li>
			                    <li>Phone: +91 8010845000</li>
			               </ul>
			        </div>            
            			<div style="width:27.4%; margin-right:5px; float:right;"></div>        
				<div style="clear:both"></div>
			</div>
		</div>';


		return $message;


    }
    
    public function checkTransactionStatus()
    {
    	require_once './techprocess/TransactionRequestBean.php';
    	//$res = "txn_status=0300|txn_msg=success|txn_err_msg=NA|clnt_txn_ref=585326|tpsl_bank_cd=470|tpsl_txn_id=385700210|txn_amt=4.50|clnt_rqst_meta={itc:NIC~Bhujangarao~Nalluri~vinitj@gmail.com~Payment~4.5~Invoice~TESTINV001}{custname:Bhujangarao Nalluri}|tpsl_txn_time=03-11-2017 13:51:45|tpsl_rfnd_id=NA|bal_amt=NA|rqst_token=792e48b9-3d5c-485b-b893-2eb7f1c83136|hash=863293df775552662c1881f4b6a10f3e351ae21d";
    	$res = "txn_status=0300|txn_msg=success|txn_err_msg=NA|clnt_txn_ref=859154|tpsl_bank_cd=50|tpsl_txn_id=399316852|txn_amt=2.00|clnt_rqst_meta={itc:NIC~Bhujangarao~Nalluri~bnalluri@osius.com~Payment~2~Invoice~OSI-INV004}{custname:Bhujangarao Nalluri}|tpsl_txn_time=06-11-2017 15:46:42|tpsl_rfnd_id=NA|bal_amt=NA|rqst_token=3d942f2d-a23f-45ba-b401-d89b83781503|hash=eb942ca6a12d76bfb2df76e78c9096b9fb687632";
    	$responseArr = explode("|",$res);
	
	foreach($responseArr as $value)
	{
		$valueArr = explode("=",$value);
		if($valueArr[0] == "txn_amt")
		{
			$amountPaid = $valueArr[1];
		}
		else if($valueArr[0] == "tpsl_txn_id")
		{
			$tpslTxnID = $valueArr[1];
		}
		else if($valueArr[0] == "clnt_txn_ref")
		{
			$txnRefNumber = $valueArr[1];
		}			
	}
    	$transactionRequestBean = new TransactionRequestBean();
	//Setting all values here
	$transactionRequestBean->setRequestType("O");
	$transactionRequestBean->setMerchantCode(TECHPROCESS_MERCHANT_CODE);
	$transactionRequestBean->setKey(TECHPROCESS_KEY);
	$transactionRequestBean->setIv(TECHPROCESS_IV);
	$transactionRequestBean->setMerchantTxnRefNumber($txnRefNumber);
	$transactionRequestBean->setTPSLTxnID($tpslTxnID);
	$transactionRequestBean->setAmount($amountPaid);
	$transactionRequestBean->setWebServiceLocator(TECHPROCESS_LOCATOR_URL);
	
	/*$transactionRequestBean->setITC($itc);	
	$transactionRequestBean->setCustomerName($custName);	
	$transactionRequestBean->setCurrencyCode(TECHPROCESS_CURRENCY_TYPE);
	$transactionRequestBean->setReturnURL(TECHPROCESS_RETURN_URL);
	$transactionRequestBean->setS2SReturnURL(TECHPROCESS_S2S_RETURN_URL);
	$transactionRequestBean->setShoppingCartDetails($requestDetail);
	$transactionRequestBean->setTxnDate($transDate);*/
	
	
	$responseDetails = $transactionRequestBean->getTransactionToken();
	$responseDetails = (array)$responseDetails;
	$response = $responseDetails[0];
	echo "response:<pre>";
	print_r($response);
	echo "</pre>";
	exit;
    }
    
    public function refundTransaction()
    {
    	/*require_once './techprocess/TransactionRequestBean.php';
    	$res = "txn_status=0300|txn_msg=success|txn_err_msg=NA|clnt_txn_ref=585326|tpsl_bank_cd=470|tpsl_txn_id=385700210|txn_amt=4.50|clnt_rqst_meta={itc:NIC~Bhujangarao~Nalluri~vinitj@gmail.com~Payment~4.5~Invoice~TESTINV001}{custname:Bhujangarao Nalluri}|tpsl_txn_time=03-11-2017 13:51:45|tpsl_rfnd_id=NA|bal_amt=NA|rqst_token=792e48b9-3d5c-485b-b893-2eb7f1c83136|hash=863293df775552662c1881f4b6a10f3e351ae21d";
    	$responseArr = explode("|",$res);
	
	foreach($responseArr as $value)
	{
		$valueArr = explode("=",$value);
		if($valueArr[0] == "txn_amt")
		{
			$amountPaid = $valueArr[1];
		}
		else if($valueArr[0] == "tpsl_txn_id")
		{
			$tpslTxnID = $valueArr[1];
		}
		else if($valueArr[0] == "clnt_txn_ref")
		{
			$txnRefNumber = $valueArr[1];
		}
		else if($valueArr[0] == "clnt_rqst_meta")
		{
			$itc = $valueArr[1];
		}			
	}
    	$transactionRequestBean = new TransactionRequestBean();
	//Setting all values here
	$transactionRequestBean->setRequestType("R");
	$transactionRequestBean->setMerchantCode(TECHPROCESS_MERCHANT_CODE);
	$transactionRequestBean->setKey(TECHPROCESS_KEY);
	$transactionRequestBean->setIv(TECHPROCESS_IV);
	$transactionRequestBean->setMerchantTxnRefNumber($txnRefNumber);
	$transactionRequestBean->setTPSLTxnID($tpslTxnID);
	$transactionRequestBean->setAmount($amountPaid);
	$transactionRequestBean->setWebServiceLocator(TECHPROCESS_LOCATOR_URL);
	
	
	
	
	$responseDetails = $transactionRequestBean->getTransactionToken();
	$responseDetails = (array)$responseDetails;
	$response = $responseDetails[0];
	echo "response:<pre>";
	print_r($response);
	echo "</pre>";
	exit;*/
    }
}