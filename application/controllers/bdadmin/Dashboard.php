<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('bdpartner_model');
		$this->load->model('giftcards_model');

    }
    
    
   	public function index(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			redirect('bdadmin/dashboard/admin_dashboard');
		}
	}
	
	public function admin_dashboard()
	{
	   // print_r("fddfd");exit;
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			/* get dashboard values start*/

			/* Get campus user count start*/
			$userTableName = PARTNER_USERS;
			$userFieldName = 'id';

			$recentUserWhereCondition = array('user.user_type'=>'Campus Ambassador');
			$getTotalCampusUsersCount = $this->bdpartner_model->getDashboardUserCount($recentUserWhereCondition);
			/* Get user count end*/

			/* Get Channel partner  user count start*/
			$recentUserWhereCondition = array('user.user_type'=>'Channel Partner');
			$getTotalParnterUsersCount = $this->bdpartner_model->getDashboardUserCount($recentUserWhereCondition);
			/* Get user count end*/

			/* Get BD executer user count start*/
			$recentUserWhereCondition = array('user.user_type'=>'BD Executive');
			$getTotalBDUsersCount = $this->bdpartner_model->getDashboardUserCount($recentUserWhereCondition);
			/* Get user count end*/

	
			/* Get total lead count start */
			$getTotalLeadCount = $this->bdpartner_model->getCountDetails(BD_LEADS,$userFieldName);
			/* last 24 hours record start */

			/* Get total Pending lead count start */
			$userWhereCondition1 = array('order_id'=> NULL );
			$getTotalPendingLeadCount = $this->bdpartner_model->getCountDetails(BD_LEADS,$userFieldName,$userWhereCondition1);
			/* last 24 hours record start */


			/* Get total Converted lead count start */
			$userWhereCondition1 = array('order_id != '=> NULL );
			$getTotalCovertedLeadCount = $this->bdpartner_model->getCountDetails(BD_LEADS,$userFieldName,$userWhereCondition1);
			/* last 24 hours record start */


			/* Get total payout commission  */
			$getTotalPaidCommissions = $this->bdpartner_model->getTotalCommissions('paid');
			$getTotalPaidCommissions = $getTotalPaidCommissions != '' ? $getTotalPaidCommissions : 0;

			/* Get total pending payout commission  */
			$getTotalPendingCommissions = $this->bdpartner_model->getTotalCommissions('pending');
			$getTotalPendingCommissions = $getTotalPendingCommissions != '' ? $getTotalPendingCommissions : 0;


			/* Get total users count  */
			$getTotalUsersCount = $this->bdpartner_model->getDashboardUserCount(array());
			
			/* Get today total users count  */
			$getTodayUsersCount = $this->bdpartner_model->getTodayUsersCount();
			/* Get this month total users count  */
			$getThisMonthCount = $this->bdpartner_model->getThisMonthCount();
			/* Get this year total users count  */
			$getLastYearCount = $this->bdpartner_model->getLastYearCount();


			/* get recent users list start*/
			$recentUserWhereCondition = array('status'=>'Active','group'=>'User');
			$userOrderBy = 'desc';
			$userLimit = "3";
			$getRecentUsersList = $this->bdpartner_model->getRecentDetails($userOrderBy,$userLimit);
			// echo "<pre>";print_r($getRecentUsersList);die;

			

			/* get recent orders details end*/

			$getOrderDetails = $this->bdpartner_model->getDashboardLeadsDetails();			

			/*Assign dashboard values to view start */
			$data = array(
				'totalCampusUserCounts'=>$getTotalCampusUsersCount,
				'totalParnterUserCounts'=>$getTotalParnterUsersCount,
				'totalBDUserCounts'=>$getTotalBDUsersCount,
				'getTotalLeadCount' => $getTotalLeadCount,
				'getTotalPendingLeadCount' => $getTotalPendingLeadCount,
				'getTotalCovertedLeadCount' => $getTotalCovertedLeadCount,
				'getTotalPaidCommissions' => $getTotalPaidCommissions,
				'getTotalPendingCommissions' => $getTotalPendingCommissions,
				'totalUserCounts' => $getTotalUsersCount,
				'todayUserCounts'=>$getTodayUsersCount,
				'getThisMonthCount'=>$getThisMonthCount,
				'getLastYearCount'=>$getLastYearCount,
				'getRecentUsersList'=>$getRecentUsersList,
				'getOrderDetails'=>$getOrderDetails 
			);


			$this->data = array_merge($data,$this->data);
			$heading = array('heading'=>'Dashboard');
			$this->data = array_merge($this->data,$heading);
// 			print_r($data);exit;
			
			$this->load->view('bdadmin/adminsettings/dashboard',$this->data);
			/*Assign dashboard values to view end */
		}

	}

}