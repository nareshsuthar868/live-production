<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to user management 
 * @author Teamtweaks
 *
 */

class Lead_request extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));	
		$this->load->model('bdpartner_model');	
	}

	public function display_lead_request_orders(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'Lead Requests';
			// $this->data['leadDetails'] = $this->bdpartner_model->get_leadlisting();
			// print_r($this->data['comission_list']);exit;
			$this->load->view('bdadmin/order/display_leadrequest',$this->data);
		}
	}

	public function get_leadlisting(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->bdpartner_model->get_leadlisting(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->bdpartner_model->get_leadlisting('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}

	public function convert_approve_lead(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$checkOrder = $this->bdpartner_model->check_order_id_valid($this->input->post('order_id'));
			if($checkOrder->num_rows() > 0){
				$CheckleadDoneWiththisorder = $this->bdpartner_model->get_all_details(BD_LEADS,array('order_id' => $this->input->post('order_id')));
				if($CheckleadDoneWiththisorder->num_rows() >  0){
					$jsonArray1 = array('status' => 400, 'message' => 'Duplicate order id');
					header('Content-Type: application/json');
					echo json_encode($jsonArray1);exit;
				}
			  	$lead_matching = $this->bdpartner_model->get_all_details(BD_LEADS,array('id' => $this->input->post('lead_id')));
		        if($lead_matching->num_rows() > 0){
		        	$user_details = $this->bdpartner_model->get_all_details(USERS,array('id' => $checkOrder->row()->user_id));
		        	if(strtotime($user_details->row()->created) > strtotime($lead_matching->row()->created)){
		     		    $this->bdpartner_model->update_details(BD_LEADS, array('order_id' => $this->input->post('order_id'),'lead_status' => 'converted'), array('id' => $this->input->post('lead_id')));
		                $insert_data['lead_id'] =  $this->input->post('lead_id');
		                $insert_data['commission_percentage'] =  $this->input->post('commission_percentage');
		                $insert_data['commission_rate'] = $this->input->post('commission_rate');
		                $insert_data['status'] = $this->input->post('status');
		                $this->bdpartner_model->simple_insert(BD_LEAD_COMMISSION, $insert_data);
		                $jsonArray1 = array('status' => 200, 'message' => 'Mark as converted');
						header('Content-Type: application/json');
						echo json_encode($jsonArray1);exit;
					}else{
						$jsonArray1 = array('status' => 400, 'message' => 'User create before lead submitted');
						header('Content-Type: application/json');
						echo json_encode($jsonArray1);exit;		
					}
		        }

			}else{
				$jsonArray1 = array('status' => 400, 'message' => 'No order found');
				header('Content-Type: application/json');
				echo json_encode($jsonArray1);exit;
			}
		}
	}

	public function mark_as_paid(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$CheckLead = $this->bdpartner_model->get_pending_lead_details($this->input->post('id'));
			if($CheckLead->num_rows() > 0){
			    $this->bdpartner_model->update_details(BD_LEAD_COMMISSION, array('status' => 'paid'), array('lead_id' => $this->input->post('id')));
			     echo  "1";
			}else{
				echo "0";
			}
		}
	}
	
	public function export_as_csv(){
		$lead_details = $this->bdpartner_model->get_export_lead_details($this->input->post());
		if($lead_details->num_rows() > 0){
			header('Content-Type: text/csv; charset=utf-8');  
	 		header('Content-Disposition: attachment; filename=leads_report.csv');  
			$output = fopen("php://output", "w");  
			fputcsv($output, array('Order ID', 'Lead Owner', 'Lead Name', 'Lead Email', 'Lead Phone','Lead Status','Lead Zoho Status','Created Date'));  
	  		foreach ($lead_details->result() as $key => $value) {
	  			$lead_row = array('#'.$value->order_id,$value->lead_owner.'('.$value->lead_owner_email.')',$value->first_name.$value->last_name,$value->email,$value->phone_no,$value->lead_status,$value->zoho_lead_status,$value->created);
	       		fputcsv($output, $lead_row);  
	  		}     		
	  		fclose($output); 
	  		// redirect(base_url()); 
		}else{
			$this->setErrorMessage('error','No leads found to export');
  			redirect('bdadmin/lead_request/display_lead_request_orders');
		}
	}
}