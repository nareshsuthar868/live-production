<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to user management 
 * @author Teamtweaks
 *
 */

class Setting extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('bdpartner_model');
    }

    public function show_comission_rates(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {

			$this->data['heading'] = 'Comission List';
			$this->data['comission_list'] = $this->bdpartner_model->get_all_details(COMISSIONS,array());
			$this->load->view('bdadmin/adminsettings/comission_rates',$this->data);
		}
    }

    public function addNewComission(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$insert_array['tenure']  = $this->input->post('tenure').' Months';
			$insert_array['comission']  = $this->input->post('comission');
			if($this->input->post('type') == 'add'){
				$this->bdpartner_model->simple_insert(COMISSIONS,$insert_array);
			}else{
				$this->bdpartner_model->update_details(COMISSIONS,$insert_array,array('id' => $this->input->post('id')));
			}
			echo json_encode(array('status' => 'success'));
			exit;
		}	
    }

    public function deleteComissions(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			
			$this->bdpartner_model->commonDelete(COMISSIONS,array('id' => $this->input->post('id')));
			echo json_encode(array('status' => 'success'));
			exit;
		}	
    }

    public function cms_pages(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'CMS Pages';
			$this->load->view('bdadmin/cms/cms_listing',$this->data);
		}	
    }

    public function get_cms_pages(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->bdpartner_model->get_cmslisting(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->bdpartner_model->get_cmslisting('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}		
    }

    public function edit_cms(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Edit Cms';
			$cms_id = $this->uri->segment(4,0);
			$this->data['cms_details'] = $this->bdpartner_model->get_all_details(BD_CMS,array('id' => $cms_id));
			if ($this->data['cms_details']->num_rows() == 1){
				$this->load->view('bdadmin/cms/edit_cms',$this->data);
			} else {
				redirect('bdadmin');
			}
		}
    }

    public function updateCms(){
     	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {

			$this->bdpartner_model->update_details(BD_CMS,array('page_name' => $this->input->post('page_name'),'page_content' => $this->input->post('page_content'),'updated_date' => date("Y/m/d")),array('id' => $this->input->post('cms_id')));
			$this->setErrorMessage('success','Cms updated successfully');
			redirect('bdadmin/setting/cms_pages');
	
		}	
    }


    public function offers_page(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {

			$this->data['heading'] = 'Offer List';
			// $this->data['offers'] = $this->bdpartner_model->get_all_details(DB_OFFERS,array());
			$this->load->view('bdadmin/cms/offer',$this->data);
		}
    }

     public function get_offer_listing(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->bdpartner_model->get_offerlisting(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->bdpartner_model->get_offerlisting('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}		
    }

    // public function add_offer()


    public function add_new_offers(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {

			$this->data['heading'] = 'Add New Offer';
			$this->load->view('bdadmin/cms/add_offers',$this->data);
		}
    }

    public function addInsertOffer(){
    	if($this->input->post('offer_id') == ''){
    		$this->bdpartner_model->commonInsertUpdate(BD_OFFERS,'insert',array(),array());
    		// redirect('bdadmin/setting/offers_page');
    	}else{
    		$excludeArr = array("offer_id");
			$this->bdpartner_model->commonInsertUpdate(BD_OFFERS,'update',$excludeArr,array(),array('id' => $this->input->post('offer_id')));
    	}
		redirect('bdadmin/setting/offers_page');
    }

    public function edit_offer(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Edit Offer';
			$offer_id = $this->uri->segment(4,0);
			$this->data['offer_details'] = $this->bdpartner_model->get_all_details(BD_OFFERS,array('id' => $offer_id));
			if ($this->data['offer_details']->num_rows() == 1){
				$this->load->view('bdadmin/cms/edit_offer',$this->data);
			} else {
				redirect('bdadmin');
			}
		}
    }

    public function delete_offer(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$offer_id = $this->uri->segment(4,0);
			$offer = $this->bdpartner_model->get_all_details(BD_OFFERS,array('id' => $offer_id));
			if ($offer->num_rows() > 0){
				$this->bdpartner_model->commonDelete(BD_OFFERS,array('id' => $offer_id));
				redirect('bdadmin/setting/offers_page');
			} else {
				redirect('bdadmin');
			}
		}	
    }
    
}
?>