<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to user management 
 * @author Teamtweaks
 *
 */

class Bdusers extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('bdpartner_model');
    }
    
    /**
     * 
     * This function loads the users list page
     */
   	public function index(){	
   		// print_r("expression");exit;
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			redirect('bdadmin/users/display_bd_users');
		}
	}
	


	public function display_bd_users(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'BD List';
			$this->load->view('bdadmin/bdusers/display_userlist',$this->data);
		}
	}

	//Show listing of bd/RM users
	public function get_bd_users(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->bdpartner_model->getBDexecuters(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->bdpartner_model->getBDexecuters('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}


		/**
	 * 
	 * This function insert and edit a user
	 */
	public function insertEditUser(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$user_id = $this->input->post('user_id');
			$user_type = $this->input->post('user_type');
			$employee_id = $this->input->post('employee_id');
			$user_name = $this->input->post('full_name');
			$phone_no = $this->input->post('phone_no');
			
			$check_mobile_exists = $this->bdpartner_model->check_data_exists(array('user.phone_no' => $phone_no,'user.id !=' => $user_id));
			// print_r($check_mobile_exists->row());exit;
			if($check_mobile_exists->num_rows() > 0){
					$this->setErrorMessage('error','Mobile Number Already Exists');
					redirect('bdadmin/bdusers/edit_user_form/'.$user_id);
			}

			$this->bdpartner_model->update_details(USERS,array('full_name' => $user_name,'phone_no' => $phone_no,'user_type'=>$user_type),array('id' => $user_id));
			$this->bdpartner_model->update_details(PARTNER_USERS,array('employee_id' => $employee_id),array('user_id' => $user_id));
			$this->setErrorMessage('success','User updated successfully');
			redirect('bdadmin/bdusers/display_bd_users');
		}
	}
	
	/**
	 * 
	 * This function loads the edit user form
	 */
	public function edit_user_form(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'Edit User';
			$user_id = $this->uri->segment(4,0);
			// $condition = array('id' => $user_id);
			$this->data['user_details'] = $this->bdpartner_model->get_user_details($user_id);
			if ($this->data['user_details']->num_rows() == 1){
				$this->load->view('bdadmin/bdusers/edit_user',$this->data);
			}else {
				redirect('admin');
			}
		}
	}

	/**
	 * 
	 * This function loads the user view page
	 */
	public function view_user(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'View User';
			$user_id = $this->uri->segment(4,0);
			// $condition = array('id' => $user_id);
			$this->data['user_details'] = $this->bdpartner_model->get_user_details($user_id);
			// print_r($this->data['user_details']->row());exit;
			if ($this->data['user_details']->num_rows() == 1){
				$this->load->view('bdadmin/bdusers/view_user',$this->data);
			}else {
				redirect('bdadmin');
			}
		}
	}
	
	/**
	 * 
	 * This function delete the user record from db
	 */
	public function delete_user(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$user_id = $this->uri->segment(4,0);
			$condition = array('id' => $user_id);
			$this->bdpartner_model->commonDelete(USERS,$condition);
			$this->setErrorMessage('success','User deleted successfully');
			redirect('admin/bdusers/display_user_list');
		}
	}


	public function approve_bdusers(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$user_id = $this->input->post('id');
			$condition = array('id' => $user_id);
			$this->bdpartner_model->update_details(PARTNER_USERS,array('user_status' => 'Approved'),$condition);
			// $this->SendAprrovalEmail($user_id);
			// $this->setErrorMessage('success','User updated successfully');
			// redirect('bdadmin/users/display_user_list');
		}
	}

	public function add_bd_users()
	{
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'Add BD Executes';
			$this->load->view('bdadmin/bdusers/add_bdexecuter',$this->data);
		}
	}

	public function insertBdExecuter()
	{
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
		  	$data = $this->bdpartner_model->check_details_exists($_POST);
	        if($data->num_rows() > 0){
	            $this->setErrorMessage('error', 'Email or Phone number already exists');
				redirect('bdadmin/bdusers/add_bd_users');   
	        }else{
				$user_insert['full_name'] = $this->input->post('first_name')." ".$this->input->post('last_name');
				$user_insert['user_name'] = $this->input->post('first_name')."_".$this->input->post('last_name');
				$user_insert['email'] = $this->input->post('email');
				$user_insert['phone_no'] = $this->input->post('phone_no');
				$user_insert['password'] = 'cityfurnish'.mt_rand();
				$user_insert['user_type'] = 'BD Executive';
				$user_insert['verify_code'] = $this->get_rand_str(10);
				$response = $this->bdpartner_model->simple_insert(USERS,$user_insert);
				$user_id = $this->db->insert_id();

				$config['overwrite'] = FALSE;
		    	$config['allowed_types'] = 'jpg|jpeg|gif|png';
			    $config['max_size'] = 2000;
			    $config['upload_path'] = './images/partner_app/profile_pic';
			    $this->load->library('upload', $config);
				if ( $this->upload->do_upload('thumbnail')){
					$imgDetails = $this->upload->data();
			    	$insert_array['profile_pic'] = $imgDetails['file_name'];
				}
				$str_result = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
				$new_random = substr(str_shuffle($str_result),0, 3).rand(00000,99999);
				$insert_array['bd_invite_code'] = $new_random;
				
				$insert_array['user_status'] = 'Approved';
				$insert_array['user_id'] = $user_id;
				$response = $this->bdpartner_model->simple_insert(PARTNER_USERS,$insert_array);
				$this->send_registration_mail($user_id,$insert_array['bd_invite_code']);
				$this->setErrorMessage('success','Bd Executer Added successfully');
				redirect('bdadmin/bdusers/display_bd_users');
			}
		}
	}

	public function send_registration_mail($user_id,$invite_code){
        $userDetails = $this->bdpartner_model->get_all_details(USERS,array('id' => $user_id)); 
        $uid = $userDetails->row()->id;
        $email = $userDetails->row()->email;

        $newsid = '25';
        $template_values=$this->bdpartner_model->get_newsletter_template($newsid);
        $subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
        $searchArray = array("%user_name", "%dynamic_data");
        $password_link = '<a href="'.base_url().'bdadmin/adminlogin/create_password/'.$userDetails->row()->verify_code.'">Create Password</a>';
        $dynamic_data = '<label>Invite Code : </label> '.$invite_code.' <br><label>Generate Password : </label>'.$password_link;
        $replaceArray = array($userDetails->row()->full_name, $dynamic_data);
        $new_message .=  str_replace($searchArray, $replaceArray ,$template_values['news_descrip'] );
        $message .= '<!DOCTYPE HTML>
            <html>
            <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            
            <meta name="viewport" content="width=device-width"/><body>';
        $message = $new_message;

        $message .= '</body>
            </html>';
        if($template_values['sender_name']=='' && $template_values['sender_email']==''){
            $sender_email=$this->data['siteContactMail'];
            $sender_name=$this->data['siteTitle'];
        }else{
            $sender_name=$template_values['sender_name'];
            $sender_email=$template_values['sender_email'];
        }

        $email_values = array('mail_type'=>'html',
                            'from_mail_id'=>$sender_email,
                            'mail_name'=>$sender_name,
                            'to_mail_id'=>$email,
                            'subject_message'=> $template_values['news_subject'],
                            'body_messages'=>$message,
                            'mail_id'=>'register mail'
                            );
        $email_send_to_common = $this->bdpartner_model->common_email_send($email_values); 
        // $password_link
        $mobile_number = '91'.$userDetails->row()->phone_no;
        $fields = [
            'apikey' => SMS_KEY,
            'msg' => "Welcome ".$userDetails->row()->full_name." to Cityfurnish Partner app. Thanks for signing up. Here is your invite code ".$invite_code,
            'sid' => 'CITYFN',
            'msisdn' => $mobile_number,
            'fl' => 0
        ];
        // $this->load->helper('sms');
        $response = $this->send_sms($fields);
    }

     public function send_sms($fields){
        $curl = curl_init();
          curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://apps.smslane.com/vendorsms/pushsms.aspx',
          // CURLOPT_URL =>'http://control.msg91.com/api/sendotp.php',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $fields,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        // print_r($err);exit;
        curl_close($curl);
        if ($err) {
          return 0;
        } else {
          return 1;
        }
  }

}

/* End of file users.php */
/* Location: ./application/controllers/admin/users.php */