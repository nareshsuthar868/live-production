<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to user management 
 * @author Teamtweaks
 *
 */

class Users extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('bdpartner_model');
    }
    
    /**
     * 
     * This function loads the users list page
     */
   	public function index(){	
   		// print_r("expression");exit;
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			redirect('bdadmin/users/display_user_list');
		}
	}
	
	/**
	 * 
	 * This function loads the users list page
	 */
	public function display_user_list(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'Users List';
			// print_r("expression");exit;
			// $condition = array('group'=>'User');
			// $this->data['usersList'] = $this->user_model->get_all_details(USERS,$condition);
			$this->load->view('bdadmin/users/display_userlist',$this->data);
		}
	}

	public function Get_user_data(){
		// print_r("expression");exit;
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
			// print_r($orderType);exit;
			$data = $this->bdpartner_model->getUsers(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
			$TotalCount = $this->bdpartner_model->getUsers('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}
	
	/**
	 * 
	 * This function insert and edit a user
	 */
	public function insertEditUser(){
		// print_r($this->input->post());exit;
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$user_id = $this->input->post('user_id');
			$user_type = $this->input->post('user_type');
			$employee_id = $this->input->post('employee_id');
			$user_name = $this->input->post('full_name');
			$phone_no = $this->input->post('phone_no');
			$bd_user = $this->input->post('bd_user');
			  // print_r($this->input->post());exit;

		
			$check_mobile_exists = $this->bdpartner_model->check_data_exists(array('user.phone_no' => $phone_no,'user.id !=' => $user_id,'user.user_type !=' => ''));
			// print_r($check_mobile_exists->row());exit;
			if($check_mobile_exists->num_rows() > 0){
					$this->setErrorMessage('error','Mobile Number Already Exists');
					redirect('bdadmin/users/edit_user_form/'.$user_id);
					
			}

			$this->bdpartner_model->update_details(USERS,array('full_name' => $user_name,'phone_no' => $phone_no,'user_type'=>$user_type),array('id' => $user_id));
			$this->bdpartner_model->update_details(PARTNER_USERS,array('employee_id' => $employee_id,'bdexecuter_id' => $bd_user),array('user_id' => $user_id));
			$this->setErrorMessage('success','User updated successfully');
			redirect('bdadmin/users/display_user_list');
		}
	}
	
	/**
	 * 
	 * This function loads the edit user form
	 */
	public function edit_user_form(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Edit User';
			$user_id = $this->uri->segment(4,0);
			$this->data['user_details'] = $this->bdpartner_model->get_user_details($user_id);
			$this->data['user_bd_executive'] = $this->bdpartner_model->get_user_bd_executive();

			if ($this->data['user_details']->num_rows() == 1){
				$this->load->view('bdadmin/users/edit_user',$this->data);
			} else {
				redirect('bdadmin');
			}
		}
	}

	/**
	 * 
	 * This function loads the user view page
	 */
	public function view_user(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'View User';
			$user_id = $this->uri->segment(4,0);
			// $condition = array('id' => $user_id);
			$this->data['user_details'] = $this->bdpartner_model->get_user_details($user_id);
			// echo "<pre>";
			// print_r($this->data['user_details']->row());exit;
			if ($this->data['user_details']->num_rows() == 1){
				$this->load->view('bdadmin/users/view_user',$this->data);
			}else {
				redirect('bdadmin');
			}
		}
	}
	
	/**
	 * 
	 * This function delete the user record from db
	 */
	public function delete_user(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$user_id = $this->uri->segment(4,0);
			$condition = array('id' => $user_id);
			$this->bdpartner_model->commonDelete(USERS,$condition);
			$this->setErrorMessage('success','User deleted successfully');
			redirect('admin/users/display_user_list');
		}
	}


	public function approve_bdusers(){
		
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$user_id = $this->input->post('id');
			$email = $this->input->post('email');
			$condition = array('id' => $user_id);
			$date = date('Y-m-d H:i:s');
			$this->SendAprrovalEmail($user_id,$email);
			$this->SendAprrovalSms($user_id);
			$this->bdpartner_model->update_details(PARTNER_USERS,array('user_status' => 'Approved', 'user_details_verified' => '1', 'document_verified' => '1', 'bank_verified' => '1','created_date'=>$date),$condition);
			// $this->setErrorMessage('success','User updated successfully');
			// redirect('bdadmin/users/display_user_list');
		}
	}

	//Working on 
	public function approve_docs(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$id = $this->input->post('id');
			$is_approved = $this->input->post('is_approved');
			$type = $this->input->post('type');
			$condition = array('partner.id' => $id);
			$user_details_verified = $this->bdpartner_model->check_data_exists($condition);
			if($user_details_verified->row()->user_type != 'BD Executive' && ($user_details_verified->row()->bdexecuter_id == '' || $user_details_verified->row()->bdexecuter_id == null)){
				$array = array('status' => 400, 'message' => 'Please assign BD executer first');
			    header('Content-Type: application/json');
				echo json_encode($array);exit;
			}
			$this->bdpartner_model->update_details(PARTNER_USERS,array($type => $is_approved),array('id' => $id));
		    $array = array('status' => 200, 'message' => 'Update successfully');
		    header('Content-Type: application/json');
			echo json_encode($array);exit;

		}
	}

	public function pending_bdusers(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$user_id = $this->input->post('id');
			$condition = array('id' => $user_id);
			$this->bdpartner_model->update_details(PARTNER_USERS,array('user_status' => 'Pending', 'user_details_verified' => '0', 'document_verified' => '0', 'bank_verified' => '0'),$condition);
		}
	}

	public function SendAprrovalEmail($id,$email)
	{
		$message .= '<!DOCTYPE HTML>
			<html>
			<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			
			<meta name="viewport" content="width=device-width"/><body>';
		$message .= "<b>Approved</b>";
		$message .= '</body>
				</html>';
		$config = Array( 
			'protocol' => 'smtp', 
			'smtp_host' => 'smtp.1and1.com', 
			'smtp_port' => 587, 
			'smtp_user' => 'naresh.suthar@agileinfoways.com', 
			'smtp_pass' => 'chiku11615652',
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
  			'wordwrap' => TRUE
		 ); 
			
			$this->load->library('email', $config); 
			$this->email->set_newline("\r\n");
			$this->email->from('email@gmail.com', 'Aprroved Mail');
			$this->email->to("pankaj.vanjara@agileinfoways.com");
			$this->email->subject(' Approved '); 
			$this->email->message($message);
			if (!$this->email->send()) {
			  show_error($this->email->print_debugger()); }
			else {
			  echo 'Your e-mail has been sent!';
			}
	}
	public function SendAprrovalSms($id)
	{
		$fields = [
            'apikey' => SMS_KEY,
            'msg' => "Approved",
            'sid' => 'CITYFN',
            'msisdn' => '917016634699',
            'fl' => 0,
            'gwid' => 2
        ];
	  $this->load->helper('sms');
	  $response = send_sms($fields);
	}
}

/* End of file users.php */
/* Location: ./application/controllers/admin/users.php */	