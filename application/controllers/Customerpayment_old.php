<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * Generic Payment related functions
 * @author OSI
 *
 */

class Customerpayment extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('cookie','date','form','email','url'));
        $this->load->library(array('encrypt','form_validation','session'));
        $this->load->model('product_model');
        $this->load->model('customerpayment_model');
        if($_SESSION['sMainCategories'] == ''){
                $sortArr1 = array('field'=>'cat_position','type'=>'asc');
                $sortArr = array($sortArr1);
                $_SESSION['sMainCategories'] = $this->product_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
        }
        $this->data['mainCategories'] = $_SESSION['sMainCategories'];

        if($_SESSION['sColorLists'] == ''){
                $_SESSION['sColorLists'] = $this->product_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
        }
        $this->data['mainColorLists'] = $_SESSION['sColorLists'];

        $this->data['loginCheck'] = $this->checkLogin('U');
    }
    /**
    *
    * Loading Generic Payment Page
    */

    public function index(){
    	if ($this->data['loginCheck'] != ''){
    		$this->load->model('user_model');
    		$userDetails = $this->user_model->get_users_details("WHERE id=".$this->data['loginCheck']);
    		$userRow = $userDetails->result_array();
    		$this->data['userEmail'] = $userRow[0]['email'];
            	$this->data['userID'] = $this->data['loginCheck'];
        }
        $this->load->view('site/customerpayment/index.php',$this->data);
    }
    	public function email_check($str)
	{
	    	$this->load->model('user_model');
		$userDetails = $this->user_model->get_users_details("WHERE email='".$_POST['user_email']."'");
		$userRow = $userDetails->result_array();
		if(count($userRow)==0)
		{
	    		$this->form_validation->set_message('email_check', 'Account does not exists with the email - '.$_POST['user_email']);
	    		return FALSE;
	    	}
	    	else
	    	{
	    		return TRUE;
	    	}
	}
    public function check_user(){
        /*echo "<pre>post values:";
        print_r($this->input->post());
        echo "</pre>";*/
			   
        $this->form_validation->set_rules('user_first_name', 'First Name', 'trim|required');
	//$this->form_validation->set_rules('user_email', 'Email Address', 'trim|required|valid_email|callback_email_check');
	$this->form_validation->set_rules('user_email', 'Email Address', 'trim|required|valid_email');
        $this->form_validation->set_rules('user_amount', 'Amount', 'trim|required|numeric');
        if ($this->form_validation->run() === FALSE)
	{
            //$this->setErrorMessage('error','First Name, Email Address and Amount fields are required');
            if($this->data['loginCheck']!='')
            {
            	$this->load->model('user_model');
    		$userDetails = $this->user_model->get_users_details("WHERE id=".$this->data['loginCheck']);
    		$userRow = $userDetails->result_array();
    		$this->data['userEmail'] = $userRow[0]['email'];
            	$this->data['userID'] = $this->data['loginCheck'];
            }
            $this->load->view('site/customerpayment/index.php',$this->data);
        }
        else 
        {
        	$this->techProcessPayment();    		
        }		
    }
								
	 
					
					 
					 
					 
											  
												 
									 
	  
								
						  
											   
	   
												  
	   
										   
	  
											   
														  
										  
										
									   
   
										 
   
	  
   
										 
   
  
	  
		 
	  
											
		   
	 
    
    public function techProcessPayment()
    {    	
        session_start();
    	if($_POST && isset($_POST['submit']))
        {
		require_once './techprocess/TransactionRequestBean.php';
		require_once './techprocess/TransactionResponseBean.php';
		$val = $_POST;
                
    		$_SESSION['iv'] = TECHPROCESS_IV;
    		$_SESSION['key']   = TECHPROCESS_KEY;
    		//$this->session->iv = TECHPROCESS_IV;
    		//$this->session->key = TECHPROCESS_KEY;
    		
														   
														 
	  
    		if(isset($val['user_first_name']) && isset($val['user_last_name']))
    		{
    			$custName = $val['user_first_name'] . " " . $val['user_last_name'];
    		}
    		else if(isset($val['user_first_name']))
    		{
    			$custName = $val['user_first_name'];
    		}
    		
    		$merchantTxtID = rand(1,1000000);
    		$transDate = date('d-m-Y');
    		$tpslTransId = 'TXN00'.rand(1,10000);
    		$invoiceNumber = "";
    		if(isset($val['user_invoice_number'])&&!empty($val['user_invoice_number']))
    		{
    			$invoiceNumber = $val['user_invoice_number'];
    		}
    		
    		$name = str_replace(" ","~",$custName);
    		$userEmail = $val['user_email'];
    		$userAmount = $val['user_amount'];
    		$_SESSION['userEmail'] = $userEmail;
    		$_SESSION['userFullName'] = $custName;
    		$_SESSION['invoiceNumber'] = $invoiceNumber;
    		$_SESSION['notes'] = $val['user_notes'];
    		$_SESSION['userAmount'] = $userAmount;
    		
    		$itc = "NIC~".$name."~".$userEmail."~Payment~".$val['user_amount']."~Invoice~".$invoiceNumber;
    		$_SESSION['itc'] = $itc;
    		$requestDetail = TECHPROCESS_SCHEME_CODE."_".$userAmount."_0.0";
		//echo "request detail: ";
		//var_dump($requestDetail);exit;
    		$transactionRequestBean = new TransactionRequestBean();
    		//Setting all values here
		$transactionRequestBean->setMerchantCode(TECHPROCESS_MERCHANT_CODE);
		//$transactionRequestBean->setAccountNo($val['tpvAccntNo']);
		$transactionRequestBean->setITC($itc);
		//$transactionRequestBean->setMobileNumber($val['mobNo']);
		$transactionRequestBean->setCustomerName($custName);
		$transactionRequestBean->setRequestType(TECHPROCESS_REQUEST_TYPE);
		$transactionRequestBean->setMerchantTxnRefNumber($merchantTxtID);
		$transactionRequestBean->setAmount($userAmount);
		$transactionRequestBean->setCurrencyCode(TECHPROCESS_CURRENCY_TYPE);
		$transactionRequestBean->setReturnURL(TECHPROCESS_RETURN_URL);
		$transactionRequestBean->setS2SReturnURL(TECHPROCESS_S2S_RETURN_URL);
		$transactionRequestBean->setShoppingCartDetails($requestDetail);
		$transactionRequestBean->setTxnDate($transDate);
		//$transactionRequestBean->setBankCode($val['bankCode']);
		$transactionRequestBean->setTPSLTxnID($tpslTransId);
		//$transactionRequestBean->setCustId($val['custID']);
		//$transactionRequestBean->setCardId($val['cardID']);
		$transactionRequestBean->setKey(TECHPROCESS_KEY);
		$transactionRequestBean->setIv(TECHPROCESS_IV);
		$transactionRequestBean->setWebServiceLocator(TECHPROCESS_LOCATOR_URL);
		//$transactionRequestBean->setMMID($val['mmid']);
		//$transactionRequestBean->setOTP($val['otp']);
		//$transactionRequestBean->setCardName($val['cardName']);
		//$transactionRequestBean->setCardNo($val['cardNo']);
		//$transactionRequestBean->setCardCVV($val['cardCVV']);
		//$transactionRequestBean->setCardExpMM($val['cardExpMM']);
		//$transactionRequestBean->setCardExpYY($val['cardExpYY']);
		//$transactionRequestBean->setTimeOut($val['timeOut']);
		
		$responseDetails = $transactionRequestBean->getTransactionToken();
		$responseDetails = (array)$responseDetails;
		$response = $responseDetails[0];
		
		if(is_string($response) && preg_match('/^msg=/',$response))
		{
			$outputStr = str_replace('msg=', '', $response);
			$outputArr = explode('&', $outputStr);
			$str = $outputArr[0];
			
			$transactionResponseBean = new TransactionResponseBean();
			$transactionResponseBean->setResponsePayload($str);
			$transactionResponseBean->setKey(TECHPROCESS_KEY);
			$transactionResponseBean->setIv(TECHPROCESS_IV);
			
			$response = $transactionResponseBean->getResponsePayload();
			$_SESSION['response'] = $response;
			$responseArr = explode("|",$response);
			$i=0;
			foreach($responseArr as $value)
			{
				$valueArr = explode("=",$value);
				if($valueArr[0] == "txn_msg" && $valueArr[1] == "success")
				{
					$i++;
					$this->transactionDualVerification($response);
					//redirect('customerpayment/success');
				}
				else if($valueArr[0] == "txn_msg" && $valueArr[1] == "failure")
				{
					$i++;
					redirect('customerpayment/failure');
				}			
			}
			if($i==0) //failed
			{
				redirect('customerpayment/failure');
			}
    		}
    		else if(is_string($response) && preg_match('/^txn_status=/',$response))
    		{
    			$_SESSION['response'] = $response;
			$responseArr = explode("|",$response);
			$i=0;
			foreach($responseArr as $value)
			{
				$valueArr = explode("=",$value);
				if($valueArr[0] == "txn_msg" && $valueArr[1] == "success")
				{
					$i++;
					$this->transactionDualVerification($response);
					//redirect('customerpayment/success');
				}
				else if($valueArr[0] == "txn_msg" && $valueArr[1] == "failure")
				{
					$i++;
					redirect('customerpayment/failure');
				}			
			}
			if($i==0) //failed
			{
				redirect('customerpayment/failure');
			}
		}

    		//echo "<script>window.location = '".$response."'</script>";
    		redirect($response);
    		//ob_flush();
        }
        else if($_POST)
        {
	        /*echo "<pre>post from else if";
	    	print_r($_POST);
	    	echo "</pre>";*/
	    	//exit;
                /*echo "session key" . $this->session->key . "<br>";
                echo "post<pre>";
                print_r($_SESSION);
                exit;*/
		$response = $_POST;
		
		if(is_array($response))
		{
			$str = $response['msg'];
		}
		else if(is_string($response) && strstr($response, 'msg='))
		{
			$outputStr = str_replace('msg=', '', $response);
			$outputArr = explode('&', $outputStr);
			$str = $outputArr[0];
		}
		else 
		{
			$str = $response;
		}
		require_once './techprocess/TransactionResponseBean.php';
		$transactionResponseBean = new TransactionResponseBean();
		
		$transactionResponseBean->setResponsePayload($str);
		//$transactionResponseBean->setKey($this->session->key);
		//$transactionResponseBean->setIv($this->session->iv);
		$transactionResponseBean->setKey($_SESSION['key']);
		$transactionResponseBean->setIv($_SESSION['iv']);
		
		$response = $transactionResponseBean->getResponsePayload();
		/*echo "First Response<pre><br>";
		$_SESSION['first_response'] = $response;
		print_r($response);
		echo "</pre>";*/
		//exit;
		$_SESSION['response'] = $response;		
		//$this->data['response'] = $response;
		
		$responseArr = explode("|",$response);
		$i=0;
		foreach($responseArr as $value)
		{
			$valueArr = explode("=",$value);
			if($valueArr[0] == "txn_msg" && $valueArr[1] == "success")
			{
				$i++;
				$this->transactionDualVerification($response);				
				//redirect('customerpayment/success');
			}
			else if($valueArr[0] == "txn_msg" && $valueArr[1] == "failure")
			{
				$i++;	
				redirect('customerpayment/failure');
			}			
		}
		if($i==0) //failed
		{
			//$this->session->set_flashdata('response',$response);
			redirect('customerpayment/failure');
		}
	}
    }
    
    public function transactionDualVerification($res)
    {
    	require_once './techprocess/TransactionRequestBean.php';
    	$responseArr = explode("|",$res);
	
	foreach($responseArr as $value)
	{
		$valueArr = explode("=",$value);
		if($valueArr[0] == "txn_amt")
		{
			$amountPaid = $valueArr[1];
		}
		else if($valueArr[0] == "tpsl_txn_id")
		{
			$tpslTxnID = $valueArr[1];
		}
		else if($valueArr[0] == "clnt_txn_ref")
		{
			$txnRefNumber = $valueArr[1];
		}			
	}
    	$transactionRequestBean = new TransactionRequestBean();
	//Setting all values here
	$transactionRequestBean->setRequestType("S");
	$transactionRequestBean->setMerchantCode(TECHPROCESS_MERCHANT_CODE);
	$transactionRequestBean->setKey(TECHPROCESS_KEY);
	$transactionRequestBean->setIv(TECHPROCESS_IV);
	$transactionRequestBean->setMerchantTxnRefNumber($txnRefNumber);
	$transactionRequestBean->setTPSLTxnID($tpslTxnID);
	$transactionRequestBean->setAmount($amountPaid);
	$transactionRequestBean->setWebServiceLocator(TECHPROCESS_LOCATOR_URL);
	
	/*$transactionRequestBean->setITC($itc);	
	$transactionRequestBean->setCustomerName($custName);	
	$transactionRequestBean->setCurrencyCode(TECHPROCESS_CURRENCY_TYPE);
	$transactionRequestBean->setReturnURL(TECHPROCESS_RETURN_URL);
	$transactionRequestBean->setS2SReturnURL(TECHPROCESS_S2S_RETURN_URL);
	$transactionRequestBean->setShoppingCartDetails($requestDetail);
	$transactionRequestBean->setTxnDate($transDate);*/
	
	
	$responseDetails = $transactionRequestBean->getTransactionToken();
	$responseDetails = (array)$responseDetails;
	$response = $responseDetails[0];	
	
	$_SESSION['response'] = $response;
	$responseArr = explode("|",$response);
	$i=0;
	foreach($responseArr as $value)
	{
		$valueArr = explode("=",$value);
		if($valueArr[0] == "txn_msg" && ($valueArr[1] == "SUCCESS"||$valueArr[1] == "success"))
		{
			$i++;
			redirect('customerpayment/success');
		}
		else if($valueArr[0] == "txn_msg" && $valueArr[1] == "failure")
		{
			$i++;
			redirect('customerpayment/failure');
		}			
	}
	if($i==0) //failed
	{
		redirect('customerpayment/failure');
	}
    }
    
    public function success()
    {
    	session_start();
    	if(!empty($_SESSION['response']))
    	{
	    	//echo $_SESSION['first_response']."<br>";
	    	//echo $_SESSION['response'];
	    	$responseArr = explode("|",$_SESSION['response']);
		
		foreach($responseArr as $value)
	  
									
		{
			$valueArr = explode("=",$value);
			if($valueArr[0] == "txn_amt")
										
	  
														   
	  
								
			{
				$this->data['amountPaid'] = $valueArr[1];
			}
			else if($valueArr[0] == "tpsl_txn_id")
			{
				$this->data['tpslTxnID'] = $valueArr[1];
			}
			else if($valueArr[0] == "clnt_txn_ref")
			{
				$this->data['txnRefNumber'] = $valueArr[1];
			}			
		}
	
							   
	 
					 
										 
	  
	    	$this->data['userEmail'] = $_SESSION['userEmail'];
	    	$this->data['userFullName'] = $_SESSION['userFullName'];
	    	$this->data['invoiceNumber'] = $_SESSION['invoiceNumber'];
	    	$this->data['notes'] = $_SESSION['notes'];
	    	//$this->data['response'] = $response['response'];
	    	/* start of - send email to the user */
													
												   
													  
													
													   
										   
	  
											 
	    	$message = $this->get_user_email_message($this->data);
	    	$email_values = array('mail_type'=>'html',
					'from_mail_id'=>$this->config->item('site_contact_mail'),
					'mail_name'=>$this->config->item('email_title'),
					'to_mail_id'=>$_SESSION['userEmail'],
					'subject_message'=>"Payment Confirmation",
					'body_messages'=>$message
					);
		$email_send_to_common = $this->product_model->common_email_send($email_values);
		/* end of - send email to the user */
		/* start of - send email to the finance team */
		$messageFinTeam = $this->get_finance_team_email_message($this->data);
	    	$finance_team_email_values = array('mail_type'=>'html',
					'from_mail_id'=>$this->config->item('site_contact_mail'),
					'mail_name'=>$this->config->item('email_title'),
					'to_mail_id'=>FINANCE_TEAM_EMAIL,
					'subject_message'=>"Received Payment from ".$_SESSION['userFullName']." (".$_SESSION['userEmail'].")",
					'body_messages'=>$messageFinTeam
					);
		$email_send_to_finance_team = $this->product_model->common_email_send($finance_team_email_values);
		/* end of - send email to the finance team */
		// insert data into table
		$insertArr = array("cp_email"=>$_SESSION['userEmail'],
					"cp_invoice_number"=>$_SESSION['invoiceNumber'],
					"cp_notes"=>$_SESSION['notes'],
					"cp_amount"=>$this->data['amountPaid'],
					"cp_merchant_txn_ref"=>$this->data['txnRefNumber'],
					"cp_tpsl_txn_id"=>$this->data['tpslTxnID'],
					"cp_itc"=>$_SESSION['itc']
				);
		$this->customerpayment_model->add_customer_payment($insertArr);
		unset($_SESSION['response']);
									  
	    	$this->load->view('site/customerpayment/success.php',$this->data);
    	}
    	else
    	{
    		redirect('customerpayment');
    	}
	 
    }
    
    public function failure()
    {   
    	session_start();
	 
										 
	  
														
																							 
																								
												
	  
																						   
													
													
												   
													  
													
													   
												
										   
											 
																   
												
																							 
																			   
										  
										 
							  
	   
																				 
									   
												 
																			  
																						   
																							 
																			   
									  
																																			  
									 
	   
																									
											   
				 
									  
				  
									  
																		  
	  
		 
	  
								  
	  
	 
	
							 
	 
					 
    	if(!empty($_SESSION['response']))
    	{
	 
	    	//echo $_SESSION['response'];
								  
	 
	    	$responseArr = explode("|",$_SESSION['response']);
		
		foreach($responseArr as $value)
		{
			$valueArr = explode("=",$value);
			if($valueArr[0] == "txn_amt")
			{
				$this->data['amountPaid'] = $valueArr[1];
			}
			else if($valueArr[0] == "tpsl_txn_id")
			{
				$this->data['tpslTxnID'] = $valueArr[1];
			}
			else if($valueArr[0] == "clnt_txn_ref")
			{
				$this->data['txnRefNumber'] = $valueArr[1];
			}			
		}
	    	$this->data['userEmail'] = $_SESSION['userEmail'];
	    	$this->data['userFullName'] = $_SESSION['userFullName'];
	    	$this->data['invoiceNumber'] = $_SESSION['invoiceNumber'];
	    	$this->data['notes'] = $_SESSION['notes'];
	    	$this->data['userAmount'] = $_SESSION['userAmount'];
	    	//$this->data['response'] = $response['response'];
	    	/* start of - send email to the user */
	    	$message = $this->get_user_failed_email_message($this->data);
	    	$email_values = array('mail_type'=>'html',
					'from_mail_id'=>$this->config->item('site_contact_mail'),
					'mail_name'=>$this->config->item('email_title'),
					'to_mail_id'=>$_SESSION['userEmail'],
					'subject_message'=>"Payment Failed",
					'body_messages'=>$message
					);
		$email_send_to_common = $this->product_model->common_email_send($email_values);
		/* end of - send email to the user */
		/* start of - send email to the finance team */
		$messageFinTeam = $this->get_finance_team_failed_email_message($this->data);
	    	$finance_team_email_values = array('mail_type'=>'html',
					'from_mail_id'=>$this->config->item('site_contact_mail'),
					'mail_name'=>$this->config->item('email_title'),
					'to_mail_id'=>FINANCE_TEAM_EMAIL,
					'subject_message'=>"Failed Payment info - ".$_SESSION['userFullName']." (".$_SESSION['userEmail'].")",
					'body_messages'=>$messageFinTeam
					);
		$email_send_to_finance_team = $this->product_model->common_email_send($finance_team_email_values);
		/* end of - send email to the finance team */
	    	//$this->data['response'] = $response['response'];
	    	unset($_SESSION['response']);
																			   
									
											
																					
												
							   
	  
																								 
  
							
								  
										   
								
						
						   
									  
			   
					
				  
				   
											 
				   
																					   
												 
	
																																					
	
									
													
													  
								
										  
		 
 
		  
   
										 
				  
						
				 
		  
									   
	
										 
	 
																						  
	 
		
	 
																			  
	  
																																																																																																												   
	   
																	  
	   
	  
	 
	
																							 
	
																																							 
									   
								
																																																										   
	 
								 
									   
																																										  
																				  
	  
									
																																		
	  
	 
																																									 
	   
											   
	   
	
									
										
								
		  
											 
	
											   
	
																		
					  
	
						   
				  
										 
																																			 
	  
																																																								   
	  
		  
																																		 
	
											 
											   
																							 
							 
										 
	
				   
	 
																					
																							 
	 
	
									
						
	
									  
												
	 
																																																																		  
										  
																				  
																					 
																								
												 
																																				  
	  
	  
																							  
																					   
	  
		 
	  
																						   
					  
							 
	  
	 
									
							   
										  
											   
	 
										 
																																																																																																																																					 
		
																	   
		
		   
	 
		
	 
										  
																																																																																																																																	 
	 
	
						 
							
										  
																																			  
								  
											  
																				  
																						
																																			 
																		   
																							 
								
										 
				   
	 
					   
							
	 
		
	 
											 
							 
	 
								 
	
   
   
		 
						  
							   
	    	$this->load->view('site/customerpayment/failure.php',$this->data);
	 
    	}
    	else
    	{
    		redirect('customerpayment');
    	}
    }
    
							 
		
					 
								
													   
 
								
  
								  
							   
   
											
   
										
   
										   
   
										 
   
											  
	  
  
													   
															 
																							  
											   
														 
													   
											
																								   
											   
																						   
													
										 
										
							 
	  
																				
									  
												
																			 
															
																						   
													
									 
																																			 
									
	  
																								   
											  
													   
																	   
	 
	
    public function get_user_failed_email_message($data)
    {
	$message = '<div style="width: 1012px; background: #FFFFFF; margin: 0 auto;">
			Hi '.$data['userFullName'].',<br><br>This is to inform you that the transaction you have tried at '.base_url().' has FAILED. Please try again.<br><br>We apologize for the inconvenience.<br><br>Regards,<br>Finance Team<br>Cityfurnish<br></div>';
	return $message;
    }
    public function get_finance_team_failed_email_message($data)
    {
    	$message = '<div style="width: 1012px; background: #FFFFFF; margin: 0 auto;">
			Dear Team,<br><br>The following user tried to process the payment. But, it has failed.<br><br>
			<table>
				<tr>
					<td>Customer Name: </td>
					<td>'.$data['userFullName'].'</td>
				</tr>
				<tr>
					<td>Customer Email: </td>
					<td>'.$data['userEmail'].'</td>
				</tr>
				<tr>
					<td>Amount: </td>
					<td>'.$data['userAmount'].'</td>
				</tr>
				<tr>
					<td>Invoice Number: </td>
					<td>'.$data['invoiceNumber'].'</td>
				</tr>
				<tr>
					<td>Notes: </td>
					<td>'.$data['notes'].'</td>
				</tr>
			</table>
			<br><br>Regards,<br>Finance Team<br>Cityfurnish<br></div>';
    	return $message;
    }
    public function get_finance_team_email_message($data)
    {
	    $message = '<div style="width: 1012px; background: #FFFFFF; margin: 0 auto;">
				<div style="width: 99.8%; background: #f3f3f3;border: 1px solid #454b56; text-align: center; margin: 0 auto;">
					<div><a id="logo" target="_blank" href="'.base_url().'"><img title="'.$this->config->item('meta_title').'" alt="'.$this->config->item('meta_title').'" src="'.base_url().'images/logo/'.$this->data['logo'].'" /></a></div>
					</div>
					<div style="width: 970px; background: #FFFFFF; float: left; padding: 20px; border: 1px solid #454B56;">
					<div style="float: right; width: 35%; margin-bottom: 20px; margin-right: 7px;">
						<table style="border: 1px solid #cecece;" cellpadding="0" cellspacing="0" border="0" width="100%">
							<tbody>
								<tr bgcolor="#f3f3f3">
									<td style="border-right: 1px solid #cecece;" width="87"><span style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; width: 100%; font-weight: bold; color: #000000; line-height: 38px; float: left;">Transaction Ref#</span></td>
									<td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">#'.$data['txnRefNumber'].'</span></td>
								</tr>
								<tr bgcolor="#f3f3f3">
									<td style="border-right: 1px solid #cecece;" width="87"><span style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; width: 100%; font-weight: bold; color: #000000; line-height: 38px; float: left;">Transaction Date</span></td>
									<td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.date("F j, Y g:i a").'</span></td>
								</tr>
							</tbody>
						</table>
					</div>
		
					<div style="float: left; width: 100%; margin-right: 3%; margin-top: 10px; font-size: 14px; font-weight: normal; line-height: 28px; font-family: Arial, Helvetica, sans-serif; color: #000; overflow: hidden;">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tbody>
								<tr>
									<td colspan="3">
										<table style="border: 1px solid #cecece; width: 99.5%;" cellpadding="0" cellspacing="0" border="0" width="100%">
											<tbody>
											<tr bgcolor="#f3f3f3">
												<td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Customer Name</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['userFullName'].'</span></td></tr>
												<tr><td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Customer Email</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['userEmail'].'</span></td></tr>
											<tr bgcolor="#f3f3f3">
												<td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Invoice Number</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['invoiceNumber'].'</span></td></tr>
												
												<tr><td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">TPSL Transaction ID</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['tpslTxnID'].'</span></td></tr>
												<tr bgcolor="#f3f3f3"><td style="border-right: 1px solid #cecece; text-align: center;" width="40%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Amount Paid</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['amountPaid'].'&nbsp;'.TECHPROCESS_CURRENCY_TYPE.'</span></td></tr>
												
												<tr><td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Notes</span></td><td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.$data['notes'].'</span></td></tr>
											
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
				</div>
			</div>';


		return $message;
    }
    
    public function get_user_email_message($data)
    {
    	$message = '<div style="width: 1012px; background: #FFFFFF; margin: 0 auto;">
			<div style="width: 99.8%; background: #f3f3f3;border: 1px solid #454b56; text-align: center; margin: 0 auto;">
				<div><a id="logo" target="_blank" href="'.base_url().'"><img title="'.$this->config->item('meta_title').'" alt="'.$this->config->item('meta_title').'" src="'.base_url().'images/logo/'.$this->data['logo'].'" /></a></div>
				</div>
				<div style="width: 970px; background: #FFFFFF; float: left; padding: 20px; border: 1px solid #454B56;">
				<div style="float: right; width: 35%; margin-bottom: 20px; margin-right: 7px;">
					<table style="border: 1px solid #cecece;" cellpadding="0" cellspacing="0" border="0" width="100%">
						<tbody>
							<tr bgcolor="#f3f3f3">
								<td style="border-right: 1px solid #cecece;" width="87"><span style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; width: 100%; font-weight: bold; color: #000000; line-height: 38px; float: left;">Transaction Ref#</span></td>
								<td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">#'.$data['txnRefNumber'].'</span></td>
							</tr>
							<tr bgcolor="#f3f3f3">
								<td style="border-right: 1px solid #cecece;" width="87"><span style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; width: 100%; font-weight: bold; color: #000000; line-height: 38px; float: left;">Transaction Date</span></td>
								<td width="100"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; color: #000000; line-height: 38px; text-align: center; width: 100%; float: left;">'.date("F j, Y g:i a").'</span></td>
							</tr>
						</tbody>
					</table>
				</div>
	
				<div style="float: left; width: 100%; margin-right: 3%; margin-top: 10px; font-size: 14px; font-weight: normal; line-height: 28px; font-family: Arial, Helvetica, sans-serif; color: #000; overflow: hidden;">
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tbody>
							<tr>
								<td colspan="3">
									<table style="border: 1px solid #cecece; width: 99.5%;" cellpadding="0" cellspacing="0" border="0" width="100%">
										<tbody>
										<tr bgcolor="#f3f3f3">
											<td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Invoice Number(optional)</span></td>
											<td style="border-right: 1px solid #cecece; text-align: center;" width="16%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">TPSL Transaction ID</span></td>
											<td style="border-right: 1px solid #cecece; text-align: center;" width="40%"><span style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000000; line-height: 38px; text-align: center;">Amount Paid</span></td>
										</tr>
										<tr>
											<td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$data['invoiceNumber'].'</span></td>
											<td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$data['tpslTxnID'].'</span></td>
											<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$data['amountPaid'].'&nbsp;'.TECHPROCESS_CURRENCY_TYPE.'</span></td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div style="width:50%; float:left;">
            				<div style="float:left; width:100%;font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; width:100%; color:#000000; line-height:38px; "><span>'.$data['userFullName'].'</span>, thank you for the payment.</div>
                
               				<ul style="width:100%; margin:10px 0px 0px 0px; padding:0; list-style:none; float:left; font-size:12px; font-weight:normal; line-height:19px; font-family:Arial, Helvetica, sans-serif; color:#000;">
			                    <li>If you have any concerns please contact us.</li>
			                    <li>Email: <span>'.stripslashes($this->data['siteContactMail']).' </span></li>
			                    <li>Phone: +91 8010845000</li>
			               </ul>
			        </div>            
            			<div style="width:27.4%; margin-right:5px; float:right;"></div>        
				<div style="clear:both"></div>
			</div>
		</div>';


		return $message;


    }
    
    public function checkTransactionStatus()
    {
    	require_once './techprocess/TransactionRequestBean.php';
    	//$res = "txn_status=0300|txn_msg=success|txn_err_msg=NA|clnt_txn_ref=585326|tpsl_bank_cd=470|tpsl_txn_id=385700210|txn_amt=4.50|clnt_rqst_meta={itc:NIC~Bhujangarao~Nalluri~vinitj@gmail.com~Payment~4.5~Invoice~TESTINV001}{custname:Bhujangarao Nalluri}|tpsl_txn_time=03-11-2017 13:51:45|tpsl_rfnd_id=NA|bal_amt=NA|rqst_token=792e48b9-3d5c-485b-b893-2eb7f1c83136|hash=863293df775552662c1881f4b6a10f3e351ae21d";
    	$res = "txn_status=0300|txn_msg=success|txn_err_msg=NA|clnt_txn_ref=859154|tpsl_bank_cd=50|tpsl_txn_id=399316852|txn_amt=2.00|clnt_rqst_meta={itc:NIC~Bhujangarao~Nalluri~bnalluri@osius.com~Payment~2~Invoice~OSI-INV004}{custname:Bhujangarao Nalluri}|tpsl_txn_time=06-11-2017 15:46:42|tpsl_rfnd_id=NA|bal_amt=NA|rqst_token=3d942f2d-a23f-45ba-b401-d89b83781503|hash=eb942ca6a12d76bfb2df76e78c9096b9fb687632";
    	$responseArr = explode("|",$res);
	
	foreach($responseArr as $value)
	{
		$valueArr = explode("=",$value);
		if($valueArr[0] == "txn_amt")
		{
			$amountPaid = $valueArr[1];
		}
		else if($valueArr[0] == "tpsl_txn_id")
		{
			$tpslTxnID = $valueArr[1];
		}
		else if($valueArr[0] == "clnt_txn_ref")
		{
			$txnRefNumber = $valueArr[1];
		}			
	}
    	$transactionRequestBean = new TransactionRequestBean();
	//Setting all values here
	$transactionRequestBean->setRequestType("O");
	$transactionRequestBean->setMerchantCode(TECHPROCESS_MERCHANT_CODE);
	$transactionRequestBean->setKey(TECHPROCESS_KEY);
	$transactionRequestBean->setIv(TECHPROCESS_IV);
	$transactionRequestBean->setMerchantTxnRefNumber($txnRefNumber);
	$transactionRequestBean->setTPSLTxnID($tpslTxnID);
	$transactionRequestBean->setAmount($amountPaid);
	$transactionRequestBean->setWebServiceLocator(TECHPROCESS_LOCATOR_URL);
	
	/*$transactionRequestBean->setITC($itc);	
	$transactionRequestBean->setCustomerName($custName);	
	$transactionRequestBean->setCurrencyCode(TECHPROCESS_CURRENCY_TYPE);
	$transactionRequestBean->setReturnURL(TECHPROCESS_RETURN_URL);
	$transactionRequestBean->setS2SReturnURL(TECHPROCESS_S2S_RETURN_URL);
	$transactionRequestBean->setShoppingCartDetails($requestDetail);
	$transactionRequestBean->setTxnDate($transDate);*/
	
	
	$responseDetails = $transactionRequestBean->getTransactionToken();
	$responseDetails = (array)$responseDetails;
	$response = $responseDetails[0];
	echo "response:<pre>";
	print_r($response);
	echo "</pre>";
	exit;
    }
    
    public function refundTransaction()
    {
    	/*require_once './techprocess/TransactionRequestBean.php';
    	$res = "txn_status=0300|txn_msg=success|txn_err_msg=NA|clnt_txn_ref=585326|tpsl_bank_cd=470|tpsl_txn_id=385700210|txn_amt=4.50|clnt_rqst_meta={itc:NIC~Bhujangarao~Nalluri~vinitj@gmail.com~Payment~4.5~Invoice~TESTINV001}{custname:Bhujangarao Nalluri}|tpsl_txn_time=03-11-2017 13:51:45|tpsl_rfnd_id=NA|bal_amt=NA|rqst_token=792e48b9-3d5c-485b-b893-2eb7f1c83136|hash=863293df775552662c1881f4b6a10f3e351ae21d";
    	$responseArr = explode("|",$res);
	
	foreach($responseArr as $value)
	{
		$valueArr = explode("=",$value);
		if($valueArr[0] == "txn_amt")
		{
			$amountPaid = $valueArr[1];
		}
		else if($valueArr[0] == "tpsl_txn_id")
		{
			$tpslTxnID = $valueArr[1];
		}
		else if($valueArr[0] == "clnt_txn_ref")
		{
			$txnRefNumber = $valueArr[1];
		}
		else if($valueArr[0] == "clnt_rqst_meta")
		{
			$itc = $valueArr[1];
		}			
	}
    	$transactionRequestBean = new TransactionRequestBean();
	//Setting all values here
	$transactionRequestBean->setRequestType("R");
	$transactionRequestBean->setMerchantCode(TECHPROCESS_MERCHANT_CODE);
	$transactionRequestBean->setKey(TECHPROCESS_KEY);
	$transactionRequestBean->setIv(TECHPROCESS_IV);
	$transactionRequestBean->setMerchantTxnRefNumber($txnRefNumber);
	$transactionRequestBean->setTPSLTxnID($tpslTxnID);
	$transactionRequestBean->setAmount($amountPaid);
	$transactionRequestBean->setWebServiceLocator(TECHPROCESS_LOCATOR_URL);
	
	
	
	
	$responseDetails = $transactionRequestBean->getTransactionToken();
	$responseDetails = (array)$responseDetails;
	$response = $responseDetails[0];
	echo "response:<pre>";
	print_r($response);
	echo "</pre>";
	exit;*/
    }
}