<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**

 * 

 * User related functions

 * @author Teamtweaks

 *

 */
class Offlinepayment extends MY_Controller {

    function __construct() {

        parent::__construct();

        $this->load->helper(array('cookie', 'date', 'form', 'email'));

        $this->load->library(array('encrypt', 'form_validation','curl'));

        $this->load->model('checkout_model');
    }

    public function index(){
        unset($_SESSION['Applied_Walled']);
        $user_id = $this->uri->segment(2);
        $dealcodenumber = $this->uri->segment(3);
        $orderDetails = $this->checkout_model->get_all_details(PAYMENT,array('user_id' => $user_id, 'dealCodeNumber' => $dealcodenumber));
        if($orderDetails->num_rows() == 0){
             $this->session->set_flashdata('payment_flash',array('class' => 'error', 'message' => 'Opps, link expired','img' => 'cancel-order-icn.svg'));
            $this->data['Confirmation'] = 'Message';                
            $this->load->view('site/order/offline_order.php',$this->data);exit;
        }else if($orderDetails->row()->status == 'Paid'){
            $this->session->set_flashdata('payment_flash',array('class' => 'success', 'message' => 'Payment already paid for your order','img' => 'thankyou-thumb.svg'));
            $this->data['Confirmation'] = 'Message';                
            $this->load->view('site/order/offline_order.php',$this->data);exit;
        }
        $this->data['payu_type'] =  $this->uri->segment(4) == 'si' ? 'si' : ''; 
        $this->data['PapiInfo'] = $this->PayuPaymentInfo($user_id,$dealcodenumber); //Load Citurs info in Data Veriable
        $this->load->view('site/checkout/checkout_payu.php', $this->data);
    }

    public function PayuPaymentInfo($user_id,$dealCode){
        $user_info = $this->checkout_model->get_all_details(USERS, array('id' => $user_id));
        $condition = array();
        $data = $this->checkout_model->get_all_details(PAYMENT_GATEWAY,$condition);
        $gatewaySettings = $data->result();
        foreach ($data->result() as $key => $val){
                    $gatewaySettings[$key]->settings = unserialize($val->settings);
        }
        if($gatewaySettings[3]->settings['mode'] == 'sandbox'){
            $PAYU_BASE_URL =  "https://test.payu.in/_payment";
        }else{
            $PAYU_BASE_URL =  "https://secure.payu.in/_payment";
        }
        $loginUserId = $user_id;
        $lastFeatureInsertId = $dealCode;
        $MERCHANT_KEY = $gatewaySettings[3]->settings['merchant_key']; //Please change this value with live key for production
        $hash_string = '';
        $SALT = $gatewaySettings[3]->settings['salt']; //Please change this value with live salt for production
        $productinfo = 'Cityfurnish';
        $orderDetails = $this->checkout_model->get_all_details(PAYMENT,array('user_id' => $user_id, 'dealCodeNumber' => $dealCode));
        $rental_amount = $this->checkout_model->get_all_details(OFFLINE_ORDERS,array('dealCodeNumber' => $dealCode));

        $orderAmount = ($orderDetails->row()->total - $rental_amount->row()->advance_rental);
        if($this->data['payu_type'] == 'si') {
            $si_discounts_row = $this->checkout_model->get_all_details(SI_DISCOUNT_SETTING,array());
            $orderAmount = $orderAmount - $si_discounts_row->row()->discount_amount;
            $orderTotal = $orderDetails->row()->total - $si_discounts_row->row()->discount_amount;
            $this->checkout_model->update_details(PAYMENT,array('discount_on_si' =>$si_discounts_row->row()->discount_amount),array('dealCodeNumber' => $dealCode));
        }
        $action = '';
        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        $hash_key =  hash('sha512', $MERCHANT_KEY.'|'.$txnid.'|'.$orderAmount.'|'.$productinfo.'|'.$user_info->row()->full_name.'|'.$user_info->row()->email.'|||||||||||'.$SALT);
        $time = time() * 1000;
        $time = number_format($time, 0, '.', ''); 
        $this->data['ABC']['action_url'] = $PAYU_BASE_URL;
        $this->data['ABC']['key'] = $MERCHANT_KEY;
        $this->data['ABC']['currency'] = 'INR';
        $this->data['ABC']['txnid'] = $txnid;
        $this->data['ABC']['hash'] = $hash_key;
        $this->data['ABC']['email'] = $user_info->row()->email;
        $this->data['ABC']['firstname'] = $user_info->row()->full_name;
        $this->data['ABC']['phone'] = $user_info->row()->phone_no;
        $this->data['ABC']['productinfo'] = $productinfo;
        $this->data['ABC']['user_credentials'] = $MERCHANT_KEY.':'.$user_info->row()->email.$time;
        $this->data['ABC']['orderAmount'] = $orderAmount;
        $this->data['ABC']['time'] = $time;
        $this->data['ABC']['returnUrl'] = base_url() . 'handlepayment/success/' . $loginUserId . '/' . $lastFeatureInsertId;
        $this->data['ABC']['faile_url'] = base_url() . 'handlepayment/failure/'.$loginUserId;
         return $this->data['ABC'];
    }

}