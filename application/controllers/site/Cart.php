<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */

class Cart extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation','Mobile_Detect'));
                $this->load->helper('url');
		$this->load->model('cart_model');
		$this->load->model('order_model');
		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->cart_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];
                
		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->cart_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];

		$this->data['loginCheck'] = $this->checkLogin('U');
		//$this->data['MiniCartViewSet'] = $this->cart_model->mini_cart_view($this->data['common_user_id']);
	}


	/**
	 *
	 * Loading Cart Page
	 */

	public function index(){
			
		if ($this->data['loginCheck'] != ''){
			$mobiledetect = new Mobile_Detect();
			if ($mobiledetect->isMobile() || $mobiledetect->isTablet() || $mobiledetect->isAndroidOS()){
				$this->data['is_mobile'] = 1;
			}else{
				$this->data['is_mobile'] = 0;
			}			
			$this->data['heading'] = 'Cart';
			$this->data['cartViewResults'] = $this->cart_model->mani_cart_view($this->data['common_user_id']);
			$this->data['countryList'] = $this->cart_model->get_all_details(COUNTRY_LIST,array(),array(array('field'=>'name','type'=>'asc')));			
			$this->load->view('site/cart/cart.php',$this->data);
		}else{
			redirect('login');
		}
	}


	/****************** Insert the cart to user********************/

	public function insertEditCart(){

		$excludeArr = array('addtocart');
		$dataArrVal = array();
		foreach($this->input->post() as $key => $val){
			if(!(in_array($key,$excludeArr))){
				$dataArrVal[$key] = trim(addslashes($val));
			}
		}

		$datestring = "%Y-%m-%d 23:59:59";
		$code = $this->get_rand_str('10');
		$exp_days = $this->config->item('cart_expiry_days');

		$dataArry_data = array('expiry_date' => mdate($datestring,strtotime($exp_days.' days')), 'code' => $code,'user_id' => $this->data['common_user_id']);
		$dataArr = array_merge($dataArrVal,$dataArry_data);

		$condition ='';

		$this->cart_model->commonInsertUpdate(GIFTCARDS_TEMP,'insert',$excludeArr,$dataArr,$condition);

		if ($this->checkLogin('U') != ''){
			if($this->lang->line('gift_add_success') != '')
				$lg_err_msg = $this->lang->line('gift_add_success');
			else 
				$lg_err_msg = 'Giftcard Added You Cart successfully';
			$this->setErrorMessage('success',$lg_err_msg);
			redirect('gift-cards');
		}else{
			redirect('login');
		}
	}


	public function cartadd(){
		$excludeArr = array('addtocart','attr_color','mqty','selected_tenure');
		$dataArrVal = array();
		$mqty = $this->input->post('mqty');
		foreach($this->input->post() as $key => $val){
			if(!(in_array($key,$excludeArr))){
                            $dataArrVal[$key] = trim(addslashes($val));
			}
		}
		
		
		
		$cart_data =  $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $this->data['common_user_id']));
		if($cart_data->row()->discount_on_si != ''){
                    $discount_on_si = '';
                    $dataArr1 = array('discount_on_si' => $discount_on_si);
                    $condition1 = array('user_id' => $this->data['common_user_id']);
                    $this->cart_model->update_details(SHOPPING_CART, $dataArr1, $condition1);  
        }
		$current_product_attr =  $this->cart_model->get_all_details(SUBPRODUCT,array( 'pid' => $this->input->post('attribute_values') ));
		foreach ($cart_data->result() as $key => $value) {
			if($value->is_coupon_used){
				$is_coupon_used = $value->is_coupon_used;
				$coupon_code = $value->couponCode;
				$couponID =  $value->couponID;
			}
			$attr_value =  $this->cart_model->get_all_details(SUBPRODUCT,array( 'pid' =>$value->attribute_values ));
			// print_r($attr_value->row()->attr_name);exit;
			if($this->input->post('attribute_values') != '' && $attr_value->row()->attr_name !=''){
				if(strtoupper($attr_value->row()->attr_name) != strtoupper($current_product_attr->row()->attr_name))
				{
					$data = array('Error'=>'AttrError','message' => "Please select same tenure as selected for other cart items.");
					header('Content-Type: application/json');
	                   echo json_encode($data);exit;
				}
			}
		}
		
		if($this->input->post('selected_tenure')){
			$this->session->set_userdata(array('first_tenure' => $this->input->post('selected_tenure')));
		}

	    $product_details =   $this->cart_model->get_all_details(PRODUCT,array( 'id' =>  $this->input->post('product_id')));
		if($this->input->post('attribute_values') !=''){
			$dataArrVal['price'] = $current_product_attr->row()->attr_price;
			$dataArrVal['product_shipping_cost'] = $product_details->row()->shipping_cost;
		}else{
		    $dataArrVal['price'] = $product_details->row()->price;
			$dataArrVal['product_shipping_cost'] = $product_details->row()->shipping_cost;
		}
		$datestring = date('Y-m-d H:i:s',now());
		$indTotal = ( $current_product_attr->row()->attr_price + $product_details->row()->shipping_cost + ($current_product_attr->row()->attr_price * 0.01 * $this->input->post('product_tax_cost')) ) * $this->input->post('quantity');

		$dataArry_data = array('created' => $datestring, 'user_id' => $this->data['common_user_id'], 'indtotal' => $indTotal, 'total' => $indTotal);
		$dataArr = array_merge($dataArrVal,$dataArry_data);
		$condition ='';

		if($this->input->post('attribute_values') !=''){

		$this->data['productVal'] = $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $this->data['common_user_id'],'product_id' => $this->input->post('product_id'),'attribute_values' => $this->input->post('attribute_values')));
		}else {
			$this->data['productVal'] = $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $this->data['common_user_id'],'product_id' => $this->input->post('product_id')));
		}
		$for_qty_check = $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $this->data['common_user_id'],'product_id' => $this->input->post('product_id')));


		if($for_qty_check->num_rows() > 0){
			$new_tot_qty = 0;
			foreach ($for_qty_check->result() as $for_qty_check_row){
				$new_tot_qty += $for_qty_check_row->quantity;
			}
			$new_tot_qty += $this->input->post('quantity');
			if ($new_tot_qty <= $mqty){
				if ($this->data['productVal']->num_rows() > 0){
				// 	$newQty = $this->data['productVal']->row()->quantity + $this->input->post('quantity');
				// 	$indTotal = ( $this->input->post('price') + $this->input->post('product_shipping_cost') + ($this->input->post('price') * 0.01 * $this->input->post('product_tax_cost')) ) * $newQty ;
				// 	$dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal);
				// 	$condition =array('id' => $this->data['productVal']->row()->id);
				    $newQty = $this->data['productVal']->row()->quantity + $this->input->post('quantity');
					$indTotal = ( $current_product_attr->row()->attr_price +  $product_details->row()->shipping_cost + ($current_product_attr->row()->attr_price * 0.01 * $this->input->post('product_tax_cost'))) * $newQty;
					$dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal);
					$condition =array('id' => $this->data['productVal']->row()->id);
					$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
				}else {
				$message = 	$this->cart_model->commonInsertUpdate(SHOPPING_CART,'insert',$excludeArr,$dataArr,$condition);
				}
			}else{
				$cart_qty = $new_tot_qty - $this->input->post('quantity');
				//echo 'Error|'.$cart_qty; die;
				$data = array('Error'=>'Error','show_error' => $cart_qty);
				header('Content-Type: application/json');
                   echo json_encode($data);exit;
			}
		}else{
			$this->cart_model->commonInsertUpdate(SHOPPING_CART,'insert',$excludeArr,$dataArr,$condition);
		}


		if ( $this->checkLogin('U')!= ''){
			/***Mini cart Lg****/

			$mini_cart_lg = array();

			if($this->lang->line('items') != '')
			$mini_cart_lg['lg_items'] =  stripslashes($this->lang->line('items'));
			else
			$mini_cart_lg['lg_items'] =  "items";

			if($this->lang->line('header_description') != '')
			$mini_cart_lg['lg_description'] =  stripslashes($this->lang->line('header_description'));
			else
			$mini_cart_lg['lg_description'] =  "Description";

			if($this->lang->line('qty') != '')
			$mini_cart_lg['lg_qty'] =  stripslashes($this->lang->line('qty'));
			else
			$mini_cart_lg['lg_qty'] =  "Qty";

			if($this->lang->line('giftcard_price') != '')
			$mini_cart_lg['lg_price'] =  stripslashes($this->lang->line('giftcard_price'));
			else
			$mini_cart_lg['lg_price'] =  "Price";

			if($this->lang->line('order_sub_total') != '')
			$mini_cart_lg['lg_sub_tot'] =  stripslashes($this->lang->line('order_sub_total'));
			else
			$mini_cart_lg['lg_sub_tot'] =  "Order Sub Total";

			if($this->lang->line('proceed_to_checkout') != '')
			$mini_cart_lg['lg_proceed'] =  stripslashes($this->lang->line('proceed_to_checkout'));
			else
			$mini_cart_lg['lg_proceed'] =  "Proceed to Checkout";

			/***Mini cart Lg****/

			 // echo 'Success|'.$this->cart_model->mini_cart_view($this->data['common_user_id'],$mini_cart_lg);

			// echo 'count|'.$cart_count;
			 // $response[] =  $this->minicart_model->items_in_cart($this->data['common_user_id']);
			 $data =  array('product_value'=>  $this->minicart_model->items_in_cart($this->data['common_user_id']),'message'=>'Product is added successfully','coupon_code' => $coupon_code);
			 header('Content-Type: application/json');
                   echo json_encode($data);exit;


		}else{
			//echo 'login|lgoin';
				$data = array('login'=>'login');
				header('Content-Type: application/json');
                   echo json_encode($data);exit;
		}
	}



	public function ajaxUpdate(){
                
		$excludeArr = array('id','qty','updval', 'op');

		$productVal = $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $this->data['common_user_id'],'id' => $this->input->post('updval')));

                $oldQty = $productVal->row()->quantity;
                $oldDisc = $productVal->row()->discountAmount;
                $newDisc = $oldDisc;		
                $newQty = $this->input->post('qty');
                $pm = $this->input->post('op');
                
                if ($pm != "false"){
                    if($pm == 1){
                     echo $newQty = $newQty + 1;
                    }
                    if($pm == 0){
                      echo  $newQty = $newQty - 1;
                    }
                }
                
                
		$indTotal = ( $productVal->row()->price + $productVal->row()->product_shipping_cost + ($productVal->row()->price * 0.01 * $productVal->row()->product_tax_cost) ) * $newQty ;
		/*if($oldQty > 0){
                     $newDisc = 	($oldDisc / $oldQty) * $newQty;
                }	*/
		$dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal, 'discountAmount' => $newDisc);
		$condition =array('id' => $productVal->row()->id);
		$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);

		//echo number_format($indTotal,2,'.','').'|'.$this->data['CartVal'] = $this->cart_model->mani_cart_total($this->data['common_user_id']);
		echo number_format($indTotal,2,'.','').'|'.$this->data['CartVal'] = $this->cart_model->mani_cart_total($this->data['common_user_id']).'|'.$newQty;
		                
                return;
	}


		public function remove_product(){
                
		$excludeArr = array('id','qty','updval', 'op');

		$productVal = $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $this->data['common_user_id'],'product_id' => $this->input->post('product_id')));
		//var_dump($productVal->row()->id);exit;

                $oldQty = $productVal->row()->quantity;
                $oldDisc = $productVal->row()->discountAmount;
                $newDisc = $oldDisc;		
                $newQty = $this->input->post('qty');
                $pm = $this->input->post('op');
                
                if ($pm != "false"){
                    if($pm == 1){
                     echo $newQty = $newQty + 1;
                    }
                    if($pm == 0){
                      echo  $newQty = $newQty - 1;
                    }
                }
                
                
		$indTotal = ( $productVal->row()->price + $productVal->row()->product_shipping_cost + ($productVal->row()->price * 0.01 * $productVal->row()->product_tax_cost) ) * $newQty ;
		/*if($oldQty > 0){
                     $newDisc = 	($oldDisc / $oldQty) * $newQty;
                }	*/
		$dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal, 'discountAmount' => $newDisc);
		$condition =array('id' => $productVal->row()->id);
		$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);

		echo number_format($indTotal,2,'.','').'|'.$this->data['CartVal'] = $this->cart_model->mani_cart_total($this->data['common_user_id']).'|'.$newQty.'|'.$productVal->row()->id;
		                
                return;
	}


	public function ajaxDelete(){

		$delt_id = $this->input->post('curval');
		$CondID = $this->input->post('cart');
		if($CondID =='gift'){
			$this->db->delete(GIFTCARDS_TEMP, array('id' => $delt_id));
			echo $this->data['GiftVal'] = $this->cart_model->mani_gift_total($this->data['common_user_id']);
		}elseif($CondID =='subscribe'){
			$this->db->delete(FANCYYBOX_TEMP, array('id' => $delt_id));
			echo $this->data['SubscribeVal'] = $this->cart_model->mani_subcribe_total($this->data['common_user_id']);
		}elseif($CondID == 'cart'){
		$response = 	$this->db->delete(SHOPPING_CART, array('id' => $delt_id));
			echo $this->data['CartVal'] = $this->cart_model->mani_cart_total($this->data['common_user_id'],$this->input->post('code'));
		}
		return;
	}

		public function addon_delete_cart(){

		$product_id = $this->input->post('product_id');
		$user_id = $this->data['common_user_id'];
		//var_dump($product_id);exit;

		$response = 	$this->db->delete(SHOPPING_CART, array('product_id' => $product_id,'user_id' => $this->data['common_user_id']));
		echo $this->data['CartVal'] = $this->cart_model->mani_cart_total($this->data['common_user_id']);
		// var_dump($this->data['CartVal']);exit;
		return;
	}


	public function checkCode(){

		$Code = $this->input->post('code');
		$amount = $this->input->post('amount');
		$shipamount = $this->input->post('shipamount');

		echo $this->cart_model->Check_Code_Val($Code,$amount,$shipamount,$this->data['common_user_id']);

		return;

	}
	public function GiveDiscountOnCI_CC(){
		$si_setting = $this->cart_model->get_all_details(SI_DISCOUNT_SETTING,array('id ' => 1));
		if($si_setting->row()->active == 'Yes'){
	        $discount = $si_setting->row()->discount_amount;
			$this->cart_model->Discount_on_si_cc($this->data['common_user_id'],'add',$discount);
			echo  $this->cart_model->getCheckDiscount($this->data['common_user_id']);
		}else{
			echo "0";
		}
	}
	public function RemoveDiscountOnCI_CC(){
		// $si_setting = $this->cart_model->get_all_details(SI_DISCOUNT_SETTING,array('id ' => 1));
		// if($si_setting->row()->active == 'Yes'){
		$this->cart_model->Discount_on_si_cc($this->data['common_user_id'],'remove');
		echo  $this->cart_model->getCheckDiscount($this->data['common_user_id']);
		// }else{
		// 	echo "0";
		// }
	}
	public function checkCodeRemove(){

		$this->cart_model->Check_Code_Val_Remove($this->data['common_user_id'],$this->input->post('code'));
		echo $this->data['CartVal'] = $this->cart_model->mani_cart_coupon_sucess($this->data['common_user_id']);
		return;

	}
	public function checkCodeSuccess(){
		echo $this->data['CartVal'] = $this->cart_model->mani_cart_coupon_sucess($this->data['common_user_id']);
	}
	
	public function checkReferralCodeSuccess(){
		echo $this->data['CartVal'] = $this->cart_model->mani_cart_referralCode_sucess($this->data['common_user_id']);
	}


	public function ajaxChangeAddress(){

		if($this->input->post('add_id') != ''){
			$ChangeAdds =  $this->cart_model->get_all_details(SHIPPING_ADDRESS,array( 'user_id' => $this->data['common_user_id'],'id' => $this->input->post('add_id')));

			$ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST,array( 'country_code' => $ChangeAdds->row()->country));
			$MainShipCost = number_format($ShipCostVal->row()->shipping_cost,2,'.','');
			$MainTaxCost = number_format(($this->input->post('amt') * 0.01 * $ShipCostVal->row()->shipping_tax),2,'.','');
			$TotalAmts = number_format(( ($this->input->post('amt') + $MainShipCost + $MainTaxCost) - $this->input->post('disamt')),2,'.','');

			$condition = array('user_id' => $this->data['common_user_id']);
			$dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $ShipCostVal->row()->shipping_tax);
			//$this->cart_model->update_details(SHOPPING_CART,$dataArr2,$condition);

			echo $Chg_Adds = $MainShipCost.'|'.$MainTaxCost.'|'.$ShipCostVal->row()->shipping_tax.'|'.$TotalAmts.'|'.$ChangeAdds -> row() -> full_name.'<br>'.$ChangeAdds -> row() -> address1.'<br>'.$ChangeAdds -> row() -> city.' '.$ChangeAdds -> row() -> state.' '.$ChangeAdds -> row() -> postal_code.'<br>'.$ChangeAdds -> row() -> country.'<br>'.$ChangeAdds -> row() -> phone;

		}else{
			echo '0';
		}


	}
	public function ajaxSubscribeAddress(){

		if($this->input->post('add_id') != ''){
			$ChangeAdds =  $this->cart_model->get_all_details(SHIPPING_ADDRESS,array( 'user_id' => $this->data['common_user_id'],'id' => $this->input->post('add_id')));

			$ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST,array( 'country_code' => $ChangeAdds->row()->country));
			$MainShipCost = number_format($ShipCostVal->row()->shipping_cost,2,'.','');
			$MainTaxCost = number_format(($this->input->post('amt') * 0.01 * $ShipCostVal->row()->shipping_tax),2,'.','');
			$TotalAmts = number_format(($this->input->post('amt') + $MainShipCost + $MainTaxCost),2,'.','');


			echo $Chg_Adds = $MainShipCost.'|'.$MainTaxCost.'|'.$ShipCostVal->row()->shipping_tax.'|'.$TotalAmts.'|'.$ChangeAdds -> row() -> full_name.'<br>'.$ChangeAdds -> row() -> address1.'<br>'.$ChangeAdds -> row() -> city.' '.$ChangeAdds -> row() -> state.' '.$ChangeAdds -> row() -> postal_code.'<br>'.$ChangeAdds -> row() -> country.'<br>'.$ChangeAdds -> row() -> phone;


		}else{
			echo '0';
		}


	}

	public function ajaxDeleteAddress(){
		if ($this->checkLogin('U')==''){
			redirect('login');
		}else {
			$delID = $this->input->post('del_ID');
			$checkAddrCount = $this->cart_model->get_all_details(SHIPPING_ADDRESS,array('id' => $delID ,'primary'=>'Yes' ));

			if ($checkAddrCount->num_rows == 0){
				$this->cart_model->commonDelete(SHIPPING_ADDRESS,array('id' => $delID));
				echo '0';
			}else{
				echo '1';
			}
		}
	}

	public function insert_shipping_address() {
            
		if ($this->checkLogin('U')=='') {
                    redirect('login');
		}else {
                    $is_default = $this->input->post('set_default');
                    
                    if ($is_default == ''){
                            $primary = 'No';
                    }else{
                            $primary = 'Yes';
                    }
                    $checkAddrCount = $this->cart_model->get_all_details(SHIPPING_ADDRESS,array('user_id'=>$this->checkLogin('U')));
                    if ($checkAddrCount->num_rows == 0){
                            $primary = 'Yes';
                    }
                    $excludeArr = array('ship_id','set_default');
                    $dataArr = array('primary'=>$primary);

                    
                    $this->cart_model->commonInsertUpdate(SHIPPING_ADDRESS,'insert',$excludeArr,$dataArr,$condition);
                    $shipID = $this->cart_model->get_last_insert_id();
                    
                    if($this->lang->line('ship_add_success') != '')
                        $lg_err_msg = $this->lang->line('ship_add_success');
                    else 
                        $lg_err_msg = 'Shipping address added successfully';

                    $this->setErrorMessage('success',$lg_err_msg);

                    if ($primary == 'Yes'){
                            $condition = array('id !='=>$shipID,'user_id'=>$this->checkLogin('U'));
                            $dataArr = array('primary'=>'No');
                            $this->cart_model->update_details(SHIPPING_ADDRESS,$dataArr,$condition);
                    }
                    
                    // $address_total = $this->db->query(' select * from fc_shipping_address where user_id = '.$this->checkLogin('U').' ');
                    
                    //echo count($address_total->result());
                    echo $shipID;
                    //redirect('cart');
		}
	}
	
	
	public function cartcheckout(){ 
	   	$payment_type = $_POST['payment_type'];
    	$is_recurring = false;
    	

    	if(isset($_POST['is_si_selected'])){
    		$is_recurring = true;
    		$recurring_type = 'si_on_credit_card';
    	}else{
    	    $recurring_type = 'not_opted';	
    	}

// 		switch ($payment_type) {
// 		    case "cc_card":
// 		    	if($_POST['is_recurring']){
// 		    		$is_recurring = true;
// 		    		$recurring_type = 'si_on_credit_card';
// 		    	}
// 		        break;
// 		    case "dc_cards":
// 		        $recurring_type = 'not_opted';
// 		        break;
// 		    case "nb":
// 		         $recurring_type = 'not_opted';
// 		        break;
// 		    default:
// 		        $recurring_type = 'not_opted';
// 		}

		$Order_Setting = $this->cart_model->get_Order_Settings();
		if($Order_Setting->OrderRentalStatus){
			$cart_val  = $this->cart_model->Get_Cart_Value( $this->checkLogin('U'));
			 $cartAmt = 0;
			foreach ($cart_val->result() as $key => $CartRow) {
			  $cartAmt = $cartAmt + (($CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)) * $CartRow->quantity);
			}
			if($Order_Setting->RentalAmount > $cartAmt){
				$this->session->set_flashdata('order_error', 'Your minimum Monthly Rental amount should be Rs '.$Order_Setting->RentalAmount.'.Please add more products or addons to proceed with the order.');
				redirect("cart");
			}
		}
            if($this->input->post('Ship_address_val') !=''){
                $shipping_address = $this->cart_model->get_all_details(SHIPPING_ADDRESS,array('id'=>$this->input->post('Ship_address_val')));                        
                    $userid = $this->checkLogin('U');
                    if ($this->data['userDetails']->row()->postal_code == '' || $this->data['userDetails']->row()->postal_code == 0){
                            $this->cart_model->update_details(USERS,array('postal_code'=>$shipping_address->row()->postal_code),array('id'=>$userid));
                    }
                    if ($this->data['userDetails']->row()->address == ''){
                            $this->cart_model->update_details(USERS,array('address'=>$shipping_address->row()->address1),array('id'=>$userid));
                    }
                    if ($this->data['userDetails']->row()->address2 == ''){
                            $this->cart_model->update_details(USERS,array('address2'=>$shipping_address->row()->address2),array('id'=>$userid));
                    }
                    if ($this->data['userDetails']->row()->city == ''){
                            $this->cart_model->update_details(USERS,array('city'=>$shipping_address->row()->city),array('id'=>$userid));
                    }
                    if ($this->data['userDetails']->row()->state == ''){
                            $this->cart_model->update_details(USERS,array('state'=>$shipping_address->row()->state),array('id'=>$userid));
                    }
                    if ($this->data['userDetails']->row()->country == ''){
                            $this->cart_model->update_details(USERS,array('country'=>$shipping_address->row()->country),array('id'=>$userid));
                    }
                    if ($this->data['userDetails']->row()->phone_no == ''){
                            $this->cart_model->update_details(USERS,array('phone_no'=>$shipping_address->row()->phone),array('id'=>$userid));
                    }
                    $userid = $this->checkLogin('U');
                    $this->cart_model->addPaymentCart($userid, $is_recurring ,$recurring_type);
                    if($is_recurring){
                     	redirect("site/checkout/payupayment?p=si");
                 	}else{
                 		redirect("site/checkout/payupayment");
                 	}
            }else{
                    if($this->lang->line('add_ship_addr_msg') != '')
                            $lg_err_msg = $this->lang->line('add_ship_addr_msg');
                    else 
                            $lg_err_msg = 'Please Add the Shipping address';
                    $this->setErrorMessage('error',$lg_err_msg);
                    redirect("cart");
            }
	}



	public function Subscribecheckout(){
            if($this->input->post('SubShip_address_val') !=''){
                $shipping_address = $this->cart_model->get_all_details(SHIPPING_ADDRESS,array('id'=>$this->input->post('SubShip_address_val')));
                $userid = $this->checkLogin('U');
                if ($this->data['userDetails']->row()->postal_code == '' || $this->data['userDetails']->row()->postal_code == 0){
                        $this->cart_model->update_details(USERS,array('postal_code'=>$shipping_address->row()->postal_code),array('id'=>$userid));
                }
                if ($this->data['userDetails']->row()->address == ''){
                        $this->cart_model->update_details(USERS,array('address'=>$shipping_address->row()->address1),array('id'=>$userid));
                }
                if ($this->data['userDetails']->row()->address2 == ''){
                        $this->cart_model->update_details(USERS,array('address2'=>$shipping_address->row()->address2),array('id'=>$userid));
                }
                if ($this->data['userDetails']->row()->city == ''){
                        $this->cart_model->update_details(USERS,array('city'=>$shipping_address->row()->city),array('id'=>$userid));
                }
                if ($this->data['userDetails']->row()->state == ''){
                        $this->cart_model->update_details(USERS,array('state'=>$shipping_address->row()->state),array('id'=>$userid));
                }
                if ($this->data['userDetails']->row()->country == ''){
                        $this->cart_model->update_details(USERS,array('country'=>$shipping_address->row()->country),array('id'=>$userid));
                }
                if ($this->data['userDetails']->row()->phone_no == ''){
                        $this->cart_model->update_details(USERS,array('phone_no'=>$shipping_address->row()->phone),array('id'=>$userid));
                }
                $this->cart_model->addPaymentSubscribe($userid);
                redirect("checkout/subscribe");
            }else{
                if($this->lang->line('add_ship_addr_msg') != '')
                    $lg_err_msg = $this->lang->line('add_ship_addr_msg');
                else 
                    $lg_err_msg = 'Please Add the Shipping address';
                $this->setErrorMessage('error',$lg_err_msg);
            
                redirect("cart");
            }
	}

	public function getQty($pid, $val) {
            if ($pid > 0 && $this->checkLogin('U') != '' && $val > 0) {
                $Query = "select sum(quantity) as QTY from " . SHOPPING_CART . " where product_id='" . $pid . "' and user_id='" . $this->checkLogin('U') . "' and id!='" . $val . "'";
                $resultArr = $this->cart_model->ExecuteQuery($Query);
                if ($resultArr->num_rows() > 0) {
                    $qty = $resultArr->row()->QTY;
                    if ($qty == '') {
                        $qty = 0;
                    }
                } else {
                    $qty = 0;
                }
            } else {
                $qty = 0;
            }
            echo $qty;
    }

    public function getTenure() {
    	$pid = $this->input->post('pid');
    	$tenure = $this->cart_model->view_sub_product_details_join($pid);

    	foreach ($tenure->result() as $tv) {
    		$res = $tv->pid;
    		//print_r($tv);
    	}
    	echo $res;
    }

    public function add_to_cart()
    {
    	$datestring = date('Y-m-d H:i:s',now());
    	$product_id = $this->input->post('product_id');
    	$user_data =  $this->cart_model->get_price($product_id);
    	$user_id = $_SESSION['fc_session_user_id'];
    	$indtotal = $this->input->post('price') + $this->input->post('product_shipping_cost') + ($this->input->post('price') * 0.01 * $this->input->post('product_tax_cost'))  * 1;


    	$data = array('created'=>$datestring,'user_id'=>$user_id,'sell_id'=>$this->input->post('sell_id'),'product_id'=>$product_id,'price'=>$this->input->post('price'),'quantity'=>'1','discountAmount'=>'0.00','indtotal'=>$indtotal,'total'=>$indtotal,'cate_id'=>$this->input->post('cate_id'),'product_shipping_cost'=>$this->input->post('product_shipping_cost'),'attribute_values'=>$user_data->pid);

    	$response = $this->cart_model->simple_insert(SHOPPING_CART,$data);
    	if($response){
	        $new_data =  array('product_value'=>  $this->minicart_model->items_in_cart($this->data['common_user_id']),'message'=>'Product is added successfully');
			header('Content-Type: application/json');
	        echo json_encode($new_data);exit;
    	}
    }
    public function get_ship_value()
    {
    	$response = $this->cart_model->get_ship_value($this->input->post('id'));
    	header('Content-Type: application/json');
	     echo json_encode($response);exit;
    }
    public function get_coupon()
    {
    	$response = $this->cart_model->get_coupon_value($this->input->post('id'));
    	header('Content-Type: application/json');
	     echo json_encode($response);exit;
    }

    public function get_product_quantity(){
    	//var_dump("fffff");exit;
    	$result = $this->cart_model->get_product_quantity($this->data['common_user_id']);
    	header('Content-Type: application/json');
    	$result = array('result' => $result['result']  , 'count' => $result['count']);
    	//var_dump($result['count']);exit;
	     echo json_encode($result);exit;
	    
	}

	//place offline orders
	public function placeofflineOrder(){
		$returnStr['success'] = '0';
		$unameArr = $this->config->item('unameArr');
		$fullname = $this->input->post('user_name');
		$is_mobile_verified = 'No';
		if (in_array($fullname, $unameArr)){
			$returnStr['msg'] = 'User name already exists';
		}else {
			$email = $this->input->post('email');
			$pwd = 'cityfurnish'.mt_rand();
			if (filter_var($email)){
					$condition = array('email'=>$email);
					$duplicateMail = $this->cart_model->get_all_details(USERS,$condition);
					$mobile = $this->input->post('mobile_no');
					if($this->input->post('otp') != ''){
						$is_mobile_verified = 'Yes';
					}
					$loginUser = $this->cart_model->get_all_details(USERS,array('id' => $this->checkLogin('U')));
					$ReturnData = $this->cart_model->CheckOriginalPriceAfterCoupon($this->checkLogin('U'));
					$NewRental = ($ReturnData['rental_amount'] - $ReturnData['cartDisc']);
					if($this->input->post('rental_amount') > $NewRental){
					  	$returnStr['msg'] = 'Advance amount should be less than rental amount';
						$returnStr['success'] = '0';
						echo json_encode($returnStr);
						exit;
					}
					if($loginUser->num_rows() > 0){
						if($loginUser->row()->is_offline_user == 1){
							if ($duplicateMail->num_rows()>0){
								$user_id = $duplicateMail->row()->id;
							} else {
								$this->cart_model->insertUserQuick($fullname,$email,$pwd,$mobile,$is_mobile_verified);
								$user_id = $this->db->insert_id();
								$this->refferal_code($user_id);
								$userDetails = $this->cart_model->get_all_details(USERS,array('id' =>$user_id));
								$this->send_confirm_mail($userDetails,$pwd);
							}
							$address_array['full_name'] = $fullname;
							$address_array['address1'] =  $this->input->post('address1');
							$address_array['address2'] =  $this->input->post('address2');
							$address_array['phone_alternate'] =  $this->input->post('alert_mobile_no');
							$address_array['country'] =  'India';
							$address_array['state'] =  $this->input->post('state');
							$address_array['city'] =  $this->input->post('city');
							$address_array['phone'] =  $mobile;
							$address_array['postal_code'] = $this->input->post('postal_code');
							$address_array['user_id'] =  $user_id;
							$this->cart_model->simple_insert(SHIPPING_ADDRESS,$address_array);
							$shipping_address_id = $this->db->insert_id();
							$dealCodeNumber = $this->cart_model->addOfflinePaymentCart($this->checkLogin('U'),$user_id,$shipping_address_id);
							$condition3 = array('user_id' => $this->checkLogin('U'));
							$this->cart_model->commonDelete(SHOPPING_CART, $condition3);
							$this->cart_model->simple_insert(OFFLINE_ORDERS,array('user_id' => $this->checkLogin('U'),'dealCodeNumber' => $dealCodeNumber,'advance_rental' => $this->input->post('rental_amount') ));
							$this->sendPaymentLink($user_id,$email,$dealCodeNumber);
							$this->sendOfflineOrderMail($dealCodeNumber,$loginUser);
							$returnStr['msg'] = 'Successfully registered';
							$returnStr['success'] = '1';
						}else{
							$returnStr['msg'] = 'Invalid Access';
							$returnStr['success'] = '0';	
						}
					}else{
						$returnStr['msg'] = 'User not found';
						$returnStr['success'] = '0';
					}
			}else {
				$returnStr['msg'] = "Invalid email id";
			}
		}
		echo json_encode($returnStr);
	}
	public function send_confirm_mail($userDetails='',$pwd=''){
		$referral_code  = $this->cart_model->get_all_details(REFERRAL_CODE,array('user_id' => $userDetails->row()->id));
		$uid = $userDetails->row()->id;
		$email = $userDetails->row()->email;
		$randStr = $this->get_rand_str('10');
		$condition = array('id'=>$uid);
		$dataArr = array('verify_code'=>$randStr);
		$this->cart_model->update_details(USERS,$dataArr,$condition);
		
		$newsid = '3';
		$template_values=$this->cart_model->get_newsletter_template($newsid);
		$cfmurl = base_url().'site/user/confirm_register/'.$uid."/".$randStr."/confirmation";
		$subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
		$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);
		extract($adminnewstemplateArr);
		$referral_code = $referral_code->row()->referral_code;
		if($pwd != ''){
			$referral_code .= '<br><br>Your Email: '.$email.' <br><br>Your Password : '.$pwd;
		}
	    $searchArray = array("%cfmurl", "%cfreferralcode");
		$replaceArray = array($cfmurl, $referral_code);
		$new_message .=  str_replace($searchArray, $replaceArray ,$template_values['news_descrip'] );
		$message .= '<!DOCTYPE HTML>
			<html>
			<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			
			<meta name="viewport" content="width=device-width"/><body>';
		$message = $new_message;
		$message .= '</body>
			</html>';
		if($template_values['sender_name']=='' && $template_values['sender_email']==''){
			$sender_email=$this->data['siteContactMail'];
			$sender_name=$this->data['siteTitle'];
		}else{
			$sender_name=$template_values['sender_name'];
			$sender_email=$template_values['sender_email'];
		}
		$email_values = array('mail_type'=>'html',
							'from_mail_id'=>$sender_email,
							'mail_name'=>$sender_name,
							'to_mail_id'=>$email,
							'cc_mail_id'=> 'naresh.suthar@agileinfoways.com',
							'subject_message'=> $template_values['news_subject'],
							'body_messages'=>$message,
							'mail_id'=>'register mail'
							);
							$email_send_to_common = $this->product_model->common_email_send($email_values);
	}
	public function refferal_code($user_id){
		$condition = array('id'=>$user_id);
		$refferal_code = 'CF'.$user_id.rand(1111,9999);
		$condition = array('id ' => 1);
		$referral_setting = $this->cart_model->get_all_details(REFERRAL_SETTING,$condition);
		$expire_date = date('Y-m-d', strtotime(' +'.$referral_setting->row()->expire_days.' day'));
		if(!empty($referral_setting->row())){
			$data_array = array('user_id' => $user_id, 'referral_code' => $refferal_code,'count'=> $referral_setting->row()->quantity ,'cart_count'=> $referral_setting->row()->quantity , 'expire_date' =>  $expire_date);
			$respose = $this->cart_model->simple_insert(REFERRAL_CODE,$data_array);
		}         
	}
		public function sendPaymentLink($user_id,$email,$dealCodeNumber){
		$this->CreateZohoCase($user_id,$dealCodeNumber);
		$orderDetails = $this->cart_model->get_all_details(PAYMENT,array('dealCodeNumber' => $dealCodeNumber));
		if($orderDetails->num_rows() > 0){

		$rental_amount = $this->cart_model->get_all_details(OFFLINE_ORDERS,array('dealCodeNumber' => $dealCodeNumber));
			$advance_rental = $rental_amount->row();
			$TotalToPay = ($orderDetails->row()->total - $advance_rental->advance_rental);
			$link = '<a href="'.base_url().'offlinePayment/'.$orderDetails->row()->user_id.'/'.$dealCodeNumber.'">Pay Now</a>';
			$link_with_si = '<a href="'.base_url().'offlinePayment/'.$orderDetails->row()->user_id.'/'.$dealCodeNumber.'/si">Pay Now</a>';
			
			$si_discounts_row = $this->cart_model->get_all_details(SI_DISCOUNT_SETTING,array());
            $orderAmount = $si_discounts_row->row()->discount_amount;
            $voucherData = $this->cart_model->get_all_details('fc_voucher_settings',array('id !=' => ''));
            $siAmount  = $TotalToPay - $si_discounts_row->row()->discount_amount;

			$OrderDetails = $this->cart_model->getSellProductAndUserDetails($user_id,$dealCodeNumber);
			$shipAddRess = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('id' => $OrderDetails['PrdList']->row()->shippingid));
			$payment_link =   '<p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:5px;font-size:12px;color:#656565;">Advance Paid: Rs '. $advance_rental->advance_rental.'</p><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:5px;font-size:12px;color:#656565;">Please pay due amount of Rs '.$TotalToPay.' to confirm your order : '.$link.'</p><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:5px;font-size:12px;color:#656565;">Get additional discount of Rs '.$si_discounts_row->row()->discount_amount.' and Gift Voucher worth Rs '.$voucherData->row()->reward.'. Opt for standing instructions on your credit/debit card.</p><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:5px;font-size:12px;color:#656565;">Revised due amount Rs '.$siAmount.' : '.$link_with_si.'</p>';
		$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			  <head>
			  
			  <title>Order Confirmation</title>
			  <style type="text/css">
			@media screen and (max-width: 580px) {
			.tab-container {
			  max-width: 100%;
			}
			.txt-pad-15 {
			  padding: 0 15px 51px 15px !important;
			}
			.foo-txt {
			  padding: 0px 15px 18px 15px !important;
			}
			.foo-add {
			  padding: 15px 15px 0px 15px !important;
			}
			.foo-add-left {
			  width: 100% !important;
			}
			.foo-add-right {
			  width: 100% !important;
			}
			.pad-bottom-15 {
			  padding-bottom: 15px !important;
			}
			.pad-20 {
			  padding: 25px 20px 20px !important;
			}
			}
			</style>
			  </head>
			  <body style="margin:0px;">
			    <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="width: 850px;background-color: #fff;margin: 0 auto;table-layout: fixed;background:#dbdbdb;font-family:Arial, Helvetica, sans-serif;font-size:15px;"> <tbody>
			    <tr>
			      <td style="padding:20px;">

			      <table width="100%" border="0" cellpadding="0" cellspacing="0">
			          <tr>
			          <td style="text-align: left;width: 100%;padding-bottom:25px;background:#90c5bc;padding:20px;"><a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/email/logo.png" alt="logo"></a></td>
			        </tr>
			     </table>

			         <table width="100%" border="0" cellpadding="0" cellspacing="0">
			     
			          <tr>
			            <td style="text-align: left;width: 100%;background:#f5f5f5;padding:25px 30px 30px;">

			            <table width="100%" border="0" cellpadding="0" cellspacing="0">
			                <tr>
			                <td style="text-align: left;width: 100%;padding-bottom:10px;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-weight:bold;font-size:15px;color:#656565;">Hi '.strtoupper($shipAddRess->row()->full_name).'</p>
			                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:10px;font-size:15px;color:#656565;">Thank you for your order.</p></td><br>
			                    
			              </tr>
			            
			              </table> 
			             <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:10px;border-color:#ddd;">
			                <tr>
			                  <td style="text-align: left;padding:15px 20px;width:50%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;">
			                    '.$payment_link.'
			                  </td>
			                </tr>
			              </table>
			              
			              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
			                <tr>
			                  <td style="text-align: left;padding:15px 20px;width:50%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Id</strong><span style="font-weight:400;"> : #'.$OrderDetails['PrdList']->row()->dealCodeNumber.'</span> </p></td>
			                  <td style="text-align: left;padding:15px 20px;border-left:1px solid #ddd;width:50%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Order Date </strong><span style="font-weight:400;"> : '.date("F j, Y g:i a", strtotime($OrderDetails['PrdList']->row()->created)).'</span> </p></td>
			                </tr>
			              </table>
			       

			              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
			                <tr>
			                  <td style="text-align: left;padding:15px 20px;width:100%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Shipping Details</strong> </p></td>
			                </tr>
			                <tr>
			                  <td style="text-align: left;padding:15px 20px;border-top:1px solid #ddd;width:100%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Full Name</strong> <span style="font-weight:400;"> : '.$shipAddRess->row()->full_name.'</span> </p>
			                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"><strong style="font-weight:600;">Address</strong> <span style="font-weight:400;"> : '.$shipAddRess->row()->address1.'</span> </p>

			                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">City</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.$shipAddRess->row()->city.'</span> <strong style="font-weight:600;;">State</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> : '.$shipAddRess->row()->state.'</span> <strong style="font-weight:600;">Zip Code</strong> <span style="font-weight:400;display:inline-block;"> : '.$shipAddRess->row()->postal_code.'</span> </p>
			                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Phone Number</strong> <span style="font-weight:400;"> : '.$shipAddRess->row()->phone.'</span> </p></td>
			                </tr>
			              </table>

			 <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;color:#656565;border-color:#ddd;">
			   
			                <tr>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:100px;"> Product Images </td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;f\ont-weight:600;border-color:#ddd;width:120px;"> Product Name </td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:35px;">QTY</td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:120px;">Security Deposit</td>
                  <td align="center" style="color:#656565;padding:20px 5px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;width:110px;">Monthly Rental</td>
                  <td align="center" style="color:#656565;padding:20px 5px;font-size:12px;font-weight:600;width:120px;"> Sub Total </td>
                </tr>';


		       $message1 .= $message;
		        $disTotal = 0;
		        $grantTotal = 0;
		        
		        foreach ($OrderDetails['PrdList']->result() as $cartRow) {
		            $InvImg = @explode(',', $cartRow->image);
		            $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->price * $cartRow->quantity;
		            $unitDeposit = $cartRow->product_shipping_cost * $cartRow->quantity;

		            $uTot = $unitPrice + $unitDeposit;


		            if ($cartRow->attr_name != '' || $cartRow->attr_type != '') {
		                $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
		            } else {
		                $atr = '';
		            }
		            $message1 .= '<tr>

		            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid; font-size:11px;border-color:#ddd;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" />
		            </td>

		            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;line-height:20px;border-color:#ddd;">' . stripslashes($cartRow->product_name) .'<br/>'. $atr .' 
		            </td>

		            <td align="center" style="padding:20px 0px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . strtoupper($cartRow->quantity) . '
		            </td>

		            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitDeposit, 2, '.', '') . '
		            </td>

		            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;border-right-width:1px;border-right-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '
		            </td>
		            <td align="center" style="padding:20px 5px;border-top-width:1px;border-top-style:solid;font-size:11px;border-color:#ddd;">' . $this->data['currencySymbol'] .number_format($uTot, 2, '.', '')  . '</td>
		        </tr>';
		            $grantTotal = $grantTotal + $uTot;
		            $grandDeposit = $grandDeposit + $unitDeposit;
		            $total_deposite  += $unitDeposit;
		            $total_rental +=  $unitPrice;
		            $total_total += $uTot;
		        }
		        $private_total = $grantTotal - $OrderDetails['PrdList']->row()->discountAmount;
		        $private_total = $private_total + $OrderDetails['PrdList']->row()->tax;
		        if($OrderDetails['PrdList']->row()->discount_on_si != ''){
		            $private_total = $private_total - $OrderDetails['PrdList']->row()->discount_on_si;
		        }

		    

		        $message1 .= '<tr>
		                  <td align="right" colspan="3" style="padding:20px 20px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:12px;font-weight:600;border-color:#ddd;"> TOTAL </td>
		                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] .  number_format($total_deposite, 2, '.', '') . ' </td>
		                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd; border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;"> ' . $this->data['currencySymbol'] . number_format($total_rental, 2, '.', '') . ' </td>
		                  <td align="center" style="color:#60a196; padding:20px 5px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($total_total, 2, '.', '') . '</td>
		            </tr>
		        </table>';

		        if ($OrderDetails['PrdList']->row()->note != '') {
		            $message1 .= '<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
		                <td width="87" ><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>
		               
		            </tr>
					<tr>
		                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($OrderDetails['PrdList']->row()->note) . '</span></td>
		            </tr></table>';
		        }


		        $message1 .= '<table border="0" align="right" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;color:#656565;border-color:#ddd;">
		            <tr>
		                <td align="left" style="color:#656565;padding:14px 20px 15px 20px;border-right:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;padding-left:20px;"> Sub Total </td>

		                <td><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</span></td>
		            </tr>';
		        if ($OrderDetails['PrdList']->row()->couponCode != '') {
		            $message1 .= '<tr>
		                <td align="left" style="color:#656565;padding:10px 20px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-weight:600;border-color:#ddd;padding-left:20px;"><p style="margin:0px;font-size:11px;">Discount <br />
		                      <span style="font-size:9px !important;font-weight:400;">(Coupon Code : '. $OrderDetails['PrdList']->row()->couponCode .')</span></p></td>
		                  <td align="center" style="color:#656565;padding:15px 20px 12px;border-top:1px solid #ddd;font-size:11px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . ' '.number_format($OrderDetails['PrdList']->row()->discountAmount, '2', '.', '') . '</td>
		            </tr>';
		        }
		        
		        
		        $message1 .= '<tr>
		                  <td align="left" style="color:#656565;padding:14px 15px 15px 20px;border-top:1px solid #ddd;border-right:1px solid #ddd;font-size:14px;font-weight:600;border-color:#ddd;padding-left:20px;">GRAND TOTAL</td>
		                  <td align="center" style="color:#60a196;padding:15px 20px;border-top:1px solid #ddd;font-size:15px;font-weight:600;border-color:#ddd;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</td>
		                </tr>
		            </tr>
		        </table>
		           </td>
		         </tr>
		          <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
		            <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
		              <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
		                Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
		          </tr>
		  </table>
		  </td>
		  </tr>
		  </table>
		</body>
		</html>';
		
			    $sender_email=$this->data['siteContactMail'];
				$sender_name=$this->data['siteTitle'];

		        $email_values = array('mail_type' => 'html',
		            'from_mail_id' => $sender_email,
		            'mail_name' => $sender_name,
		            'to_mail_id' => $email,
		            'subject_message' => 'Order Confirmation',
		            'body_messages' => $message1,
		            'mail_id' => 'Forgot Password Mail',
		        );
		        $email = $this->cart_model->common_email_send($email_values);
	    }
	}
	function CreateZohoCase($userID,$dealCode){
		$currentSuccessOrders = $this->cart_model->getCurrentSuccessOrdersOffline($userID,$dealCode);
		$checkContactsURL = 'https://www.zohoapis.com/crm/v2/Contacts/search?criteria=(Email:equals:'.$currentSuccessOrders[0]['email'].')';
// 		$headers = [
// 			'Contentk-Type:application/json',
//     		'Authorization:'.CRM_AUTH_TOKEN
//     	];
        $acceess_token = $this->get_zohaccess_token();
	    $headers = [
   		    'Content-Type:application/json',
            'Authorization:'.$acceess_token
        ];
		$curl = curl_init($checkContactsURL);
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => $headers
		));
		$contactsExists = curl_exec($curl);
		$contactsExists_array = json_decode($contactsExists);		
		$accountName = $currentSuccessOrders[0]['full_name'];
		    
		if(array_key_exists("data",$contactsExists_array)){
				$contactID = $contactsExists_array->data[0]->id;
				$accountName = $contactsExists_array->data[0]->Account_Name->name;
		}
		else{
			$checkAccountsURL = 'https://www.zohoapis.com/crm/v2/Accounts/search?criteria=(Email:equals:'.$currentSuccessOrders[0]['email'].')';
		    $curl = curl_init($checkAccountsURL);
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HTTPHEADER => $headers
			));
			$accountsExists = curl_exec($curl);
		    $accountsExists_array = json_decode($accountsExists);
			$accountID = "";
			$existingEmail="";
			if(array_key_exists("data",$accountsExists_array)){
		  		$existingEmail = $accountsExists_array->data[0]->Email;
		  		if($currentSuccessOrders[0]['email']!=$existingEmail)
		  		{
		           	$accountName = $currentSuccessOrders[0]['full_name'] . " - " . $currentSuccessOrders[0]['email'];
		           	$checkAccounts2URL = 'https://www.zohoapis.com/crm/v2/Accounts/search?criteria=(accountname:equals:'.$accountName.')';
	           	  	$curl = curl_init($checkAccountsURL);
					curl_setopt_array($curl, array(
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_HTTPHEADER => $headers
					));
					$accounts2Exists = curl_exec($curl);
		    		$accounts2Exists_array = json_decode($accounts2Exists);
		           	if(array_key_exists("data",$accounts2Exists_array)){
						$accountID = $accounts2Exists_array->data[0]->id;
		           	}else{
		           		$accountID = "";
		           	}
			    }   
			}
		
			
			if($accountID=="")
			{
			      	$createAccountURL = 'https://www.zohoapis.com/crm/v2/Accounts';	
			        $AccountJsonData =   '{
			            "data" : [
						       	{
						       		"Account_Name":"'.$accountName.'",
						       		"Account_Owner":"'.CASE_OWNER.'",
						       		"Email":"'.$currentSuccessOrders[0]['email'].'",
						       		"Phone":"'.$currentSuccessOrders[0]['ship_phone'].'",
						       		"Billing_Street":"'.$currentSuccessOrders[0]['ship_address1'].'",
						       		"Billing_Street2":"'.$currentSuccessOrders[0]['ship_address2'].'",
						       		"Billing_City":"'.$currentSuccessOrders[0]['ship_city'].'",
						       		"Billing_State":'.$currentSuccessOrders[0]['ship_state'].',
						       		"Billing_Code":"'.$currentSuccessOrders[0]['ship_postal_code'].'",
						       		"Billing_Country":"'.$currentSuccessOrders[0]['ship_country'].'",
						       		"Shipping_Street":"'.$currentSuccessOrders[0]['ship_address1'].'",
						       		"Shipping_Street2":"'.$currentSuccessOrders[0]['ship_address2'].'",
						       		"Shipping_City":"'.$currentSuccessOrders[0]['ship_city'].'",
						       		"Shipping_State": "'.$currentSuccessOrders[0]['ship_state'].'",
						       		"Shipping_Code":"'.$currentSuccessOrders[0]['ship_postal_code'].'",
						       		"Shipping_Country":"'.$currentSuccessOrders[0]['ship_country'].'"
						        }
						    ]
						}';
			      
					    $curl = curl_init($createAccountURL);
							curl_setopt_array($curl, array(
								CURLOPT_POST => 1,
				            	CURLOPT_HTTPHEADER => $headers,
				            	CURLOPT_POSTFIELDS => $AccountJsonData,
				            	CURLOPT_RETURNTRANSFER => true
							));
						$createAccountResult = curl_exec($curl);
				    	$createAccountResultArray = json_decode($createAccountResult);
			      
			      if(array_key_exists("data",$createAccountResultArray))
			      {
			          $accountID = $createAccountResultArray->data[0]->details->id;
			      }
			      else
			      {
			          $accountID = "";
			      }
			}
			// create a contact
			$fullNameArr = explode(" ",$currentSuccessOrders[0]['full_name']);
			if(count($fullNameArr)==3)
			{
				$lastName = $fullNameArr[2];
				$firstName = $fullNameArr[0] . " " . $fullNameArr[1];
			}
			else if(count($fullNameArr)==2)
			{
				$lastName = $fullNameArr[1];
				$firstName = $fullNameArr[0];
			}
			else
			{
				$lastName = $currentSuccessOrders[0]['full_name'];
				$firstName = "";
			}
			$createContactURL = 'https://www.zohoapis.com/crm/v2/Contacts';	
			        $ContactJsonData =   '{
			            "data" : [
						       	{
						       		"First_Name":"Mr'.$firstName.'",
						       		"Last_Name":"'.$lastName.'",
						       		"Contact_Owner":"'.CASE_OWNER.'",
						       		"Account_Name":"'.$accountName.'",
						       		"Mobile":"'.$currentSuccessOrders[0]['ship_phone'].'",
						       		"Email":"'.$currentSuccessOrders[0]['email'].'",
						       		"Mailing_Street":"'.$currentSuccessOrders[0]['ship_address1'].'",
						       		"Mailing_Street2":'.$currentSuccessOrders[0]['ship_address2'].',
						       		"Mailing_City":"'.$currentSuccessOrders[0]['ship_city'].'",
						       		"Mailing_State":"'.$currentSuccessOrders[0]['ship_state'].'",
						       		"Mailing_Zip":"'.$currentSuccessOrders[0]['ship_postal_code'].'",
						       		"Mailing_Country":"'.$currentSuccessOrders[0]['ship_country'].'"
						        }
						    ]
						}';
			      
					    $curl = curl_init($createContactURL);
							curl_setopt_array($curl, array(
								CURLOPT_POST => 1,
				            	CURLOPT_HTTPHEADER => $headers,
				            	CURLOPT_POSTFIELDS => $ContactJsonData,
				            	CURLOPT_RETURNTRANSFER => true
							));
						$jsonContactRes = curl_exec($curl);
				    	$createContactResultArray = json_decode($jsonContactRes);
			
			if(array_key_exists("data",$createContactResultArray))
			{
			     $contactID = $createContactResultArray->data[0]->details->id;
			}
			else
			{
			    $contactID = "";
			}
		}
		$deposit = $currentSuccessOrders[0]['shippingcost'];
		$monthlyRent = ($currentSuccessOrders[0]['total'] - $currentSuccessOrders[0]['shippingcost']);
		//$tenure = $currentSuccessOrders[0]['attr_name'];
		$fullName = $currentSuccessOrders[0]['ship_full_name'];
		$address1 = $currentSuccessOrders[0]['ship_address1'];
		$address2 = $currentSuccessOrders[0]['ship_address2'];
		$city = $currentSuccessOrders[0]['ship_city'];
		$country = $currentSuccessOrders[0]['ship_country'];
		$state = $currentSuccessOrders[0]['ship_state'];
		$zipCode = $currentSuccessOrders[0]['ship_postal_code'];
		$phoneNumber = $currentSuccessOrders[0]['ship_phone'];
		$couponcode = $currentSuccessOrders[0]['couponCode'] ? $currentSuccessOrders[0]['couponCode'] : '';
		$coupon_code_details  = $this->cart_model->get_all_details(COUPONCARDS,array('id' => $currentSuccessOrders[0]['coupon_id']));
    	$monthDescription = '';
        if($coupon_code_details->row()->recurring_invoice_tenure > 0 ){
        	$ongoingAmount = $monthlyRent + ($currentSuccessOrders[0]['discountAmount'] +  $currentSuccessOrders[0]['discount_on_si']);
        	for($i = 1;$i <= $coupon_code_details->row()->recurring_invoice_tenure; $i++){
        		$month = '';
        		if($i == 1){
        			$month = $i.'st';
        		}else if($i == 2){
        			$month = $i.'nd';
        		}else if($i == 3){
        			$month = $i.'rd';
        		}else{
        			$month = $i.'th';
        		}
        		$monthDescription .=  $month.' Month Rent: Rs '. $monthlyRent.'\n';
        	}
        }else{
        	$ongoingAmount = $monthlyRent;
        }
		
		$OrderOwnerDetails  = '';
        $userDetails  = $this->order_model->getOfflineOrderOwnerDetails($dealCode);
        if($userDetails->num_rows() > 0){
        	$userData = $userDetails->row();
        	$OrderOwnerDetails = '\n\n OFFLINE ORDER \n\n Advance Payment:'.$userData->advance_rental.' \n Order Owner Name: '.$currentSuccessOrders[0]['ship_full_name'].
        	'\n Order Owner Email: '.$currentSuccessOrders[0]['email'].
        	'\n Transaction ID: '.$currentSuccessOrders[0]['dealCodeNumber'].
        	'\n Mode: website'.
        	'\n Order Date: '. date('Y-m-d h:i:s A', strtotime($currentSuccessOrders[0]['modified']));
        }
        // print_r($OrderOwnerDetails);exit;
        $description ='\n Total Amount: Rs. '.($deposit + $monthlyRent).
        '\n Deposit: Rs. '.$deposit.
        '\n Discount: '.($currentSuccessOrders[0]['discountAmount'] - $currentSuccessOrders[0]['discount_on_si']).
        '\n Coupon Used: '.$couponcode.'\n\n'.
        $monthDescription.
        ' Rent For The Rest Of Mnths: Rs. '.$ongoingAmount .
        '\n Tenure: '.$currentSuccessOrders[0]['attr_name'].'\n'.
        '\n Product:\n'.$products.
        '\n Full Name: '.$fullName.
        '\n Address: '.$address1.
        '\n Address2: '.$address2.
        '\n City: '.$city.
        '\n Country: '.$country.
        '\n State: '.$state.
        '\n Zip Code: '.$zipCode.
        '\n Phone Number: '.$phoneNumber.
        '\n '.$OrderOwnerDetails;
        // print_r($description);exit;
		$city = $currentSuccessOrders[0]['city'];
		$relatedTo = $contactID;
		$caseOwner = CASE_OWNER;
		$createCaseURL = 'https://www.zohoapis.com/crm/v2/Cases';	
        $CreateCaseJsonData =   '{
        "data" : [
		       	{
	       		"Case_Origin":"Offline",
	       		"Subject":"New Offline Order - '.$currentSuccessOrders[0]['ship_full_name'].' ('.$currentSuccessOrders[0]['email'].')",
	       		"Status":"New Order",
	       		"Owner":"649600348",
	       		"Order_ID":"'.$currentSuccessOrders[0]['dealCodeNumber'].'",
	       		"Type":"Order",
	       		"Sub_Type":"Offline - Rental",
	       		"Coupon_Code":"'.$couponcode.'",
	       		"Related_To":"'.$relatedTo.'",
	       		"Description":"'.$description.'",
	       		"City":"'.$currentSuccessOrders[0]['ship_city'].'",
	       		"Account_Name" :"'.$accountName.'",
	       		"LOB":"B2C",
	       		"Email":"'.$currentSuccessOrders[0]['email'].'",
	       		"Phone":"'.$currentSuccessOrders[0]['ship_phone'].'",
	       		"Offline_User_Name" : "'.$userData->full_name.'",
	       		"Offline_User_Email": "'.$userData->email.'"
		        }
		    ]
		}';
	    $curl = curl_init($createCaseURL);
			curl_setopt_array($curl, array(
				CURLOPT_POST => 1,
            	CURLOPT_HTTPHEADER => $headers,
            	CURLOPT_POSTFIELDS => $CreateCaseJsonData,
            	CURLOPT_RETURNTRANSFER => true
			));
		$jsonCaseRes = curl_exec($curl);
    	$createCaseResultArray = json_decode($jsonCaseRes);
    	// print_r($createCaseResultArray);exit;
	}
	public function sendOfflineOrderMail($dealCodeNumber,$loginUser){
		$orderDetails = $this->cart_model->get_all_details(PAYMENT,array('dealCodeNumber' => $dealCodeNumber));
		if($orderDetails->num_rows() > 0){
			$user_details =  $this->cart_model->get_all_details(USERS,array('id' => $orderDetails->row()->user_id));
			$user = $user_details->row();
			$login_user = $loginUser->row();
			$link = '<a href="'.base_url().'admin/order/view_order/'.$orderDetails->row()->user_id.'/'.$orderDetails->row()->dealCodeNumber.'">Click Here</a>';
			$message .= '<!DOCTYPE HTML>
			<html>
			<head>
			
			<meta name="viewport" content="width=device-width"/><body>';
			$message .= "<p>New offline order placed</p><br>
			<p>Order ID : #$dealCodeNumber </p><br>
			<p>View Order : $link</p><br>
			<strong> CUSTOMER </strong>
			<p>Customer Name : $user->full_name</p>
			<p>Customer Email : $user->email</p><br>
			<strong> OFFLINE USER </strong>
			<p>Offline User Name : $login_user->full_name</p>
			<p>Offline User Email : $login_user->email</p>";
			$message .= '</body>
				</html>';
			$sender_email=$this->data['siteContactMail'];
			$sender_name=$this->data['siteTitle'];
			$email_values = array(
				'mail_type'=>'html',
				'from_mail_id'=>$sender_email,
				'mail_name'=>$sender_name,
				'to_mail_id'=> 'hello@cityfurnish.com',
				// 'cc_mail_id'=> 'naresh.suthar@agileinfoways.com,nareshsuthar868@gmail.com',
				'subject_message'=> 'Offline Order Notification',
				'body_messages'=>$message,
				'mail_id'=>'register mail'
			);
			$email_send_to_common = $this->product_model->common_email_send($email_values);
		}
	}
}
/* End of file user.php */
/* Location: ./application/controllers/site/user.php */