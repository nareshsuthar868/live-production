<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */
class Product extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'email', 'text', 'string'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model(array('product_model', 'user_model'));
        $this->load->model('cart_model');

        if ($_SESSION['sMainCategories']) {
            $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
            $sortArr = array($sortArr1);
            $_SESSION['sMainCategories'] = $this->product_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
        }
        $this->data['mainCategories'] = $_SESSION['sMainCategories'];

        // if ($_SESSION['sColorLists'] == '') {
            $_SESSION['sColorLists'] = $this->product_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
        // }
        $this->data['mainColorLists'] = $_SESSION['sColorLists'];

        $this->data['loginCheck'] = $this->checkLogin('U');
        $this->data['likedProducts'] = array();
        if ($this->data['loginCheck'] != '') {
            $this->data['likedProducts'] = $this->product_model->get_all_details(PRODUCT_LIKES, array('user_id' => $this->checkLogin('U')));
        }
    }

    public function onboarding() {
        if ($this->checkLogin('U') == '') {
            redirect(base_url());
        } else {
            $this->data['userDetails'] = $this->product_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));

            if ($this->data['userDetails']->num_rows() == 1) {
                if ($this->data['mainCategories']->num_rows() > 0) {

                    foreach ($this->data['mainCategories']->result() as $cat) {
                        //$condition = " where p.category_id like '".$cat->id.",%' OR p.category_id like '%,".$cat->id."' OR p.category_id like '%,".$cat->id.",%' OR p.category_id='".$cat->id."' order by p.created desc";
                        /* 	$condition = " where FIND_IN_SET('".$cat->id."',p.category_id) and p.quantity>0 and p.status='Publish' and u.group='Seller' and u.status='Active' or p.status='Publish' and p.quantity > 0 and p.user_id=0 and FIND_IN_SET('".$cat->id."',p.category_id) order by p.created desc limit 4";
                          $this->data['productDetails'][$cat->cat_name] = $this->product_model->view_product_details($condition);

                          $nextlimit = $this->data['productDetails'][$cat->cat_name]->num_rows();
                          if($nextlimit == 4){
                          $usercondition = " where FIND_IN_SET('".$cat->id."',p.category_id) and p.status='Publish' and p.global_visible=1 and u.status='Active' or p.status='Publish' and FIND_IN_SET('".$cat->id."',p.category_id) and p.global_visible=1 order by p.created desc limit 0";
                          $this->data['userproductDetails'][$cat->cat_name] = $this->product_model->view_product_details_user($usercondition);

                          }else{
                          $four = 4;
                          $nextlimit = $four - $nextlimit;
                          $usercondition = " where FIND_IN_SET('".$cat->id."',p.category_id) and p.status='Publish' and p.global_visible=1 and u.status='Active' or p.status='Publish' and FIND_IN_SET('".$cat->id."',p.category_id) and p.global_visible=1 order by p.created desc limit $nextlimit"; */
                        $usercondition = " where FIND_IN_SET('" . $cat->id . "',p.category_id) and p.status='Publish' and p.global_visible=1 and u.status='Active'limit 4";
                        $this->data['userproductDetails'][$cat->cat_name] = $this->product_model->view_product_details_user($usercondition);

                        $param = htmlspecialchars($_GET["next"]);
                        $next = '';
                        if ($param != '') {
                            $next = $param;
                        } else {
                            $next = base_url();
                        }
                        $this->data['next'] = $next;
                        //}
                    }
                }
                $this->load->view('site/user/onboarding', $this->data);
            } else {
                redirect(base_url());
            }
        }
    }

    public function onboarding_get_products_categories() {
        $returnCnt = '<div id="onboarding-category-items"><ol class="stream vertical">';
        $left = $top = $count = 0;
        $width = 220;
        $productArr = array();
        $userFallowCats = $this->input->get('categories');
        $this->data['userDetails'] = $this->product_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));
        if ($this->data['userDetails']->num_rows() == 1) {
            $condition = array('id' => $this->data['userDetails']->row()->id);
            $newdata = array(
                'follow_category' => $userFallowCats
            );
            $this->product_model->update_details(USERS, $newdata, $condition);
        } else {
            redirect(base_url());
        }
        $catID = explode(',', $this->input->get('categories'));
        if (count($catID) > 0) {
            foreach ($catID as $cat) {
                //				$condition = " where p.category_id like '".$cat.",%' AND p.status = 'Publish' OR p.category_id like '%,".$cat."' AND p.status = 'Publish' OR p.category_id like '%,".$cat.",%' AND p.status = 'Publish' OR p.category_id='".$cat."' AND p.status = 'Publish'";
                $condition = " where FIND_IN_SET('" . $cat . "',p.category_id) and p.quantity>0 and p.status='Publish' and u.group='Seller' and u.status='Active' or p.status='Publish' and p.quantity > 0 and p.user_id=0 and FIND_IN_SET('" . $cat . "',p.category_id) order by p.created desc limit 10";
                $productDetails = $this->product_model->view_product_details($condition);
                $usercondition = " where FIND_IN_SET('" . $cat . "',p.category_id) and p.status='Publish' and p.global_visible=1 and u.status='Active' order by p.created desc limit 10";
                $userproductDetails = $this->product_model->view_product_details_user($usercondition);
                if ($productDetails->num_rows() > 0) {
                    foreach ($productDetails->result() as $productRow) {
                        if (!in_array($productRow->id, $productArr)) {
                            array_push($productArr, $productRow->id);
                            $img = '';
                            $imgArr = explode(',', $productRow->image);
                            if (count($imgArr) > 0) {
                                foreach ($imgArr as $imgRow) {
                                    if ($imgRow != '') {
                                        $img = $imgRow;
                                        break;
                                    }
                                }
                            }
                            if ($img != '') {
                                $count++;
                                $leftPos = $count % 3;
                                $leftPos = ($leftPos == 0) ? 3 : $leftPos;
                                $leftPos--;
                                if ($count % 3 == 0) {
                                    $topPos = $count / 3;
                                } else {
                                    $topPos = ceil($count / 3);
                                }
                                $topPos--;
                                $leftVal = $leftPos * $width;
                                $topVal = $topPos * $width;
                                $returnCnt .= '
									<li style="opacity: 1; top: ' . $topVal . 'px; left: ' . $leftVal . 'px;" class="start_marker_"><span class="pre hide"></span>
										<div class="figure-item">
											<a class="figure-img">
												<span style="background-image:url(\'' . base_url() . 'images/product/' . $img . '\')" class="figure">
													<em class="back"></em>
													<img height="200" data-height="640" data-width="640" src="' . base_url() . 'images/product/' . $img . '"/>
												</span>
											</a>
											<a tid="' . $productRow->seller_product_id . '" class="button fancy noedit" href="#"><span><i></i></span>' . LIKE_BUTTON . '</a>
										</div>
									</li>
								';
                            }
                        }
                    }
                }
                if ($userproductDetails->num_rows() > 0) {
                    foreach ($userproductDetails->result() as $productRow) {
                        if (!in_array($productRow->id, $productArr)) {
                            array_push($productArr, $productRow->id);
                            $img = '';
                            $imgArr = explode(',', $productRow->image);
                            if (count($imgArr) > 0) {
                                foreach ($imgArr as $imgRow) {
                                    if ($imgRow != '') {
                                        $img = $imgRow;
                                        break;
                                    }
                                }
                            }
                            if ($img != '') {
                                $count++;
                                $leftPos = $count % 3;
                                $leftPos = ($leftPos == 0) ? 3 : $leftPos;
                                $leftPos--;
                                if ($count % 3 == 0) {
                                    $topPos = $count / 3;
                                } else {
                                    $topPos = ceil($count / 3);
                                }
                                $topPos--;
                                $leftVal = $leftPos * $width;
                                $topVal = $topPos * $width;
                                $returnCnt .= '
									<li style="opacity: 1; top: ' . $topVal . 'px; left: ' . $leftVal . 'px;" class="start_marker_"><span class="pre hide"></span>
										<div class="figure-item">
											<a class="figure-img">
												<span style="background-image:url(\'' . base_url() . 'images/product/' . $img . '\')" class="figure">
													<em class="back"></em>
													<img height="200" data-height="640" data-width="640" src="' . base_url() . 'images/product/' . $img . '"/>
												</span>
											</a>
											<a tid="' . $productRow->seller_product_id . '" class="button fancy noedit" href="#"><span><i></i></span>' . LIKE_BUTTON . '</a>
										</div>
									</li>
								';
                            }
                        }
                    }
                }
            }
        }
        $returnCnt .= '
			</div>
		';
        echo $returnCnt;
    }

    public function onboarding_get_users_follow() {
        $catID = explode(',', $this->input->get('categories'));
        $productArr = array();
        $userArr = array();
        $userCountArr = array();
        $returnArr = array();

        /*         * **********Get Suggested Users List***************************** */

        $returnArr['suggested'] = '<ul class="suggest-list">';
        if (count($catID) > 0) {
            foreach ($catID as $cat) {
                //				$condition = " where p.category_id like '".$cat.",%' AND p.status = 'Publish' OR p.category_id like '%,".$cat."' AND p.status = 'Publish' OR p.category_id like '%,".$cat.",%' AND p.status = 'Publish' OR p.category_id='".$cat."' AND p.status = 'Publish'";
                $condition = " where FIND_IN_SET('" . $cat . "',p.category_id) and p.quantity>0 and p.status='Publish' and u.group='Seller' and u.status='Active' or p.status='Publish' and p.quantity > 0 and p.user_id=0 and FIND_IN_SET('" . $cat . "',p.category_id)";
                $productDetails = $this->product_model->view_product_details($condition);
                $usercondition = " where FIND_IN_SET('" . $cat . "',p.category_id) and p.status='Publish' and p.global_visible=1 and u.status='Active' ";
                $userproductDetails = $this->product_model->view_product_details_user($usercondition);
                if ($productDetails->num_rows() > 0) {
                    foreach ($productDetails->result() as $productRow) {
                        if (!in_array($productRow->id, $productArr)) {
                            array_push($productArr, $productRow->id);
                            if ($productRow->user_id != '') {
                                if (!in_array($productRow->user_id, $userArr)) {
                                    array_push($userArr, $productRow->user_id);
                                    $userCountArr[$productRow->user_id] = 1;
                                } else {
                                    $userCountArr[$productRow->user_id] ++;
                                }
                            }
                        }
                    }
                }
                if ($userproductDetails->num_rows() > 0) {
                    foreach ($userproductDetails->result() as $productRow) {
                        if (!in_array($productRow->id, $productArr)) {
                            array_push($productArr, $productRow->id);
                            if ($productRow->user_id != '') {
                                if (!in_array($productRow->user_id, $userArr)) {
                                    array_push($userArr, $productRow->user_id);
                                    $userCountArr[$productRow->user_id] = 1;
                                } else {
                                    $userCountArr[$productRow->user_id] ++;
                                }
                            }
                        }
                    }
                }
            }
        }
        arsort($userCountArr);
        $limitCount = 0;
        foreach ($userCountArr as $user_id => $products) {
            if ($limitCount > 5) {
                break;
            }
            if ($user_id != '') {
                //$condition = array('id'=>$user_id,'is_verified'=>'Yes','status'=>'Active');
                $condition = array('id' => $user_id, 'status' => 'Active');

                $userDetails = $this->product_model->get_all_details(USERS, $condition);
                if ($userDetails->num_rows() == 1) {
                    $condition = array('user_id' => $user_id, 'status' => 'Publish');
                    $userProductDetails = $this->product_model->get_all_details(PRODUCT, $condition);
                    $total_seller_product = $userProductDetails->num_rows();
                    $affiProductDetails = $this->product_model->get_all_details(USER_PRODUCTS, $condition);
                    $total_user_product = $affiProductDetails->num_rows();
                    $total_things = $total_seller_product + $total_user_product;
                    if ($limitCount < 5) {
                        $userImg = $userDetails->row()->thumbnail;
                        if ($userImg == '') {
                            $userImg = 'user-thumb1.png';
                        }
                        $returnArr['suggested'] .= '
							<li><span class="vcard"><img src="' . base_url() . 'images/users/' . $userImg . '"></span>
							<b>' . $userDetails->row()->full_name . '</b><br>
							' . $userDetails->row()->followers_count . ' followers<br>
							' . $total_things . ' things<br>
							<a uid="' . $user_id . '" class="follow-user-link" href="javascript:void(0)">Follow</a>
							<span class="category-thum">';
                        $plimit = 0;
                        if ($userProductDetails->num_rows() > 0) {
                            foreach ($userProductDetails->result() as $userProduct) {
                                if ($plimit > 3) {
                                    break;
                                }
                                $img = '';
                                $imgArr = explode(',', $userProduct->image);
                                if (count($imgArr) > 0) {
                                    foreach ($imgArr as $imgRow) {
                                        if ($imgRow != '') {
                                            $img = $imgRow;
                                            break;
                                        }
                                    }
                                }
                                if ($img != '') {

                                    $returnArr['suggested'] .= '<img alt="' . $userProduct->product_name . '" src="' . base_url() . 'images/product/' . $img . '">';
                                    $plimit++;
                                }
                            }
                        }
                        if ($plimit == 4) {
                            $plimit = 4;
                        } else {
                            if ($plimit == 0) {
                                $plimit = 0;
                            } else {
                                if ($plimit == 1) {
                                    $plimit = 1;
                                } else {
                                    if ($plimit == 2) {
                                        $plimit = 2;
                                    } else {
                                        if ($plimit == 3) {
                                            $plimit = 3;
                                        }
                                    }
                                }
                            }
                        }
                        if ($affiProductDetails->num_rows() > 0) {
                            foreach ($affiProductDetails->result() as $userProduct) {
                                if ($plimit > 3) {
                                    break;
                                }
                                $img = '';
                                $imgArr = explode(',', $userProduct->image);
                                if (count($imgArr) > 0) {
                                    foreach ($imgArr as $imgRow) {
                                        if ($imgRow != '') {
                                            $img = $imgRow;
                                            break;
                                        }
                                    }
                                }
                                if ($img != '') {

                                    $returnArr['suggested'] .= '<img alt="' . $userProduct->product_name . '" src="' . base_url() . 'images/product/' . $img . '">';
                                    $plimit++;
                                }
                            }
                        }

                        $returnArr['suggested'] .= '</span>
							</li>
						';
                        $limitCount++;
                    }
                }
            }
        }
        $returnArr['suggested'] .= '</ul>';

        /*         * ******************************************************** */

        /*         * **************Get Top Users For All Categories********* */

        $returnArr['categories'] = '';
        if ($this->data['mainCategories']->num_rows() > 0) {
            foreach ($this->data['mainCategories']->result() as $catRow) {
                if ($catRow->id != '' && $catRow->cat_name != '') {
                    $returnArr['categories'] .= '
					<div style="display:none;" class="intxt ' . url_title($catRow->cat_name, '_', TRUE) . '">
					<p class="stit"><span>' . $catRow->cat_name . '</span>
					<button class="btns-blue-embo btn-followall">Follow All</button></p>
					<ul class="suggest-list">';
                    $userCountArr = $this->product_model->get_top_users_in_category($catRow->id);
                    $limitCount = 0;
                    foreach ($userCountArr as $user_id => $products) {
                        if ($limitCount > 5) {
                            break;
                        }
                        if ($user_id != '') {
                            //$condition = array('id'=>$user_id,'is_verified'=>'Yes','status'=>'Active');
                            $condition = array('id' => $user_id, 'status' => 'Active');
                            $userDetails = $this->product_model->get_all_details(USERS, $condition);
                            if ($userDetails->num_rows() == 1) {
                                $condition = array('user_id' => $user_id, 'status' => 'Publish');
                                $userProductDetails = $this->product_model->get_all_details(PRODUCT, $condition);
                                $total_seller_product = $userProductDetails->num_rows();
                                $affiProductDetails = $this->product_model->get_all_details(USER_PRODUCTS, $condition);
                                $total_user_product = $affiProductDetails->num_rows();
                                $total_things = $total_seller_product + $total_user_product;
                                if ($limitCount < 5) {
                                    $userImg = $userDetails->row()->thumbnail;
                                    if ($userImg == '') {
                                        $userImg = 'user-thumb1.png';
                                    }
                                    $returnArr['categories'] .= '
											<li><span class="vcard"><img src="' . base_url() . 'images/users/' . $userImg . '"></span>
											<b>' . $userDetails->row()->full_name . '</b><br>
											' . $userDetails->row()->followers_count . ' followers<br>
											' . $total_things . ' things<br>
											<a uid="' . $user_id . '" class="follow-user-link" href="javascript:void(0)">Follow</a>
											<span class="category-thum">';
                                    $plimit = 0;
                                    if ($userProductDetails->num_rows() > 0) {
                                        foreach ($userProductDetails->result() as $userProduct) {
                                            if ($plimit > 3) {
                                                break;
                                            }
                                            $img = '';
                                            $imgArr = explode(',', $userProduct->image);
                                            if (count($imgArr) > 0) {
                                                foreach ($imgArr as $imgRow) {
                                                    if ($imgRow != '') {
                                                        $img = $imgRow;
                                                        break;
                                                    }
                                                }
                                            }
                                            if ($img != '') {

                                                $returnArr['categories'] .= '<img alt="' . $userProduct->product_name . '" src="' . base_url() . 'images/product/' . $img . '">';
                                                $plimit++;
                                            }
                                        }
                                    }
                                    if ($plimit == 4) {
                                        $plimit = 4;
                                    } else {
                                        if ($plimit == 0) {
                                            $plimit = 0;
                                        } else {
                                            if ($plimit == 1) {
                                                $plimit = 1;
                                            } else {
                                                if ($plimit == 2) {
                                                    $plimit = 2;
                                                } else {
                                                    if ($plimit == 3) {
                                                        $plimit = 3;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if ($affiProductDetails->num_rows() > 0) {
                                        foreach ($affiProductDetails->result() as $userProduct) {
                                            if ($plimit > 3) {
                                                break;
                                            }
                                            $img = '';
                                            $imgArr = explode(',', $userProduct->image);
                                            if (count($imgArr) > 0) {
                                                foreach ($imgArr as $imgRow) {
                                                    if ($imgRow != '') {
                                                        $img = $imgRow;
                                                        break;
                                                    }
                                                }
                                            }
                                            if ($img != '') {
                                                $returnArr['categories'] .= '<img alt="' . $userProduct->product_name . '" src="' . base_url() . 'images/product/' . $img . '">';
                                                $plimit++;
                                            }
                                        }
                                    }

                                    $returnArr['categories'] .= '</span>
											</li>
										';
                                    $limitCount++;
                                }
                            }
                        }
                    }
                    $returnArr['categories'] .= '</ul></div>';
                }
            }
        }

        /*         * ******************************************************* */

        echo json_encode($returnArr);
    }

    public function display_product_shuffle() {
        $productDetails = $this->product_model->view_product_details(' where p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" or p.status="Publish" and p.quantity > 0 and p.user_id=0');
        if ($productDetails->num_rows() > 0) {
            $productId = array();
            foreach ($productDetails->result() as $productRow) {
                array_push($productId, $productRow->id);
            }
            array_filter($productId);
            shuffle($productId);
            $pid = $productId[0];
            $productName = '';
            foreach ($productDetails->result() as $productRow) {
                if ($productRow->id == $pid) {
                    $productName = $productRow->product_name;
                }
            }
            if ($productName == '') {
                redirect(base_url());
            } else {
                $link = 'things/' . $pid . '/' . url_title($productName, '-');
                redirect($link);
            }
        } else {
            redirect(base_url());
        }
    }

    public function load_short_url() {
        $short_url = $this->uri->segment(2, 0);
        if ($short_url != '') {
            $url_details = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
            if ($url_details->num_rows() == 1) {
                redirect($url_details->row()->long_url);
            } else {
                show_error('Invalid short url provided. Make sure the url you typed is correct. <a href="' . base_url() . '">Click here</a> to go home page.', '404', 'Invalid Url');
            }
        }
    }

    public function get_short_url() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            $returnStr['message'] = 'Login required';
        } else {
            $url = $this->input->post('url');
            $product_type = $this->input->post('product_type');
            $pidArr = explode('things/', $url);
            $pidArr = explode('/', $pidArr[1]);
            $pid = $pidArr[0];
            if ($product_type == 'selling') {
                $product_details = $this->product_model->get_all_details(PRODUCT, array('id' => $pid));
                $pid = $product_details->row()->seller_product_id;
            }

            //Check same refer url exists in db
            $check_url = $this->product_model->get_all_details(SHORTURL, array('product_id' => $pid, 'user_id' => $this->checkLogin('U')));

            if ($check_url->num_rows() > 0) {
                $short_url = $check_url->row()->short_url;
            } else {
                $short_url = $this->get_rand_str('6');
                $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
                while ($checkId->num_rows() > 0) {
                    $short_url = $this->get_rand_str('6');
                    $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
                }
                $this->product_model->simple_insert(SHORTURL, array('short_url' => $short_url, 'long_url' => $url, 'product_id' => $pid, 'user_id' => $this->checkLogin('U')));
            }

            $returnStr['short_url'] = base_url() . 't/' . $short_url;
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function single_product_details() {
        $pid = $this->input->post('pid');
        $condition = "  where u.group='Seller' and u.status='Active' and p.id='" . $pid . "' or p.status='Publish' and p.user_id=0 and p.id='" . $pid . "'";
        $productDetails = $this->product_model->view_product_details($condition);

        echo json_encode($productDetails->result_array()[0]);
    }

    public function display_product_detail() {
        
        $pid = $this->uri->segment(2, 0);
        $limit = 0;
        $relatedArr = array();
        $relatedProdArr = array();
        
        //$condition = " where p.id = '".$pid."' AND p.status = 'Publish'";
        $condition = "  where u.group='Seller' and u.status='Active' and p.id='" . $pid . "' or p.status='Publish' and p.user_id=0 and p.id='" . $pid . "'";
        $this->data['productDetails'] = $this->product_model->view_product_details($condition);
        $this->data['PrdAttrVal'] = $this->product_model->view_subproduct_details_join($pid);
        // $this->data['allAddonProducts'] = $this->product_model->get_all_addon_products();
        if($this->data['productDetails']->num_rows() == 1){
            if($this->data['productDetails']->row()->seourl !== $this->uri->segment(3, 0)){
                header('Location:'.base_url().'things/'.$pid.'/'.$this->data['productDetails']->row()->seourl.'');
            }
        }
        
        $addonProducts =  $this->product_model->get_all_addon_products();
        $this->data['allAddonProducts'] = $addonProducts;
        $ResponseProductList = $addonProducts->result();

        foreach ($ResponseProductList as $key => $value) {
            $cityQuantity = $this->product_model->view_product_quantity(array('product_id' => $value->id));
            if($this->session->userdata("prcity")){
                if($cityQuantity[$this->session->userdata("prcity")]  &&  $cityQuantity[$this->session->userdata("prcity")] > 0){
                    $ResponseProductList[$key]->is_sold = 0;
                }else{
                     $ResponseProductList[$key]->is_sold = 1;  
                }
            }else{
                 $total_quantity = array_sum ( $cityQuantity );
                 if($total_quantity > 0){
                    $ResponseProductList[$key]->is_sold = 0;   
                 }else{
                    $ResponseProductList[$key]->is_sold = 1;   
                 }

            }
        }
        $this->data['ResponseProductList'] = $ResponseProductList;
        
        //echo $this->db->last_query();

        $cat_addon = $this->product_model->get_addon_category();

        if ($cat_addon != 0) {
            /* Return Addon's Sub Categories */
            $addon_cats = $this->product_model->get_addon_category_details($cat_addon);
            $this->data['addon_cats'] = $addon_cats->result();
        } else {
            $this->data['addon_cats'] = null;
        }


        //getting details of all products which has added in cart.
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $this->session->userdata('fc_session_user_id')));
        $attr_value = $cartVal->result();

        foreach ($attr_value as $attr_values) {
            //stores all added basket product ids in array
            $current_basket_prod_ids[] = $attr_values->product_id;
        }

        //convert all product ids into comma seprater to get product details which is are not available in basket(addons,is this addon or not)
        $added_cart_product_ids = implode(',', $current_basket_prod_ids);
        $get_basket_addon_id = $this->product_model->get_basket_addon_id($added_cart_product_ids);

        // got addon product's parent id of this screen product
        foreach ($get_basket_addon_id as $get_basket_addon) {
            $basket_addon_array[] = $get_basket_addon['addons'];
            if ($get_basket_addon['addons'] != 0) {
                if (strpos($get_basket_addon['addons'], $pid) !== false) {
                    $parent_prod_id = $get_basket_addon['id'];
                }
            }
        }

        //while adding to cart checking parent prodcut is available or not in basket
        $basket_addons = implode(',', $basket_addon_array);
        $explode_basket_addons = explode(',', $basket_addons);
        if (in_array($pid, $explode_basket_addons)) {
            $this->data['parent_available'] = 1;
        } else {
            $this->data['parent_available'] = 0;
        }
        //getting product duration id of parent parent product is available in cart.
        foreach ($attr_value as $res) {
            if ($res->product_id == $parent_prod_id) {
                $selected_prod_duration_id = $res->attribute_values;
            }
        }
        $cart_prod_duration = $this->product_model->get_prod_attribute($selected_prod_duration_id);
        $this->data['cart_product_duration'] = $cart_prod_duration[0]['attr_name'];

        $this->data['prod_cart_count'] = count($attr_value);
        $this->data['is_addon_prod'] = $this->data['productDetails']->row()->is_addon;

        if ($this->data['productDetails']->num_rows() == 1) {

            if ($this->data['productDetails']->row()->subproducts != '') {

                $subProductArr = explode(',', $this->data['productDetails']->row()->subproducts);
                //print_r($subProductArr);exit;
                foreach ($subProductArr as $sp) {
                    $subProAttrArray[] = $this->product_model->view_subproduct_details_join($sp);
                    $subProArray[] = $this->product_model->get_product_by_id($sp);
                }

                foreach ($subProAttrArray as $subProAttr) {
                    $subProducts[] = $subProAttr->result();
                }
                //print_r($subProducts);exit;
                $this->data['subProAttrArray'] = $subProducts;
                $this->data['subProductsArray'] = $subProArray;
            }

            $this->data['productComment'] = $this->product_model->view_product_comments_details('where c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');


            $catArr = explode(',', $this->data['productDetails']->row()->addons);
            if (count($catArr) > 0) {
                foreach ($catArr as $cat) {
                    if ($cat != '') {
                        $condition = " where p.id = '" . $cat . "' AND p.id != '" . $pid . "' and p.quantity > 0 AND p.status = 'Publish'";
                        //$condition =' where FIND_IN_SET("'.$cat.'",p.category_id) and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" and p.id != "'.$pid.'" or p.status="Publish" and p.quantity > 0 and p.user_id=0 and FIND_IN_SET("'.$cat.'",p.category_id) and p.id != "'.$pid.'" limit 6';
                        $usercondition = ' where FIND_IN_SET("' . $cat . '",p.category_id) and p.status="Publish" and p.global_visible=1 and u.status="Active" and p.id != "' . $pid . '" or p.status="Publish" and p.user_id=0 and FIND_IN_SET("' . $cat . '",p.category_id) and p.id != "' . $pid . '" limit 4';
                        $relatedProductDetails = $this->product_model->view_product_details($condition);
                        $relatedaffiProductDetails = $this->product_model->view_product_details_user($usercondition);

                        if ($relatedProductDetails->num_rows() > 0) {
                            foreach ($relatedProductDetails->result() as $relatedProduct) {
                                if (!in_array($relatedProduct->id, $relatedArr)) {
                                    array_push($relatedArr, $relatedProduct->id);
                                    $relatedProdArr[] = $relatedProduct;
                                    $limit++;
                                }
                            }
                        }
                        if ($relatedaffiProductDetails->num_rows() > 0) {
                            foreach ($relatedaffiProductDetails->result() as $relatedProduct) {
                                if (!in_array($relatedProduct->id, $relatedArr)) {
                                    array_push($relatedArr, $relatedProduct->id);
                                    $affirelatedProdArr[] = $relatedProduct;
                                    $limit++;
                                }
                            }
                        }
                    }
                }
                $sr = 0;
                $CidName = array();
                $si = 0;
                $breadCumps .= '<a class="shop-home" href="' . base_url() . $this->data['cat_slug']. '/all">Shop &nbsp;</a>';
                foreach ($catArr as $cat) {
                    if ($cat != '') {
                        $CatNameDetails = $this->product_model->get_all_details(CATEGORY, array('id' => $cat));
                        if ($CatNameDetails->num_rows() == 1) {
                            if ($CatNameDetails->row()->cat_name != 'Our Picks') {
                                $Cresult = $this->product_model->get_Sub_to_Root_cat($cat);
                                if ($Cresult->num_rows() == 1) {
                                    $string = $Cresult->row()->node_name;
                                    $string2 = '/ <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->node_seourl . '">' . $Cresult->row()->node_name . '&nbsp;</a>';
                                    if (isset($Cresult->row()->up1_id)) {
                                        $sr++;
                                        $string = $Cresult->row()->up1_name . '>' . $Cresult->row()->node_name;
                                        $string2 = '/ <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up1_seourl . '">' . $Cresult->row()->up1_name . '&nbsp;</a>
										/ <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up1_seourl . '/' . $Cresult->row()->node_seourl . '">' . $Cresult->row()->node_name . '&nbsp;</a>';
                                        if (isset($Cresult->row()->up2_id)) {
                                            $sr++;
                                            $string = $Cresult->row()->up2_name . '>' . $Cresult->row()->up1_name . '>' . $Cresult->row()->node_name;
                                            $string2 = '/ <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up2_seourl . '">' . $Cresult->row()->up2_name . '&nbsp;</a>
											/ <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up2_seourl . '/' . $Cresult->row()->up1_seourl . '">' . $Cresult->row()->up1_name . '&nbsp;</a>
											/ <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up2_seourl . '/' . $Cresult->row()->up1_seourl . '/' . $Cresult->row()->node_seourl . '">' . $Cresult->row()->node_name . '&nbsp;</a>';

                                            if (isset($Cresult->row()->up3_id)) {
                                                $sr++;
                                                $string = $Cresult->row()->up3_name . '>' . $Cresult->row()->up2_name . '>' . $Cresult->row()->up1_name . '>' . $Cresult->row()->node_name;
                                                $string2 = '/ <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up3_seourl . '">' . $Cresult->row()->up3_name . '&nbsp;</a>
												/ <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up3_seourl . '/' . $Cresult->row()->up2_seourl . '">' . $Cresult->row()->up2_name . '&nbsp;</a>
												/ <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up3_seourl . '/' . $Cresult->row()->up2_seourl . '/' . $Cresult->row()->up1_seourl . '">' . $Cresult->row()->up1_name . '&nbsp;</a>
												/ <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up3_seourl . '/' . $Cresult->row()->up2_seourl . '/' . $Cresult->row()->up1_seourl . '/' . $Cresult->row()->node_seourl . '">' . $Cresult->row()->node_name . '&nbsp;</a>';
                                                break;
                                            }
                                        }
                                    }
                                    $value[$si] = $sr;
                                    $si++;
                                }
                            }
                        }
                    }
                }
                $breadCumps .= $string2;
                $breadCumps .= '<div class="clear"></div>';
                
                $this->data['breadCumps'] = $breadCumps;
            }
        }
        $this->data['relatedProductsArr'] = $relatedProdArr;
        $this->data['affiliaterelatedProductsArr'] = $affirelatedProdArr;
        $recentLikeArr = $this->product_model->get_recent_like_users($this->data['productDetails']->row()->seller_product_id);
        $recentUserLikes = array();
        if ($recentLikeArr->num_rows() > 0) {
            foreach ($recentLikeArr->result() as $recentLikeRow) {
                if ($recentLikeRow->user_id != '') {
                    $recentUserLikes[$recentLikeRow->user_id] = $this->product_model->get_recent_user_likes($recentLikeRow->user_id, $this->data['productDetails']->row()->seller_product_id);
                }
            }
        }
        $this->data['recentLikeArr'] = $recentLikeArr;
        $this->data['recentUserLikes'] = $recentUserLikes;
        if ($this->checkLogin('U') != '') {
            $this->data['userDetails'] = $this->product_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));
        } else {
            $this->data['userDetails'] = array();
        }
        $this->data['seller_product_details'] = $this->product_model->get_all_details(PRODUCT, array('user_id' => $this->data['productDetails']->row()->user_id, 'id !=' => $pid, 'status' => 'Publish'));
        $this->data['seller_affiliate_products'] = $this->product_model->get_all_details(USER_PRODUCTS, array('user_id' => $this->data['productDetails']->row()->user_id));

        if ($this->data['productDetails']->row()->meta_title != '') {
            $this->data['meta_title'] = $this->data['productDetails']->row()->meta_title;
            $this->data['heading'] = $this->data['productDetails']->row()->meta_title;
        } else {
            $this->data['meta_title'] = ' Rent Furniture Online - ' . $this->data['productDetails']->row()->product_name;
            $this->data['heading'] = $this->data['productDetails']->row()->product_name;
        }
        if ($this->data['productDetails']->row()->meta_keyword != '') {
            $this->data['meta_keyword'] = $this->data['productDetails']->row()->meta_keyword;
        }
        //else {
        // 	$this->data['meta_keyword'] = $this->data['productDetails']->row()->product_name;
        // }
        if ($this->data['productDetails']->row()->meta_description != '') {
            $this->data['meta_description'] = $this->data['productDetails']->row()->meta_description;
        } else {
            //$this->data['meta_description'] = $this->data['productDetails']->row()->product_name;
            $PRODUCTNAME = $this->data['productDetails']->row()->product_name;
            $USERNAME = $this->data['productDetails']->row()->user_name;
            if ($PRODUCTNAME != '' && $USERNAME != '') {
                $this->data['meta_description'] = 'Furniture Rental - Rent ' . $PRODUCTNAME . ' Online in India - by ' . $USERNAME . '. Door Step Delivery, Hight Quality Products, Easy Terms.';
            }
        }
        $this->data['product_feedback'] = $this->product_model->product_feedback_view($this->data['productDetails']->row()->user_id);
        $clrWherCond .= ' where p.id !="" and p.excerpt!="" and p.status="Publish" and u.status="Active" order by p.created desc limit 0,6';
        $clreviewList = $this->product_model->searchShopyByCategoryuser($clrWherCond);
        $this->data['clreviewList'] = $clreviewList->result();

        $this->data['slider_view'] = $this->data['productDetails']->row()->slider_view;
        
        $breadcrump = $this->product_model->getProductCategories($pid);
        
        $this->data['breadcrump'] = $breadcrump;
        $this->data['selectedCity'] = $this->product_model->get_current_city_details();

        if(isset($_SESSION['prcity']) && $_SESSION['prcity'] != '') {
            $this->data['current_city'] = $this->product_model->get_current_city_details();
        }
        
        $this->data['is_sold'] = 1;
        $this->data['cityQuantity'] = $this->product_model->view_product_quantity(array('product_id' => $pid));

        if($this->session->userdata("prcity")){
            if($this->data['cityQuantity'][$this->session->userdata("prcity")] && $this->data['cityQuantity'][$this->session->userdata("prcity")] > 0 ){
                $this->data['is_sold'] = 0;
            }else{
                $this->data['is_sold'] = 1;
            }
        }else{
             $total_quantity = array_sum ( $this->data['cityQuantity'] );
             if($total_quantity > 0){
                  $this->data['is_sold'] = 0; 
             }else{
                  $this->data['is_sold'] = 1;   
             }
        }
        
        $this->data['header_code_snippet'] = $this->data['productDetails']->row()->header_code_snippet;
                
        if ($this->data['slider_view'] == 'yes') {
            $this->load->view('site/product/product_detail1', $this->data);
        } else {
            $this->load->view('site/product/product_detail2', $this->data);
        }
    }

    public function delete_featured_find() {
        $uid = $this->checkLogin('U');
        $dataArr = array('feature_product' => '');
        $condition = array('id' => $uid);
        $this->product_model->update_details(USERS, $dataArr, $condition);
        echo '1';
    }

    public function add_featured_find() {
        $pid = $this->input->post('tid');
        $uid = $this->checkLogin('U');
        $dataArr = array('feature_product' => $pid);
        $condition = array('id' => $uid);
        $this->product_model->update_details(USERS, $dataArr, $condition);
        $datestring = "%Y-%m-%d %h:%i:%s";
        $time = time();
        $createdTime = mdate($datestring, $time);
        $actArr = array(
            'activity' => 'featured',
            'activity_id' => $pid,
            'user_id' => $this->checkLogin('U'),
            'activity_ip' => $this->input->ip_address(),
            'created' => $createdTime
        );
        $this->product_model->simple_insert(NOTIFICATIONS, $actArr);
        $this->send_feature_noty_mail($pid);
        echo '1';
    }

    /* Ajax update for Product Details product */

    public function ajaxProductDetailAttributeUpdate() {

        $attrPriceVal = $this->product_model->get_all_details(SUBPRODUCT, array('pid' => $this->input->post('attId'), 'product_id' => $this->input->post('prdId')));
        /* $shopattrVal = $this->product_model->get_all_details(SHOPPING_CART,array('user_id'=>$this->checkLogin('U'),'attribute_values'=>$attrPriceVal->row()->attr_id,'product_id'=>$this->input->post('prdId')));
          if($shopattrVal->row()->quantity != ''){ $ShopVals = $shopattrVal->row()->quantity; }else{ $ShopVals = 0;} .'|'.$ShopVals */
        if ($attrPriceVal->num_rows() == 0) {
            $product_details = $this->product_model->get_all_details(PRODUCT, array('id' => $this->input->post('prdId')));
            echo '0|' . $product_details->row()->sale_price;
        } else {
            echo $attrPriceVal->row()->attr_id . '|' . $attrPriceVal->row()->attr_price;
        }
    }

    public function share_with_someone() {
        $returnStr['status_code'] = 0;
        $thing = array();
        $thing['url'] = $this->input->post('url');
        $thing['name'] = $this->input->post('name');
        $thing['id'] = $this->input->post('oid');
        $thing['refid'] = $this->input->post('ooid');
        $thing['msg'] = $this->input->post('message');
        $thing['uname'] = $this->input->post('uname');
        $thing['timage'] = base_url() . $this->input->post('timage');
        $email = $this->input->post('emails');
        $users = $this->input->post('users');
        if (filter_var($email)) {
            $this->send_thing_share_mail($thing, $email);
            $returnStr['status_code'] = 1;
        } else {
            if ($this->lang->line('invalid_email') != '')
                $returnStr['message'] = $this->lang->line('invalid_email');
            else
                $returnStr['message'] = 'Invalid email';
        }
        echo json_encode($returnStr);
    }

    public function send_thing_share_mail($thing = '', $email = '') {

        $newsid = '2';
        $template_values = $this->product_model->get_newsletter_template_details($newsid);
        $adminnewstemplateArr = array(
            'meta_title' => $this->config->item('meta_title'),
            'logo' => $this->data['logo'],
            'uname' => ucfirst($thing['uname']),
            'name' => $thing['name'],
            'url' => $thing['url'],
            'msg' => $thing['msg'],
            'timage' => $thing['timage'],
            'email_title' => $this->config->item('email_title')
        );
        extract($adminnewstemplateArr);
        if ($this->data['userDetails']->row()->full_name != '') {
            $uname = $this->data['userDetails']->row()->full_name;
        }
        $subject = $template_values['news_subject'];
        $message .= '<!DOCTYPE HTML>
								<html>
								<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								
								<meta name="viewport" content="width=device-width"/>
								<title>' . $adminnewstemplateArr['meta_title'] . ' - Share Things</title>
								<body>';
        include('./newsletter/registeration' . $newsid . '.php');

        $message .= '</body>
								</html>';
        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->config->item('site_contact_mail');
            $sender_name = $this->config->item('email_title');
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }

        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $email,
            'subject_message' => $subject,
            'body_messages' => $message
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);

        /* 		echo $this->email->print_debugger();die;
         */
    }

    public function add_have_tag() {
        $returnStr['status_code'] = 0;
        $tid = $this->input->post('thing_id');
        $uid = $this->checkLogin('U');
        if ($uid != '') {
            $ownArr = explode(',', $this->data['userDetails']->row()->own_products);
            $ownCount = $this->data['userDetails']->row()->own_count;
            if (!in_array($tid, $ownArr)) {
                array_push($ownArr, $tid);
                $ownCount++;
                $dataArr = array('own_products' => implode(',', $ownArr), 'own_count' => $ownCount);
                $wantProducts = $this->product_model->get_all_details(WANTS_DETAILS, array('user_id' => $this->checkLogin('U')));
                if ($wantProducts->num_rows() == 1) {
                    $wantProductsArr = explode(',', $wantProducts->row()->product_id);
                    if (in_array($tid, $wantProductsArr)) {
                        if (($key = array_search($tid, $wantProductsArr)) !== false) {
                            unset($wantProductsArr[$key]);
                        }
                        $wantsCount = $this->data['userDetails']->row()->want_count;
                        $wantsCount--;
                        $dataArr['want_count'] = $wantsCount;
                        $this->product_model->update_details(WANTS_DETAILS, array('product_id' => implode(',', $wantProductsArr)), array('user_id' => $uid));
                    }
                }
                $this->product_model->update_details(USERS, $dataArr, array('id' => $uid));
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function delete_have_tag() {
        $returnStr['status_code'] = 0;
        $tid = $this->input->post('thing_id');
        $uid = $this->checkLogin('U');
        if ($uid != '') {
            $ownArr = explode(',', $this->data['userDetails']->row()->own_products);
            $ownCount = $this->data['userDetails']->row()->own_count;
            if (in_array($tid, $ownArr)) {
                if ($key = array_search($tid, $ownArr) !== false) {
                    unset($ownArr[$key]);
                    $ownCount--;
                }
                $this->product_model->update_details(USERS, array('own_products' => implode(',', $ownArr), 'own_count' => $ownCount), array('id' => $uid));
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function upload_product_image() {
        $returnStr['status_code'] = 0;
        $config['overwrite'] = FALSE;
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        //	    $config['max_size'] = 2000;
        $config['upload_path'] = './images/product';
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('thefile')) {
            $this->upload_one_more($_FILES['thefile']);
            $imgDetails = $this->upload->data();
            $returnStr['image']['url'] = base_url() . 'images/product/' . $imgDetails['file_name'];
            $returnStr['image']['width'] = $imgDetails['image_width'];
            $returnStr['image']['height'] = $imgDetails['image_height'];
            $returnStr['image']['name'] = $imgDetails['file_name'];

            $this->imageResizeWithSpace(800, 600, $imgDetails['file_name'], './images/product/');
            $this->imageResizeWithSpace(210, 210, $imgDetails['file_name'], './images/product/thumb/');
            $returnStr['status_code'] = 1;
        } else {
            if ($this->lang->line('cant_upload') != '')
                $returnStr['message'] = $this->lang->line('cant_upload');
            else
                $returnStr['message'] = 'Can\'t be upload';
        }
        echo json_encode($returnStr);
    }

    public function upload_one_more($image) {
        $config1['overwrite'] = false;
        $config1['allowed_types'] = 'jpg|jpeg|gif|png';
        $config1['upload_path'] = './images/product/Copressed Images';
        $this->upload->initialize($config1);

        if ($this->upload->do_upload('thefile')) {
            $this->imageResizeWithSpace(210, 210, $imgDetails['file_name'], './images/product/Copressed Images/');
        }
    }

    public function add_new_thing() {
        $returnStr['status_code'] = 0;
        $returnStr['message'] = '';
        if ($this->checkLogin('U') != '') {
            $short_url = $this->get_rand_str('6');
            $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
            while ($checkId->num_rows() > 0) {
                $short_url = $this->get_rand_str('6');
                $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
            }
            $result = $this->product_model->add_user_product($this->checkLogin('U'), $short_url);
            if ($result['image'] != '') {
                $this->imageResizeWithSpace(210, 210, $result['image'], './images/product/thumb/');
            }
            $returnStr['status_code'] = 1;
            $userDetails = $this->data['userDetails'];
            $total_added = $userDetails->row()->products;
            $total_added++;
            $this->product_model->update_details(USERS, array('products' => $total_added), array('id' => $this->checkLogin('U')));
            $returnStr['thing_url'] = 'user/' . $userDetails->row()->user_name . '/things/' . $result['pid'] . '/' . url_title($this->input->post('name'), '-');
        }
        echo json_encode($returnStr);
    }

    public function extract_image_urls() {
        include('./simple_html_dom.php');
        //	$returnStr['status_code'] = 0;
        $returnStr['response'] = array();
        $host_name_arr = explode('/', $this->input->get('url'));
        $url = 'http://' . $this->input->get('url');
        $url = str_replace(" ", '%20', $url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//		curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); //Set curl to return the data instead of printing it to the browser.
        $html_cnt = curl_exec($ch);
        //echo $html_cnt;die;
        /* 		echo "<pre>"; print_r(curl_getinfo($ch)) . '<br/>';
          echo "<pre>"; var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE)) . '<br/>';
          echo curl_errno($ch) . '<br/>';
          echo curl_error($ch) . '<br/>'; */
        curl_close($ch);
        /* $html_cnt = file_get_html($url); */
        $html_base = new simple_html_dom();
        $html_base->load($html_cnt);
//		var_dump($html_cnt);//die;
        foreach ($html_base->find('img') as $imageURL) {      ////Map Url
            if (substr($imageURL->src, 0, 4) == 'http' || substr($imageURL->src, 0, 2) == '//') {
                //				if (substr($imageURL->src, -3,3)=='jpg' || substr($imageURL->src, -3,3)=='png' || substr($imageURL->src, -3,3)=='gif' || substr($imageURL->src, -4,4)=='jpeg'){
                array_push($returnStr['response'], $imageURL->src);
                $returnStr['alt'][] = $imageURL->alt;
                //				}
            } else {
                //				if (substr($imageURL->src, -3,3)=='jpg' || substr($imageURL->src, -3,3)=='png' || substr($imageURL->src, -3,3)=='gif' || substr($imageURL->src, -4,4)=='jpeg'){
                if (substr($imageURL->src, 0, 1) == '/') {
                    array_push($returnStr['response'], 'http://' . $host_name_arr[0] . $imageURL->src);
                } else {
                    array_push($returnStr['response'], 'http://' . $host_name_arr[0] . '/' . $imageURL->src);
                }
                $returnStr['alt'][] = $imageURL->alt;
                //				}
            }
        }
        foreach ($html_base->find('title') as $titleName) {
            $returnStr['title'][] = $titleName->innertext;
        }
        $html_base->clear();
        unset($html_base);
        echo json_encode($returnStr);
    }

    public function display_user_thing() {
        $uname = $this->uri->segment(2, 0);
        $pid = $this->uri->segment(4, 0);
        $this->data['productUserDetails'] = $this->product_model->get_all_details(USERS, array('user_name' => $uname));
        $this->data['productDetails'] = $this->product_model->view_notsell_product_details(' where p.seller_product_id="' . $pid . '" and p.status="Publish"');
        if ($this->data['productDetails']->num_rows() == 1) {
            $this->data['heading'] = $this->data['productDetails']->row()->product_name;
            $categoryArr = explode(',', $this->data['productDetails']->row()->category_id);
            $catID = 0;
            if (count($categoryArr) > 0) {
                foreach ($categoryArr as $catRow) {
                    if ($catRow != '') {
                        $catID = $catRow;
                        break;
                    }
                }
                $sr = 0;
                $CidName = array();
                $si = 0;
                $breadCumps .= '<ul class="breadcrumb">
                            <li><a class="shop-home" href="' . base_url() . $this->data['cat_slug']. '/all">Shop</a></li>';
                foreach ($categoryArr as $cat) {
                    if ($cat != '') {
                        $CatNameDetails = $this->product_model->get_all_details(CATEGORY, array('id' => $cat));
                        if ($CatNameDetails->num_rows() == 1) {
                            if ($CatNameDetails->row()->cat_name != 'Our Picks') {
                                $Cresult = $this->product_model->get_Sub_to_Root_cat($cat);
                                if ($Cresult->num_rows() == 1) {
                                    $string = $Cresult->row()->node_name;
                                    $string2 = '<li><a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->node_seourl . '">' . $Cresult->row()->node_name . '</a></li>';
                                    if (isset($Cresult->row()->up1_id)) {
                                        $sr++;
                                        $string = $Cresult->row()->up1_name . '>' . $Cresult->row()->node_name;
                                        $string2 = '/<li> <a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up1_seourl . '">' . $Cresult->row()->up1_name . '</a></li>
										/ <li><a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up1_seourl . '/' . $Cresult->row()->node_seourl . '">' . $Cresult->row()->node_name . '</a></li>';
                                        if (isset($Cresult->row()->up2_id)) {
                                            $sr++;
                                            $string = $Cresult->row()->up2_name . '>' . $Cresult->row()->up1_name . '>' . $Cresult->row()->node_name;
                                            $string2 = '/<li><a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up2_seourl . '">' . $Cresult->row()->up2_name . '</a></li>
											/<li><a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up2_seourl . '/' . $Cresult->row()->up1_seourl . '">' . $Cresult->row()->up1_name . '</a></li>
											/<li><a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up2_seourl . '/' . $Cresult->row()->up1_seourl . '/' . $Cresult->row()->node_seourl . '">' . $Cresult->row()->node_name . '</a></li>';

                                            if (isset($Cresult->row()->up3_id)) {
                                                $sr++;
                                                $string = $Cresult->row()->up3_name . '>' . $Cresult->row()->up2_name . '>' . $Cresult->row()->up1_name . '>' . $Cresult->row()->node_name;
                                                $string2 = '/ <li><a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up3_seourl . '">' . $Cresult->row()->up3_name . '</a></li>
												/<li><a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up3_seourl . '/' . $Cresult->row()->up2_seourl . '">' . $Cresult->row()->up2_name . '</a></li>
												/<li><a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up3_seourl . '/' . $Cresult->row()->up2_seourl . '/' . $Cresult->row()->up1_seourl . '">' . $Cresult->row()->up1_name . '</a></li>
												/<li><a href="' . base_url() . $this->data['cat_slug']. '/' . $Cresult->row()->up3_seourl . '/' . $Cresult->row()->up2_seourl . '/' . $Cresult->row()->up1_seourl . '/' . $Cresult->row()->node_seourl . '">' . $Cresult->row()->node_name . '</a></li>';
                                                break;
                                            }
                                        }
                                    }
                                    $value[$si] = $sr;
                                    $si++;
                                }
                            }
                        }
                    }
                }
                $breadCumps .= $string2;
                $breadCumps .= '</ul><div class="clear"></div>';
                $this->data['breadCumps'] = $breadCumps;
            }
            $this->data['relatedProductsArr'] = $this->product_model->get_products_by_category($catID);
            $this->data['affiliaterelatedProductsArr'] = $this->product_model->get_affiliate_products_by_category($catID);
            $this->data['seller_affiliate_products'] = $this->product_model->get_all_details(USER_PRODUCTS, array('user_id' => $this->data['productDetails']->row()->user_id));

            $recentLikeArr = $this->product_model->get_recent_like_users_things($this->data['productDetails']->row()->seller_product_id);
            $recentUserLikes = array();
            if ($recentLikeArr->num_rows() > 0) {
                foreach ($recentLikeArr->result() as $recentLikeRow) {
                    if ($recentLikeRow->user_id != '') {
                        $recentUserLikes[$recentLikeRow->user_id] = $this->product_model->get_recent_user_likes_things($recentLikeRow->user_id, $this->data['productDetails']->row()->seller_product_id);
                    }
                }
            }
            $this->data['recentLikeArr'] = $recentLikeArr;
            if ($this->data['productDetails']->row()->meta_title != '') {
                $this->data['meta_title'] = $this->data['productDetails']->row()->meta_title;
            } else if ($this->data['productDetails']->row()->sale_price > 0) {
                $this->data['meta_title'] = 'Buy Online ' . $this->data['productDetails']->row()->product_name . ' in India on ' . $this->data['siteTitle'];
            } else {
                $this->data['meta_title'] = $this->data['productDetails']->row()->product_name;
            }
            if ($this->data['productDetails']->row()->meta_keyword != '') {
                $this->data['meta_keyword'] = $this->data['productDetails']->row()->meta_keyword;
            } else {
                $this->data['meta_keyword'] = $this->data['productDetails']->row()->product_name;
            }
            if ($this->data['productDetails']->row()->meta_description != '') {
                $this->data['meta_description'] = $this->data['productDetails']->row()->meta_description;
            } else if ($this->data['productDetails']->row()->sale_price > 0) {
                $this->data['meta_description'] = 'Shop for ' . $this->data['productDetails']->row()->product_name . ' in India from ' . $this->data['productDetails']->row()->user_name . '. Read ' . $this->data['productDetails']->row()->user_name . '\'s Review, Check Latest Collection and Price of Products Offered by ' . $this->data['productDetails']->row()->user_name;
            } else {
                /* $this->data['meta_description'] = $this->data['productDetails']->row()->product_name;
                  $PRODUCTNAME = $this->data['productDetails']->row()->product_name;
                  if($PRODUCTNAME != '' && $uname != ''){
                  $this->data['meta_description'] = $PRODUCTNAME; */
                if ($this->data['productDetails']->row()->excerpt != '') {
                    $EXCERPT = substr($this->data['productDetails']->row()->excerpt, 0, 155);
                    $this->data['meta_description'] = $EXCERPT;
                } else {
                    $this->data['meta_description'] = 'Check ' . $this->data['productDetails']->row()->product_name . ' from ' . $this->data['productDetails']->row()->user_name;
                }
            }
            $this->data['meta_image'] = $this->data['productDetails']->row()->image;
            $this->load->view('site/product/display_user_product', $this->data);
        } else {
            $this->load->view('site/product/product_detail', $this->data);
        }
    }

    public function sales_create() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $userType = $this->data['userDetails']->row()->group;
            if ($userType == 'Seller') {
                $pid = $this->input->get('ntid');
                $productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
                if ($productDetails->num_rows() == 1) {
                    if ($productDetails->row()->user_id == $this->data['userDetails']->row()->id) {
                        $sortArr1 = array('field'=>'cat_position','type'=>'asc');
                        $sortArr = array($sortArr1);
                        $this->data['mainCategories'] = $this->product_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
                        $this->data['productDetails'] = $productDetails;
                        $this->data['editmode'] = '0';
                        $this->load->view('site/product/edit_seller_product', $this->data);
                    } else {
                        show_404();
                    }
                } else {
                    show_404();
                }
            } else {
                redirect('seller-signup');
            }
        }
    }

    /**
     *
     * Ajax function for delete the product pictures
     */
    public function editPictureProducts() {
        $ingIDD = $this->input->post('imgId');
        $currentPage = $this->input->post('cpage');
        $id = $this->input->post('val');
        $productImage = explode(',', $this->session->userdata('product_image_' . $ingIDD));
        if (count($productImage) < 2) {
            echo json_encode("No");
            exit();
        } else {
            $empImg = 0;
            foreach ($productImage as $product) {
                if ($product != '') {
                    $empImg++;
                }
            }
            if ($empImg < 2) {
                echo json_encode("No");
                exit();
            }
            $this->session->unset_userdata('product_image_' . $ingIDD);
            $resultVar = $this->setPictureProducts($productImage, $this->input->post('position'));
            $insertArrayItems = trim(implode(',', $resultVar)); //need validation here...because the array key changed here

            $this->session->set_userdata(array('product_image_' . $ingIDD => $insertArrayItems));
            $dataArr = array('image' => $insertArrayItems);
            $condition = array('id' => $ingIDD);
            $this->product_model->update_details(PRODUCT, $dataArr, $condition);
            echo json_encode($insertArrayItems);
        }
    }

    public function edit_product_detail() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $pid = $this->uri->segment(2, 0);
            $viewMode = $this->uri->segment(4, 0);
            $productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
            $ourpick_condition = ' and id !=107';
            $this->data['categoryView'] = $this->product_model->get_category_details($productDetails->row()->category_id, $ourpick_condition);
            $this->data['atrributeValue'] = $this->product_model->view_atrribute_details();


            if ($productDetails->num_rows() == 1) {
                if ($productDetails->row()->user_id == $this->checkLogin('U')) {
                    $this->data['productDetails'] = $productDetails;
                    $this->load->view('site/product/edit_user_product', $this->data);
                } else {
                    show_404();
                }
            } else {
                $productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
                $ourpick_condition = ' and id !=107';
                $this->data['categoryView'] = $this->product_model->get_category_details($productDetails->row()->category_id);
                $this->data['atrributeValue'] = $this->product_model->view_atrribute_details();
                $this->data['PrdattrVal'] = $this->product_model->view_product_atrribute_details();
                $this->data['SubPrdVal'] = $this->product_model->view_subproduct_details($productDetails->row()->id);
                if ($productDetails->num_rows() == 1) {
                    if ($productDetails->row()->user_id == $this->checkLogin('U')) {
                        $this->data['productDetails'] = $productDetails;
                        $this->data['editmode'] = '1';
                        if ($viewMode == '') {
                            $this->load->view('site/product/edit_seller_product', $this->data);
                        } else {
                            $this->load->view('site/product/edit_seller_product_' . $viewMode, $this->data);
                        }
                    } else {
                        show_404();
                    }
                } else {
                    show_404();
                }
            }
        }
    }

    public function edit_user_product_process() {
        $mode = $this->input->post('submit');
        $pid = $this->input->post('productID');
        if ($pid != '') {
            if ($mode == 'Upload') {
                $config['overwrite'] = FALSE;
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                //			    $config['max_size'] = 2000;
                $config['upload_path'] = './images/product';
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('uploadphoto')) {
                    $imgDetails = $this->upload->data();
                    $this->imageResizeWithSpace(800, 600, $imgDetails['file_name'], './images/product/');
                    $this->imageResizeWithSpace(210, 210, $imgDetails['file_name'], './images/product/thumb/');
                    $dataArr['image'] = $imgDetails['file_name'];
                    $this->product_model->update_details(USER_PRODUCTS, $dataArr, array('seller_product_id' => $pid));
                    if ($this->lang->line('poto_chage_succ') != '')
                        $lg_err_msg = $this->lang->line('poto_chage_succ');
                    else
                        $lg_err_msg = 'Photo changed successfully';
                    $this->setErrorMessage('success', $lg_err_msg);
                    echo '<script>window.history.go(-1)</script>';
                }else {
                    if ($this->lang->line('cant_upload') != '')
                        $lg_err_msg = $this->lang->line('cant_upload');
                    else
                        $lg_err_msg = 'Can\'t able to upload';
                    $this->setErrorMessage('error', $lg_err_msg);
                    echo '<script>window.history.go(-1)</script>';
                }
            }else {
                $excludeArr = array('productID', 'submit', 'uploadphoto');
                $dataArr = array(
                    'seourl' => url_title($this->input->post('product_name'), '-'),
                    'modified' => 'now()'
                );
                $this->product_model->commonInsertUpdate(USER_PRODUCTS, 'update', $excludeArr, $dataArr, array('seller_product_id' => $pid));
                if ($this->lang->line('det_updat_succ') != '')
                    $lg_err_msg = $this->lang->line('det_updat_succ');
                else
                    $lg_err_msg = 'Details updated successfully';
                $this->setErrorMessage('success', $lg_err_msg);
                redirect('user/' . $this->data['userDetails']->row()->user_name . '/things/' . $pid . '/' . url_title($this->input->post('product_name'), '-'));
            }
        }
    }

    public function update_price_range_in_table($mode = '', $price_range = '', $product_id = '0', $old_product_details = '') {
        $list_values = $this->product_model->get_all_details(LIST_VALUES, array('list_value' => $price_range));
        if ($list_values->num_rows() == 1) {
            $products = explode(',', $list_values->row()->products);
            $product_count = $list_values->row()->product_count;
            if ($mode == 'add') {
                if (!in_array($product_id, $products)) {
                    array_push($products, $product_id);
                    $product_count++;
                }
            } else if ($mode == 'edit') {
                $old_price_range = '';
                if ($old_product_details != '' && count($old_product_details) > 0 && $old_product_details->num_rows() == 1) {
                    $old_price_range = $old_product_details->row()->price_range;
                }
                if ($old_price_range != '' && $old_price_range != $price_range) {
                    $old_list_values = $this->product_model->get_all_details(LIST_VALUES, array('list_value' => $old_price_range));
                    if ($old_list_values->num_rows() == 1) {
                        $old_products = explode(',', $old_list_values->row()->products);
                        $old_product_count = $old_list_values->row()->product_count;
                        if (in_array($product_id, $old_products)) {
                            if (($key = array_search($product_id, $old_products)) !== false) {
                                unset($old_products[$key]);
                                $old_product_count--;
                                $updateArr = array('products' => implode(',', $old_products), 'product_count' => $old_product_count);
                                $updateCondition = array('list_value' => $old_price_range);
                                $this->product_model->update_details(LIST_VALUES, $updateArr, $updateCondition);
                            }
                        }
                    }
                    if (!in_array($product_id, $products)) {
                        array_push($products, $product_id);
                        $product_count++;
                    }
                } else if ($old_price_range != '' && $old_price_range == $price_range) {
                    if (!in_array($product_id, $products)) {
                        array_push($products, $product_id);
                        $product_count++;
                    }
                }
            }
            $updateArr = array('products' => implode(',', $products), 'product_count' => $product_count);
            $updateCondition = array('list_value' => $price_range);
            $this->product_model->update_details(LIST_VALUES, $updateArr, $updateCondition);
        }
    }
    
    public function updatecity_vise_availability($product_id,$price_range){
        $list_values = $this->product_model->get_all_details(LIST_VALUES,array('list_id'=>1));
        $product = [];
        foreach ($list_values->result_array() as $key => $value) {
            $products = explode(',',$value['products']);
            array_push($products, $product_id);
            $product_count = $value['product_count'] + 1;
            $updateArr = array('products'=>implode(',', $products),'product_count'=>$product_count);
            $updateCondition = array('id'=>$value['id']);
            $this->product_model->update_details(LIST_VALUES,$updateArr,$updateCondition);
        }
    }

    public function sell_it() {
        $mode = $this->uri->segment(4, 0);
        $pid = $this->input->post('PID');
        $nextMode = $this->input->post('nextMode');
        $excludeArr = array('PID', 'nextMode', 'changeorder', 'imaged', 'gateway_tbl_length', 'category_id', 'attribute_name', 'attribute_val');
        if ($mode == '1') {
            $price_range = 0;
            $price = $this->input->post('sale_price');
            if ($price > 0 && $price < 21) {
                $price_range = '1-20';
            } else if ($price > 20 && $price < 101) {
                $price_range = '21-100';
            } else if ($price > 100 && $price < 201) {
                $price_range = '101-200';
            } else if ($price > 200 && $price < 501) {
                $price_range = '201-500';
            } else if ($price > 500) {
                $price_range = '501+';
            }
            if ($pid == '') {
                $old_product_details = array();
                //$condition = array('product_name' => $product_name);
            } else {
                $old_product_details = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
                //$condition = array('product_name' => $product_name,'seller_product_id !=' => $pid);
            }
            $dataArr = array('seller_product_id' => $pid);
            $checkProduct = $this->product_model->get_all_details(PRODUCT, $dataArr);

            if ($checkProduct->num_rows() == 0) {
                $userProduct = $this->product_model->get_all_details(USER_PRODUCTS, $dataArr);
                if ($userProduct->num_rows() == 1) {
                    $dataArr['image'] = $userProduct->row()->image;
                    $dataArr['seourl'] = url_title($this->input->post('product_name'), '-');
                    $dataArr['user_id'] = $userProduct->row()->user_id;
                    $dataArr['price_range'] = $price_range;
                    $dataArr['category_id'] = $userProduct->row()->category_id;
                    
                    $dataArr['list_value'] = '45,46,47,48,49,50';
                    $dataArr['list_name'] = '1,1,1,1,1';
                    $this->product_model->commonInsertUpdate(PRODUCT, 'insert', $excludeArr, $dataArr);
                    $product_id = $this->product_model->get_last_insert_id();
                    $this->updatecity_vise_availability($product_id,$price_range);
                    $this->update_price_range_in_table('add', $price_range, $product_id, $old_product_details);
                    $this->product_model->commonDelete(USER_PRODUCTS, array('seller_product_id' => $pid));
                    
                    $dataSubArr = [];
                    for($k=1;$k< 15;$k++){
                        if($k < 13){
                          $dataSubArr[] = array('product_id'=> $product_id,'attr_id'=>4,'attr_name'=>$k.' Months','attr_price'=>$price); 
                        }else if($k == 13){
                            $dataSubArr[] = array('product_id'=> $product_id,'attr_id'=>4,'attr_name'=>'24 Months','attr_price'=>$price); 
                        }else{
                            $dataSubArr[] = array('product_id'=> $product_id,'attr_id'=>4,'attr_name'=>'36 Months','attr_price'=>$price); 
                        }
                    }
                    $this->db->insert_batch(SUBPRODUCT, $dataSubArr); 
                    if ($this->lang->line('change_saved') != '')
                        $lg_err_msg = $this->lang->line('change_saved');
                    else
                        $lg_err_msg = 'Yeah ! changes have been saved';
                    $this->setErrorMessage('success', $lg_err_msg);
                    $addedProd = $this->session->userdata('prodID');
                    if ($addedProd == '') {
                        $addedProd = array();
                    }
                    array_push($addedProd, $pid);
                    $this->session->set_userdata('prodID', $addedProd);
                    redirect('things/' . $pid . '/edit/' . $nextMode);
                }
            } else {
                $dataArr['seourl'] = url_title($this->input->post('product_name'), '-');
                $dataArr['price_range'] = $price_range;
                $this->product_model->commonInsertUpdate(PRODUCT, 'update', $excludeArr, $dataArr, array('seller_product_id' => $pid));
                $this->update_price_range_in_table('edit', $price_range, $old_product_details->row()->id, $old_product_details);
                if ($this->lang->line('change_saved') != '')
                    $lg_err_msg = $this->lang->line('change_saved');
                else
                    $lg_err_msg = 'Yeah ! changes have been saved';
                $this->setErrorMessage('success', $lg_err_msg);
                redirect('things/' . $pid . '/edit');
            }
        }else if ($mode == 'seo') {
            $this->product_model->commonInsertUpdate(PRODUCT, 'update', $excludeArr, array(), array('seller_product_id' => $pid));
            if ($this->lang->line('change_saved') != '')
                $lg_err_msg = $this->lang->line('change_saved');
            else
                $lg_err_msg = 'Yeah ! changes have been saved';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('things/' . $pid . '/edit/' . $nextMode);
        }else if ($mode == 'images') {
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            //		    $config['max_size'] = 2000;
            $config['upload_path'] = './images/product';
            $this->load->library('upload', $config);
            //echo "<pre>";print_r($_FILES);die;
            $ImageName = '';
            if ($this->upload->do_multi_upload('product_image')) {
                $logoDetails = $this->upload->get_multi_upload_data();
                foreach ($logoDetails as $fileDetails) {
                    $this->imageResizeWithSpace(800, 600, $fileDetails['file_name'], './images/product/');
                    $this->imageResizeWithSpace(210, 210, $fileDetails['file_name'], './images/product/thumb/');
                    $ImageName .= $fileDetails['file_name'] . ',';
                }
            }
            $existingImage = $this->input->post('imaged');

            $newPOsitionArr = $this->input->post('changeorder');
            $imagePOsit = array();

            for ($p = 0; $p < sizeof($existingImage); $p++) {
                $imagePOsit[$newPOsitionArr[$p]] = $existingImage[$p];
            }

            ksort($imagePOsit);
            foreach ($imagePOsit as $keysss => $vald) {
                $imgArraypos[] = $vald;
            }
            $imagArraypo0 = @implode(",", $imgArraypos);
            $allImages = $imagArraypo0 . ',' . $ImageName;

            $dataArr = array('image' => $allImages);
            $this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $pid));
            if ($this->lang->line('change_saved') != '')
                $lg_err_msg = $this->lang->line('change_saved');
            else
                $lg_err_msg = 'Yeah ! changes have been saved';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('things/' . $pid . '/edit/' . $nextMode);
        }else if ($mode == 'categories') {
            if ($this->input->post('category_id') != '') {
                $category_id = implode(',', $this->input->post('category_id'));
            } else {
                $category_id = '';
            }
            $dataArr = array('category_id' => $category_id);
            $this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $pid));
            if ($this->lang->line('change_saved') != '')
                $lg_err_msg = $this->lang->line('change_saved');
            else
                $lg_err_msg = 'Yeah ! changes have been saved';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('things/' . $pid . '/edit/' . $nextMode);
        }else if ($mode == 'list') {
            $list_name_str = $list_val_str = '';
            $list_name_arr = $this->input->post('attribute_name');
            $list_val_arr = $this->input->post('attribute_val');
            if (is_array($list_name_arr) && count($list_name_arr) > 0) {
                $list_name_str = implode(',', $list_name_arr);
                $list_val_str = implode(',', $list_val_arr);
            }
            $dataArr = array('list_name' => $list_name_str, 'list_value' => $list_val_str);
            $this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $pid));

            //Update the list table
            if (is_array($list_val_arr)) {
                foreach ($list_val_arr as $list_val_row) {
                    $list_val_details = $this->product_model->get_all_details(LIST_VALUES, array('id' => $list_val_row));
                    if ($list_val_details->num_rows() == 1) {
                        $product_count = $list_val_details->row()->product_count;
                        $products_in_this_list = $list_val_details->row()->products;
                        $products_in_this_list_arr = explode(',', $products_in_this_list);
                        if (!in_array($pid, $products_in_this_list_arr)) {
                            array_push($products_in_this_list_arr, $pid);
                            $product_count++;
                            $list_update_values = array(
                                'products' => implode(',', $products_in_this_list_arr),
                                'product_count' => $product_count
                            );
                            $list_update_condition = array('id' => $list_val_row);
                            $this->product_model->update_details(LIST_VALUES, $list_update_values, $list_update_condition);
                        }
                    }
                }
            }

            if ($this->lang->line('change_saved') != '')
                $lg_err_msg = $this->lang->line('change_saved');
            else
                $lg_err_msg = 'Yeah ! changes have been saved';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('things/' . $pid . '/edit/' . $nextMode);
        }else if ($mode == 'attribute') {
            $dataArr = array('seller_product_id' => $pid);
            $checkProduct = $this->product_model->get_all_details(PRODUCT, $dataArr);
            if ($checkProduct->num_rows() == 1) {
                $prodId = $checkProduct->row()->id;
                $Attr_name_str = $Attr_val_str = '';
                $Attr_type_arr = $this->input->post('product_attribute_type');
                $Attr_name_arr = $this->input->post('product_attribute_name');
                $Attr_val_arr = $this->input->post('product_attribute_val');
                if (is_array($Attr_type_arr) && count($Attr_type_arr) > 0) {
                    for ($k = 0; $k < sizeof($Attr_type_arr); $k++) {
                        $dataSubArr = '';
                        $dataSubArr = array('product_id' => $prodId, 'attr_id' => $Attr_type_arr[$k], 'attr_name' => $Attr_name_arr[$k], 'attr_price' => $Attr_val_arr[$k]);
                        //echo '<pre>'; print_r($dataSubArr);
                        $this->product_model->add_subproduct_insert($dataSubArr);
                    }
                }

                if ($this->lang->line('change_saved') != '')
                    $lg_err_msg = $this->lang->line('change_saved');
                else
                    $lg_err_msg = 'Yeah ! changes have been saved';
                $this->setErrorMessage('success', $lg_err_msg);
            }else {
                if ($this->lang->line('prod_not_found_db') != '')
                    $lg_err_msg = $this->lang->line('prod_not_found_db');
                else
                    $lg_err_msg = 'Product not found in database';
                $this->setErrorMessage('error', $lg_err_msg);
            }
            redirect('things/' . $pid . '/edit/' . $nextMode);
        }else {
            show_404();
        }
    }

    public function delete_product() {
        $pid = $this->uri->segment(2, 0);
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
            if ($productDetails->num_rows() == 1) {
                if ($productDetails->row()->user_id == $this->checkLogin('U')) {
                    $this->product_model->commonDelete(USER_PRODUCTS, array('seller_product_id' => $pid));
                    $productCount = $this->data['userDetails']->row()->products;
                    $productCount--;
                    $this->product_model->update_details(USERS, array('products' => $productCount), array('id' => $this->checkLogin('U')));
                    if ($this->lang->line('prod_del_succ') != '')
                        $lg_err_msg = $this->lang->line('prod_del_succ');
                    else
                        $lg_err_msg = 'Product deleted successfully';
                    $this->setErrorMessage('success', $lg_err_msg);
                    redirect('user/' . $this->data['userDetails']->row()->user_name . '/added');
                }else {
                    show_404();
                }
            } else {
                $productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
                if ($productDetails->num_rows() == 1) {
                    if ($productDetails->row()->user_id == $this->checkLogin('U')) {
                        $this->product_model->commonDelete(PRODUCT, array('seller_product_id' => $pid));
                        $productCount = $this->data['userDetails']->row()->products;
                        $productCount--;
                        $this->product_model->update_details(USERS, array('products' => $productCount), array('id' => $this->checkLogin('U')));
                        if ($this->lang->line('prod_del_succ') != '')
                            $lg_err_msg = $this->lang->line('prod_del_succ');
                        else
                            $lg_err_msg = 'Product deleted successfully';
                        $this->setErrorMessage('success', $lg_err_msg);
                        redirect('user/' . $this->data['userDetails']->row()->user_name . '/added');
                    }else {
                        show_404();
                    }
                } else {
                    show_404();
                }
            }
        }
    }

    public function add_reaction_tag() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('thing_id');
            $checkProductLike = $this->user_model->get_all_details(PRODUCT_LIKES, array('product_id' => $tid, 'user_id' => $this->checkLogin('U')));
            if ($checkProductLike->num_rows() == 0) {
                $productDetails = $this->user_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
                if ($productDetails->num_rows() == 0) {
                    $productDetails = $this->user_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
                    $productTable = USER_PRODUCTS;
                } else {
                    $productTable = PRODUCT;
                }
                if ($productDetails->num_rows() == 1) {
                    $likes = $productDetails->row()->likes;
                    $dataArr = array('product_id' => $tid, 'user_id' => $this->checkLogin('U'), 'ip' => $this->input->ip_address());
                    $this->user_model->simple_insert(PRODUCT_LIKES, $dataArr);
                    $actArr = array(
                        'activity_name' => 'fancy',
                        'activity_id' => $tid,
                        'user_id' => $this->checkLogin('U'),
                        'activity_ip' => $this->input->ip_address()
                    );
                    $this->user_model->simple_insert(USER_ACTIVITY, $actArr);
                    $likes++;
                    $dataArr = array('likes' => $likes);
                    $condition = array('seller_product_id' => $tid);
                    $this->user_model->update_details($productTable, $dataArr, $condition);
                    $totalUserLikes = $this->data['userDetails']->row()->likes;
                    $totalUserLikes++;
                    $this->user_model->update_details(USERS, array('likes' => $totalUserLikes), array('id' => $this->checkLogin('U')));
                    /*
                     * -------------------------------------------------------
                     * Creating list automatically when user likes a product
                     * -------------------------------------------------------
                     *
                      $listCheck = $this->user_model->get_list_details($tid,$this->checkLogin('U'));
                      if ($listCheck->num_rows() == 0){
                      $productCategoriesArr = explode(',', $productDetails->row()->category_id);
                      if (count($productCategoriesArr)>0){
                      foreach ($productCategoriesArr as $productCategoriesRow){
                      if ($productCategoriesRow != ''){
                      $productCategory = $this->user_model->get_all_details(CATEGORY,array('id'=>$productCategoriesRow));
                      if ($productCategory->num_rows()==1){

                      }
                      }
                      }
                      }
                      }
                     */
                    $returnStr['status_code'] = 1;
                } else {
                    if ($this->lang->line('prod_not_avail') != '')
                        $returnStr['message'] = $this->lang->line('prod_not_avail');
                    else
                        $returnStr['message'] = 'Product not available';
                }
            }else {
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function delete_reaction_tag() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('thing_id');
            $checkProductLike = $this->user_model->get_all_details(PRODUCT_LIKES, array('product_id' => $tid, 'user_id' => $this->checkLogin('U')));
            if ($checkProductLike->num_rows() == 1) {
                $productDetails = $this->user_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
                if ($productDetails->num_rows() == 0) {
                    $productDetails = $this->user_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
                    $productTable = USER_PRODUCTS;
                } else {
                    $productTable = PRODUCT;
                }
                if ($productDetails->num_rows() == 1) {
                    $likes = $productDetails->row()->likes;
                    $conditionArr = array('product_id' => $tid, 'user_id' => $this->checkLogin('U'));
                    $this->user_model->commonDelete(PRODUCT_LIKES, $conditionArr);
                    $actArr = array(
                        'activity_name' => 'unfancy',
                        'activity_id' => $tid,
                        'user_id' => $this->checkLogin('U'),
                        'activity_ip' => $this->input->ip_address()
                    );
                    $this->user_model->simple_insert(USER_ACTIVITY, $actArr);
                    $likes--;
                    $dataArr = array('likes' => $likes);
                    $condition = array('seller_product_id' => $tid);
                    $this->user_model->update_details($productTable, $dataArr, $condition);
                    $totalUserLikes = $this->data['userDetails']->row()->likes;
                    $totalUserLikes--;
                    $this->user_model->update_details(USERS, array('likes' => $totalUserLikes), array('id' => $this->checkLogin('U')));
                    $returnStr['status_code'] = 1;
                } else {
                    if ($this->lang->line('prod_not_avail') != '')
                        $returnStr['message'] = $this->lang->line('prod_not_avail');
                    else
                        $returnStr['message'] = 'Product not available';
                }
            }else {
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function loadListValues() {
        $returnStr['listCnt'] = '<option value="">--Select--</option>';
        $lid = $this->input->post('lid');
        $lvID = $this->input->post('lvID');
        if ($lid != '') {
            $listValues = $this->product_model->get_all_details(LIST_VALUES, array('list_id' => $lid,'status'=>'1'));
            if ($listValues->num_rows() > 0) {
                foreach ($listValues->result() as $listRow) {
                    $selStr = '';
                    if ($listRow->id == $lvID) {
                        $selStr = 'selected="selected"';
                    }
                    $returnStr['listCnt'] .= '<option ' . $selStr . ' value="' . $listRow->id . '">' . $listRow->list_value . '</option>';
                }
            }
        }
        echo json_encode($returnStr);
    }

    public function approve_comment() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') != '') {
            $cid = $this->input->post('cid');
            $this->product_model->update_details(PRODUCT_COMMENTS, array('status' => 'Active'), array('id' => $cid));
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $createdTime = mdate($datestring, $time);
            $product_id = $this->input->post('tid');
            $user_id = $this->input->post('uid');
            $this->product_model->commonDelete(NOTIFICATIONS, array('comment_id' => $cid));
            $actArr = array(
                'activity' => 'comment',
                'activity_id' => $product_id,
                'user_id' => $user_id,
                'activity_ip' => $this->input->ip_address(),
                'comment_id' => $cid,
                'created' => $createdTime
            );
            $this->product_model->simple_insert(NOTIFICATIONS, $actArr);
            $this->send_comment_noty_mail($product_id, $cid);
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function delete_comment() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') != '') {
            $cid = $this->input->post('cid');
            $this->product_model->commonDelete(PRODUCT_COMMENTS, array('id' => $cid));
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function send_feature_noty_mail($pid = '0') {
        if ($pid != '0') {
            $productUserDetails = $this->product_model->get_product_full_details($pid);
            if ($productUserDetails->num_rows() > 0) {
                $emailNoty = explode(',', $productUserDetails->row()->email_notifications);
                if (in_array('featured', $emailNoty)) {
                    if ($productUserDetails->prodmode == 'seller') {
                        $prodLink = base_url() . 'things/' . $productUserDetails->row()->id . '/' . url_title($productUserDetails->row()->product_name, '-');
                    } else {
                        $prodLink = base_url() . 'user/' . $productUserDetails->row()->user_name . '/things/' . $productUserDetails->row()->seller_product_id . '/' . url_title($productUserDetails->row()->product_name, '-');
                    }

                    $newsid = '10';
                    $template_values = $this->product_model->get_newsletter_template_details($newsid);
                    $adminnewstemplateArr = array('logo' => $this->data['logo'], 'meta_title' => $this->config->item('meta_title'), 'full_name' => $productUserDetails->row()->full_name, 'cfull_name' => $this->data['userDetails']->row()->full_name, 'product_name' => $productUserDetails->row()->product_name, 'user_name' => $this->data['userDetails']->row()->user_name);
                    extract($adminnewstemplateArr);
                    $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
                    $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			
			<meta name="viewport" content="width=device-width"/>
			<title>' . $template_values['news_subject'] . '</title><body>';
                    include('./newsletter/registeration' . $newsid . '.php');

                    $message .= '</body>
			</html>';

                    if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                        $sender_email = $this->data['siteContactMail'];
                        $sender_name = $this->data['siteTitle'];
                    } else {
                        $sender_name = $template_values['sender_name'];
                        $sender_email = $template_values['sender_email'];
                    }

                    $email_values = array('mail_type' => 'html',
                        'from_mail_id' => $sender_email,
                        'mail_name' => $sender_name,
                        'to_mail_id' => $productUserDetails->row()->email,
                        'subject_message' => $subject,
                        'body_messages' => $message
                    );
                    $email_send_to_common = $this->product_model->common_email_send($email_values);
                }
            }
        }
    }

    public function contactform() {

        $dataArrVal = array();
        foreach ($this->input->post() as $key => $val) {
            $dataArrVal[$key] = trim(addslashes($val));
        }

        $this->product_model->simple_insert(CONTACTSELLER, $dataArrVal);
        //$contact_id = $this->product_model->get_last_insert_id();
        $this->data['productVal'] = $this->product_model->get_all_details(PRODUCT, array('id' => $this->input->post('product_id')));


        $newimages = array_filter(@explode(',', $this->data['productVal']->row()->image));

        $newsid = '20';
        $template_values = $this->product_model->get_newsletter_template_details($newsid);

        $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
        $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo'],
            'name' => $this->input->post('name'),
            'question' => $this->input->post('question'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'productId' => $this->data['productVal']->row()->id,
            'productName' => $this->data['productVal']->row()->product_name,
            'productSeourl' => $this->data['productVal']->row()->seourl,
            'productImage' => $newimages[0],
        );
        extract($adminnewstemplateArr);



        //$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
        $header .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";

        $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			
			<meta name="viewport" content="width=device-width"/><body>';
        include('./newsletter/registeration' . $newsid . '.php');

        $message .= '</body>
			</html>';

        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->data['siteContactMail'];
            $sender_name = $this->data['siteTitle'];
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }

        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $this->input->post('selleremail'),
            'cc_mail_id' => $this->config->item('site_contact_mail'),
            'subject_message' => $template_values['news_subject'],
            'body_messages' => $message
        );

        $email_send_to_common = $this->product_model->common_email_send($email_values);
        $this->setErrorMessage('success', 'Message Sent Successfully!');
        echo 'Success';
    }

    public function send_comment_noty_mail($pid = '0', $cid = '0') {
        if ($pid != '0' && $cid != '0') {
            $likeUserList = $this->product_model->get_like_user_full_details($pid);
            if ($likeUserList->num_rows() > 0) {
                $productUserDetails = $this->product_model->get_product_full_details($pid);
                $commentDetails = $this->product_model->view_product_comments_details('where c.id=' . $cid);
                if ($productUserDetails->num_rows() > 0 && $commentDetails->num_rows() == 1) {
                    foreach ($likeUserList->result() as $likeUserListRow) {
                        $emailNoty = explode(',', $likeUserListRow->email_notifications);
                        if (in_array('comments_on_fancyd', $emailNoty)) {
                            if ($productUserDetails->prodmode == 'seller') {
                                $prodLink = base_url() . 'things/' . $productUserDetails->row()->id . '/' . url_title($productUserDetails->row()->product_name, '-');
                            } else {
                                $prodLink = base_url() . 'user/' . $productUserDetails->row()->user_name . '/things/' . $productUserDetails->row()->seller_product_id . '/' . url_title($productUserDetails->row()->product_name, '-');
                            }

                            $newsid = '8';
                            $template_values = $this->product_model->get_newsletter_template_details($newsid);
                            $adminnewstemplateArr = array('logo' => $this->data['logo'], 'meta_title' => $this->config->item('meta_title'), 'full_name' => $likeUserListRow->full_name, 'cfull_name' => $commentDetails->row()->full_name, 'user_name' => $commentDetails->row()->user_name, 'product_name' => $productUserDetails->row()->product_name);
                            extract($adminnewstemplateArr);
                            $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
                            $message .= '<!DOCTYPE HTML>
                                <html>
                                <head>
                                
                                <meta name="viewport" content="width=device-width"/>
                                <title>' . $template_values['news_subject'] . '</title><body>';
                            include('./newsletter/registeration' . $newsid . '.php');

                            $message .= '</body>
                                </html>';

                            if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                                $sender_email = $this->data['siteContactMail'];
                                $sender_name = $this->data['siteTitle'];
                            } else {
                                $sender_name = $template_values['sender_name'];
                                $sender_email = $template_values['sender_email'];
                            }

                            $email_values = array('mail_type' => 'html',
                                'from_mail_id' => $sender_email,
                                'mail_name' => $sender_name,
                                'to_mail_id' => $likeUserListRow->email,
                                'subject_message' => $subject,
                                'body_messages' => $message
                            );
                            $email_send_to_common = $this->product_model->common_email_send($email_values);
                        }
                    }
                }
            }
        }
    }

    /**
     * Updates the Affiliate Product SEO / Category / List and Images
     * 
     */
    public function edit_it_affiliate() {
        $mode = $this->uri->segment(4, 0);
        $pid = $this->input->post('PID');
        $nextMode = $this->input->post('nextMode');
        $excludeArr = array('PID', 'nextMode', 'changeorder', 'imaged', 'gateway_tbl_length', 'category_id', 'attribute_name', 'attribute_val');
        if ($mode == '1') {
            $price_range = 0;
            $price = $this->input->post('sale_price');
            if ($price > 0 && $price < 1001) {
                $price_range = '1-1000';
            } else if ($price > 1000 && $price < 2001) {
                $price_range = '1001-2000';
            } else if ($price > 2000 && $price < 5001) {
                $price_range = '2001-5000';
            } else if ($price > 5000 && $price < 10001) {
                $price_range = '5001-10000';
            } else if ($price > 10000) {
                $price_range = '10000+';
            }
            if ($pid == '') {
                $old_product_details = array();
                //$condition = array('product_name' => $product_name);
            } else {
                $old_product_details = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
                //$condition = array('product_name' => $product_name,'seller_product_id !=' => $pid);
            }
            $dataArr = array('seller_product_id' => $pid);
            $checkProduct = $this->product_model->get_all_details(PRODUCT, $dataArr);
            if ($checkProduct->num_rows() == 0) {
                $userProduct = $this->product_model->get_all_details(USER_PRODUCTS, $dataArr);
                if ($userProduct->num_rows() == 1) {
                    $dataArr['image'] = $userProduct->row()->image;
                    $dataArr['seourl'] = url_title($this->input->post('product_name'), '-');
                    $dataArr['user_id'] = $userProduct->row()->user_id;
                    // $dataArr['price_range'] =	$price_range;
                    $dataArr['category_id'] = $userProduct->row()->category_id;
                    $this->product_model->commonInsertUpdate(USER_PRODUCTS, 'update', $excludeArr, $dataArr, array('seller_product_id' => $pid));
                    $product_id = $this->product_model->get_last_insert_id();
                    $this->update_price_range_in_table('add', $price_range, $product_id, $old_product_details);
                    //$this->product_model->commonDelete(USER_PRODUCTS,array('seller_product_id'=>$pid));
                    $this->setErrorMessage('success', 'Yeah ! changes have been saved');
                    $addedProd = $this->session->userdata('prodID');
                    if ($addedProd == '') {
                        $addedProd = array();
                    }
                    array_push($addedProd, $pid);
                    $this->session->set_userdata('prodID', $addedProd);
                    redirect('things/' . $pid . '/edit/' . $nextMode);
                }
            } else {
                $dataArr['seourl'] = url_title($this->input->post('product_name'), '-');
                $dataArr['price_range'] = $price_range;
                $this->product_model->commonInsertUpdate(USER_PRODUCTS, 'update', $excludeArr, $dataArr, array('seller_product_id' => $pid));
                $this->update_price_range_in_table('edit', $price_range, $old_product_details->row()->id, $old_product_details);
                $this->setErrorMessage('success', 'Yeah ! changes have been saved');
                redirect('things/' . $pid . '/edit');
            }
        } else if ($mode == 'seo') {
            $this->product_model->commonInsertUpdate(USER_PRODUCTS, 'update', $excludeArr, array(), array('seller_product_id' => $pid));
            $this->setErrorMessage('success', 'Yeah! changes have been saved');
            redirect('things/' . $pid . '/edit/' . $nextMode);
        } else if ($mode == 'images') {
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
//		    $config['max_size'] = 2000;
            $config['upload_path'] = './images/product';
            $this->load->library('upload', $config);
            //echo "<pre>";print_r($_FILES);die;
            $ImageName = '';
            if ($this->upload->do_multi_upload('product_image')) {
                $logoDetails = $this->upload->get_multi_upload_data();
                foreach ($logoDetails as $fileDetails) {
                    $this->imageResizeWithSpace(800, 600, $fileDetails['file_name'], './images/product/');
                    $ImageName .= $fileDetails['file_name'] . ',';
                }
            }
            $existingImage = $this->input->post('imaged');

            $newPOsitionArr = $this->input->post('changeorder');
            $imagePOsit = array();

            for ($p = 0; $p < sizeof($existingImage); $p++) {
                $imagePOsit[$newPOsitionArr[$p]] = $existingImage[$p];
            }

            ksort($imagePOsit);
            foreach ($imagePOsit as $keysss => $vald) {
                $imgArraypos[] = $vald;
            }
            $imagArraypo0 = @implode(",", $imgArraypos);
            $allImages = $imagArraypo0 . ',' . $ImageName;

            $dataArr = array('image' => $allImages);
            $this->product_model->update_details(USER_PRODUCTS, $dataArr, array('seller_product_id' => $pid));
            $this->setErrorMessage('success', 'Yeah ! changes have been saved');
            redirect('things/' . $pid . '/edit/' . $nextMode);
        } else if ($mode == 'categories') {
            if ($this->input->post('category_id') != '') {
                $category_id = implode(',', $this->input->post('category_id'));
            } else {
                $category_id = '';
            }
            $dataArr = array('category_id' => $category_id);
            $this->product_model->update_details(USER_PRODUCTS, $dataArr, array('seller_product_id' => $pid));
            $this->setErrorMessage('success', 'Yeah ! changes have been saved');
            redirect('things/' . $pid . '/edit/' . $nextMode);
        } else if ($mode == 'list') {
            $list_name_str = $list_val_str = '';
            $list_name_arr = $this->input->post('attribute_name');
            $list_val_arr = $this->input->post('attribute_val');
            if (is_array($list_name_arr) && count($list_name_arr) > 0) {
                $list_name_str = implode(',', $list_name_arr);
                $list_val_str = implode(',', $list_val_arr);
            }
            $dataArr = array('list_name' => $list_name_str, 'list_value' => $list_val_str);
            $this->product_model->update_details(USER_PRODUCTS, $dataArr, array('seller_product_id' => $pid));

            //Update the list table
            if (is_array($list_val_arr)) {
                foreach ($list_val_arr as $list_val_row) {
                    $list_val_details = $this->product_model->get_all_details(LIST_VALUES, array('id' => $list_val_row));
                    if ($list_val_details->num_rows() == 1) {
                        $product_count = $list_val_details->row()->product_count;
                        $products_in_this_list = $list_val_details->row()->products;
                        $products_in_this_list_arr = explode(',', $products_in_this_list);
                        if (!in_array($pid, $products_in_this_list_arr)) {
                            array_push($products_in_this_list_arr, $pid);
                            $product_count++;
                            $list_update_values = array(
                                'products' => implode(',', $products_in_this_list_arr),
                                'product_count' => $product_count
                            );
                            $list_update_condition = array('id' => $list_val_row);
                            $this->product_model->update_details(LIST_VALUES, $list_update_values, $list_update_condition);
                        }
                    }
                }
            }

            $this->setErrorMessage('success', 'Yeah ! changes have been saved');
            redirect('things/' . $pid . '/edit/' . $nextMode);
        } else {
            show_404();
        }
    }

    public function add_product_via_email() {
        $returnStr['status_code'] = 0;
        $returnStr['message'] = '';
        if ($this->checkLogin('U') != '') {

            /*             * *---Update in db---** */
            $userDetails = $this->data['userDetails'];
            $dataArr = array(
                'user_id' => $this->checkLogin('U'),
                'user_name' => $userDetails->row()->user_name,
                'title' => $this->input->post('title'),
                'comment' => $this->input->post('comment')
            );
            $this->product_model->simple_insert(UPLOAD_MAILS, $dataArr);
            /*             * *---Update in db---** */

            /*             * *---Send Mail---** */
            $newsid = '18';
            $template_values = $this->product_model->get_newsletter_template_details($newsid);
            $full_name = $userDetails->row()->full_name;
            if ($full_name == '') {
                $full_name = $userDetails->row()->user_name;
            }
            $thumbnail = '';
            if ($userDetails->row()->thumbnail != '') {
                $thumbnail = '<img width="100px" src="' . base_url() . 'images/users/' . $userDetails->row()->thumbnail . '"/>';
            }
            $adminnewstemplateArr = array(
                'logo' => $this->data['logo'],
                'meta_title' => $this->config->item('meta_title'),
                'user_name' => $userDetails->row()->user_name,
                'title' => $this->input->post('title'),
                'comment' => $this->input->post('comment')
            );
            extract($adminnewstemplateArr);
            $subject = $template_values['news_subject'];
            $message .= '<!DOCTYPE HTML>
                                <html>
                                <head>
                                
                                <meta name="viewport" content="width=device-width"/>
                                <title>' . $template_values['news_subject'] . '</title><body>';
            include('./newsletter/registeration' . $newsid . '.php');

            $message .= '</body>
                                </html>';

            if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                $sender_email = $this->data['siteContactMail'];
                $sender_name = $this->data['siteTitle'];
            } else {
                $sender_name = $template_values['sender_name'];
                $sender_email = $template_values['sender_email'];
            }

            $email_values = array('mail_type' => 'html',
                'from_mail_id' => $sender_email,
                'mail_name' => $sender_name,
                'to_mail_id' => $this->data['siteContactMail'],
                'subject_message' => $subject,
                'body_messages' => $message
            );
            $email_send_to_common = $this->product_model->common_email_send($email_values);
            /*             * *---Send Mail---** */

            $returnStr['status_code'] = 1;
            if ($this->lang->line('ur_msg_sent') != '')
                $returnStr['message'] = $this->lang->line('ur_msg_sent');
            else
                $returnStr['message'] = 'Your message sent';
        }else {
            if ($this->lang->line('login_requ') != '')
                $returnStr['message'] = $this->lang->line('login_requ');
            else
                $returnStr['message'] = 'Login required';
        }
        echo json_encode($returnStr);
    }

    public function ajaxProductAttributeUpdate() {

        $conditons = array('pid' => $this->input->post('pid'));
        $dataArr = array('attr_id' => $this->input->post('attId'), 'attr_name' => $this->input->post('attname'), 'attr_price' => $this->input->post('attprice'));
        $subproductDetails = $this->product_model->edit_subproduct_update($dataArr, $conditons);
    }

    public function remove_attr() {
        if ($this->checkLogin('U') != '') {
            $this->product_model->commonDelete(SUBPRODUCT, array('pid' => $this->input->post('pid')));
        }
    }

    /**
     *
     * Update unique id for products
     */
    /* 	public function qq(){
      $productDetails = $this->product_model->get_all_details(PRODUCT,array());
      foreach ($productDetails->result() as $row){
      $pid = mktime();
      $checkId = $this->product_model->check_product_id($pid);
      while ($checkId->num_rows()>0){
      $pid = mktime();
      $checkId = $this->product_model->check_product_id($pid);
      }
      $this->product_model->update_details(PRODUCT,array('seller_product_id'=>$pid),array('id'=>$row->id));
      echo $row->id.' , ';
      }
      echo 'rows updated';
      }
     */

    public function update_owns() {
        echo 'Updating Own Products<br/><hr/><br/>';
        $user_list = $this->product_model->get_all_details(USERS, array());
        if ($user_list->num_rows() > 0) {
            foreach ($user_list->result() as $user_details) {
                $own_count = 0;
                $own_products = array_filter(explode(',', $user_details->own_products));
                if (count($own_products) > 0) {
                    $id_str = '';
                    foreach ($own_products as $id_row) {
                        $id_str .= $id_row . ',';
                    }
                    $id_str = substr($id_str, 0, -1);
                    $Query = "select `id` from " . PRODUCT . " where `seller_product_id` in ('" . $id_str . "')";
                    $products = $this->product_model->ExecuteQuery($Query);
                    $own_count += $products->num_rows();
                    $Query = "select `id` from " . USER_PRODUCTS . " where `seller_product_id` in ('" . $id_str . "')";
                    $products = $this->product_model->ExecuteQuery($Query);
                    $own_count += $products->num_rows();
                }
                $this->product_model->update_details(USERS, array('own_count' => $own_count), array('id' => $user_details->id));
                echo $user_details->id . '-*--' . $user_details->user_name . '--*-' . $own_count . '<br/>';
            }
        }
        echo 'Complete.!';
    }

    public function qq() {
        $productDetails = $this->product_model->get_all_details(PRODUCT, array());
        foreach ($productDetails->result() as $row) {
            $pid = $this->get_rand_str('6');
            $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $pid));
            while ($checkId->num_rows() > 0) {
                $pid = $this->get_rand_str('6');
                $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $pid));
            }
            $url = base_url() . 'things/' . $row->id . '/' . url_title($row->product_name, '-');
            $this->product_model->simple_insert(SHORTURL, array('short_url' => $pid, 'long_url' => $url));
            $urlid = $this->product_model->get_last_insert_id();
            $this->product_model->update_details(PRODUCT, array('short_url_id' => $urlid), array('seller_product_id' => $row->seller_product_id));
        }
        echo 'Short urls for selling products added<br/>';
        $productDetails = $this->product_model->view_notsell_product_details('');
        foreach ($productDetails->result() as $row) {
            $pid = $this->get_rand_str('6');
            $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $pid));
            while ($checkId->num_rows() > 0) {
                $pid = $this->get_rand_str('6');
                $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $pid));
            }
            $url = base_url() . 'user/' . $row->user_name . '/things/' . $row->seller_product_id . '/' . url_title($row->product_name, '-');
            $this->product_model->simple_insert(SHORTURL, array('short_url' => $pid, 'long_url' => $url));
            $urlid = $this->product_model->get_last_insert_id();
            $this->product_model->update_details(USER_PRODUCTS, array('short_url_id' => $urlid), array('seller_product_id' => $row->seller_product_id));
        }
        echo 'Short urls for affiliate products added';
    }

    public function qq_update_counts() {
        $qryCount = 0;
        $user_list = $this->product_model->get_all_details(USERS, array());
        $qryCount++;
        if ($user_list->num_rows() > 0) {
            foreach ($user_list->result() as $user_list_row) {
                $sell_products = $this->product_model->get_all_details(PRODUCT, array('user_id' => $user_list_row->id));
                $qryCount++;
                $affil_products = $this->product_model->get_all_details(USER_PRODUCTS, array('user_id' => $user_list_row->id));
                $qryCount++;
                $total_products = $sell_products->num_rows() + $affil_products->num_rows();
                if ($total_products != $user_list_row->products) {
                    $this->product_model->update_details(USERS, array('products' => $total_products), array('id' => $user_list_row->id));
                    $qryCount++;
                }
            }
        }
        echo $qryCount++ . ' queries executed.';
    }

    public function check_upload() {
        $url = 'http://hugoboss.scene7.com/is/image/hugoboss/20_hbeu50264459_001_10?$re_productSliderCategory$';
//		$img_data = file_get_contents($image_url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); //Set curl to return the data instead of printing it to the browser.
        $img_data = curl_exec($ch);
        curl_close($ch);
        echo $url . '<br/>';
        echo $img_data;
        die;
        $img_full_name = substr($image_url, strrpos($image_url, '/') + 1);
        $img_name_arr = explode('.', $img_full_name);
        $img_name = $img_name_arr[0];
        $ext = $img_name_arr[1];
        if ($ext == '') {
            $ext = 'jpg';
        }
        $new_name = str_replace(array(',', '&', '<', '>', '$', '(', ')'), '', $img_name . mktime() . '.' . $ext);
        $new_img = 'images/product/' . $new_name;

        file_put_contents($new_img, $img_data);
    }

    public function add_to_whislist() {
        $ip = $_SERVER['REMOTE_ADDR'];
        $date = date("Y-m-d h:i:s");
        $product_id = $this->input->post('product_id');
        $user_id = $this->input->post('user_id');

        $check_exists = $this->product_model->wishlist_exists($product_id, $user_id);
        if ($check_exists->id != '') {
            $remove_whislist = $this->product_model->remove_like($product_id, $user_id);
            $response = false;
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        } else {
            $response = $this->product_model->simple_insert(PRODUCT_LIKES, array('product_id' => $product_id, 'user_id' => $user_id, 'time' => $date, 'ip' => $ip));
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }
    }

    public function remove_wishlist() {
        $product_id = $this->input->post('product_id');
        $response = $this->product_model->remove_like($product_id, $user_id);
        echo json_encode($response);
    }

    public function search_product() {
        $searchValue = $this->input->post('searchValue');
        $result = $this->product_model->search_product($searchValue);
        $cat = $this->product_model->search_cat($searchValue);
        $response = array('product' => $result, 'cat' => $cat);
        header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function OfficeFurniureRequest(){
        $excludeArr = array();
        $dataArr = array();
        $response = $this->product_model->commonInsertUpdate(OFFICE_FURNITURE_ORDERS, 'insert', $excludeArr, $dataArr);
        if($response){
            $this->send_office_furniture_email($this->db->insert_id());
        }
        echo $response;
        
    }

    public function get_furniture_mail(){
        $this->db->select('office_furniture_order_emails');
        $this->db->from('fc_admin_settings');
        $data = $this->db->get();
        return $data->row()->office_furniture_order_emails;
    }

    public function send_office_furniture_email($id){
        $this->db->select('*');
        $this->db->from(OFFICE_FURNITURE_ORDERS);
        $this->db->where('id',$id);
        $bulk_data = $this->db->get();
        $bulkemail = $this->get_furniture_mail();

        $message ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml"><head>
                <meta name="viewport" content="width=device-width"/></head>
                    <head>
                        
                        <title> Office Furniture Order Details</title>
                        <style type="text/css">
                        @media screen and (max-width: 580px) {
                        .tab-container {
                            max-width: 100%;
                        }
                        .txt-pad-15 {
                            padding: 0 15px 51px 15px !important;
                        }
                        .foo-txt {
                            padding: 0px 15px 18px 15px !important;
                        }
                        .foo-add {
                            padding: 15px 15px 0px 15px !important;
                        }
                        .foo-add-left {
                            width: 100% !important;
                        }
                        .foo-add-right {
                            width: 100% !important;
                        }
                        .pad-bottom-15 {
                            padding-bottom: 15px !important;
                        }
                        .pad-20 {
                            padding: 25px 20px 20px !important;
                        }
                        }
                        </style>
                        </head>
                    <body style="margin:0px;">
                        <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="width: 850px;background-color: #fff;margin: 0 auto;table-layout: fixed;background:#dbdbdb;font-family:Arial, Helvetica, sans-serif;font-size:15px;">
                            <tr>
                                <td style="padding:20px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left;width: 100%;padding-bottom:25px;background:#90c5bc;padding:20px;"><a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/email/logo.png" alt="logo"></a></td>
                            </tr>
                        </table>
                        <!-- address table -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                            <tr>
                                <td style="text-align: left;padding:15px 20px;width:100%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Office Furniture Order Details</strong> </p></td>
                            </tr>
                            <tr>
                                <td style="text-align: left;padding:15px 20px;border-top:1px solid #ddd;width:100%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Full Name</strong> <span style="font-weight:400;"> : '.$bulk_data->row()->user_name.'</span> </p>
                                   <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Email</strong> <span style="font-weight:400;"> :  '.$bulk_data->row()->email.'</span> </p>
                                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> 
                                    <strong style="font-weight:600;">Phone</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> :  '.$bulk_data->row()->phone.'</span>   <strong style="font-weight:600;">City</strong> <span style="font-weight:400;display:inline-block;"> :  '.$bulk_data->row()->city.'</span> </p>
                                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Message</strong> <span style="font-weight:400;"> :  '.$bulk_data->row()->remark.'</span> </p></td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                            <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
                                <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
                              <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
                                Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
                            </tr>
                        </table>
                    </body>
                </html>';

        $email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Office Furniture Order',
                         'to_mail_id'=>$bulkemail,
                         'subject_message'=>'Office Furniture Order',
                         'body_messages'=>$message
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);
       // return  $email_send_to_common;
       
    }

    public function script_update_quantity_citywise() {
        $productDetails = $this->product_model->get_all_details(PRODUCT, array('quantity !=' => '0'));

        $datas = '';
        $i=1;
        foreach($productDetails->result_array() as $val) {            

            $this->db->select('id');
            $this->db->from(LIST_VALUES);
            $this->db->where('FIND_IN_SET("'.$val['id'].'", `products`) and `list_id` = "1"');
            $this->db->order_by("list_value", "ASC");
            $datas = $this->db->get()->result_array();

            if(!empty($datas)) {
                $datas = array_column($datas, 'id');
                $arr = array();
                foreach($datas as $data) {
                    $cityCatData = array(
                        'city_id' => $data,
                        'product_id' => $val['id'],
                        'quantity' => $val['quantity']
                    );
                    if($this->db->insert('fc_city_product_quantity',$cityCatData)) {
                        echo "succes-".$i."<br/>";
                    } else {
                        echo "failed-".$i."<br/>";
                    }
                }
            }
            $i++;
        }
    }

    public function script_color_update_product() {
        $this->db->select('id, list_value, list_name');
        $this->db->from('fc_product');
        $this->db->where('FIND_IN_SET("45", `list_value`) and !FIND_IN_SET("49", `list_value`) and !FIND_IN_SET("50", `list_value`)');
        $datas = $this->db->get()->result_array();

        $new_list_name = '';
        $new_list_value = '';
        $i=1;
        foreach($datas as $key => $val) {

            if($val['list_name'] == '') {
                $new_list_name = '1,1';
            } else {
                $new_list_name = $val['list_name'].',1,1';
            }

            if($val['list_value'] == '') {
                $new_list_value = '49,50';
            } else {
                $new_list_value = $val['list_value'].',49,50';
            }

            $pro_data = array('list_name' => $new_list_name, 'list_value' => $new_list_value);

            $this->db->where('id', $val['id']);
            $this->db->update('fc_product', $pro_data);
            echo $this->db->last_query().'<br/>';
            $i++;
        }
    }

    public function script_color_attribute_update() {
        $cities = $this->minicart_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));        

        $datas = '';
        $i=1;
        foreach($cities->result_array() as $key => $val) {

            $this->db->select('id, list_value, list_name');
            $this->db->from('fc_product');
            $datas = $this->db->get();

            $proIds = array();
            foreach($datas->result_array() as $proVal) {
                $proIds[] = $proVal['id'];
                if($i == '1') {
                    $this->db->where('id', $proVal['id']);
                    $this->db->update('fc_product', array('list_value' => '45,46,47,48,49,50', 'list_name' => '1,1,1,1,1,1'));
                }
            }

            $this->db->where('id', $val['id']);
            $this->db->update(LIST_VALUES, array('products' => implode(",",$proIds), 'product_count' => count($proIds)));
            $i++;
        }
    }

}

/*End of file product.php */
/* Location: ./application/controllers/site/product.php */