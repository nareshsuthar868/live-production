<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */

class Order extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form','email','pdf_helper'));
		$this->load->library(array('encrypt','form_validation','curl'));
		$this->load->model('order_model');
		$this->load->model('product_model');
		$this->load->model('user_model');
		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->order_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];
                
		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->order_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];

		$this->data['loginCheck'] = $this->checkLogin('U');
	}


	/**
	 *
	 * Loading Order Page
	 */

	public function index(){
		/*****************************When transection is casnceled**************************/
		if($_POST['TxStatus']=="CANCELED") {
                    
			$this->data['Confirmation'] = 'Failure';
                        $this->data['errors'] =$_POST['TxMsg']; 
                        $this->order_model->send_failed_order_mail();
                        $this->load->view('site/order/order.php',$this->data);
                        //echo "Error";
			
		}
		
		/*****************************When transection is Successfull**************************/
		else{
                        session_start();
			$this->data['Confirmation'] = 'Success';						
			//$this->load->view('site/order/order.php',$this->data);
                        if ($this->data['loginCheck'] != ''){
                            $this->data['heading'] = 'Order Confirmation';
                            if($this->uri->segment(2) == 'giftsuccess'){
				if($this->uri->segment(4)==''){
					$transId = $_REQUEST['txn_id'];
					$Pray_Email = $_REQUEST['payer_email'];
				}else{
					$transId = $this->uri->segment(4);
					$Pray_Email = '';
				}
				$this->data['Confirmation'] = $this->order_model->PaymentGiftSuccess($this->uri->segment(3),$transId,$Pray_Email);
				redirect("order/confirmation/gift");
                            }elseif($this->uri->segment(2) == 'subscribesuccess'){
                                    $transId = $this->uri->segment(4);
                                    $Pray_Email = '';

                                    $this->data['Confirmation'] = $this->order_model->PaymentSubscribeSuccess($this->uri->segment(3),$transId);
                                    redirect("order/confirmation/subscribe");

                            }elseif($this->uri->segment(2) == 'success'){
                                    if($this->uri->segment(5)==''){
                                            $transId = $_REQUEST['txn_id'];
                                            $Pray_Email = $_REQUEST['payer_email'];
                                    }else{
                                            $transId = $this->uri->segment(5);
                                            $Pray_Email = '';
                                    }
                                    $this->data['Confirmation'] = $this->order_model->PaymentSuccess($this->uri->segment(3),$this->uri->segment(4),$transId,$Pray_Email);				
                                    $_SESSION['OrderID'] = $this->uri->segment(4);
                                    $currentSuccessOrders = $this->order_model->getCurrentSuccessOrders($this->uri->segment(3),$this->uri->segment(4));
                                    $_SESSION['OrderAmount'] = $currentSuccessOrders[0]['total'];				
                                    $this->createCaseInZohoCRM($this->uri->segment(3),$this->uri->segment(4));
                                    redirect("order/confirmation/cart");
                                    //$this->load->view('site/order/order.php',$this->data);

                            }elseif($this->uri->segment(2) == 'successgift'){

                                    $transId = 'GIFT'.$this->uri->segment(4);
                                    $Pray_Email = '';
                                    $this->data['Confirmation'] = $this->order_model->PaymentSuccess($this->uri->segment(3),$this->uri->segment(4),$transId,$Pray_Email);
                                    $_SESSION['OrderID'] = $this->uri->segment(4);
                                    $currentSuccessOrders = $this->order_model->getCurrentSuccessOrders($this->uri->segment(3),$this->uri->segment(4));
                                    $_SESSION['OrderAmount'] = $currentSuccessOrders[0]['total'];
                                    $this->createCaseInZohoCRM($this->uri->segment(3),$this->uri->segment(4));
                                    redirect("order/confirmation");

                            }elseif($this->uri->segment(2) == 'failure'){
                                    $this->data['Confirmation'] = 'Failure';
                                    $this->data['errors'] = $this->uri->segment(3);
                                    $this->order_model->send_failed_order_mail("Payment Unsuccessful");
                                    $this->load->view('site/order/order.php',$this->data);
                            }elseif($this->uri->segment(2) == 'notify'){
                                    $this->data['Confirmation'] = 'Failure';
                                    $this->order_model->send_failed_order_mail("Payment Unsuccessful");
                                    $this->load->view('site/order/order.php',$this->data);
                            }elseif($this->uri->segment(2) == 'confirmation'){
                                    $this->data['Confirmation'] = 'Success';				
                                    $this->load->view('site/order/order.php',$this->data);
                            }


		}else{
			redirect('login');
		}
		
		}
	}
	
	
	public function ipnpayment(){

		mysql_query('CREATE TABLE IF NOT EXISTS fc_transactions ( `id` int(255) NOT NULL AUTO_INCREMENT,`payment_cycle` varchar(500) NOT NULL,`txn_type` varchar(500) NOT NULL, `last_name` varchar(500) NOT NULL,`next_payment_date` varchar(500) NOT NULL, `residence_country` varchar(500) NOT NULL, `initial_payment_amount` varchar(500) NOT NULL, `currency_code` varchar(500) NOT NULL, `time_created` varchar(500) NOT NULL, `verify_sign` varchar(750) NOT NULL, `period_type` varchar(500) NOT NULL, `payer_status` varchar(500) NOT NULL, `test_ipn` varchar(500) NOT NULL, `tax` varchar(500) NOT NULL, `payer_email` varchar(500) NOT NULL, `first_name` varchar(500) NOT NULL, `receiver_email` varchar(500) NOT NULL, `payer_id` varchar(500) NOT NULL, `product_type` varchar(500) NOT NULL, `shipping` varchar(500) NOT NULL, `amount_per_cycle` varchar(500) NOT NULL, `profile_status` varchar(500) NOT NULL, `charset` varchar(500) NOT NULL, `notify_version` varchar(500) NOT NULL, `amount` varchar(500) NOT NULL, `outstanding_balance` varchar(500) NOT NULL, `recurring_payment_id` varchar(500) NOT NULL, `product_name` varchar(500) NOT NULL,`custom_values` varchar(500) NOT NULL, `ipn_track_id` varchar(500) NOT NULL, `tran_date` datetime NOT NULL, PRIMARY KEY (`id`) ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3;');

		mysql_query("insert into fc_transactions set  payment_cycle='".$_REQUEST['payment_cycle']."', txn_type='".$_REQUEST['txn_type']."', last_name='".$_REQUEST['last_name']."',
next_payment_date='".$_REQUEST['next_payment_date']."', residence_country='".$_REQUEST['residence_country']."', initial_payment_amount='".$_REQUEST['initial_payment_amount']."',
currency_code='".$_REQUEST['currency_code']."', time_created='".$_REQUEST['time_created']."', verify_sign='".$_REQUEST['verify_sign']."', period_type= '".$_REQUEST['period_type']."', payer_status='".$_REQUEST['payer_status']."', test_ipn='".$_REQUEST['test_ipn']."', tax='".$_REQUEST['tax']."', payer_email='".$_REQUEST['payer_email']."', first_name='".$_REQUEST['first_name']."', receiver_email='".$_REQUEST['receiver_email']."', payer_id='".$_REQUEST['payer_id']."', product_type='".$_REQUEST['product_type']."', shipping='".$_REQUEST['shipping']."', amount_per_cycle='".$_REQUEST['amount_per_cycle']."', profile_status='".$_REQUEST['profile_status']."', charset='".$_REQUEST['charset']."',
notify_version='".$_REQUEST['notify_version']."', amount='".$_REQUEST['amount']."', outstanding_balance='".$_REQUEST['payment_status']."', recurring_payment_id='".$_REQUEST['txn_id']."', product_name='".$_REQUEST['product_name']."', custom_values ='".$_REQUEST['custom']."', ipn_track_id='".$_REQUEST['ipn_track_id']."', tran_date=NOW()");


		$this->data['heading'] = 'Order Confirmation'; 

		if($_REQUEST['payment_status'] == 'Completed'){
			$newcustom = explode('|',$_REQUEST['custom']);
	
			if($newcustom[0]=='Product'){
				$userdata = array('fc_session_user_id' => $newcustom[1],'randomNo' => $newcustom[2]);
				$this->session->set_userdata($userdata);	
				$transId = $_REQUEST['txn_id'];
				$Pray_Email = $_REQUEST['payer_email'];
				$this->data['Confirmation'] = $this->order_model->PaymentSuccess($newcustom[1],$newcustom[2],$transId,$Pray_Email);	
				$this->session->unset_userdata($userdata);
			}elseif($newcustom[0]=='Gift'){
				$userdata = array('fc_session_user_id' => $newcustom[1]);
				$this->session->set_userdata($userdata);
				$transId = $_REQUEST['txn_id'];
				$Pray_Email = $_REQUEST['payer_email'];
				$this->data['Confirmation'] = $this->order_model->PaymentGiftSuccess($newcustom[1],$transId,$Pray_Email);	
				$this->session->unset_userdata($userdata);
			}

		}	
			
	}

	public function insert_product_comment(){
		$uid= $this->checkLogin('U');
		$returnStr['status_code'] = 0;
		$comments = $this->input->post('comments');
		$product_id = $this->input->post('cproduct_id');
		$datestring = "%Y-%m-%d %h:%i:%s";
		$time = time();
		$conditionArr = array(
				'comments'=>$comments,
				'user_id'=>$uid,
				'product_id'=>$product_id,
				'status'=>'InActive',
				'dateAdded'=>mdate($datestring,$time)
		);
		$this->order_model->simple_insert(PRODUCT_COMMENTS,$conditionArr);
		$cmtID = $this->order_model->get_last_insert_id();
		$datestring = "%Y-%m-%d %h:%i:%s";
		$time = time();
		$createdTime = mdate($datestring,$time);
		$actArr = array(
					'activity'		=>	'own-product-comment',
					'activity_id'	=>	$product_id,
					'user_id'		=>	$this->checkLogin('U'),
					'activity_ip'	=>	$this->input->ip_address(),
					'created'		=>	$createdTime,
					'comment_id'	=> $cmtID
		);
		$this->order_model->simple_insert(NOTIFICATIONS,$actArr);
		$this->send_comment_noty_mail($cmtID,$product_id);
		$this->send_comment_noty_mail_to_admin($cmtID,$product_id);
		$returnStr['status_code'] = 1;
		$returnStr['comment_ID'] = $cmtID;
		echo json_encode($returnStr);
	}

	public function send_comment_noty_mail($cmtID='0',$pid='0'){
		if ($this->checkLogin('U')!=''){
			if ($cmtID != '0' && $pid != '0'){
				$productUserDetails = $this->product_model->get_product_full_details($pid);
				if ($productUserDetails->num_rows()==1){
					$emailNoty = explode(',', $productUserDetails->row()->email_notifications);
					if (in_array('comments', $emailNoty)){
						$commentDetails = $this->product_model->view_product_comments_details('where c.id='.$cmtID);
						if ($commentDetails->num_rows() == 1){
							if ($productUserDetails->prodmode == 'seller'){
								$prodLink = base_url().'things/'.$productUserDetails->row()->id.'/'.url_title($productUserDetails->row()->product_name,'-');
							}else {
								$prodLink = base_url().'user/'.$productUserDetails->row()->user_name.'/things/'.$productUserDetails->row()->seller_product_id.'/'.url_title($productUserDetails->row()->product_name,'-');
							}

							$newsid='1';
							$template_values=$this->order_model->get_newsletter_template_details($newsid);
							$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo'],'full_name'=>$commentDetails->row()->full_name,'product_name'=>$productUserDetails->row()->product_name,'user_name'=>$commentDetails->row()->user_name);
							extract($adminnewstemplateArr);
							$subject = $template_values['news_subject'];

							$message .= '<!DOCTYPE HTML>
								<html>
								<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								<meta name="viewport" content="width=device-width"/>
								<title>'.$template_values['news_subject'].'</title>
								<body>';
							include('./newsletter/registeration'.$newsid.'.php');

							$message .= '</body>
								</html>';

							if($template_values['sender_name']=='' && $template_values['sender_email']==''){
								$sender_email=$this->data['siteContactMail'];
								$sender_name=$this->data['siteTitle'];
							}else{
								$sender_name=$template_values['sender_name'];
								$sender_email=$template_values['sender_email'];
							}

							$email_values = array('mail_type'=>'html',
												'from_mail_id'=>$sender_email,
												'mail_name'=>$sender_name,
												'to_mail_id'=>$productUserDetails->row()->email,
												'subject_message'=>$subject,
												'body_messages'=>$message
							);
							$email_send_to_common = $this->product_model->common_email_send($email_values);
						}
					}
				}
			}
		}
	}

	public function send_comment_noty_mail_to_admin($cmtID='0',$pid='0'){
		if ($this->checkLogin('U')!=''){
			if ($cmtID != '0' && $pid != '0'){
				$productUserDetails = $this->product_model->get_product_full_details($pid);
				if ($productUserDetails->num_rows()==1){
					$commentDetails = $this->product_model->view_product_comments_details('where c.id='.$cmtID);
					if ($commentDetails->num_rows() == 1){
						if ($productUserDetails->prodmode == 'seller'){
							$prodLink = base_url().'things/'.$productUserDetails->row()->id.'/'.url_title($productUserDetails->row()->product_name,'-');
						}else {
							$prodLink = base_url().'user/'.$productUserDetails->row()->user_name.'/things/'.$productUserDetails->row()->seller_product_id.'/'.url_title($productUserDetails->row()->product_name,'-');
						}

						$newsid='20';
						$template_values=$this->order_model->get_newsletter_template_details($newsid);
						$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo'],'full_name'=>$commentDetails->row()->full_name,'product_name'=>$productUserDetails->row()->product_name,'user_name'=>$commentDetails->row()->user_name);
						extract($adminnewstemplateArr);
						$subject = $template_values['news_subject'];

						$message .= '<!DOCTYPE HTML>
								<html>
								<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								<meta name="viewport" content="width=device-width"/>
								<title>'.$template_values['news_subject'].'</title>
								<body>';
						include('./newsletter/registeration'.$newsid.'.php');

						$message .= '</body>
								</html>';

						if($template_values['sender_name']=='' && $template_values['sender_email']==''){
							$sender_email=$this->data['siteContactMail'];
							$sender_name=$this->data['siteTitle'];
						}else{
							$sender_name=$template_values['sender_name'];
							$sender_email=$template_values['sender_email'];
						}

						$email_values = array('mail_type'=>'html',
												'from_mail_id'=>$sender_email,
												'mail_name'=>$sender_name,
												'to_mail_id'=>$this->data['siteContactMail'],
												'subject_message'=>$subject,
												'body_messages'=>$message
						);
						$email_send_to_common = $this->product_model->common_email_send($email_values);
					}
				}
			}
		}
	}
	public function createCaseInZohoCRM($userID,$dealCode)
    {
        $this->load->helper('pdf_helper');
        $purchaseList = $this->user_model->get_purchase_list($userID,$dealCode);
	    $invoice = $this->get_invoice($purchaseList);                
		tcpdf();

		$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "PDF Report";
		$obj_pdf->SetTitle($title);
		$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setPrintHeader(false);
		$obj_pdf->AddPage();
		ob_start();
		// we can have any view part here like HTML, PHP etc
		$content = $invoice;
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->lastPage();
		$pdfFileName = $userID."_order_details.pdf";
		$obj_pdf->Output(CASE_PDF_FILE_PATH.$pdfFileName, 'F');

		//exit;
		$currentSuccessOrders = $this->order_model->getCurrentSuccessOrders($userID,$dealCode);
		//echo "<pre>";
		//print_r($currentSuccessOrders);
		//echo "</pre>";

		// check whether already have a contact with the login email address. If not exists, create a new contact in CRM
		$checkContactsURL = "https://crm.zoho.com/crm/private/json/Contacts/searchRecords?authtoken=".CRM_AUTH_TOKEN."&scope=".CRM_SCOPE."&criteria=(Email:".$currentSuccessOrders[0]['email'].")";
		
		$this->curl->create($checkContactsURL);
		$contactsExists = $this->curl->execute();
		$contactsExists_array = json_decode($contactsExists,TRUE);
		
		$accountName = $currentSuccessOrders[0]['full_name'];
		if(array_key_exists("result",$contactsExists_array['response']))
		{
			$contactID = $contactsExists_array['response']['result']['Contacts']['row']['FL'][0]['content'];
			foreach($contactsExists_array['response']['result']['Contacts']['row']['FL'] as $key=>$val)
			{
				if($val['val']=='Account Name')
				{
					$accountName = $val['content'];
				}
				if($val['val']=='CONTACTID')
				{
					$contactID = $val['content'];
				}
			}
		}
		else
		{
			// check whether account exists with the login email address and full name
			
			$checkAccountsURL = "https://crm.zoho.com/crm/private/json/Accounts/getSearchRecordsByPDC?newFormat=1&authtoken=".CRM_AUTH_TOKEN."&scope=".CRM_SCOPE."&selectColumns=All&searchColumn=accountname&searchValue=".$currentSuccessOrders[0]['full_name'];
			$this->curl->create($checkAccountsURL);
			$accountsExists = $this->curl->execute();
			$accountsExists_array = json_decode($accountsExists,TRUE);
			
			$accountID = "";
			$existingEmail="";
		
			if(array_key_exists("result",$accountsExists_array['response']))
			{
			      $accountID = $accountsExists_array['response']['result']['Accounts']['row']['FL'][0]['content'];
			      foreach($accountsExists_array['response']['result']['Accounts']['row']['FL'] as $key=>$val)
			      {
			         if($val['val']=='Email')
			         {
			             $existingEmail = $val['content'];
			         }
			      }
			      if($currentSuccessOrders[0]['email']!=$existingEmail)
			      {
			           $accountName = $currentSuccessOrders[0]['full_name'] . " - " . $currentSuccessOrders[0]['email'];
			           $checkAccounts2URL = "https://crm.zoho.com/crm/private/json/Accounts/getSearchRecordsByPDC?newFormat=1&authtoken=".CRM_AUTH_TOKEN."&scope=".CRM_SCOPE."&selectColumns=All&searchColumn=accountname&searchValue=".$accountName;
			           $this->curl->create($checkAccounts2URL);
			           $accounts2Exists = $this->curl->execute();
			           $accounts2Exists_array = json_decode($accounts2Exists,TRUE);
			           
			           if(array_key_exists("result",$accounts2Exists_array['response']))
			           {
			                $accountID = $accounts2Exists_array['response']['result']['Accounts']['row']['FL'][0]['content'];
			           }
			           else
			           {
			                $accountID = "";
			           }
			      }              
			}
			
			if($accountID=="")
			{
			      // create an account name
			      $xmlDataAccounts = '<?xml version="1.0" encoding="UTF-8"?>';
			      $xmlDataAccounts = $xmlDataAccounts . '<Accounts>';
			      $xmlDataAccounts = $xmlDataAccounts . '<row no="1">';
			      //start of mandatory fields
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Account Name">'.$accountName .'</FL>';	
			      // end of mandatory fields
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Account Owner">'.CASE_OWNER.'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Email">'.$currentSuccessOrders[0]['email'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Phone">'.$currentSuccessOrders[0]['ship_phone'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Billing Street">'.$currentSuccessOrders[0]['ship_address1'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Billing Street2">'.$currentSuccessOrders[0]['ship_address2'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Billing City">'.$currentSuccessOrders[0]['ship_city'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Billing State">'.$currentSuccessOrders[0]['ship_state'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Billing Code">'.$currentSuccessOrders[0]['ship_postal_code'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Billing Country">'.$currentSuccessOrders[0]['ship_country'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Shipping Street">'.$currentSuccessOrders[0]['ship_address1'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Shipping Street2">'.$currentSuccessOrders[0]['ship_address2'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Shipping City">'.$currentSuccessOrders[0]['ship_city'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Shipping State">'.$currentSuccessOrders[0]['ship_state'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Shipping Code">'.$currentSuccessOrders[0]['ship_postal_code'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '<FL val="Shipping Country">'.$currentSuccessOrders[0]['ship_country'].'</FL>';
			      $xmlDataAccounts = $xmlDataAccounts . '</row>';
			      $xmlDataAccounts = $xmlDataAccounts . '</Accounts>';
			      $createAccountURL = "https://crm.zoho.com/crm/private/xml/Accounts/insertRecords";
			
			      $account_data_xml = "newFormat=1&authtoken=".CRM_AUTH_TOKEN."&scope=".CRM_SCOPE."&xmlData=".($xmlDataAccounts);
			      $this->curl->create($createAccountURL);        
			      $this->curl->option(CURLOPT_HEADER, false);
			      $this->curl->option(CURLOPT_RETURNTRANSFER, true);        
			      $this->curl->post($account_data_xml);
			      $createAccountResult = $this->curl->execute();
			
			      
			      $xmlAccountRes = simplexml_load_string($createAccountResult);
			      $jsonAccountRes = json_encode($xmlAccountRes);
			      $createAccountResultArray = json_decode($jsonAccountRes,TRUE);
			      
			      if(array_key_exists("result",$createAccountResultArray))
			      {
			          $accountID = $createAccountResultArray['result']['recorddetail']['FL'][0];
			      }
			      else
			      {
			          $accountID = "";
			      }
			
			      
			}
			//echo "account ID".$accountID;
			//exit;
			// create a contact
			$fullNameArr = explode(" ",$currentSuccessOrders[0]['full_name']);
			if(count($fullNameArr)==3)
			{
				$lastName = $fullNameArr[2];
				$firstName = $fullNameArr[0] . " " . $fullNameArr[1];
			}
			else if(count($fullNameArr)==2)
			{
				$lastName = $fullNameArr[1];
				$firstName = $fullNameArr[0];
			}
			else
			{
				$lastName = $currentSuccessOrders[0]['full_name'];
				$firstName = "";
			}
			$xmlDataContacts = '<?xml version="1.0" encoding="UTF-8"?>';
			$xmlDataContacts = $xmlDataContacts . '<Contacts>';
			$xmlDataContacts = $xmlDataContacts . '<row no="1">';
			//start of mandatory fields
			$xmlDataContacts = $xmlDataContacts . '<FL val="Last Name">'.$lastName.'</FL>';	
			// end of mandatory fields
			$xmlDataContacts = $xmlDataContacts . '<FL val="Contact Owner">'.CASE_OWNER.'</FL>';
			$xmlDataContacts = $xmlDataContacts . '<FL val="Account Name">'.$accountName.'</FL>';
			if($firstName!="")
			{
			     $xmlDataContacts = $xmlDataContacts . '<FL val="First Name">'.$firstName.'</FL>';
			}
			$xmlDataContacts = $xmlDataContacts . '<FL val="Mobile">'.$currentSuccessOrders[0]['ship_phone'].'</FL>';
			$xmlDataContacts = $xmlDataContacts . '<FL val="Email">'.$currentSuccessOrders[0]['email'].'</FL>';
			$xmlDataContacts = $xmlDataContacts . '<FL val="Mailing Street">'.$currentSuccessOrders[0]['ship_address1'].'</FL>';
			$xmlDataContacts = $xmlDataContacts . '<FL val="Mailing Street2">'.$currentSuccessOrders[0]['ship_address2'].'</FL>';
			$xmlDataContacts = $xmlDataContacts . '<FL val="Mailing City">'.$currentSuccessOrders[0]['ship_city'].'</FL>';
			$xmlDataContacts = $xmlDataContacts . '<FL val="Mailing State">'.$currentSuccessOrders[0]['ship_state'].'</FL>';
			$xmlDataContacts = $xmlDataContacts . '<FL val="Mailing Zip">'.$currentSuccessOrders[0]['ship_postal_code'].'</FL>';
			$xmlDataContacts = $xmlDataContacts . '<FL val="Mailing Country">'.$currentSuccessOrders[0]['ship_country'].'</FL>';
			$xmlDataContacts = $xmlDataContacts . '</row>';
			$xmlDataContacts = $xmlDataContacts . '</Contacts>';
			
			$createContactURL = "https://crm.zoho.com/crm/private/xml/Contacts/insertRecords";
			
			$data_xml = "newFormat=1&authtoken=".CRM_AUTH_TOKEN."&scope=".CRM_SCOPE."&xmlData=".($xmlDataContacts);
			$this->curl->create($createContactURL);        
			$this->curl->option(CURLOPT_HEADER, false);
			$this->curl->option(CURLOPT_RETURNTRANSFER, true);        
			$this->curl->post($data_xml);
			$createContactResult = $this->curl->execute();			
			
			$xmlContactRes = simplexml_load_string($createContactResult);
			$jsonContactRes = json_encode($xmlContactRes);
			$createContactResultArray = json_decode($jsonContactRes,TRUE);
			
			if(array_key_exists("result",$createContactResultArray))
			{
			     $contactID = $createContactResultArray['result']['recorddetail']['FL'][0];
			}
			else
			{
			     $contactID = "";
			}		
			
		}
		
		$products = "";
		$deposit = 0;
		$monthlyRent = 0;
                $couponcode = "NA";
		$i=1;
		foreach($currentSuccessOrders as $order)
		{
			$products .= $i . ". " . $order['product_name'] . " - " . $order['quantity'] . " (".$order['attr_name'].")" . "\n";
			//$deposit += $order['shippingcost'];
			//$monthlyRent += ($order['total']-$order['shippingcost']);
			$i++;
		}
		$deposit = $currentSuccessOrders[0]['shippingcost'];
		$monthlyRent = ($currentSuccessOrders[0]['total']-$currentSuccessOrders[0]['shippingcost']);
		//$tenure = $currentSuccessOrders[0]['attr_name'];
		$fullName = $currentSuccessOrders[0]['ship_full_name'];
		$address1 = $currentSuccessOrders[0]['ship_address1'];
		$address2 = $currentSuccessOrders[0]['ship_address2'];
		$city = $currentSuccessOrders[0]['ship_city'];
		$country = $currentSuccessOrders[0]['ship_country'];
		$state = $currentSuccessOrders[0]['ship_state'];
		$zipCode = $currentSuccessOrders[0]['ship_postal_code'];
		$phoneNumber = $currentSuccessOrders[0]['ship_phone'];
                $couponcode = $currentSuccessOrders[0]['couponCode'];


        $description = <<<EOD

Monthly Rent: Rs. $monthlyRent

Deposit: Rs. $deposit

Coupon Used: $couponcode

Product:

$products
Full Name: $fullName
Address: $address1
Address2: $address2
City: $city
Country: $country
State: $state
Zip Code: $zipCode
Phone Number: $phoneNumber

EOD;
		//$description = "test desc";
		$city = $currentSuccessOrders[0]['city'];
		$relatedTo = $contactID;
		$caseOwner = CASE_OWNER;
		
		$xmlData = '<?xml version="1.0" encoding="UTF-8"?>';
		$xmlData = $xmlData . '<Cases>';
		$xmlData = $xmlData . '<row no="1">';
		//start of mandatory fields
		$xmlData = $xmlData . '<FL val="Case Origin">Website</FL>';
		$xmlData = $xmlData . '<FL val="Subject">New Order - '.$currentSuccessOrders[0]['ship_full_name'].' ('.$currentSuccessOrders[0]['email'].')</FL>';
		$xmlData = $xmlData . '<FL val="Status">New Order</FL>';
		// end of mandatory fields
		$xmlData = $xmlData . '<FL val="Case Owner">'.$caseOwner.'</FL>';
		$xmlData = $xmlData . '<FL val="Type">Order</FL>';
		$xmlData = $xmlData . '<FL val="Sub Type">New - Rental</FL>';
		$xmlData = $xmlData . '<FL val="Description">'.$description.'</FL>';
                $xmlData = $xmlData . '<FL val="Coupon Code">'.$couponcode.'</FL>';
		$xmlData = $xmlData . '<FL val="WHOID">'.$relatedTo.'</FL>';
		$xmlData = $xmlData . '<FL val="City">'.$currentSuccessOrders[0]['ship_city'].'</FL>';
		$xmlData = $xmlData . '<FL val="Account Name">'.$accountName.'</FL>';
		$xmlData = $xmlData . '<FL val="LOB">B2C</FL>';
		$xmlData = $xmlData . '<FL val="Email">'.$currentSuccessOrders[0]['email'].'</FL>';
		$xmlData = $xmlData . '<FL val="Phone">'.$currentSuccessOrders[0]['ship_phone'].'</FL>';
		$xmlData = $xmlData . '</row>';
		$xmlData = $xmlData . '</Cases>';
		//echo "<br>xml Data".htmlentities($xmlData);
		$createCaseURL = "https://crm.zoho.com/crm/private/xml/Cases/insertRecords";
		$cases_data_xml = "newFormat=1&authtoken=".CRM_AUTH_TOKEN."&scope=".CRM_SCOPE."&xmlData=".($xmlData);
		$this->curl->create($createCaseURL);        
		$this->curl->option(CURLOPT_HEADER, false);
		$this->curl->option(CURLOPT_RETURNTRANSFER, true);        
		$this->curl->post($cases_data_xml);
		$createCaseResult = $this->curl->execute();		
		$xmlCaseRes = simplexml_load_string($createCaseResult);
		$jsonCaseRes = json_encode($xmlCaseRes);
		$createCaseResultArray = json_decode($jsonCaseRes,TRUE);
		/*echo "<pre>";
		print_r($createCaseResultArray);
		echo "</pre>";	*/
		if(array_key_exists("result",$createCaseResultArray))
		{
		    $caseID = $createCaseResultArray['result']['recorddetail']['FL'][0];
			$caseRecordId=$caseID;
			$ch=curl_init();
			curl_setopt($ch,CURLOPT_HEADER,0);
			curl_setopt($ch,CURLOPT_VERBOSE,0);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			//curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
			curl_setopt($ch,CURLOPT_URL,"https://crm.zoho.com/crm/private/xml/Cases/uploadFile?authtoken=".CRM_AUTH_TOKEN."&scope=".CRM_SCOPE);
			curl_setopt($ch,CURLOPT_POST,true);

			$content = curl_file_create(CASE_PDF_FILE_PATH.$pdfFileName,"application/pdf",CASE_PDF_FILE_PATH."order_details.pdf");
			//$content = "@".CASE_PDF_FILE_PATH.$pdfFileName;
			$post=array("id"=>$caseRecordId,"content"=>$content);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$post);
			$response=curl_exec($ch);
			//$this->load->helper("file");
			unlink(CASE_PDF_FILE_PATH.$pdfFileName);
		}
		else
		{
			$caseID = "";
		}	
    }
    public function get_invoice($PrdList)
	{
        $redoliveAdd = $this->user_model->get_all_details(USERS, array( 'full_name' => 'RedOlive'));
		$shipAddRess = $this->user_model->get_all_details(SHIPPING_ADDRESS,array( 'id' => $PrdList->row()->shippingid ));
		if($shipAddRess->row()->address2!="")
		{
			$shipAddress2 = stripslashes($shipAddRess->row()->address2) . ", ";
		}
		else
		{
			$shipAddress2 = "";
		}
		$disTotal =0; 
		$grantTotal = 0;
		$products = "";
		foreach ($PrdList->result() as $cartRow) 
		{ 
			$InvImg = @explode(',',$cartRow->image);
			$unitPrice = ($cartRow->price*(0.01*$cartRow->product_tax_cost))+$cartRow->price;
			$unitDeposit =  $cartRow->product_shipping_cost;
			$grandDeposit = $grandDeposit + $unitDeposit;
			$uTot = ($unitPrice + $unitDeposit)*$cartRow->quantity;
			if($cartRow->attr_name != '' || $cartRow->attr_type != '')
			{ 
			  $atr = '<br>'.$cartRow->attr_type.' / '.$cartRow->attr_name; 
			}
			else
			{ 
			  $atr = '';
			}
			$products.='<tr>
					<td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;"><img src="'.base_url().PRODUCTPATH.$InvImg[0].'" alt="'.stripslashes($cartRow->product_name).'" width="70" /></span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.stripslashes($cartRow->sku).'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style=" font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.stripslashes($cartRow->product_name).$atr.'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.strtoupper($cartRow->quantity).'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($unitDeposit,2,'.','').'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($unitPrice,2,'.','').'</span></td>
					<td style="text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($uTot,2,'.','').'</span></td>
				</tr>';
			$grantTotal = $grantTotal + $uTot;
		}
		$private_total = $grantTotal - $PrdList->row()->discountAmount;
		$private_total = $private_total + $PrdList->row()->tax;

		if($PrdList->row()->note !='')
		{
			$note ='<tr><td colspan="2">
						   <table cellpadding="3">
							   <tr>
								  <td><span>Note:</span></td>               
							   </tr>
					   <tr>
								  <td style="border:1px solid #cecece;"><span>'.stripslashes($PrdList->row()->note).'</span></td>
							   </tr>
							</table>
						 </td>
					 </tr>
					 <tr><td colspan="2">&nbsp;</td></tr>';
		}
		else
		{
			 $note = '';
		}
		if($PrdList->row()->order_gift == 1)
		{
			$gift ='<tr><td colspan="2">
					 <table cellpadding="3">
						<tr>
						   <td style="border:1px solid #cecece;"><span style="font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; color:#000000; margin:10px;">This Order is a gift</span></td>
						</tr>
					 </table></td></tr>
				   <tr><td colspan="2">&nbsp;</td></tr>';
		}
		else
		{
			 $gift = '';
		}

		$message = '
<html>
<head>
<style>
 .table1 {
    
    border: 1px solid black;
}
 .items th{
    background-color: #f3f3f3;
    border: 1px solid #cecece;
}
 .items td{
border: 1px solid #cecece;
}
</style>
</head>
<body>
<table class="table1" cellpadding="3">
<tr>
   <td width="30%" height="43px;"><img src="'.base_url().'images/logo/cityfurnish-transparent.png" alt="'.$this->data['WebsiteTitle'].'" title="'.$this->data['WebsiteTitle'].'" style="height:43px;"></td>
   <td width="70%" height="80px;">
       <table style="font-size:12px;" cellpadding="3">
        	<tr>
        		<td align="right">Merchant Name :&nbsp;&nbsp;</td>
        		<td align="left">'.stripslashes($PrdList->row()->full_name).'</td>
        	</tr>
        	<tr>
        		<td align="right">TIN / VAT / CST No. :&nbsp;&nbsp;</td>
        		<td align="left">'.stripslashes($PrdList->row()->s_tin_no).'/'.stripslashes($PrdList->row()->s_vat_no).'/'.stripslashes($PrdList->row()->s_cst_no).'</td>
        	</tr>
        	
    	</table>
   </td>
</tr>
<tr style="border-bottom:solid 1px;"><td colspan="2"><hr></td></tr>
<tr>
   <td width="50%"></td>
   <td width="50%" align="right">
        <table width="100%" style="border:1px solid #cecece;font-size:11px;" cellspacing="3" cellpadding="3">
	    <tr bgcolor="#f3f3f3">
            	<td style="border-right:1px solid #cecece;" width="30%"><b>Order Id</b></td>
	        <td width="70%"><span>#'.$PrdList->row()->dealCodeNumber.'</span></td>
	    </tr>
	    <tr bgcolor="#f3f3f3">
                <td style="border-right:1px solid #cecece;" width="30%"><b>Order Date</b></td>
                <td width="70%"><span>'.date("F j, Y g:i a",strtotime($PrdList->row()->created)).'</span></td>
            </tr>	 
        </table>
   </td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
   <td>
        <table style="font-size:11px;" cellpadding="5">
    	   <tr><td colspan="2" style="background-color:#f3f3f3;border:1px solid #cecece;">Delivery Address</td></tr>
        </table>
        <table style="border:1px solid #cecece;font-size:11px;" cellpadding="3">
           <tr>
               <td width="40%">Full Name</td>
               <td width="10%">:</td>
               <td width="50%" align="left">'.stripslashes($shipAddRess->row()->full_name).'</td>
           </tr>
           <tr>
               <td width="40%">Address</td>
               <td width="10%">:</td>
               <td width="50%" align="left">'.stripslashes($shipAddRess->row()->address1).', '.$shipAddress2.stripslashes($shipAddRess->row()->city).', '.stripslashes($shipAddRess->row()->state).', '.stripslashes($shipAddRess->row()->postal_code).', '.stripslashes($shipAddRess->row()->country).'</td>
           </tr>
           <tr>
               <td width="40%">Phone Number</td>
               <td width="10%">:</td>
               <td width="50%" align="left">'.stripslashes($shipAddRess->row()->phone).'</td>
           </tr>            	
        </table>
   </td>
   <td>
        <table style="font-size:11px;" cellpadding="5">
    	   <tr><td colspan="2" style="background-color:#f3f3f3;border: 1px solid #cecece;">From :</td></tr>
        </table>
        <table style="border:1px solid #cecece;font-size:11px;" cellpadding="3">
           <tr>
               <td width="30%">Full Name</td>
               <td width="5%">:</td>
               <td width="65%" align="left">'.stripslashes($PrdList->row()->full_name).'</td>
           </tr>
           <tr>
               <td width="30%">Address</td>
               <td width="5%">:</td>
               <td width="65%" align="left">'.stripslashes($PrdList->row()->address).','.stripslashes($PrdList->row()->address2).','.stripslashes($PrdList->row()->city).','.stripslashes($PrdList->row()->state).','.stripslashes($PrdList->row()->postal_code).','.stripslashes($PrdList->row()->country).'</td>
           </tr>                      	
        </table>
   </td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
   <td colspan="2">
        <table class="items" cellpadding="5" style="font-size:9px;border: 1px solid #cecece;">
           <tr style="font-size:10px;">
               <th width="20%">Product Image</th>
               <th width="12.25%">SKU Code</th>
               <th width="23%">Product Name</th>
               <th width="7%">Qty</th>
               <th width="12.25%">Deposit</th>
               <th width="12.25%">Rental</th>
               <th width="12.25%">Sub Total</th>
           </tr>
           '.$products.'
        </table>
   </td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
'.$note.'
'.$gift.'
<tr>
   <td width="40%">&nbsp;</td>
   <td width="60%">
     <table class="items" style="border:1px solid #cecece;" cellpadding="5">
	<tr bgcolor="#f3f3f3">
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Sub Total</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($grantTotal,'2','.','').'</span></td>
	</tr>
	<tr>
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Discount Amount</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($PrdList->row()->discountAmount,'2','.','').'</span></td>
	</tr>
	<tr bgcolor="#f3f3f3">
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Deposit</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($grandDeposit,2,'.','').'</span></td>
	</tr>
	<tr>
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Shipping Tax</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($PrdList->row()->tax ,2,'.','').'</span></td>
	</tr>
	<tr bgcolor="#f3f3f3">
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Grand Total</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($private_total,'2','.','').'</span></td>
	</tr>
	<tr>
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Amount Paid</b></td>
		<td style="border-bottom:1px solid #cecece;"><b>'.$this->data['currencySymbol'].number_format($private_total,'2','.','').'**</b></td>
	</tr>
      </table>
   </td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2" align="center"><p>Have a question? Our customer service is here to help you - +91 8010845000</p></td></tr>

</table>
</body></html>';
        return $message;
	}

}
