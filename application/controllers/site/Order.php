<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */

class Order extends MY_Controller {
	function __construct(){    
		parent::__construct();
		$this->load->helper(array('cookie','date','form','email','pdf_helper'));
		$this->load->library(array('encrypt','form_validation','curl'));
		$this->load->model('order_model');
		$this->load->model('product_model');
		$this->load->model('user_model');
		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->order_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];
                
		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->order_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];

		$this->data['loginCheck'] = $this->checkLogin('U');
	}


	/**
	 *
	 * Loading Order Page
	 */

	public function index(){
		/*****************************When transection is casnceled**************************/
		if($_POST['TxStatus']=="CANCELED") {
                    
			$this->data['Confirmation'] = 'Failure';
			$this->data['errors'] =$_POST['TxMsg']; 

			//Code added By Manish Soni - 2/9/2019 - Start
			$payment_data =  $this->order_model->get_faild_order($this->data['common_user_id']);

			$this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
			$this->db->from(PAYMENT . ' as p');
			$this->db->join(USERS . ' as u', 'p.user_id = u.id');
			$this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
			$this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
			$this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
			$this->db->where('p.user_id = "' . $this->data['common_user_id'] . '" and p.dealCodeNumber="' . $payment_data->dealCodeNumber . '"');
			$PrdList = $this->db->get();

			$this->db->select('p.sell_id,p.couponCode,u.email');
			$this->db->from(PAYMENT . ' as p');
			$this->db->join(USERS . ' as u', 'p.sell_id = u.id');
			$this->db->where('p.user_id = "' . $this->data['common_user_id'] . '" and p.dealCodeNumber="' . $payment_data->dealCodeNumber . '"');
			$this->db->group_by("p.sell_id");
			$SellList = $this->db->get();
			$this->order_model->new_failed_order_email($PrdList, $SellList);
			//Code added By Manish Soni - 2/9/2019 - End

			//$this->order_model->send_failed_order_mail();
			$this->load->view('site/order/order.php',$this->data);
			//echo "Error";			
		}
		
		/*****************************When transection is Successfull**************************/
		else{
                        session_start();
			$this->data['Confirmation'] = 'Success';						
			//$this->load->view('site/order/order.php',$this->data);
                        if ($this->data['loginCheck'] != ''){
                            $this->data['heading'] = 'Order Confirmation';
                            if($this->uri->segment(2) == 'giftsuccess'){
				if($this->uri->segment(4)==''){
					$transId = $_REQUEST['txn_id'];
					$Pray_Email = $_REQUEST['payer_email'];
				}else{
					$transId = $this->uri->segment(4);
					$Pray_Email = '';
				}
				$this->data['Confirmation'] = $this->order_model->PaymentGiftSuccess($this->uri->segment(3),$transId,$Pray_Email);
				redirect("order/confirmation/gift");
                            }elseif($this->uri->segment(2) == 'subscribesuccess'){
                                    $transId = $this->uri->segment(4);
                                    $Pray_Email = '';

                                    $this->data['Confirmation'] = $this->order_model->PaymentSubscribeSuccess($this->uri->segment(3),$transId);
                                    redirect("order/confirmation/subscribe");

                            }elseif($this->uri->segment(2) == 'success'){
                                    if($this->uri->segment(5)==''){
                                            $transId = $_REQUEST['txn_id'];
                                            $Pray_Email = $_REQUEST['payer_email'];
                                    }else{
                                            $transId = $this->uri->segment(5);
                                            $Pray_Email = '';
                                    }
                                    
                                    if($this->uri->segment(6)==''){
						                    $transId = $_REQUEST['txnid'];
						                    $Pray_Email = $_REQUEST['email'];
						                    $card_token = $_REQUEST['cardToken'];
						                    $mandate_id = $_REQUEST['mihpayid'];


						            }else{
						                    $transId = $this->uri->segment(6);
						                    $Pray_Email = '';
						                    $card_token = $_REQUEST['cardToken'];
						            }
						            
                                    $this->data['Confirmation'] = $this->order_model->PaymentSuccess($this->uri->segment(3),$this->uri->segment(4),$transId,$Pray_Email,$card_token,$mandate_id);				
                                    $_SESSION['OrderID'] = $this->uri->segment(4);
                                    $currentSuccessOrders = $this->order_model->getCurrentSuccessOrders($this->uri->segment(3),$this->uri->segment(4));
                                    $_SESSION['OrderAmount'] = $currentSuccessOrders[0]['total'];
                                    $_SESSION['user_id'] =  $this->uri->segment(3);
                                    $_SESSION['dealCodeNumber'] =  $this->uri->segment(4);
                                    $_SESSION['OrderUserCart'] =  $currentSuccessOrders;
                                    $this->createCaseInZohoCRM($this->uri->segment(3), $this->uri->segment(4));
                                    // $this->CreateSalesOrderInvoice($this->uri->segment(3), $this->uri->segment(4));


                                    redirect("order/confirmation/cart");
                                    //$this->load->view('site/order/order.php',$this->data);

                            }elseif($this->uri->segment(2) == 'successgift'){

                                    $transId = 'GIFT'.$this->uri->segment(4);
                                    $Pray_Email = '';
                                    $this->data['Confirmation'] = $this->order_model->PaymentSuccess($this->uri->segment(3),$this->uri->segment(4),$transId,$Pray_Email);
                                    $_SESSION['OrderID'] = $this->uri->segment(4);
                                    $currentSuccessOrders = $this->order_model->getCurrentSuccessOrders($this->uri->segment(3),$this->uri->segment(4));
                                    $_SESSION['OrderAmount'] = $currentSuccessOrders[0]['total'];
                                    //$this->createCaseInZohoCRM($this->uri->segment(3),$this->uri->segment(4));
                                    redirect("order/confirmation");

                            }elseif($this->uri->segment(2) == 'failure'){
                                    $this->data['Confirmation'] = 'Failure';
									$this->data['errors'] = $this->uri->segment(3);
									
									//Code added By Manish Soni - 2/9/2019 - Start
									$payment_data =  $this->order_model->get_faild_order($this->data['common_user_id']);

									$this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
									$this->db->from(PAYMENT . ' as p');
									$this->db->join(USERS . ' as u', 'p.user_id = u.id');
									$this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
									$this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
									$this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
									$this->db->where('p.user_id = "' . $this->data['common_user_id'] . '" and p.dealCodeNumber="' . $payment_data->dealCodeNumber . '"');
									$PrdList = $this->db->get();

									$this->db->select('p.sell_id,p.couponCode,u.email');
									$this->db->from(PAYMENT . ' as p');
									$this->db->join(USERS . ' as u', 'p.sell_id = u.id');
									$this->db->where('p.user_id = "' . $this->data['common_user_id'] . '" and p.dealCodeNumber="' . $payment_data->dealCodeNumber . '"');
									$this->db->group_by("p.sell_id");
									$SellList = $this->db->get();
									$this->order_model->new_failed_order_email($PrdList, $SellList);
									//Code added By Manish Soni - 2/9/2019 - End

                                    //$this->order_model->send_failed_order_mail("Payment Unsuccessful");
                                    $this->load->view('site/order/order.php',$this->data);
                            }elseif($this->uri->segment(2) == 'notify'){
									$this->data['Confirmation'] = 'Failure';

									//Code added By Manish Soni - 2/9/2019 - Start
									$payment_data =  $this->order_model->get_faild_order($this->data['common_user_id']);

									$this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
									$this->db->from(PAYMENT . ' as p');
									$this->db->join(USERS . ' as u', 'p.user_id = u.id');
									$this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
									$this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
									$this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
									$this->db->where('p.user_id = "' . $this->data['common_user_id'] . '" and p.dealCodeNumber="' . $payment_data->dealCodeNumber . '"');
									$PrdList = $this->db->get();

									$this->db->select('p.sell_id,p.couponCode,u.email');
									$this->db->from(PAYMENT . ' as p');
									$this->db->join(USERS . ' as u', 'p.sell_id = u.id');
									$this->db->where('p.user_id = "' . $this->data['common_user_id'] . '" and p.dealCodeNumber="' . $payment_data->dealCodeNumber . '"');
									$this->db->group_by("p.sell_id");
									$SellList = $this->db->get();
									$this->order_model->new_failed_order_email($PrdList, $SellList);
									//Code added By Manish Soni - 2/9/2019 - End

                                    //$this->order_model->send_failed_order_mail("Payment Unsuccessful");
                                    $this->load->view('site/order/order.php',$this->data);
                            }elseif($this->uri->segment(2) == 'confirmation'){
                                    $this->data['Confirmation'] = 'Success';
                                    $this->load->view('site/order/order.php',$this->data);
                            }


		}else{
			redirect('login');
		}
		
		}
	}
	
	
	public function ipnpayment(){

		mysql_query('CREATE TABLE IF NOT EXISTS fc_transactions ( `id` int(255) NOT NULL AUTO_INCREMENT,`payment_cycle` varchar(500) NOT NULL,`txn_type` varchar(500) NOT NULL, `last_name` varchar(500) NOT NULL,`next_payment_date` varchar(500) NOT NULL, `residence_country` varchar(500) NOT NULL, `initial_payment_amount` varchar(500) NOT NULL, `currency_code` varchar(500) NOT NULL, `time_created` varchar(500) NOT NULL, `verify_sign` varchar(750) NOT NULL, `period_type` varchar(500) NOT NULL, `payer_status` varchar(500) NOT NULL, `test_ipn` varchar(500) NOT NULL, `tax` varchar(500) NOT NULL, `payer_email` varchar(500) NOT NULL, `first_name` varchar(500) NOT NULL, `receiver_email` varchar(500) NOT NULL, `payer_id` varchar(500) NOT NULL, `product_type` varchar(500) NOT NULL, `shipping` varchar(500) NOT NULL, `amount_per_cycle` varchar(500) NOT NULL, `profile_status` varchar(500) NOT NULL, `charset` varchar(500) NOT NULL, `notify_version` varchar(500) NOT NULL, `amount` varchar(500) NOT NULL, `outstanding_balance` varchar(500) NOT NULL, `recurring_payment_id` varchar(500) NOT NULL, `product_name` varchar(500) NOT NULL,`custom_values` varchar(500) NOT NULL, `ipn_track_id` varchar(500) NOT NULL, `tran_date` datetime NOT NULL, PRIMARY KEY (`id`) ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3;');

		mysql_query("insert into fc_transactions set  payment_cycle='".$_REQUEST['payment_cycle']."', txn_type='".$_REQUEST['txn_type']."', last_name='".$_REQUEST['last_name']."',
next_payment_date='".$_REQUEST['next_payment_date']."', residence_country='".$_REQUEST['residence_country']."', initial_payment_amount='".$_REQUEST['initial_payment_amount']."',
currency_code='".$_REQUEST['currency_code']."', time_created='".$_REQUEST['time_created']."', verify_sign='".$_REQUEST['verify_sign']."', period_type= '".$_REQUEST['period_type']."', payer_status='".$_REQUEST['payer_status']."', test_ipn='".$_REQUEST['test_ipn']."', tax='".$_REQUEST['tax']."', payer_email='".$_REQUEST['payer_email']."', first_name='".$_REQUEST['first_name']."', receiver_email='".$_REQUEST['receiver_email']."', payer_id='".$_REQUEST['payer_id']."', product_type='".$_REQUEST['product_type']."', shipping='".$_REQUEST['shipping']."', amount_per_cycle='".$_REQUEST['amount_per_cycle']."', profile_status='".$_REQUEST['profile_status']."', charset='".$_REQUEST['charset']."',
notify_version='".$_REQUEST['notify_version']."', amount='".$_REQUEST['amount']."', outstanding_balance='".$_REQUEST['payment_status']."', recurring_payment_id='".$_REQUEST['txn_id']."', product_name='".$_REQUEST['product_name']."', custom_values ='".$_REQUEST['custom']."', ipn_track_id='".$_REQUEST['ipn_track_id']."', tran_date=NOW()");


		$this->data['heading'] = 'Order Confirmation'; 

		if($_REQUEST['payment_status'] == 'Completed'){
			$newcustom = explode('|',$_REQUEST['custom']);
	
			if($newcustom[0]=='Product'){
				$userdata = array('fc_session_user_id' => $newcustom[1],'randomNo' => $newcustom[2]);
				$this->session->set_userdata($userdata);	
				$transId = $_REQUEST['txn_id'];
				$Pray_Email = $_REQUEST['payer_email'];
				$this->data['Confirmation'] = $this->order_model->PaymentSuccess($newcustom[1],$newcustom[2],$transId,$Pray_Email);	
				$this->session->unset_userdata($userdata);
			}elseif($newcustom[0]=='Gift'){
				$userdata = array('fc_session_user_id' => $newcustom[1]);
				$this->session->set_userdata($userdata);
				$transId = $_REQUEST['txn_id'];
				$Pray_Email = $_REQUEST['payer_email'];
				$this->data['Confirmation'] = $this->order_model->PaymentGiftSuccess($newcustom[1],$transId,$Pray_Email);	
				$this->session->unset_userdata($userdata);
			}

		}	
			
	}

	public function insert_product_comment(){
		$uid= $this->checkLogin('U');
		$returnStr['status_code'] = 0;
		$comments = $this->input->post('comments');
		$product_id = $this->input->post('cproduct_id');
		$datestring = "%Y-%m-%d %h:%i:%s";
		$time = time();
		$conditionArr = array(
				'comments'=>$comments,
				'user_id'=>$uid,
				'product_id'=>$product_id,
				'status'=>'InActive',
				'dateAdded'=>mdate($datestring,$time)
		);
		$this->order_model->simple_insert(PRODUCT_COMMENTS,$conditionArr);
		$cmtID = $this->order_model->get_last_insert_id();
		$datestring = "%Y-%m-%d %h:%i:%s";
		$time = time();
		$createdTime = mdate($datestring,$time);
		$actArr = array(
					'activity'		=>	'own-product-comment',
					'activity_id'	=>	$product_id,
					'user_id'		=>	$this->checkLogin('U'),
					'activity_ip'	=>	$this->input->ip_address(),
					'created'		=>	$createdTime,
					'comment_id'	=> $cmtID
		);
		$this->order_model->simple_insert(NOTIFICATIONS,$actArr);
		$this->send_comment_noty_mail($cmtID,$product_id);
		$this->send_comment_noty_mail_to_admin($cmtID,$product_id);
		$returnStr['status_code'] = 1;
		$returnStr['comment_ID'] = $cmtID;
		echo json_encode($returnStr);
	}

	public function send_comment_noty_mail($cmtID='0',$pid='0'){
		if ($this->checkLogin('U')!=''){
			if ($cmtID != '0' && $pid != '0'){
				$productUserDetails = $this->product_model->get_product_full_details($pid);
				if ($productUserDetails->num_rows()==1){
					$emailNoty = explode(',', $productUserDetails->row()->email_notifications);
					if (in_array('comments', $emailNoty)){
						$commentDetails = $this->product_model->view_product_comments_details('where c.id='.$cmtID);
						if ($commentDetails->num_rows() == 1){
							if ($productUserDetails->prodmode == 'seller'){
								$prodLink = base_url().'things/'.$productUserDetails->row()->id.'/'.url_title($productUserDetails->row()->product_name,'-');
							}else {
								$prodLink = base_url().'user/'.$productUserDetails->row()->user_name.'/things/'.$productUserDetails->row()->seller_product_id.'/'.url_title($productUserDetails->row()->product_name,'-');
							}

							$newsid='1';
							$template_values=$this->order_model->get_newsletter_template_details($newsid);
							$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo'],'full_name'=>$commentDetails->row()->full_name,'product_name'=>$productUserDetails->row()->product_name,'user_name'=>$commentDetails->row()->user_name);
							extract($adminnewstemplateArr);
							$subject = $template_values['news_subject'];

							$message .= '<!DOCTYPE HTML>
								<html>
								<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								
								<meta name="viewport" content="width=device-width"/>
								<title>'.$template_values['news_subject'].'</title>
								<body>';
							include('./newsletter/registeration'.$newsid.'.php');

							$message .= '</body>
								</html>';

							if($template_values['sender_name']=='' && $template_values['sender_email']==''){
								$sender_email=$this->data['siteContactMail'];
								$sender_name=$this->data['siteTitle'];
							}else{
								$sender_name=$template_values['sender_name'];
								$sender_email=$template_values['sender_email'];
							}

							$email_values = array('mail_type'=>'html',
												'from_mail_id'=>$sender_email,
												'mail_name'=>$sender_name,
												'to_mail_id'=>$productUserDetails->row()->email,
												'subject_message'=>$subject,
												'body_messages'=>$message
							);
							$email_send_to_common = $this->product_model->common_email_send($email_values);
						}
					}
				}
			}
		}
	}

	public function send_comment_noty_mail_to_admin($cmtID='0',$pid='0'){
		if ($this->checkLogin('U')!=''){
			if ($cmtID != '0' && $pid != '0'){
				$productUserDetails = $this->product_model->get_product_full_details($pid);
				if ($productUserDetails->num_rows()==1){
					$commentDetails = $this->product_model->view_product_comments_details('where c.id='.$cmtID);
					if ($commentDetails->num_rows() == 1){
						if ($productUserDetails->prodmode == 'seller'){
							$prodLink = base_url().'things/'.$productUserDetails->row()->id.'/'.url_title($productUserDetails->row()->product_name,'-');
						}else {
							$prodLink = base_url().'user/'.$productUserDetails->row()->user_name.'/things/'.$productUserDetails->row()->seller_product_id.'/'.url_title($productUserDetails->row()->product_name,'-');
						}

						$newsid='20';
						$template_values=$this->order_model->get_newsletter_template_details($newsid);
						$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo'],'full_name'=>$commentDetails->row()->full_name,'product_name'=>$productUserDetails->row()->product_name,'user_name'=>$commentDetails->row()->user_name);
						extract($adminnewstemplateArr);
						$subject = $template_values['news_subject'];

						$message .= '<!DOCTYPE HTML>
								<html>
								<head>
								
								<meta name="viewport" content="width=device-width"/>
								<title>'.$template_values['news_subject'].'</title>
								<body>';
						include('./newsletter/registeration'.$newsid.'.php');

						$message .= '</body>
								</html>';

						if($template_values['sender_name']=='' && $template_values['sender_email']==''){
							$sender_email=$this->data['siteContactMail'];
							$sender_name=$this->data['siteTitle'];
						}else{
							$sender_name=$template_values['sender_name'];
							$sender_email=$template_values['sender_email'];
						}

						$email_values = array('mail_type'=>'html',
												'from_mail_id'=>$sender_email,
												'mail_name'=>$sender_name,
												'to_mail_id'=>$this->data['siteContactMail'],
												'subject_message'=>$subject,
												'body_messages'=>$message
						);
						$email_send_to_common = $this->product_model->common_email_send($email_values);
					}
				}
			}
		}
	}

    public function createCaseInZohoCRM($userID, $dealCode)
    {
        $caseOwner = '649600348';
        $this->load->helper('pdf_helper');
        $purchaseList = $this->user_model->get_purchase_list($userID, $dealCode);
	    $invoice = $this->get_invoice($purchaseList);                
		tcpdf();

		$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "PDF Report";
		$obj_pdf->SetTitle($title);
		$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setPrintHeader(false);
		$obj_pdf->AddPage();
		ob_start();
		// we can have any view part here like HTML, PHP etc
		$content = $invoice;
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->lastPage();
		$pdfFileName = $dealCode."_order_details.pdf";
		$obj_pdf->Output(CASE_PDF_FILE_PATH.$pdfFileName, 'F');

		$currentSuccessOrders = $this->order_model->getCurrentSuccessOrders($userID,$dealCode);
		$checkContactsURL = 'https://www.zohoapis.com/crm/v2/Contacts/search?criteria=(Email:equals:'.$currentSuccessOrders[0]['email'].')';
	    $acceess_token = $this->get_zohaccess_token();
   		$headers = [
   		    'Content-Type:application/json',
            'Authorization:'.$acceess_token
        ];

// 		$headers = [
// 			'Content-Type:application/json',
//     		'Authorization:'.CRM_AUTH_TOKEN
//     	];
		$curl = curl_init($checkContactsURL);
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => $headers
		));
		$contactsExists = curl_exec($curl);
		$contactsExists_array = json_decode($contactsExists);		
		$accountName = $currentSuccessOrders[0]['full_name'];

		    
		if(array_key_exists("data",$contactsExists_array)){
				$contactID = $contactsExists_array->data[0]->id;
				$accountName = $contactsExists_array->data[0]->Account_Name->name;
		}
		else
		{

			$checkAccountsURL = 'https://www.zohoapis.com/crm/v2/Accounts/search?criteria=(Email:equals:'.$order_data[0]['email'].')';
		    $curl = curl_init($checkAccountsURL);
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HTTPHEADER => $headers
			));
			$accountsExists = curl_exec($curl);
		    $accountsExists_array = json_decode($accountsExists);

			$accountID = "";
			$existingEmail="";
			if(array_key_exists("data",$accountsExists_array)){
		  		$existingEmail = $accountsExists_array->data[0]->Email;
		  		if($currentSuccessOrders[0]['email']!=$existingEmail)
		  		{
		           	$accountName = $currentSuccessOrders[0]['full_name'] . " - " . $currentSuccessOrders[0]['email'];
		           	$checkAccounts2URL = 'https://www.zohoapis.com/crm/v2/Accounts/search?criteria=(accountname:equals:'.$accountName.')';
	           	  	$curl = curl_init($checkAccountsURL);
					curl_setopt_array($curl, array(
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_HTTPHEADER => $headers
					));
					$accounts2Exists = curl_exec($curl);
		    		$accounts2Exists_array = json_decode($accounts2Exists);
		           	if(array_key_exists("data",$accounts2Exists_array)){
						$accountID = $accounts2Exists_array->data[0]->id;
		           	}else{
		           		$accountID = "";
		           	}
			    }   
			}
		
			
			if($accountID=="")
			{

			      	$createAccountURL = 'https://www.zohoapis.com/crm/v2/Accounts';	
			        $AccountJsonData =   '{
			            "data" : [
						       	{
						       		"Account_Name":"'.$accountName.'",
						       		"Account_Owner":"'.CASE_OWNER.'",
						       		"Email":"'.$currentSuccessOrders[0]['email'].'",
						       		"Phone":"'.$currentSuccessOrders[0]['ship_phone'].'",
						       		"Billing_Street":"'.$currentSuccessOrders[0]['ship_address1'].'",
						       		"Billing_Street2":"'.$currentSuccessOrders[0]['ship_address2'].'",
						       		"Billing_City":"'.$currentSuccessOrders[0]['ship_city'].'",
						       		"Billing_State":'.$currentSuccessOrders[0]['ship_state'].',
						       		"Billing_Code":"'.$currentSuccessOrders[0]['ship_postal_code'].'",
						       		"Billing_Country":"'.$currentSuccessOrders[0]['ship_country'].'",
						       		"Shipping_Street":"'.$currentSuccessOrders[0]['ship_address1'].'",
						       		"Shipping_Street2":"'.$currentSuccessOrders[0]['ship_address2'].'",
						       		"Shipping_City":"'.$currentSuccessOrders[0]['ship_city'].'",
						       		"Shipping_State": "'.$currentSuccessOrders[0]['ship_state'].'",
						       		"Shipping_Code":"'.$currentSuccessOrders[0]['ship_postal_code'].'",
						       		"Shipping_Country":"'.$currentSuccessOrders[0]['ship_country'].'"
						        }
						    ]
						}';
			      
					    $curl = curl_init($createAccountURL);
							curl_setopt_array($curl, array(
								CURLOPT_POST => 1,
				            	CURLOPT_HTTPHEADER => $headers,
				            	CURLOPT_POSTFIELDS => $AccountJsonData,
				            	CURLOPT_RETURNTRANSFER => true
							));
						$createAccountResult = curl_exec($curl);
				    	$createAccountResultArray = json_decode($createAccountResult);
			      
			      if(array_key_exists("data",$createAccountResultArray))
			      {
			          $accountID = $createAccountResultArray->data[0]->details->id;
			      }
			      else
			      {
			          $accountID = "";
			      }
			
			}
			// create a contact
			$fullNameArr = explode(" ",$currentSuccessOrders[0]['full_name']);
			if(count($fullNameArr)==3)
			{
				$lastName = $fullNameArr[2];
				$firstName = $fullNameArr[0] . " " . $fullNameArr[1];
			}
			else if(count($fullNameArr)==2)
			{
				$lastName = $fullNameArr[1];
				$firstName = $fullNameArr[0];
			}
			else
			{
				$lastName = $currentSuccessOrders[0]['full_name'];
				$firstName = "";
			}

			$createContactURL = 'https://www.zohoapis.com/crm/v2/Contacts';	
			        $ContactJsonData =   '{
			            "data" : [
						       	{
						       		"First_Name":"Mr'.$firstName.'",
						       		"Last_Name":"'.$lastName.'",
						       		"Contact_Owner":"'.CASE_OWNER.'",
						       		"Account_Name":"'.$accountName.'",
						       		"Mobile":"'.$currentSuccessOrders[0]['ship_phone'].'",
						       		"Email":"'.$currentSuccessOrders[0]['email'].'",
						       		"Mailing_Street":"'.$currentSuccessOrders[0]['ship_address1'].'",
						       		"Mailing_Street2":"'.$currentSuccessOrders[0]['ship_address2'].'",
						       		"Mailing_City":"'.$currentSuccessOrders[0]['ship_city'].'",
						       		"Mailing_State":"'.$currentSuccessOrders[0]['ship_state'].'",
						       		"Mailing_Zip":"'.$currentSuccessOrders[0]['ship_postal_code'].'",
						       		"Mailing_Country":"'.$currentSuccessOrders[0]['ship_country'].'"
						        }
						    ]
						}';
			      
					    $curl = curl_init($createContactURL);
							curl_setopt_array($curl, array(
								CURLOPT_POST => 1,
				            	CURLOPT_HTTPHEADER => $headers,
				            	CURLOPT_POSTFIELDS => $ContactJsonData,
				            	CURLOPT_RETURNTRANSFER => true
							));
						$jsonContactRes = curl_exec($curl);
				    	$createContactResultArray = json_decode($jsonContactRes);
			
			if(array_key_exists("data",$createContactResultArray))
			{
			     $contactID = $createContactResultArray->data[0]->details->id;
			}
			else
			{
			    $contactID = "";
			}		
			
		}
		
		$Possible_Values_recurring = '';
		////check order is recurring or not
		if($currentSuccessOrders[0]['recurring_type'] == 'si_on_credit_card'){
			$Possible_Values_recurring  = 'SI On Credit Card';
		}else if($currentSuccessOrders[0]['recurring_type'] == 'enach'){
			$Possible_Values_recurring = 'Enach';
		}else if($currentSuccessOrders[0]['recurring_type'] == 'physical_nach'){
			$Possible_Values_recurring = 'Physical Nach';
		}else if($currentSuccessOrders[0]['recurring_type'] == 'pdc'){
			$Possible_Values_recurring = 'PDC';
		}else if($currentSuccessOrders[0]['recurring_type'] == 'not_opted'){
			$Possible_Values_recurring = 'Not Opted';
		}else{
			$Possible_Values_recurring = 'Not Opted';
		}
		
		$products = "";
		$deposit = 0;
		$monthlyRent = 0;
        $couponcode = "NA";
        $currentSuccessOrders_coupon_code = "NA";
		$i=1;
		$coupon_id = $currentSuccessOrders[0]['coupon_id'];
		foreach($currentSuccessOrders as $order)
		{
			$products .= $i . '. ' . $order['product_name'] . ' - ' . $order['quantity'] . ' ('.$order['attr_name'].')' . '\n';
			$i++;
			if($order['couponCode'] != ''){
				$currentSuccessOrders_coupon_code = $order['couponCode'];
				$coupon_id = $order['coupon_id'];
			}
		}
		$deposit = $currentSuccessOrders[0]['shippingcost'];
		$monthlyRent = ($currentSuccessOrders[0]['total']-$currentSuccessOrders[0]['shippingcost']);
		//$tenure = $currentSuccessOrders[0]['attr_name'];
		$fullName = $currentSuccessOrders[0]['ship_full_name'];
		$address1 = $currentSuccessOrders[0]['ship_address1'];
		$address2 = $currentSuccessOrders[0]['ship_address2'];
		$city = $currentSuccessOrders[0]['ship_city'];
		$country = $currentSuccessOrders[0]['ship_country'];
		$state = $currentSuccessOrders[0]['ship_state'];
		$zipCode = $currentSuccessOrders[0]['ship_postal_code'];
		$phoneNumber = $currentSuccessOrders[0]['ship_phone'];
        $couponcode = $currentSuccessOrders_coupon_code ? $currentSuccessOrders_coupon_code : '';

        $coupon_code_details  = $this->order_model->get_all_details(COUPONCARDS,array('id' => $coupon_id));
    	$monthDescription = '';
        if($coupon_code_details->row()->recurring_invoice_tenure > 0 ){
        	$ongoingAmount = $monthlyRent + ($currentSuccessOrders[0]['discountAmount'] +  $currentSuccessOrders[0]['discount_on_si']);
        	for($i = 1;$i <= $coupon_code_details->row()->recurring_invoice_tenure; $i++){
        		$month = '';
        		if($i == 1){
        			$month = $i.'st';
                    $NewRent = $monthlyRent;
        		}else if($i == 2){
        			$month = $i.'nd';
        		    $NewRent = $monthlyRent + $currentSuccessOrders[0]['discount_on_si'];
        		}else if($i == 3){
        			$month = $i.'rd';
        		}else{
        			$month = $i.'th';
        		}
        		$monthDescription .=  $month.' Month Rent: Rs '. $NewRent .'\n';
        	}
        }else{
        	$ongoingAmount = $monthlyRent  +  $currentSuccessOrders[0]['discount_on_si'];
        }

        $description ='\n Total Amount Paid: Rs. '.($deposit + $monthlyRent).
        '\n Deposit: Rs. '.$deposit.
        '\n Discount: '.$currentSuccessOrders[0]['discountAmount'].
        '\n Discount On SI: '.$currentSuccessOrders[0]['discount_on_si'].
        '\n Coupon Used: '.$couponcode.'\n\n'.
        $monthDescription.
        ' Rent For The Rest Of Mnths: Rs. '.$ongoingAmount  .
        '\n Tenure: '.$currentSuccessOrders[0]['attr_name'].'\n'.
        '\n Product:\n'.$products.
        '\n Full Name: '.$fullName.
        '\n Address: '.$address1.
        '\n Address2: '.$address2.
        '\n City: '.$city.
        '\n Country: '.$country.
        '\n  State: '.$state.
        '\n Zip Code: '.$zipCode.
        '\n Phone Number: '.$phoneNumber;



		$city = $currentSuccessOrders[0]['city'];
		$relatedTo = $contactID;
		$createCaseURL = 'https://www.zohoapis.com/crm/v2/Cases';	
        $CreateCaseJsonData =   '{
        "data" : [
		       	{
	       		"Case_Origin":"Website",
	       		"Subject":"New Order - '.$currentSuccessOrders[0]['ship_full_name'].' ('.$currentSuccessOrders[0]['email'].')",
	       		"Status":"New Order",
	       	    "Sub_Status":"KYC In Progress",
	       		"Owner":"649600348",
	       		"Recurring_Opted":"'.$Possible_Values_recurring.'",
	       		"Mandate_ID":"'.$currentSuccessOrders[0]['mandate_id'].'",
	       		"Order_ID":"'.$currentSuccessOrders[0]['dealCodeNumber'].'",
	       		"Type":"Order",
	       		"Sub_Type":"New - Rental",
	       		"Coupon_Code":"'.$couponcode.'",
	       		"Related_To":"'.$relatedTo.'",
	       		"Description":"'.$description.'",
	       		"City":"'.$currentSuccessOrders[0]['ship_city'].'",
	       		"Account_Name" :"'.$accountName.'",
	       		"LOB":"B2C",
	       		"Email":"'.$currentSuccessOrders[0]['email'].'",
	       		"Phone":"'.$currentSuccessOrders[0]['ship_phone'].'",
	       		"Invoice_Url":"'.base_url().'uploaded/'.$pdfFileName.'",
		        }
		    ]
		}';
	    $curl = curl_init($createCaseURL);
			curl_setopt_array($curl, array(
				CURLOPT_POST => 1,
            	CURLOPT_HTTPHEADER => $headers,
            	CURLOPT_POSTFIELDS => $CreateCaseJsonData,
            	CURLOPT_RETURNTRANSFER => true
			));
		$jsonCaseRes = curl_exec($curl);
    	$createCaseResultArray = json_decode($jsonCaseRes);
    	
    	//SEND SMS
    	$template_values = $this->order_model->get_all_details(ORDER_NOTIFICATION,array('order_sub_status' => 'KYC In Progress'));
		if($template_values->num_rows() > 0){
			$template_values = $template_values->row_array();

			if($template_values['sms_status'] == '1'){
				$searchArray = array("{{CUSTOMER_NAME}}", "{{ORDER_ID}}");
				$replaceArray = array(ucfirst($fullName), '#'.$currentSuccessOrders[0]['dealCodeNumber']);
				$new_sms_msg .=  str_replace($searchArray, $replaceArray ,$template_values['sms_content'] );

				$mobile_number = '91'.$currentSuccessOrders[0]['phone_no'];
		        $fields = [
		            'apikey' => SMS_KEY,
		            'msg' => $new_sms_msg,
		            'sid' => 'CITYFN',
		            'msisdn' => $mobile_number,
		            'fl' => 0,
		            'gwid' => 2
		        ];

		        $this->load->helper('sms');
		    	$response = send_sms($fields);
			}				
		}
	   
	   
		if(!array_key_exists("data",$createCaseResultArray))
		{
			$failed_msg = 'For '.$fullName . ' Zoho Case is not Working<br>'.$products.'<br> Order ID:'.$currentSuccessOrders[0]['dealCodeNumber'].'<br>Order Amount:'.$currentSuccessOrders[0]['total'].'<br> email is '.$currentSuccessOrders[0]['email'];
			$email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Zoho Error Mail',
                         'to_mail_id'=>'naresh.suthar@agileinfoways.com',
                         'subject_message'=>'Zoho Case Error',
                         'body_messages'=>$failed_msg
        				);
        	$email_send_to_common = $this->product_model->common_email_send($email_values);
		}
    }
    public function get_invoice($PrdList)
	{
        $redoliveAdd = $this->user_model->get_all_details(USERS, array( 'full_name' => 'RedOlive'));
		$shipAddRess = $this->user_model->get_all_details(SHIPPING_ADDRESS,array( 'id' => $PrdList->row()->shippingid ));
		if($shipAddRess->row()->address2!="")
		{
			$shipAddress2 = stripslashes($shipAddRess->row()->address2) . ", ";
		}
		else
		{
			$shipAddress2 = "";
		}
		$disTotal =0; 
		$grantTotal = 0;
		$products = "";
		foreach ($PrdList->result() as $cartRow) 
		{ 
			$InvImg = @explode(',',$cartRow->image);
			$unitPrice = ($cartRow->price*(0.01*$cartRow->product_tax_cost))+$cartRow->price;
			$unitDeposit =  $cartRow->product_shipping_cost;
			$grandDeposit = $grandDeposit + $unitDeposit;
			$uTot = ($unitPrice + $unitDeposit)*$cartRow->quantity;
			if($cartRow->attr_name != '' || $cartRow->attr_type != '')
			{ 
			  $atr = '<br>'.$cartRow->attr_type.' / '.$cartRow->attr_name; 
			}
			else
			{ 
			  $atr = '';
			}
			$product_deposite = $cartRow->quantity * $unitDeposit;
			$products.='<tr>
					<td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;"><img src="'.base_url().PRODUCTPATH.$InvImg[0].'" alt="'.stripslashes($cartRow->product_name).'" width="70" /></span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.stripslashes($cartRow->sku).'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style=" font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.stripslashes($cartRow->product_name).$atr.'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.strtoupper($cartRow->quantity).'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($product_deposite,2,'.','').'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($unitPrice,2,'.','').'</span></td>
					<td style="text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($uTot,2,'.','').'</span></td>
				</tr>';
			$grantTotal = $grantTotal + $uTot;
		}
		$private_total = $grantTotal - $PrdList->row()->discountAmount;
		$private_total = $private_total + $PrdList->row()->tax;
		$private_total = $private_total - $PrdList->row()->discount_on_si;

		if($PrdList->row()->note !='')
		{
			$note ='<tr><td colspan="2">
						   <table cellpadding="3">
							   <tr>
								  <td><span>Note:</span></td>               
							   </tr>
					   <tr>
								  <td style="border:1px solid #cecece;"><span>'.stripslashes($PrdList->row()->note).'</span></td>
							   </tr>
							</table>
						 </td>
					 </tr>
					 <tr><td colspan="2">&nbsp;</td></tr>';
		}
		else
		{
			 $note = '';
		}
		if($PrdList->row()->order_gift == 1)
		{
			$gift ='<tr><td colspan="2">
					 <table cellpadding="3">
						<tr>
						   <td style="border:1px solid #cecece;"><span style="font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; color:#000000; margin:10px;">This Order is a gift</span></td>
						</tr>
					 </table></td></tr>
				   <tr><td colspan="2">&nbsp;</td></tr>';
		}
		else
		{
			 $gift = '';
		}

		$message = '
<html>
<head>
<style>
 .table1 {
    
    border: 1px solid black;
}
 .items th{
    background-color: #f3f3f3;
    border: 1px solid #cecece;
}
 .items td{
border: 1px solid #cecece;
}
</style>
</head>
<body>
<table class="table1" cellpadding="3">
<tr>
   <td width="30%" height="43px;"><img src="'.CDN_URL.'/images/logo-stick.png" alt="'.$this->data['WebsiteTitle'].'" title="'.$this->data['WebsiteTitle'].'" style="height:43px;"></td>
   <td width="70%" height="45px;">
       
   </td>
</tr>
<tr style="border-bottom:solid 1px;"><td colspan="2"><hr></td></tr>
<tr>
   <td width="50%"></td>
   <td width="50%" align="right">
        <table width="100%" style="border:1px solid #cecece;font-size:11px;" cellspacing="3" cellpadding="3">
	    <tr bgcolor="#f3f3f3">
            	<td style="border-right:1px solid #cecece;" width="30%"><b>Order Id</b></td>
	        <td width="70%"><span>#'.$PrdList->row()->dealCodeNumber.'</span></td>
	    </tr>
	    <tr bgcolor="#f3f3f3">
                <td style="border-right:1px solid #cecece;" width="30%"><b>Order Date</b></td>
                <td width="70%"><span>'.date("F j, Y g:i a",strtotime($PrdList->row()->created)).'</span></td>
            </tr>	 
        </table>
   </td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
   <td>
        <table style="font-size:11px;" cellpadding="5">
    	   <tr><td colspan="2" style="background-color:#f3f3f3;border:1px solid #cecece;">Delivery Address</td></tr>
        </table>
        <table style="border:1px solid #cecece;font-size:11px;" cellpadding="3">
           <tr>
               <td width="40%">Full Name</td>
               <td width="10%">:</td>
               <td width="50%" align="left">'.stripslashes($shipAddRess->row()->full_name).'</td>
           </tr>
           <tr>
               <td width="40%">Address</td>
               <td width="10%">:</td>
               <td width="50%" align="left">'.stripslashes($shipAddRess->row()->address1).', '.$shipAddress2.stripslashes($shipAddRess->row()->city).', '.stripslashes($shipAddRess->row()->state).', '.stripslashes($shipAddRess->row()->postal_code).', '.stripslashes($shipAddRess->row()->country).'</td>
           </tr>
           <tr>
               <td width="40%">Phone Number</td>
               <td width="10%">:</td>
               <td width="50%" align="left">'.stripslashes($shipAddRess->row()->phone).'</td>
           </tr>            	
        </table>
   </td>
   <td>
        <table style="font-size:11px;" cellpadding="5">
    	   <tr><td colspan="2" style="background-color:#f3f3f3;border: 1px solid #cecece;">From :</td></tr>
        </table>
        <table style="border:1px solid #cecece;font-size:11px;" cellpadding="3">
           <tr>
               <td width="30%">Full Name</td>
               <td width="5%">:</td>
               <td width="65%" align="left">'.stripslashes($PrdList->row()->full_name).'</td>
           </tr>
           <tr>
               <td width="30%">Address</td>
               <td width="5%">:</td>
               <td width="65%" align="left">'.stripslashes($PrdList->row()->address).','.stripslashes($PrdList->row()->address2).','.stripslashes($PrdList->row()->city).','.stripslashes($PrdList->row()->state).','.stripslashes($PrdList->row()->postal_code).','.stripslashes($PrdList->row()->country).'</td>
           </tr>                      	
        </table>
   </td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
   <td colspan="2">
        <table class="items" cellpadding="5" style="font-size:9px;border: 1px solid #cecece;">
           <tr style="font-size:10px;">
               <th width="20%">Product Image</th>
               <th width="12.25%">SKU Code</th>
               <th width="23%">Product Name</th>
               <th width="7%">Qty</th>
               <th width="12.25%">Security Deposit</th>
               <th width="12.25%">Monthly Rental</th>
               <th width="12.25%">Sub Total</th>
           </tr>
           '.$products.'
        </table>
   </td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
'.$note.'
'.$gift.'
<tr>
   <td width="40%">&nbsp;</td>
   <td width="60%">
     <table class="items" style="border:1px solid #cecece;" cellpadding="5">
	<tr bgcolor="#f3f3f3">
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Sub Total</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($grantTotal,'2','.','').'</span></td>
	</tr>
	<tr>
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Discount Amount</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($PrdList->row()->discountAmount,'2','.','').'</span></td>
	</tr>
	<tr>
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Discount For SI Amount</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($PrdList->row()->discount_on_si,'2','.','').'</span></td>
	</tr>

	<tr bgcolor="#f3f3f3">
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Deposit</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($grandDeposit,2,'.','').'</span></td>
	</tr>
	<tr>
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Shipping Tax</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($PrdList->row()->tax ,2,'.','').'</span></td>
	</tr>
	<tr bgcolor="#f3f3f3">
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Grand Total</b></td>
		<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($private_total,'2','.','').'</span></td>
	</tr>
	<tr>
		<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Amount Paid</b></td>
		<td style="border-bottom:1px solid #cecece;"><b>'.$this->data['currencySymbol'].number_format($private_total,'2','.','').'**</b></td>
	</tr>
      </table>
   </td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2" align="center"><p>Have a question? Our customer service is here to help you - +91 8010845000</p></td></tr>

</table>
</body></html>';
        return $message;
	}
	
	
	 public function insert_bulk_order() {
        $bulk_data = $this->input->post();
        $inserted_id = $this->order_model->insert_new_bulk_order($this->input->post());

        if ($inserted_id > 0) {
            $response = $this->order_model->send_bulk_data_email($inserted_id);
        }

        echo json_encode($response);
    }
    
    
    public function zoho_submit(){
        $user_id = $this->input->post('user_id');
        $dealCode = $this->input->post('deal_id');
        $description = $this->input->post('description');
        $submit_type = $this->input->post('type');
        $order_data  =  $this->order_model->getCurrentSuccessOrders($user_id, $dealCode);
        $Possible_Values = '';
        $pick_type = '';
        $pick_reason = '';
        $pick_up_request_date = '';
        $type = "Request";
        $extends_req_tenure = '';
        $order_alteration = '';
        $pickup_request_raised = '';
        $requested_date = '';
        $caseOwner = '649600348';
        $response = true;
        $CaseSubject = $order_data[0]['full_name'].'('.$order_data[0]['email'].')';
        $condition =  array('order_id' => $order_data[0]['dealCodeNumber'], 'request_type' => $submit_type);
        $zoho_response = $this->order_model->Check_zoho_exists($condition);
        if($submit_type == 'cancellation'){
            $description_heading = 'Cancellation Request:';
            $sub_type = 'Cancellation';
            $status = 'Customer Requested';
            $subject = 'Cancellation - '.$CaseSubject;
            $Possible_Values = $this->input->post('Possible_Values');
        }else if($submit_type == 'request_pickup'){
            $description_heading = 'Order Pickup Request:';
            $sub_type = 'PickUp and Refund';
            $status = 'Pickup Pending';
            $pick_up_request_date = $this->input->post('pickup_request_date');
            $pick_type = $this->input->post('Pickup_Request_Type');
            $pick_reason = $this->input->post('pickup_reason');
            $subject = 'PickUp - '.$CaseSubject;
        }else if($submit_type == 'full_extension'){
            $description_heading = 'Extension Request:';
            $sub_type = 'Extension';
            $status = 'Extend Tenure';
            $extends_req_tenure = $this->input->post('tenure');
            $order_alteration = 'No';
            $pickup_request_raised = 'NA';
            $subject = 'Extension - '.$CaseSubject;
        }else if($submit_type == 'repair'){
            $description_heading = 'Repair Request:';
            $sub_type = 'Repair';
            $status = 'Pending';
            $subject = 'Repair - '.$CaseSubject;
            $caseOwner = 'Operation Dept(ops@cityfurnish.com)';
            $requested_date = $this->input->post('requested_date');
        }else if($submit_type == 'replacement'){
            $description_heading = 'Replacement Request:';
            $sub_type = 'Replacement';
            $status = 'Pending';
            $subject = 'Replacement - '.$CaseSubject;
            $requested_date = $this->input->post('requested_date');
        }else if($submit_type == 'upgrade'){
            $description_heading = 'Order Upgrade Request:';
            $sub_type = 'Upgrade';
            $status = 'Pending';
            $subject = 'Upgrade - '.$CaseSubject;
        }else if($submit_type == 'installation'){
            $description_heading = 'Installation Request:';
            $sub_type = 'installation';
            $status = 'Pending';
            $subject = 'installation - '.$CaseSubject;
            $requested_date = $this->input->post('requested_date');
        }else if($submit_type == 'relocation'){
            $description_heading = 'Relocation Request:';
            $sub_type = 'Relocation';
            $status = 'Pending';
            $subject = 'Relocation - '.$CaseSubject;
        }else if($submit_type == 'buy'){
            $description_heading = 'Buy Request:';
            $sub_type = 'Buy';
            $status = 'Pending';
            $subject = 'Buy - '.$CaseSubject;

        }else if($submit_type == 'change_bill_cycle'){
            $description_heading = 'Change Bill Cycle:';
            $sub_type = 'Change Bill Cycle';
            $status = 'Pending';
			$subject = 'Change Bill Cycle - '.$CaseSubject;
			$cycle = true;
			$caseOwner = '647838672';
		}else{
            $new_result = array('status' =>false ,'msg'=>'Request Not Found.');
            header('Content-Type: application/json');
            echo json_encode($new_result);
        }

            $products = "";
            $deposit = 0;
            $monthlyRent = 0;
            $couponcode = "NA";
            $i = 1;
            foreach ($order_data as $order) {
                $products .= $i . ". " . $order['product_name'] . " - " . $order['quantity'] . " (" . $order['attr_name'] . ")" . "\n";
                $i++;
            }
            $deposit = $order_data[0]['shippingcost'];
            $monthlyRent = ( $order_data[0]['total'] - $order_data[0]['shippingcost']);
            $order_id = $order_data[0]['dealCodeNumber'];
            $order_date = $order_data[0]['created'];
            $email = $order_data[0]['email'];
            $total = $order_data[0]['total'];
            $fullName =  $order_data[0]['ship_full_name'];
            $address1 =  $order_data[0]['ship_address1'];
            $address2 =  $order_data[0]['ship_address2'];
            $city =  $order_data[0]['ship_city'];
            $state =  $order_data[0]['ship_state'];
            $zipCode =  $order_data[0]['ship_postal_code'];
            $phoneNumber =  $order_data[0]['ship_phone'];
            $couponcode =  $order_data[0]['couponCode'];
            
            $acceess_token = $this->get_zohaccess_token();
   			$headers = [
                'Authorization:'.$acceess_token
	        ];



        $description = $description_heading.'\n Order ID:  '.$order_id.' \n Order Date: '.$order_date.'\n Email: '.$email.' \n Customer Name: '.$fullName.'\n Order Amount:  '.$total.' \n Description:  '.$description.''.

            $contactID = "";
            $accountName = $order_data[0]['full_name'];
            $checkContactsURL = 'https://www.zohoapis.com/crm/v2/Contacts/search?criteria=(Email:equals:'.$order_data[0]['email'].')';
            $curl = curl_init($checkContactsURL);
        		curl_setopt_array($curl, array(
            		CURLOPT_RETURNTRANSFER => true,
            		CURLOPT_HTTPHEADER => $headers
        		));
        	$result = curl_exec($curl);
	        $contactsExists_array = json_decode($result);
            if (array_key_exists("data", $contactsExists_array)) {
            	$contactID = $contactsExists_array->data[0]->id;
            	$accountName = $contactsExists_array->data[0]->Account_Name->name;
            }
            $checkAccountsURL = 'https://www.zohoapis.com/crm/v2/Accounts/search?criteria=(Email:equals:'.$order_data[0]['email'].')';
            $curl = curl_init($checkAccountsURL);
        		curl_setopt_array($curl, array(
            		CURLOPT_RETURNTRANSFER => true,
            		CURLOPT_HTTPHEADER => $headers
        		));
        	$result = curl_exec($curl);
	        $accountsExists = json_decode($result);
            if (array_key_exists("data", $accountsExists)) {
            	$accountID = $accountsExists->data[0]->id;
            	$accountName = $accountsExists->data[0]->Account_Name;
            }
            
            $createCaseURL = 'https://www.zohoapis.com/crm/v2/Cases';
            $Invoice =   '{
            "data" : [
			       	{
			       		"Priority":"Medium",
			       		"Email":"'.$order_data[0]['email'].'",
			       		"Type":"'.$type.'",
			       		"Sub_Type":"'.$sub_type.'",
			       		"Subject":"'.$subject.'",
			       		"Owner":"'.$caseOwner.'",
			       		"Case_Origin":"Website",
			       		"Status":"'.$status.'",
			       		"Related_To":"'.$contactID.'",
			       		"City":"'.$city.'",
			       		"Account_Name":"'.$accountName.'",
			       		"Description": "'.$description.'",
			       		"Phone":"'.$phoneNumber.'",
			       		"LOB":"B2C",
			       		"Cancellation_Reason":"'.$Possible_Values.'",
			       		"Pick_Type":"'.$pick_type.'",
			       		"Pickup_Reason":"'.$pick_reason.'",
			       		"Pickup_Requested_Date":"'.$pick_up_request_date.'",
			       		"Extn_Req_months":"'.$extends_req_tenure.'",
			       		"Order_Alteration":"'.$order_alteration.'",
			       		"Pickup_Req_Raised":"'.$pickup_request_raised.'",
			       		"Requested_Date":"'.$requested_date.'",
			       		"Align_Bill_Cycle_to_1st_day_of_month":true
			        }
			    ]
			}';
        if($zoho_response->num_rows() > 0){

        	$CheckCrmStatusUrl = 'https://www.zohoapis.com/crm/v2/Cases/'.$zoho_response->row()->zoho_case_id.'';
            $curl = curl_init($CheckCrmStatusUrl);
        		curl_setopt_array($curl, array(
            		CURLOPT_RETURNTRANSFER => true,
            		CURLOPT_HTTPHEADER => $headers
        		));
        	$result = curl_exec($curl);
	        $CheckStatus_Array = json_decode($result);
	       // print_r($CheckStatus_Array);exit;
            if (array_key_exists("data", $CheckStatus_Array)) {
            	  $Zoho_Crm_SubStatus = $CheckStatus_Array->data[0]->Sub_Status;
            	  $Zoho_Crm_Status = $CheckStatus_Array->data[0]->Status;
            }
	        if($Zoho_Crm_Status == 'Closed' && $Zoho_Crm_SubStatus == 'Cancelled'){

                $curl = curl_init($createCaseURL);
        		curl_setopt_array($curl, array(
	            	CURLOPT_POST => 1,
	            	CURLOPT_HTTPHEADER => $headers,
	            	CURLOPT_POSTFIELDS => $Invoice,
	            	CURLOPT_RETURNTRANSFER => true
        		));
	        	$customer_payment = curl_exec($curl);
                $createCaseResultArray = json_decode($customer_payment, TRUE);
                $zoho_case_id = $createCaseResultArray->data[0]->id;
                // $createCaseResultArray['result']['recorddetail']['FL'][0];
                if($zoho_case_id > 0){
                    $zoho_update = $this->order_model->Update_Zoho($zoho_response->row()->zoho_case_id);
                    $data =  array('order_id' => $order_id,'zoho_case_id' => $zoho_case_id, 'request_type' => $submit_type,'created_date' => date('d-m-y'));
                    $Zoho_Insert = $this->order_model->Insert_New_Zoho($data);
                    if($Zoho_Insert){
                        $new_result = array('status' =>$Zoho_Insert ,'msg'=>'We have received your service request. Our team will contact you shortly');
                        header('Content-Type: application/json');
                        echo json_encode($new_result);exit;
                    }
                }
            }
            else{
                $new_submit_type = str_replace('_', ' ', $submit_type);
                $new_result = array('status' =>false ,'msg'=>'We Already Got Request For '.ucfirst($new_submit_type).'');
                header('Content-Type: application/json');
                echo json_encode($new_result);exit;
            }
        }
        else{
    	 	$curl = curl_init($createCaseURL);
        	curl_setopt_array($curl, array(
            	CURLOPT_POST => 1,
            	CURLOPT_HTTPHEADER => $headers,
            	CURLOPT_POSTFIELDS => $Invoice,
            	CURLOPT_RETURNTRANSFER => true
        	));
	        $customer_payment = curl_exec($curl);
	        $respo = json_decode($customer_payment);
            $zoho_case_id = $respo->data[0]->details->id;
            
            if($zoho_case_id > 0){
                $data =  array('order_id' => $order_id,'zoho_case_id' => $zoho_case_id, 'request_type' => $submit_type,'created_date' => date('d-m-y'));

                $Zoho_Insert = $this->order_model->Insert_New_Zoho($data);
                if($Zoho_Insert){
                    $new_result = array('status' =>$Zoho_Insert ,'msg'=>'We have received your service request. Our team will contact you shortly');
                    header('Content-Type: application/json');
                    echo json_encode($new_result);
                }
            }
            else{
                $new_result = array('status' =>false ,'msg'=>'Please Try Again');
                header('Content-Type: application/json');
                echo json_encode($new_result);
            }
        } 

    }

    //zoho books codes
    
    public function CreateSalesOrderInvoice($user_id,$deal_code){
        $product_info = $this->order_model->getCurrentSuccessOrders($user_id, $deal_code);
        $subproducts_quantity = array();   
        $wearhouse_code  = $this->order_model->get_all_details(CITY_WEARHOUSE_CODE,array('id !=' => ''));
        $wareCode = 'BAN';
        foreach ($wearhouse_code->result() as $key => $value) {
        	if($value->city_name == $product_info[0]['shippingcity']){

                    $wareCode = $value->wearhouse_code;
        	}
        }
       
		if($product_info[0]['couponCode'] != ''){
			$couponCode = $product_info[0]['couponCode'];
		}else{
			$couponCode = '';
		}       
        $seller_info = $this->order_model->get_all_details(USERS,array('id' => $product_info[0]['sell_id']));
        $url = "https://books.zoho.com/api/v3/contacts?organization_id=".BOOKS_ORGANIZATION_ID."&email_contains=".$product_info[0]['email']."";
        $headers = [
                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
                    'Authorization:'.BOOKS_AUTH_TOKEN
            ];
        $curl = curl_init($url);
        		curl_setopt_array($curl, array(
            		CURLOPT_RETURNTRANSFER => true,
            		CURLOPT_HTTPHEADER => $headers
        		));
        $result = curl_exec($curl);
        $contact_data =  json_decode($result, TRUE);
        if (array_key_exists("contact_id", $contact_data['contacts'][0])) {
            $contact_id = $contact_data['contacts'][0]['contact_id'];
            $data = array(
	                'authtoken'=>BOOKS_AUTH_TOKEN,
	                'JSONString' => '
	                {
	                    "contact_name": "'.$product_info[0]['full_name'].'",
	                    "contact_type": "customer",
	                    "shipping_address": {
	                        "attention": "Mr.'.$product_info[0]['full_name'].'",
	                        "address": "'.$product_info[0]['ship_address1'].'",
	                        "city": '.$product_info[0]['ship_city'].',
	                        "state": '.$product_info[0]['ship_state'].',
	                        "zip": '.$product_info[0]['ship_postal_code'].',
	                        "country": "India",
	                        "phone": '.$product_info[0]['ship_phone'].',
	                    },
	                      "billing_address": {
			                 "attention": "Mr.'.$product_info[0]['full_name'].'",
	                        "address": "'.$product_info[0]['ship_address1'].'",
	                        "city": "'.$product_info[0]['ship_city'].'",
	                        "state": '.$product_info[0]['ship_state'].',
	                        "zip": '.$product_info[0]['ship_postal_code'].',
	                        "country": "India",
	                        "phone": '.$product_info[0]['ship_phone'].',
			            },
	                    
	                }',
	                "organization_id"=>BOOKS_ORGANIZATION_ID
	            );

					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
					$response = curl_exec($ch);
        }else
            {
	            $Contact_url ="https://books.zoho.com/api/v3/contacts";
	            $Contact_data = array(
	                'authtoken'=>BOOKS_AUTH_TOKEN,
	                'JSONString' => '
	                {
	                    "contact_name": '.$product_info[0]['full_name'].',
	                    "contact_type": "customer",
	                    "shipping_address": {
	                        "attention": "Mr.'.$product_info[0]['full_name'].'",
	                        "address": '.$product_info[0]['ship_address1'].',
	                        "city": '.$product_info[0]['ship_city'].',
	                        "state": '.$product_info[0]['ship_state'].',
	                        "zip": '.$product_info[0]['ship_postal_code'].',
	                        "country": "India",
	                        "phone": '.$product_info[0]['ship_phone'].',
	                    },
	                    "contact_persons": [
	                        {
	                            "salutation": "Mr",
	                            "first_name": '.$product_info[0]['full_name'].',
	                            "email": '.$product_info[0]['email'].',
	                            "phone": '.$product_info[0]['ship_phone'].'
	                        }
	                    ],
	                    
	                }',
	                "organization_id"=>BOOKS_ORGANIZATION_ID
	            );
	        	$curl = curl_init($Contact_url);
	                	curl_setopt_array($curl, array(
	                    	CURLOPT_POST => 1,
	                    	CURLOPT_POSTFIELDS => $Contact_data,
	                    	CURLOPT_RETURNTRANSFER => true
	                	));
	            $Contact_response = curl_exec($curl);
	            $Decode_Contact = json_decode($Contact_response);
	            $contact_id = $Decode_Contact->contact->contact_id;
            }
            foreach ($product_info as $key =>  $cartRow) {
            	if($cartRow['attr_name'] != ''){
            		$tenure  = explode(' ', $cartRow['attr_name']);  
            		$dayx = date("Y-m-d", strtotime("+".$cartRow['attr_name']));
        			$pick_up_date =  date("Y-m-d", strtotime($dayx.' +5 day'));
            	}
            	array_push($subproducts_quantity, unserialize($cartRow['subproduct_quantity']));
	            $InvImg = @explode(',',$cartRow->image);
	            $unitPrice = ($cartRow['price']*(0.01*$cartRow['product_tax_cost']))+$cartRow['price'] * $cartRow['quantity'] ;
	            $unitDeposit =  $cartRow['product_shipping_cost'] * $cartRow['quantity'];
	            $grandDeposit = $grandDeposit + ($unitDeposit*$cartRow['quantity']);
	            $uTot = $unitPrice + $unitDeposit;
                $new_deposite +=  $unitDeposit;
        	}
	        $private_total = $new_deposite - $product_info[0]['discountAmount'];
	        $private_total = $private_total + $product_info[0]['tax'];
        	$itemsarray = array();
        	$counter = 0;
	        foreach ($product_info as $key =>  $cartRow) {
	       		if($cartRow['subproducts'] == ''){
	       			$zoho_item_id = $this->check_sku($cartRow['sku'], $cartRow['product_name'], $cartRow['id'],'1');
	       			if(!array_key_exists('item_id', $zoho_item_id)){
                          break;
	       			}
	       			$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
	    	 		$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
	                $itemsarray[$counter]['quantity'] = $cartRow['quantity'];
	                if($cartRow['attribute_values'] != ''){
	                	$itemsarray[$counter]['rate'] = round($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']) ;
	                	$itemsarray[$counter]['bcy_rate'] = round($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']);
	                	// $decimal_price +=  round(($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']) * $cartRow['quantity']);
	            	}else{
	            		$itemsarray[$counter]['rate'] = $cartRow['price'];
	                	$itemsarray[$counter]['bcy_rate'] = $cartRow['price'];
	                	$decimal_price +=  $cartRow['price'] * $cartRow['quantity'];
	            	}
	               $itemsarray[$counter]['name'] =  $zoho_item_id['item_name'];
                   $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
	                // $cartRow['product_name'];
	                // $itemsarray[$counter]['discount'] = $cartRow['discountAmount'];
		            $counter++;
		       	}else{ 
		   			$product_id = explode(',',$cartRow['subproducts']);
		   			$product_data = $this->order_model->Get_subproducts($product_id);
		   			foreach ($product_data as $packagekey => $packagevalue) {
		   				if($packagevalue->subproducts != '')
		   				{

		   					$subproduct_id = explode(',', $packagevalue->subproducts);
		   					$subproducts_subproduct = $this->order_model->Get_subproducts($subproduct_id);
		   					foreach ($subproducts_subproduct as $key => $value) {
		   						$zoho_item_id = $this->check_sku($value->sku, $value->product_name, $value->id,'2');
				       			if(!array_key_exists('item_id', $zoho_item_id)){
			                          break;
				       			}

				       			$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
				       			if(!$value->is_addon){
				       			    $attr_price = $this->order_model->GetPriceOnTenure($cartRow['attr_name'],$value->id);
				       				$itemsarray[$counter]['rate'] =  round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
		                    		$itemsarray[$counter]['bcy_rate'] =   round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);

				       			}else{
				       				$itemsarray[$counter]['rate'] =  round($value->price - ($value->price / 100) * $cartRow['package_discount']);
		                    		$itemsarray[$counter]['bcy_rate'] = round($value->price - ($value->price / 100) * $cartRow['package_discount']);
				       			}
				       			// $itemsarray[$counter]['item_id'] = $zoho_item_id;
		   						$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
		                		$itemsarray[$counter]['quantity'] = $subproducts_quantity[$key][$packagevalue->id];
		                    	$itemsarray[$counter]['name'] =  $zoho_item_id['item_name'];
		                    	  $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		                    	// $value->product_name;
		   						$counter++;
		   					}
		   				}
		   				else
		   				{
		   					$zoho_item_id = $this->check_sku($packagevalue->sku, $packagevalue->product_name, $packagevalue->id,'3');
				       			if(!array_key_exists('item_id', $zoho_item_id)){
			                          break;
				       			}

				       		$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
				       		if(!$packagevalue->is_addon){
		   						$attr_price = $this->order_model->GetPriceOnTenure($cartRow['attr_name'],$packagevalue->id);
		               	    	$itemsarray[$counter]['rate'] =  round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
		                    	$itemsarray[$counter]['bcy_rate'] =   round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);

		   					}else{
		   						$itemsarray[$counter]['rate'] =  round($packagevalue->price - ($packagevalue->price / 100) * $cartRow['package_discount']);
		                    	$itemsarray[$counter]['bcy_rate'] =   round($packagevalue->price - ($packagevalue->price / 100) * $cartRow['package_discount']);
		   					}
		   					$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
		                	$itemsarray[$counter]['quantity'] = $subproducts_quantity[$key][$packagevalue->id];
		                    $itemsarray[$counter]['name'] =  $zoho_item_id['item_name'];
		                      $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		                    // $packagevalue->product_name;
		                    $counter++;
		   				}						
		                
		   			}

		       	}
			}

	    	$json = json_encode($itemsarray);
	    	$expload_price = explode('.', $decimal_price);
	        if (array_key_exists("contact_id", $contact_data['contacts'][0])) {
	            $contact_id = $contact_data['contacts'][0]['contact_id'];
	        }
	        $sales_order_id = $this->order_model->getAdminSettings();
	        $new_order_id = str_pad(ltrim($sales_order_id->row()->zoho_books_sales_order_id, '0') + 1, 4, '0', STR_PAD_LEFT);
	        $order_date = $product_info[0]['created'];
	        $createDate = strtotime($order_date);
	        $strip = date('Y-m-d', $createDate); 
	        if($product_info[0]['tax'] > 0){
	            $tax = 'true';
	        }else{
	            $tax = 'false';
	        }

          	$customer_notes = 'Tenure: '.$tenure[0].' Place of Supply: ' .$product_info[0]['full_name'].' '.$product_info[0]['ship_city'].' '.$product_info[0]['ship_state'].' '.$product_info[0]['ship_postal_code'].' '.'India Phone: '.' '.$product_info[0]['ship_phone'].'';
	                            // "shipping_charge": '.$new_deposite.',
	        $Curl = "https://books.zoho.com/api/v3/salesorders";
	        $Invoice = array(
	                        'authtoken'=>BOOKS_AUTH_TOKEN,
	                        'JSONString' => '{
                    	        "customer_id": '.$contact_id.',
	                            "date": '.$strip.',    
	                            "line_items": '.$json.',
	                            "notes":"'.$customer_notes.'",
	                            "salesorder_number":"ON-RET-'.$wareCode.'-'.$new_order_id.'",
	                            "status": "open",
	                            "delivery_method": "Company Transport",
	                            "custom_fields": [
	                          		{
	                          			"placeholder": "cf_warehouse_code",
								    	"index": 1,
								    	"value":"'.$wareCode.'" 
	                                },
	                                {
									    "placeholder": "cf_order_type",
									    "index": 2,
									    "value":"Rent"
									},
									{
									    "placeholder": "cf_order_status",
									    "index": 3,
									    "value":"Live"
									},
									{
									    "placeholder": "cf_tenure",
									    "index": 4,
									    "value": '.$tenure[0].'
									},
									{
									    "placeholder": "cf_in",
									    "index": 5,
									    "value":"'.ucfirst($tenure[1]).'"
									},
									{
									    "placeholder": "cf_pickup_date",
									    "index": 6,
									    "value": "'.$pick_up_date.'"
									},
									{
									    "placeholder": "cf_actual_pickup_date",
									    "index": 7,
									    "value": "'.$dayx.'"
									},
									{
									    "placeholder": "cf_offer_given",
									    "index": 9,
									    "value":"'.$couponCode.'"
									   
									},		
	                            ],
	                            "adjustment_description": "Adjustment",
	                            "documents": [
	                                {}
	                            ]
	                        }',
	                        "organization_id"=>BOOKS_ORGANIZATION_ID
	                    );
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $invoice_result = curl_exec($curl); 
	        $decode_result = json_decode($invoice_result);
	        if (isset($decode_result->salesorder)) {
	        	$invoice_id = $decode_result->salesorder->salesorder_id;
	            $status_url ='https://books.zoho.com/api/v3/salesorders/'.$invoice_id.'/status/open?organization_id=82487023';
			    $headers = [
		                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
		                    'Authorization:'.BOOKS_AUTH_TOKEN
				            ];
		        $curl = curl_init($status_url);
		        		curl_setopt_array($curl, array(
		            		CURLOPT_RETURNTRANSFER => true,
		            		CURLOPT_HTTPHEADER => $headers,
		            		CURLOPT_POST => 1,
		        		));
		        $result = curl_exec($curl);
        		$respo =  json_decode($result, TRUE);
					
	      	}
			$dataArr = array('zoho_books_sales_order_id' => $new_order_id);
			$condition  = array('id' => 1);
	        $this->order_model->UpdateAdminSetting($dataArr,$condition);
	        $this->CreateRetainerInvoice($contact_id,$user_id,$deal_code,$invoice_result);

    }



     public function CreateRetainerInvoice($contact_id,$user_id,$deal_code,$invoice){
     	$invoice_data =  json_decode($invoice, TRUE);
     	 	if (array_key_exists("salesorder_number", $invoice_data['salesorder'])) {
	            $reference_number = $invoice_data['salesorder']['salesorder_number'];
	      	}else{
	        	 $reference_number = '';
	    	}
	    	
        $product_info = $this->order_model->getCurrentSuccessOrders($user_id, $deal_code);
        
        foreach ($product_info as $key =>  $cartRow) {
            	if($cartRow['attr_name'] != ''){
            		$tenure  = explode(' ', $cartRow['attr_name']);  
            		$dayx = date("Y-m-d", strtotime("+".$cartRow['attr_name']));
        			$pick_up_date =  date("Y-m-d", strtotime($dayx.' +5 day'));
            	}
        }
	    	
	    $wearhouse_code  = $this->order_model->get_all_details(CITY_WEARHOUSE_CODE,array('id !=' => ''));	
	    foreach ($wearhouse_code->result() as $key => $value) {
        	if($value->city_name == $product_info[0]['shippingcity']){
                    $gst_number = $value->gst_num;
        	}else{
        	    $gst_number = 'MH - 27AAJCM9442J1ZY';
        	}
        }
        // print_r($wearhouse_code);exit;
        $url = "https://books.zoho.com/api/v3/contacts?organization_id=".BOOKS_ORGANIZATION_ID."&email_contains=".$product_info[0]['email']."";
        $headers = [
                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
                    'Authorization:'.BOOKS_AUTH_TOKEN
            ];
        $curl = curl_init($url);
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers
        ));
        $result = curl_exec($curl);
        $contact_data =  json_decode($result, TRUE);
        if (array_key_exists("contact_id", $contact_data['contacts'][0])) {
            $contact_id = $contact_data['contacts'][0]['contact_id'];
        }

                foreach ($product_info as $cartRow) { 
                    $unitPrice = ($cartRow['price']*(0.01*$cartRow['product_tax_cost']))+$cartRow['price'] * $cartRow['quantity'] ;
                    $unitDeposit =  $cartRow['product_shipping_cost'] * $cartRow['quantity'];
                    $grandDeposit = $grandDeposit + ($unitDeposit*$cartRow['quantity']);
                    $uTot = $unitPrice + $unitDeposit;
                	$new_deposite +=  $unitDeposit;
                }

                $itemsarray = array();
                // foreach ($product_info   as $key => $value) {
                    // $Refundable_price =  $value['quantity'] * $value['product_shipping_cost'];
                    $itemsarray['item_order'] = '1';
                    $itemsarray['rate'] =   $new_deposite;
                    // $itemsarray[$key]['quantity'] = $value['quantity'];
                    $itemsarray['description'] =  'Security Deposit';
                // }
                $json = json_encode($itemsarray);
                // print_r($json);exit;
                $order_date = $product_info[0]['created'];
                $createDate = strtotime($order_date);
                $strip = date('Y-m-d', $createDate); 

        $RetainerUrl = "https://books.zoho.com/api/v3/retainerinvoices";
        $RetainerData = array(
                        'authtoken'=>BOOKS_AUTH_TOKEN,
                        'JSONString' => '{
                            "customer_id": '.$contact_id.',
                            "reference_number":"'.$reference_number.'",
                            "notes":  "This Is Your Refundable Money",
                            "terms": "Terms & Conditions apply",
                            "line_items":[
							        {
							            "description": "Security Deposit",
							            "item_order": 1,
							            "rate": '.$new_deposite.'
							        }
							    ],
						     "custom_fields": [
	      						{
								    "placeholder": "cf_gst",
								    "index": 1,
								    "value":"'.$gst_number.'"
								   
								},
	                        ],
                            "payment_options": {
                                "payment_gateways": [
                                    {
                                        "gateway_name": "paypal"
                                    }
                                ]
                            },
                        }',
                        "organization_id"=>BOOKS_ORGANIZATION_ID
                    );
        // print_r($RetainerData);exit;
          $curl = curl_init($RetainerUrl);
            curl_setopt_array($curl, array(
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $RetainerData,
                CURLOPT_RETURNTRANSFER => true
            ));
            $result = curl_exec($curl);
            $respo = json_decode($result);
        if(isset($respo->retainerinvoice)){
 				$Invoice_id = $respo->retainerinvoice->retainerinvoice_id;
	      //       $status_url ='https://books.zoho.com/api/v3/retainerinvoices/'.$Invoice_id.'/status/sent?organization_id=82487023';
			    // $headers = [
		     //                'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
		     //                'Authorization:'.BOOKS_AUTH_TOKEN
				   //          ];
		     //    $curl = curl_init($status_url);
		     //    		curl_setopt_array($curl, array(
		     //        		CURLOPT_RETURNTRANSFER => true,
		     //        		CURLOPT_HTTPHEADER => $headers,
		     //        		CURLOPT_POST => 1,
		     //    		));
		     //    $result = curl_exec($curl);
       //  		$respo =  json_decode($result, TRUE);
        // 		$this->CreateCustomerPayment($contact_id,$product_info[0]['dealCodeNumber'],$Invoice_id,$new_deposite);
            $this->CreateCustomerPayment_retainer($contact_id,$product_info[0]['dealCodeNumber'],$Invoice_id,$new_deposite);
        		// print_r($respo);exit;
        }
             $this->CreateSaleInvoice($user_id,$deal_code,$reference_number,$gst_number);

    }


 
    public function CreateSaleInvoice($user_id,$deal_code,$reference_number,$gst_number){
        $product_info = $this->order_model->getCurrentSuccessOrders($user_id, $deal_code);
        $seller_info = $this->order_model->get_all_details(USERS,array('id' => $product_info[0]['sell_id']));
        $subproducts_quantity = array();
        // $wearhouse_code  = $this->order_model->get_all_details(CITY_WEARHOUSE_CODE,array('id !=' => ''));

        $coupon_code  = $this->order_model->get_all_details(COUPONCARDS,array('id' => $product_info[0]['coupon_id']));
    	if($coupon_code->row()->price_value >= 100){
                $discount_first_month  = 0;
    	}else{
    		$discount_first_month =  $product_info[0]['discountAmount'];
    	}
        $url = "https://books.zoho.com/api/v3/contacts?organization_id=".BOOKS_ORGANIZATION_ID."&email_contains=".$product_info[0]['email']."";
        $headers = [
                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
                    'Authorization:'.BOOKS_AUTH_TOKEN
            ];
        $curl = curl_init($url);
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers
        ));
        $result = curl_exec($curl);
        $contact_data =  json_decode($result, TRUE);

        foreach ($product_info as $key =>  $cartRow) {
            $InvImg = @explode(',',$cartRow->image);
            if($cartRow['attr_name'] != ''){
            		$tenure  = explode(' ', $cartRow['attr_name']);  
            		$dayx = date("Y-m-d", strtotime("+".$cartRow['attr_name']));
        			$pick_up_date =  date("Y-m-d", strtotime($dayx.' +5 day'));
            	}
            array_push($subproducts_quantity, unserialize($cartRow['subproduct_quantity']));

            $unitPrice = ($cartRow['price']*(0.01*$cartRow['product_tax_cost']))+$cartRow['price'] * $cartRow['quantity'] ;
            $unitDeposit =  $cartRow['product_shipping_cost'] * $cartRow['quantity'];
            $grandDeposit = $grandDeposit + ($unitDeposit*$cartRow['quantity']);
            $uTot = $unitPrice + $unitDeposit;
        }
	        $private_total = $grantTotal - $product_info[0]['discountAmount'];
	        $private_total = $private_total + $product_info[0]['tax'];

        $itemsarray = array();
        $counter = 0;


            foreach ($product_info as $key =>  $cartRow) {
	       		if($cartRow['subproducts'] == ''){
	       		    $zoho_item_id = $this->check_sku($cartRow['sku'], $cartRow['product_name'], $cartRow['id']);
           			if(!array_key_exists('item_id', $zoho_item_id)){
                  		break;
    	       		}
	       			$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
	    	 		$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
	                $itemsarray[$counter]['quantity'] = $cartRow['quantity'];
	                if($cartRow['attribute_values'] != ''){
	                	$itemsarray[$counter]['rate'] = round($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']);
	                	$itemsarray[$counter]['bcy_rate'] = round($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']);
	            	}else{
	            		$itemsarray[$counter]['rate'] = $cartRow['price'];
	                	$itemsarray[$counter]['bcy_rate'] = $cartRow['price'];
	            	}
	                $itemsarray[$counter]['name'] = $zoho_item_id['item_name'];
	                 $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
	                // $cartRow['product_name'];
	                // $itemsarray[$counter]['discount'] = $cartRow['discountAmount'];
		            $counter++;
		       	}else{ 
		   			$product_id = explode(',',$cartRow['subproducts']);
		   			$product_data = $this->order_model->Get_subproducts($product_id);
		   			foreach ($product_data as $packagekey => $packagevalue) {
		   				if($packagevalue->subproducts != '')
		   				{
		   					$subproduct_id = explode(',', $packagevalue->subproducts);
		   					$subproducts_subproduct = $this->order_model->Get_subproducts($subproduct_id);
		   					foreach ($subproducts_subproduct as $key => $value) {
		   						$zoho_item_id = $this->check_sku($value->sku, $value->product_name, $value->id);
					       		if(!array_key_exists('item_id', $zoho_item_id)){
				                    break;
					       		}
		   						$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
		   						$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
		                		$itemsarray[$counter]['quantity'] = $subproducts_quantity[$key][$packagevalue->id];
		               	    	$itemsarray[$counter]['rate'] =  round($value->price - ($value->price / 100) * $cartRow['package_discount']);
		                    	$itemsarray[$counter]['bcy_rate'] = round($value->price - ($value->price / 100) * $cartRow['package_discount']);
		                    	$itemsarray[$counter]['name'] =  $zoho_item_id['item_name'];
		                    	 $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		                    	// $value->product_name;
		                    	// $itemsarray[$counter]['discount'] = $cartRow['discountAmount'];
		   						$counter++;
		   					}
		   				}
		   				else
		   				{
		   				    $zoho_item_id = $this->check_sku($packagevalue->sku, $packagevalue->product_name, $packagevalue->id);
			       			if(!array_key_exists('item_id', $zoho_item_id)){
		                          break;
			       			}
		   					$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
		   					$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
		                	$itemsarray[$counter]['quantity'] = $subproducts_quantity[$key][$packagevalue->id];
		               	    $itemsarray[$counter]['rate'] =  round($packagevalue->price - ($packagevalue->price / 100) * $cartRow['package_discount']);
		                    $itemsarray[$counter]['bcy_rate'] =   round($packagevalue->price - ($packagevalue->price / 100) * $cartRow['package_discount']);
		                    $itemsarray[$counter]['name'] = $zoho_item_id['item_name'];
		                     $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		                     // $packagevalue->product_name;
		                    // $itemsarray[$counter]['discount'] = $cartRow['discountAmount'];
		                    $counter++;
		   				}						
		                
		   			}

		       	}
			}
    	  	$json = json_encode($itemsarray);
        	if (array_key_exists("contact_id", $contact_data['contacts'][0])) {
            	$contact_id = $contact_data['contacts'][0]['contact_id'];
        	}
                $order_date = $product_info[0]['created'];
                $createDate = strtotime($order_date);
                $strip = date('Y-m-d', $createDate); 
                if($product_info[0]['tax'] > 0){
                    $tax = 'true';
                }else{
                    $tax = 'false';
                }
                $customer_notes = 'Tenure: '.$tenure[0].' Place of Supply: ' .$product_info[0]['full_name'].' '.$product_info[0]['ship_city'].' '.$product_info[0]['ship_state'].' '.$product_info[0]['ship_postal_code'].' '.'India Phone: '.' '.$product_info[0]['ship_phone'].'';
                             
                            // "shipping_charge":'.$product_info[0]['shippingcost'].',
        $Curl = "https://books.zoho.com/api/v3/invoices";
        $Invoice = array(
                        'authtoken'=>BOOKS_AUTH_TOKEN,
                        'JSONString' => '{
                            "customer_id": '.$contact_id.',
                            "date": '.$strip.',
                            "discount": '.$product_info[0]['discountAmount'].',
                            "reference_number":"'.$reference_number.'",
                            "discount_type": "entity_level",
                            "is_inclusive_tax": '.$tax.',
    						"salesperson_name": '.$seller_info->row()->full_name.',
                            "line_items":'.$json.',
                             "notes":"'.$customer_notes.'",
                            "payment_options": {},
                            "allow_partial_payments": true,
                            "custom_body": " ",
                            "custom_subject": "Sale Invoice",
                            "notes": "Looking forward for your business.",
                            "terms": "Terms & Conditions apply",
                            "gateway_name": "PayU",
                            "custom_fields": [
	          						{
									    "placeholder": "cf_gst",
									    "index": 4,
									    "value":"'.$gst_number.'"
									   
									},
									{
									    "placeholder": "cf_reverse_charge_applicable",
									    "index": 6,
									    "value":"No"
									   
									},		
									{
									    "placeholder": "cf_invoice_number_exception",
									    "index": 7,
									    "value":"No"
									   
									},
									{
									    "placeholder": "cf_pos",
									    "index": 8,
									    "value":"'.$product_info[0]['ship_city'].'"
									   
									},
									{
									    "placeholder": "cf_delivery_state",
									    "index": 9,
									    "value":"'.$product_info[0]['ship_state'].'"
									   
									},	
	                            ],
                            "additional_field1": "standard"
                           
                        }',
                        "organization_id"=>BOOKS_ORGANIZATION_ID
                    );

          $curl = curl_init($Curl);
            curl_setopt_array($curl, array(
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $Invoice,
                CURLOPT_RETURNTRANSFER => true
            ));
            $invoice_result = curl_exec($curl);  
            $new_result = json_decode($invoice_result);   
            if (isset($new_result->invoice)) {
	            $Invoice_id = $new_result->invoice->invoice_id;

	            $status_url ='https://books.zoho.com/api/v3/invoices/'.$Invoice_id.'/status/sent?organization_id=82487023';
	 
			    $headers = [
		                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
		                    'Authorization:'.BOOKS_AUTH_TOKEN
				            ];
		        $curl = curl_init($status_url);
		        		curl_setopt_array($curl, array(
		            		CURLOPT_RETURNTRANSFER => true,
		            		CURLOPT_HTTPHEADER => $headers,
		            		CURLOPT_POST => 1,
		        		));
		        $result = curl_exec($curl);
        		$respo =  json_decode($result, TRUE);
          		$this->CreateCustomerPayment($contact_id,$product_info[0]['dealCodeNumber'],$Invoice_id,$new_result->invoice->total);
	      	}
            $this->CreateRecurringInvoice($reference_number,$user_id,$deal_code,$discount_first_month);
    }

     public function CreateRecurringInvoice($reference_number,$user_id,$deal_code,$discount_first_month){
    	$product_info = $this->order_model->getCurrentSuccessOrders($user_id, $deal_code);
    	$subproducts_quantity = array();
    	$wearhouse_code  = $this->order_model->get_all_details(CITY_WEARHOUSE_CODE,array('id !=' => ''));
        foreach ($wearhouse_code->result() as $key => $value) {
        	if($value->city_name == $product_info[0]['shippingcity']){
                    $gst_number = $value->gst_num;
        	}else{
        	    $gst_number = 'MH - 27AAJCM9442J1ZY';
        	}
        }
        $end_date =  date("Y-m-d", strtotime($dayx.' +5 day'));
    	$url = "https://books.zoho.com/api/v3/contacts?organization_id=". BOOKS_ORGANIZATION_ID ."&email_contains=".$product_info[0]['email']."";
        $headers = [
                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
                    'Authorization:'.BOOKS_AUTH_TOKEN
            ];
        $curl = curl_init($url);
        		curl_setopt_array($curl, array(
            		CURLOPT_RETURNTRANSFER => true,
            		CURLOPT_HTTPHEADER => $headers
        		));
        $result = curl_exec($curl);
        $contact_data =  json_decode($result, TRUE);

        if (array_key_exists("contact_id", $contact_data['contacts'][0])) {
            $contact_id = $contact_data['contacts'][0]['contact_id'];
        }else
            {
	            $Contact_url ="https://books.zoho.com/api/v3/contacts";
	            $Contact_data = array(
	                'authtoken'=> BOOKS_AUTH_TOKEN,
	                'JSONString' => '
	                {
	                    "contact_name": '.$product_info[0]['full_name'].',
	                    "contact_type": "customer",
	                    "shipping_address": {
	                        "attention": "Mr.'.$product_info[0]['full_name'].'",
	                        "address": '.$product_info[0]['ship_address1'].',
	                        "city": '.$product_info[0]['ship_city'].',
	                        "state": '.$product_info[0]['ship_state'].',
	                        "zip": '.$product_info[0]['ship_postal_code'].',
	                        "country": "India",
	                        "phone": '.$product_info[0]['ship_phone'].',
	                    },
	                    "contact_persons": [
	                        {
	                            "salutation": "Mr",
	                            "first_name": '.$product_info[0]['full_name'].',
	                            "email": '.$product_info[0]['email'].',
	                            "phone": '.$product_info[0]['phone_no'].'
	                        }
	                    ],
	                    
	                }',
	                "organization_id"=>BOOKS_ORGANIZATION_ID
	            );
	        	$curl = curl_init($Contact_url);
	                	curl_setopt_array($curl, array(
	                    	CURLOPT_POST => 1,
	                    	CURLOPT_POSTFIELDS => $Contact_data,
	                    	CURLOPT_RETURNTRANSFER => true
	                	));
	            $Contact_response = curl_exec($curl);
	            $Decode_Contact = json_decode($Contact_response);
	            $contact_id = $Decode_Contact->contact->contact_id;
            }


            $itemsarray = array();
        	$counter = 0;
	        foreach ($product_info as $key =>  $cartRow) {
	        	if($cartRow['attr_name'] != ''){
            		$tenure  = explode(' ', $cartRow['attr_name']);  
            		$dayx = date("Y-m-d", strtotime("+".$cartRow['attr_name']));
        			$pick_up_date =  date("Y-m-d", strtotime($dayx.' +5 day'));
            	}
            	array_push($subproducts_quantity, unserialize($cartRow['subproduct_quantity']));
	       		if($cartRow['subproducts'] == ''){
	       			$zoho_item_id = $this->check_sku($cartRow['sku'], $cartRow['product_name'], $cartRow['id']);
		       		if(!array_key_exists('item_id', $zoho_item_id)){
	                    break;
		       		}
		       		$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
	    	 		$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
	                $itemsarray[$counter]['quantity'] = $cartRow['quantity'];
	                if($cartRow['attribute_values'] != ''){
	                	$itemsarray[$counter]['rate'] = round($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']);
	                	$itemsarray[$counter]['bcy_rate'] = round($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']);
	            	}else{
	            		$itemsarray[$counter]['rate'] = $cartRow['price'];
	                	$itemsarray[$counter]['bcy_rate'] = $cartRow['price'];
	            	}
	                 $itemsarray[$counter]['name'] = $zoho_item_id['item_name'];
	                  $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
	                // $cartRow['product_name'];
	                // $itemsarray[$counter]['discount'] = $cartRow['discountAmount'];
		            $counter++;
		       	}else{ 
		   			$product_id = explode(',',$cartRow['subproducts']);
		   			$product_data = $this->order_model->Get_subproducts($product_id);
		   			foreach ($product_data as $packagekey => $packagevalue) {
		   				if($packagevalue->subproducts != '')
		   				{
		   					$subproduct_id = explode(',', $packagevalue->subproducts);
		   					$subproducts_subproduct = $this->order_model->Get_subproducts($subproduct_id);
		   					foreach ($subproducts_subproduct as $key => $value) {
		   						$zoho_item_id = $this->check_sku($value->sku, $value->product_name, $value->id);
					       		if(!array_key_exists('item_id', $zoho_item_id)){
				                    break;
					       		}
		   						$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
		   						$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
		                		$itemsarray[$counter]['quantity'] = $subproducts_quantity[$key][$packagevalue->id];
		               	    	$itemsarray[$counter]['rate'] =  round($value->price - ($value->price / 100) * $cartRow['package_discount']);
		                    	$itemsarray[$counter]['bcy_rate'] = round($value->price - ($value->price / 100) * $cartRow['package_discount']);
		                    	$itemsarray[$counter]['name'] = $zoho_item_id['item_name']; 
		                    	 $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		                    	 // $value->product_name;
		                    	// $itemsarray[$counter]['discount'] = $cartRow['discountAmount'];
		   						$counter++;
		   					}
		   				}
		   				else
		   				{
		   				    $zoho_item_id = $this->check_sku($packagevalue->sku, $packagevalue->product_name, $packagevalue->id);
			       			if(!array_key_exists('item_id', $zoho_item_id)){
		                          break;
			       			}
		   					$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
		   					$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
		                	$itemsarray[$counter]['quantity'] = $subproducts_quantity[$key][$packagevalue->id];
		               	    $itemsarray[$counter]['rate'] =  round($packagevalue->price - ($packagevalue->price / 100) * $cartRow['package_discount']);
		                    $itemsarray[$counter]['bcy_rate'] = round($packagevalue->price - ($packagevalue->price / 100) * $cartRow['package_discount']);
		                    $itemsarray[$counter]['name'] =   $zoho_item_id['item_name']; 
		                     $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		                    // $packagevalue->product_name;
		                    // $itemsarray[$counter]['discount'] = $cartRow['discountAmount'];
		                    $counter++;
		   				}						
		                
		   			}

		       	}
			}
	    	$json = json_encode($itemsarray);

       

           	$Curl = "https://books.zoho.com/api/v3/recurringinvoices";
	        $Invoice = array(
	                        'authtoken'=> BOOKS_AUTH_TOKEN,
	                        'JSONString' => '{
	                        		"recurrence_name": "'.strtoupper($product_info[0]['full_name']).'('.$reference_number.')",
								    "customer_id": '.$contact_id.',
								    "place_of_supply": "TN",
								    "start_date": "'.date('Y-m-d',strtotime("+1 month")).'",
								    "discount_type":"entity_level",
								    "reference_number":"'.$reference_number.'",
								    "recurrence_frequency": "months",
								    "salesperson_name":"Accounts Team",
								    "payment_terms_label":"Due on Receipt",
								    "payment_terms":"0",
								    "line_items":'.$json.',
								    "email": "'.$product_info[0]['email'].'",
								    "payment_gateways": [
								        {
								            "configured": true,
								            "additional_field1": "standard",
								            "gateway_name": "paypal"
								        }
								    ],

								    "custom_fields": [
	          						{
									    "placeholder": "cf_gst",
									    "index": 4,
									    "value":"'.$gst_number.'"
									   
									},
									{
									    "placeholder": "cf_reverse_charge_applicable",
									    "index": 6,
									    "value":"No"
									   
									},		
									{
									    "placeholder": "cf_invoice_number_exception",
									    "index": 7,
									    "value":"No"
									   
									},
									{
									    "placeholder": "cf_pos",
									    "index": 8,
									    "value":"'.$product_info[0]['ship_city'].'"
									   
									},
									{
									    "placeholder": "cf_delivery_state",
									    "index": 9,
									    "value":"'.$product_info[0]['ship_state'].'"
									   
									},	
	                            ],
								 
								    "is_discount_before_tax": true,
								    "discount": '.$discount_first_month.',
								    "adjustment_description": "Rounding off"
								}',
	                        "organization_id"=> BOOKS_ORGANIZATION_ID
	                    );
								    // "shipping_charge": '.$product_info[0]['shippingcost'].',
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $invoice_result = curl_exec($curl);
	        $respo = json_decode($invoice_result);


    }
    
     public function CreateCustomerPayment_retainer($contact_id,$order_id,$Invoice_id,$total ){
    	  	$Curl = "https://books.zoho.com/api/v3/customerpayments?organization_id=82487023";
    	  	$Invoice = 'JSONString={
    	  		   	"customer_id": '.$contact_id.',
					"payment_mode": "Payment Gateway",
					"amount": '.$total.',
					"date": "'.date('Y-m-d').'",
					"reference_number": "#'.$order_id.'",
					"retainerinvoice_id" : '.$Invoice_id.'
    	  	}';
	         $headers = [
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization:'.BOOKS_AUTH_TOKEN
	            ];
		
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_HTTPHEADER => $headers,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $customer_payment = curl_exec($curl);
	        $respo = json_decode($customer_payment);
    }
    
     public function CreateCustomerPayment($contact_id,$order_id,$Invoice_id,$total ){
    	  	$Curl = "https://books.zoho.com/api/v3/customerpayments?organization_id=82487023";
    	  	$Invoice = 'JSONString={
    	  		   "customer_id": '.$contact_id.',
					"payment_mode": "Payment Gateway",
					"amount": '.$total.',
					"date": "'.date('Y-m-d').'",
				    "reference_number": "#'.$order_id.'",
					"description": "Payment has been added to '.$order_id.'",
					"invoices": [
					    {
						    "invoice_id": '.$Invoice_id.',
							"amount_applied": '.$total.'
						}
					],
					"amount_applied": '.$total.',
					"tax_amount_withheld": 0
    	  	}';
	         $headers = [
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization:'.BOOKS_AUTH_TOKEN
	            ];
		
			// // 	    // "shipping_charge": '.$product_info[0]['shippingcost'].',
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_HTTPHEADER => $headers,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $customer_payment = curl_exec($curl);
	        $respo = json_decode($customer_payment);
	        if(!isset($respo->payment)){
	        		$email_values = array(
									'mail_type'=>'html',
	             					'from_mail_id'=>'hello@cityfurnish.com',
	             					'mail_name'=>'Zoho Error Mail',
	             					'to_mail_id'=>'naresh.suthar@agileinfoways.com',
	             					'subject_message'=>'Customer Payment Error',
	             					'body_messages'=> $customer_payment
								);
				$email_send_to_common = $this->order_model->common_email_send($email_values);
	        }
    }

    public function check_sku($sku,$product_name,$product_id, $count = 0){
    	$url_pro =  "https://books.zoho.com/api/v3/items?organization_id=". BOOKS_ORGANIZATION_ID ."&search_text=".$sku."";
        $headers = [
                    'Content-Type: application/json;charset=UTF-8',
                    'Authorization:'.BOOKS_AUTH_TOKEN
        ];
        $curl = curl_init($url_pro);
				curl_setopt_array($curl, array(
        		CURLOPT_RETURNTRANSFER => true,
        		CURLOPT_HTTPHEADER => $headers
	    ));
	        $result = curl_exec($curl);
	        $product_info_data =  json_decode($result, TRUE);
        if(array_key_exists("item_id", $product_info_data['items'][0])){
            return $product_info_data['items'][0];
        }
        else{
        	$failed_msg = $product_name.' is not found in zoho items where item id is '.$product_id.' and sku is'.$sku.' delay '.$count;
			$email_values = array(
								'mail_type'=>'html',
             					'from_mail_id'=>'hello@cityfurnish.com',
             					'mail_name'=>'Zoho Error Mail',
             					'to_mail_id'=>'vinit.jain@cityfurnish.com,naresh.suthar@agileinfoways.com',
             					'subject_message'=>'Zoho Case Error',
             					'body_messages'=>$failed_msg
							);
			$email_send_to_common = $this->order_model->common_email_send($email_values);
			return null;
    	}

    }
    
    public function capturePayment(){
    	$cart_data =  $this->order_model->get_all_details(SHOPPING_CART,array( 'user_id' => $this->data['common_user_id']));
    	// $product_name = $this->order_model->get_all_details(PRODUCT,array('id' => $or))
    	$product_ids = [];
    	$return_data = [];
    	$quantity = 0;
    	$total = 0;
    	$voucher_code = '';
    	foreach ($cart_data->result_array() as $key => $value) {
    		array_push($product_ids, $value['product_id']);
    	}
    	$product_name = $this->order_model->get_productname($product_ids);
    	foreach ($cart_data->result_array() as $key => $value) {
    		$total = $value['total'];
    		$quantity += $value['quantity'];
    		$product = $product_name[$key]['product_name'].',';
    		$voucher_code = $value['couponCode'];
    	}
    	$product = rtrim($product, ',');
    	$return_data['total'] = $total;
    	$return_data['quantity'] = $quantity;
		$return_data['product_name'] = $product;
		$return_data['user_id'] = $this->data['common_user_id'];
		$return_data['voucher_code'] = $voucher_code;
		echo json_encode($return_data);
		exit;
    }

    public function getOrderDetails(){
    	$payment_data =  $this->order_model->get_faild_order($this->data['common_user_id']);
    	$response_array = [];
    	$response_array['total'] = $payment_data->total;
    	$response_array['order_id'] = $payment_data->dealCodeNumber;
    	$response_array['user_id'] = $this->data['common_user_id'];
    	echo json_encode($response_array);
		exit;
    	// print_r($payment_data);exit;
    	
    }

}
