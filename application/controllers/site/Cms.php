<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * CMS related functions
 * @author Teamtweaks
 *
 */

class Cms extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form','email','url'));
		$this->load->library(array('encrypt','form_validation','session'));		
		$this->load->model('product_model');
		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->product_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];
		 
		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->product_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];
		
		$this->data['loginCheck'] = $this->checkLogin('U');
		$this->data['likedProducts'] = array();
	 	if ($this->data['loginCheck'] != ''){
	 		$this->data['likedProducts'] = $this->product_model->get_all_details(PRODUCT_LIKES,array('user_id'=>$this->checkLogin('U')));
	 	}
    }
    
    	private $mobilepattern = '/^[1-9][0-9]*$/';
	private $email_pat = "/^[(a-z)(0-9).-_]+@[(a-z)(0-9)]+\.[(a-z).]{2,5}$/";
	
	public function index(){
    	$seourl = $this->uri->segment(2);
		$pageDetails = $this->product_model->get_all_details(CMS,array('seourl'=>$seourl,'status'=>'Publish'));
    	if ($pageDetails->num_rows() == 0){
    		show_404();
    	}else {
    		if ($pageDetails->row()->meta_title != ''){
	    		$this->data['heading'] = $pageDetails->row()->meta_title;
				$this->data['meta_title'] = $pageDetails->row()->meta_title;
			}
			if ($pageDetails->row()->meta_tag != ''){
		    	$this->data['meta_keyword'] = $pageDetails->row()->meta_tag;
			}
			if ($pageDetails->row()->meta_description != ''){
		    	$this->data['meta_description'] = $pageDetails->row()->meta_description;
			}
    		$this->data['heading'] = $pageDetails->row()->meta_title;
    		$this->data['pageDetails'] = $pageDetails;
    		$this->load->view('site/cms/display_cms',$this->data);
    	}
    }
    
	public function page_by_id(){
    	$cid = $this->uri->segment(2);
		$pageDetails = $this->product_model->get_all_details(CMS,array('id'=>$cid,'status'=>'Publish'));
    	if ($pageDetails->num_rows() == 0){
    		show_404();
    	}else {
    		if ($pageDetails->row()->meta_title != ''){
	    		$this->data['heading'] = $pageDetails->row()->meta_title;
				$this->data['meta_title'] = $pageDetails->row()->meta_title;
			}
			if ($pageDetails->row()->meta_tag != ''){
		    	$this->data['meta_keyword'] = $pageDetails->row()->meta_tag;
			}
			if ($pageDetails->row()->meta_description != ''){
		    	$this->data['meta_description'] = $pageDetails->row()->meta_description;
			}
    		$this->data['heading'] = $pageDetails->row()->meta_title;
    		$this->data['pageDetails'] = $pageDetails;
    		$this->load->view('site/cms/display_cms',$this->data);
    	}
    }
	public function contactus(){
			$this->data['heading'] = 'Cityfurnish Customer Support | Contact Us - cityfurnish.com';
			$this->data['meta_title'] = 'Cityfurnish Customer Support | Contact Us - cityfurnish.com';
			$this->data['meta_description'] = 'Contact Cityfurnish Customer Support at hello@cityfurnish.com for your inquiries or suggestions. We will be happy to help you.';
			$this->data['msg'] = '';
			$this->data['msgasas'] = '';
			$this->data['uemail'] = '';
			$this->data['ucontact'] = '';
			$this->data['uquery'] = '';
			$this->data['udescription'] = '';
			if($this->input->post('submit')){
				$error = '';
				if($this->input->post('uemail')=='' || $this->input->post('ucontact')=='' || $this->input->post('uquery')=='' || $this->input->post('udescription')==''){
					$error = "All Fileds are Required<br/>";
				}
				if($this->input->post('uemail')!=''){
					if(!preg_match($this->email_pat, $this->input->post('uemail'))){
						$error .= "Invalid Email Format<br/>";
					}
				}				
				if($this->input->post('ucontact')!=''){
					if(!is_numeric($this->input->post('ucontact'))){
						$error .= "Invalid Contact No.<br/>";			
					}elseif(strlen($this->input->post('ucontact'))<>10){
						$error .= "Invalid Contact No.<br/>";			
					}elseif(preg_match($this->mobilepattern, $this->input->post('ucontact')) === 0){
						$error .= "Do not start with 0 or +91<br/>";	
					}
				}
				if($error==''){
					$this->db->query("INSERT INTO ".TICKETS." SET t_email='".mysql_real_escape_string($this->input->post('uemail'))."',t_contact='".mysql_real_escape_string($this->input->post('ucontact'))."',TPID='".mysql_real_escape_string($this->input->post('uquery'))."',t_description='".mysql_real_escape_string($this->input->post('udescription'))."',t_status='NEW',adddateTime=now()");
					if($this->db->affected_rows() > 0){
						$this->data['msgasas'] = "Thanks for submitting your query.<br/>Will revert back soon";
						$headers = "From: CityFurnish Team <info@cityfurnish.com>\r\n";
						$query = $this->db->query("SELECT t_emailidccc FROM ".TICKETS_TOPIC." WHERE ID='".mysql_real_escape_string($this->input->post('uquery'))."' LIMIT 1");
						if($query->num_rows() > 0){
							$resdsd = $query->result();
							foreach($resdsd as $rtrt){
								$headers .= 'Cc: <'.$rtrt->t_emailidccc.'>' . "\r\n";
							}
						}						
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
						$message = "Hello,<br/><br/>
									Check details Below<br/>
									Email: ".$this->input->post('uemail')."<br/>
									Contact: ".$this->input->post('ucontact')."<br/>
									Description: ".$this->input->post('udescription')."<br/><br/>
									Regards
						";
						$query = $this->db->query("SELECT t_emailtitle,t_emailid FROM ".TICKETS_TOPIC." WHERE ID='".mysql_real_escape_string($this->input->post('uquery'))."' LIMIT 1");
						if($query->num_rows() > 0){
							$resdsd = $query->result();
							foreach($resdsd as $rtrt){								
								mail($rtrt->t_emailid, $rtrt->t_emailtitle, $message, $headers);
							}
						}
						$this->session->set_userdata(array('msgasas'=>"Thanks for submitting your query.<br/>Will revert back soon"));
						$this->sendcustomercopy($this->input->post('uemail'));
						redirect(''.BASE_URL.'pages/contact-us', 'location');
					}
				}else{		
					$this->data['msg'] = $error;
					$this->data['uemail'] = $this->input->post('uemail');
					$this->data['ucontact'] = $this->input->post('ucontact');
					$this->data['uquery'] = $this->input->post('uquery');
					$this->data['udescription'] = $this->input->post('udescription');
				}
			}
			$query = $this->db->query("SELECT ID,t_title FROM ".TICKETS_TOPIC." ORDER BY t_title ASC");
			if($query->num_rows() > 0){
				$this->data['topics'] = $query->result();
			}else{
				$this->data['topics'] = array();
			}
    	    $this->load->view('site/cms/contactus',$this->data);
	}
		function sendcustomercopy($to){
		$headers = "From: CityFurnish Team <info@cityfurnish.com>\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$message = '<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>HOMER -  Email Template</title>
    <style>
        * {
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            font-size: 100%;
            line-height: 1.6em;
            margin: 0;
            padding: 0;
        }

        img {
            max-width: 600px;
            width: 100%;
        }

        body {
            -webkit-font-smoothing: antialiased;
            height: 100%;
            -webkit-text-size-adjust: none;
            width: 100% !important;
        }

        a {
            color: #348eda;
        }

        .btn-primary {
            Margin-bottom: 10px;
            width: auto !important;
        }

        .btn-primary td {
            background-color: #62cb31;
            border-radius: 3px;
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-size: 14px;
            text-align: center;
            vertical-align: top;
        }

        .btn-primary td a {
            background-color: #62cb31;
            border: solid 1px #62cb31;
            border-radius: 3px;
            border-width: 4px 20px;
            display: inline-block;
            color: #ffffff;
            cursor: pointer;
            font-weight: bold;
            line-height: 2;
            text-decoration: none;
        }

        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .padding {
            padding: 10px 0;
        }

        table.body-wrap {
            padding: 20px;
            width: 100%;
        }

        table.body-wrap .container {
            border: 1px solid #e4e5e7;
        }

        table.footer-wrap {
            clear: both !important;
            width: 100%;
        }

        .footer-wrap .container p {
            color: #666666;
            font-size: 12px;

        }

        table.footer-wrap a {
            color: #999999;
        }

        h1,
        h2,
        h3 {
            color: #111111;
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-weight: 200;
            line-height: 1.2em;
            margin: 40px 0 10px;
        }

        h1 {
            font-size: 36px;
        }
        h2 {
            font-size: 28px;
        }
        h3 {
            font-size: 22px;
        }

        p,
        ul,
        ol {
            font-size: 14px;
            font-weight: normal;
            margin-bottom: 10px;
        }

        ul li,
        ol li {
            margin-left: 5px;
            list-style-position: inside;
        }

        /* ---------------------------------------------------
            RESPONSIVENESS
        ------------------------------------------------------ */

        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            clear: both !important;
            display: block !important;
            Margin: 0 auto !important;
            max-width: 600px !important;
        }

        /* Set the padding on the td rather than the div for Outlook compatibility */
        .body-wrap .container {
            padding: 40px;
        }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            display: block;
            margin: 0 auto;
            max-width: 600px;
        }

        /* Let\'s make sure tables in the content area are 100% wide */
        .content table {
            width: 100%;
        }

    </style>
</head>

<body bgcolor="#f7f9fa">

<table class="body-wrap" bgcolor="#f7f9fa">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <div class="content">
                <table>
                    <tr>
                        <td>
                            <strong>Hello,</strong>
                            <p>Thanks for contacting us.</p>
                            <p>We have recieved your query and we are processing it.</p>                            
                            <p>Regards<br/>CityFurnish Team</p>                            
                        </td>
                    </tr>
                </table>
            </div>

        </td>
        <td></td>
    </tr>
</table>

</body>
</html>';
		mail($to, 'Contact Request At CityFurnish', $message, $headers);
	}
	
	public function whyus(){
			$this->data['heading'] = 'Why Rent furniture from Cityfurnish?';
			$this->data['meta_title'] = 'Why Rent furniture from Cityfurnish?';
			$this->data['meta_description'] = 'Cityfurnish.com: Wht Choose Us for Renting Furniture';
  
    	    $this->load->view('site/cms/whyus',$this->data);
	}
	public function faq(){
        if(empty($_SESSION['clreviewList'])){
                    $clrWherCond .= ' where p.id !="" and p.excerpt!="" and p.status="Publish" and u.status="Active" order by p.created desc limit 0,6';
                    $clreviewList = $this->product_model->searchShopyByCategoryuser($clrWherCond);
                    $_SESSION['clreviewList'] = $clreviewList->result();
                 }
            $this->data['clreviewList'] = $_SESSION['clreviewList'];
			$this->data['heading'] = 'Frequently Asked Questions';
			$this->data['meta_title'] = 'Furniture Rental - Frequently Asked Questions';
			$this->data['meta_description'] = 'Cityfurnish.com: Answers of Frequently Asked Quetions for Furniture Rental';
  
    	    $this->load->view('site/cms/faq',$this->data);
	}
	public function careers(){
			$this->data['heading'] = 'We are Hiring | Careers @ Cityfurnish';
			$this->data['meta_title'] = 'We are Hiring | Careers @ Cityfurnish';
			$this->data['meta_description'] = 'We are Hiring | At Cityfurnish we are building an on-demand furniture and appliances rental startup';
  
    	    $this->load->view('site/cms/careers',$this->data);
	}
	public function offers(){
		$this->data['heading'] = 'Cityfurnish Offers and Discount Coupons';
		$this->data['meta_title'] = 'Cityfurnish Offers and Discount Coupons';
		$this->data['meta_description'] = 'Cityfurnish Offers and Discount Coupons';
        $this->data['voucher_data'] = $this->product_model->get_all_details(VOUCHER,array('voucher_title != ' => ''));
	    $this->load->view('site/cms/offers',$this->data);
	}
	public function ex_offers(){
			$this->data['heading'] = 'Discount Coupons';
			$this->data['meta_title'] = 'Discount Coupons';
			$this->data['meta_description'] = 'Discount Coupons';
  
    	    $this->load->view('site/cms/ex_offers',$this->data);
	}
public function how_it_works(){
			$this->data['heading'] = 'Cityfurnish- Rental Process';
			$this->data['meta_title'] = 'Cityfurnish- Rental Process';
			$this->data['meta_description'] = 'How Process of Rental Works';
  
    	    $this->load->view('site/cms/how_it_works',$this->data);
	}
public function refer_a_friend(){
			$this->data['heading'] = 'Cityfurnish- Referral Program';
			$this->data['meta_title'] = 'Cityfurnish- Refer a friend and get extra benefits';
			$this->data['meta_description'] = 'Cityfurnish Friends Referral Program';
  
    	    $this->load->view('site/cms/refer_a_friend',$this->data);
	}
public function rental_agreement(){

			$this->data['heading'] = 'Rental Agreement';
			$this->data['meta_title'] = 'Cityfurnish Sample Rental Agreement';
			$this->data['meta_description'] = 'Check Our Sample Rental Agreement - A Customer Friendly Approach.';
            
            $this->load->view('site/cms/rental_agreement',$this->data);
}

public function categorylist(){   
        $this->data['heading'] = 'Category Landing';
        $this->data['meta_title'] = 'Furniture and Home Appliances - Cityfurnish';
        $this->data['meta_description'] = 'Furniture and Home Appliances on Rent. Free Delivery, Installation and maintenance.';
        // $this->data['meta_keyword'] = 'Check Our Sample Rental Agreement - A Customer Friendly Approach.';
        $this->data['ListCateogry'] = $this->product_model->category_landing();
        $this->load->view('site/cms/category_landing',$this->data);
}

public function mumbaifurniture(){   
        $this->data['heading'] = 'Furniture on Rent | Mumbai, Navi Mumbai, Thane';
        $this->data['meta_title'] = 'Furniture on Rent | Mumbai, Navi Mumbai, Thane';
        $this->data['meta_description'] = 'Rent Furniture in Mumbai, Navi Mumbai and Thane on Easy Monthly Furniture Rental Plans. Furniture Rentals was Never So Easy. Free Delivery and Installation';
        $this->data['meta_keyword'] = '
        <h2>Rental Furniture in Mumbai</h2>
        <p>Are you planning to furnish your home and don&apos;t want to invest too much capital in that? Then, you are at the right place, furniture renting in the new buying. Now you can take furniture on rent and furnish your home the way you want it. We provide a variety of office and home furniture on rent in Mumbai, Thane and Navi Mumbai.</p>
        <br /><h3>Quality Furniture on Rent</h3>
        <p>Quality and stylish furniture in our forte. We can proudly admit that our approach is not only refreshingly flexible and modern but also at the same time, preserve the value of old customer services where customer&apos;s satisfaction and quality of work is the first priority of the vendor. We keep the excellence, soothe and style of the furniture, furnishing and kitchen equipment on the top of our checklist while choosing the right furniture for our clients since we take our work very personally.</p>
        <br /><h3>Carefully Designed Furniture Rental Packages</h3>
        <p>Nobody understands Mumbai better than us. We complete understand that the Mumbai has a space crunch, so we have designed space saver furniture especially for Mumbai. Now you can rent furniture which is not only stylish but also multipurpose and compact. We follow very firm protocols when it comes to the quality and looks of our goods. Our collection is completely handpicked and strikingly outstanding. Our thoughtfully chosen furniture can turn a simple looking usable area into a luxury and spacious accommodation. But, we also understand that mere looks are not sufficient while choosing furniture for either your personal use or professional use. All of our selected furniture items are thoroughly tested by using many firm dynamic rules decided by few very well-known experts in the relative field. We understand that the quality and looks go hand in hand. No home can leave an everlasting impression in our memory if anyone from the above is missing. Next, come delivery and installation.</p>
        <br /><h3>Hassle-Free Delivery within 72 Hours</h3>
        <p>Our delivery team of dedicated and ethical professionals realizes their responsibility very well by understanding the importance of providing hassle-free end to end customer service. They will give a courtesy confirmation call prior to delivery. Our customer&apos;s comfort and convenience is our first priority and our systemic delivery and installation program is designed to make the whole job hassle free and brisk. Punctuality, sophistication, and professionalism are practised by every associate of ours who has, in any way, joined hands with us. Our delivery team will keep your personal choices in mind and the placement of furniture will be done according to your wishes. They will unpack all the furniture and furnishing items including bed sheets, cushions, and any other kitchen items ordered. They will remove all packaging before vacuuming up after themselves, and the will leave you alone with the comfort of your apartment with no additional work at all.<p>
        <br /><h3>Why Rent Furniture from Cityfurnish</h3>
        <p>When you rent furniture from Cityfurnish, it comes with a variety of benefits. These benefits are provided to make your furniture rental experience truly hassle-free.</p>
<ul>
<li>We provide free delivery and installation and that too within 72 hours of placing the order</li>
<li>Once you rent the furniture from us, you are also entitled to yearly maintenance and cleaning service</li>
<li>If you change your place within a city or some other city where we provide our services, we provide free relocation for the furniture you rented from us</li>
<li>That&apos;s not all, with rented furniture you also get the flexibility of upgrading the furniture every year, just complete a year and choose the new available designs on our site, and we will provide you a free upgrade on rented furniture without any additional cost</li>
</ul>
<br /><h3>Areas We Cover in Mumbai</h3>
<p>Areas we cover in Mumbai include Powai, Thane, Boriwali, Kandiwali, Lower Parel, Vashi, Andheri, Kalyan, Dombiwali, Dadar, Worli, Colaba, Chembur.</p>
        ';
        $this->data['ListCateogry'] = $this->product_model->category_landing();
        $this->load->view('site/cms/mumbai_furniture',$this->data);
}
public function delhifurniture(){   
        $this->data['heading'] = 'Furniture on Rent in Delhi NCR';
        $this->data['meta_title'] = 'Furniture on Rent in Delhi NCR';
        $this->data['meta_description'] = 'Rent Furniture in Delhi NCR on Easy Monthly Rental Plans. Furniture Rentals was Never So Easy. Free 48 Hours Delivery and Installation. Rent Now and Get 10% Off.';
        $this->data['meta_keyword'] = '
        <h2>Furniture on Rent in Delhi NCR</h2>
        <p>Delhi has been one of the hot spots in our country.  A major portion of Delhi&#39;s population is constituted by young working professionals and students who settle down from various parts of our country. They mostly settle down in the Delhi, Noida or Gurgaon as these cities are one of the IT hubs of the country.</p>
        <p>There lifestyle is very unconventional and these people are completely urban and modern in their approach towards life.  One of the emerging trend is <a hrefs="https://cityfurnish.com">renting furniture</a> and other lifestyle goods instead of buying. </p>
        <p>With Cityfurnish, renting is quick and easy. Our team installs the rented furniture in Delhi swiftly, without any charges. Plus, they will install all your favorite furniture gracefully.</p>
        <br /><h3>Furniture rental in Delhi</h3>
        <p>Furniture renting in Delhi is readily available with us. Be it any part of the city, from the amazing flats of Janakpuri to the streets of  Malviya Nagar, one can easily rent furniture with us.	</p>
        <br /><h3>Why rent furniture in Delhi</h3>
        <p>Renting furniture is one of the best decision, especially when you are living on rent in delhi. It not only saves a lot of money of furnishing your home. It also removes the hassle of relocation and reselling.  Renting is an easy and convenient option for the people who are new to the city. 
And when it comes to renting premium furniture, one does not need to look apart from Cityfurnish</p>
        <br /><h3>Home furniture on rent in Delhi</h3>
        <p>Young Professionals to Young Couple, the first step comes to one&#39;s next chapter of life is to furnish their homes. Living is delhi is not only restricted to bread and butter. One need many things to lead even a simple life. Therefore, having home furniture on rent saves a lot of money which one can spent that on the rest of things. <p>
        <br /><h3>Office furniture on rent in Delhi</h3>
        <p>Delhi is also the hub to the thousands of startups. When you are starting your own venture, you need a lot of capital to be invested at various areas. Spending money buying the office furniture might sound unnecessary. Office furniture costs are an expansive capital venture. To cut down the cost, it is wise to <a hrefs="https://cityfurnish.com/shopby/office-furniture-rental">rent office furniture</a> then buying. It will encourage ventures to spare time, money and furthermore the issue of overseeing bought furniture. </p>
        <br /><h3>Rent Home and Kitchen Appliances</h3>
        <p>Coming to Delhi, starting life on your own is not easy. One need many things to lead a good lifestyle whether its washing machine, fridge or any other appliances.  There when we miss our home where these things are easily available. A simple way to have them and save money is to <a hrefs="https://cityfurnish.com/shopby/appliances-rental">Rent Home Appliances</a> and leave the headache of maintenance and repairs to Cityfurnish.   </p>
<ul>
<li>We provide free delivery and installation and that too within 72 hours of placing the order</li>
<li>Once you rent the furniture from us, you are also entitled to yearly maintenance and cleaning service</li>
<li>If you change your place within a city or some other city where we provide our services, we provide free relocation for the furniture you rented from us</li>
<li>That&apos;s not all, with rented furniture you also get the flexibility of upgrading the furniture every year, just complete a year and choose the new available designs on our site, and we will provide you a free upgrade on rented furniture without any additional cost</li>
</ul>
<br /><h3>Areas We Cover in Mumbai</h3>
<p>Areas we cover in Mumbai include Powai, Thane, Boriwali, Kandiwali, Lower Parel, Vashi, Andheri, Kalyan, Dombiwali, Dadar, Worli, Colaba, Chembur.</p>
        ';
        $this->data['ListCateogry'] = $this->product_model->category_landing();
        $this->load->view('site/cms/delhi_furniture',$this->data);
}



public function bulkorder(){
        $this->data['heading'] = 'Bulk Order';
        $this->data['meta_title'] = 'Cityfurnish | Bulk Order';
        $this->data['meta_description'] = 'Place Bulk Order. Office Furniture, Hotel Furniture, Restaurant Furniture';
        $this->load->view('site/cms/bulkorder',$this->data);

}

	
}
/*End of file cms.php */
/* Location: ./application/controllers/site/product.php */