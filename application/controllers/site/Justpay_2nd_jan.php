<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */
class Justpay extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('security');
	}
	private function esc_string($x){
		return $this->security->xss_clean(mysql_real_escape_string($x));
	}
	function index(){	
		$this->load->view('site/justpay');
	}	
	function getrazorvals(){			
		if(isset($_POST['razorpay_payment_id'])){
		$ch = curl_init();
			$url = "https://api.razorpay.com/v1/payments/".$_POST['razorpay_payment_id']."/capture"; 
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "amount=".$this->session->userdata('cartamount')."");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_USERPWD, "rzp_live_XpgmspjuN5zT4n:4zBywVWXv3TVEUZYDzInGkmI");
			$output = curl_exec ($ch);
			curl_close ($ch);
			redirect(base_url().'order/success/'.$this->session->userdata('cartuserid').'/'.$this->session->userdata('randomNo'));
		}else{
			redirect(base_url().'order/failure/'.$this->session->userdata('cartuserid').'/'.$this->session->userdata('randomNo'));
		}
		//$this->load->view('site/justpay');
	}
}