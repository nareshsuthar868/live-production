<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */
class Ajaxhandler extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('security');
	}
	private function esc_string($x){
		return $this->security->xss_clean(mysql_real_escape_string($x));
	}
	function setcity(){		
		$query = $this->db->query("SELECT list_id FROM ".LIST_VALUES." WHERE id='".$this->esc_string($this->input->post('v'))."' LIMIT 1");
		if($query->num_rows() > 0){
			$this->session->set_userdata('prcity',$this->esc_string($this->input->post('v')));
			$result['ok'] = true;
		}else{
			$result['ok'] = false;
		}	
		echo json_encode($result);
	}
}