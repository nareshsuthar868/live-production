<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */
class Ajaxhandler extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('security');
		//$this->load->library('session');
	}
	private function esc_string($x){
		return $this->security->xss_clean(mysqli_real_escape_string($x));
	}

	function setcity(){		
		$query = $this->db->query("SELECT list_id, list_value FROM ".LIST_VALUES." WHERE id='".$this->input->post('v')."' LIMIT 1");
		if($query->num_rows() > 0){
			$res = $query->first_row();
			$citySlug = $this->generateSeoURL($res->list_value);

			$this->session->set_userdata(array(
				'prcity' => $this->input->post('v'),
				'cityslug' => $citySlug,
				'city_selected' => '0',
				'slugForurl' => 'true'
			));
			$result['ok'] = true;
		}else{
			$result['ok'] = false;
		}	
		echo json_encode($result);
	}
	function createorder(){
		$url = "https://sandbox.juspay.in/order/create";
		$params = array();
		$params['order_id'] = rand(0,1000000000);
		$params['amount'] = 100.00;
		$params['currency'] = "INR";
		$params['customer_id'] = "guest_user_101";
		$params['customer_email'] = "customer@gmail.com";
	    $params['customer_phone'] = "9988665522";
	    $params['product_id'] = "prod-141833";
	    
	    $params['description'] = "Sample description";
		
		$postData = json_encode($params);
		$ch = curl_init();  
		$headers = array();


		$headers[] = 'version: 2016-07-19';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_HEADER, true); 
		curl_setopt($ch, CURLOPT_POST, count($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_USERPWD, "C2C3F51048124A23AD54DB2340FDCA71: ");
		$output=curl_exec($ch);
		curl_close($ch);
		print_r($output);
		$result['ok'] = true;
		echo json_encode($result); 
	}
}