<?php 
$config['id'] = '1'; 
$config['site_contact_mail'] = 'hello@cityfurnish.com'; 
$config['site_contact_number'] = ''; 
$config['email_title'] = 'Cityfurnish'; 
$config['google_verification'] = '<meta name=\'google-site-verification\' content=\'-7HYCsHFSLsnVIKsDD6-2sAPS280EgG3x8SB6Imvk34\' />'; 
$config['google_verification_code'] = ''; 
$config['facebook_link'] = 'http://www.facebook.com/cityfurnish2015'; 
$config['twitter_link'] = 'http://twitter.com/cityfurnish'; 
$config['pinterest'] = ''; 
$config['googleplus_link'] = 'http://google.com'; 
$config['linkedin_link'] = ''; 
$config['rss_link'] = ''; 
$config['youtube_link'] = ''; 
$config['footer_content'] = '&copy;  2016 - 2020 All Rights Reserved by Cityfurnish'; 
$config['logo_image'] = '200x60New1.png'; 
$config['meta_title'] = 'Rent Premium Furniture & Branded Home Appliances | Cityfurnish'; 
$config['meta_description'] = 'Rent Premium Wooden Furniture & Branded Appliances on Easy Monthly Rental. Free Delivery & Installation. We Serve Delhi, Gurgaon, Noida, Ghaziabad, Mumbai, Bangalore & Pune.'; 
$config['fevicon_image'] = 'FaviconNew.png'; 
$config['facebook_api'] = ''; 
$config['facebook_secret_key'] = ''; 
$config['paypal_api_name'] = ''; 
$config['paypal_api_pw'] = ''; 
$config['paypal_api_key'] = ''; 
$config['authorize_net_key'] = ''; 
$config['paypal_id'] = 'gopinath@teamtweaks.com'; 
$config['paypal_live'] = '2'; 
$config['smtp_port'] = '465'; 
$config['smtp_uname'] = 'admin@teamtweaks.com'; 
$config['smtp_password'] = ''; 
$config['consumer_key'] = ''; 
$config['consumer_secret'] = ''; 
$config['google_client_secret'] = ''; 
$config['google_client_id'] = ''; 
$config['google_redirect_url'] = ''; 
$config['google_developer_key'] = ''; 
$config['facebook_app_id'] = ''; 
$config['facebook_app_secret'] = ''; 
$config['like_text'] = 'Favorite'; 
$config['unlike_text'] = 'Unlike'; 
$config['liked_text'] = 'Liked'; 
$config['fancyybox_banner'] = ''; 
$config['sharing_name'] = ''; 
$config['bulk_order_mail'] = 'neerav.jain@cityfurnish.com,sales@cityfurnish.com,ajay.menon@cityfurnish.com'; 
$config['office_furniture_order_emails'] = 'hello@cityfurnish.com, neerav.jain@cityfurnish.com,ajay.menon@cityfurnish.com'; 
$config['us_lead_email'] = 'hello@cityfurnish.com'; 
$config['report_receipt_mail'] = 'hello@cityfurnish.com,piyushk@cityfurnish.com'; 
$config['RentalAmount'] = '300'; 
$config['OrderRentalStatus'] = '1'; 
$config['zoho_books_sales_order_id'] = '6373'; 
$config['payout_expiry_days'] = '3'; 
$config['failed_emails'] = 'hello@cityfurnish.com'; 
$config['header_code_snippet'] = '<link rel=\"canonical\" href=\"https://cityfurnish.com\" />'; 
$config['weekly_report_email'] = 'ops@cityfurnish.com,ayush.pathak@cityfurnish.com,neerav.jain@cityfurnish.com,saurabh.gupta@cityfurnish.com,piyushk@cityfurnish.com,ankita.goswami@cityfurnish.com'; 
$config['created'] = '2013-07-17'; 
$config['modified'] = '2019-12-24'; 
$config['admin_name'] = 'admin'; 
$config['email'] = 'hello@cityfurnish.com'; 
$config['admin_type'] = 'super'; 
$config['privileges'] = 'a:10:{s:5:\"users\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}s:8:\"category\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}s:11:\"subcategory\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}s:7:\"product\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}s:6:\"slider\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}s:5:\"video\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}s:3:\"cms\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}s:5:\"order\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}s:10:\"statistics\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}s:10:\"newsletter\";a:4:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";}}'; 
$config['last_login_date'] = '2019-12-24 11:23:41'; 
$config['last_logout_date'] = '2019-12-14 03:57:10'; 
$config['last_login_ip'] = '182.68.172.205'; 
$config['is_verified'] = 'Yes'; 
$config['status'] = 'Active'; 
$config['cat_roles'] = ''; 
$config['common_prefix'] = 'socktail-'; 
$config['https_enabled'] = 'yes'; 
$config['base_url'] =  'http://45.113.122.221/production/';  ?>