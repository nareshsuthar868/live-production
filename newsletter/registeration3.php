<?php $message .= '<p>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<title>Welcome Cityfurnish</title>
</p>
<style type=\"text/css\"><!--
  @media screen and (max-width: 580px) '.
    .tab-container'.max-width: 100%;.'
    .txt-pad-15 '.padding: 0 15px 51px 15px !important;.'
    .foo-txt '.padding: 0px 15px 18px 15px !important;.'
    .foo-add '.padding: 20px !important;.'
    .tab-padd-zero'.padding:0px !important;.'
	.tab-padd-right'.padding-right:25px !important;.'
	.pad-20'.padding:25px 20px 20px !important;.'
	.social-padd-left'.padding:15px 20px 0px 0px; !important;.'
	.offerimg'.width:100% !important;.'
	.mobilefont'.font-size:16px !important;line-height:18px !important;.'
  .'
--></style>
<table class=\"tab-container\" name=\"main\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 620px; background-color: #fff; margin: 0 auto; background: #90c5bc; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; width: 620px;\">
<tbody>
<tr style=\"background-color: #fff;\">
<td style=\"width: 100%;\">
<table style=\"width: 100%; table-layout: fixed;\" cellpadding=\"0\" cellspacing=\"0px\">
<tbody>
<tr>
<td style=\"text-align: left; padding: 15px 0px 15px 20px;\"><a href=\"https://cityfurnish.com/\"><img src=\"https://cityfurnish.com/images/email/logo-2.png\" alt=\"logo\" style=\"width: 150px;\" /></a></td>
<td style=\"text-align: right; padding: 15px 20px 15px 0px; width: 50%;\" class=\"social-padd-left\"><a href=\"https://www.facebook.com/cityFurnishRental\" style=\"display: inline-block; margin: 5px 0px 0px 15px;\"><img src=\"https://cityfurnish.com/images/email/facebook.png\" alt=\"facebook\" width=\"18px\" /></a> <a href=\"https://twitter.com/CityFurnish\" style=\"display: inline-block; margin: 5px 0px 0px 15px;\"><img src=\"https://cityfurnish.com/images/email/twitter.png\" alt=\"twitter\" width=\"18px\" /></a> <a href=\"https://plus.google.com/+cityfurnish\" style=\"display: inline-block; margin: 5px 0px 0px 15px;\"><img src=\"https://cityfurnish.com/images/email/google-plus.png\" alt=\"google\" height=\"18px\" /></a> <a href=\"https://in.pinterest.com/cityfurnish/\" style=\"display: inline-block; margin: 5px 0px 0px 15px;\"><img src=\"https://cityfurnish.com/images/email/pintrest.png\" alt=\"pintrest\" width=\"18px\" /></a> <a href=\"https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym\" style=\"display: inline-block; margin: 5px 0px 0px 15px;\"><img src=\"https://cityfurnish.com/images/email/linkedin.png\" alt=\"linkedin\" width=\"18px\" /></a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"background: #dbdbdb; display: table-row !important;\">
<td style=\"width: 100%;\">
<table style=\"padding: 20px 20px 12px; width: 100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr style=\"background-color: #dbdbdb; width: 100%; display: table-row !important;\">
<td style=\"padding: 37px 10px 39px 25px; text-align: left; background: #90c5bc; vertical-align: top; width: 38%; color: #fff; font-family: Times New Roman;\" class=\"foo-add-left\"><span style=\"height: 5px; display: inline-block; margin-bottom: 5px;\">Your referral code : %cfreferralcode</span></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"background: #dbdbdb; display: table-row !important;\">
<td style=\"width: 100%;\">
<table style=\"padding: 20px 20px 12px; width: 100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr style=\"background-color: #dbdbdb; width: 100%; display: table-row !important;\">
<td style=\"padding: 37px 10px 39px 25px; text-align: left; background: #90c5bc; vertical-align: top; width: 38%; color: #fff; font-family: Times New Roman;\" class=\"foo-add-left\">
<h1 style=\"color: #fff; text-transform: uppercase; font-size: 26px; line-height: 32px; font-weight: bold; margin: 0px 0px 16px;\" class=\"mobilefont\">Welcome to cityfurnish</h1>
<span style=\"height: 5px; width: 76px; background: #fff; display: inline-block; margin-bottom: 5px;\">&nbsp;</span>
<p style=\"font-family: Arial, Helvetica, sans-serif; line-height: 22px; color: #fff; font-size: 14px;\" class=\"cont_text\"><span>Thanks for Signing up! Verify your account by pressing the following button</span>.</p>
<a href=\"%cfmurl\" style=\"background: #fff; line-height: 45px; display: inline-block; width: 150px; text-align: center; font-size: 12px; text-transform: uppercase; color: #90c5bc; text-decoration: none; font-weight: bold; margin-top: 10px;\">Let\'s&nbsp; GET started</a></td>
<td style=\"vertical-align: middle; width: 55%; margin-top: 0px; background: #90c5bc;\" class=\"foo-add-left\"><img src=\"https://cityfurnish.com/images/email/welcome--top-pic.png\" alt=\"Welcome Pic\" style=\"width: 100%; display: inline-block; vertical-align: middle;\" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"background-color: #dbdbdb; display: table-row !important;\">
<td style=\"padding: 0px 20px 20px; width: 100%;\">
<table style=\"width: 100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"padding: 0px 12px 0px 0px; width: 48%;\" class=\"foo-add-left-offer\"><img src=\"https://cityfurnish.com/images/email/offer-left-pic.png\" alt=\"offer\" style=\"width: 100%; display: inline-block; vertical-align: middle;\" class=\"offerimg\" /></td>
<td style=\"font-family: \'Times New Roman\', Times, serif; padding: 0px 30px; vertical-align: middle; background-color: #ff9630; text-align: center; color: #fff; width: 48%;\" class=\"foo-add-left-offer\">
<h1 style=\"color: #fff; text-transform: uppercase; font-size: 20px; line-height: 24px; font-weight: bold; margin: 15px 0px 25px;\" class=\"summer_title\">Summer Offers</h1>
<p style=\"margin: 5px 0px; line-height: 20px; font-family: Arial, Helvetica, sans-serif; color: #fff;\" class=\"cont_text\">Upto 100% off on first<br />month rent! Offer ending tonight.</p>
<a href=\"https://cityfurnish.com/pages/offers\" style=\"background: #fff; line-height: 40px; display: inline-block; width: 140px; text-align: center; font-size: 12px; text-transform: uppercase; color: #ff9630; text-decoration: none; font-weight: bold; margin: 10px 0px;\">Know More </a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"background: #fff; width: 100%; display: table-row !important;\">
<td style=\"padding: 26px 50px; text-align: center; line-height: 24px; width: 100%;\" colspan=\"2\" class=\"pad-20\">
<p style=\"margin: 0px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: #b2b2b2;\">Sent by <a href=\"https://cityfurnish.com/\" style=\"color: #38373d; text-decoration: none;\">Cityfurnish</a>, 525-527, Block D, JMD Megapolis, Sohna Road, Sector 48, Gurgaon, Haryana , 122018<br /> <a href=\"mailto:hello@cityfurnish.com\"><img src=\"https://cityfurnish.com/images/email/email.png\" alt=\"email\" /></a><a href=\"tel:8010845000\" style=\"margin-left: 8px;\"><img src=\"https://cityfurnish.com/images/email/call.png\" alt=\"email\" /></a></p>
</td>
</tr>
</tbody>
</table>';  ?>