<?php $message .= '<p>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<title>Order Confirmation</title>
</p>
<style type=\"text/css\"><!--
@media screen and (max-width: 580px) '.
.tab-container '.
  max-width: 100%;
.'
.txt-pad-15 '.
  padding: 0 15px 51px 15px !important;
.'
.foo-txt '.
  padding: 0px 15px 18px 15px !important;
.'
.foo-add '.
  padding: 15px 15px 0px 15px !important;
.'
.foo-add-left '.
  width: 100% !important;
.'
.foo-add-right '.
  width: 100% !important;
.'
.pad-bottom-15 '.
  padding-bottom: 15px !important;
.'
.pad-20 '.
  padding: 25px 20px 20px !important;
.'
.'
--></style>
<table class=\"tab-container\" name=\"main\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 850px; background-color: #fff; margin: 0 auto; table-layout: fixed; background: #dbdbdb; font-family: Arial, Helvetica, sans-serif; font-size: 15px;\">
<tbody>
<tr>
<td style=\"padding: 20px;\">
<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"text-align: left; width: 100%; padding-bottom: 25px; background: #90c5bc; padding: 20px;\"><a href=\"https://cityfurnish.com/\"><img src=\"https://cityfurnish.com/images/email/logo.png\" alt=\"logo\" /></a></td>
</tr>
</tbody>
</table>
<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"text-align: left; width: 100%; background: #f5f5f5; padding: 25px 30px 30px;\">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"text-align: left; width: 100%; padding-bottom: 10px;\">
<p style=\"font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 10px; font-weight: bold; font-size: 15px; color: #656565;\">Hi '.'.USER_NAME.'.'</p>
<p style=\"font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 10px; font-size: 15px; color: #656565;\">Thank you for your order.</p>
'.'.PAYMENT_LINK.'.'</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border: 1px solid #dddddd; background-color: #fff; margin-bottom: 15px; border-color: #ddd;\">
<tbody>
<tr>
<td style=\"text-align: left; padding: 15px 20px; width: 50%;\">
<p style=\"font-family: Arial, Helvetica, sans-serif; margin: 0px; font-weight: bold; font-size: 12px; color: #656565;\"><strong style=\"font-weight: 600;\">Order Id</strong><span style=\"font-weight: 400;\"> : #'.'.INVOICE_NUMBER.'.'</span></p>
</td>
<td style=\"text-align: left; padding: 15px 20px; border-left: 1px solid #ddd; width: 50%; border-color: #ddd;\">
<p style=\"font-family: Arial, Helvetica, sans-serif; margin: 0px; font-weight: bold; font-size: 12px; color: #656565;\"><strong style=\"font-weight: 600;\">Order Date </strong><span style=\"font-weight: 400;\"> : '.'.INVOICE_DATE.'.'</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border: 1px solid #dddddd; background-color: #fff; margin-bottom: 15px; border-color: #ddd;\">
<tbody>
<tr>
<td style=\"text-align: left; padding: 15px 20px; width: 100%;\">
<p style=\"font-family: Arial, Helvetica, sans-serif; margin: 0px; font-weight: bold; font-size: 12px; color: #656565;\"><strong style=\"font-weight: 600;\">Shipping Details</strong></p>
</td>
</tr>
<tr>
<td style=\"text-align: left; padding: 15px 20px; border-top: 1px solid #ddd; width: 100%; border-color: #ddd;\">
<p style=\"font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 17px; font-weight: bold; font-size: 12px; color: #656565;\"><strong style=\"font-weight: 600;\">Full Name</strong> <span style=\"font-weight: 400;\"> : '.'.FULL_NAME.'.'</span></p>
<p style=\"font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 17px; font-weight: bold; font-size: 12px; color: #656565;\"><strong style=\"font-weight: 600;\">Address</strong> <span style=\"font-weight: 400;\"> : '.'.ADDRESS1.'.'</span></p>
<p style=\"font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 17px; font-weight: bold; font-size: 12px; color: #656565;\"><strong style=\"font-weight: 600;\">City</strong> <span style=\"font-weight: 400; display: inline-block; margin-right: 75px;\"> : '.'.CITY.'.'</span> <strong style=\"font-weight: 600;\">State</strong> <span style=\"font-weight: 400; display: inline-block; margin-right: 75px;\"> : '.'.STATE.'.'</span> <strong style=\"font-weight: 600;\">Zip Code</strong> <span style=\"font-weight: 400; display: inline-block;\"> : '.'.ZIP_CODE.'.'</span></p>
<p style=\"font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 17px; font-weight: bold; font-size: 12px; color: #656565;\"><strong style=\"font-weight: 600;\">Phone Number</strong> <span style=\"font-weight: 400;\"> : '.'.PHONE.'.'</span></p>
</td>
</tr>
</tbody>
</table>
'.'.DYNAMIC_TABLE.'.'  
<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border: 1px solid #dddddd; background-color: #fff; margin-bottom: 15px; color: #656565; border-color: #ddd;\">
<tbody>
<tr>
<td align=\"center\" style=\"color: #656565; padding: 20px 5px; border-right: 1px solid #ddd; font-size: 11px; font-weight: 600; border-color: #ddd; width: 100px;\">Product Images</td>
<td align=\"center\" style=\"color: #656565; padding: 20px 5px; border-right: 1px solid #ddd; font-size: 11px; f\\ont-weight: 600; border-color: #ddd; width: 120px;\">Product Name</td>
<td align=\"center\" style=\"color: #656565; padding: 20px 5px; border-right: 1px solid #ddd; font-size: 11px; font-weight: 600; border-color: #ddd; width: 35px;\">QTY</td>
<td align=\"center\" style=\"color: #656565; padding: 20px 5px; border-right: 1px solid #ddd; font-size: 11px; font-weight: 600; border-color: #ddd; width: 120px;\">Security Deposite</td>
<td align=\"center\" style=\"color: #656565; padding: 20px 5px; border-right: 1px solid #ddd; font-size: 11px; font-weight: 600; border-color: #ddd; width: 110px;\">Rental</td>
<td align=\"center\" style=\"color: #656565; padding: 20px 5px; font-size: 12px; font-weight: 600; width: 120px;\">Sub Total</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"background: #fff; width: 100%; display: table-row !important; color: #fff;\">
<td style=\"padding: 20px 15px; text-align: center; line-height: 30px; width: 100%; background: #fff;\" class=\"foo-add\">
<p style=\"margin: 0px; font-family: Arial, Helvetica, sans-serif; color: #989898; font-size: 14px;\">If you have any concern please contact us.</p>
<p style=\"margin: 0px; font-family: Arial, Helvetica, sans-serif; color: #38373d; font-size: 14px;\">Email : <a href=\"mailto:hello@cityfurnish.com\" style=\"color: #38373d; display: inline-block; text-decoration: none; font-family: Arial, Helvetica, sans-serif;\">hello@cityfurnish.com</a> |                  Mobile: <a href=\"mailto:hello@cityfurnish.com\" style=\"color: #38373d; display: inline-block; text-decoration: none; font-family: Arial, Helvetica, sans-serif;\">+91 8010845000</a></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>';  ?>