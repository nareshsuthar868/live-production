	//Here is my custom js for only bd admin panel



	$('#displayCommissions').ready(function(){
        $('#commissions_table').DataTable({
            processing:  true,
            serverSide: true,
            ajax:  baseURL + 'bdadmin/commission/get_commissions',
            "deferRender": true,
            "pagingType": "full_numbers",
            responsive: true,
            order: [],
            columns: [
    			null,
                { "data": "full_name" },
                { "data": "email" },
                { "data": "total_commission" },
				{ "data": "user_type" },
				{ "data": "status" },
    			{ "data": "Action"},
            ],
            columnDefs: [
            	{
    				orderable: false, targets: [0],
                    "render": function ( data, type, full, meta ) {
    					var link = '<input name="checkbox_id[]" type="checkbox" value="'+ full.id +'" class="case" onclick="change_all()">';
                        return link;
                    }
                },
                {
    				orderable: false, targets: [6],
                    "render": function ( data, type, full, meta ) {
                       var button = '<span><a class="action-icons c-suspend"  onclick="ShowCommissions('+ full.user_id +')" title="View">View Commissions</a></span>';
                        return button;
                    }
                },
       
            {
                orderable: false, targets: [0,3,4,6],
            }],
            language: {
                searchPlaceholder: "Search By Email"
            },
            fnDrawCallback: function (oSettings) {
            }
    	});
	});

	function ShowCommissions(user_id){
        // alert("dff");
        $('#model_css123').addClass('show-modal');
        $.ajax({
	        type: 'POST',
	        url: baseURL + 'bdadmin/commission/get_users_leads',
	        dataType: 'json',
	        data: {
	            'user_id': user_id
	        },
	        success:function(response){
	            var html = '';
	            if(response.data.length > 0){
	            	$('#total_leads').html(response.data.length);
	                for (var i = response.data.length - 1; i >= 0; i--) {
	                	var cm_p = (response.data[i].cm_percentage == null) ? '--' : response.data[i].cm_percentage+'%';
	                    html += '<tr>\
	                    <td>'+response.data[i].order_id+'</td>\
	                    <td>'+response.data[i].full_name+'</td>\
	                    <td>'+response.data[i].email+'</td>\
	                    <td>'+response.data[i].phone_no+'</td>\
	                    <td>'+ cm_p +'</td>\
	                    <td>'+response.data[i].cm_rate+'</td>';
	                    if(response.data[i].lead_status == 'delivered'){
	                      html +=  '<td><label style="color:green">'+response.data[i].lead_status.toUpperCase()+'</label></td>';
	                    }else{
	                       html += '<td><label style="color:red">'+response.data[i].lead_status.toUpperCase()+'</label></td>';
	                    }
	                    html +=  '<td>'+response.data[i].comission_status+'</td>\
	                    <td>'+response.data[i].order_date+'</td>';
	                }
	            }else{
	                html += '<tr><td colspan="7" class="no-data-available">No data!</td></tr>';
	            }

	            $('#payout_table_body').html(html);
	        }
    	}); 
    }


    //Show Lead Request Data Table
    $('#LeadRequestForm').ready(function(){
    	$('#userListTbl123').DataTable({
	        processing:  true,
	        serverSide: true,
	        ajax: baseURL + 'bdadmin/lead_request/get_leadlisting',
	        "deferRender": true,
	        "pagingType": "full_numbers",
	        responsive: true,
	        order: [7,'desc'],
	        columns: [
				{"data": "order_id"},
				{ "data": "owner_name" },
	            { "data": "owner_email" },
	            { "data": "full_name" },
	            { "data": "email" },
	            { "data": "phone_no" },
	            { "data": "city" },
	            { "data": "lead_status" },
	            { "data": "cm_status" },
	            { "data": "created" },
				{ "data": "Action"},
	        ],
	        columnDefs: [

	            {
					orderable: true, targets: [1],
	                "render": function ( data, type, full, meta ) {
	                    var href = baseURL+'bdadmin/users/view_user/'+full.partner_id;
	                    if(full.user_type == 'BD Executive'){
	                        href = baseURL+'bdadmin/bdusers/view_user/'+full.partner_id;
	                    }
	                    var value  = '<a href='+href+' target="_blank"><b>'+ data + '</b></a>';
	                    return value;
	                }
	            },
	            {
					orderable: false, targets: [2],
	                "render": function ( data, type, full, meta ) {
	                    var value  = '<b>'+ data + '</b>';
	                    return value;
	                }
	            },
	            {
					orderable: false, targets: [3],
	                "render": function ( data, type, full, meta ) {
	                    var value  = full.first_name + ' ' + full.last_name;
	                    return value;
	                }
	            },
	            {
	                orderable: true, targets: [7],
	                "render": function ( data, type, full, meta ) {
	                    var value  = '<font style=color:green><b>'+data.toUpperCase()+'</b></font>';
	                    if(data == 'Pending' || data == 'pending' ){
	                        value = '<font style=color:red><b>'+data.toUpperCase()+'</b></font>';
	                    }
	                    return value;
	                }
	            },	  
	            {
	                orderable: true, targets: [8],
	                "render": function ( data, type, full, meta ) {
	                	if(data != '' && data != null){
		                    var value  = '<font style=color:green><b>'+data.toUpperCase()+'</b></font>';
		                    if(data == 'pending'){
		                        value = '<font style=color:red><b>'+data.toUpperCase()+'</b></font>';
		                    }
		                    return value;
	                	}else{
	                		return '--';
	                	}
	                }
	            },	  
	            {
					orderable: false, targets: [10],
	                "render": function ( data, type, full, meta ) {
	                    var image_url = baseURL + "images/partner_app/profile_pic/"+full.profile_pic;
	                    var button = '';
	                    if(full.lead_status == 'pending' || full.lead_status == 'Pending'){
		                    button += '<span class="action-icons c-add"  original-title="Link Order" \
		                    onclick="showLinkOrderForm('+full.id+')"></span>';  	
	                    }
	                    if(full.cm_status == 'pending' || full.cm_status == 'Pending'){
	                       button += '<span class="action-icons c-approve"  original-title="Mark As Paid" \
		                    onclick="MarkAsPaid('+full.id+')"></span>';  		
	                    }


	                    button += '<span class="action-icons c-edit"  original-title="View Requirements" \
	                    onclick="ShowLeadRequirements(\''+ image_url +'\',\''+ full.city +'\',\''+ full.requirements +'\')"></span>';

	                    return button;
	                }
	            },
	   
	        {
	            orderable: false, targets: [0],
	        }],
	        language: {
	            searchPlaceholder: "Search By Email"
	        },
	        fnDrawCallback: function (oSettings) {
	        }
    	});
    })


  	function ShowLeadRequirements(profile,city,requiremnet){
 	    $('#other_allData').show();
  		$('#details_show').show();
  		$('#add_form_show').hide();
		$("#profile_pic").attr("src",profile);
     	$('#model_css').addClass('show-modal');
     	$('#city_lead').html(city);
 		$('#lead_reqirenment').html(requiremnet);
 	    $('#export_model').hide();
	 	// $('#LeadRequirements').text(city);
	}
	
	function showexportForm(profile,city,requiremnet){
        $('#export_model').show();
        $('#other_allData').hide();
        $('#model_css').addClass('show-modal');
    }



    function showLinkOrderForm(lead_id){
    	// alert("here i am clicked");
     	$('#other_allData').show();
    	$('#details_show').hide();
        $('#export_model').hide();
  		$('#add_form_show').show();
     	$('#model_css').addClass('show-modal');
     	$('#lead_id').val(lead_id);
    }

    function LinkOrderWithLead(){
	 	if($('#bdorder_id').val().trim() == ''){
        	alert('Please Enter Order ID'); $('#bdorder_id').focus();
    	}else if($('#bdcommission_percentage').val().trim() == ''){
        	alert('Please Enter Commissions Percentage'); $('#bdcommission_percentage').focus();
     	}else if($('#bdcommission_rate').val().trim() == ''){
         	alert('Please Enter Commissions Rate'); $('#bdcommission_rate').focus();
     	}else{
        	var res = window.confirm('Are you sure ?');
        	if(res){
            	$.ajax({
                	type: 'POST',
                	url: baseURL + 'bdadmin/lead_request/convert_approve_lead',
                	dataType: 'json',
                	data: {
                    	'order_id': $('#bdorder_id').val(),
                    	'commission_percentage':$('#bdcommission_percentage').val(),
                    	'commission_rate':$('#bdcommission_rate').val(),
                    	'status':$('#bdstatus').val(),
                    	'lead_id':$('#lead_id').val()
                	},
                	beforeSend:function(){
                    	$('#cashgram_button').attr('disabled','disabled');
                     	$('#admin_loader').show();
                	},
                	success:function(response){
                		if(response.status == 200){
                    		location.reload();
                		}else{
                			alert(response.message);
                			$('#cashgram_button').removeAttr('disabled','disabled');
                		}
                		 $("#lead_link_form").trigger("reset");
                    }
            	});  
        	}
     	}    
   	}

   	//Mark Lead as paid
   	function MarkAsPaid(id){
  
	    var cfm = window.confirm('Are you want to mark this lead commission as paid ?.');
	    if (cfm) {
	        // var cid = $(evt).data('cid');
	        $.ajax({
	            type: 'post',
	            url: baseURL + 'bdadmin/lead_request/mark_as_paid',
	            data: {
	                'id': id
	            },
	            dataType: 'json',
	            success: function(json) {
	          		alert("Update successfully");
	          		location.reload();
	            }
	        });
	    }
   	}


// alert(baseURL);
    //Cms listing 
    $('#cms_lisitingID').ready(function(){
        $('#cms_listing').DataTable({
            processing:  true,
            serverSide: true,
            ajax:  baseURL + 'bdadmin/setting/get_cms_pages',
            "deferRender": true,
            "pagingType": "full_numbers",
            responsive: true,
            order: [],
            columns: [
                { "data": "page_name" },
                { "data": "page_content" },
                { "data": "updated_date" },
                { "data": "created_date" },
    			{ "data": "Action"},
            ],
            columnDefs: [
            	{
    				orderable: false, targets: [1],
                    "render": function ( data, type, full, meta ) {
                        return data.slice(0, 100) + (data.length > 100 ? "..." : "");
                    }
                },
                {
    				orderable: false, targets: [4],
                    "render": function ( data, type, full, meta ) {
                       var button = '<span><a class="action-icons c-edit" href="bdadmin/setting/edit_cms/'+ full.id +'"  title="Edit Cms"></a></span>';
                        return button;
                    }
                },
       
            {
                orderable: false, targets: [0],
            }],
            language: {
                searchPlaceholder: "Search By Name"
            },
            fnDrawCallback: function (oSettings) {
            }
    	});
	});



 	//Cms listing 
    $('#offer_listing').ready(function(){
        $('#offer_listing').DataTable({
            processing:  true,
            serverSide: true,
            ajax:  baseURL + 'bdadmin/setting/get_offer_listing',
            "deferRender": true,
            "pagingType": "full_numbers",
            responsive: true,
            order: [],
            columns: [
                { "data": "coupon_tenure" },
                { "data": "coupon_desc" },
                { "data": "price" },
                { "data": "created_at" },
    			{ "data": "Action"},
            ],
            columnDefs: [
                {
    				orderable: false, targets: [4],
                    "render": function ( data, type, full, meta ) {
                       var button = '<span><a class="action-icons c-edit" href="bdadmin/setting/edit_offer/'+ full.id +'"  title="Edit Offer"></a></span>';
                       button += '<span><a class="action-icons c-delete" href="javascript:confirm_delete(\'bdadmin/setting/delete_offer/'+ full.id +'\')"  title="Delete Offer"></a></span>';
                        return button;
                    }
                },
       
            {
                orderable: false, targets: [0],
            }],
            language: {
                searchPlaceholder: "Search By Name"
            },
            fnDrawCallback: function (oSettings) {
            }
    	});
	});

    function ShowbdRevenue(user_id){
        $('#model_css123').addClass('show-modal');
        $.ajax({
            type: 'POST',
            url: baseURL + 'bdadmin/commission/get_revenue',
            dataType: 'json',
            data: {
                'user_id': user_id
            },
            success:function(response){
                var revenue = '<td>Rs '+response.my_revenue+'</td><td>Rs '+response.broker_revenue+'</td><td><b>Rs '+response.total_revenue+'</b></td>';                   
                var orders = '<td>'+response.my_orders+'</td><td>'+response.broker_orders+'</td><td><b>'+response.total_orders+'</b></td>';
                $('#revenue').html(revenue);
                $('#orders').html(orders);  
            }
        }); 

    }


