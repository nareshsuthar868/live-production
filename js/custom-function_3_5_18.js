$(document).ready(function(){
	// sticky menu
	$(window).scroll(function() {
		if ($(this).scrollTop() > 10){  
		 	$('header').addClass("sticky");
		}
		else{
			$('header').removeClass("sticky");
		}	
	});
	$(function() {
		$('body').on('click', '.scrolldown a', function(event) {
			var $anchor = $(this);
			var topoffset = 90;
			$('html, body').animate({
				scrollTop: $($anchor.attr('href')).offset().top-topoffset+2
			}, 1500, 'easeInOutExpo');
			event.preventDefault();
		});
	});
	
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 300) {
			
			$('.back_to_top').fadeIn();
		} else {
			$('.back_to_top').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.back_to_top').click(function(){
		//alert("hello");
		$('html, body').animate({scrollTop : 0},500);
		return false;
	});
	// bootstrap accordian function
	function toggleIcon(e) {
	$(e.target)
		.prev('.panel-heading')
		.find(".more-less")
		.toggleClass('expand-down expand-up');
	}
	$('.panel-group').on('hidden.bs.collapse', toggleIcon);
	$('.panel-group').on('shown.bs.collapse', toggleIcon);
    
	if($(window).width() <= 767){ 
    	$('.panel-collapse').removeClass('in');
	}
	// bootstrap menu responsive
	$("a.togglemenu").click(function(event){
		event.stopPropagation();
		$("nav").toggleClass("intro");
		$(".mobileoverlay").toggleClass("overvisible");
	});
	$(".mobileoverlay").on('click', function(){
		$("nav").removeClass("intro");
		$(this).removeClass("overvisible");
	});
	
	// header select city dropdown
	$(".dropdown-menu li a").click(function(){
		var selText = $(this).text();
		$(this).parents('.btn-group').find('.dropdown-toggle').html('<span>'+ selText +'</span>'+'<i class="material-icons">keyboard_arrow_down</i>');
	});
	
	// tab slider timeout function
	$( ".nav-tabs" ).on( "click", "li a", function(e) {
		setTimeout(function(){ $('.tabcenterslider').slick('setPosition');}, 200)
	});
	
	
	//cart sidebar 
	$(".carticn").click(function(){
 	    //event.stopPropagation();
		$(".cartsidebar").addClass("sideright");
		$(".mobileoverlay").toggleClass("overvisible");
	});
	$(".cartcross,.mobileoverlay").click(function(){
		$(".cartsidebar").removeClass("sideright");
		$('.cartcross').removeClass("overvisible");	
		$('.mobileoverlay').removeClass("overvisible");	
	}); 
	
	   // This button will increment the value
       var quantitiy=0;
	   $('.quantity-right-plus').click(function(e){
			
			// Stop acting like a button
			e.preventDefault();
			// Get the field name
			var quantity = parseInt($('#quantity').val());
			
			// If is not undefined
				
				$('#quantity').val(quantity + 1);
			  // Increment
			
		});
	
		 $('.quantity-left-minus').click(function(e){
			// Stop acting like a button
			e.preventDefault();
			// Get the field name
			var quantity = parseInt($('#quantity').val());
			
			// If is not undefined
		  
				// Increment
				if(quantity>0){
				$('#quantity').val(quantity - 1);
				}
		});
	  
	  // This button will increment the value
       var quantitiy=0;
	   $('.quantity-right-plus').click(function(e){
			
			// Stop acting like a button
			e.preventDefault();
			// Get the field name
			var quantity = parseInt($('#quantity1').val());
			
			// If is not undefined
				
				$('#quantity1').val(quantity + 1);
			  // Increment
			
		});
		 $('.quantity-left-minus').click(function(e){
			// Stop acting like a button
			e.preventDefault();
			// Get the field name
			var quantity = parseInt($('#quantity1').val());
			
			// If is not undefined
		  
				// Increment
				if(quantity>0){
				$('#quantity1').val(quantity - 1);
				}
		});
		
		
		// Cart incroment decrement function
		$('.btn-number').click(function(e){
		e.preventDefault();
		
		fieldName = $(this).attr('data-field');
		type      = $(this).attr('data-type');
		var input = $("input[name='"+fieldName+"']");
		var currentVal = parseInt(input.val());
		if (!isNaN(currentVal)) {
		if(type == 'minus') {
			
			if(currentVal > input.attr('min')) {
				input.val(currentVal - 1).change();
			} 
			if(parseInt(input.val()) == input.attr('min')) {
				$(this).attr('disabled', true);
			}
		
		} else if(type == 'plus') {
		
			if(currentVal < input.attr('max')) {
				input.val(currentVal + 1).change();
			}
			if(parseInt(input.val()) == input.attr('max')) {
				$(this).attr('disabled', true);
			}
		
		}
		} else {
		input.val(0);
		}
		});
		$('.input-number').focusin(function(){
		$(this).data('oldValue', $(this).val());
		});
		$('.input-number').change(function() {
		
		minValue =  parseInt($(this).attr('min'));
		maxValue =  parseInt($(this).attr('max'));
		valueCurrent = parseInt($(this).val());
		
		name = $(this).attr('name');
		if(valueCurrent >= minValue) {
		$(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
		} else {
		alert('Sorry, the minimum value was reached');
		$(this).val($(this).data('oldValue'));
		}
		if(valueCurrent <= maxValue) {
		$(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
		} else {
		alert('Sorry, the maximum value was reached');
		$(this).val($(this).data('oldValue'));
		}
		
		
		});
		$(".input-number").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
			 // Allow: Ctrl+A
			(e.keyCode == 65 && e.ctrlKey === true) || 
			 // Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
				 // let it happen, don't do anything
				 return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
		});
		
		
	// login from tab active function	
   /* $('.signbottom').click(function() {
    	 $('.tabsignin').addClass('active');
		 $('.tabsignup').removeClass('active');
	});
    $('.signupbottom').click(function() {
    	 $('.tabsignup').addClass('active');
		 $('.tabsignin').removeClass('active');
    });*/

    $('.tabsignup').click(function(e) {
		e.preventDefault();
		$('.popup-left span').show();
	});
	
	$('.tabsignin').click(function(e) {
		e.preventDefault();
		$('.popup-left span').hide();
	}); 
	
	$('.forgot-pass').click(function(e) {
		e.preventDefault();
		$('.login-block').hide();
		$('.popup-left span').hide();
		$('.forgot-pass-block').show();
	});
	
	$('.backtologin').click(function(e) {
		e.preventDefault();
		$('.login-block').show();
		$('.forgot-pass-block').hide();
    }); 
	
	
	 // read more expand function 
	   $('article.qulitysec').readmore({
		   speed: 500,
		   collapsedHeight: 50,
		   moreLink: '<a href="javascript:void(0)" class="readmorebtn">Read More<i class="material-icons">keyboard_arrow_down</i></a>',
		   lessLink: '<a href="javascript:void(0)" class="readmorebtn">Read Less<i class="material-icons icnup">keyboard_arrow_up</i></a>',
		});
	
	
	// product month UI slider
	$("#monthslider, #monthslider2").slider({
		tooltip: 'always',
		//ticks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
		//ticks_labels:[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
	});
	

	// tab center responsive 
	$('.tabcenterslider').slick({
		centerMode: true,
		centerPadding: '0px',	
		slidesToShow:3,
		focusOnSelect: true,
		arrows:false
	});
		
	$('.product-slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.product-thumb'
	});

	$('.product-thumb').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.product-slider-for',
		dots: false,
		focusOnSelect: true,
		responsive: [
	
			
			{
				breakpoint: 650,
				settings: {
				slidesToShow: 4,
				slidesToScroll: 2
				}
			},
			{
				breakpoint: 599,
				settings: {
				slidesToShow: 3,
				slidesToScroll: 1
				}
			}
		]
	});

	$('.product-slider-full').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.product-full-thumb'
	});

	$('.product-full-thumb').slick({
		slidesToShow: 8,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.product-slider-full',
		dots: false,
		infinite: true,
		focusOnSelect: true,
		responsive: [
	
			{
				breakpoint: 650,
				settings: {
				slidesToShow: 4,
				slidesToScroll: 2
				}
			},
			{
				breakpoint: 599,
				settings: {
				slidesToShow: 3,
				slidesToScroll: 1
				}
			}
		]
	});
	

	// category mobile slider fucntion	
	$('.categorymobile').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		infinite: false,
	    responsive: [
			{
				breakpoint: 599,
				settings: {
				slidesToShow: 4,
				slidesToScroll: 1
				}
			},
				{
				breakpoint: 479,
				settings: {
				slidesToShow: 3,
				slidesToScroll: 1
				}
			}
		]
	});
	 $('.clientslider').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 2,
		  slidesToScroll: 1,
		  responsive: [
			{
			  breakpoint: 767,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				centerMode: true,
				arrows: false,
				centerPadding: '80px'
			  }
			},
			{
			  breakpoint: 415,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				centerMode: true,
				arrows: false,  
				centerPadding: '40px'
			  }
			}
			
		  ]
		});

    var slider = $("div#mySliderTabs").sliderTabs({
        autoplay: false,
        position: "top"
});

    $('.addon-title .material-icons').click(function() {
        $("#myModaladdon").removeClass("in");
    });
	
    $('.product-detail a').click(function() {
        $(this).toggleClass('added');
        $('.add-cart-popup').addClass('mobile-cart');
        setTimeout(function(){
            $('.add-cart-popup').removeClass('mobile-cart');
        },2000);
    });
    
});

$(window).on("load", function() {
	$('#my-Modal').modal('show');
}); 

$(window).load(function() {
	var win_width = $(window).width();
	if(win_width < 600){
		var ele_width=$("#prfmenu li").find(".current").position().left;

		$(".pn-ProductNav").scrollLeft((ele_width - 100));
	}
});



