
// $(document).ready(function() { 
    var baseURL = $('#base_url').val();
    // var baseURL = "<?php echo base_url();?>";
    // alert(baseURL);
// })

function BulkOrder()
{
    $("#bulk_order_form").validate ({
        rules: {
            User_Name: {
                required:true, 
                minlength: 2,
                maxlength: 20              
            },
            Email: {
                required:true,
                email: true                  
            },
            Phone: {
                required:true,
                minlength:10,
                maxlength:12,
                number: true                  
            },
            City: {
                required:true, 
                minlength: 2,
                maxlength: 20                  
            },
             Message: {
                required:true, 
                minlength: 5,
                maxlength: 500                  
            }
        },          
        messages:{ 
            User_Name: {
                required  : "Name cannot be empty.",
                minlength : "Name has atleast 2 character long.",
                maxlength : "Name should be 20 characters maximum."
            },              
            Email: {
                required  : "Email cannot be empty.",
                email: "Please enter a valid email address."
            },
            Phone: {
                required  : "Number cannot be empty.",
                minlength : "Number has atleast 10 character long.",
                maxlength : "Number should be 12 characters maximum.",
                number: "Please enter a valid Mobile Number."
            },
             City: {
                required  : "City cannot be empty.",
                minlength : "City has atleast 2 character long.",
                maxlength : "City should be 20 characters maximum."
            },
             Message: {
                required  : "Message cannot be empty.",
                minlength : "Message has atleast 5 character long.",
                maxlength : "Message should be 500 characters maximum."
            }  
          
        },
        submitHandler: function(form) {
            // alert("hello");
            // alert(baseURL);
            $.ajax({
                type: 'POST',
                url: baseURL + 'landing/submit_lead_details',
                dataType: 'json',
                data: {
                    'name': $('#User_Name').val(),
                    'email':$('#Email').val(),
                    'phone':$('#Phone').val(),
                    'city':$('#City').val(),
                    'message':$('#Message').val() 
                },
                beforeSend:function(){
                    $('#bulk_order_button').attr('disabled','disabled');
                    $('#response_msg').show();
                    $('#response_msg').html("<font color='green'>Thank You For Your Request</font>");

                },
                success:function(response){
                    if(response){
                            $('#response_msg').fadeOut('slow');
                            $('#bulk_order_form')[0].reset();
                            $('#bulk_order_button').removeAttr('disabled','disabled');
                    }
                }
            });  
        }   
    });
}

function change_flag(type){
    // alert("ffff");
    if(type == 'india'){
        $('#change_flag').attr('src',''+ baseURL +'assets/images/icons/india_icon.png');
        window.location.href = "https://cityfurnish.com";
    }else{
        // window.location = BaseURL + path    }else{
        $('#change_flag').attr('src',''+ baseURL +'assets/images/icons/us_icon.png');
    }
    
}
