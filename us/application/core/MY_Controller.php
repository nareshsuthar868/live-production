<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * This controller contains the common functions
 * @author Teamtweaks
 *
 */
class MY_Controller extends CI_Controller {
	public $privStatus;
	public $data = array();
	function __construct()
	{
		parent::__construct();
		ob_start();
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$this->load->helper('url');
	
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
// 		$this->load->library('session');
	}

	/**
	 *
	 * This function return the session value based on param
	 * @param $type
	 */
	public function checkLogin($type=''){
		if ($type == 'A'){
			return $this->session->userdata('fc_session_admin_id');
		}else if ($type == 'N'){
			return $this->session->userdata('fc_session_admin_name');
		}else if ($type == 'M'){
			return $this->session->userdata('fc_session_admin_email');
		}else if ($type == 'P'){
			return $this->session->userdata('fc_session_admin_privileges');
		}else if ($type == 'U'){
			return $this->session->userdata('fc_session_user_id');
		}else if ($type == 'T'){
			return $this->session->userdata('fc_session_temp_id');

		}
	}

	/**
	 *
	 * This function set the error message and type in session
	 * @param string $type
	 * @param string $msg
	 */
	public function setErrorMessage($type='',$msg=''){
		($type == 'success') ? $msgVal = 'message-green' : $msgVal = 'message-red';
		$this->session->set_flashdata('sErrMSGType', $msgVal);
		$this->session->set_flashdata('sErrMSG', $msg);
	}
	/**
	 *
	 * This function check the admin privileges
	 * @param String $name	->	Management Name
	 * @param Integer $right	->	0 for view, 1 for add, 2 for edit, 3 delete
	 */
	public function checkPrivileges($name='',$right=''){
		$prev = '0';
		$privileges = $this->session->userdata('fc_session_admin_privileges');
		extract($privileges);
		$userName =  $this->session->userdata('fc_session_admin_name');
		$adminName = $this->config->item('admin_name');
		if ($userName == $adminName){
			$prev = '1';
		}
		if (isset(${$name}) && is_array(${$name}) && in_array($right, ${$name})){
			$prev = '1';
		}
		if ($prev == '1'){
			return TRUE;
		}else {
			return FALSE;
		}
	}

	/**
	 *
	 * Generate random string
	 * @param Integer $length
	 */
	public function get_rand_str($length='6'){
		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	}

	/**
	 *
	 * Unsetting array element
	 * @param Array $productImage
	 * @param Integer $position
	 */
	public function setPictureProducts($productImage,$position){
		unset($productImage[$position]);
		return $productImage;
	}

	/**
	 *
	 * Resize the image
	 * @param int target_width
	 * @param int target_height
	 * @param string image_name
	 * @param string target_path
	 */
	public function imageResizeWithSpace($box_w,$box_h,$userImage,$savepath){
			
		
		$thumb_file = $savepath.$userImage;

		list($w, $h, $type, $attr) = getimagesize($thumb_file);
		if( $box_w == 800 && $box_h == 600)
		{
			if($w > 800 || $h > 600)
			{
				$box_w = 800;
				$box_h = 600;
			}
			else if( $w < 150 && $h < 150)
			{
				$box_w = 150;
				$box_h = 150;
			}
			else
			{
				if($w > $h)
				{
					$box_w = 800;
					$box_h = 600;	
				}
				else
				{
					$box_w = 800;
					$box_h = 600;
				}
			}
		}
		else{
				$box_w = 1920;
				$box_h = 600;
		}
		
		$size=getimagesize($thumb_file);
		switch($size["mime"]){
			case "image/jpeg":
				$img = imagecreatefromjpeg($thumb_file); //jpeg file
				break;
			case "image/gif":
				$img = imagecreatefromgif($thumb_file); //gif file
				break;
			case "image/png":
				$img = imagecreatefrompng($thumb_file); //png file
				break;

			default:
				$im=false;
				break;
		}

		$new = imagecreatetruecolor($box_w, $box_h);
		if($new === false) {
			//creation failed -- probably not enough memory
			return null;
		}


		$fill = imagecolorallocate($new, 255, 255, 255);
		imagefill($new, 0, 0, $fill);

		//compute resize ratio
		$hratio = $box_h / imagesy($img);
		$wratio = $box_w / imagesx($img);
		$ratio = min($hratio, $wratio);

		if($ratio > 1.0)
		$ratio = 1.0;

		//compute sizes
		$sy = floor(imagesy($img) * $ratio);
		$sx = floor(imagesx($img) * $ratio);

		$m_y = floor(($box_h - $sy) / 2);
		$m_x = floor(($box_w - $sx) / 2);

		if(!imagecopyresampled($new, $img,
		$m_x, $m_y, //dest x, y (margins)
		0, 0, //src x, y (0,0 means top left)
		$sx, $sy,//dest w, h (resample to this size (computed above)
		imagesx($img), imagesy($img)) //src w, h (the full size of the original)
		) {
			//copy failed
			imagedestroy($new);
			return null;

		}
		imagedestroy($i);
		imagejpeg($new, $thumb_file, 99);
		

	}





}