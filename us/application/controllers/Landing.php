<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));	
		$this->load->model('landing_model');
		$this->load->database();	
	}

	public function index(){
		$this->load->view('landing');
	}

	public function submit_lead_details(){
		// $bulk_data = $this->input->post();
        $this->landing_model->simple_insert(LEAD_REQUEST,$this->input->post());
       	$lead_id  = $this->db->insert_id();
        $this->landing_model->send_mail_to_user($lead_id);
        $response = $this->landing_model->send_mail_to_admin($lead_id);
        echo json_encode($response);

	}
}
