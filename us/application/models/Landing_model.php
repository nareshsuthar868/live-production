<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to admin management
 * @author Teamtweaks
 *
 */
class Landing_model extends My_Model
{
	public function __construct() 
	{
		parent::__construct();
	}


	public function send_mail_to_admin($lead_id){
		$this->db->select('us_lead_email');
        $this->db->from('fc_admin_settings');
        $data = $this->db->get();
        return $this->send_lead_mail($data->row()->us_lead_email,$lead_id,'admin');
	}

	public function send_mail_to_user($lead_id){
		$this->db->select('email');
        $this->db->from(LEAD_REQUEST);
        $this->db->where('id',$lead_id);
        $data = $this->db->get();
        return $this->send_lead_mail($data->row()->email,$lead_id,'user');
	}


	public function send_lead_mail($email,$lead_id,$type){
		$this->db->select('*');
        $this->db->from(LEAD_REQUEST);
        $this->db->where('id',$lead_id);
        $bulk_data = $this->db->get();
        // print_r($bulk_data->row()->name);
        if($type == 'user'){
        	 $message = '<p>Dear '. $bulk_data->row()->name .',</p><p>This is to confirm that we have received your requirements. We will like to thank you for your interest to be a part of cityfurnish family. Our team will revert to you shortly.</p><p>Best regards,</p><p>City Furnish</p>';
        }else{
        // print_r($data->row());exit;

        $message ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <head>
                        <meta name="viewport" content="width=device-width"/>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>Lead Registration Informations</title>
                        <style type="text/css">
                        @media screen and (max-width: 580px) {
                        .tab-container {
                            max-width: 100%;
                        }
                        .txt-pad-15 {
                            padding: 0 15px 51px 15px !important;
                        }
                        .foo-txt {
                            padding: 0px 15px 18px 15px !important;
                        }
                        .foo-add {
                            padding: 15px 15px 0px 15px !important;
                        }
                        .foo-add-left {
                            width: 100% !important;
                        }
                        .foo-add-right {
                            width: 100% !important;
                        }
                        .pad-bottom-15 {
                            padding-bottom: 15px !important;
                        }
                        .pad-20 {
                            padding: 25px 20px 20px !important;
                        }
                        }
                        </style>
                        </head>
                    <body style="margin:0px;">
                        <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="width: 850px;background-color: #fff;margin: 0 auto;table-layout: fixed;background:#dbdbdb;font-family:Arial, Helvetica, sans-serif;font-size:15px;">
                            <tr>
                                <td style="padding:20px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left;width: 100%;padding-bottom:25px;background:#90c5bc;padding:20px;"><a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/email/logo.png" alt="logo"></a></td>
                            </tr>
                        </table>
                        <!-- address table -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                            <tr>
                                <td style="text-align: left;padding:15px 20px;width:100%;"><p style="font-family:Arial, Helvetica, sans-serif;margin:0px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Lead Registration Details</strong> </p></td>
                            </tr>
                            <tr>
                                <td style="text-align: left;padding:15px 20px;border-top:1px solid #ddd;width:100%;border-color:#ddd;"><p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Full Name</strong> <span style="font-weight:400;"> : '.$bulk_data->row()->name.'</span> </p>
                                   <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Email</strong> <span style="font-weight:400;"> :  '.$bulk_data->row()->email.'</span> </p>
                                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> 
                                    <strong style="font-weight:600;">Phone</strong> <span style="font-weight:400;display:inline-block;margin-right:75px;"> :  '.$bulk_data->row()->phone.'</span>   <strong style="font-weight:600;">City</strong> <span style="font-weight:400;display:inline-block;"> :  '.$bulk_data->row()->city.'</span> </p>
                                    <p style="font-family:Arial, Helvetica, sans-serif;margin-top:0px;margin-bottom:17px;font-weight:bold;font-size:12px;color:#656565;"> <strong style="font-weight:600;">Message</strong> <span style="font-weight:400;"> :  '.$bulk_data->row()->message.'</span> </p></td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #dddddd;background-color:#fff;margin-bottom:15px;border-color:#ddd;">
                            <tr style="background:#fff;width:100%;display:table-row !important;color:#fff">
                                <td style="padding:20px 15px;text-align:center;line-height:30px;width:100%;background:#fff;" class="foo-add"><p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#989898;font-size:14px;">If you have any concern please contact us.</p>
                              <p style="margin:0px;font-family:Arial, Helvetica, sans-serif;color:#38373d;font-size:14px;"> Email : <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">hello@cityfurnish.com</a> | 
                                Mobile: <a href="mailto:hello@cityfurnish.com" style="color:#38373d;display:inline-block;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">+91 8010845000</a> </p></td>
                            </tr>
                        </table>
                    </body>
                </html>';
            }

        $email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Requirement received',
                         'to_mail_id'=>$email,
                         'subject_message'=>'Cityfurnish: Requirement received',
                         'body_messages'=>$message
        );
        
        // print_r($email_values);exit;

        $email_send_to_common = $this->landing_model->common_email_send($email_values);
       return  $email_send_to_common;

	}


	
}