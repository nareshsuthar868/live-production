
<!doctype html>
<html lang="en">
    <head>
        <title>Furniture on Rent in San Francisco and Los Angeles | Cityfurnish</title>
        <meta name="Title" content="Furniture on Rent in San Francisco and Los Angeles | Cityfurnish" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!--meta info-->
        <meta name="author" content="">
        <meta name="description" content="Rent Furniture on Easy Monthly Rental Terms. Choose From Wide Range of Furniture Available on Rent in Mumbai, Bangalore, Delhi, Gurgaon & Noida. New Year Offer - Get upto 100% Off on First Month Rent. Hurry! Offer Ending Soon." />
        <meta property="og:image" content="https://cityfurnish.com/images/product/" />
        <base href="https://cityfurnish.com/" />

        <!--include favicon-->
        <link rel="shortcut icon" type="image/x-icon" href="https://cityfurnish.com/images/logo/FaviconNew.png">
        <!-- <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet"> -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="all" href="https://cityfurnish.com/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="all" href="https://cityfurnish.com/css/style.css">
        <link href="https://cityfurnish.com/assets/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
        <link href="https://cityfurnish.com/assets/bootstrap-sweetalert/dist/swalExtend.css" rel="stylesheet" type="text/css">

        <link href="http://us.cityfurnish.com/assets/css/style.css" rel="stylesheet" type="text/css">

        <script src="https://cityfurnish.com/js/jquery-2.1.1.min.js"></script>

        <script src="https://cityfurnish.com/assets/js/jquery.validate.min.js"></script>
        <script src="https://cityfurnish.com/js/modernizr.min.js"></script>
        <script src="https://cityfurnish.com/js/bootstrap.js"></script>
        <input type="hidden" id="base_url" value="<?php echo base_url() ?>">

        

        <!-- <a href="https://plus.google.com/+Cityfurnish" rel="publisher"></a> -->
        <!-- <a href="https://plus.google.com/+Cityfurnish?rel=author"></a> -->
        <!-- Global site tag (gtag.js) - Google Ads: 867888766 --> 
    </head>
    <body>
    <!--layout-->
    <div class="wide_layout db_centered bg_white">
        <header>
            <div class="header_bottom_part w_inherit">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="headertop">
                                <div class="mobilemenu"> 
                                    <a href="javascript:void(0)" class="togglemenu"> <i class="material-icons">menu</i> </a> 
                                </div>
                                <div class="logo clearfix t_sm_align_c"> 
                                                    <a href="https://cityfurnish.com/" class="d_inline_b hide_on_mobile"> 
                                                        <img src="https://d1eohs8f9n2nha.cloudfront.net/images/logo-white.png" alt="Logo Here" class="logounstick"> 
                                                        <img src="https://d1eohs8f9n2nha.cloudfront.net/images/logo-stick.png" alt="Logo Here" class="logostick">
                                                    </a>
                                                    <!-- mobile menu -->
                                                    <a href="https://cityfurnish.com/" class="d_inline_b show_on_mobile"> 
                                                        <img src="https://d1eohs8f9n2nha.cloudfront.net/images/mobile-logo-white.png" alt="Logo Here" class="logounstick"> 
                                                        <img src="https://d1eohs8f9n2nha.cloudfront.net/images/mobile-logo-stick.png" alt="Logo Here" class="logostick"> 
                                                    </a> 
                                                </div>
                                 <div class="mobileoverlay"></div>
                                <ul class="hr_list headeropt">
                                   <li>
                                        <div class="btn-group"> <img class="flag_class" id="change_flag" src="http://us.cityfurnish.com/assets/images/icons/us_icon.png">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
                                                <span>USA</span><i class="material-icons">keyboard_arrow_down</i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)" onclick="change_flag('india')" ><i class="cityicon"><img class="flag_class" src="http://us.cityfurnish.com/assets/images/icons/india_icon.png" alt="delhi"></i>INDIA</a></li>
                                                <li><a href="javascript:void(0)"  onclick="change_flag('us')" ><i class="cityicon"><img class="flag_class" src="http://us.cityfurnish.com/assets/images/icons/us_icon.png" alt="Bangalore"></i>USA</a></li>
                                            
                                            </ul>
                                        </div>
                                    </li>
                                   <!--<li>-->
                                   <!--    <a href=""><img src="<?php echo base_url()?>assets/images/icons/indian_flag.png" width="40px"></a>-->
                                   <!--</li>-->
                                </ul>
                                <div class="mobileoverlay"></div>
                                <nav role="navigation" class="navigation"> 
                                   <!--<a href="javascript:void(0)" class="cross-menu"><i class="material-icons">close</i></a>-->
                                    <div class="menuheader  visible-xs">
                                        <div class="mobilemenu"> 
                                            <a href="javascript:void(0)" class="togglemenu"> <i class="material-icons">menu</i> </a> 
                                        </div>
                                        <div class="logo"> <a href="https://cityfurnish.com/" class="d_inline_b show_on_mobile"> <img src="https://d1eohs8f9n2nha.cloudfront.net/images/mobile-logo-white.png" alt="Logo Here"> </a> </div>
                                        <div class="header-right-top" >
                                            <ul class="hr_list shop_list f_right second_font fs_medium f_sm_none d_sm_inline_b t_sm_align_l scheme_color_font">
                                                <li><a  href="mailto:hello@cityfurnish.com"> <i class="material-icons">mail_outline</i></a> </li>
                                                <li><a  href="tel:8010845000"><i class="material-icons">phone</i></a> 
                                            </ul>
                                          
                                        </div> 
                                    </div>
                                </nav>                
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </header>