<script src="http://us.cityfurnish.com/assets/js/validation.js"></script>
<div id="overlay" style="display: none;">
    <div class="loader-inner line-scale-pulse-out-rapid">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>

<footer role="contentinfo">
    <div class="container hidden-xs">
        <div class="row">
            <div class="m_bottom_30 footerlogosec">
                <figure class="m_bottom_15">
                    <img src="https://d1eohs8f9n2nha.cloudfront.net/images/logo-stick.png" alt="footer logo" />
                </figure>
                <div class="m_bottom_15">
                    <div class="color_light">
                      <p>Cityfurnish is revolutionizing the furniture industry by providing quality furniture and home appliances on easy monthly rental. With the immense focus on product quality and customer service, we strive to become most preferred name in furniture industry by customer's choice.</p>
                    </div>
                </div>
            </div>
            <div class="footerlink m_bottom_30">
                <!--<h5 class="tt_uppercase  m_bottom_15 nw_scheme_color">Help</h5>-->
                <ul class="second_font vr_list_type_1 with_links">
                    <!--<li><a href="https://cityfurnish.com/pages/contact-us">Contact us</a></li>-->
                    <!--<li><a href="https://cityfurnish.com/pages/how-it-works">How It Works?</a>  </li>-->
                    <!--<li><a href="https://cityfurnish.com/pages/faq">FAQs</a></li>-->
                    <!--<li><a href="https://cityfurnish.com/reviews-testimonials/all">Customer Reviews</a></li>-->
                    <!--<li><a href="customerpayment" target="_blank">Customer Payment</a></li>-->
                </ul>
            </div>
            <div class="footerlink m_bottom_30">
                <!--<h5 class="tt_uppercase  m_bottom_15 nw_scheme_color">Information</h5>-->
                <ul class="second_font vr_list_type_1 with_links">
                    <!--<li><a href="https://cityfurnish.com/blog/" target="_blank">Blog</a></li>-->
                    <!--<li><a target="_blank" href="http://vior.in" rel="nofollow" >Clearance Sale</a></li>-->
                    <!--<li><a href="https://cityfurnish.com/pages/offers">Offers</a></li>-->
                    <!--<li><a href="https://cityfurnish.com/pages/careers">We are hiring</a></li>-->
                    <!--<li><a href="https://cityfurnish.com/pages/friends-and-partners">Friends & Partners</a></li>-->
                    
                </ul>
            </div>
            <!--<div class="footerlink last m_bottom_30">-->
            <!--    <h5 class="tt_uppercase  m_bottom_15 nw_scheme_color">POLICIES</h5>-->
            <!--    <ul class="second_font vr_list_type_1 with_links">-->
            <!--        <li><a href="https://cityfurnish.com/pages/terms-of-use">Terms of use</a></li>-->
            <!--        <li><a href="https://cityfurnish.com/pages/privacy-policy">Privacy policy</a></li>-->
            <!--        <li><a href="https://cityfurnish.com/pages/refer-a-friend">Referral Terms of use</a></li>-->
            <!--        <li><a href="https://cityfurnish.com/pages/rentalagreement">Sample Rental Agreement</a></li>-->
            <!--    </ul>-->
            <!--</div>-->
            <div class="new_scroll">
                <ul id="results" /> 
            </div>
            <div class="quickcontactcol m_bottom_30">
                <div class="footerquickcontact">
                    <h5 class="m_bottom_15 nw_scheme_color">QUICK CONTACT</h5>
                    <ul class="contactquick">
                        <li>
                            <a href="mailto:hello@cityfurnish.com">
                                <i class="material-icons">mail_outline</i>
                                <span>hello@cityfurnish.com</span>
                            </a>
                        </li>
                        <li>
                            <a href="tel:+1-6504308412">
                                <i class="material-icons">phone</i>
                                <span>+1-6504308412</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="socialicon">
                        <li>
                            <a href="https://www.facebook.com/cityFurnishRental" rel="nofollow" target="_blank">
                                <!--<i class="fa fa-facebook fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/CityFurnish" rel="nofollow" target="_blank" class="twitter">
                                <!--<i class="fa fa-twitter fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/+cityfurnish" rel="nofollow" target="_blank" class="google">
                                <!--<i class="fa fa-google-plus fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://in.pinterest.com/cityfurnish/" rel="nofollow" target="_blank" class="pintrest">
                                <!--<i class="fa fa-pinterest-p fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" rel="nofollow" target="_blank" class="linkedin">
                                <!--<i class="fa fa-linkedin fs_large"></i>-->
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-12 col-sm-12 col-md-12">
                <hr class="divider_grey m_bottom_25">
            </div>
            
        </div>
    </div>
<script src="js/validation.js" type="text/javascript"></script>
</footer>
<section class="copyright" id="mobile_footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6 hidden-xs">
                <p>&copy; Copyright 2019                    <strong class="nw_theme_color">Cityfurnish</strong>. All Rights Reserved.</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 headertopcontact visible-xs">
                <a href="mailto:hello@cityfurnish.com">
                    <i class="material-icons">mail_outline</i>
                    <span>hello@cityfurnish.com</span>
                </a>
                <a href="tel:+1-6504308412">
                    <i class="material-icons">phone</i>
                    <span>+1-6504308412</span>
                </a>
            </div>
        </div>
    </div>
</section>

<button class="back_to_top scrollToTop">
    <i class="material-icons d_inline_m">keyboard_arrow_up</i>
</button>


<style type="text/css">
    #overlay{
      position:fixed;
      top:0px;
      right:0px;
      width:100%;
      height:100%;
      background-color:rgba(0,0,0,0.8);
      //background-image:url('images/loader.gif');
      background-repeat:no-repeat;
      background-position:center;
      z-index:10000000;
    }
    #overlay .loader-inner{
           position: absolute;
           top: 0px;
           left: 0px;
           bottom: 0px;
           right: 0px;
           margin: auto;
           display: inline-block;
           height: 35px;
           width: 110px;
    }
</style>                



   
<script src="https://cityfurnish.com/js/easing.jquery.js"></script> 
<script src="https://cityfurnish.com/js/readmore.js"></script> 
<script src="https://cityfurnish.com/js/slick.js"></script> 
<script src="https://cityfurnish.com/js/jquery.sliderTabs.js"></script>
<script src="https://cityfurnish.com/js/bootstrap-slider.js"></script> 
<script src="https://cityfurnish.com/js/custom-function.js"></script>    </div>
<script src="plugins/owl-carousel/owl.carousel.min.js"></script> 
<script>
    $(document).ready(function () {
        var totalItems = $('.item').length;
        if (totalItems <= 7) {
            var isNav = false;
        }
        else {
            var isNav = true;
        }

        $('.owl-carousel').owlCarousel({
            loop: false,
            nav: isNav,
            navText: ["<i class='material-icons'>keyboard_arrow_left</i>", "<i class='material-icons'>keyboard_arrow_right</i>"],
            margin: 0,
            mouseDrag: false,
            pullDrag: false,
            touchDrag: true,
            autoplay: false,
            responsive: {
                768: {
                    items: 5
                },
                1000: {
                    items: 6,
                },
                1200: {
                    items: 7
                }
            }
        });
    });
</script>
</body>
</html>
