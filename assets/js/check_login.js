function check_value() {
    $("#password_change").validate({
        rules: {
            chngpass: {
                required: !0,
                minlength: 6,
                maxlength: 16
            },
            cchngpass: {
                required: !0,
                minlength: 6,
                maxlength: 16
            }
        },
        messages: {
            chngpass: {
                required: "<font color='red'>Password cannot be empty.</font>",
                minlength: "<font color='red'>Password has atleast 6 character long.</font>",
                maxlength: "<font color='red'>Password length should be 16 characters maximum.</font>"
            },
            cchngpass: {
                required: "<font color='red'>Confirm Password cannot be empty.</font>",
                minlength: "<font color='red'>Password has atleast 6 character long.</font>",
                maxlength: "<font color='red'>Password length should be 16 characters maximum.</font>"
            }
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(e) {
            var t = $("#base_url").val(),
                a = $("#user_id").val(),
                o = $("#chngpass").val(),
                r = $("#cchngpass").val();
            o != r ? sweetAlert({
                title: "Warning!",
                text: "Password Not Match",
                type: "warning",
                showCancelButton: !1,
                closeOnConfirm: !0,
                animation: "slide-from-top",
                showConfirmButton: !0
            }) : $.ajax({
                method: "POST",
                url: baseURL + "site/user/change_password",
                data: {
                    id: a,
                    cpass: r
                },
                success: function(e) {
                    e.status ? ($("#reset_pass").hide(), $("#reset_password").show(), setTimeout(function() {
                        location.href = t
                    }, 3e3)) : sweetAlert({
                        title: "Warning!",
                        text: "Something Went Wrong !...Please Try Again",
                        type: "warning",
                        showCancelButton: !1,
                        closeOnConfirm: !0,
                        animation: "slide-from-bottom",
                        showConfirmButton: !0
                    }, function(e) {
                        e && (location.href = t)
                    })
                }
            })
        }
    })
}

function check_validation() {
    $("#sign-up").validate({
        rules: {
            full_name: {
                required: !0,
                minlength: 2,
                maxlength: 20
            },
            signup_email: {
                required: !0,
                email: !0
            },
            signup_password: {
                required: !0,
                minlength: 6,
                maxlength: 16
            },
            mobile_number: {
                required: !0,
                minlength: 10,
                maxlength: 10,
                number: !0
            }
        },
        messages: {
            full_name: {
                required: "Name cannot be empty.",
                minlength: "Name has atleast 2 character long.",
                maxlength: "Name should be 20 characters maximum."
            },
            signup_email: {
                required: "Email cannot be empty.",
                email: "Please enter a valid email address."
            },
            signup_password: {
                required: "Password cannot be empty.",
                minlength: "Password has atleast 6 character long.",
                maxlength: "Password length should be 16 characters maximum."
            },
            mobile_number: {
                required: "Number cannot be empty.",
                minlength: "Number has atleast 10 character long.",
                maxlength: "Number should be 10 characters maximum.",
                number: "Please enter a valid Mobile Number."
            }
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(e) {
            return register_user()
        }
    })
}

function login() {
    $("#form-login").validate({
        rules: {
            email: {
                required: !0,
                email: !0
            },
            password: {
                required: !0
            }
        },
        messages: {
            email: {
                required: "<font color='red'>Email cannot be empty.</font>",
                email: "<font color='red'>Please enter a valid email address.</font>"
            },
            password: {
                required: "<font color='red'>Password cannot be empty.</font>"
            }
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(e) {
            $("#base_url").val();
            var t = $("#login_email").val(),
                a = $("#login_password").val();
            $.ajax({
                method: "POST",
                url: baseURL + "site/user/login_user",
                data: {
                    email: t,
                    password: a
                },
                success: function(e) {
                    if ($("#myModal3").hide(), e.status)
                        if (1 == e.key) {
                            var t = window.location.href;
                            location.href = t
                        } else sweetAlert({
                            title: e.title,
                            text: e.message,
                            type: e.type,
                            showCancelButton: !1,
                            closeOnConfirm: !0,
                            animation: "slide-from-top",
                            showConfirmButton: !0
                        }, function(t) {
                            if (t)
                                if (1 == e.key) {
                                    var a = window.location.href;
                                    location.href = a
                                } else $("#myModal3").show()
                        })
                }
            })
        }
    })
}

function Add_to_whishlist(e) {
    "" != $("#user_id").val() ? (e = e, $("#base_url").val(), $.ajax({
        method: "POST",
        url: baseURL + "site/product/add_to_whislist",
        data: {
            user_id: $("#user_id").val(),
            product_id: e
        },
        success: function(t) {
            t ? ($("#show_icon_" + e).show(), $("#show_button_" + e).addClass("wishactive"), $("#hide_like_" + e).hide()) : ($("#show_icon_" + e).hide(), $("#hide_like_" + e).show())
        }
    })) : $("#myModal3").modal("show")
}

function remove_wishlist(e) {
    event.preventDefault(), confirm("Do you really want to remove this Product?") && (e = e, $("#base_url").val(), $.ajax({
        method: "POST",
        url: baseURL + "site/product/remove_wishlist",
        data: {
            user_id: $("#user_id").val(),
            product_id: e
        },
        success: function(t) {
            $("#new_row_" + e).fadeOut("slow")
        }
    }))
}

function add_to_cart(e) {
    var t = $("#product_id_" + e).val(),
        a = $("#sell_id_" + e).val(),
        o = $("#price_" + e).val(),
        r = $("#product_shipping_cost_" + e).val(),
        n = $("#product_tax_cost_" + e).val(),
        s = $("#cateory_id_" + e).val();
    return $.ajax({
        type: "POST",
        url: baseURL + "site/cart/add_to_cart",
        data: {
            product_id: t,
            sell_id: a,
            cate_id: s,
            price: o,
            product_shipping_cost: r,
            product_tax_cost: n
        },
        success: function(e) {
            var t = 0;
            $("#add_new_product").empty(), $("#new_count").text(e.product_value.length);
            for (var a = 0; a < e.product_value.length; a++) {
                var o = e.product_value[a].image.split(",");
                if (t += e.product_value[a].product_shipping_cost * e.product_value[a].quantity + e.product_value[a].price * e.product_value[a].quantity - e.product_value[a].discountAmount, $("#null_value").hide(), "" != e.product_value[a].attr_type && e.product_value[a].attr_name) var r = e.product_value[a].attr_type + "/",
                    n = e.product_value[a].attr_name;
                else r = "", n = "";
                $("#add_new_product").append('<li id="header-cart-row-' + e.product_value[a].id + '"><div class="cart-thumb"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/product/' + o[0] + '" alt="" /></div><div class="cart-desc"><strong>' + e.product_value[a].product_name + "</strong><small>" + r + n + "</small><span>" + e.product_value[a].quantity + "x Rs " + e.product_value[a].price + '</span><a href="cart"><i class="material-icons">mode_edit</i></a><a href="javascript:void(0)" onclick="delete_cart(' + e.product_value[a].id + "," + e.product_value[a].id + ')"><i class="material-icons">delete</i></a></div> <strong></li>')
            }
            $("#add_new_product").append('<li><div class="pull-left">SUBTOTAL</div><div class="pull-right"><strong id="CartGAmt"><i id="new_price" class="fa fa-inr" aria-hidden="true"></i>' + t + "</strong></div></li>"), $("#add_new_product").append('<div class="btncartdiv"><a href="' + baseURL + 'cart" class="btn-check pull-left btngray"><i class="material-icons">shopping_cart</i><span>View Cart</span></a><a href="' + baseURL + 'cart" class="btn-check pull-right"><span>Check Out</span> <i class="material-icons checkrot">reply</i></a></div>'), sweetAlert({
                title: "Success!",
                text: e.message,
                animation: "slide-from-bottom",
                type: "success",
                showConfirmButton: !1,
                timer: 1500
            })
        }
    }), !1
}

function search_product() {
    var e = document.getElementById("search_value").value;
    var catslug = document.getElementById("catSlug").value;
    "" == e ? ($(".new_search_data").empty(), $(".new_cat").empty(), $("#no_found").show(), $("#new_text").html("Search Your Products")) : $.ajax({
        type: "POST",
        url: baseURL + "site/product/search_product",
        dataType: "json",
        data: {
            searchValue: e
        },
        success: function(e) {
            if (e.product.length > 0) {
                $(".new_search_data").empty(), $("#no_found").hide(), $(".new_autofill_tranding_product").hide();
                for (var t = 0; t < e.product.length; t++) {
                    var a = e.product[t].image.split(","),
                        o = e.product[t].product_name.replace(/\s+/g, "-");
                    $(".new_search_data").append('<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin-bottom-30" ><a href="things/' + e.product[t].id + "/" + o + '"><figure><div class="searchprothumg"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/' + a[0] + '" alt=/></div><figcaption><span><strong>' + e.product[t].product_name + "</strong></span><span>&#8377 " + e.product[t].price + "</span></figcaption></figure></a></div>")
                }
            } else $(".new_search_data").empty(), $("#no_found").show(), $("#new_text").html("No Products Found");
            if (e.cat.length > 0)
                for ($(".new_cat").empty(), $("#not_found_cat").hide(), t = 0; t < e.cat.length; t++) $(".new_cat").append('<div class="catagorycol"><a href="' + baseURL + catslug +"/" + e.cat[t].seourl + '"><figure><img src="https://d1eohs8f9n2nha.cloudfront.net/images/category/' + e.cat[t].image + '" alt="catagory icon"><figcaption>' + e.cat[t].cat_name + "</figcaption></figure></a></div>");
            else $(".new_cat").empty(), $("#cat_no").show(), $("#not_found_cat").html("No Cateory Found");
            $(".new_autofill_tranding_category").hide()
        }
    })
}

function get_default_address() {
    var e = $("input[name='Ship_address_val']:checked").val();
    $.ajax({
        type: "POST",
        url: baseURL + "site/cart/get_ship_value",
        dataType: "json",
        data: {
            id: e
        },
        success: function(e) {
            get_coupon_code(), $("#address_name").html(e.full_name + "<br>" + e.address1 + "," + e.city + ",<br>" + e.state + "-" + e.postal_code + ",<br> Phone:" + e.phone), $("#form_submit_button").removeAttr("disabled")
        }
    })
}

function get_coupon_code() {
    var e = $("#user_id").val();
    $.ajax({
        type: "POST",
        url: baseURL + "site/cart/get_coupon",
        dataType: "json",
        data: {
            id: e
        },
        success: function(e) {
            e ? $("#coupon_code").text(e) : $("#coupon_code").text("No")
        }
    })
}
$(".searchicn").click(function() {
    $(".new_search_data").empty(), $(".new_cat").empty(), $("#new_text").html("Search Your Products"), $("#not_found_cat").html("Search By Cateory")
}), $(function() {
    $("#customer_payment_form").validate({
        rules: {
            user_first_name: {
                required: !0
            },
            user_email: {
                required: !0,
                email: !0
            },
            user_amount: {
                required: !0,
                min: 1,
                number: !0
            }
        },
        messages: {
            user_first_name: {
                required: "<font color='red'>User name cannot be empty.</font>"
            },
            user_email: {
                required: "<font color='red'>Email cannot be empty.</font>",
                email: "<font color='red'>Please enter a valid email address.</font>"
            },
            user_amount: {
                required: "<font color='red'>Amount cannot be empty.</font>",
                min: "<font color='red'>Amount must be greater than zero</font>",
                number: "<font color='red'>Please Enter Amount</font>"
            }
        },
        submitHandler: function(e) {
            var t = $("#user_email").val(),
                a = $("#order_id").val(),
                o = $("input[name=payment_option]:checked").val();
            $.ajax({
                type: "POST",
                url: baseURL + "customerpayment/CustomerPayment",
                data: {
                    user_email: t,
                    order_id: a,
                    payment_option: o
                },
                beforeSend: function() {
                    $("#admin_loader").show()
                },
                success: function(t) {
                    if (400 == t.status_code) return sweetAlert({
                        title: "Warning!",
                        text: t.msg,
                        type: "warning",
                        showCancelButton: !1,
                        closeOnConfirm: !0,
                        animation: "slide-from-top",
                        showConfirmButton: !0
                    }), !1;
                    e.submit()
                }
            })
        }
    })
});
var alreadyFetchingData = !1,
    pageNumber = 1,
    needRecordCount = 12,
    isAvailableRecords = !0;

function get_data()
{
    var url = location.href;    
    $.ajax({
        type: 'POST',
        url: 'site/searchShop/search_shopby',
        data: {
            'pg': pageNumber,
            'url':url
        },
        beforeSend: function(xhr) {
            $("#product_main_container").after($("<center><li class='loading'>Loading...</li></center>").fadeIn('slow')).data("loading", true);
        },
        success:function(response){
            console.log("response",response);
             $(".loading").fadeOut('slow', function() {
                $(this).remove();
            });
            pageNumber++;
            if(response.length < needRecordCount){
                isAvailableRecords = false;
            }
            for (var i = 0; i <= response.length-1; i++) {

                var product = response[i];
                var image  = product.image.split(',');
                var product_name = product.product_name.replace(/\s+/g, '-');
                var seo_url = product.seourl;
                var description = product.description.substr(0, 60);
                if(product.subproducts != ''){ 
                        var subprodcuts_array =   product.subproducts.split(',');
                        count = subprodcuts_array.length;
                }
                else
                {
                    count = 1;
                }
                var Item_sold = '';
                var html = '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 prolistcol">\
                            <div class="productlistitem">';
                var ISsold = '';
                if(product.is_sold){
                    Item_sold =  '<span class="soldout">Out of stock</span>';
                }
                    html += '<a target="_blank" href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="productcatimg hidden-xs"><img data-src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" src="'+baseURL+'images/ajax-loader/ajax-loader_new.gif"  />'+ Item_sold +'</a><a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="productcatimg visible-xs"><img data-src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" src="'+baseURL+'images/ajax-loader/ajax-loader_new.gif" />'+ Item_sold +'</a>';
                    html +=   ' <div class="relative"><div class="proshortdetail hidden-xs"><span class="title_minheight"><a target="_blank" href="'+  baseURL +'things/'+ product.id + '/'+ seo_url +'">'+ product.product_name +'</a></span><div class="listcontent"><p>'+ description +'.....</p></div></div>';
                    html +=  '<div class="priceitemgray"><a href="'+ baseURL +'things/' + product.id +'/'+ seo_url +'" class="visible-xs">' + product.product_name + '</a><span class="startprice">Starting from  <i class="fa fa-inr" aria-hidden="true"></i><strong> '+ product.sale_price +'<strong></span><span class="itemleftw">'+ count +' item</span></div></div>';
                $('#product_main_container').append(html);

            }
            alreadyFetchingData = false;
            $("img").unveil(400);
        }
    });
}
function BulkOrder() {
    $("#bulk_order_form").validate({
        rules: {
            User_Name: {
                required: !0,
                minlength: 2,
                maxlength: 20
            },
            Email: {
                required: !0,
                email: !0
            },
            Phone: {
                required: !0,
                minlength: 10,
                maxlength: 12,
                number: !0
            },
            City: {
                required: !0,
                minlength: 2,
                maxlength: 20
            },
            Message: {
                required: !0,
                minlength: 5,
                maxlength: 500
            }
        },
        messages: {
            User_Name: {
                required: "<font color='red'>Name cannot be empty.</font>",
                minlength: "<font color='red'>Name has atleast 2 character long.</font>",
                maxlength: "<font color='red'>Name should be 20 characters maximum.</font>"
            },
            Email: {
                required: "<font color='red'>Email cannot be empty.</font>",
                email: "<font color='red'>Please enter a valid email address.</font>"
            },
            Phone: {
                required: "<font color='red'>Number cannot be empty.</font>",
                minlength: "<font color='red'>Number has atleast 10 character long.</font>",
                maxlength: "<font color='red'>Number should be 12 characters maximum.</font>",
                number: "<font color='red'>Please enter a valid Mobile Number.</font>"
            },
            City: {
                required: "<font color='red'>City cannot be empty.</font>",
                minlength: "<font color='red'>City has atleast 2 character long.</font>",
                maxlength: "<font color='red'>City should be 20 characters maximum.</font>"
            },
            Message: {
                required: "<font color='red'>Message cannot be empty.</font>",
                minlength: "<font color='red'>Message has atleast 5 character long.</font>",
                maxlength: "<font color='red'>Message should be 500 characters maximum.</font>"
            }
        },
        submitHandler: function(e) {
            $("#User_Name").val(), $.ajax({
                type: "POST",
                url: baseURL + "site/order/insert_bulk_order",
                dataType: "json",
                data: {
                    name: $("#User_Name").val(),
                    email: $("#Email").val(),
                    phone: $("#Phone").val(),
                    city: $("#City").val(),
                    message: $("#Message").val()
                },
                beforeSend: function() {
                    $("#bulk_order_button").attr("disabled", "disabled"), $("#response_msg").show(), $("#response_msg").text("Thank You For Your Order")
                },
                success: function(e) {
                    e && ($("#response_msg").fadeOut("slow"), $("#bulk_order_form")[0].reset(), $("#bulk_order_button").removeAttr("disabled", "disabled"))
                }
            })
        }
    })
}

function Send_Voucher(e, t) {
    confirm("Do you really want to send voucher mail ?") && $.ajax({
        type: "POST",
        url: baseURL + "admin/order/send_voucher_email",
        data: {
            user_id: e,
            order_id: t
        },
        beforeSend: function() {
            $("#admin_loader").show()
        },
        success: function(e) {
            $("#admin_loader").hide(), e.status ? alert(e.msg) : alert("Please Try Again")
        }
    })
}

function payment_method(e) {
    $(".main_div").show(), $("#new_div").empty(), $("#payment_div").empty(), "cc_card" == e ? (payment_button = '<div class="checkdiv"><input id="type_recurring" name="is_recurring" type="checkbox" value="true" onchange=\'change_label()\' ><label for="type_recurring">Select to register for Standing Instructions on provided credit Card</label></div>', auto_select_button = '<input class="btn-std  btn-check" type="submit" id="form_submit_button1" value="Proceed To Payment">', $("#new_div").html(auto_select_button), $("#payment_div").html(payment_button)) : "dc_card" == e ? (auto_select_button = '<input class="btn-std  btn-check" type="submit" id="form_submit_button1" value="Proceed To Payment">', $("#new_div").html(auto_select_button)) : "nb" == e && (auto_select_button = '<input class="btn-std  btn-check" type="submit" id="form_submit_button1" value="Proceed To Payment">', $("#new_div").html(auto_select_button))
}

function change_label() {
    $("#new_div").empty();
    var e = "";
    if ($("#type_recurring").val(), $("input:radio[name=payment_type]:checked").val(), $("#type_recurring").is(":checked")) {
        var t = '<div class="delivaryaddlocate" ><ul><li type="1" id="recurring_offer"><font color="red">Congratulations, you will get gift vouchers worth Rs ' + $("#voucher_money").val() + " by email.</font></li>";
        e = '<font color="red">' + (t += "<li><a href='" + baseURL + 'pages/faq\' target="_blank"><u>Refer FAQ for details regarding standing instructions and gift vouchers</u><a></li></ul>') + '</font><br><input class="btn-std  btn-check" type="submit" id="form_submit_button1" value="Proceed To Payment"></div>', $("#new_div").html(e)
    } else e = '<input class="btn-std  btn-check" type="submit" id="form_submit_button1" value="Proceed To Payment">', $("#new_div").html(e)
}
$(window).scroll(function() {
    var e = $(".new_scroll").offset().top,
        t = $(".new_scroll").outerHeight(),
        a = $(window).height();
    $(this).scrollTop() > e + t - a && !alreadyFetchingData && isAvailableRecords && (alreadyFetchingData = !0, get_data())
}), $(function() {
    $("#form_submit_button").attr("disabled", !0)
});
var clicks = 0;

function Add_more_receipt_email() {
    var e = '<div class="row" id=' + (clicks += 1) + '><input type="text" id="new_reporting_email" class="large tipTop" ><button type="button" onclick="add_more_recipent_email(' + clicks + ')">Add</button><button type="button" onclick="remove_recipent_email(' + clicks + ')">Remove</button><br></div>';
    $("#new_email_address").html(e)
}

function remove_recipent_email(e) {
    $("#" + e).remove()
}

function add_more_recipent_email(e) {
    var t = [],
        a = $("#report_receipt_mail").val(),
        o = $("#new_reporting_email").val();
    t.push(a), t.push(o), $("#report_receipt_mail").val(t), $("#new_reporting_email").val("")
}

function serach_on_home_page() {
    var e = document.getElementById("home_page_search").value;
    var catSlug = document.getElementById("catSlug").value;
    "" == e ? $("#home_search_div").hide() : $.ajax({
        type: "POST",
        url: baseURL + "site/product/search_product",
        dataType: "json",
        data: {
            searchValue: e
        },
        success: function(e) {
            if ($(".home_serach_container").show(), e.product.length > 0) {
                $(".orverflow_search").css({
                    overflow: "initial"
                }), $(".orverflow_search > figure").css({
                    overflow: "initial"
                }), $("#home_search_div").show(), $(".search_data").empty(), $("#home_no_found").hide();
                for (var t = 0; t < e.product.length; t++) {
                    var a = e.product[t].image.split(","),
                        o = e.product[t].product_name.replace(/\s+/g, "-");
                    $(".search_data").append('<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 margin-bottom-30" ><a href="things/' + e.product[t].id + "/" + o + '"><figure><div class="searchprothumg"><img  class="home_search_img" src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/' + a[0] + '" alt=/></div><figcaption><span><strong>' + e.product[t].product_name + "</strong></span><span>&#8377 " + e.product[t].price + "</span></figcaption></figure></a></div>")
                }
            } else $(".orverflow_search").attr("style", ""), $(".search_data").empty(), $("#home_no_found").show(), $("#home_new_text").html("No Products Found");
            if (e.cat.length > 0)
                for ($(".orverflow_search").css({
                        overflow: "initial"
                    }), $(".orverflow_search > figure").css({
                        overflow: "initial"
                    }), $(".new_cat_data").empty(), $("#not_found_cat").hide(), t = 0; t < e.cat.length; t++) $(".new_cat_data").append('<div class="catagorycol"><a href="' + baseURL + catSlug + "/" + e.cat[t].seourl + '"><figure><img src="https://d1eohs8f9n2nha.cloudfront.net/images/category/' + e.cat[t].image + '" alt="catagory icon"><figcaption>' + e.cat[t].cat_name + "</figcaption></figure></a></div>");
            else $(".new_cat_data").empty(), $("#new_cat_data").html("No Category Found")
        }
    })
}

function verify_otp() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL + "site/User/verify_otp",
        data: {
            otp: $("#new_otp").val(),
            mobile_number: $("#phone_no").val()
        },
        success: function(e) {
            200 == e.status_code ? ($("#is_validate").html("(Verified)"), $("#is_validate").css({
                color: "green"
            }), $(".otp_section").hide(), $("#do_verify").hide(), $("#clockdiv").hide(), sweetAlert({
                title: "Success!",
                text: e.message,
                type: "success",
                showCancelButton: !1,
                closeOnConfirm: !0,
                animation: "slide-from-top",
                showConfirmButton: !0
            })) : sweetAlert({
                title: "Opps!",
                text: e.message,
                type: "error",
                showCancelButton: !1,
                closeOnConfirm: !0,
                animation: "slide-from-top",
                showConfirmButton: !0
            })
        }
    })
}

function Verify_button() {
    $("#new_otp").val(""), 10 == $("#phone_no").val().length ? ($("#home_do_verify").show(), $(".small-text-details").css("position", "absolute")) : $("#home_do_verify").hide()
}

function Send_otp() {
    $("#home_do_verify").attr("disabled"), $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL + "site/User/send_otp",
        data: {
            send_to: $("#phone_no").val()
        },
        success: function(e) {
            "success" == e ? ($(".otp_field").hide(), $(".Verify_otp_field").show(), $("#error_msg").hide()) : ($("#error_msg").show(), $("#error_msg").text(e))
        }
    })
}

function verify_otp_home() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL + "site/User/verify_otp",
        data: {
            otp: $("#new_otp").val(),
            mobile_number: $("#phone_no").val()
        },
        success: function(e) {
            200 == e.status_code ? ($(".otp_field").show(), $(".Verify_otp_field").hide(), $("#home_do_verify").hide(), $("#sign_up_button *").removeAttr("disabled"), alert(e.message)) : alert(e.message)
        }
    })
}

function CreateZohoCases(e, t) {
    $("#overlay").show(), $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL + "site/zoho/CreateAllZohoInvoices",
        data: {
            user_id: e,
            dealcodenumber: t
        },
        success: function(e) {
            200 == e.code && $("#overlay").css("display", "none")
        }
    })
}


function OpenOfficeInquiryPage(){
    $('#showInquiryForm').modal('show');
       $.ajax({
                type: 'GET',
                dataType: 'json',
                url: baseURL + 'site/user/getUserDetails',
                data:{ },
                success:function(response){
                    if(response.data){
                        $('#inquiry_user_name').val(response.data.full_name);
                        $('#inquiry_user_email').val(response.data.email);
                        $('#inquiry_user_mobile').val(response.data.phone_no);
                        $('#inquiry_user_city').val(response.data.city);
                    }
                }

            });
}

function SubmitAddOfficeFurnishtureRequest(){
    $("#office_furniture_inquiry").validate({
        rules: {
            inquiry_user_name: {
                required:true, 
                minlength: 2,
                maxlength: 20              
            },
            inquiry_user_email: {
                required:true,
                email: true                  
            },                
            inquiry_user_mobile: {
                required:true,
                minlength:10,
                maxlength:10,
                number: true            
            },
            inquiry_user_city: {
                required:{
                    depends: function(element){
                        if('default' == $('#inquiry_user_city').val()){
                            $('#inquiry_user_city').val('');
                        }
                        return true;
                    }
                }                         
            }, 
            inquiry_user_remarks: {
                required:true,
                minlength: 30,
                maxlength: 400                             
            },
        },          
        messages:{ 
            inquiry_user_name: {
                required  : "Name cannot be empty.",
                minlength : "Name has atleast 2 character long.",
                maxlength : "Name should be 20 characters maximum."
            },              
            inquiry_user_email: {
                required  : "Email cannot be empty.",
                 email: "Please enter a valid email address."
            },
            inquiry_user_mobile: {
                required  : "Number cannot be empty.",
                minlength : "Number has atleast 10 character long.",
                maxlength : "Number should be 10 characters maximum.",
                number: "Please enter a valid Mobile Number."
            },   
            inquiry_user_city: {
                required : "City cannot be empty."
            },
            inquiry_user_remarks: {
                required : "Remark cannot be empty.",
                minlength : "Remark has atleast 30 character long.",
                maxlength : "Remark length should be 400 characters maximum."
            },
        },
        submitHandler: function(form) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: baseURL + 'site/product/OfficeFurniureRequest',
                data:{
                    'user_name':$('#inquiry_user_name').val(),
                    'email':$('#inquiry_user_email').val(),
                    'phone':$('#inquiry_user_mobile').val(),
                    'city':$('#inquiry_user_city').val(),
                    'remark':$('#inquiry_user_remarks').val(),
                },
                beforeSend:function(){
                    $('#overlay').show();
                    $('#requestSubmitButton').attr('disabled','disabled');
                    $('#furniture_order_msg').show();
                    $('#furniture_order_msg').html("<font color='green'>Thank You For Your Order</font>");
                },
                success:function(response){
                    $('#overlay').css('display','none');
                    if(response){
                        sweetAlert({
                            title: "Success!",
                            text: 'Thank you for your enquiry. We will get in touch with you shortly.',
                            type: "success",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            animation: "slide-from-top",
                            showConfirmButton: true,      
                        },  function(isConfirm){   
                                    if (isConfirm) {
                                       $('#showInquiryForm').modal('hide');
                                    }
                                }
                        ); 
                        $('#furniture_order_msg').fadeOut('slow');
                        $('#office_furniture_inquiry')[0].reset();
                        $('#requestSubmitButton').removeAttr('disabled','disabled');
                    }
                }

            });
        }   
    });

}



//caputre all offline orders from one specific user
function CreateUserAndPlaceOrder(){
    $.validator.addMethod('decimal', function(value, element) {
        return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
    }, "Please enter a correct number, format 0.00");

    $("#offlineUserForm").validate({
        rules: {
            offline_full_name: 'required',
            offline_email: {
                required: true,
                email: true,
            },
            offline_mobile:{
                required:true,
                minlength:10,
                maxlength:10,
                number: true   
            },
            offline_address1:'required',
            offline_state:'required',
            offline_postal_code:{
                required:true,
                minlength: 5,
                maxlength: 6     
            },
            offline_city:{
                required:{
                    depends: function(element){
                        if('default' == $('#offline_city').val()){
                            $('#offline_city').val(null);
                        }
                        return true;
                    }
                }   
            },
            rental_amount:{
                decimal: true   
            },
        },          
        messages:{ 
            offline_full_name: 'Full name cannot be empty.',
            offline_email: {
                required  : "Email cannot be empty.",
                email: "Please enter a valid email address."
            },
            offline_mobile:{
                required  : "Number cannot be empty.",
                minlength : "Number has atleast 10 character long.",
                maxlength : "Number should be 10 characters maximum.",
                number: "Please enter a valid Mobile Number."
            },
            offline_address1:'Address1 cannot be empty.',
            offline_state:"State cannot be empty.",
            offline_postal_code:{
                required : "Postal code cannot be empty.",
                minlength : "Postal code has atleast 5 character long.",
                maxlength : "Postal code length should be 6 characters maximum."
            },
            offline_city:"Please select city.",
            rental_amount:{
             decimal: "Please enter a price only."
            },   
        },
        submitHandler: function(form) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: baseURL + 'site/cart/placeofflineOrder',
                data:{
                    'user_name':$('#offline_full_name').val(),
                    'email':$('#offline_email').val(),
                    'mobile_no':$('#offline_mobile').val(),
                    'alert_mobile_no':$('#offline_Alter_phone').val(),
                    'password':$('#offline_password').val(),
                    'address1':$('#offline_address1').val(),
                    'address2':$('#offline_address2').val(),
                    'state':$('#state').val(),
                    'postal_code':$('#offline_postal_code').val(),
                    'city':$('#city').val(),
                    'order_status':$('#order_status').val(),
                    'rental_amount': $('#rental_amount').val() ?  $('#rental_amount').val() : 0
                },
                beforeSend:function(){
                    $('#overlay').show();
                },
                success:function(response){
                    $('#overlay').hide();
                    if(response.success == 1){
                        swal({
                            title: "Success!",
                            text: 'Your order submitted successfully.',
                            animation: "slide-from-bottom",
                            type: 'success',
                            showConfirmButton: true,
                            confirmButtonText : 'Okay',
                            confirmButtonColor: "#e8e8e8",
                            allowOutsideClick: true  
                        },  function(isConfirm){   
                            if (isConfirm) {
                                $('#sectional').hide();
                                $('#add_new_product').empty();
                                $('#new_product_added').empty();
                                $('.priceaddon').hide();
                                $('.btncartdiv').hide();
                                $('#subtotal').hide();
                                $('#add_new_product').append('<div class="emptycartbar"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/empty-cart-icn.svg" alt="Empaty cart"><h5>Your Cart is Empty</h5><p>Looks like you have not chosen <br> any product yet</p></div>');
                                $('#new_product_added').append('<div class="emptycartbar"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/empty-cart-icn.svg" alt="Empaty cart"><h5>Your Cart is Empty</h5><p>Looks like you have not chosen <br> any product yet</p></div>');
                                $('#empty_msg').show();
                                $('#title').hide();
                                $('.emptycartbar').show();
                                setTimeout(function() {
                                    location.href = baseURL + 'purchases';
                                }, 6000);
                            }
                        });
                    }else{
                        swal({
                            title: "Error!",
                            text: response.msg,
                            animation: "slide-from-bottom",
                            type: 'error',
                            showConfirmButton: true,
                            confirmButtonText : 'Okay',
                            confirmButtonColor: "#e8e8e8",
                            allowOutsideClick: true  
                        },  function(isConfirm){   
                            if (isConfirm) {
                               
                            }
                        });
                    }
                }
            });
        }   
    });
}


function CreateOfflineOrderZohoInvoices(user_id,dealcodenumber){
    $('#overlay').show();
     $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseURL + 'site/handleofflinepayment/CreateAllZohoInvoicesIndirect',
            data:{
                'user_id': user_id,
                'deal_code': dealcodenumber
            },
            success:function(response){
                if(response.code == 200){
                    $('#overlay').css('display','none');
                }
            }

    });
}

function ApprovePayment(){
    if($('#transaction_id').val().trim() == ''){
        alert('Please Enter Transaction ID'); $('#transaction_id').focus();
    }else if($('#order_date').val().trim() == ''){
         alert('Please Select Date'); $('#order_date').focus();
    }else{

        $.confirm({
            'title': '',
            'message': 'Are your sure want to update order?.',
            'buttons': {
                'Yes': {
                    'class': 'yes',
                    'action': function() {
                        $.ajax({
                            type: 'post',
                            url: baseURL + 'admin/order/update_offline_orders',
                            data: {
                                'transaction_id':$('#transaction_id').val(),
                                'payment_mode':$('#payment_mode').val(),
                                'order_date':$('#order_date').val(),
                                'dealcodenumber':$('#dealCodeNumber').val(),
                                'user_id':$('#user_id').val()
                            },
                            beforeSend:function(){
                                $('#admin_loader').show();
                            },
                            success: function(response) {
                                if(response.code == 200){
                                    $.ajax({
                                        type: 'post',
                                        url: baseURL + 'site/handleofflinepayment/adminCreateCaseAndInvoices',
                                        data: {
                                            'user_id':$('#user_id').val(),
                                            'dealcodenumber':$('#dealCodeNumber').val()
                                        },
                                        success: function(response) {
                                            $('#admin_loader').hide();
                                            // alert(response.message);
                                            location.reload();
                                        }
                                    });
                                }else{
                                    // alert(response.message);
                                }
                            }
                        });
                    }
                },
                'No': {
                    'class': 'no',
                    'action': function() {
                        console.log("cancelled by user");
                    }
                }
            }
        });    
    }
}

function gotBackCartPage(){
    $('#sectional div:nth-child(2)').removeAttr('style');
    $('#sectional div:first-child').addClass('open');
    $('#sectional div:first-child').removeClass('valid');
}


function change_city(){
    $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseURL + 'crone/get_state',
            data:{
                'city': $('#city').val()
            },
            success:function(response){
               $('#state').val(response.data);
            }

    });
}

//check social login is valid or not
function Register_with_facebook(){
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test($('#check_signup_email').val()) == false) {
        alert('Invalid Email Address');
        return (false);
    }
    var value = $('#facebook_reponse').val();
    var user_data = {};
    user_data  = JSON.parse(value);
    user_data.new_email  = $('#check_signup_email').val();
     $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseURL + 'site/user/login_with_facebook',
            data: user_data,
            success:function(response){
                if(response.status == 400){
                    $('#email_exists_error').show();
                    $('#email_exists_error').html(response.message);

                console.log(response);
                }
                else{
                     location.reload();
                }
            }

    });
    // console.log(user_data);

}
